PHONY: fix-permissions
fix-permissions:
	docker run --privileged -v $(HOME)\:/h ubuntu chown -R $(shell id -u)\:$(shell id -g) /h