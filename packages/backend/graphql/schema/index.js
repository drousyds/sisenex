const { gql } = require("apollo-server");


const { pessoaTypeDef, pessoaResolver } = require("./pessoa.js");
const { apresentacaoTypeDef, apresentacaoResolver } = require("./apresentacao.js");
const { projetoTypeDef, projetoResolver } = require("./projeto.js");
const { unidadeTypeDef, unidadeResolver } = require("./unidade.js");
const { perguntaTypeDef, perguntaResolver } = require("./pergunta.js");
const { categoriaTypeDef, categoriaResolver } = require("./categoria.js");
const { avaliacaoTypeDef, avaliacaoResolver } = require("./avaliacao.js");
const { areaTematicaTypeDef, areaTematicaResolver } = require("./areaTematica.js");
const { eventoTypeDef, eventoResolver } = require("./evento.js");
const { campusTypeDef, campusResolver } = require("./campus.js");
const { reportTypeDef, reportResolver } = require("./report.js");
const {autenticacaoTypeDef, autenticacaoResolver} = require("./autenticacao.js");
const { solicitacaoTypeDef, solicitacaoResolver } = require("./solicitacaoLGPD.js");
const { solicitacao2TypeDef, solicitacao2Resolver } = require("./solicitacaoLGPD2.js");
const { relatorioTypeDef, relatorioResolver } = require("./relatorio.js");


const Query = gql`
  #caso eu precise criar novas queries não relacionadas a nenhum tipo
  type Query {
    _dummy:String   #é necessário pelo menos um
  }
  type Mutation {
    _dummy:String   #é necessário pelo menos um
  }
`;
const Resolver = {};   //caso eu precise criar novas mutations não relacionadas a nenhum tipo

const resolvers = [Resolver, pessoaResolver, apresentacaoResolver, projetoResolver, unidadeResolver,perguntaResolver, categoriaResolver, 
  avaliacaoResolver, areaTematicaResolver, campusResolver, eventoResolver, reportResolver, autenticacaoResolver,solicitacaoResolver,solicitacao2Resolver, relatorioResolver]

const typeDefs = [Query, apresentacaoTypeDef, pessoaTypeDef, projetoTypeDef, unidadeTypeDef,perguntaTypeDef, categoriaTypeDef, 
  avaliacaoTypeDef, areaTematicaTypeDef, campusTypeDef, eventoTypeDef, reportTypeDef, autenticacaoTypeDef,solicitacaoTypeDef,solicitacao2TypeDef,relatorioTypeDef]

module.exports = { typeDefs, resolvers };
