const { gql } = require('apollo-server');

const SolicitacaoLGPD = require('../../objectionDatabase/queries/solicitacaoLGPD2');
const Pessoas = require('../../objectionDatabase/queries/pessoas');

const solicitacao2TypeDef = gql`
    extend type Query{
      solicitacoesLGPD: [Solicitacao2!]
    }
    type Solicitacao2 {
      idSolicitacao: Int!
      tipoSolicitacao: Int!
      textoSolicitacao: String 
      pessoa: Pessoa
      nomeSolicitacao: String!
      sobrenomeSolicitacao: String!
      emailPessoa: String!
      statusSolicitacao: String
    }
`;


const solicitacao2Resolver = {
  Query: {
    solicitacoesLGPD: (obj, args, context, info) =>{
      return SolicitacaoLGPD.getSolicitacoesLGPD();
    }
  },
  Solicitacao2: {
    pessoa: ({ idPessoa, idVinculoPessoa }) => {
      return Pessoas.getPessoa({ idPessoa, idVinculoPessoa });
    }
  }    
};


module.exports = {
    solicitacao2TypeDef,
    solicitacao2Resolver
  };