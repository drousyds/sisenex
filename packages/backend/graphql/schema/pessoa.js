const { gql, ApolloError, withFilter } = require("apollo-server");
const md5 = require('md5');
const knex = require('../../knexinstance')

const Pessoas = require('../../objectionDatabase/queries/pessoas');
const Avaliadores = require('../../objectionDatabase/queries/avaliadores');
const Monitores = require('../../objectionDatabase/queries/monitores');
const Avaliacoes = require('../../objectionDatabase/queries/avaliacoes');
const Apresentacoes = require('../../objectionDatabase/queries/apresentacoes');
const Categorias = require('../../objectionDatabase/queries/categorias');

const pessoaTypeDef = gql`
  extend type Query {
    pessoa(idPessoa: String!, idVinculoPessoa: String!): Pessoa
    pessoas: [Pessoa!]
    avaliadores:[Avaliador!]
    monitores:[Monitor!]
    gerentes:[Gerente!]
    
    apresentacoesAvaliador(idPessoa: String!, idVinculoPessoa: String!): [Apresentacao!]
  }

  extend type Mutation {
    iniciarApresentacao(input:IniciarApresentacaoInput!): Apresentacao!
    fecharApresentacao(input:FecharApresentacaoInput!): Boolean!
    baterAvaliador(input:BaterAvaliadorInput!): Avaliador!
    removerAvaliadorDaApresentacao(input:RemoverAvaliadorDaApresentacaoInput!): Boolean!
    login(input:loginInput!):loginOutput!
    atualizarPessoa(input:atualizarPessoaInput!):Pessoa!
    excluirPessoa(input:excluirPessoaInput!):Boolean!
    avaliarProjeto(input:avaliarProjetoInput!):Boolean!
    avaliarProjetoPorUmAvaliador(input:avaliarProjetoPorUmAvaliadorInput!):Boolean!
    liberarProjeto(input:liberarProjetoInput!):Projeto!
    liberarProjetoParaUmAvaliador(input:liberarProjetoParaUmAvaliadorInput!):Projeto!
    fecharProjeto(input:fecharProjetoInput!):Projeto!
    fecharProjetoParaUmAvaliador(input:fecharProjetoParaUmAvaliadorInput!):Projeto!
    removerProjetoDeApresentacao(input:removerProjetoDeApresentacaoInput!):Boolean!
    criarPessoa(input: criarPessoaInput!):Pessoa! 
    alocarAvaliadoresVideo: Boolean!
    liberarAvaliadorProjetoPorCategoria(idCategoria: Int!): Boolean!
  }

  type Subscription {
    projetoLiberado(input: liberarProjetoInput): Projeto!
    projetoFechado(input: fecharProjetoInput): Projeto!
    projetoLiberadoParaUmAvaliador(input: liberarProjetoParaUmAvaliadorInput): Projeto!
    projetoFechadoParaUmAvaliador(input: fecharProjetoParaUmAvaliadorInput): Projeto!
    subBaterAvaliador(input: BaterAvaliadorInput): Avaliador!
    apresentacaoFechada(input: FecharApresentacaoInput): Apresentacao!
    projetoAvaliado(input: projetoLiberadoInput): Avaliacao!
    avaliadorRemovidoDaApresentacao(input: RemoverAvaliadorDaApresentacaoInput): Apresentacao!
	}

  #Inputs

  input projetoLiberadoInput {
    idProjeto: Int!
  }

  input projetoAvaliadoInput {
    idProjeto: Int!
  }

  input IniciarApresentacaoInput {
    codigoApresentacao: String!
  }

  # type IniciarApresentacao{
  #   apresentacao:Apresentacao!
  # }

  input FecharApresentacaoInput {
    codigoApresentacao: String!
  }

  # type FecharApresentacao{
  #   status:Boolean
  # }

  input BaterAvaliadorInput{
    idPessoa: String!
    idVinculoPessoa: String!
    idApresentacao: Int!
    id2Apresentacao: String!
  }

  # type BaterAvaliador{
  #   avaliador:Avaliador!
  # }

  input RemoverAvaliadorDaApresentacaoInput{
    idPessoa: String!
    idVinculoPessoa: String!
    idApresentacao: Int!
    id2Apresentacao: String!
  }

  # type RemoverAvaliadorDaApresentacao{
  #   status:Boolean!
  # }

  input avaliarProjetoInput{
    avaliacao: AvaliacaoInput!
  }

  input avaliarProjetoPorUmAvaliadorInput{
    idPessoaGerente: String!
    idVinculoPessoaGerente: String!
    avaliacao: AvaliacaoInput!
    nomeAvaliadorPapel: String!
  }

  # input fecharProjeto{
  #   avaliacao: AvaliacaoInput!
  # }

  input liberarProjetoInput{
    idProjeto: Int!
  }

  input liberarProjetoParaUmAvaliadorInput{
    idProjeto: Int!
    idPessoa: String!
    idVinculoPessoa: String!
  }

  input fecharProjetoInput{
    idProjeto: Int!
  }

  input fecharProjetoParaUmAvaliadorInput{
    idProjeto: Int!
    idPessoa: String!
    idVinculoPessoa: String!
  }

  input removerProjetoDeApresentacaoInput{
    idProjeto: Int!
  }

  input atualizarPessoaInput{
    idPessoa: String!
    idVinculoPessoa: String!
    matriculaPessoa: String
    nomePessoa: String
    nomeSocialPessoa: String
    lotacaoPessoa: String
    emailPessoa: String
    telefonePessoa: String
    aptidaoPessoa: Int
    avaliadorPessoa: Int
    monitorPessoa: Int
    gerentePessoa: Int
    administradorPessoa: Int
    avatarUrl:String
    idCategoria:Int
  }

  input excluirPessoaInput{
    idPessoa: String!
    idVinculoPessoa: String!
  }

  input loginInput{
    digitoVerificador: String!
    idPessoa: String!
    idVinculoPessoa: String!
    matriculaPessoa: String
    nomePessoa: String
    nomeSocialPessoa: String
    lotacaoPessoa: String
    emailPessoa: String
    telefonePessoa: String
    aptidaoPessoa: Int
    avaliadorPessoa: Int
    monitorPessoa: Int
    gerentePessoa: Int
    administradorPessoa: Int
    avatarUrl:String
  }

  input criarPessoaInput{
    idPessoa: String!
    idVinculoPessoa: String!
    matriculaPessoa: String
    nomePessoa: String
    nomeSocialPessoa: String
    lotacaoPessoa: String
    emailPessoa: String
    telefonePessoa: String
    aptidaoPessoa: Int
    avaliadorPessoa: Int
    monitorPessoa: Int
    gerentePessoa: Int
    administradorPessoa: Int
    avatarUrl:String
    idCategoria: Int
  }

  #Types

  interface Pessoa {
    idPessoa: String!
    idVinculoPessoa: String!
    matriculaPessoa: String
    nomePessoa: String
    nomeSocialPessoa: String
    lotacaoPessoa: String
    emailPessoa: String
    telefonePessoa: String
    aptidaoPessoa: Int!
    avaliadorPessoa: Int
    monitorPessoa: Int
    gerentePessoa: Int
    administradorPessoa: Int
    avatarUrl:String
    categoria: Categoria
    apresentacoesAlocacao:[Apresentacao!]
    created_at: String
  }

  type Monitor implements Pessoa {
    idPessoa: String!
    idVinculoPessoa: String!
    matriculaPessoa: String
    nomePessoa: String
    nomeSocialPessoa: String
    lotacaoPessoa: String
    emailPessoa: String
    telefonePessoa: String
    aptidaoPessoa: Int!
    avaliadorPessoa: Int
    monitorPessoa: Int
    gerentePessoa: Int
    administradorPessoa: Int
    avatarUrl:String
    apresentacoesAlocacao:[Apresentacao!]
    categoria: Categoria
    created_at: String
  }

  type Ouvinte implements Pessoa {
    idPessoa: String!
    idVinculoPessoa: String!
    matriculaPessoa: String
    nomePessoa: String
    nomeSocialPessoa: String
    lotacaoPessoa: String
    emailPessoa: String
    telefonePessoa: String
    aptidaoPessoa: Int!
    avaliadorPessoa: Int
    monitorPessoa: Int
    gerentePessoa: Int
    administradorPessoa: Int
    avatarUrl:String
    categoria: Categoria
    apresentacoesAlocacao:[Apresentacao!]
    created_at: String
  }

  type Gerente implements Pessoa {
    idPessoa: String!
    idVinculoPessoa: String!
    matriculaPessoa: String
    nomePessoa: String
    nomeSocialPessoa: String
    lotacaoPessoa: String
    emailPessoa: String
    telefonePessoa: String
    aptidaoPessoa: Int!
    avaliadorPessoa: Int
    monitorPessoa: Int
    gerentePessoa: Int
    administradorPessoa: Int
    avatarUrl:String
    projetosParaAvaliar: [Projeto!]
    apresentacoesAlocacao:[Apresentacao!]
    avaliacoes(idProjeto: Int): [Avaliacao!]
    categoria: Categoria
    created_at: String
  }

  type Administrador implements Pessoa {
    idPessoa: String!
    idVinculoPessoa: String!
    matriculaPessoa: String
    nomePessoa: String
    nomeSocialPessoa: String
    lotacaoPessoa: String
    emailPessoa: String
    telefonePessoa: String
    aptidaoPessoa: Int!
    avaliadorPessoa: Int
    monitorPessoa: Int
    gerentePessoa: Int
    administradorPessoa: Int
    avatarUrl:String
    projetosParaAvaliar: [Projeto!]
    apresentacoesAlocacao:[Apresentacao!]
    avaliacoes(idProjeto: Int): [Avaliacao!]
    categoria: Categoria
    created_at: String
  }
  type Avaliador implements Pessoa {
    idPessoa: String!
    idVinculoPessoa: String!
    matriculaPessoa: String
    nomePessoa: String
    nomeSocialPessoa: String
    lotacaoPessoa: String
    emailPessoa: String
    telefonePessoa: String
    aptidaoPessoa: Int!
    avaliadorPessoa: Int
    monitorPessoa: Int
    gerentePessoa: Int
    administradorPessoa: Int
    avatarUrl:String
    projetosParaAvaliar: [Projeto!]
    apresentacoesAlocacao:[Apresentacao!]
    avaliacoes(idProjeto: Int): [Avaliacao!]
    categoria: Categoria
    created_at: String
  }
  type loginOutput{
    token: String!
    pessoa:Pessoa!
  }
`;

const subsChannels = {
  PROJETO_LIBERADO:                "PROJETO_LIBERADO",
  PROJETO_FECHADO:                 "PROJETO_FECHADO",
  PROJETO_LIBERADO_AVALIADOR:      "PROJETO_LIBERADO_AVALIADOR",
  PROJETO_FECHADO_AVALIADOR:       "PROJETO_FECHADO_AVALIADOR",
  APRESENTACAO_AVALIADOR_REMOVIDO: "APRESENTACAO_AVALIADOR_REMOVIDO",
  APRESENTACAO_FECHADA:            "APRESENTACAO_FECHADA",
  AVALIACAO_SUBMETIDA:             "AVALIACAO_SUBMETIDA",
  BATER_AVALIADOR:                 "BATER_AVALIADOR"
}

const pessoaResolver = {
	Subscription: {
    projetoLiberado: {
      subscribe: withFilter(
        (_, __, { pubsub })  => pubsub.asyncIterator(subsChannels.PROJETO_LIBERADO), 
        (payload, variables) => payload.projetoLiberado.idProjeto === variables.input.idProjeto),
    },
    projetoFechado: {
      subscribe: withFilter(
        (_, __, { pubsub })  => pubsub.asyncIterator(subsChannels.PROJETO_FECHADO), 
        (payload, variables) => payload.projetoFechado.idProjeto === variables.input.idProjeto),
    },
    projetoLiberadoParaUmAvaliador: {
      subscribe: withFilter(
        (_, __, { pubsub })  => pubsub.asyncIterator(subsChannels.PROJETO_LIBERADO_AVALIADOR), 
        (payload, variables) => payload.projetoFechadoParaUmAvaliadoridProjeto === 
                                  variables.input.idProjeto &&
                                payload.input.projetoFechadoParaUmAvaliadoridPessoa === 
                                  variables.input.idPessoa &&
                                payload.input.projetoFechadoParaUmAvaliadoridVinculoPessoa === 
                                  variables.input.idVinculoPessoa 
      ),
    },
    projetoFechadoParaUmAvaliador: {
      subscribe: withFilter(
        (_, __, { pubsub })  => pubsub.asyncIterator(subsChannels.PROJETO_FECHADO_AVALIADOR), 
        (payload, variables) => payload.projetoFechadoParaUmAvaliadoridProjeto === 
                                 variables.input.idProjeto &&
                                payload.input.projetoFechadoParaUmAvaliadoridPessoa === 
                                 variables.input.idPessoa &&
                                payload.input.projetoFechadoParaUmAvaliadoridVinculoPessoa === 
                                 variables.input.idVinculoPessoa 
        ),
    },
    subBaterAvaliador: {
      subscribe: withFilter(
        (_, __, { pubsub }) => pubsub.asyncIterator(subsChannels.BATER_AVALIADOR),
        (payload, variables) => payload.subBaterAvaliador.idPessoa === variables.input.idPessoa
      ),
    },
    apresentacaoFechada: {
      subscribe: withFilter(
        (_, __, { pubsub }) => pubsub.asyncIterator(subsChannels.APRESENTACAO_FECHADA),
        (payload, variables) => payload.apresentacaoFechada.codigoApresentacao === variables.input.codigoApresentacao
      ),
    },
    projetoAvaliado: {
      subscribe: withFilter(
        (_, __, { pubsub }) => pubsub.asyncIterator(subsChannels.AVALIACAO_SUBMETIDA),
        (payload, variables) => payload.projetoAvaliado.idProjeto == variables.input.idProjeto 
        //TODO: BUG projetoAvaliado.idProjeto eh uma string?
      ),
    },
    avaliadorRemovidoDaApresentacao: {
      subscribe: withFilter(
        (_, __, { pubsub }) => pubsub.asyncIterator(subsChannels.APRESENTACAO_AVALIADOR_REMOVIDO),
        (payload, variables) => payload.avaliadorRemovidoDaApresentacao.idApresentacao === variables.input.idApresentacao &&
                                payload.avaliadorRemovidoDaApresentacao.id2Apresentacao === variables.input.id2Apresentacao
      ),
    },
	},
  Query: {
    pessoa: (obj, args, context, info) => {
      return Pessoas.getPessoa(args);
    },
    pessoas: (obj, args, context, info) => {
      return Pessoas.getPessoas(args);
    },
    avaliadores: (obj, args, context, info) => {
      return Avaliadores.getAvaliadores(args);
    },
    monitores: (obj, args, context, info) => {
      return Monitores.getMonitores(args);
    },
    apresentacoesAvaliador: (obj, args, context, info) => {
      return Avaliadores.getApresentacoesAvaliador(args);
    },
    //gerentes: (obj, args, context, info) => {
    //   return Gerentes.getGerentes(args)
    // }
  },
  Monitor:{
    apresentacoesAlocacao: (obj, args, context, info) => {
     return Avaliadores.getListaDeApresentacoesAlocado(obj);
    }
  },
  Avaliador: {
    avaliacoes: ({ idPessoa, idVinculoPessoa }, { idProjeto }, context, info) => {
      return Avaliacoes.getAvaliacoesPorAvaliador({ idPessoa, idVinculoPessoa, idProjeto })
    },
    projetosParaAvaliar: (obj, args, context, info) => {
      return Avaliadores.getProjetosParaAvaliar(obj);
    },
    categoria: ({ idCategoria }) => {
      return Categorias.getCategoria({ idCategoria });
    },
    apresentacoesAlocacao: (obj, args, context, info) => {
      return Avaliadores.getListaDeApresentacoesAlocado(obj);
    }
  },
  Gerente: {
    avaliacoes: ({ idPessoa, idVinculoPessoa }, { idProjeto }, context, info) => {
      return Avaliacoes.getAvaliacoesPorAvaliador({ idPessoa, idVinculoPessoa, idProjeto })
    },
    projetosParaAvaliar: (obj, args, context, info) => {
      return Avaliadores.getProjetosParaAvaliar(obj);
    },
    categoria: ({ idCategoria }) => {
      return Categorias.getCategoria({ idCategoria });
    },
    apresentacoesAlocacao: (obj, args, context, info) => {
      return Avaliadores.getListaDeApresentacoesAlocado(obj);
    }
  },
  Administrador: {
    avaliacoes: ({ idPessoa, idVinculoPessoa }, { idProjeto }, context, info) => {
      return Avaliacoes.getAvaliacoesPorAvaliador({ idPessoa, idVinculoPessoa, idProjeto })
    },
    projetosParaAvaliar: (obj, args, context, info) => {
      return Avaliadores.getProjetosParaAvaliar(obj);
    },
    categoria: ({ idCategoria }) => {
      return Categorias.getCategoria({ idCategoria });
    },
    apresentacoesAlocacao: (obj, args, context, info) => {
      return Avaliadores.getListaDeApresentacoesAlocado(obj);
    }
  },
  Pessoa: {
    __resolveType(obj, context, info) {
      if (obj.administradorPessoa == 1) return 'Administrador';
      else if (obj.gerentePessoa == 1) return 'Gerente';
      else if (obj.avaliadorPessoa == 1) return 'Avaliador';
      else if (obj.monitorPessoa == 1) return 'Monitor';
      else return 'Ouvinte';
    },
    categoria: ({ idCategoria }) => {
      return Categorias.getCategoria({ idCategoria });
    }
  },
  Mutation: {
    iniciarApresentacao: (obj, { input: { codigoApresentacao } }, info) => {
      const data = {
        codigoApresentacao,
        dataInicioApresentacao: knex.fn.now(),
        disponibilidadeApresentacao: 1
      }

      return Apresentacoes.iniciarApresentacao(data);
    },
    fecharApresentacao: async (obj, { input: { codigoApresentacao } }, { pubsub }) => {
      const data = {
        codigoApresentacao,
        dataFimApresentacao: knex.fn.now()
      }

      if (res = await Apresentacoes.fecharApresentacao(data)) {
        pubsub.publish(subsChannels.APRESENTACAO_FECHADA,
          { apresentacaoFechada: await Apresentacoes.getApresentacao({ codigoApresentacao }) });
      }

      return res
    },
    baterAvaliador: async (obj, { input }, {pubsub}) => {
      let res = Monitores.baterAvaliador(input);
      pubsub.publish(subsChannels.BATER_AVALIADOR,
        { subBaterAvaliador: await res });
      return res; 
    },
    removerAvaliadorDaApresentacao: async (obj, { input }, { pubsub }) => {
      if (res = await Monitores.removerAvaliadorDaApresentacao(input)) {
        const { idApresentacao, id2Apresentacao } = input;
        pubsub.publish(subsChannels.APRESENTACAO_AVALIADOR_REMOVIDO,
            { avaliadorRemovidoDaApresentacao: await Apresentacoes.getApresentacao({ idApresentacao, id2Apresentacao }) });
      }
      return res
    },
    atualizarPessoa: (obj, { input }, info) => {
      const { idPessoa, idVinculoPessoa, ...data } = input
      return Pessoas.putPessoa({ idPessoa, idVinculoPessoa }, data)
    },
    excluirPessoa: (obj, { input }, info) => {
      return Pessoas.delPessoa(input)
    },
    login: async (obj, { input }, info) => {
      const hashed = md5(`${input.idPessoa}A monad is just a monoid in the category of endofunctors, whats the problem?${input.idVinculoPessoa}`)
      if (hashed == input.digitoVerificador) {
        const pessoa = await Pessoas.postPessoa({ input })
        const token = 'asd'
        return { token, pessoa }
      }
      else
        new ApolloError('Hash não verificado')
    },
    avaliarProjeto: async (obj, { input: { avaliacao } }, { pubsub }) => {
      const { idPessoa, idVinculoPessoa, idProjeto } = avaliacao

      if(res = await Avaliadores.avaliarProjeto({ idPessoa, idVinculoPessoa, avaliacao })) {
        pubsub.publish(subsChannels.AVALIACAO_SUBMETIDA, 
          { projetoAvaliado: await Avaliacoes.getAvaliacao({ idPessoa, idVinculoPessoa, idProjeto }) });
      }

      return res
    },
    avaliarProjetoPorUmAvaliador: (obj, { input: { idPessoaGerente, idVinculoPessoaGerente, avaliacao, nomeAvaliadorPapel } }, info) => {
      const { idPessoa, idVinculoPessoa } = avaliacao
      return Avaliadores.avaliarProjetoPorUmAvaliador({ idPessoaGerente, idVinculoPessoaGerente, idPessoa, idVinculoPessoa, avaliacao, nomeAvaliadorPapel })
    },
    liberarProjeto: async (_, { input }, { pubsub }) => {
      let res = Monitores.liberarProjeto(input);
      pubsub.publish(subsChannels.PROJETO_LIBERADO, { projetoLiberado: await res });
      return res
    },
    liberarProjetoParaUmAvaliador: async (obj, { input }, { pubsub }) => {
      let res = Monitores.liberarProjetoParaUmAvaliador(input);
      pubsub.publish(subsChannels.PROJETO_LIBERADO_AVALIADOR, 
        { projetoLiberadoParaUmAvaliador: await res, input });
      return res
    },
    fecharProjeto: async (obj, { input }, { pubsub }) => {
      let res = Monitores.fecharProjeto(input);
      pubsub.publish(subs.PROJETO_FECHADO, { projetoFechado: await res });
      return res
    },
    fecharProjetoParaUmAvaliador: async (obj, { input }, { pubsub }) => {
      let res = Monitores.fecharProjetoParaUmAvaliador(input);
      pubsub.publish(subsChannels.PROJETO_FECHADO_AVALIADOR, 
        { projetoFechadoParaUmAvaliador: await res, input });
      return res
    },
    removerProjetoDeApresentacao: (obj, { input }, info) => {
      return Monitores.removerProjetoDeApresentacao(input)
    },
    criarPessoa: (obj, input , info) => {
      return Pessoas.postPessoa(input)
    },
    alocarAvaliadoresVideo: () => {
      return Avaliadores.alocarAvaliadoresVideo()
    },
    liberarAvaliadorProjetoPorCategoria: (obj, { idCategoria }) => {
      return Avaliadores.liberarAvaliadorProjetoPorCategoria(idCategoria);
    }
  }
};

module.exports = {
  pessoaTypeDef,
  pessoaResolver
};
