const { gql } = require("apollo-server");

const Campus = require('../../objectionDatabase/queries/campus')
const Unidades = require('../../objectionDatabase/queries/unidades')

const campusTypeDef = gql`
  extend type Query {
    campus(idCampus: Int!): Campus
    campi: [Campus!]
  }
  extend type Mutation {
    criarCampus(input:criarCampusInput!): Campus
    atualizarCampus(input: atualizarCampusInput!): Campus
    excluirCampus(input: excluirCampusInput!): Boolean
  }
  #Inputs
  input criarCampusInput{
    idCampus: Int!
    nomeCampus: String!
  }
  input atualizarCampusInput{
    idCampus: Int!
    nomeCampus: String
  }
  input excluirCampusInput{
    idCampus: Int!
  }

  type Campus {
    idCampus: Int!
    nomeCampus: String
    unidades: [Unidade!]
  }
`;
const campusResolver = {
  Query: {
    campus: (obj, args, context, info) => {
      return Campus.getCampus(args)       //singular
    },
    campi: (obj, args, context, info) => {
      return Campus.getCampi();       //plural
    }
  },
  Mutation: {
    criarCampus: (obj, {input}, context, info) => {
      return Campus.criarCampus(input)
    },
    atualizarCampus: (obj, {input}, context, info) => {
      return Campus.atualizarCampus(input.idCampus,input)
    },
    excluirCampus: (obj, {input}, context, info) => {
      return Campus.excluirCampus(input)
    }
  },
  Campus: {
    unidades: ({ idCampus }) => {
      return Unidades.getUnidadesByCampus({ idCampus })
    }
  }
};

module.exports = {
  campusTypeDef,
  campusResolver
};