const { gql } = require('apollo-server');
const axios = require('axios')

const autenticacaoTypeDef = gql`
    extend type Mutation {
        autenticar(code: String!): Token!
    }

    type Token{
        access_token: String!
        token_type: String!
        refresh_token: String!
        expires_in: Int!
        scope: String!
        foto: String!
        id_usuario: Int!
        nome: String!
        jti: String!
    }
`;

const autenticacaoResolver = {

    Mutation: {
        autenticar: async ( obj , {code},  contexto ,  info ) => {
            const redirect = process.env.REACT_APP_NODE_ENV === 'local'
                ? 'https%3A%2F%2Fauth.expo.io%2F%40v-in%2Fsisenex'
                : encodeURIComponent(process.env.REACT_APP_BASE_URL + '/login')

            const tokenUrl = `https://sistemas.ufpb.br/auth-server/oauth/token?code=${code}&grant_type=authorization_code&redirect_uri=${redirect}`
            const tokenRes = await axios({
                method: "post",
                url: tokenUrl,
                withCredentials: true,
                headers: {
                    Authorization: `Basic c2lzZW5leDo5M2ZiODc4YzQ4MzM5ZDBlNWM4MzgwN2YxNjFjYjY1ZA`
                }
            })
            //https%3A%2F%2Fauth.expo.io%2F%40v-in%2Fsisenex
            const token = tokenRes.data;
            return token;//{code, redirectUri, token};
        }
    }
}

module.exports = {
    autenticacaoTypeDef,
    autenticacaoResolver
};