const { gql } = require('apollo-server');

const Projetos = require('../../objectionDatabase/queries/projetos');
const Eventos = require('../../objectionDatabase/queries/eventos');
const Unidades = require('../../objectionDatabase/queries/unidades')
const Apresentacoes = require('../../objectionDatabase/queries/apresentacoes')
const AreasTematicas = require('../../objectionDatabase/queries/areasTematicas')
const Categorias = require('../../objectionDatabase/queries/categorias');
// const loaderAreasTematicas = require('../loaders/loaderAreasTematicas')
// const loaderApresentacoes = require('../loaders/loaderApresentacao')
// const loaderUnidade = require('../loaders/loaderUnidade')

// alterando para os projetos de 2020
const { updateProjects } = require('../../scripts/pegarProjetos2020') 

const projetoTypeDef = gql`
  extend type Query {
    projeto(idProjeto:Int!): Projeto
    projetos(input: projetosInput): [Projeto!]
    # projetosAvaliados: [Projeto!]
    # projetosPendentes: [Projeto!]
  }

  extend type Mutation {
    criarProjeto(input: criarProjetoInput!): Projeto
    atualizarProjeto(input: atualizarProjetoInput!): Projeto
    atualizarListaDeProjetos:Boolean
    # excluirProjeto(input:excluirProjetoInput): Boolean
  }

  input projetosInput {
    idCampus: Int
    idEvento: Int
    idAreaTematica: Int
  }

  input criarProjetoInput {
    idProjeto: Int
    tituloProjeto: String!
    descricaoProjeto: String!
    dataInicioProjeto: String!
    dataFimProjeto: String!
    anoProjeto: String!
    idAreaTematica: Int!
    idUnidade: Int!
    disponibilidadeProjeto: Int
    codigoProjeto: String
    modalidadeProjeto: String
    liberacaoProjeto: String
    idApresentacao: Int
    id2Apresentacao: String
    idCategoria: Int!
    linkArtefato: String
  }

  input atualizarProjetoInput {
    idProjeto: Int!
    tituloProjeto: String
    codigoProjeto: String
    descricaoProjeto: String
    dataInicioProjeto: String
    dataFimProjeto: String
    anoProjeto: String
    idAreaTematica: Int
    modalidadeProjeto: String
    idUnidade: Int
    disponibilidadeProjeto: Int
    liberacaoProjeto: String
    idApresentacao: Int
    id2Apresentacao: String
    idEvento: Int
    idCategoria: Int
    linkArtefato: String
    statusProjeto: String
  }

  # input excluirProjetoInput{
  #   idProjeto
  # }

  type Membro{
    idProjeto: Int!
    idPessoa: String!
    idVinculoPessoa: String!
    funcao: String!
    nomePessoa: String
  }

  type Projeto {
    idProjeto: Int!
    tituloProjeto: String!
    descricaoProjeto: String!
    dataInicioProjeto: String!
    dataFimProjeto: String!
    anoProjeto: String!
    modalidadeProjeto: String!
    disponibilidadeProjeto: Int!
    liberacaoProjeto: String!
    areaTematica: AreaTematica!
    unidade: Unidade
    codigoProjeto: String
    apresentacao: Apresentacao
    avaliacoes: [Avaliacao!]
    media: Float
    avaliadoresHabilitados: [Avaliador!]
    mediaIndividual(idPessoa:String!, idVinculoPessoa:String!): mediaPorAvaliador
    mediasIndividuais: [mediaPorAvaliador!]
    coordenador: [Membro!]
    evento: Evento
    categoria: Categoria!
    linkArtefato: String
    statusProjeto: String!
  }

  type mediaPorAvaliador {
    avaliador: Pessoa
    mediaProjeto: Float
  }
`;

const projetoResolver = {
  Query: {
    projeto: (obj, { idProjeto }, context, info) => {
      return Projetos.getProjeto(idProjeto);
    },
    projetos: (obj, { input }, context, info) => {
      // console.log(input);
      return Projetos.getProjetos(input);
    }
    // projetosAvaliados: (obj, args, context, info) => {
    //   return Projetos.getProjetosPorStatus({1});              //ainda nao implementados
    // },
    // projetosPendentes: (obj, args, context, info) => {
    //   return Projetos.getProjetosPorStatus({0});              //ainda nao implementados
    // },
  },
  Mutation: {
    criarProjeto: (obj, { input }, context, info) => {

      return Projetos.postProjeto(input);
    },
    atualizarProjeto: (obj, { input }, context, info) => {
      const { idProjeto, ...data } = input;
      return Projetos.putProjeto({ idProjeto }, data);
    },
    atualizarListaDeProjetos: async (obj, args, context, info) => {
      return await updateProjects()

    }
    // excluirApresentacao: (obj, {input}, context, info) => {
    //   return Apresentacoes.delApresentacoes(input);
    // }
  },
  Projeto: {
    areaTematica: ({ idAreaTematica }, info, context) => {
      // console.log(info.loaderAreasTematicas);
      // console.log(idAreaTematica);
      return context.loaderAreasTematicas.load(idAreaTematica);
    },
    unidade: ({ idUnidade }, info, context) => {
      // console.log(context.loaderUnidades);
      // console.log(idUnidade);
      return context.loaderUnidades.load(idUnidade);
    },
    apresentacao: ({ idApresentacao, id2Apresentacao }, info, context) => {
      // console.log(context.loaderApresentacao);
      // console.log( idApresentacao, id2Apresentacao );
      if ((id2Apresentacao || idApresentacao) == null) {
        return null
      }
      return context.loaderApresentacao.load({ idApresentacao, id2Apresentacao });
    },
    avaliacoes: ({ idProjeto }) => {
      return Projetos.getAvaliacoes({ idProjeto });
    },
    media: ({ idProjeto }) => {
      // return context.loaderEvento.load({ idProjeto});
      return Projetos.mediaProjeto({ idProjeto });
    },
    mediaIndividual: async ({ idProjeto }, { idPessoa, idVinculoPessoa }) => {
      return await Projetos.mediaDeUmAvaliador({ idProjeto, idPessoa, idVinculoPessoa });
    },
    mediasIndividuais: ({ idProjeto }) => {
      return Projetos.mediaPorAvaliador({ idProjeto });
    },
    avaliadoresHabilitados: ({ idProjeto }) => {
      return Projetos.avaliadoresHabilitados({ idProjeto });
    },
    coordenador: ({ idProjeto }) => {
      return Projetos.getMembros({ idProjeto });
    },
    evento: ({ idEvento }) => {
      return Eventos.getEvento({ idEvento });
    },
    categoria: ({ idCategoria }) => {
      return Categorias.getCategoria({ idCategoria });
    }
  }
};

module.exports = {
  projetoTypeDef,
  projetoResolver
};
