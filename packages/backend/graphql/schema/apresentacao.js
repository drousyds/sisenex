const { gql } = require('apollo-server');


const Apresentacoes = require('../../objectionDatabase/queries/apresentacoes');
const Eventos = require('../../objectionDatabase/queries/eventos');
const AreasTematicas = require('../../objectionDatabase/queries/areasTematicas');
const Categorias = require('../../objectionDatabase/queries/categorias');
const { enviarEmail } = require('../../scripts/enviarEmail');
const Projetos = require('../../objectionDatabase/queries/projetos');


const apresentacaoTypeDef = gql`
  extend type Query {
    apresentacao(input:apresentacaoInput!): Apresentacao
    apresentacoes(input:apresentacoesInput): [Apresentacao!]
  }


  extend type Mutation {
    criarApresentacao(input: criarApresentacaoInput!): Apresentacao
    atualizarApresentacao(input: atualizarApresentacaoInput!): Apresentacao
    excluirApresentacao(input: excluirApresentacaoInput): Apresentacao
    enviarEmailParaAvaliadores(input: enviarEmailParaAvaliadoresInput): Boolean
    fecharApresentacoesPorArea(idAreaTematica: Int!): Boolean
    fecharApresentacoesPorEvento(idEvento: Int!): Boolean
    liberarApresentacoesPorCategoria(idCategoria: Int!): Boolean
    adicionarProjetos(input: adicionarProjetosInput): Apresentacao
  }


  input apresentacoesInput{
    idEvento:Int
    idAreaTematica: Int
    modalidadeApresentacao: String
  }


  input apresentacaoInput{
    idApresentacao: Int
    id2Apresentacao: String
    codigoApresentacao: String
  }


  input criarApresentacaoInput {
    id2Apresentacao: String!
    salaApresentacao: String!
    horaApresentacao: String!
    dataInicioApresentacao: String
    dataFimApresentacao: String
    disponibilidadeApresentacao: Int
    modalidadeApresentacao: String
    idAreaTematica: Int
    idEvento: Int #em breve obrigatorio
    idCategoria: Int!
    linkApresentacao: String
  }


  input atualizarApresentacaoInput {
    idApresentacao: Int!
    id2Apresentacao: String!
    salaApresentacao: String
    horaApresentacao: String
    dataInicioApresentacao: String
    dataFimApresentacao: String
    disponibilidadeApresentacao: Int
    modalidadeApresentacao: String
    idAreaTematica: Int
    idEvento: Int
    idCategoria: Int
    linkApresentacao: String
  }


  input excluirApresentacaoInput {
    idApresentacao: Int!
    id2Apresentacao: String!
  }


  input enviarEmailParaAvaliadoresInput {
    idApresentacao: Int!
    id2Apresentacao: String!
  }


  input adicionarProjetosInput {
    idProjeto: Int!
    idApresentacao: Int!
    id2Apresentacao: String!
  }


  type Apresentacao {
    idApresentacao: Int!
    id2Apresentacao: String!
    salaApresentacao: String
    horaApresentacao: String
    dataInicioApresentacao: String
    dataFimApresentacao: String
    disponibilidadeApresentacao: Int
    codigoApresentacao: String!
    modalidadeApresentacao: String
    projetos: [Projeto!]
    avaliadores: [Avaliador!]
    areaTematica: AreaTematica
    evento: Evento
    categoria: Categoria!
    linkApresentacao: String
  }
`;


const apresentacaoResolver = {
  Query: {
    apresentacao: (obj, { input }, context, info) => {
      return Apresentacoes.getApresentacao(input);
    },
    apresentacoes: (obj, { input }, context, info) => {
      return Apresentacoes.getApresentacoes(input);
    }
  },
  Mutation: {
    criarApresentacao: (obj, { input }, context, info) => {
      return Apresentacoes.postApresentacoes(input);
    },
    atualizarApresentacao: (obj, { input }, context, info) => {
      const { idApresentacao, id2Apresentacao, ...data } = input;
      return Apresentacoes.putApresentacoes({ idApresentacao, id2Apresentacao }, data);
    },
    excluirApresentacao: (obj, { input }, context, info) => {
      return Apresentacoes.delApresentacoes(input);
    },
    enviarEmailParaAvaliadores: (obj, { input }, context, info) => {
      return enviarEmail(input);
    },
    fecharApresentacoesPorArea: (_obj, { idAreaTematica }) => {
      return Apresentacoes.fecharApresentacoesPorArea(idAreaTematica);
    },
    fecharApresentacoesPorEvento: (_obj, { idEvento }) => {
      return Apresentacoes.fecharApresentacoesPorEvento(idEvento);
    },
    liberarApresentacoesPorCategoria: (_obj, { idCategoria }) => {
      return Apresentacoes.liberarApresentacoesPorCategoria(idCategoria);
    },
    adicionarProjetos: (obj, { input }, context, info) => {
      const { idProjeto, ...data } = input;


      return Projetos.putProjeto({ idProjeto }, data);
    }
  },
  Apresentacao: {
    avaliadores: ({ id2Apresentacao, idApresentacao }) => {
      return Apresentacoes.getPessoas({ id2Apresentacao, idApresentacao });
    },
    projetos: ({ id2Apresentacao, idApresentacao }) => {
      return Apresentacoes.getProjetos({ id2Apresentacao, idApresentacao });
    },
    areaTematica: ({ idAreaTematica }) => {
      return AreasTematicas.getAreaTematica({ idAreaTematica });
    },
    evento: ({ idEvento }) => {
      return idEvento ? Eventos.getEvento({ idEvento }) : null;
    },
    categoria: ({ idCategoria }) => {
      return Categorias.getCategoria({ idCategoria });
    }
  }
};


module.exports = {
  apresentacaoTypeDef,
  apresentacaoResolver
};
