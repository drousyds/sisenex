const { gql } = require("apollo-server");

const Eventos = require('../../objectionDatabase/queries/eventos')
const Projetos = require('../../objectionDatabase/queries/projetos')
const Apresentacoes = require('../../objectionDatabase/queries/apresentacoes')

const eventoTypeDef = gql`
  extend type Query {
    evento(idEvento: Int!): Evento
    eventos: [Evento!]
  }
  extend type Mutation {
    criarEvento(input:criarEventoInput!): Evento
    atualizarEvento(input:atualizarEventoInput!): Evento
    excluirEvento(idEvento: Int!): Evento
    alocarProjetos(input: alocarProjetoInput): [Apresentacao!]
    alocarProjetosVideo(input: alocarProjetoVideoInput): [Apresentacao!]
  }

  input atualizarEventoInput {
    idEvento: Int!
    nomeEvento: String
    areasTematicas: [Int!]
    descricaoEvento: String
    dataInicioEvento: String
    dataFimEvento: String
    statusEvento: String
}
  input criarEventoInput {
    nomeEvento: String!
    campi:[Int!]!
    areasTematicas: [Int!]
    descricaoEvento: String
    dataInicioEvento: String!
    dataFimEvento: String!
    statusEvento: String!
}

input alocarProjetoInput {
    #localAlocacao: String
    eventoHorarioInicial: String!
    eventoHorarioFinal: String!
    numeroDeSalas: Int!
    numeroProjetosSala: Int!
    horarios: [String!]
    idEvento: Int!
    idAreaTematica: Int!
    idCategoria: Int!
  }

  input alocarProjetoVideoInput {
    horarioInicial: String!
    numeroProjetosApresentacao: Int!
    idAreaTematica: Int!
  }

  type Evento {
    idEvento: Int!
    nomeEvento: String!
    campi:[Campus!]!
    descricaoEvento: String
    dataInicioEvento: String
    dataFimEvento: String
    areasTematicas: [AreaTematica!]!
    apresentacoes(idAreaTematica: Int, modalidadeApresentacao: String): [Apresentacao!]
    projetos: [Projeto!]
    statusEvento: String!

}
`;
const eventoResolver = {
  Query: {
    eventos: (obj, args, context, info) => {
      return Eventos.getEventos(args)
    },
    evento: (obj, args, context, info) => {
      return Eventos.getEvento(args)
    }
  },
  Mutation: {
    criarEvento: (obj, { input }, context, info) => {
      const { campi, areasTematicas, ...evento } = input
      return Eventos.postEvento({ areasTematicas, campi, evento })
    },
    atualizarEvento: (obj, { input }, context, info) => {
      const { campi, areasTematicas, idEvento, ...data } = input
      return Eventos.putEvento({ campi, areasTematicas, idEvento, data })
    },
    excluirEvento: (obj, args, context, info) => {
      return Eventos.delEvento(args)
    },
    alocarProjetos: (obj, { input }, context, info) => {
      // console.log(input)
      return Eventos.alocarProjetos(input);
    },
    alocarProjetosVideo: (obj, { input }, context, info) => {
      return Eventos.alocarProjetosVideo(input);
    }

  },
  Evento: {
    areasTematicas: ({ idEvento }) => {
      return Eventos.getAreasTematicas({ idEvento })
    },
    campi: ({ idEvento }) => {
      return Eventos.getCampi({ idEvento })
    },
    apresentacoes: ({ idEvento }, args) => {
      return Apresentacoes.getApresentacoes({ idEvento, ...args })
    },
    projetos: ({ idEvento }) => {
      return Projetos.getProjetos({ idEvento })
    }
  }
};

module.exports = {
  eventoTypeDef,
  eventoResolver
};
