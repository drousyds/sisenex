const { gql } = require("apollo-server");

const Projetos = require('../../objectionDatabase/queries/projetos')
const Pessoas = require('../../objectionDatabase/queries/pessoas')
const Perguntas = require('../../objectionDatabase/queries/perguntas')
const Avaliacoes = require('../../objectionDatabase/queries/avaliacoes')

const avaliacaoTypeDef = gql`
  extend type Query {
    avaliacao(idProjeto: Int!,idPessoa:String!,idVinculoPessoa:String!): Avaliacao
  }

  input AvaliacaoInput {
    notas: [NotaPerguntaInput]!
    idProjeto: Int!
    idPessoa: String!
    idVinculoPessoa: String!
  }

  input NotaPerguntaInput {
      idPergunta:Int!
      nota: Float
      respostaTextual: String
  }
  type Avaliacao {
    media: Float!
    notas: [NotaPergunta]!
    projeto: Projeto!
    avaliador:Pessoa!
    avaliacaoManual: Int
    nomeAvaliadorPapel: String
  }
  type NotaPergunta {
      pergunta:Pergunta!
      nota: Float
      respostaTextual: String
      data: String!
  }
`;
const avaliacaoResolver = {
  Query: {
    avaliacao: (obj, args, context, info) => {
      return Avaliacoes.getAvaliacao(args)
    }
  },
  Avaliacao: {
    projeto: ({ idProjeto }) => {
      return Projetos.getProjeto(idProjeto)
    },
    avaliador: ({ idPessoa, idVinculoPessoa }) => {
      return Pessoas.getPessoa({ idPessoa, idVinculoPessoa })
    }
  },
  NotaPergunta: {
    pergunta: ({ idPergunta }) => {
      return Perguntas.getPergunta({ idPergunta });
    },
  },
};

module.exports = {
  avaliacaoTypeDef,
  avaliacaoResolver
};
