const { gql } = require('apollo-server');
const Solicitacoes = require("../../objectionDatabase/queries/solicitacaoLGPD");
const Pessoas = require('../../objectionDatabase/queries/pessoas'); 

const solicitacaoTypeDef = gql`
    
    type Solicitacao {
        idSolicitacao: Int!
        tipoSolicitacao: Int!
        textoSolicitacao: String
        pessoa: Pessoa!
        nomeSolicitacao: String!
        sobrenomeSolicitacao: String!
        emailPessoa: String!
        statusSolicitacao: String
    }

    extend type Query{
        solicitacaoLGPD(idSolicitacao: Int!): Solicitacao
        pessoaSolicitacoesLGPD(idPessoa: String!, idVinculoPessoa: String!): [Solicitacao]
    }

    extend type Mutation {
        criarSolicitacaoLGPD(input: criarSolicitacaoLGPDInput!): Solicitacao
        finalizarSolicitacoesLGPD(input: finalizarSolicitacoesLGPDInput!): Solicitacao
        atualizarSolicitacaoLGPD(input: atualizarSolicitacaoLGPDInput!): Solicitacao
    }

    input criarSolicitacaoLGPDInput{
        tipoSolicitacao: Int!
        textoSolicitacao: String
        idPessoa: String!
        idVinculoPessoa: String!
        nomeSolicitacao: String!
        sobrenomeSolicitacao: String!
        emailPessoa: String!
    }

    input finalizarSolicitacoesLGPDInput{
        idSolicitacao: Int!
    }

    input atualizarSolicitacaoLGPDInput{
        idSolicitacao: Int!
        tipoSolicitacao: Int
        textoSolicitacao: String
        idPessoa: String
        idVinculoPessoa: String
        nomeSolicitacao: String
        sobrenomeSolicitacao: String
        emailPessoa: String
        statusSolicitacao: String
    }
`;

const solicitacaoResolver = {
    Query: {
        solicitacaoLGPD: (obj, input, context, info) => {
            return Solicitacoes.getSolicitacaoById(input);
        },
        pessoaSolicitacoesLGPD: (obj, input, context, info) =>{
            return Solicitacoes.getSolicitacoes(input);
        }
    },
    Solicitacao: {
        pessoa: ({ idPessoa, idVinculoPessoa }) => {
            return Pessoas.getPessoa({ idPessoa, idVinculoPessoa });
        },
    },
    Mutation: {
        criarSolicitacaoLGPD: (obj, {input}, context, info) =>{
            return Solicitacoes.postSolicitacoes(input);
        },
        finalizarSolicitacoesLGPD: (obj, {input}, context, info) => {
            return Solicitacoes.finalizarSolicitacoes(input);
        },
        atualizarSolicitacaoLGPD: (obj, {input}, context, info) => {
            return Solicitacoes.atualizarSolicitacao(input.idSolicitacao,input);
        }
    }
};

module.exports = {
    solicitacaoTypeDef,
    solicitacaoResolver
};