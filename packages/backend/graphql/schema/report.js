const { gql } = require('apollo-server');

const Reports = require('../../objectionDatabase/queries/report');
const Apresentacoes = require('../../objectionDatabase/queries/apresentacoes')
const Pessoas = require('../../objectionDatabase/queries/pessoas');

const reportTypeDef = gql`
  extend type Query {
    report(idReport: Int!): Report
    reports: [Report!]
  }
  extend type Mutation {
    criarReport(input: criarReportInput!): Report
    excluirReport(input: excluirReportInput!): Boolean
    atualizarStatusReport(input: atualizarStatusReportInput!): Report
  }

  type Report {
    idReport: Int!
    conteudoReport: String!
    avaliador: Pessoa!
    apresentacao: Apresentacao
    dataCriacao: String
    statusReport: String!
    emailReport: String!
  }

  input criarReportInput{
    conteudoReport: String!
    idPessoa: String!
    idVinculoPessoa: String!
    emailReport: String!
    idApresentacao: Int
    id2Apresentacao: String
  }

  input atualizarStatusReportInput{
    idReport: Int!
    statusReport: String!
  }

  input excluirReportInput{
    idReport: Int!
  }
`;
const reportResolver = {
  Query: {
    report: (obj, args, context, info) => {
      return Reports.getReport(args);
    },
    reports: (obj, args, context, info) => {
      return Reports.getReports();
    }
  },
  Mutation: {
    criarReport: (obj, { input }, context, info) => {
      return Reports.postReport(input);
    },
    excluirReport: (obj, { input }, context, info) => {
      return Reports.excluirReport(input)
    },
    atualizarStatusReport: (obj, { input }, context, info) => {
      return Reports.atualizarStatusReport(input.idReport, input)
    },
  },
  Report: {
    avaliador: ({ idPessoa, idVinculoPessoa }) => {
      return Pessoas.getPessoa({ idPessoa, idVinculoPessoa });
    },
    apresentacao: ({ idApresentacao, id2Apresentacao }) => {
      return Apresentacoes.getApresentacao({ idApresentacao, id2Apresentacao });
    },


  }
};

module.exports = {
  reportTypeDef,
  reportResolver
};
