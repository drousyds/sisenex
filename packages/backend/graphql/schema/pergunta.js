const { gql } = require('apollo-server');

const Perguntas = require('../../objectionDatabase/queries/perguntas');
const Categorias = require('../../objectionDatabase/queries/categorias');

const perguntaTypeDef = gql`
  extend type Query {
    pergunta(idPergunta: Int!): Pergunta
    perguntas: [Pergunta!]
  }
  extend type Mutation {
    criarPergunta(input: criarPerguntaInput!): Pergunta
    atualizarPergunta(idPergunta: Int!): Pergunta
    excluirPergunta: Int
  }

  input criarPerguntaInput{
    idPergunta: Int!
    idCategoria: Int!
    conteudoPergunta: String!
    dataPergunta:String
  }

  type Pergunta {
    idPergunta: Int!
    conteudoPergunta: String!
    dataPergunta: String
    categoria: Categoria!
  }
`;
const perguntaResolver = {
  Query: {
    pergunta: (obj, args, context, info) => {
      return Perguntas.getPergunta({ args });
    },
    perguntas: (obj, args, context, info) => {
      return Perguntas.getPerguntas();
    }
  },
  Mutation: {
    criarPergunta: (obj, args, context, info) => {
      return Perguntas.criaPergunta(args);
    },
    atualizarPergunta: (obj, args, context, info) => {
      return Perguntas.atualizaPergunta(args);
    },
    excluirPergunta: (obj, args, context, info) => {
      return Perguntas.excluiPergunta(args);
    }
  },
  Pergunta: {
    categoria: ({ idCategoria }) => {
      return Categorias.getCategoria({ idCategoria });
    }
  }
};

module.exports = {
  perguntaTypeDef,
  perguntaResolver
};
