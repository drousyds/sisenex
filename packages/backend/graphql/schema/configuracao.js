const { gql } = require("apollo-server");

const Configuracoes = require('../../objectionDatabase/queries/configuracoes')

const ConfiguracoesTypeDef = gql`
  extend type Query {
    configuracao(idConfiguracao: Int!, idAreaTematica: Int!): Configuracao
  }
  extend type Mutation {
    criarConfiguracao(input:criarConfiguracaoInput!): Configuracao
    atualizarConfiguracao(input:atualizarConfiguracaoInput!): Configuracao
    excluirConfiguracao(input:excluirConfiguracaoInput!): Boolean
  }
  type Configuracao {
    localConfiguracao: String
    descricaoConfiguracao: String
    dataInicioConfiguracao: String
    dataFimConfiguracao: String
    evento: Evento!
    areaTematica: AreaTematica!
}
`;
const ConfiguracoesResolver = {
  Query: {
    Configuracoes: (obj, args, context, info) => {
      return Configuracoes.getConfiguracoes(args)
    },
    Configuracao: (obj, args, context, info) => {
      return Configuracoes.getConfiguracao(args)
    }
  },
  Mutation: {
    criarConfiguracoes: (obj, args, context, info) => {
      return Configuracoes.criarConfiguracoes(args)
    },
    atualizarConfiguracoes: (obj, args, context, info) => {
      return Configuracoes.atualizarConfiguracoes(args)
    },
    excluirConfiguracoes: (obj, args, context, info) => {
      return Configuracoes.excluirConfiguracoes(args)
    }
  },
  Configuracoes: {
    unidades: ({ idConfiguracoes }) => {
      return Unidades.getUnidade({ idConfiguracoes })
    }
  }
};

module.exports = {
  ConfiguracoesTypeDef,
  ConfiguracoesResolver
};