const { gql } = require("apollo-server");

const Pessoas = require('../../objectionDatabase/queries/pessoas');
const Avaliacoes = require('../../objectionDatabase/queries/avaliacoes');
const Projetos = require('../../objectionDatabase/queries/projetos');


const relatorioTypeDef = gql`
    extend type Query{
        relatoriosAvaliadores: [RelatorioAvaliador!]
        relatoriosMedias: [RelatorioMedia!]
        relatoriosNotas: [RelatorioNotas!] 
        
    }
    type RelatorioMedia{
        projeto: Projeto!
        media: Float!
        numAvaliadores: Int!
        
    }

    type RelatorioAvaliador{
        avaliador: Pessoa!
        avaliou: String! 
    }

    type RelatorioNotas{
        avaliador: Pessoa!
        projeto: Projeto!
        nota: Float!
    }

`;

const relatorioResolver = {
    Query:{
        relatoriosMedias: () => {
             return Avaliacoes.getProjetosAvaliadosId();
        },
        relatoriosAvaliadores: () => {
            return Pessoas.getPessoasAvaliadoras();
        },
        relatoriosNotas: () => {
            return Avaliacoes.getAvaliacaoIndividualIds();
        }
    },

    RelatorioMedia: {
        projeto: ({ idProjeto }) => {
            return Projetos.getProjeto(idProjeto)
        },
        media: ({ idProjeto }) => {
            return Projetos.mediaProjeto({ idProjeto});
        },
        numAvaliadores: ({ idProjeto })=> {
            return Avaliacoes.getnumAvaliadores({idProjeto});
        }
    },

    RelatorioAvaliador: {
        avaliador: ({ idPessoa, idVinculoPessoa }) => {
            return Pessoas.getPessoa({ idPessoa, idVinculoPessoa })
        },
        avaliou: ({idPessoa, idVinculoPessoa}) =>{
            return Avaliacoes.getAvaliou({idPessoa, idVinculoPessoa})
        }

    },

    RelatorioNotas: {
        avaliador: ({ idPessoa, idVinculoPessoa }) => {
            return Pessoas.getPessoa({ idPessoa, idVinculoPessoa })
        },
        projeto: ({ idProjeto }) => {
            return Projetos.getProjeto(idProjeto)
        },
        nota: ({idProjeto, idPessoa, idVinculoPessoa}) =>{
            return Avaliacoes.getMediaIndividualAvaliadorProjeto({idProjeto, idPessoa, idVinculoPessoa});
        }

    }

}

module.exports = {
    relatorioTypeDef,
    relatorioResolver
}
