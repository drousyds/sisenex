const { gql } = require("apollo-server");

const Perguntas = require('../../objectionDatabase/queries/perguntas')
const Categorias = require('../../objectionDatabase/queries/categorias')


const categoriaTypeDef = gql`
  extend type Query {
    categoria(idCategoria: Int!): Categoria
    categorias: [Categoria!]
  }
  extend type Mutation {
    criarCategoria(idCategoria: Int!): Categoria
    atualizarCategoria(idCategoria: Int!): Categoria
    excluirCategoria: Int
  }
  type Categoria {
    idCategoria: Int!
    nomeCategoria: String!
    perguntas:[Pergunta!]
  }
`;
const categoriaResolver = {
  Query: {
    categoria: (obj, args, context, info) => {
      return Categorias.getCategoria({ args })
    },
    categorias: (obj, args, context, info) => {
      return Categorias.getCategorias()
    }
  },
  Mutation:{
    criarCategoria: (obj, args, context, info) => {
      return Categorias.criaCategoria(args)
    },
    atualizarCategoria: (obj, args, context, info) => {
      return Categorias.atualizaCategoria(args)
    },
    excluirCategoria: (obj, args, context, info) => {
      return Categorias.excluiCategoria(args)
    }
  },
  Categoria: {
    perguntas: ({ idCategoria }) => {
      return Perguntas.getPerguntas({ idCategoria })
    }
  }
};

module.exports = {
  categoriaTypeDef,
  categoriaResolver
};