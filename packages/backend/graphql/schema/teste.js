const { gql } = require("apollo-server");

var carros = [
    {nome: "Montana", ano: 2009},
    {nome: "Saveiro", ano: 2008}
]

const carrosTypedef = gql`
    type Carro{
        nome: String!
        ano: Int!
    }

    extend type Query{
        carros(): [Carro!]
    }
`;

const carrosResolver = {
    carros: () => carros
}

module.exports = {
    carrosTypedef,
    carrosResolver
};