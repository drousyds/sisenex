const { gql } = require("apollo-server");
const Unidades = require('../../objectionDatabase/queries/unidades')

const unidadeTypeDef = gql`
  extend type Query {
    unidade(idUnidade:Int): Unidade
    unidades: [Unidade]
  }
  extend type Mutation {
    criarUnidade(input: criarUnidadeInput!): Unidade
    atualizarUnidade(input: atualizarUnidadeInput!): Unidade
    excluirUnidade(input: excluirUnidadeInput!): Boolean
  }
  
  #Inputs
  input atualizarUnidadeInput{
    idUnidade: Int!
    codigoUnidade: String
    nomeUnidade: String
    siglaUnidade: String
    hierarquiaUnidade: String
    unidadeGestora: Int
    tipoUnidade: String 
    idCampus: Int
  }
  input excluirUnidadeInput{
    idUnidade: Int!
  }
  input criarUnidadeInput{
    idUnidade: Int!
    codigoUnidade: String!
    nomeUnidade: String!
    siglaUnidade: String!
    hierarquiaUnidade: String!
    unidadeGestora: Int!
    tipoUnidade: String !
    idCampus: Int!
  }

  type Unidade {
    idUnidade: Int!
    codigoUnidade: String!
    nomeUnidade: String!
    siglaUnidade: String!
    hierarquiaUnidade: String!
    unidadeGestora: Int!
    tipoUnidade: String!
    idCampus: Int!
  }
`;

const unidadeResolver = {
  Query: {
    unidade: (obj, input, context, info) => { 
      return Unidades.getUnidade(input) 
    },
    unidades: (obj, args, context, info) => { 
      return Unidades.getUnidades()
    }
  },
  Mutation:{
    criarUnidade: (obj,{input},context, info) => {
      return Unidades.criarUnidade(input)
    },
    atualizarUnidade: (obj, {input}, context, info) => {
      return Unidades.atualizaUnidade(input.idUnidade,input)
    },
    excluirUnidade: (obj, {input}, context, info) => {
      return Unidades.excluiUnidade(input)
    }
  },
  // Unidade: {
  //   idCampus: ({ idUnidade }) => {
  //      return Unidades.getUnidade({ idCampus })
  //   }
  // }
};

module.exports = {
  unidadeTypeDef,
  unidadeResolver
};
