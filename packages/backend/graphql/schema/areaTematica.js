const { gql } = require('apollo-server');

const AreasTematicas = require('../../objectionDatabase/queries/areasTematicas');
const Projetos = require('../../objectionDatabase/queries/projetos');

const areaTematicaTypeDef = gql`
  extend type Query {
    areaTematica(idAreaTematica: Int!): AreaTematica
    areasTematicas: [AreaTematica!]
  }
  extend type Mutation {
    criarAreaTematica(input: criarAreaTematica!): AreaTematica
    atualizarAreaTematica(input: atualizarAreaTematica!): AreaTematica
    excluirAreaTematica(idAreaTematica: Int!): Boolean
  }

  input criarAreaTematica {
    idAreaTematica: Int!
    nomeAreaTematica: String!
  }

  input atualizarAreaTematica {
    idAreaTematica: Int!
    nomeAreaTematica: String!
  }

  type AreaTematica {
    idAreaTematica: Int!
    nomeAreaTematica: String!
    projetos(idEvento: Int): [Projeto!]
  }
`;
const areaTematicaResolver = {
  Query: {
    areaTematica: (obj, args, context, info) => {
      return AreasTematicas.getAreaTematica(args);
    },
    areasTematicas: (obj, args, context, info) => {
      return AreasTematicas.getAreasTematicas();
    }
  },
  Mutation: {
    criarAreaTematica: (obj, { input }, context, info) => {
      return AreasTematicas.postAreaTematica(input);
    },
    atualizarAreaTematica: async (obj, { input }, context, info) => {
      const { idAreaTematica } = input;
      return AreasTematicas.putAreaTematica({ idAreaTematica }, input);
    },
    excluirAreaTematica: async (obj, args, context, info) => {
      const excluido = (await AreasTematicas.delAreaTematica(args)) == 1;
      return excluido;
    }
  },
  AreaTematica: {
    projetos: ({ idAreaTematica }, { idEvento }) => {
      data = idEvento ? { idAreaTematica, idEvento } : { idAreaTematica }
      return Projetos.getProjetos(data);
    }
  }
};

module.exports = {
  areaTematicaTypeDef,
  areaTematicaResolver
};
