const DataLoader = require("dataloader")
const Eventos = require('../../objectionDatabase/queries/eventos')

async function eventosBatch (ids) {
    const eventos = await Eventos.getEventoByid(ids)
    const EventosMap = {}
    eventos.map(obj =>{ EventosMap[obj.idEvento] = obj}) 
    
    return ids.map(obj =>EventosMap[obj])
}

const buildEventosLoader = () => {
    return new DataLoader(eventosBatch)
}

module.exports = buildEventosLoader;