const DataLoader = require("dataloader")
const Unidades = require('../../objectionDatabase/queries/unidades')
// const model_AreasTematicas = require('../../objectionDatabase/models/model_areasTematicas')

async function unidadesBatch (ids) {
    // console.log(ids)
    const unidades = await Unidades.getUnidadeById(ids)
    const unidadesMap = {}
    unidades.map(obj =>{ unidadesMap[obj.idUnidade] = obj}) 
    
    return ids.map(obj =>unidadesMap[obj])
    // const areasTematicas = AreasTematicas.getAreasTematicasById(ids)
    // return areasTematicas.map((projetos) => projetos.areasTematicas);
}

const buildUnidadesLoader = () => {
    return new DataLoader(unidadesBatch)
}
module.exports = buildUnidadesLoader;