const DataLoader = require("dataloader")
const AreasTematicas = require('../../objectionDatabase/queries/areasTematicas')
// const model_AreasTematicas = require('../../objectionDatabase/models/model_areasTematicas')

async function areasTematicasBatch (ids) {
    const areasT = await AreasTematicas.getAreasTematicasById(ids)
    const AreasTematicasMap = {}
    areasT.map(obj =>{ AreasTematicasMap[obj.idAreaTematica] = obj}) 
    
    return ids.map(obj =>AreasTematicasMap[obj])
}

const buildAreasTematicasLoader = () => {
    return new DataLoader(areasTematicasBatch)
}

module.exports = buildAreasTematicasLoader;