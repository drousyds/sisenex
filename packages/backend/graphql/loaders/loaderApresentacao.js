const DataLoader = require("dataloader");
const Apresentacoes = require("../../objectionDatabase/queries/apresentacoes");

const buildApresentacoesLoader = () => {
  return new DataLoader(apresentacoesBatch, {
    cacheKeyFn: id => `${id.idApresentacao}-${id.id2Apresentacao}`
  });
};

async function apresentacoesBatch(ids) {
  const apresentacoes = await Apresentacoes.getApresentacoesById(
    ids.map(elem => [elem.idApresentacao, elem.id2Apresentacao])
  );
  // console.log(apresentacoes);
  
  const apresentacoesMap = {};
  apresentacoes.map(obj => {
    // {obj.idApresentacao,obj.id2Apresentacao}
    apresentacoesMap[`${obj.idApresentacao}-${obj.id2Apresentacao}`] = obj;
  });

  return ids.map(obj => apresentacoesMap[`${obj.idApresentacao}-${obj.id2Apresentacao}`]);
}

module.exports = buildApresentacoesLoader;
