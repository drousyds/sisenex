const defaultConfig = {
  client: "pg",
  connection: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB
  },
  migrations: {
    directory: __dirname + "/knexDatabase/migrations"
  },
  seeds: {
    directory: __dirname + "/knexDatabase/seeds"
  }
};

module.exports = {
  development: defaultConfig,
  staging: defaultConfig,
  local: defaultConfig,
  production: { ...defaultConfig, seeds: { directory: __dirname + "/knexDatabase/prod/seeds" } }
};
