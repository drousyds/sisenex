var mysql = require('mysql')
var msgError = require('./errosSQL')

function sqlTransactionLiberaProjeto(req, res) {
    //operador ternario!!!
    //se eu tiver passado uma funcao que ajeita os resultados da busca como parametro da funcao, eu atribuo,
    //do contrario eu apenas retorno os resultados brutos
    var funcaoDeTratamento =
        arguments.length > 3 && arguments[3] !== undefined
            ? arguments[3]
            : function (results) {
                return { resultados: results };
            };



    const connection = mysql.createConnection({
        host: "localhost",
        port: "3306",
        user: "root",
        //HULU NUT BESTBUY DRIP EGG walmart 7 2 
        // password: 'lum0l4b1',
        password: "root",
        // password: 'cin>>',
        database: "Sisenex"
    });


    var data = [req.body.disponibilidadeProjeto, req.params.idProjeto];

    connection.beginTransaction(function (err) {
        if (err) { console.log(err);
         }
        connection.query("UPDATE Projetos SET dataApresentacaoProjeto = NOW(), disponibilidadeProjeto = ? WHERE idProjeto = ?"
            , data, function (error, results) {
                if (error) {
                    return connection.rollback(function () {
                        var err = msgError(error);
                        res.status(417).send({"titulo": "error", status: error.errno, message: err });
                    });
                }

                var log = 'Updated ' + results.affectedRows + ' rows';
                console.log(log);

                connection.query("UPDATE Pessoas_Projetos SET aptidaoPessoa = 1 WHERE idProjeto = ?",
                    req.params.idProjeto, function (error, results) {
                        if (error) {
                            return connection.rollback(function () {
                                var err = msgError(error);
                                res.status(417).send({ "titulo": "error", status: error.errno, message: err });
                            });
                        }

                        connection.commit(function (err) {
                            if (err) {
                                return connection.rollback(function () {
                                    var err = msgError(error);
                                    res.status(417).send({"titulo": "error", status: error.errno, message: err });
                                });
                            }
                            console.log('success!');
                            res.json({"titulo": `Operação modificou ${results.affectedRows} colunas`});
                            connection.end();
                        });


                    });
            });
    });
    
}

module.exports = sqlTransactionLiberaProjeto;