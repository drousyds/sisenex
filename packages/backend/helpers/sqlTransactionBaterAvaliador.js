var mysql = require('mysql')
var msgError = require('./errosSQL')

function sqlTransactionBaterAvaliador(req, res) {
    //operador ternario!!!
    //se eu tiver passado uma funcao que ajeita os resultados da busca como parametro da funcao, eu atribuo,
    //do contrario eu apenas retorno os resultados brutos
    var funcaoDeTratamento =
        arguments.length > 3 && arguments[3] !== undefined
            ? arguments[3]
            : function (results) {
                return { resultados: results };
            };



    const connection = mysql.createConnection({
        host: "localhost",
        port: "3306",
        user: "root",
        // password: 'lum0l4b1',
        password: "root",
        // password: 'cin>>',
        database: "Sisenex"
    });

    var data = [
        req.params.idPessoa,
        req.params.idVinculoPessoa,
        req.params.id1Apresentacao,
        req.params.id2Apresentacao
    ];

    connection.beginTransaction(function (err) {
        if (err) { console.log(err);
         }
        connection.query("INSERT INTO Pessoas_Apresentacoes (idPessoa, idVinculoPessoa, idApresentacao, id2Apresentacao, aptidaoPessoa) " +
            "VALUES (?,?,?,?,1) "
            , data, function (error, results) {
                if (error) {
                    return connection.rollback(function () {
                        var err = msgError(error);
                        res.status(417).send({ title: "error", status: error.errno, message: err });
                    });
                }

                var log = 'Post ' + results.insertId + ' added';
                console.log(log);


                var data2 = [req.params.id1Apresentacao, req.params.id2Apresentacao];
                connection.query('SELECT Projetos.idProjeto FROM Projetos WHERE Projetos.idApresentacao = ? AND Projetos.id2Apresentacao= ?',
                    data2, function (error, results) {
                        if (error) {
                            return connection.rollback(function () {
                                var err = msgError(error);
                                res.status(417).send({ title: "error", status: error.errno, message: err });
                            });
                        }

                        var temp = [];
                        var query =
                            "INSERT INTO Pessoas_Projetos (idProjeto, idPessoa, idVinculoPessoa, idApresentacao, id2Apresentacao, aptidaoPessoa) " +
                            "VALUES ";
                        results.map(x => {
                            query += `(${x.idProjeto}, ${req.params.idPessoa}, ${
                                req.params.idVinculoPessoa
                                }, ${req.params.id1Apresentacao}, ${
                                req.params.id2Apresentacao
                                }, ${0}), `;
                        });
                        query = query.slice(0, -2);

                        connection.query(query, temp, function (error, results) {
                            if (error) {
                                return connection.rollback(function () {
                                    var err = msgError(error);
                                    res.status(417).send({ title: "error", status: error.errno, message: err });
                                });
                            }

                            log = 'Post ' + results.insertId + ' added';
                            console.log(log);

                            connection.commit(function (err) {
                                if (err) {
                                    return connection.rollback(function () {
                                        var err = msgError(error);
                                        res.status(417).send({ title: "error", status: error.errno, message: err });
                                    });
                                }
                                console.log('success!');
                                var formatedResults = funcaoDeTratamento(results);
                                res.status(200).json(formatedResults);
                                connection.end();
                            });
                        });


                    });
            });
    });























    // connection.query(query, data, function (error, results) {
    //     if (error) {
    //         // res.status = 
    //         var err = msgError(error);
    //         res.send({ title: "error", status: error.errno, message: err });
    //         // res.send(error)
    //     }
    //     else if (results.length == 0) res.status(204).json({ resultados: [] });
    //     else {
    //         if (results.affectedRows)
    //             res.json({ "titulo": `Operação modificou ${results.affectedRows} colunas` });

    //         else {
    //             var formatedResults = funcaoDeTratamento(results);
    //             res.status(200).json(formatedResults);

    //         }
    //     }
    //     connection.end();
    //     console.log("executou!");
    // });
}

module.exports = sqlTransactionBaterAvaliador;