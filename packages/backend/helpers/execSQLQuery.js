var mysql = require('mysql');
var msgError = require('./errosSQL');

function execSQLQuery(query, data, res) {
  //operador ternario!!!
  //se eu tiver passado uma funcao que ajeita os resultados da busca como parametro da funcao, eu atribuo,
  //do contrario eu apenas retorno os resultados brutos
  var funcaoDeTratamento =
    arguments.length > 3 && arguments[3] !== undefined
      ? arguments[3]
      : function(results) {
          return {resultados: results};
        };

  //   var results = {
  //     resultados: [{ id: "id1" }, { id: "id2" }]
  //   };

  const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    port: '3306',
    user: 'root',
    password: process.env.MYSQL_ROOT_PASSWORD,
    database: process.env.MYSQL_DATABASE,
  });

  connection.query(query, data, function(error, results) {
    console.log('Querying...');

    if (error) {
      // res.status =
      var err = msgError(error);
      console.log(error.message);
      res.status(417).json({title: 'error', status: error.errno, message: err});
      // res.send(error)
    }
    //else if (results.length == 0) res.status(204).json({ resultados: [] });
    else {
      if (results.affectedRows) {
        if (results.affectedRows == 0) {
          res.status(204).json({titulo: `Operação alterou 0 colunas`});
        } else {
          res.status(200).json({
            titulo: 'OK',
            msg: `Operação modificou ${results.affectedRows} colunas`,
          });
        }
      } else {
        var formatedResults = funcaoDeTratamento(results);
        res.status(200).json(formatedResults);
      }
    }
    connection.end();
    console.log('executou!');
  });
}

module.exports = execSQLQuery;
