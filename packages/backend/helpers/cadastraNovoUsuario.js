var mysql = require('mysql')
var msgError = require('./errosSQL')

function cadastraNovoUsuario(req, res) {
    //operador ternario!!!
    //se eu tiver passado uma funcao que ajeita os resultados da busca como parametro da funcao, eu atribuo,
    //do contrario eu apenas retorno os resultados brutos
    var funcaoDeTratamento =
        arguments.length > 3 && arguments[3] !== undefined
            ? arguments[3]
            : function (results) {
                return { resultados: results };
            };


    const connection = mysql.createConnection({
        host: "localhost",
        port: "3306",
        user: "root",
        // password: 'lum0l4b1',
        password: "root",
        // password: 'cin>>',
        database: "Sisenex"
    });

    var data = [
        req.body.idPessoa,
        req.body.idVinculoPessoa,
        req.body.matriculaPessoa,
        req.body.nomePessoa,
        req.body.nomeSocialPessoa,
        req.body.lotacaoPessoa,
        req.body.emailPessoa,
        req.body.telefonePessoa
    ];

    connection.beginTransaction(function (err) {
        if (err) { console.log(err) }
        connection.query("SELECT nomePessoa, papelPessoa from PapelPessoas WHERE nomePessoa like ?", [req.body.nomeSocialPessoa, req.body.nomeSocialPessoa]
            , function (error, results) {
                
                var query = '';
                if (error) {
                    return connection.rollback(function () {
                        var err = msgError(error);
                        res.status(417).send({ "titulo": "error", status: error.errno, message: err });
                    });
                }
                
                if (results.length > 0 && results[0].nomePessoa == req.body.nomeSocialPessoa) {
                    switch (results[0].papelPessoa) {
                        case 1: query =
                        "INSERT INTO Pessoas (idPessoa, idVinculoPessoa, matriculaPessoa, nomePessoa, nomeSocialPessoa, " +
                            "lotacaoPessoa, emailPessoa, telefonePessoa, aptidaoPessoa, avaliadorPessoa, monitorPessoa, gerentePessoa) "+
                            "VALUES (?,?,?,?,?,?,?,?,0,1,0,0)";
                            break;

                        case 2: query =
                            "INSERT INTO Pessoas (idPessoa, idVinculoPessoa, matriculaPessoa, nomePessoa, nomeSocialPessoa, " +
                            "lotacaoPessoa, emailPessoa, telefonePessoa, aptidaoPessoa, avaliadorPessoa, monitorPessoa, gerentePessoa) "+
                            "VALUES (?,?,?,?,?,?,?,?,0,0,1,0)";
                            break;

                        case 3: query =
                            "INSERT INTO Pessoas (idPessoa, idVinculoPessoa, matriculaPessoa, nomePessoa, nomeSocialPessoa, " +
                            "lotacaoPessoa, emailPessoa, telefonePessoa, aptidaoPessoa, avaliadorPessoa, monitorPessoa, gerentePessoa) "+
                            "VALUES (?,?,?,?,?,?,?,?,0,0,0,1)";
                            break;
                    }
                }
                else {
                    query = "INSERT INTO Pessoas (idPessoa, idVinculoPessoa, matriculaPessoa, nomePessoa, nomeSocialPessoa, " +
                    "lotacaoPessoa, emailPessoa, telefonePessoa, aptidaoPessoa) VALUES (?,?,?,?,?,?,?,?,0)";
                    
                }

                var queried = connection.query(query, data, function (error, results) {
                    console.log(queried.sql);
                    if (error) {
                        return connection.rollback(function () {
                            var err = msgError(error);
                            console.log({'numero de erro do mysql':error.errno});
                            
                            if(error.errno == 1062)
                                //RETORNAMOS UM CODIGO QUALQUER!!!!! Ajeitar isso no futuro.
                                res.status(202).json({ "titulo": "error", status: error.errno, message: err })
                            else
                                res.status(417).json({ "titulo": "error", status: error.errno, message: err });
                        });
                    }
                    connection.commit(function (err) {
                        if (err) {
                            return connection.rollback(function () {
                                var err = msgError(error);
                                res.status(417).json({ "titulo": "error", status: error.errno, message: err });
                                
                            });
                        }
                        res.status(200).json({ "titulo": `Operação modificou ${results.affectedRows} colunas` });
                        connection.end();
                    });

                });

            });
    });
}

module.exports = cadastraNovoUsuario;