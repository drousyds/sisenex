function msgError(err) {
    var errono = err.errno;
    switch (errono) {
        case 1045:
            return "Acesso negado !!";
        case 1215:
            return "Falha na associacao de chave extrangeira!!";
        case 1062:
            return "Entrada duplicada, chave do registro ja existente!!";
        case 1065:
            return "Comando executado está vazio!!";
        case 1053:
            return "Server Interrompido durante o progresso!!";
        case 1048:
            return "Coluna especifica do registro nao pode ser NULL!!";
        case 1040:
            return "Excesso de Conexoes a base de dados!!";
        case 1105:
            return "Erro desconhecido na base de dados!!";
        case 1172:
            return "Resultado obtido consite em mais de registro!!";
        case 1184:
            return "Conexao avortada!!";
        case 1216:
            return "Nao eh possivel criar ou alterar chave inexistente de" +
                " uma tabela pai em uma tabela interna!!";
        case 1217:
            return "Nao eh possivel deletar ou alterar chave inexistente de" +
                " uma tabela pai que esta sendo utilizada em tabela interna!!";
        case 1242:
            return "Resultado obtido da subquery consite em mais de registro!!";
        case 1291:
            return "Alguma coluna com valor duplicado!!";
        case 1317:
            return "Execucao do comando interrompido!!";
        case 1348:
            return "Alguma coluna do registro inalteravel!!";
        case 1451:
            return "Nao eh possivel deletar ou alterar chave inexistente de" +
                " uma tabela pai que esta sendo utilizada em tabela interna!!";
        case 1452:
            return "Nao eh possivel criar ou alterar chave inexistente de" +
                " uma tabela pai em uma tabela interna!!";
        case 2000 - 3000:
            return "Problema com a conexao com a base de dados!!";
        case 1586:
            return "Entrada Duplicada, registro ja existente!!";
        case 1054:
            return err;
        case 1292:
            return "Valor truncado ou errado (provavelmente tipo do campo errado)";
        case 1364:
            return "Campos insuficientes passados para criar entidade";
        case 1049:
            return "Banco desconhecido";
        default:
            return err.message;
    }
}
module.exports = msgError;