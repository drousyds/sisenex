const axios = require("axios");
const _ = require("lodash");
var knex = require("../knexinstance");
const fs = require("fs");

const MAX_PAGES = 63;
const PROJS_2022_1 = ['PJ910-2022','PJ883-2022','PJ086-2022','PJ084-2022','PJ915-2022','PJ914-2022','PJ909-2022','PJ886-2022','PJ882-2022','PJ671-2022','PJ077-2022','PJ075-2022','PJ048-2022','PJ047-2022','PJ040-2022','PJ031-2022','PJ018-2022','PJ079-2022','PJ078-2022','PJ064-2022','PJ063-2022','PJ025-2022','PJ024-2022','PJ022-2022','PJ020-2022','PJ017-2022','PG001-2022','PJ918-2022','PJ911-2022','PJ908-2022','PJ884-2022','PJ879-2022','PJ104-2022','PJ085-2022','PJ081-2022','PJ080-2022','PJ069-2022','PJ067-2022','PJ062-2022','PJ061-2022','PJ050-2022','PJ042-2022','PJ039-2022','PJ038-2022','PJ033-2022','PJ029-2022','PJ028-2022','PJ027-2022','PJ021-2022','PJ880-2022','PJ198-2022','PJ197-2022','PJ082-2022','PJ054-2022','PJ877-2022','PJ032-2022','PJ170-2022','PJ124-2022','PJ112-2022','PJ189-2022','PJ188-2022','PJ182-2022','PJ178-2022','PJ169-2022','PJ168-2022','PJ165-2022','PJ158-2022','PJ155-2022','PJ140-2022','PJ135-2022','PJ125-2022','PJ123-2022','PJ119-2022','PJ116-2022','PJ108-2022','PJ171-2022','PJ144-2022','PJ196-2022','PJ193-2022','PJ186-2022','PJ179-2022','PJ177-2022','PJ176-2022','PJ160-2022','PJ151-2022','PJ150-2022','PJ147-2022','PJ141-2022','PJ136-2022','PJ134-2022','PJ115-2022','PJ113-2022','PJ181-2022','PJ166-2022','PJ163-2022','PJ161-2022','PJ148-2022','PJ145-2022','PJ122-2022','PJ194-2022','PJ183-2022','PJ180-2022','PJ174-2022','PJ156-2022','PJ154-2022','PJ139-2022','PJ126-2022','PJ117-2022','PJ190-2022','PJ162-2022','PJ157-2022','PJ152-2022','PJ149-2022','PJ142-2022','PJ001-2023','PJ187-2022','PJ175-2022','PJ121-2022','PJ902-2022','PJ901-2022','PJ900-2022','PJ092-2022','PJ094-2022','PJ090-2022','PJ088-2022','PJ101-2022','PJ097-2022','PJ096-2022','PJ093-2022','PJ014-2022','PJ013-2022','PJ005-2022','PJ004-2022','PJ015-2022','PJ008-2022','PJ006-2022','PJ003-2022','PJ002-2022','PJ051-2022','PJ012-2022','PJ007-2022','PJ001-2022'];
const PROJS_2022_23 = ['PJ878-2022','PJ030-2022','PJ913-2022','PJ035-2022','PJ076-2022','PJ102-2022','PJ071-2022','PJ167-2022','PJ111-2022','PJ153-2022','PJ137-2022','PJ110-2022','PJ164-2022','PJ114-2022','PJ109-2022','PJ191-2022','PJ184-2022','PJ133-2022','PJ132-2022','PJ131-2022','PJ130-2022','PJ129-2022','PJ128-2022','PJ127-2022','PJ192-2022','PJ185-2022','PJ173-2022','PJ120-2022','PJ095-2022','PJ098-2022','PJ073-2022','PJ074-2022','PJ011-2022','PJ009-2022'];
const PROJS_2022_4 = ['PJ199-2022','PJ041-2022','PJ055-2022','PJ037-2022','PJ143-2022','PJ138-2022','PJ118-2022','PJ195-2022','PJ172-2022','PJ159-2022','PJ146-2022','PJ099-2022','PJ091-2022','PJ100-2022','PJ089-2022','PJ010-2022'];
const Total = ['PJ199-2022','PJ041-2022','PJ055-2022','PJ037-2022','PJ143-2022','PJ138-2022','PJ118-2022','PJ195-2022','PJ172-2022','PJ159-2022','PJ146-2022','PJ099-2022','PJ091-2022','PJ100-2022','PJ089-2022','PJ010-2022',
'PJ878-2022','PJ030-2022','PJ913-2022','PJ035-2022','PJ076-2022','PJ102-2022','PJ071-2022','PJ167-2022','PJ111-2022','PJ153-2022','PJ137-2022','PJ110-2022','PJ164-2022','PJ114-2022','PJ109-2022','PJ191-2022','PJ184-2022','PJ133-2022','PJ132-2022','PJ131-2022','PJ130-2022','PJ129-2022','PJ128-2022','PJ127-2022','PJ192-2022','PJ185-2022','PJ173-2022','PJ120-2022','PJ095-2022','PJ098-2022','PJ073-2022','PJ074-2022','PJ011-2022','PJ009-2022','PJ910-2022','PJ883-2022','PJ086-2022','PJ084-2022','PJ915-2022','PJ914-2022','PJ909-2022','PJ886-2022','PJ882-2022','PJ671-2022','PJ077-2022','PJ075-2022','PJ048-2022','PJ047-2022','PJ040-2022','PJ031-2022','PJ018-2022','PJ079-2022','PJ078-2022','PJ064-2022','PJ063-2022','PJ025-2022','PJ024-2022','PJ022-2022','PJ020-2022','PJ017-2022','PG001-2022','PJ918-2022','PJ911-2022','PJ908-2022','PJ884-2022','PJ879-2022','PJ104-2022','PJ085-2022','PJ081-2022','PJ080-2022','PJ069-2022','PJ067-2022','PJ062-2022','PJ061-2022','PJ050-2022','PJ042-2022','PJ039-2022','PJ038-2022','PJ033-2022','PJ029-2022','PJ028-2022','PJ027-2022','PJ021-2022','PJ880-2022','PJ198-2022','PJ197-2022','PJ082-2022','PJ054-2022','PJ877-2022','PJ032-2022','PJ170-2022','PJ124-2022','PJ112-2022','PJ189-2022','PJ188-2022','PJ182-2022','PJ178-2022','PJ169-2022','PJ168-2022','PJ165-2022','PJ158-2022','PJ155-2022','PJ140-2022','PJ135-2022','PJ125-2022','PJ123-2022','PJ119-2022','PJ116-2022','PJ108-2022','PJ171-2022','PJ144-2022','PJ196-2022','PJ193-2022','PJ186-2022','PJ179-2022','PJ177-2022','PJ176-2022','PJ160-2022','PJ151-2022','PJ150-2022','PJ147-2022','PJ141-2022','PJ136-2022','PJ134-2022','PJ115-2022','PJ113-2022','PJ181-2022','PJ166-2022','PJ163-2022','PJ161-2022','PJ148-2022','PJ145-2022','PJ122-2022','PJ194-2022','PJ183-2022','PJ180-2022','PJ174-2022','PJ156-2022','PJ154-2022','PJ139-2022','PJ126-2022','PJ117-2022','PJ190-2022','PJ162-2022','PJ157-2022','PJ152-2022','PJ149-2022','PJ142-2022','PJ001-2023','PJ187-2022','PJ175-2022','PJ121-2022','PJ902-2022','PJ901-2022','PJ900-2022','PJ092-2022','PJ094-2022','PJ090-2022','PJ088-2022','PJ101-2022','PJ097-2022','PJ096-2022','PJ093-2022','PJ014-2022','PJ013-2022','PJ005-2022','PJ004-2022','PJ015-2022','PJ008-2022','PJ006-2022','PJ003-2022','PJ002-2022','PJ051-2022','PJ012-2022','PJ007-2022','PJ001-2022'];

var PROJS_IN = [];
exports.updateProjects = async () => {
    let promises = [];
    console.log(`${PROJS_2022_1.length + PROJS_2022_23.length + PROJS_2022_4.length} NUMBER OF projects 2022`);
    console.log(PROJS_2022_1.includes('PJ024-2022'))
    for (let i = 0; i <= MAX_PAGES; i++) {
        let url = `https://api.ufpb.br/api/projetos/extensao/todos-projetos?step=${i}`;
        promises = [...promises, axios.get(url).then(({ data }) => data)];
    }

    return await Promise.all(promises)
        .then(async (projetos) => {
            const mergedProjects = [].concat(...projetos); //Flattens all projects separated by pages
            console.log(`${mergedProjects.length} total projects`);

            let validProjects = [];
            for (project of mergedProjects) {

                    project.codigo = addPadding(project.codigo)
                    if (PROJS_2022_1.includes(project.codigo)) {
                        PROJS_IN.push(project.codigo);
                        validProjects.push({
                            idCategoria: 3,
                            idEvento: 1,
                            dataInicioProjeto: '01-01-2022 00:00:00',
                            dataFimProjeto: '12-31-2022 23:59:59',
                            ...project,
                        });
                    }
                    else if (PROJS_2022_23.includes(project.codigo)) {
                        PROJS_IN.push(project.codigo);
                        validProjects.push({
                            idCategoria: 3,
                            idEvento: 2,
                            dataInicioProjeto: '01-01-2022 00:00:00',
                            dataFimProjeto: '12-31-2022 23:59:59',
                            ...project,
                        });
                    }
                    else if (PROJS_2022_4.includes(project.codigo)) {
                        PROJS_IN.push(project.codigo);
                        validProjects.push({
                            idCategoria: 3,
                            idEvento: 3,
                            dataInicioProjeto: '01-01-2022 00:00:00',
                            dataFimProjeto: '12-31-2022 23:59:59',
                            ...project,
                        });
                    }
        }
            console.log(`${validProjects.length} projects this year`);

            let membros = [];
            const formattedProjects = validProjects.map((el) => {
                const {
                    idProjeto,
                    titulo: tituloProjeto,
                    codigo: codigoProjeto,
                    descricao: descricaoProjeto,
                    // dataInicio: dataInicioProjeto,
                    // dataFim: dataFimProjeto,
                    ano: anoProjeto,
                    idAreaTematica,
                    idUnidade,
                    idCategoria,
                    idEvento,
                    dataInicioProjeto,
                    dataFimProjeto,
                } = _.pick(el, [
                    "idProjeto",
                    "codigo",
                    "titulo",
                    "descricao",
                    // "dataInicio",
                    // "dataFim",
                    "ano",
                    "idAreaTematica",
                    "idUnidade",
                    "idCategoria",
                    "idEvento",
                    "dataInicioProjeto",
                    "dataFimProjeto",
                ]);

                const membrosAssociados = el.membros.filter(
                    ({ funcao }) => funcao === "COORDENADOR(A)"
                );
                const formattedMembros = membrosAssociados.map(
                    ({ idPessoa, idVinculo, funcao, nome }) => ({
                        idPessoa,
                        idVinculoPessoa: idVinculo,
                        idProjeto,
                        funcaoMembro: funcao,
                        nomePessoa: nome,
                    })
                );

                membros.push(formattedMembros);

                return {
                    idProjeto,
                    codigoProjeto,
                    tituloProjeto,
                    descricaoProjeto,
                    dataInicioProjeto,
                    dataFimProjeto,
                    anoProjeto: `${anoProjeto}-01-01`,
                    idAreaTematica:
                        idAreaTematica > 3
                            ? idAreaTematica - 1
                            : idAreaTematica,
                    idUnidade,
                    idEvento,
                    idCategoria,
                };
            });
            
            for (let i = 0; i < Total.length; i++) {
                if (!PROJS_IN.includes(Total[i]))
                    console.log(Total[i])
            }
            // flattening membros array
            let flattenedMembros = [];
            flattenedMembros = flat(membros);

            // removing duplicates
            const uniqueProjects = _.uniqBy(formattedProjects, "idProjeto");
            const uniqueMembros = _.uniqWith(flattenedMembros, _.isEqual);

            console.log(uniqueProjects.length);
            // inserting
            const projectsInserted = await knex
                .raw(
                    `${knex("Projetos")
                        .insert(uniqueProjects)
                        .toQuery()} ON CONFLICT ("idProjeto") DO UPDATE SET "idUnidade" = excluded."idUnidade", "codigoProjeto" = excluded."codigoProjeto"`
                )
                .then(({ rowCount }) => {
                    console.log(`${rowCount} projetos upserted`);
                    return true;
                })
                .catch((error) => console.log(error));

            const membersInserted = await knex
                .raw(
                    `${knex("Membros_Extensao")
                        .insert(uniqueMembros)
                        .toQuery()} ON CONFLICT ("idProjeto", "idPessoa", "idVinculoPessoa") DO UPDATE SET "nomePessoa" = excluded."nomePessoa"`
                )
                .then(({ rowCount }) => {
                    console.log(`${rowCount} membros upserted`);
                    return true;
                })
                .catch((error) => console.log(error));

            return projectsInserted && membersInserted;
        })
        .catch((error) => console.log(error));
};

const addPadding = (code) => {
    const tipo = code.split("-")[0].slice(0, 2);
    const numero = code.split("-")[0].slice(2).padStart(3, "0");
    const ano = code.split("-")[1];
    return `${tipo}${numero}-${ano}`;
};

const flat = (input, depth = 1, stack = []) => {
    for (let item of input) {
        if (item instanceof Array && depth > 0) {
            flat(item, depth - 1, stack);
        } else {
            stack.push(item);
        }
    }

    return stack;
};
