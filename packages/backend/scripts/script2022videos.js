const axios = require("axios");
const _ = require("lodash");
var knex = require("../knexinstance");
const fs = require("fs");

const MAX_PAGES = 63;

const PROJS_2022_VIDEOS = ['PJ653-2022','PJ261-2022','PJ822-2022','PJ682-2022','PJ457-2022','PJ367-2022','PJ224-2022','PJ856-2022','PJ742-2022','PJ698-2022','PJ648-2022','PJ279-2022','PJ527-2022','PJ552-2022','PJ591-2022','PJ463-2022','PJ551-2022','PJ264-2022','PJ604-2022','PJ850-2022','PJ540-2022','PJ695-2022','PJ802-2022','PJ829-2022','PJ642-2022','PJ265-2022','PJ869-2022','PJ816-2022','PJ378-2022','PJ380-2022','PJ619-2022','PJ554-2022','PJ445-2022','PJ364-2022','PJ223-2022','PJ577-2022','PJ618-2022','PJ651-2022','PJ633-2022','PJ611-2022','PJ216-2022','PJ497-2022','PJ390-2022','PJ468-2022','PJ246-2022','PJ218-2022','PJ662-2022','PJ725-2022','PJ340-2022','PJ470-2022','PJ610-2022','PJ315-2022','PJ563-2022','PJ771-2022','PJ837-2022','PJ366-2022','PJ896-2022','PJ679-2022','PJ338-2022','PJ428-2022','PJ713-2022','PJ776-2022','PJ509-2022','PJ825-2022','PJ525-2022','PJ292-2022','PJ770-2022','PJ598-2022','PJ872-2022','PJ371-2022','PJ831-2022','PJ300-2022','PJ217-2022','PJ373-2022','PJ669-2022','PJ580-2022','PJ282-2022','PJ351-2022','PJ875-2022','PJ739-2022','PJ461-2022','PJ216-2022','PJ601-2022','PJ296-2022','PJ254-2022','PJ263-2022','PJ789-2022','PJ562-2022','PJ860-2022','PJ697-2022','PJ203-2022','PJ215-2022','PJ295-2022','PJ575-2022','PJ887-2022','PJ859-2022','PJ887-2022','PJ546-2022','PJ221-2022','PJ412-2022','PJ635-2022','PJ372-2022','PJ409-2022','PJ321-2022','PJ284-2022','PJ393-2022','PJ375-2022','PJ830-2022','PJ600-2022','PJ352-2022','PJ270-2022','PJ617-2022','PJ643-2022','PJ335-2022','PJ743-2022','PJ394-2022','PJ228-2022','PJ389-2022','PJ281-2022','PJ490-2022','PJ348-2022','PJ533-2022','PJ772-2022','PJ572-2022','PJ535-2022','PJ511-2022','PJ418-2022','PJ731-2022','PJ447-2022','PJ516-2022','PJ622-2022','PJ405-2022','PJ841-2022','PJ322-2022','PJ467-2022','PJ752-2022','PJ773-2022','PJ843-2022','PJ688-2022','PJ747-2022','PJ327-2022','PJ320-2022','PJ206-2022','PJ549-2022','PJ311-2022','PJ783-2022','PJ630-2022','PJ665-2022','PJ415-2022','PJ727-2022','PJ703-2022','PJ795-2022','PJ683-2022','PJ481-2022','PJ755-2022','PJ803-2022']


const links =   ['https://drive.google.com/file/d/1ZkTejSDbUojzCOC1OB4GHboHBbpkekMl/','https://drive.google.com/file/d/1ZKti62Pt2Fj3moPadRQEH7BMuZMx6twB/','https://drive.google.com/file/d/1etWhd0eOAdeCEdRchvpsaTYvulwjgGN2/','https://drive.google.com/file/d/17pyODKWrxkUFKxH4Ad0bZcuYJmANrD6T/','https://drive.google.com/file/d/1gf249llJfmzBYmXezXGt6tTToNr3GQjI/','https://drive.google.com/file/d/1wKkjBzqK0p0WpOOgbowrvy0fym3US47q/','https://drive.google.com/file/d/14xwzrOXpL7d6ctPzIoc2m0X3_mBL6zY-/','https://drive.google.com/file/d/1AcD2d_7C5mNghwupaOZOvRcbBccL-cYw/','https://drive.google.com/file/d/1q4mqRmlT9NrHk6IxptpzOLM4jYILSrJN/','https://drive.google.com/file/d/1UWcQd2qVwvznzGDqPPOLkTc0FcAX89Ia/','https://drive.google.com/file/d/1wyLOzA7_phnWYxx6DijJYfGV4wLncLnt/','https://drive.google.com/file/d/18I4Tx723_Y3y67zBIQe9aL89qI5TCWtM/','https://drive.google.com/file/d/1ikuweyoAA9whDG2FTo_saPn3KJUeklDH/','https://drive.google.com/file/d/10ZDp8czis_ZE0kMTRHYshqKvI9j5Cl5c/','https://drive.google.com/file/d/1LHHc6wWdP5oQdMyVoK4F1_UYAGa3G8JL/','https://drive.google.com/file/d/1uwOm1wjJEHx2VUnsbs6bjzjM7vNguVSm/','https://drive.google.com/file/d/1_rFewMNTHsRTZN_X_HiNVRjqOY0XZocw/','https://drive.google.com/file/d/1vbAAPDj-77Sb2ME-U9Qx9PIBr6OpmFh-/','https://drive.google.com/file/d/1g_su5z_SYNN2wxpYmr3duvN0mw2hZxG9/','https://drive.google.com/file/d/1rZ0eV58c999C4ktxbGdYCJTVeaCbIx5H/','https://drive.google.com/file/d/1xKn2cRBmvxiFqGLg_-3h3BGSo4r3aoXP/','https://drive.google.com/file/d/1aXmgY-kVwuHQXkWHv-iW50wwV99KQIZk/','https://drive.google.com/file/d/1kWJ86Wqk-tQ_-WUCGto66Df6COmxaUnS/','https://drive.google.com/file/d/1kIgyqf1PvostKtt-YZZg7jhh0Jf8UYrw/',
'https://drive.google.com/file/d/1MROTIkRAg1OvPb0ddYSOJBXj_zH2XIjK/','https://drive.google.com/file/d/1ZubaVxzdpU7TU6rFS58k3CJ4MqCFCGnP/','https://drive.google.com/file/d/1Fp5CeB9OEoKfbexUrNAMo1vIqTHC_Qxm/','https://drive.google.com/file/d/1CyKSPuJlytzKBphAnla3AJCEdDopa72q/','https://drive.google.com/file/d/1QSVGwd6C1LNtaC4k85nNU3xL0o8RMmvb/','https://drive.google.com/file/d/1B-kydpOVaUPITkE-VQ1OyIbfZVJXxWvQ/','https://drive.google.com/file/d/1QdcuT_6NbFPyA3hokEGWhtHSZAVlFsPB/','https://drive.google.com/file/d/17DzEoeeIgVV_OXV5_znea5I4Zt3EW6-4/','https://drive.google.com/file/d/1S5Fo83cymEq3YtHsP4rotU4bzt7K50Qv/','https://drive.google.com/file/d/1v1oiPQdY14raFmRXIU91PjPXUxrxp0Ry/','https://drive.google.com/file/d/1Gc8dcApFDpUZAGVow9gM9Mxs5rXOk1OT/','https://drive.google.com/file/d/14PWNSWWs7ynhJUiwzEzXXu5C1cRb-8Jo/','https://drive.google.com/file/d/1X3KaYu6PBe_3D9Kq0cXo0uBIBT6Ql0Ki/','https://drive.google.com/file/d/1Ox3sPPLVDDYCsy_FOAXrp3VLs8Vxhxec/','https://drive.google.com/file/d/1cOF3shVZ39cqK0ap37YbUfzEVnhzBVBy/','https://drive.google.com/file/d/11zypZ4WVPC6TgBcpcAdsSOgT0LN8N_yv/','https://drive.google.com/file/d/1cu1mOdwiDy4Qs7So15Gq_DZ0KXF-zuxO/','https://drive.google.com/file/d/1GuoLLh5iZC1dlEkcdsFT6GKmfRlLHM74/','https://drive.google.com/file/d/1mycEh09QSdzzNtQSsjS0-hNsO4HExVtY/','https://drive.google.com/file/d/1Q26tzbHlWehhP5u40EqgK9xCGelvoYy1/','https://drive.google.com/file/d/1vHFTlU0hXa8NI3LCWa_vu2eh2O0NbmTK/','https://drive.google.com/file/d/18rzxJlnJ7oll2gDL6KJ2gmuYAyFEYxD0/','https://drive.google.com/file/d/1t4AWKK-LPjKhqEToPtzkvUnorgPAJGpz/','https://drive.google.com/file/d/1oFQzwMt4b2V_kHSJR71N390HeNy8wqyc/',
'https://drive.google.com/file/d/17K8BGlzBL5M_eI5e-7CsfZXtqagy2ToW/','https://drive.google.com/file/d/13acRNRHkELOhU0scgNkKAdoP0N9Eo8WN/','https://drive.google.com/file/d/1pPILzZboi0JGcUpDrGPJ9o6NObZpcQQT/','https://drive.google.com/file/d/1CzNYkPRJ-3l9Q2uzdAefEujuhGG5ovZG/','https://drive.google.com/file/d/1oSZW-1E7GhbjzhXoJ9CMIq4y6O4-wMBL/','https://drive.google.com/file/d/10QPD0JaVVslL9iy2nnf6tq_Misg-Kk5I/','https://drive.google.com/file/d/15mPBV1lWNxWsg5SchPgP4j63gp7vwCtJ/','https://drive.google.com/file/d/1T0egYbOEon7HuQCOtSqDl30vh4uhQ1Ko/','https://drive.google.com/file/d/1owqe5Yhxdro3bDnXlcn5klthl3tcEP06/','https://drive.google.com/file/d/1VC7GvfxUPOgG_RRTo0vdVw2a25PFrvTQ/','https://drive.google.com/file/d/1FFiZTbHIct4Z-4mYpoizygpW1bW53PWT/','https://drive.google.com/file/d/10GZthuYb7FVj99RbyEiWZbrM8j9nRpev/','https://drive.google.com/file/d/1MsTJIkUmFpSzqTW4Zr0gnN8DEjakAZTO/','https://drive.google.com/file/d/1Jsi5Bcna72DO-PHJieDBuM2670ad2bGq/','https://drive.google.com/file/d/1U-ZML5QNHcJDEDDEbrONXo7Zn8S2pryQ/','https://drive.google.com/file/d/1_r14lkXU-d0F96eYsxg2C-G_agUYRywc/','https://drive.google.com/file/d/1SE3PNU5HvxNcxo7pOQ3PgsiIJI1vGWwA/','https://drive.google.com/file/d/1xT0vuX7mhPyBPdJRVXZKbJt1cxS-qChs/','https://drive.google.com/file/d/1adKpvtIZpoVFh96mjHFMr1gWQTa_QTfb/','https://drive.google.com/file/d/15flIrzC1YOrA6V4AJRVzkP2dsuT4zRJ3/','https://drive.google.com/file/d/1DswnDaec9agCVkVp0K02mrPjIDZnXVAX/','https://drive.google.com/file/d/10wEF_14V7otKRAY3u42pkRIeosvrVzjA/','https://drive.google.com/file/d/1UDABWiXaNyrlOAxdYltHWFvG0JfSteYT/','https://drive.google.com/file/d/1WVoVrSXsl1cwaJP9GP0yHz2tqC4dVFPw/',
'https://drive.google.com/file/d/1Em-3sier9qxLNEfDhGgbPm74sdGGCqPe/','https://drive.google.com/file/d/17ftWP0MZe6yQE9Z3tz7q6U49hBndGUGI/','https://drive.google.com/file/d/1TjN0eIDhLBwmdt7ifOB3bETm7Ytrrbgk/','https://drive.google.com/file/d/1GrDWXfE0DI1Dh3kiD2c0F_piaH60oxqi/','https://drive.google.com/file/d/1SsDK17utumepMcuRfplVYCeKcP3y4WW-/','https://drive.google.com/file/d/1IIw4QzeWIK_EkQtIiRvldbfWAHOla8tt/','https://drive.google.com/file/d/1n0LJzZuk7ezVkJ_B-S7y5yHsF8zZvvud/','https://drive.google.com/file/d/15lcVE5mIEszty73v76JnSMmoJCl4K3Y3/','https://drive.google.com/file/d/1xNpMEYAeyHarZPa9a62uVN2i-ZajUs8q/','https://drive.google.com/file/d/1cu1mOdwiDy4Qs7So15Gq_DZ0KXF-zuxO/','https://drive.google.com/file/d/1SLtsC3VsEQg5wNCyYqfBxT7ZxRoWNoEw/','https://drive.google.com/file/d/1cY8VyGLdoYWCJ1iyjoRPwpjLoXrPHyFK/','https://drive.google.com/file/d/1xTfdopTFXUZJASkbYAiLYB5AjjNFVXvo/','https://drive.google.com/file/d/1XFuCX8zCAnWqwrSmWRnvz5SbzipfIgIo/','https://drive.google.com/file/d/1mQCOJOLjQK-glmpbL_L2_iLrBZhiH43c/','https://drive.google.com/file/d/1JHVUbpe230I9LcsqrPag7q7z80cu2wap/','https://drive.google.com/file/d/1qzGMSeUgU-pucl9DzZnmLjrEmJ3OOy8B/','https://drive.google.com/file/d/1q7wcOcWzNtj0dxkaPOBh-dc8h-186AMr/','https://drive.google.com/file/d/1lix4PCrabvf98ZELed0vbk6wE1yLaJF6/','https://drive.google.com/file/d/1E3CSU5bmyGDVkifJZ-S22m0pUsSp3Hy5/','https://drive.google.com/file/d/1oKmJxrJ6XrQz_b4QsPiTbJPIoNRJNAkD/','https://drive.google.com/file/d/1pMSlU-FPS8TnxEVSRgDKgDxFEzXFrEdl/','https://drive.google.com/file/d/1WE5N3Ufk9KDptIMLKdNbYrZuOcotNFH4/','https://drive.google.com/file/d/1vNI-6E1rGb7PDpY83iFZxuTc0rQxBHzZ/',
'https://drive.google.com/file/d/10T7h_6ACcFWEoobnp0hD1gmiz7fCi19x/','https://drive.google.com/file/d/1OpXmdwrZqdNCt_3gj-fx0iRv-FAiP3kN/','https://drive.google.com/file/d/1z0wAwp40Dbo2vyaC-c7g84UB8sT6Ft8c/','https://drive.google.com/file/d/1PNyZ4PUh5jxQBhC8nbg8z36fBgGKrjqk/','https://drive.google.com/file/d/1QpSc3g4YJ2LzrRyvJBAS2rGF4nWQrRI4/','https://drive.google.com/file/d/1aiMH6ZzOOr3Qc2BPV5LMXNbRqpQnptkn/','https://drive.google.com/file/d/1bTzRql8MfCDtqlXxBdtZ6yESky0Xttkg/','https://drive.google.com/file/d/14v2U2kJu4VphSPXpd4aTEVwVlO5eLCHL/','https://drive.google.com/file/d/1DgGDsjO41KTfmre0MAdVZ3b6jr7c_ekr/','https://drive.google.com/file/d/19a6gQcxH_pDo1LWzJnq3q7IHMKw8O65P/','https://drive.google.com/file/d/1UTPJXnCmQqXC5TsrMNf25jvwDkqJh4o6/','https://drive.google.com/file/d/1aNQG5qqx9kouhXuFtqOzc6onZ7SMMQrL/','https://drive.google.com/file/d/1V8bpw5p_usMVDvasRB02REB4rd2m71I3/','https://drive.google.com/file/d/1xdyFxEB34So8xXkXtOAVzgBMQKXsqhAz/','https://drive.google.com/file/d/1rjhKAnsE7gbafxnx5mclQrZV8KSIsypA/','https://drive.google.com/file/d/1DsPDIcQsCImade0aeYURh5SD57rcMp_x/','https://drive.google.com/file/d/132O689PKaWNgCvqgswMhj0EQNIh7gpQC/','https://drive.google.com/file/d/1k2rBh1kNMY4tsqFEDqKTNDxqVNuo-wvJ/','https://drive.google.com/file/d/1Vl6g9fW5reGnl80MEP-k7K35xWj-wLhV/','https://drive.google.com/file/d/1FcYn6-1qjzncwLxpkLX2G9gLAAfhpDib/','https://drive.google.com/file/d/1h0UWqMcGqkvvdoYlkNC_F1__utjkVueY/','https://drive.google.com/file/d/107f-mD9ZFAZrk2y0eehnS4WFEz7N5JLj/','https://drive.google.com/file/d/1UtIywmiyyyk-176x03pm23dRoKxcmaG7/','https://drive.google.com/file/d/1yVvs1nI0upGILxE1cARzA2xWQ9LHX_Ny/',
'https://drive.google.com/file/d/1Drs-zwU3WpvqLZ4TIRepRfRAuUByDvtM/','https://drive.google.com/file/d/1OEbx1MxS0ijLh8MWlag5Jwf6zvM_XYka/','https://drive.google.com/file/d/1ch_IZUnNfEQmkhmA99dfynPRuwIYlv06/','https://drive.google.com/file/d/1YA5MaX7t3jTpv0s6AXrP6vJ5up7TXklP/','https://drive.google.com/file/d/1L8I8sIx9brI9w2vCwVKTvdd3LZ16WzOn/','https://drive.google.com/file/d/1QA6Iuns00J-ASPtOL1OpNGbVqIXywSzq/','https://drive.google.com/file/d/1749ldMvWPWb5y67qE-BdROFq3bkx1HSZ/','https://drive.google.com/file/d/1EoDm50Anlc64FGRMsIxk39s0bMQQA4FQ/','https://drive.google.com/file/d/1zFnsFOVQrooKY5abP_761wJGwhMNccty/','https://drive.google.com/file/d/1-8Q8r4b4uD-Ts0kG3kEpJa0y9Zqtkr1H/','https://drive.google.com/file/d/1_cQqata-NHtEyNPD0gzSFUr7_jKKbI5I/','https://drive.google.com/file/d/1WhM-PVc-z7qXR-I_F-AEvMdGCyTALK5_/','https://drive.google.com/file/d/1VzkAV9gvYVkcGNgj3T5In1Pfoh2docRZ/','https://drive.google.com/file/d/1DxNTCyBh7FdLgrbj-hJ-2Maxj4xHe9c6/','https://drive.google.com/file/d/1y-7d4hdUCnm7sDv-egJL9A8Vlei16nI7/','https://drive.google.com/file/d/1MKAS8h0Gfiu_1d-AlugTzJ116Qqq6-Tr/','https://drive.google.com/file/d/1OdK3AmKQMJ3SLpfRrAiXySaJAQX5Cgi-/','https://drive.google.com/file/d/1IOumxTe6_UgZQF7wQsjcuBap7CKpOdBj/','https://drive.google.com/file/d/1rjDwTKJcHVf85ksl7g5NY0hfWU4wofKr/','https://drive.google.com/file/d/1Z7fkLOukI_xba_ZUseyy-1UwSihwUnLG/','https://drive.google.com/file/d/1qBgXDI0UOSk-dJrgzhrEL8u4B1EUDF0y/','https://drive.google.com/file/d/1jiuKtQAcEr2m_bOopdn6PJgMHiJs5nq4/','https://drive.google.com/file/d/1D2xoXew3NXXBA4B-Dwfzc4OKRWZo55Ni/','https://drive.google.com/file/d/1HNnjc5S459Zk42gl8SkmcG8z8t48D_PQ/',
'https://drive.google.com/file/d/11WxgaYqmQld0oOiULgeX82rEg9GOtlmd/','https://drive.google.com/file/d/12ioNqDI9iVhExOPsYonIcy1TDu-XkZMS/','https://drive.google.com/file/d/1WvZFnoUwEyedZBaQVbMJ6Oy6SJ-QfIIB/','https://drive.google.com/file/d/1PW7SrGRVuYXY9r6YdY2F0qsLRxOJETUE/','https://drive.google.com/file/d/1SLPDqD0I7_k6iUghaf1dmZXcakq3DyiF/','https://drive.google.com/file/d/1ESsb81T08kiocr9F2nNFA-3A5uHJExyG/','https://drive.google.com/file/d/1haxnDT8RDiQDKWCp9u4JtTn54vrVI2b_/','https://drive.google.com/file/d/1W7zBcHYMoE18dpG2j4c01WNkZ8UIhWIN/','https://drive.google.com/file/d/1pMcDPH6wsp9cpY814Qms6gYsW6xez7d3/','https://drive.google.com/file/d/1c6xMKR_O-whFUJIfNnwsZhtRQ5JYlVv8/','https://drive.google.com/file/d/1tPlHRDbM11-1GeC2mlJs2T55jE700kTo/','https://drive.google.com/file/d/1Nx4j4NkqXHGF6oKEWR_qLorAQZUJspjc/'];




var PROJS_IN = [];
exports.updateProjects = async () => {
    let promises = [];

    console.log(`Numero de projetos de videos 2022 ${PROJS_2022_VIDEOS.length}`)
    console.log(`Numero de projetos de Links 2022 ${links.length}`)
    

    for (let i = 0; i <= MAX_PAGES; i++) {
        let url = `https://api.ufpb.br/api/projetos/extensao/todos-projetos?step=${i}`;
        promises = [...promises, axios.get(url).then(({ data }) => data)];
    }

    return await Promise.all(promises)
        .then(async (projetos) => {
            const mergedProjects = [].concat(...projetos); //Flattens all projects separated by pages
            console.log(`${mergedProjects.length} total projects`);

            let validProjects = [];
            for (project of mergedProjects) {
                project.codigo = addPadding(project.codigo)
                if (PROJS_2022_VIDEOS.includes(project.codigo)) {
                    PROJS_IN.push(project.codigo);
                    link_index = PROJS_2022_VIDEOS.indexOf(project.codigo)
                    validProjects.push({
                        idCategoria: 2,
                        idEvento: 1,
                        dataInicioProjeto: '01-01-2022 00:00:00',
                        dataFimProjeto: '12-31-2022 23:59:59',
                        linkArtefato: links[link_index],
                        ...project,
                    });
                    }
                    
        }
            console.log(`${validProjects.length} projects this year`);

            let membros = [];
            const formattedProjects = validProjects.map((el) => {
                const {
                    idProjeto,
                    titulo: tituloProjeto,
                    codigo: codigoProjeto,
                    descricao: descricaoProjeto,
                    // dataInicio: dataInicioProjeto,
                    // dataFim: dataFimProjeto,
                    ano: anoProjeto,
                    idAreaTematica,
                    idUnidade,
                    idCategoria,
                    linkArtefato,
                    idEvento,
                    dataInicioProjeto,
                    dataFimProjeto,
                } = _.pick(el, [
                    "idProjeto",
                    "codigo",
                    "titulo",
                    "descricao",
                    // "dataInicio",
                    // "dataFim",
                    "ano",
                    "idAreaTematica",
                    "idUnidade",
                    "idCategoria",
                    "linkArtefato",
                    "idEvento",
                    "dataInicioProjeto",
                    "dataFimProjeto",
                ]);

                const membrosAssociados = el.membros.filter(
                    ({ funcao }) => funcao === "COORDENADOR(A)"
                );
                const formattedMembros = membrosAssociados.map(
                    ({ idPessoa, idVinculo, funcao, nome }) => ({
                        idPessoa,
                        idVinculoPessoa: idVinculo,
                        idProjeto,
                        funcaoMembro: funcao,
                        nomePessoa: nome,
                    })
                );

                membros.push(formattedMembros);

                return {
                    idProjeto,
                    codigoProjeto,
                    tituloProjeto,
                    descricaoProjeto,
                    dataInicioProjeto,
                    dataFimProjeto,
                    anoProjeto: `${anoProjeto}-01-01`,
                    idAreaTematica:
                        idAreaTematica > 3
                            ? idAreaTematica - 1
                            : idAreaTematica,
                    idUnidade,
                    idEvento,
                    idCategoria,
                    linkArtefato
                };
            });
            
 
            // flattening membros array
            let flattenedMembros = [];
            flattenedMembros = flat(membros);

            // removing duplicates
            const uniqueProjects = _.uniqBy(formattedProjects, "idProjeto");
            const uniqueMembros = _.uniqWith(flattenedMembros, _.isEqual);

            console.log(uniqueProjects.length);
            // inserting
            const projectsInserted = await knex
                .raw(
                    `${knex("Projetos")
                        .insert(uniqueProjects)
                        .toQuery()} ON CONFLICT ("idProjeto") DO UPDATE SET "idUnidade" = excluded."idUnidade", "codigoProjeto" = excluded."codigoProjeto"`
                )
                .then(({ rowCount }) => {
                    console.log(`${rowCount} projetos upserted`);
                    return true;
                })
                .catch((error) => console.log(error));

            const membersInserted = await knex
                .raw(
                    `${knex("Membros_Extensao")
                        .insert(uniqueMembros)
                        .toQuery()} ON CONFLICT ("idProjeto", "idPessoa", "idVinculoPessoa") DO UPDATE SET "nomePessoa" = excluded."nomePessoa"`
                )
                .then(({ rowCount }) => {
                    console.log(`${rowCount} membros upserted`);
                    return true;
                })
                .catch((error) => console.log(error));

            return projectsInserted && membersInserted;
        })
        .catch((error) => console.log(error));
};

const addPadding = (code) => {
    const tipo = code.split("-")[0].slice(0, 2);
    const numero = code.split("-")[0].slice(2).padStart(3, "0");
    const ano = code.split("-")[1];
    return `${tipo}${numero}-${ano}`;
};

const flat = (input, depth = 1, stack = []) => {
    for (let item of input) {
        if (item instanceof Array && depth > 0) {
            flat(item, depth - 1, stack);
        } else {
            stack.push(item);
        }
    }

    return stack;
};
