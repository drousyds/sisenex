const axios = require("axios");
const _ = require("lodash");
var knex = require("../knexinstance");
const fs = require("fs");

const MAX_PAGES = 52;

exports.updateProjects = async () => {
    let promises = [];
    for (let i = 0; i <= MAX_PAGES; i++) {
        let url = `https://api.ufpb.br/api/projetos/extensao/todos-projetos?step=${i}`;
        promises = [...promises, axios.get(url).then(({ data }) => data)];
    }

    return await Promise.all(promises)
        .then(async (projetos) => {
            const mergedProjects = [].concat(...projetos); //Flattens all projects separated by pages
            console.log(`${mergedProjects.length} total projects`);

            let validProjects = [];
            for (project of mergedProjects) {
                if (project.ano == new Date().getFullYear()) {
                    validProjects.push({
                        idCategoria: 2,
                        dataInicioProjeto: '01-01-2021 00:00:00',
                        dataFimProjeto: '12-31-2021 23:59:59',
                        ...project,
                    });
                }
            }

            console.log(`${validProjects.length} projects this year`);

            let membros = [];
            const formattedProjects = validProjects.map((el) => {
                const {
                    idProjeto,
                    titulo: tituloProjeto,
                    codigo: codigoProjeto,
                    descricao: descricaoProjeto,
                    // dataInicio: dataInicioProjeto,
                    // dataFim: dataFimProjeto,
                    ano: anoProjeto,
                    idAreaTematica,
                    idUnidade,
                    idCategoria,
                    dataInicioProjeto,
                    dataFimProjeto,
                } = _.pick(el, [
                    "idProjeto",
                    "codigo",
                    "titulo",
                    "descricao",
                    // "dataInicio",
                    // "dataFim",
                    "ano",
                    "idAreaTematica",
                    "idUnidade",
                    "idCategoria",
                    "dataInicioProjeto",
                    "dataFimProjeto",
                ]);

                const membrosAssociados = el.membros.filter(
                    ({ funcao }) => funcao === "COORDENADOR(A)"
                );
                const formattedMembros = membrosAssociados.map(
                    ({ idPessoa, idVinculo, funcao, nome }) => ({
                        idPessoa,
                        idVinculoPessoa: idVinculo,
                        idProjeto,
                        funcaoMembro: funcao,
                        nomePessoa: nome,
                    })
                );

                membros.push(formattedMembros);

                return {
                    idProjeto,
                    codigoProjeto: addPadding(codigoProjeto),
                    tituloProjeto,
                    descricaoProjeto,
                    dataInicioProjeto,
                    dataFimProjeto,
                    anoProjeto: `${anoProjeto}-01-01`,
                    idAreaTematica:
                        idAreaTematica > 3
                            ? idAreaTematica - 1
                            : idAreaTematica,
                    idUnidade,
                    idCategoria,
                };
            });

            // flattening membros array
            let flattenedMembros = [];
            flattenedMembros = flat(membros);

            // removing duplicates
            const uniqueProjects = _.uniqBy(formattedProjects, "idProjeto");
            const uniqueMembros = _.uniqWith(flattenedMembros, _.isEqual);

            console.log(uniqueProjects.length);
            // inserting
            const projectsInserted = await knex
                .raw(
                    `${knex("Projetos")
                        .insert(uniqueProjects)
                        .toQuery()} ON CONFLICT ("idProjeto") DO UPDATE SET "idUnidade" = excluded."idUnidade", "codigoProjeto" = excluded."codigoProjeto"`
                )
                .then(({ rowCount }) => {
                    console.log(`${rowCount} projetos upserted`);
                    return true;
                })
                .catch((error) => console.log(error));

            const membersInserted = await knex
                .raw(
                    `${knex("Membros_Extensao")
                        .insert(uniqueMembros)
                        .toQuery()} ON CONFLICT ("idProjeto", "idPessoa", "idVinculoPessoa") DO UPDATE SET "nomePessoa" = excluded."nomePessoa"`
                )
                .then(({ rowCount }) => {
                    console.log(`${rowCount} membros upserted`);
                    return true;
                })
                .catch((error) => console.log(error));

            return projectsInserted && membersInserted;
        })
        .catch((error) => console.log(error));
};

const addPadding = (code) => {
    const tipo = code.split("-")[0].slice(0, 2);
    const numero = code.split("-")[0].slice(2).padStart(3, "0");
    const ano = code.split("-")[1];
    return `${tipo}${numero}-${ano}`;
};

const flat = (input, depth = 1, stack = []) => {
    for (let item of input) {
        if (item instanceof Array && depth > 0) {
            flat(item, depth - 1, stack);
        } else {
            stack.push(item);
        }
    }

    return stack;
};
