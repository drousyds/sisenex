const nodemailer = require('nodemailer');
const Pessoas_Apresentacoes = require('../objectionDatabase/models/model_pessoasApresentacoes')
const Pessoas_Projetos = require('../objectionDatabase/models/model_pessoasProjetos')
const Avaliacoes = require('../objectionDatabase/models/model_avaliacoes')
const Projetos = require('../objectionDatabase/models/model_projetos')

const transporter = nodemailer.createTransport({
  service: 'Hotmail',
  auth: {
    user: 'alisson_galiza_@hotmail.com'
  }
});

exports.enviarEmail = async ({ idApresentacao, id2Apresentacao }) => {

  const projetos = await Projetos
    .query()
    .select('idProjeto')
    .where({ idApresentacao, id2Apresentacao })
    .map(proj => proj.idProjeto)

  const pessoas = await Pessoas_Apresentacoes
    .query()
    .select(['idPessoa', 'idVinculoPessoa'])
    .where({ idApresentacao, id2Apresentacao })
    .map(async pessoa => {
      await Pessoas_Projetos
        .query()
        .joinRelation('projeto')
        .select(['projeto.tituloProjeto', 'Pessoas_Projetos.idProjeto as idProjeto', 'Pessoas_Projetos.mediaProjeto'])
        .where(pessoa)
        .whereIn(['Pessoas_Projetos.idProjeto'], projetos)
        .then(projetos => {
          projetos.map(proj => console.log(`Pessoa de id ${pessoa.idPessoa} avaliou o projeto de id ${proj.idProjeto} e deu média ${proj.mediaProjeto}`))
        })
    })



}

const enviar = async (emailPessoa, body) => {
  let info = await transporter.sendMail({
    from: '"SisEnex" <alisson_galiza_@hotmail.com>',
    to: emailPessoa,
    subject: 'Suas avaliações no Enex 2019',
    html: body
  });
}

