const axios = require("axios");
const _ = require("lodash");
var knex = require("../knexinstance");

const MAX_PAGES = 95;
const projetos = require("./projetos/projetos2024.json");
const PROJS_2024_1 = projetos['1']
const PROJS_2024_23 = projetos['2/3']
const PROJS_2024_4 = projetos['4']
const Total = Object.values(projetos)

var PROJS_IN = [];
// Helper function to check if idUnidade exists
const checkUnidadeExists = async (idUnidade) => {
    const result = await knex("Unidades").where("idUnidade", idUnidade).first();
    return result !== undefined;
};

// Helper function to check if idProjeto exists
const checkProjetoExists = async (idProjeto) => {
    const result = await knex("Projetos").where("idProjeto", idProjeto).first();
    return result !== undefined;
};

exports.updateProjects = async () => {
    let promises = [];
    console.log(`${PROJS_2024_1.length + PROJS_2024_23.length + PROJS_2024_4.length} NUMBER OF projects 2024`);
    
    for (let i = 0; i <= MAX_PAGES; i++) {
        let url = `https://api.ufpb.br/api/projetos/extensao/todos-projetos?step=${i}`;
        promises = [...promises, axios.get(url).then(({ data }) => data)];
    }

    return await Promise.all(promises)
        .then(async (projetos) => {
            const mergedProjects = [].concat(...projetos); // Flattens all projects separated by pages
            console.log(`${mergedProjects.length} total projects`);

            let validProjects = [];
            for (project of mergedProjects) {
                try {
                    project.codigo = addPadding(project.codigo);

                    // Ensure idUnidade exists in Unidades table before inserting into Projetos
                    if (!await checkUnidadeExists(project.idUnidade)) {
                        console.log(`Unidade with idUnidade = ${project.idUnidade} does not exist. Skipping project.`);
                        continue;
                    }

                    // If valid, proceed to add the project to validProjects
                    if (PROJS_2024_1.includes(project.codigo)) {
                        validProjects.push({
                            idCategoria: 3,
                            idEvento: 1,
                            dataInicioProjeto: '01-01-2023 00:00:00',
                            dataFimProjeto: '12-31-2024 23:59:59',
                            ...project,
                        });
                    } else if (PROJS_2024_23.includes(project.codigo)) {
                        validProjects.push({
                            idCategoria: 3,
                            idEvento: 2,
                            dataInicioProjeto: '01-01-2023 00:00:00',
                            dataFimProjeto: '12-31-2024 23:59:59',
                            ...project,
                        });
                    } else if (PROJS_2024_4.includes(project.codigo)) {
                        validProjects.push({
                            idCategoria: 3,
                            idEvento: 3,
                            dataInicioProjeto: '01-01-2023 00:00:00',
                            dataFimProjeto: '12-31-2024 23:59:59',
                            ...project,
                        });
                    }
                } catch (error) {
                    console.log(`Error with project codigo: ${project.codigo}`);
                    console.error(error);
                    continue;
                }
            }

            console.log(`${validProjects.length} projects this year`);

            let membros = [];
            const formattedProjects = validProjects.map((el) => {
                const {
                    idProjeto,
                    titulo: tituloProjeto,
                    codigo: codigoProjeto,
                    descricao: descricaoProjeto,
                    ano: anoProjeto,
                    idAreaTematica,
                    idUnidade,
                    idCategoria,
                    idEvento,
                    dataInicioProjeto,
                    dataFimProjeto,
                } = _.pick(el, [
                    "idProjeto",
                    "codigo",
                    "titulo",
                    "descricao",
                    "ano",
                    "idAreaTematica",
                    "idUnidade",
                    "idCategoria",
                    "idEvento",
                    "dataInicioProjeto",
                    "dataFimProjeto",
                ]);

                const membrosAssociados = el.membros.filter(({ funcao }) => funcao === "COORDENADOR(A)");
                const formattedMembros = membrosAssociados.map(
                    ({ idPessoa, idVinculo, funcao, nome }) => ({
                        idPessoa,
                        idVinculoPessoa: idVinculo,
                        idProjeto,
                        funcaoMembro: funcao,
                        nomePessoa: nome,
                    })
                );

                membros.push(formattedMembros);

                return {
                    idProjeto,
                    codigoProjeto,
                    tituloProjeto,
                    descricaoProjeto,
                    dataInicioProjeto,
                    dataFimProjeto,
                    anoProjeto: `${anoProjeto}-01-01`,
                    idAreaTematica: idAreaTematica > 3 ? idAreaTematica - 1 : idAreaTematica,
                    idUnidade,
                    idEvento,
                    idCategoria,
                };
            });

            // Flatten and remove duplicates for members
            let flattenedMembros = flat(membros);
            const uniqueProjects = _.uniqBy(formattedProjects, "idProjeto");
            const uniqueMembros = _.uniqWith(flattenedMembros, _.isEqual);

            // Insert valid projects
            const projectsInserted = await knex
            .raw(
                `${knex("Projetos")
                    .insert(uniqueProjects)
                    .toQuery()} ON CONFLICT ("idProjeto") DO UPDATE SET "idUnidade" = excluded."idUnidade", "codigoProjeto" = excluded."codigoProjeto"`
            )
            .then(({ rowCount }) => {
                console.log(`${rowCount} projetos upserted`);
                return true;
            })
            .catch((error) => console.log(error));

            const membersInserted = await knex
            .raw(
                `${knex("Membros_Extensao")
                    .insert(uniqueMembros)
                    .toQuery()} ON CONFLICT ("idProjeto", "idPessoa", "idVinculoPessoa") DO UPDATE SET "nomePessoa" = excluded."nomePessoa"`
            )
            .then(({ rowCount }) => {
                console.log(`${rowCount} membros upserted`);
                return true;
            })
            .catch((error) => console.log(error));

        return projectsInserted && membersInserted;
    })
    .catch((error) => console.log(error));
};


const addPadding = (code) => {
    if (!code || typeof code !== "string") {
        // Log the invalid code
        console.error(`Invalid 'codigo' value: ${code}`);
        return null;  // Return a default value, or handle as appropriate
    }

    try {
        const tipo = code.split("-")[0].slice(0, 2);
        const numero = code.split("-")[0].slice(2).padStart(3, "0");
        const ano = code.split("-")[1];
        return `${tipo}${numero}-${ano}`;
    } catch (err) {
        console.error(`Error processing 'codigo' value: ${code}`);
        console.error(err);
        return null;  // Return a default value or handle the error as needed
    }
};

const flat = (input, depth = 1, stack = []) => {
    for (let item of input) {
        if (item instanceof Array && depth > 0) {
            flat(item, depth - 1, stack);
        } else {
            stack.push(item);
        }
    }
    return stack;
};
