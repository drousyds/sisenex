const axios = require("axios");
const _ = require("lodash");
var knex = require("../knexinstance");

const MAX_PAGES = 74;
const projetos = require("./projetos/projetos2023.json");
const PROJS_2023_1 = projetos['1']
const PROJS_2023_23 = projetos['2/3']
const PROJS_2023_4 = projetos['4']
const Total = Object.values(projetos)

var PROJS_IN = [];
exports.updateProjects = async () => {
    let promises = [];
    console.log(`${PROJS_2023_1.length + PROJS_2023_23.length + PROJS_2023_4.length} NUMBER OF projects 2023`);
    console.log(Total.includes('PJ349-2022'))
    for (let i = 0; i <= MAX_PAGES; i++) {
        let url = `https://api.ufpb.br/api/projetos/extensao/todos-projetos?step=${i}`;
        promises = [...promises, axios.get(url).then(({ data }) => data)];
    }

    return await Promise.all(promises)
        .then(async (projetos) => {
            const mergedProjects = [].concat(...projetos); //Flattens all projects separated by pages
            console.log(`${mergedProjects.length} total projects`);

            let validProjects = [];
            for (project of mergedProjects) {

                    project.codigo = addPadding(project.codigo)
                    if (PROJS_2023_1.includes(project.codigo)) {
                        PROJS_IN.push(project.codigo);
                        validProjects.push({
                            idCategoria: 3,
                            idEvento: 1,
                            dataInicioProjeto: '01-01-2023 00:00:00',
                            dataFimProjeto: '12-31-2023 23:59:59',
                            ...project,
                        });
                    }
                    else if (PROJS_2023_23.includes(project.codigo)) {
                        PROJS_IN.push(project.codigo);
                        validProjects.push({
                            idCategoria: 3,
                            idEvento: 2,
                            dataInicioProjeto: '01-01-2023 00:00:00',
                            dataFimProjeto: '12-31-2023 23:59:59',
                            ...project,
                        });
                    }
                    else if (PROJS_2023_4.includes(project.codigo)) {
                        PROJS_IN.push(project.codigo);
                        validProjects.push({
                            idCategoria: 3,
                            idEvento: 3,
                            dataInicioProjeto: '01-01-2023 00:00:00',
                            dataFimProjeto: '12-31-2023 23:59:59',
                            ...project,
                        });
                    }
        }
            console.log(`${validProjects.length} projects this year`);

            let membros = [];
            const formattedProjects = validProjects.map((el) => {
                const {
                    idProjeto,
                    titulo: tituloProjeto,
                    codigo: codigoProjeto,
                    descricao: descricaoProjeto,
                    // dataInicio: dataInicioProjeto,
                    // dataFim: dataFimProjeto,
                    ano: anoProjeto,
                    idAreaTematica,
                    idUnidade,
                    idCategoria,
                    idEvento,
                    dataInicioProjeto,
                    dataFimProjeto,
                } = _.pick(el, [
                    "idProjeto",
                    "codigo",
                    "titulo",
                    "descricao",
                    // "dataInicio",
                    // "dataFim",
                    "ano",
                    "idAreaTematica",
                    "idUnidade",
                    "idCategoria",
                    "idEvento",
                    "dataInicioProjeto",
                    "dataFimProjeto",
                ]);

                const membrosAssociados = el.membros.filter(
                    ({ funcao }) => funcao === "COORDENADOR(A)"
                );
                const formattedMembros = membrosAssociados.map(
                    ({ idPessoa, idVinculo, funcao, nome }) => ({
                        idPessoa,
                        idVinculoPessoa: idVinculo,
                        idProjeto,
                        funcaoMembro: funcao,
                        nomePessoa: nome,
                    })
                );

                membros.push(formattedMembros);

                return {
                    idProjeto,
                    codigoProjeto,
                    tituloProjeto,
                    descricaoProjeto,
                    dataInicioProjeto,
                    dataFimProjeto,
                    anoProjeto: `${anoProjeto}-01-01`,
                    idAreaTematica:
                        idAreaTematica > 3
                            ? idAreaTematica - 1
                            : idAreaTematica,
                    idUnidade,
                    idEvento,
                    idCategoria,
                };
            });
            
            for (let i = 0; i < Total.length; i++) {
                if (!PROJS_IN.includes(Total[i]))
                    console.log(Total[i])
            }
            // flattening membros array
            let flattenedMembros = [];
            flattenedMembros = flat(membros);

            // removing duplicates
            const uniqueProjects = _.uniqBy(formattedProjects, "idProjeto");
            const uniqueMembros = _.uniqWith(flattenedMembros, _.isEqual);

            console.log(uniqueProjects.length);
            // inserting
            const projectsInserted = await knex
                .raw(
                    `${knex("Projetos")
                        .insert(uniqueProjects)
                        .toQuery()} ON CONFLICT ("idProjeto") DO UPDATE SET "idUnidade" = excluded."idUnidade", "codigoProjeto" = excluded."codigoProjeto"`
                )
                .then(({ rowCount }) => {
                    console.log(`${rowCount} projetos upserted`);
                    return true;
                })
                .catch((error) => console.log(error));

            const membersInserted = await knex
                .raw(
                    `${knex("Membros_Extensao")
                        .insert(uniqueMembros)
                        .toQuery()} ON CONFLICT ("idProjeto", "idPessoa", "idVinculoPessoa") DO UPDATE SET "nomePessoa" = excluded."nomePessoa"`
                )
                .then(({ rowCount }) => {
                    console.log(`${rowCount} membros upserted`);
                    return true;
                })
                .catch((error) => console.log(error));

            return projectsInserted && membersInserted;
        })
        .catch((error) => console.log(error));
};

const addPadding = (code) => {
    const tipo = code.split("-")[0].slice(0, 2);
    const numero = code.split("-")[0].slice(2).padStart(3, "0");
    const ano = code.split("-")[1];
    return `${tipo}${numero}-${ano}`;
};

const flat = (input, depth = 1, stack = []) => {
    for (let item of input) {
        if (item instanceof Array && depth > 0) {
            flat(item, depth - 1, stack);
        } else {
            stack.push(item);
        }
    }

    return stack;
};
