var AreasTematicas = require('../models/model_areasTematicas')
var _ = require('lodash')

exports.getAreasTematicasById = ids =>{
    return AreasTematicas
    .query()
    .findByIds(ids)
}

exports.getAreasTematicas = (filters = {}) => {
  return AreasTematicas
    .query()
    .where((q) => {
      _.mapKeys(filters, (value, key) => {
        q.where(`AreasTematicas.${key}`, value)
      });
    })
};

exports.getAreaTematica = ({ idAreaTematica }) => {
  return AreasTematicas
    .query()
    .where({ idAreaTematica })
    .then(e => e[0])
};

exports.postAreaTematica = data => {
  return AreasTematicas
    .query()
    .insertAndFetch(data)
    .then(el=>el)
};

exports.putAreaTematica = ({ idAreaTematica }, data) => {
  return AreasTematicas
    .query()
    .patchAndFetchById(idAreaTematica, data)
};

exports.delAreaTematica = ({ idAreaTematica }) => {
  return AreasTematicas
    .query()
    .deleteById(idAreaTematica)
};
