const _ = require("lodash");
const { ApolloError } = require('apollo-server')
const Pessoas = require("../models/model_pessoas");
const Projetos = require("../models/model_projetos");
const PessoasApresentacoes = require("../models/model_pessoasApresentacoes");
const PessoasProjetos = require("../models/model_pessoasProjetos");
const Membros = require('../models/model_membros');
const knex = require("../../knexinstance");

exports.getMonitores = (filters = {}) => {
  return Pessoas.query()
    .where(q => {
      _.mapKeys(filters, (value, key) => {
        q.where(`Pessoas.${key}`, value);
      });
    })
    .where({ monitorPessoa: 1 });
};

exports.baterAvaliador = async ({
  idPessoa,
  idVinculoPessoa,
  idApresentacao,
  id2Apresentacao
}) => {

  await Membros
    .query()
    .where({
      idPessoa,
      idVinculoPessoa
    })
    .whereIn('idProjeto', Projetos.query().select('idProjeto').where({ idApresentacao, id2Apresentacao }))
    .then(el => {
      if (el.length >= 1)
        throw new ApolloError('Avaliador não pode avaliar pois coordena um ou mais projetos nesta apresentação', code = 666, ids = el)
    })

  return knex
    .raw(
      `${knex("Pessoas_Apresentacoes")
        .insert({
          idPessoa,
          idVinculoPessoa,
          id2Apresentacao,
          idApresentacao,
          aptidaoPessoa: '1'
        })
        .toQuery()} ON CONFLICT ("idApresentacao", "id2Apresentacao", "idPessoa", "idVinculoPessoa") DO UPDATE SET "aptidaoPessoa" = '1'`
    )
    .then(() => {
      return PessoasApresentacoes.query()
        .where({ idPessoa, idVinculoPessoa, id2Apresentacao, idApresentacao })
        .eager('pessoa')
        .then(ava => ava[0].pessoa);
    });
};

exports.removerAvaliadorDaApresentacao = async ({
  idPessoa,
  idVinculoPessoa,
  idApresentacao,
  id2Apresentacao
}) => {
  const avaliador = await PessoasApresentacoes.query().updateAndFetchById(
    [idPessoa, idVinculoPessoa, idApresentacao, id2Apresentacao],
    {
      aptidaoPessoa: 0
    }
  );
  return avaliador.aptidaoPessoa == 0 ? true : false;
};

exports.removerProjetoDeApresentacao = ({ idProjeto }) => {
  return Projetos.query()
    .patch({ idApresentacao: null, id2Apresentacao: null })
    .where({ idProjeto })
    .then(rows => (rows == 1 ? true : false));
};

exports.liberarProjeto = ({
  idProjeto
}) => {
  return Projetos
    .query()
    .patch({ disponibilidadeProjeto: 1, liberacaoProjeto: knex.fn.now() })
    .findById(idProjeto)
    .returning('*')
};

exports.liberarProjetoParaUmAvaliador = async ({
  idProjeto,
  idPessoa,
  idVinculoPessoa
}) => {
  //Coordena projeto?
  await Membros
    .query()
    .where({
      idPessoa,
      idVinculoPessoa,
      idProjeto
    })
    .then(el => {
      if (el.length == 1)
        throw new ApolloError('Projeto não pode ser liberado para este avaliador pois ele coordena este projeto', code = 666)
    })

  const projeto = await Projetos.query()
    .findById(idProjeto);

  const aptidaoNaApresentacao = await PessoasApresentacoes.query().select('aptidaoPessoa')
    .where({ idApresentacao: projeto.idApresentacao, id2Apresentacao: projeto.id2Apresentacao, idPessoa, idVinculoPessoa })
    .then(el => el[0])
  // .findById([idApresentacao, id2Apresentacao, idPessoa, idVinculoPessoa])

  if (!aptidaoNaApresentacao)
    throw new ApolloError("Avaliador não está apto na apresentacao")

  await knex.raw(`${knex("Pessoas_Projetos")
    .insert({ idPessoa, idVinculoPessoa, idProjeto, aptidaoPessoa: '1' })
    .toQuery()} ON CONFLICT ("idPessoa","idVinculoPessoa","idProjeto") DO UPDATE SET "aptidaoPessoa" = '1'`
  )
  return projeto
};

exports.fecharProjeto = async ({
  idProjeto
}) => {
  return await Projetos
    .query()
    .patch({ disponibilidadeProjeto: 0, liberacaoProjeto: knex.fn.now() })
    .findById(idProjeto)
    .returning('*')
}

exports.fecharProjetoParaUmAvaliador = async ({
  idProjeto,
  idPessoa,
  idVinculoPessoa
}) => {
  await knex.raw(`${knex("Pessoas_Projetos")
    .insert({ idPessoa, idVinculoPessoa, idProjeto, aptidaoPessoa: '0' })
    .toQuery()} ON CONFLICT ("idPessoa","idVinculoPessoa","idProjeto") DO UPDATE SET "aptidaoPessoa" = '0'`
  )

  return Projetos.query().findById(idProjeto);
};

exports.getMonitor = ({ idPessoa, idVinculoPessoa }, filters) => {
  return Pessoas.query()
    .where({
      idPessoa,
      idVinculoPessoa
    })
    .then(e => e[0]);
};
