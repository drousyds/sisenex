var Apresentacoes = require('../models/model_apresentacoes');
var Projetos = require('../models/model_projetos');
var Avaliacoes = require('../models/model_avaliacoes');
var PessoasApresentacoes = require('../models/model_pessoasApresentacoes');
var Pessoas = require('../models/model_pessoas');
var Reports = require('../models/model_reports');
var knex = require('../../knexinstance');
var _ = require('lodash');
var MonitoresQueries = require('./monitores.js');

exports.getApresentacoesById = ids => {
  return Apresentacoes.query().findByIds(ids);
};

exports.getApresentacoes = (filters = {}) => {
  return Apresentacoes.query().where(q => {
    _.mapKeys(filters, (value, key) => {
      q.where(`Apresentacoes.${key}`, value);
    });
  }).orderBy(['horaApresentacao'])
};

exports.getApresentacao = (filters) => {
  return Apresentacoes.query()
    .where(filters)
    .then(e => e[0]);
};

exports.iniciarApresentacao = async ({
  codigoApresentacao,
  dataInicioApresentacao,
  disponibilidadeApresentacao
}) => {
  const apresentacao = await Apresentacoes.query()
    .select('idApresentacao', 'id2Apresentacao')
    .where({ codigoApresentacao })
    .then(el => el[0]);
  return apresentacao
    .$query()
    .updateAndFetch({ dataInicioApresentacao, disponibilidadeApresentacao });
};

exports.fecharApresentacao = async ({
  codigoApresentacao,
  dataFimApresentacao
}) => {
  const apresentacao = await Apresentacoes.query()
    .select('idApresentacao', 'id2Apresentacao')
    .where({ codigoApresentacao })
    .then(e => e[0]);

  const temAvaliacoes = await Avaliacoes.query()
    .joinRelation('projeto')
    .count('projeto.idProjeto')
    .where({ 'projeto.idApresentacao': apresentacao.idApresentacao, 'projeto.id2Apresentacao': apresentacao.id2Apresentacao })
    .then(el => el[0].count > 1)

  //'2' significa encerrado e '0' significa que a apresentacao não possui avaliacao, logo concluímos que ela foi aberta por engano
  const novaApresentacao = await apresentacao
    .$query()
    .updateAndFetch({ dataFimApresentacao, disponibilidadeApresentacao: temAvaliacoes ? 2 : 0 });
  return novaApresentacao.disponibilidadeApresentacao == 0 || novaApresentacao.disponibilidadeApresentacao == 2 ? true : false;
};

exports.getPessoas = ({ idApresentacao, id2Apresentacao }) => {
  return PessoasApresentacoes.query()
    .joinRelation('pessoa')
    .select('pessoa.*')
    .where({ idApresentacao, id2Apresentacao, 'Pessoas_Apresentacoes.aptidaoPessoa': true })
};

exports.getProjetos = ({ idApresentacao, id2Apresentacao }) => {
  return Projetos.query().where({ idApresentacao, id2Apresentacao });
};

exports.postApresentacoes = async data => {
  let notUnique = true;
  let uuid = '';
  while (notUnique) {
    const [, { rows: [{ upper: codigoApresentacao }] }] =
      await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";select UPPER(LEFT(uuid_generate_v4()::text,8))');
    notUnique = await Apresentacoes.query().where({ codigoApresentacao }).then(el => el.length > 0);
    uuid = codigoApresentacao;
  }

  return await Apresentacoes.query()
    .insert({
      ...data,
      codigoApresentacao: uuid,
      disponibilidadeApresentacao: data.disponibilidadeApresentacao ? data.disponibilidadeApresentacao : 0
    })
    .returning('*');
};

exports.putApresentacoes = ({ idApresentacao, id2Apresentacao }, data) => {
  return Apresentacoes.query().patchAndFetchById([idApresentacao, id2Apresentacao], data);
};

exports.delApresentacoes = async ({ idApresentacao, id2Apresentacao }) => {
  await Projetos.query()
    .update({ idApresentacao: null, id2Apresentacao: null, idEvento: null })
    .where({ idApresentacao, id2Apresentacao })

  await Reports.query()
    .del()
    .where({ idApresentacao, id2Apresentacao })

  return Apresentacoes.query()
    .delete()
    .findById([idApresentacao, id2Apresentacao])
    .returning("*")
};

exports.fecharApresentacoesPorArea = idAreaTematica => {
  console.log(idAreaTematica)
  return Apresentacoes.query()
    .patch({ disponibilidadeApresentacao: 0 })
    .where('idAreaTematica', '=', idAreaTematica);
};

exports.fecharApresentacoesPorEvento = idEvento => {
  return Apresentacoes.query()
    .patch({ disponibilidadeApresentacao: 0 })
    .where('idEvento', '=', idEvento);
};

exports.liberarApresentacoesPorCategoria = async idCategoria => {
  const apres = await Apresentacoes.query()
    .where({ idCategoria });

  for ({idApresentacao, id2Apresentacao, codigoApresentacao} of apres) {
    const projs = await Projetos.query()
      .where({ idCategoria, idApresentacao, id2Apresentacao });

    for ({ idProjeto } of projs) {
      await MonitoresQueries.liberarProjeto({idProjeto});
    }

    await exports.iniciarApresentacao({
      codigoApresentacao,
      dataInicioApresentacao: knex.fn.now(6),
      disponibilidadeApresentacao: 1});
  }

  return true;
}
