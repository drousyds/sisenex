var _ = require("lodash");
var Campus = require("../models/model_campus");

exports.getCampi = filters => {

  return Campus
    .query()
    .where((q) => {
      _.mapKeys(filters, (value, key) => {
        q.where(`Campus.${key}`, value)
      });
    })
};

exports.getCampus = ({ idCampus }, filters) => {
  return Campus
    .query()
    .where({ idCampus })
    .then(e => e[0])
};

exports.atualizarCampus = (idCampus, data) => {
  return Campus.query()
    .patchAndFetchById(idCampus, data)
};

exports.excluirCampus = ({ idCampus }, data) => {
  return Campus.query()
    .deleteById(idCampus)
    .then(el => el == 1 ? true : false)
};

exports.criarCampus = (data) => {
  return Campus.query()
    .select()
    .where({ idCampus: data.idCampus })
    .then(resultados => {
      if (resultados.length == 0) {
        return Campus.query()
          .insertAndFetch(data)
      } else {
        return resultados[0]
      }
    })
};
