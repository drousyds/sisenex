var Perguntas = require("../models/model_perguntas");
const _ = require("lodash");

exports.getPerguntas = (filters = {}) => {
  return Perguntas.query();
};

exports.getPergunta = ({ idPergunta }) => {
  return Perguntas.query()
    .findById(idPergunta)
};

exports.postPergunta = data => {
  return Perguntas.query().insertAndFetch(data);
};

exports.putPergunta = ({ idPergunta }, data) => {
  return Perguntas.query().patchAndFetchById(idPergunta, data);
};

exports.delPergunta = ({ idPergunta }) => {
  return Perguntas.query()
    .delete()
    .where(idPergunta);
};
