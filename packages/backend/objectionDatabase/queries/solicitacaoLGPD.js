var Solicitacao = require("../models/model_solicitacaoLGPD");

exports.getSolicitacaoById = ({idSolicitacao}) =>{
  return Solicitacao.query()
    .findById(idSolicitacao); 
}

exports.getSolicitacoes = (input) =>{
  return Solicitacao.query()
    .where("idPessoa",input.idPessoa)
    .andWhere("idVinculoPessoa",input.idVinculoPessoa);
}

exports.postSolicitacoes = async (input) => {
  return await Solicitacao
    .query()
    .insert({ ...input,...{statusSolicitacao: "ABERTA"}}) // estado inicial da solicitacao eh ABERTA
    .returning('*');
}

exports.atualizarSolicitacao = (idSolicitacao, input) => {
  return Solicitacao.query()
    .patchAndFetchById(idSolicitacao, input)
    .returning('*');
}

exports.finalizarSolicitacoes = ({idSolicitacao}) => {
  return Solicitacao
    .query()
    .update({statusSolicitacao: "RESPONDIDA"})
    .findById(idSolicitacao)
    .returning('*');
}

