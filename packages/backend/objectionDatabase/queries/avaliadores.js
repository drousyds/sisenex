var _ = require("lodash");
var Pessoas = require("../models/model_pessoas");
var Avaliacoes = require("../models/model_avaliacoes");
var Apresentacoes = require("../models/model_apresentacoes");
var Membros = require('../models/model_membros');
var Pessoas_Apresentacoes = require("../models/model_pessoasApresentacoes");
var Pessoas_Projetos = require("../models/model_pessoasProjetos");
var Projetos = require("../models/model_projetos");
var knex = require('../../knexinstance')
var { transaction } = require('objection')
const { ApolloError } = require('apollo-server')

var MonitoresQueries = require('./monitores');

exports.getAvaliadores = filters => {
    return Pessoas.query()
        .where(q => {
            _.mapKeys(filters, (value, key) => {
                q.where(`Pessoas.${key}`, value);
            });
        })
        .where("avaliadorPessoa", 1);
};

exports.getAvaliador = (id, filters) => {
    return Pessoas.query().where({
        ...id,
        avaliadorPessoa: 1
    });
};

exports.getAvaliacoes = (id, filters) => {
    console.log(id);

    return Pessoas.query()
        .eager("avaliacoes(orderByIdPergunta).[projeto]") //função customizada definida no model de Pessoas
        // .modifyEager("avaliacoes", builder => {
        //     //caso haja um projeto como filtro, busque ele
        //     builder.where("idProjeto", filters.idProjeto);
        // })
        .skipUndefined()
        .where(id)
        .then(results => {
            // console.log(results[0],results[1]);

            var avaliacoesPorProjeto = [];
            _.chain(results[0].avaliacoes)
                .groupBy("idProjeto")
                .mapKeys((v, k) => {
                    avaliacoesPorProjeto.push({
                        idProjeto: k,
                        tituloProjeto: v[0].projeto.tituloProjeto,
                        avaliacoes: []
                    });
                    v.map(ava => {
                        avaliacoesPorProjeto[
                            avaliacoesPorProjeto.length - 1
                        ].avaliacoes.push({
                            idPergunta: ava.idPergunta,
                            notaAvaliacao: ava.notaAvaliacao,
                            dataAvaliacao: ava.dataAvaliacao
                        });
                    });
                })
                .value();
            // console.log('\n\n',avaliacoesPorProjeto);

            return (results[0].avaliacoes = avaliacoesPorProjeto);
        });
};

exports.getProjetosParaAvaliar = async ({ idPessoa, idVinculoPessoa }) => {
    // //recupera os projetos da apresentacao que o avaliador está
    try {

        const projetosParaAvaliar = await transaction(Pessoas_Projetos.knex(), async trx => {
            const projetos = await Pessoas_Projetos.query(trx)
                .eager('projeto.apresentacao')
                .where({ idPessoa, idVinculoPessoa })

            const apresentacoes = _.groupBy(projetos.map(({ projeto: { idApresentacao, id2Apresentacao } }) => {
                return { idApresentacao, id2Apresentacao }
            }),
                ({ idApresentacao, id2Apresentacao }) => `${idApresentacao}-${id2Apresentacao}`)

            const values = Object.keys(apresentacoes).map(ap => {
                const [idApresentacao, id2Apresentacao] = ap.split('-')
                return [idApresentacao, id2Apresentacao]
            })

            const pessoasApresentacoes = await Pessoas_Apresentacoes.query(trx)
                .where({ idPessoa, idVinculoPessoa, aptidaoPessoa: 1 })
                .whereInComposite(['idApresentacao', 'id2Apresentacao'], values)

            const projetosParaAvaliar = projetos.filter(pessoasProjeto => {
                return pessoasProjeto.aptidaoPessoa &&
                    pessoasProjeto.projeto.disponibilidadeProjeto &&
                    pessoasProjeto.projeto.apresentacao.disponibilidadeApresentacao == 1 &&
                    _.find(pessoasApresentacoes, { 'idApresentacao': pessoasProjeto.projeto.apresentacao.idApresentacao, 'id2Apresentacao': pessoasProjeto.projeto.apresentacao.id2Apresentacao }) &&
                    _.find(pessoasApresentacoes, { 'idApresentacao': pessoasProjeto.projeto.apresentacao.idApresentacao, 'id2Apresentacao': pessoasProjeto.projeto.apresentacao.id2Apresentacao }).aptidaoPessoa == 1
            })

            return projetosParaAvaliar.map(({ projeto }) => projeto)
        })
        return projetosParaAvaliar;
    }
    catch (err) {
        console.log(err);
    }

};

exports.getApresentacaoAtiva = (id) => {
    //pesquisa apresentacao ativa de um avaliador
    return Pessoas_Apresentacoes.query()
        .select("idApresentacao", "id2Apresentacao")
        .where({
            ...id,
            aptidaoPessoa: 1
        })
};

exports.avaliarProjeto = async ({ idPessoa, idVinculoPessoa, avaliacao }) => {
    /*
          Fluxo:
          1- Procura pela apresentacao que o projeto avaliado está alocado
          2- Verifica se o avaliador está apto a avaliar (pessoaProjeto e pessoaApresentacao) e se a apresentacao está aberta
          3- Avalia
          CATCH- Qualquer problema a transacao é cancelada e o erro reportado
      */

    try {

        const respostaAvaliacao = await transaction(Pessoas_Projetos.knex(), async trx => {
            const idsApresentacao = await Projetos
                .query(trx)
                .select("idApresentacao", "id2Apresentacao")
                .where("idProjeto", avaliacao.idProjeto)

            if (idsApresentacao.length === 0) {
                throw new ApolloError('Projeto não existe na apresentação')
            }

            // Coordena projeto?
            await Membros
                .query(trx)
                .where({
                    idPessoa,
                    idVinculoPessoa,
                    idProjeto: avaliacao.idProjeto
                })
                .then(el => {
                    if (el.length == 1)
                        throw new ApolloError('Avaliador não pode avaliar pois coordena este projeto', code = 666)
                })

            const aptidaoPessoaApresentacao = await Pessoas_Apresentacoes
                .query(trx)
                .select("aptidaoPessoa as aptidaoPessoaApresentacao")
                .where(idsApresentacao[0])
                .where({
                    idPessoa,
                    idVinculoPessoa
                })
                .then((ap) => {
                    if (ap.length === 0) {
                        throw new ApolloError('Avaliador não está na Apresentacao')
                    }
                    return ap[0].aptidaoPessoaApresentacao
                })


            const disponibilidadeApresentacao = await Apresentacoes
                .query(trx)
                .select("disponibilidadeApresentacao")
                .joinRelation('projetos')
                .where('projetos.idProjeto', avaliacao.idProjeto)
                .then((ap) => {
                    if (ap.length === 0) {
                        throw new ApolloError('Apresentacao não encontrada ou Projeto não possui Apresentacao')
                    }
                    return ap[0].disponibilidadeApresentacao
                })

            const disponibilidadePessoaProjeto = await Pessoas_Projetos
                .query(trx)
                .select("aptidaoPessoa as aptidaoPessoaProjeto")
                .where({
                    idProjeto: avaliacao.idProjeto,
                    idPessoa,
                    idVinculoPessoa
                })
                .then((ap) => {
                    if (ap.length === 0) {
                        throw new ApolloError('Projeto nunca foi liberado para esse Avaliador')
                    }
                    return ap[0].aptidaoPessoaProjeto
                })

            const disponibilidadeProjeto = await Projetos
                .query(trx)
                .select("disponibilidadeProjeto")
                .where({ idProjeto: avaliacao.idProjeto })
                .then((pj) => {
                    return pj[0].disponibilidadeProjeto
                })

            if (
                aptidaoPessoaApresentacao == 1 &&
                disponibilidadePessoaProjeto == 1 &&
                disponibilidadeApresentacao == 1 &&
                disponibilidadeProjeto == 1
            ) {
                let notas = [];
                avaliacao.notas.map(p => {
                    notas.push({
                        idPergunta: p.idPergunta,
                        notaAvaliacao: p.nota,
                        respostaTextual: p.respostaTextual,
                        idProjeto: avaliacao.idProjeto,
                        idPessoa,
                        idVinculoPessoa,
                        dataAvaliacao: knex.fn.now(),
                        avaliacaoManual: '0'
                    })
                });
                return await knex.raw(
                    //escrever avaliacao
                    `${knex("Avaliacoes")
                        .insert(notas)
                        .toQuery()
                    } ON CONFLICT ("idPergunta", "idProjeto", "idPessoa", "idVinculoPessoa") DO UPDATE SET "notaAvaliacao" = excluded."notaAvaliacao", 
          "avaliacaoManual" = excluded."avaliacaoManual", "respostaTextual" = excluded."respostaTextual" ,"dataAvaliacao" = ${knex.fn.now()}`
                )
                    .then(async (affectedRows) => {
                        const avaliado = affectedRows && affectedRows.rowCount === notas.length ? true : false
                        //escrever media na relacao pessoa<->avaliacao

                        // console.log(notas);

                        let notaLen = mediaProjeto = 0;
                        for ({ notaAvaliacao } of notas) {
                            if (notaAvaliacao != undefined) {
                                notaLen += 1;
                                mediaProjeto += notaAvaliacao;
                            }
                        };
                        mediaProjeto /= notaLen;

                        return await Pessoas_Projetos
                            .query()
                            .update({ mediaProjeto })
                            .where({ idPessoa: notas[0].idPessoa, idVinculoPessoa: notas[0].idVinculoPessoa, idProjeto: notas[0].idProjeto })
                            .then(() => avaliado)
                    })
            } else {
                throw new ApolloError("Avaliador não está apto a avaliar ou a apresentacao está fechada");
            }
        })
        return respostaAvaliacao;
    }
    catch (err) {
        console.log(err);
        throw new ApolloError(err.message);
    }
};

exports.avaliarProjetoPorUmAvaliador = async ({ idPessoaGerente, idVinculoPessoaGerente, idPessoa, idVinculoPessoa, avaliacao, nomeAvaliadorPapel }) => {
    /*
          Fluxo:
          1- Procuro se a pessoa que está avaliando é um gerente
          3- Avalio
          CATCH- Qualquer problema a transacao é cancelada e o erro reportado
      */

    try {

        const respostaAvaliacao = await transaction(Pessoas_Projetos.knex(), async trx => {

            const gerente = await Pessoas.query().select('gerentePessoa').where({ idPessoa: idPessoaGerente, idVinculoPessoa: idVinculoPessoaGerente, gerentePessoa: 1 })
            if (gerente.length === 0)
                throw new ApolloError('Pessoa tentando criar uma avaliação não é um gerente');

            const notas = avaliacao.notas.map(p => {
                return {
                    idPergunta: p.idPergunta,
                    notaAvaliacao: p.nota,
                    idProjeto: avaliacao.idProjeto,
                    respostaTextual: p.respostaTextual,
                    idPessoa,
                    idVinculoPessoa,
                    dataAvaliacao: knex.fn.now(),
                    avaliacaoManual: '1',
                    nomeAvaliadorPapel
                }
            });

            return knex.raw(
                `${knex("Avaliacoes")
                    .insert(notas)
                    .toQuery()
                } ON CONFLICT ("idPergunta","idProjeto","idPessoa","idVinculoPessoa") DO UPDATE SET "notaAvaliacao" = excluded."notaAvaliacao", 
        "avaliacaoManual" = excluded."avaliacaoManual", "nomeAvaliadorPapel" = excluded."nomeAvaliadorPapel","respostaTextual" = excluded."respostaTextual", "dataAvaliacao" = ${knex.fn.now()}`
            )
                .then(async (affectedRows) => {
                    const avaliado = affectedRows && affectedRows.rowCount === notas.length ? true : false
                    //escrever media na relacao pessoa<->avaliacao
                    let notaLen = mediaProjeto = 0;
                    for ({ notaAvaliacao } of notas) {
                        if (notaAvaliacao != undefined) {
                            notaLen += 1;
                            mediaProjeto += notaAvaliacao;
                        }
                    };
                    mediaProjeto /= notaLen;
                    return knex.raw(
                        `${knex("Pessoas_Projetos")
                            .insert({
                                idProjeto: notas[0].idProjeto,
                                idPessoa: notas[0].idPessoa,
                                idVinculoPessoa: notas[0].idVinculoPessoa,
                                aptidaoPessoa: notas[0].aptidaoPessoa,
                                mediaProjeto
                            })
                            .toQuery()
                        } ON CONFLICT ("idProjeto","idPessoa","idVinculoPessoa") DO UPDATE SET "mediaProjeto" = excluded."mediaProjeto"`
                    )
                        .then(() => avaliado)
                        .catch(e => console.log(e))
                })

        })
        return respostaAvaliacao;
    }
    catch (err) {
        console.log(err);
        throw new ApolloError(err.message);
    }
};

exports.getApresentacoesAvaliador = ({ idPessoa, idVinculoPessoa }) => {


    return Pessoas_Apresentacoes.query()
        .joinRelation('apresentacao')
        .select('apresentacao.*')
        .where({ 'Pessoas_Apresentacoes.aptidaoPessoa': 1, 'apresentacao.disponibilidadeApresentacao': 1 })
        .joinRelation('pessoa')
        .where({ 'pessoa.idPessoa': idPessoa, 'pessoa.idVinculoPessoa': idVinculoPessoa, 'pessoa.aptidaoPessoa': 1 })

};

async function avaliadorCoordenaProjetos(trx, avaliador, projetos) {
    let coordena = false;
    for (projeto of projetos) {
        await Membros
            .query(trx)
            .where({
                idPessoa: avaliador.idPessoa,
                idVinculoPessoa: avaliador.idVinculoPessoa,
                idProjeto: projeto.idProjeto
            })
            .then(el => {
                if (el.length == 1)
                    coordena = true
            });
    }
    return coordena
}

async function encontrarAvaliador(trx, avaliadores, projetos) {
    let avaliador = null;
    for (i = 0; i < avaliadores.length; ++i) {
        if (await avaliadorCoordenaProjetos(trx, avaliadores[0], projetos)) {
            avaliadores.push(avaliadores.shift());
        } else {
            avaliador = avaliadores.shift();
            break;
        }
    }
    return avaliador;
}

function liberarAvaliador(trx, {idPessoa, idVinculoPessoa}, {idApresentacao, id2Apresentacao}) {
    return trx.raw(`${knex("Pessoas_Apresentacoes")
                .insert({
                    idPessoa,
                    idVinculoPessoa,
                    id2Apresentacao,
                    idApresentacao,
                    aptidaoPessoa: '1'
                })
                .toQuery()} ON CONFLICT ("idApresentacao", "id2Apresentacao", "idPessoa", "idVinculoPessoa") DO UPDATE SET "aptidaoPessoa" = '1'`
    );
}

exports.alocarAvaliadoresVideo = async () => {
    await knex.transaction( async trx => {
        const avaliadores = await Pessoas.query(trx) 
            .where({avaliadorPessoa: 1, idCategoria: 2});

        const apresentacoes = await Apresentacoes.query(trx)
            .where({idCategoria: 2});

        if ((avaliadores.length / 2) < apresentacoes.length) {
            throw new ApolloError("Nao ha avaliadores suficientes para todas as apresentacoes; " +
                `min. necessario: ${apresentacoes.length*2}, existente: ${avaliadores.length}`);
        }

        for ({idApresentacao, id2Apresentacao} of apresentacoes) {
            const projetos = await Projetos.query(trx)
                .where({idApresentacao, id2Apresentacao});

            let avaliador0 = await encontrarAvaliador(trx, avaliadores, projetos);
            let avaliador1 = await encontrarAvaliador(trx, avaliadores, projetos);

            await liberarAvaliador(trx, avaliador0, {idApresentacao, id2Apresentacao});
            await liberarAvaliador(trx, avaliador1, {idApresentacao, id2Apresentacao});
        }
    });

    return true
}

exports.liberarAvaliadorProjetoPorCategoria = async idCategoria => {
    const apres = await Apresentacoes.query()
        .where({idCategoria});

    for ({idApresentacao, id2Apresentacao} of apres) {
        const avaliadores = await Pessoas_Apresentacoes.query()
            .where({idApresentacao, id2Apresentacao});

        const projs = await Projetos.query()
            .where({idApresentacao, id2Apresentacao});

        for ({idPessoa, idVinculoPessoa} of avaliadores) {
            for ({idProjeto} of projs) {
                await MonitoresQueries.liberarProjetoParaUmAvaliador({idProjeto, idPessoa, idVinculoPessoa});
            }
        }
    }

    return true;
}

exports.getListaDeApresentacoesAlocado = async ({ idPessoa, idVinculoPessoa }) => {
    //Pesquisa na tabela secundária 
    const pessoas_apresentacoes = await Pessoas_Apresentacoes.query()
        .where({ idPessoa, idVinculoPessoa, aptidaoPessoa: 1 })

    var values = []
    // pega os ids das apresentações
    for(let pessoa_apresentacoes of pessoas_apresentacoes){
        values.push([pessoa_apresentacoes.idApresentacao, pessoa_apresentacoes.id2Apresentacao]);
    }
    // retorna lista de apresentacoes alocada
    return  await Apresentacoes.query().findByIds(values);

}
