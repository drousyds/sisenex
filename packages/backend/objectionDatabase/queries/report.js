var _ = require("lodash");
var Report = require("../models/model_reports");
const knex = require("../../knexinstance");


exports.getReports = filters => {
  return Report
    .query()
    .where((q) => {
      _.mapKeys(filters, (value, key) => {
        q.where(`Reports.${key}`, value)
      });
    })
    .orderBy('dataCriacao', 'desc')
};

exports.getReport = ({ idReport }) => {
    return Report
        .query()
        .findById(idReport)
}

exports.postReport = async input => {
  console.log(input);
  console.log({ ...input });
  return await Report.query().insert({ ...input, ...{ statusReport: "ABERTA" } }).returning('*')
}

exports.atualizarStatusReport = (idReport, input) => {
  return Report.query()
    .patchAndFetchById(idReport, input)
    .returning('*');
}

exports.excluirReport = ({ idReport }, data) => {
  return Report.query()
    .deleteById(idReport)
    .then(el => el == 1 ? true : false)
};

exports.getPessoa = ({ idPessoa, idVinculoPessoa }) => {
  return Pessoas
    .query()
    .findById({ idPessoa, idVinculoPessoa })
};
