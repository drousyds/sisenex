const Avaliacoes = require("../models/model_avaliacoes");
const _ = require('lodash')



exports.getAvaliacao = ({ idPessoa, idVinculoPessoa, idProjeto }) => {
  return Avaliacoes
    .query()
    .where({ idPessoa, idVinculoPessoa, idProjeto })
    .skipUndefined()
    .then(results => {

      if (results.length === 0)
        return null

      let avaliacoesPorProjeto = [];
      _.chain(results)
        .groupBy("idProjeto")
        .mapKeys((v, k) => {

          avaliacoesPorProjeto.push({
            idProjeto: k,
            notas: [],
            idPessoa,
            idVinculoPessoa,
            avaliacaoManual: v[0].avaliacaoManual,
            nomeAvaliadorPapel: v[0].nomeAvaliadorPapel
          });

          let notas = []
          v.map(ava => {
            avaliacoesPorProjeto[avaliacoesPorProjeto.length - 1].notas.push({
              idPergunta: ava.idPergunta,
              nota: ava.notaAvaliacao,
              data: ava.dataAvaliacao,
              respostaTextual: ava.respostaTextual
            });
            notas.push(ava.notaAvaliacao)
          });

          let mediaProjeto = notasLen = 0;
          for (nota of notas) {
            if (nota != undefined) {
              mediaProjeto += nota;
              notasLen += 1;
            }
          }

          avaliacoesPorProjeto[avaliacoesPorProjeto.length - 1].media = mediaProjeto / notasLen;
        })
        .value();
      // console.log('\n\n', avaliacoesPorProjeto[0].notas);
      // console.log('\n\n', avaliacoesPorProjeto);

      // return (results[0].avaliacoes = avaliacoesPorProjeto);
      return avaliacoesPorProjeto[0]
    })
}

exports.getAvaliacoesPorAvaliador = ({ idPessoa, idVinculoPessoa, idProjeto }) => {
  const conditions = (idProjeto === undefined) ?
    { idPessoa, idVinculoPessoa } :
    { idPessoa, idVinculoPessoa, idProjeto }
  return Avaliacoes
    .query()
    .where(conditions)
    .skipUndefined()
    .then(results => {

      let avaliacoesPorProjeto = [];
      _.chain(results)
        .groupBy("idProjeto")
        .mapKeys((v, k) => {

          avaliacoesPorProjeto.push({
            idProjeto: k,
            notas: [],
            idPessoa,
            idVinculoPessoa
          });

          let notas = []
          v.map(ava => {
            avaliacoesPorProjeto[avaliacoesPorProjeto.length - 1].notas.push({
              idPergunta: ava.idPergunta,
              nota: ava.notaAvaliacao,
              data: ava.dataAvaliacao,
              respostaTextual: ava.respostaTextual
            });
            notas.push(ava.notaAvaliacao)
          });

          let mediaProjeto = notasLen = 0;
          for (nota of notas) {
            if (nota != undefined) {
              mediaProjeto += nota;
              notasLen += 1;
            }
          }

          avaliacoesPorProjeto[avaliacoesPorProjeto.length - 1].media = mediaProjeto / notasLen;
        })
        .value();

      const avaliacoesPorProjetoSorted = _.orderBy(avaliacoesPorProjeto, ({ notas }) => notas[0].data.getTime(), 'desc')
      return avaliacoesPorProjetoSorted
    })
};

// Retorna uma string indicando se o avaliador em questão já avaliou algo.
exports.getAvaliou = async ({idPessoa, idVinculoPessoa}) => {
  const registro_Avaliacoes = await Avaliacoes.query()
  .where({idPessoa, idVinculoPessoa})

  return registro_Avaliacoes.length > 0 ? "Sim" : "Não";

}

exports.getProjetosAvaliadosId = async ()=> {
  const avaliacoes = await Avaliacoes.query().select("idProjeto");
  
  var id_Projetos = []
  for(let avaliacao of avaliacoes){
    id_Projetos.push(avaliacao.idProjeto);
  }
  // remove duplicados
  var id_Projetos_unique = [...new Set(id_Projetos)]
  
  // lista de chave-valor {idProjeto : id_Projeto_value} 
  var id_projetos_Lista = []
  for(let id_Projeto_value of id_Projetos_unique){
    id_projetos_Lista.push({idProjeto: id_Projeto_value});
  }

  return id_projetos_Lista;
}

exports.getAvaliacoes = () => {
  return Avaliacoes.query();
}


function removeSubDuplicateSubArrays(arr){
  const setArray = new Set(arr.map(x => JSON.stringify(x)))
  const uniqArray = [...setArray].map(x => JSON.parse(x))

  return uniqArray;

}

// retornar (idPessoa, idVinculoPessoa, idProjeto)
exports.getAvaliacaoIndividualIds = async() => {
  //itera sobre todas as linhas da tabela caputando os ids
  const todas_Avaliacoes = await Avaliacoes.query().select("idPessoa","idVinculoPessoa", "idProjeto");

  var lista_avaliacao_ids_redun = []

  for(let avaliacao of todas_Avaliacoes){
    lista_avaliacao_ids_redun.push([avaliacao.idProjeto,avaliacao.idPessoa, avaliacao.idVinculoPessoa]);
  }

  //remove subarrays contendo uma trinca de ids repetidos, deixando somente avaliaçoes
  var lista_avaliacao_ids = removeSubDuplicateSubArrays(lista_avaliacao_ids_redun);

  // transforma em um array de objetos que será utilizado pelos resolversa para resolver o tipo projeto, avaliador e para  calcular as médias individuais
  var lista_objs_avaliacao_ids = [];

  for(let i = 0; i < lista_avaliacao_ids.length; i++){
    lista_objs_avaliacao_ids.push(
      { 
        idProjeto: lista_avaliacao_ids[i][0],
        idPessoa: lista_avaliacao_ids[i][1],
        idVinculoPessoa: lista_avaliacao_ids[i][2]

      }

    )
  }

  return lista_objs_avaliacao_ids;

}


exports.getMediaIndividualAvaliadorProjeto = async({idProjeto, idPessoa, idVinculoPessoa}) => {
  const avaliacoes = await Avaliacoes.query()
    .where(
      { idProjeto, 
        idPessoa, 
        idVinculoPessoa
      });

  var n = 0;
  var acumulador = 0;    
  for(let avaliacao of avaliacoes){
    if(avaliacao.notaAvaliacao !== null){
      acumulador +=  avaliacao.notaAvaliacao;
      n++;
    }
  }

  return acumulador / n;

}

exports.getnumAvaliadores = async({idProjeto}) => {
  const avaliacoes = await Avaliacoes.query().where({idProjeto});

  var contador = 0;

  for(let _ of avaliacoes){
    contador++;
  }
  
  return contador;
}

// exports.getCategoria = ({ idPessoa, idVinculoPessoa, idProjeto }) => {
//     return Avaliacoes
//         .query()
//         .where({ idPessoa, idVinculoPessoa, idProjeto })
//         .then(e => e[0])
// };

// exports.getPerguntas = ({ idPessoa, idVinculoPessoa, idProjeto }, filters) => {
//     return Avaliacoes.query()
//         .where(idPessoa, idVinculoPessoa, idProjeto)
//         .then(e => e[0])
// };

// exports.postCategoria = data => {
//     return Avaliacoes.query().insertAndFetch(data);
// };

// exports.putCategoria = ({ idPessoa, idVinculoPessoa, idProjeto }, data) => {
//     return Avaliacoes.query().patchAndFetchById(idPessoa, idVinculoPessoa, idProjeto, data);
// };

// exports.delCategoria = ({ idPessoa, idVinculoPessoa, idProjeto }) => {
//     return Avaliacoes.query()
//         .delete()
//         .where(idPessoa, idVinculoPessoa, idProjeto);
// };
