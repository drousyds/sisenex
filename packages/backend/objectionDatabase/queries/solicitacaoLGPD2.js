const SolicitacaoLGPD = require("../models/model_solicitacaoLGPD");
const Pessoas = require("../models/model_pessoas");
const _ = require('lodash');

exports.getSolicitacoesLGPD = (filters = {}) =>{
  return SolicitacaoLGPD.query();
};

exports.getPessoa = ({ idPessoa, idVinculoPessoa }) => {
  return Pessoas
    .query()
    .findById({ idPessoa, idVinculoPessoa })
};
