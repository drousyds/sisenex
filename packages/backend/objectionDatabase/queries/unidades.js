var _ = require("lodash");
var Unidades = require("../models/model_unidades");

exports.getUnidades = filters => {

  return Unidades
    .query()
    .where((q) => {
      _.mapKeys(filters, (value, key) => {
        q.where(`Unidades.${key}`, value)
      });
    })
};
exports.getUnidadeById = ids =>{
  return Unidades
  .query()
  .findByIds(ids)
}
exports.getUnidade = ({ idUnidade }, filters) => {
  return Unidades
    .query()
    .where({ idUnidade })
    .then(e => e[0])
};

exports.getUnidadesByCampus = ({ idCampus }, filters) => {
  return Unidades
    .query()
    .where('idCampus', idCampus)
};
exports.atualizaUnidade = (idUnidade, data) => {

  return Unidades.query()
    .patchAndFetchById(idUnidade, data)
};

exports.excluiUnidade = ({ idUnidade }, data) => {
  return Unidades.query()
    .deleteById(idUnidade)
    .then(el => el == 1 ? true : false)
};

exports.criarUnidade = (data) => {
  return Unidades.query()
    .select()
    .where({ idUnidade: data.idUnidade })
    .then(resultados => {
      if (resultados.length == 0) {
        return Unidades.query()
          .insertAndFetch(data)
      } else {
        return resultados[0]
      }
    })
};