var _ = require("lodash");
var Pessoas = require("../models/model_pessoas");

exports.getPessoas = filters => {

  return Pessoas
    .query()
    .where((q) => {
      _.mapKeys(filters, (value, key) => {
        q.where(`Pessoas.${key}`, value)
      });
    })
};

exports.getPessoa = ({ idPessoa, idVinculoPessoa }, filters) => {
  return Pessoas
    .query()
    .where({
      idPessoa,
      idVinculoPessoa
    })
    .then(e => e[0])
};

exports.putPessoa = ({ idPessoa, idVinculoPessoa }, data) => {
  return Pessoas.query()
    .patchAndFetchById([idPessoa, idVinculoPessoa], data)
};

exports.delPessoa = ({ idPessoa, idVinculoPessoa }, data) => {
  return Pessoas.query()
    .deleteById([idPessoa, idVinculoPessoa])
    .then(el => el == 1 ? true : false)
};

exports.postPessoa = ({ input: data }) => {
  //existe ? retornaOK : cria e retorna OK
  // console.log(data)
  return Pessoas.query()
    .select()
    .where({
      idPessoa: data.idPessoa,
      idVinculoPessoa: data.idVinculoPessoa
    })
    .then(resultados => {
      if (resultados.length == 0) {
        const { digitoVerificador, idCategoria, ...newData } = data

        pessoa = {
          ...newData,
          idCategoria: idCategoria ? idCategoria : 2,
          aptidaoPessoa: 1
        };

        return Pessoas.query()
          .insertAndFetch(pessoa)
      } else {
        return resultados[0]
      }
    })
};

exports.getPessoasAvaliadoras = () => {
  return Pessoas.query().where({avaliadorPessoa: 1});
}
