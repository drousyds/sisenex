const Categorias = require("../models/model_categorias");
const _ = require('lodash')

exports.getCategorias = (filters = {}) => {
  return Categorias
    .query()
    .where((q) => {
      _.mapKeys(filters, (value, key) => {
        q.where(`Categorias.${key}`, value)
      });
    })
};

exports.getCategoria = ({ idCategoria }) => {
  return Categorias
    .query()
    .where({ idCategoria })
    .then(e => e[0])
};

exports.getPerguntas = ({ idCategoria }, filters) => {
  return Categorias.query()
    .where(idCategoria)
    .then(e => e[0])
};

exports.postCategoria = data => {
  return Categorias.query().insertAndFetch(data);
};

exports.putCategoria = ({ idCategoria }, data) => {
  return Categorias.query().patchAndFetchById(idCategoria, data);
};

exports.delCategoria = ({ idCategoria }) => {
  return Categorias.query()
    .delete()
    .where(idCategoria);
};
