var _ = require('lodash');
const { ApolloError } = require('apollo-server');
var Projetos = require('../models/model_projetos');
var Pessoas = require('../models/model_pessoas');
var Avaliacoes = require('../models/model_avaliacoes');
var Apresentacao = require('../models/model_apresentacoes');
var Pessoas_Projetos = require('../models/model_pessoasProjetos');
var Membros = require('../models/model_membros');
var { transaction } = require('objection');

exports.getProjetos = (filters = {}) => {
  if (filters.idCampus && filters.idAreaTematica)
    return Projetos.query()
      .joinRelation('unidade')
      .where('unidade.idCampus', filters.idCampus)
      .where('Projetos.idAreaTematica', filters.idAreaTematica);

  if (filters.idCampus)
    return Projetos.query()
      .joinRelation('unidade')
      .where('unidade.idCampus', filters.idCampus);

  return Projetos.query().where(q => {
    _.mapKeys(filters, (value, key) => {
      q.where(`Projetos.${key}`, value);
    });
  });
};

exports.getProjeto = idProjeto => {
  return Projetos.query()
    .where({ idProjeto })
    .then(e => e[0]);
};

exports.getProjetosPorStatus = ({ status }) => {
  // return Avaliacoes
  //   .query()
  //   .where({ idProjeto })
  //   .then(e => e[0])
};

// exports.postProjeto = async (data) => {
//   // const uniqueProjects = _.uniqBy(data, 'idProjeto')

//   const projeto = {
//     ...data,
//     idProjeto: iProjeto,
//   };

//   const response = await Projetos.query()
//     .insert(projeto)
//     .returning('*');

//   iProjeto++;

//   return response;
// };

exports.postProjeto = async (data) => {
  // Obter o último idProjeto cadastrado
  const lastProject = await Projetos.query()
    .select('idProjeto')
    .orderBy('idProjeto', 'desc')
    .first();

  // Incrementar o idProjeto
  const newIdProjeto = lastProject ? lastProject.idProjeto + 1 : 1; // Se não houver nenhum projeto, começa com 1

  // Criar o novo projeto com o idProjeto incrementado
  const projeto = {
    ...data,
    idProjeto: newIdProjeto,
  };

  // Inserir o novo projeto no banco de dados
  const response = await Projetos.query()
    .insert(projeto)
    .returning('*');

  return response;
};

exports.putProjeto = async ({ idProjeto }, data) => {
  // console.log('DATA: ', data);
  if (data.idApresentacao && data.id2Apresentacao && data.modalidadeProjeto) {
    modalidadeApresentacao = await Apresentacao.query()
      .select('modalidadeApresentacao')
      .where({ idApresentacao: data.idApresentacao, id2Apresentacao: data.id2Apresentacao })
      .then(el => el[0].modalidadeApresentacao);

    // modalidadeProjeto = await Projetos.query()
    // .select('modalidadeProjeto')
    // .where({ idProjeto })
    // .then(el => el[0].modalidadeProjeto)

    if (data.modalidadeProjeto != modalidadeApresentacao)
      throw new ApolloError(`Não é possível adicionar Projeto à Apresentacao com esta Modalidade`);
  } else if (data.idApresentacao && data.id2Apresentacao) {
    modalidadeApresentacao = await Apresentacao.query()
      .select('modalidadeApresentacao')
      .where({ idApresentacao: data.idApresentacao, id2Apresentacao: data.id2Apresentacao })
      .then(el => el[0].modalidadeApresentacao)
      .catch(e => {
        throw new ApolloError(`Apresentação não existe`)
      })

    modalidadeProjeto = await Projetos.query()
      .select('modalidadeProjeto')
      .where({ idProjeto })
      .then(el => el[0].modalidadeProjeto);

    if (modalidadeProjeto != modalidadeApresentacao)
      throw new ApolloError(`Não é possível adicionar Projeto à Apresentacao com esta Modalidade`);
  } else if (data.modalidadeProjeto) {
    // console.log('AQUIIII');
    const apresentacao = await Projetos.query()
      .select('idApresentacao', 'id2Apresentacao')
      .where({ idProjeto })
      .then(el => ({
        idApresentacao: el[0].idApresentacao,
        id2Apresentacao: el[0].id2Apresentacao
      }));
    // console.log('AP: ', apresentacao);
    if (apresentacao.idApresentacao && apresentacao.id2Apresentacao) {
      // console.log('ACHOU APRESENTACAO');
      modalidadeApresentacao = await Apresentacao.query()
        .select('modalidadeApresentacao')
        .where(apresentacao)
        .then(el => el[0].modalidadeApresentacao);

      if (data.modalidadeProjeto != modalidadeApresentacao)
        throw new ApolloError(`Não é possível alterar a modalidade do Projeto nesta Apresentação`);
    }
  }

  const projetoAlterado = await transaction(Projetos.knex(), async trx => {
    await Projetos.query(trx)
      .patch(data)
      .where({ idProjeto });
    const projeto = await Projetos.query(trx).where({ idProjeto });

    return projeto[0];
  });
  return projetoAlterado;
};

exports.delProjeto = ({ idProjeto }) => {
  return Projetos.query()
    .delete()
    .where(idProjeto);
};

exports.mediaProjeto = ({ idProjeto }) => {
  return Pessoas_Projetos.query()
    .avg('mediaProjeto as media')
    .where({ idProjeto })
    .then(e => e[0].media);
};

exports.mediaPorAvaliador = async ({ idProjeto }) => {
  return await Pessoas_Projetos.query()
    .eager('avaliador')
    .where({ idProjeto })
};

exports.mediaDeUmAvaliador = async ({ idProjeto, idPessoa, idVinculoPessoa }) => {
  const media = await Pessoas_Projetos.query()
    .select('mediaProjeto as media')
    .where({ idProjeto, idPessoa, idVinculoPessoa })
    .then(el => el[0])

  const pessoa = await Pessoas.query()
    .where({ idPessoa, idVinculoPessoa })
    .then(el => el[0]);

  return media == null ? null : { avaliador: pessoa, mediaProjeto: media.media };
};

exports.avaliadoresHabilitados = ({ idProjeto }) => {
  return Pessoas_Projetos.query()
    .joinRelation('avaliador')
    .select('avaliador.*')
    .where({ idProjeto, 'Pessoas_Projetos.aptidaoPessoa': true });
};

exports.getMembros = ({ idProjeto }) => {
  return Membros.query().where({ idProjeto });
};

exports.getAvaliacoes = async ({ idProjeto }) => {
  const avaliacoes = await Pessoas_Projetos.query()
    .eager('avaliacao')
    .where({ idProjeto })

  const formattedAvaliacao = avaliacoes
    .filter(el => el.avaliacao.length > 0)
    .map(el => ({
      idProjeto: el.idProjeto,
      idPessoa: el.idPessoa,
      idVinculoPessoa: el.idVinculoPessoa,
      media: el.mediaProjeto,
      avaliacaoManual: el.avaliacao.length > 0 ? el.avaliacao[0].avaliacaoManual : [],
      nomeAvaliadorPapel: el.avaliacao.length > 0 ? el.avaliacao[0].nomeAvaliadorPapel : [],
      notas: el.avaliacao.map(el => ({
        pergunta: el.idPergunta,
        nota: el.notaAvaliacao,
        data: el.dataAvaliacao,
        respostaTextual: el.respostaTextual
      }))
    }))
  return formattedAvaliacao

}
