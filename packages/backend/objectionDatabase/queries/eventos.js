const { ApolloError } = require('apollo-server');
const Eventos = require('../models/model_eventos');
const Projetos = require('../models/model_projetos');
const Configuracoes = require('../models/model_configuracoes');
const Horarios = require('../models/model_horarios');
const Apresentacoes = require('../models/model_apresentacoes');
const EventosCampus = require('../models/model_eventosCampus');
const Pessoas_Apresentacoes = require('../models/model_pessoasApresentacoes');
const AreasTematicas = require('../models/model_areasTematicas');
const knex = require('../../knexinstance');
// const Projetos = require("../models/model_projetos");
const _ = require('lodash');
var { transaction } = require('objection');
const moment = require('moment');
const momentTz = require('moment-timezone');
require('moment/locale/pt-br');
exports.getEventos = (filters = {}) => {
  return Eventos.query().where(q => {
    _.mapKeys(filters, (value, key) => {
      q.where(`Eventos.${key}`, value);
    });
  });
};

exports.getEvento = ({ idEvento }) => {
  return Eventos.query().findById(idEvento);
};

exports.getEventoByid = ids => {
  return Eventos.query().findByIds(ids);
};

exports.postEvento = async ({ areasTematicas, campi, evento }) => {
  return await transaction(Eventos.knex(), async trx => {
    // criando evento
    // const dataInicioSec = `${evento.dataInicioEvento}`
    // const dataFimSec = `${evento.dataFimEvento}`
    // evento.dataInicioEvento = dataInicioSec.substring(0, dataInicioSec.length - 3)
    // evento.dataFimEvento = dataFimSec.substring(0, dataFimSec.length - 3)
    const {
      rows: [eventoCriado]
    } = await knex.raw(
      `INSERT INTO public."Eventos"("nomeEvento","dataInicioEvento", "dataFimEvento", "descricaoEvento")
      VALUES (?,?,?,?) returning *`,
      [
        evento.nomeEvento,
        evento.dataInicioEvento,
        evento.dataFimEvento,
        evento.descricaoEvento ? evento.descricaoEvento : null
      ]
    );

    //criando evento_campus (tabela n-n)
    const evento_campus = campi.map(campus => ({
      idCampus: campus,
      idEvento: eventoCriado.idEvento
    }));
    await EventosCampus.query().insert(evento_campus);

    //criando configs para table de Configuracao
    const configs = areasTematicas.map(idAreaTematica => ({
      idAreaTematica,
      idEvento: eventoCriado.idEvento,
      configAtiva: true,
      quantidadeSalas: 5
    }));
    await Configuracoes.query(trx).insert(configs);
    return eventoCriado;
  });
};

exports.putEvento = async ({ areasTematicas, campi, idEvento, data }) => {
  return await transaction(Eventos.knex(), async trx => {
    if (areasTematicas) {
      const configsAntigas = await Configuracoes.query()
        .where({ idEvento })
        .first()
        .then(el => {
          if (el) return _.omit(el, ['idAreaTematica', 'idEvento', 'configAtiva']);
        });
      const areasParaAtivar = areasTematicas.map(idAreaTematica => ({
        idEvento,
        idAreaTematica,
        ...configsAntigas,
        configAtiva: true
      }));

      await Configuracoes.query()
        .patch({ configAtiva: false })
        .where({ idEvento });

      await knex.raw(
        `${knex('Configuracoes')
          .insert(areasParaAtivar)
          .toQuery()} ON CONFLICT ("idEvento", "idAreaTematica") DO UPDATE SET "configAtiva" = true`
      );
    }

    if (_.isEmpty(data)) return await Eventos.query().findById(idEvento);

    return await Eventos.query()
      .patch({ ...data })
      .findById(idEvento)
      .returning('*');
  });
};

exports.delEvento = async ({ idEvento }) => {
  return await transaction(Eventos.knex(), async trx => {
    const apresentacoes = await Apresentacoes.query()
      .select(['idApresentacao', 'id2Apresentacao'])
      .where({ idEvento })
      .map(el => [el.idApresentacao, el.id2Apresentacao]);
    await Configuracoes.query()
      .delete()
      .where({ idEvento });
    await Horarios.query()
      .delete()
      .where({ idEvento });
    await EventosCampus.query()
      .delete()
      .where({ idEvento });
    await Pessoas_Apresentacoes.query()
      .delete()
      .whereInComposite(['idApresentacao', 'id2Apresentacao'], apresentacoes);
    await Projetos.query()
      .update({ idApresentacao: null, id2Apresentacao: null })
      .whereInComposite(['idApresentacao', 'id2Apresentacao'], apresentacoes);

    const excluido = await Eventos.query()
      .delete()
      .findById(idEvento)
      .returning('*');

    if (_.isEmpty(excluido)) throw new ApolloError('Evento não existe.');
    else return excluido;
  });
};

exports.getCampi = async ({ idEvento }) => {
  return await EventosCampus.query()
    .joinRelation('campus')
    .select('campus.*')
    .where({ idEvento });
};

exports.getAreasTematicas = async ({ idEvento }) => {
  return await Configuracoes.query()
    .where({ idEvento, configAtiva: true })
    .eager('areaTematica')
    .map(el => el.areaTematica);
};

exports.alocarProjetos = async ({
  idEvento,
  eventoHorarioInicial,
  eventoHorarioFinal,
  idAreaTematica,
  numeroDeSalas,
  numeroProjetosSala,
  horarios,
  idCategoria,
}) => {
  return (apresentacoesPreenchidas = await transaction(Eventos.knex(), async trx => {
    const timeEventI = momentTz.tz(Number(eventoHorarioInicial), 'America/Recife');
    const timeEventF = momentTz.tz(Number(eventoHorarioFinal), 'America/Recife');
    // console.log(timeEventI.format('LLLL'));
    // console.log(timeEventF.format('LLLL'));
    for (let index = 0; index < horarios.length; index++) {
      // console.log('I: ', horarios[index]);
      const q = momentTz.tz(horarios[index], 'America/Recife');
      // console.log(q.format('LLLL'));
      if (!q.isBetween(timeEventI, timeEventF))
        throw new ApolloError(`Os Horario da Alocação não se adequam com a Data do Evento! ${q} ${timeEventI} ${timeEventF}`);
    }

    const campus = await EventosCampus.query()
      .joinRelation('campus')
      .select('campus.idCampus')
      .where({ idEvento })
      .map(el => el.idCampus);

    const nomeArea = await AreasTematicas.query()
      .select('nomeAreaTematica')
      .findById(idAreaTematica)
      .then(el => el.nomeAreaTematica);

    const projetosDoCampus = await Projetos.query()
      .joinRelation('unidade')
      .whereIn('unidade.idCampus', campus)
      .where({ idAreaTematica })
      .where({ modalidadeProjeto: 'T' })
      .map(({ idProjeto }) => idProjeto);
    if (projetosDoCampus.length == 0) {
      throw new ApolloError(`Evento ou Área Temática Inválida!`);
    }
    const projetos = projetosDoCampus.sort();

    nrSalasIdeal = projetosDoCampus.length / (numeroProjetosSala * horarios.length);

    if (nrSalasIdeal > numeroDeSalas)
      throw new ApolloError(
        `Numero de Salas ou Horarios Insuficientes. Número de Salas Ideal : ${Math.ceil(
          nrSalasIdeal
        )} `
      );

    var i_projeto = 0;
    var apresentacoesPreenchidas = [];
    aloc: while (i_projeto < projetos.length) {
      for (i = 0; i < horarios.length; i++) {
        for (j = 0; j < numeroDeSalas; j++) {
          const [
            ,
            {
              rows: [{ upper: codigoApresentacao }]
            }
          ] = await knex.raw(
            'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";select UPPER(LEFT(uuid_generate_v4()::text,8))'
          );
          const data = {
            id2Apresentacao: new Date().getFullYear(),
            salaApresentacao: `${nomeArea.slice(0, 3)}-${j + 1}`, //nome Area tematica
            horaApresentacao: horarios[i],
            disponibilidadeApresentacao: 0,
            codigoApresentacao: codigoApresentacao,
            modalidadeApresentacao: 'T',
            idAreaTematica,
            idEvento,
            idCategoria
          };
          const apresentacao = await Apresentacoes.query()
            .insert(data)
            .returning('*');

          for (k = 0; k < numeroProjetosSala; k++) {
            if (i_projeto >= projetos.length) {
              if (k == 0) {
                //se nao alocou projetos da o break
                break aloc;
              } else {
                // caso contrario adiciona a ultima no retorno
                apresentacoesPreenchidas.push(apresentacao);
                break aloc;
              }
            }
            const projetosModificados = await Projetos.query()
              .update({ ..._.pick(apresentacao, ['idApresentacao', 'id2Apresentacao']), idEvento })
              .where('idProjeto', projetos[i_projeto]);
            i_projeto++;
          }
          apresentacoesPreenchidas.push(apresentacao);
        }
      }
    }
    return apresentacoesPreenchidas;
  }));
};

 exports.alocarProjetosVideo = async  ({
  horarioInicial,
  numeroProjetosApresentacao,
  idAreaTematica,
 }) => {
  return (apresentacoesPreenchidas = await transaction(Eventos.knex(), async trx => {
    const idEvento = 1;

    const nomeArea = await AreasTematicas.query(trx)
      .select('nomeAreaTematica')
      .findById(idAreaTematica)
      .then(el => el.nomeAreaTematica);

    const projetos = (await Projetos.query(trx)
      .select()
      .where({ idCategoria: 2, idAreaTematica })
      .whereNotNull('linkArtefato')
      .map(({ idProjeto }) => idProjeto))
      .sort()

    let apresentacoes = [];
    for (i = 0; i < Math.ceil(projetos.length / numeroProjetosApresentacao); ++i) {
      const [
        ,
        {
          rows: [{ upper: codigoApresentacao }]
        }
      ] = await knex.raw(
        'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";select UPPER(LEFT(uuid_generate_v4()::text,8))'
      );

      const data = {
        id2Apresentacao: new Date().getFullYear(),
        salaApresentacao: `Vídeos ${i + 1} - ${nomeArea}`, //nome Area tematica
        horaApresentacao: horarioInicial,
        disponibilidadeApresentacao: 0,
        codigoApresentacao: codigoApresentacao,
        modalidadeApresentacao: 'T',
        idAreaTematica,
        idEvento,
        idCategoria: 2,
      };

      apresentacoes.push(
        await Apresentacoes.query(trx)
        .insert(data)
        .returning('*')
      );
    }

    for (i = 0; i < projetos.length; ++i) {
      const idxApresentacao = i % apresentacoes.length;
      const apresentacao = apresentacoes[idxApresentacao];

      await Projetos.query(trx)
        .update({ ..._.pick(apresentacao, ['idApresentacao', 'id2Apresentacao']), idEvento })
        .where('idProjeto', projetos[i]);
    }

    return apresentacoes;
  }));
};
