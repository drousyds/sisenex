const { Model } = require('objection');

class pessoas_apresentacoes extends Model {

  static get tableName() {
    return 'Pessoas_Apresentacoes';
  }

  static get idColumn() {
    return ['idPessoa','idVinculoPessoa','idApresentacao','id2Apresentacao'];
  }
  
  static get relationMappings() {
    const Pessoas = require('./model_pessoas');
    const Apresentacoes = require('./model_apresentacoes');

    return {
      pessoa: {
        relation: Model.BelongsToOneRelation,
        modelClass: Pessoas,
        join: {
          from: ['Pessoas_Apresentacoes.idPessoa','Pessoas_Apresentacoes.idVinculoPessoa'],
          to: ['Pessoas.idPessoa','Pessoas.idVinculoPessoa']
        }
      },
      apresentacao: {
        relation: Model.BelongsToOneRelation,
        modelClass: Apresentacoes,
        join: {
          from: ['Pessoas_Apresentacoes.idApresentacao','Pessoas_Apresentacoes.id2Apresentacao'],
          to: ['Apresentacoes.idApresentacao','Apresentacoes.id2Apresentacao']
        }
      }

    };
  }
}

module.exports = pessoas_apresentacoes;