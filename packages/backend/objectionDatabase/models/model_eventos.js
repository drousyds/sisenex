const { Model } = require('objection');
const Campus = require('./model_campus');

class Eventos extends Model {

  static get tableName() {
    return 'Eventos';
  }

  static get idColumn() {
    return 'idEvento';
  }

  // static get jsonSchema() {
  //   return {
  //     type: 'object',
  //     properties: {
  //       idEvento: { type: 'integer' },
  //       descricaoEvento: { type: 'string' },
  //       nomeEvento: { type: 'string' },
  //       localEvento: { type: 'string' },
  //       dataFimEvento: { type: 'string' },
  //       dataInicioEvento: { type: 'string' }
  //     }
  //   }
  // }
  static relationMappings() {
    return {
      campi: {
        relation: Model.ManyToManyRelation,
        modelClass: Campus,
        join: {
          from: 'Eventos.idEvento',
          through: {
            // persons_movies is the join table.
            from: 'Eventos_Campus.idEvento',
            to: 'Eventos_Campus.idCampus'
          },
          to: 'Campus.idCampus'
        }
      }
    }
  }

}

module.exports = Eventos;