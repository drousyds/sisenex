const { Model } = require('objection');

class Campus extends Model {

  static get tableName() {
    return 'Campus';
  }

  static get idColumn() {
    return 'idCampus';
  }

  // static get jsonSchema() {
  //   return {
  //     type: 'object',
  //     required: ['idCampus'],
  //     properties: {
  //       idCampus: { type: 'integer' }
  //     }
  //   }
  // }

  static get relationMappings() {
    const Unidades = require('./model_unidades');

    return {
      unidades: {
        relation: Model.HasManyRelation,
        modelClass: Unidades,
        join: {
          from: ['Campus.idCampus'],
          to: ['Unidades.idCampus']
        }
      }
    }
  }

}
module.exports = Campus;