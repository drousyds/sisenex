const { Model } = require('objection');

class AreasTematicas extends Model {

  static get tableName() {
    return 'AreasTematicas';
  }

  static get idColumn() {
    return 'idAreaTematica';
  }

  // static get jsonSchema(){
  //   return{
  //     type:'object',
  //     required:['idAreaTematica'],
  //     properties:{
  //       idAreaTematica:{type:'integer'},
  //       nomeAreaTematica:{type:'string',maxLength:56}
  //     }
  //   }
  // }

  static get relationMappings() {
    const Projetos = require('./model_projetos');

    return {
      projetos: {
        relation: Model.HasManyRelation,
        modelClass: Projetos,
        join: {
          from: 'AreasTematicas.idAreaTematica',
          to: 'Projetos.idAreaTematica'
        }
      }
    };
  }
}

module.exports = AreasTematicas;