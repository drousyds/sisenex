const { Model } = require('objection');

class Categorias extends Model {

  static get tableName() {
    return 'Categorias';
  }

  static get idColumn() {
    return 'idCategoria';
  }

  // static get jsonSchema(){
  //   return{
  //     type:'object',
  //     required:['idCategoria'],
  //     properties:{
  //       idCategoria:{type:'integer'},
  //       idPessoa:{type:'string'}
  //     }
  //   }
  // }

  static get relationMappings() {
    const Perguntas = require('./model_perguntas');
    const Pessoas = require('./model_pessoas');
    const Projetos = require('./model_projetos');
    const Apresentacoes = require('./model_apresentacoes');

    return {
      perguntas: {
        relation: Model.HasManyRelation,
        modelClass: Perguntas,
        join: {
          from: 'Categorias.idCategoria',
          to: 'Perguntas.idCategoria'
        }
      },
      pessoas: {
        relation: Model.HasManyRelation,
        modelClass: Pessoas,
        join: {
          from: 'Categorias.idCategoria',
          to: 'Pessoas.idCategoria'
        }
      },
      projetos: {
        relation: Model.HasManyRelation,
        modelClass: Projetos,
        join: {
          from: 'Categorias.idCategoria',
          to: 'Projetos.idCategoria'
        }
      },
      apresentacoes: {
        relation: Model.HasManyRelation,
        modelClass: Apresentacoes,
        join: {
          from: 'Categorias.idCategoria',
          to: 'Apresentacoes.idCategoria'
        }
      }
    };
  }
}

module.exports = Categorias;
