const { Model } = require('objection');

class Avaliacoes extends Model {

    static get tableName() {
        return 'Avaliacoes';
    }

    static get idColumn() {
        return ['idPergunta', 'idProjeto', 'idPessoa', 'idVinculoPessoa'];
    }

    static get modifiers(){
        return {
            orderByIdPergunta: (builder)=>{
                builder.orderBy('idPergunta');
            }
        }
    }

    $beforeValidate(jsonSchema,json,opt){
      console.log(json);
      console.log(jsonSchema);

      
      return jsonSchema
    }

    // static get jsonSchema(){
    //   return{
    //     type:'object',
    //     required:['idPergunta', 'idProjeto', 'idPessoa', 'idVinculoPessoa'],
    //     properties:{
    //       idPergunta:{type:'integer'},
    //       idProjeto:{type:'integer'},
    //       idPessoa:{type:'string',maxLength:'16'},
    //       idVinculoPessoa:{type:'string',maxLength:'16'},
    //       notaAvaliacao:{type:'integer'}
    //     }
    //   }
    // }

    static get relationMappings() {
        const Pessoas = require('./model_pessoas');
        const Projetos = require('./model_projetos');
        const Perguntas = require('./model_perguntas');

        return {
            avaliador: {
                relation: Model.BelongsToOneRelation,
                modelClass: Pessoas,
                join: {
                    from: ['Avaliacoes.idPessoa', 'Avaliacoes.idVinculoPessoa'],
                    to: ['Pessoas.idPessoa', 'Pessoas.idVinculoPessoa']
                }
            },
            projeto: {
                relation: Model.BelongsToOneRelation,
                modelClass: Projetos,
                join: {
                    from: 'Avaliacoes.idProjeto',
                    to: 'Projetos.idProjeto'
                }
            },
            pergunta: {
                relation: Model.BelongsToOneRelation,
                modelClass: Perguntas,
                join: {
                    from: 'Avaliacoes.idPergunta',
                    to: 'Perguntas.idPergunta'
                }
            },

        };
    }
}

module.exports = Avaliacoes;