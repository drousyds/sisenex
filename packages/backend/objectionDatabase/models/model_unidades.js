const { Model } = require('objection');

class Unidades extends Model {

  static get tableName() {
    return 'Unidades';
  }

  static get idColumn() {
    return 'idUnidade';
  }

  // static get jsonSchema(){
  //   return{
  //     type:'object',
  //     required:['idUnidade'],
  //     properties:{
  //       idUnidade:{type:'integer'},
  //       codigoUnidade:{type:'string',maxLength:45},
  //       nomeUnidade:{type:'string'},
  //       siglaUnidade:{type:'string',maxLength:45},
  //       hierarquiaUnidade:{type:'string',maxLength:45},
  //       unidadeGestora:{type:'integer'},
  //       tipoUnidade:{type:'string',maxLength:45}
  //     }
  //   }
  // }

  static get relationMappings() {
    const Projetos = require('./model_projetos');
    const Campus = require('./model_campus');

    return {
      projetos: {
        relation: Model.HasManyRelation,
        modelClass: Projetos,
        join: {
          from: 'Unidades.idUnidade',
          to: 'Projetos.idUnidade'
        }
      },
      campus: {
        relation: Model.BelongsToOneRelation,
        modelClass: Campus,
        join: {
          from: 'Unidades.idCampus',
          to: 'Campus.idCampus'
        }
      }
    };
  }
}

module.exports = Unidades;