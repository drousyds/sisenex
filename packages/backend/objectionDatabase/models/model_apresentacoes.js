const { Model } = require('objection');


class Apresentacoes extends Model {


  static get tableName() {
    return 'Apresentacoes';
  }


  static get idColumn() {
    return ['idApresentacao','id2Apresentacao'];
  }

  // static get jsonSchema(){
  //   return{
  //     type:'object',
  //     // required:['id2Apresentacao'],
  //     properties:{
  //       idApresentacao:{type:'integer'},
  //       id2Apresentacao:{type:'string',maxLength:5},
  //       salaApresentacao:{type:'string'},
  //       disponibilidadeApresentacao:{type:'integer'}
  //     }
  //   }
  // }

  static get relationMappings() {
    const Projetos = require('./model_projetos');
    const Pessoas_Apresentacoes = require('./model_pessoasApresentacoes');
    const Pessoas_Projetos = require('./model_pessoasProjetos');
    const Categorias = require('./model_categorias');
    const Eventos = require('./model_eventos');

    return {
      projetos: {
        relation: Model.HasManyRelation,
        modelClass: Projetos,
        join: {
          from: ['Apresentacoes.idApresentacao','Apresentacoes.id2Apresentacao'],
          to: ['Projetos.idApresentacao','Projetos.id2Apresentacao']
        }
      },
      pessoas_apresentacoes: {
        relation: Model.HasManyRelation,
        modelClass: Pessoas_Apresentacoes,
        join: {
          from: ['Apresentacoes.idApresentacao','Apresentacoes.id2Apresentacao'],
          to: ['Pessoas_Apresentacoes.idApresentacao','Pessoas_Apresentacoes.id2Apresentacao']
        }
      },
      evento: {
        relation: Model.BelongsToOneRelation,
        modelClass: Eventos,
        join: {
          from: 'Apresentacoes.idEvento',
          to: 'Eventos.idEvento'
        }
      },
      categoria: {
        relation: Model.BelongsToOneRelation,
        modelClass: Categorias,
        join: {
          from: 'Apresentacoes.idCategoria',
          to: 'Categorias.idCategoria'
        }
      }
    //   ,
    //   projetos_pessoas_apresentacoes: {
    //     relation: Model.HasManyRelation,
    //     modelClass: Pessoas_Projetos,
    //     join: {
    //       from: ['Apresentacoes.idApresentacao','Apresentacoes.id2Apresentacao'],
    //       to: ['Pessoas_Projetos.idApresentacao','Pessoas_Projetos.id2Apresentacao']
    //     }
    //   }
    };
  }
}

module.exports = Apresentacoes;
