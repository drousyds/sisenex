const { Model } = require('objection');

class Pessoas extends Model {
    //TODO: Everything
    static get tableName() {
        return 'Pessoas';
    }

    static get idColumn() {
        return ['idPessoa', 'idVinculoPessoa'];
    }

    // static get jsonSchema(){
    //   return{
    //     type:'object',
    //     required:['idPessoa', 'idVinculoPessoa'],
    //     properties:{
    //       idPessoa:{type:'string',maxLength:'16'},
    //       idVinculoPessoa:{type:'string',maxLength:'16'},
    //       matriculaPessoa:{type:'string',maxLength:'45'},
    //       nomePessoa:{type:'string'},
    //       nomeSocialPessoa:{type:'string'},
    //       lotacaoPessoa:{type:'string'},
    //       emailPessoa:{type:'string',maxLength:'45'},
    //       telefonePessoa:{type:'string',maxLength:'32'},
    //       aptidaoPessoa:{type:'integer'},
    //       avaliadorPessoa:{type:'integer'},
    //       monitorPessoa:{type:'integer'},
    //       gerentePessoa:{type:'integer'},
    //     }
    //   }
    // }

    static get relationMappings() {
        const Avaliacoes = require('./model_avaliacoes');
        const Pessoas_Apresentacoes = require('./model_pessoasApresentacoes');
        const Pessoas_Projetos = require('./model_pessoasProjetos');
        const Categorias = require('./model_categorias');
        const SolicitacaoLGPD = require('./model_solicitacaoLGPD');

        return {
            avaliacoes: {
                relation: Model.HasManyRelation,
                modelClass: Avaliacoes,
                join: {
                    from: ['Pessoas.idPessoa', 'Pessoas.idVinculoPessoa'],
                    to: ['Avaliacoes.idPessoa', 'Avaliacoes.idVinculoPessoa']
                }
            },
            pessoas_apresentacoes: {
                relation: Model.HasManyRelation,
                modelClass: Pessoas_Apresentacoes,
                join: {
                    from: ['Pessoas.idPessoa', 'Pessoas.idVinculoPessoa'],
                    to: ['Pessoas_Apresentacoes.idPessoa', 'Pessoas_Apresentacoes.idVinculoPessoa']
                }
            },
            categorias: {
                relation: Model.BelongsToOneRelation,
                modelClass: Categorias,
                join: {
                    from: 'Pessoas.idCategoria',
                    to: 'Categorias.idCategoria'
                }
            },
            solicitacoes_LGPD: {
              relation: Model.HasManyRelation,
              modelClass: SolicitacaoLGPD,
              join: {
                from: ['Pessoas.idPessoa','Pessoas.idVinculoPessoa'],
                to : ['SolicitacaoLGPD.idPessoa','SolicitacaoLGPD.idVinculoPessoa']
              }
            }
            // ,
            // projetos_pessoas_apresentacoes: {
            //     relation: Model.HasManyRelation,
            //     modelClass: Pessoas_Projetos,
            //     join: {
            //         from: ['Pessoas.idPessoa', 'Pessoas.idVinculoPessoa'],
            //         to: ['Pessoas_Projetos.idPessoa', 'Pessoas_Projetos.idVinculoPessoa']
            //     }
            // }

        };
    }
}

module.exports = Pessoas;
