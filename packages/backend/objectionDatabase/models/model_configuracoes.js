const { Model } = require('objection');

class Configuracoes extends Model {

  static get tableName() {
    return 'Configuracoes';
  }

  static get idColumn() {
    return ['idEvento', 'idAreaTematica'];
  }

  // static get jsonSchema() {
  //   return {
  //     type: 'object',
  //     properties: {
  //       idEvento: { type: 'integer' },
  //       idAreaTematica: { type: 'integer' },
  //       quantidadeSalas: { type: 'integer', maxLength: 1 },
  //       quantidadeApresentacoes: { type: 'integer', maxLength: 1 }
  //     }
  //   }
  // }

  static get relationMappings() {
    const Areas_Tematicas = require('./model_areasTematicas');
    const Eventos = require('./model_eventos');

    return {
      evento: {
        relation: Model.BelongsToOneRelation,
        modelClass: Eventos,
        join: {
          from: 'Configuracoes.idEvento',
          to: 'Eventos.idEvento'
        }
      },
      areaTematica: {
        relation: Model.BelongsToOneRelation,
        modelClass: Areas_Tematicas,
        join: {
          from: 'Configuracoes.idAreaTematica',
          to: 'AreasTematicas.idAreaTematica'
        }
      }
    };
  }

}

module.exports = Configuracoes;