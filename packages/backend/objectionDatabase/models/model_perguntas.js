const { Model } = require('objection');

class Perguntas extends Model {

  static get tableName() {
    return 'Perguntas';
  }

  static get idColumn() {
    return 'idPergunta';
  }

  static get jsonSchema(){
    return{
      type:'object',
      required:['idPergunta'],
      properties:{
        idCategoria:{type:'integer'},
        conteudoPergunta:{type:'string'}
      }
    }
  }

  static get relationMappings() {
    const Categorias = require('./model_categorias');

    return {
      categoria: {
        relation: Model.BelongsToOneRelation,
        modelClass: Categorias,
        join: {
          from: 'Perguntas.idCategoria',
          to: 'Categorias.idCategoria'
        }
      }
    };
  }
}

module.exports = Perguntas;