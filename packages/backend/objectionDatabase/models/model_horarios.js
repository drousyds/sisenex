const { Model } = require('objection');

class Configuracoes extends Model {

  static get tableName() {
    return 'Configuracoes';
  }

  static get idColumn() {
    return ['idEvento', 'idAreaTematica'];
  }

  // static get jsonSchema() {
  //   return {
  //     type: 'object',
  //     properties: {
  //       idEvento: { type: 'integer' },
  //       idAreaTematica: { type: 'integer' },
  //       quantidadeSalas: { type: 'integer', maxLength: 1 },
  //       quantidadeApresentacoes: { type: 'integer', maxLength: 1 }
  //     }
  //   }
  // }

}

module.exports = Configuracoes;