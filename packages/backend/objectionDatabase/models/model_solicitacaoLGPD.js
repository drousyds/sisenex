const { Model } = require('objection');

class SolicitacaoLGPD extends Model {

  static get tableName() {
    return 'SolicitacaoLGPD';
  }

  static get idColumn() {
    return 'idSolicitacao';
  }

  // static get jsonSchema(){
  //   return{
  //     type:'object',
  //     properties:{
  //       idSoliticacaoLGPD{type:'integer'},
  //       tipoSolicitacao:{[type:'interger','null']},
  //       textoSolicitacao:{type:'string',maxLength:'4096'},
  //       idPessoa:{type:'string',maxLength:'16'},
  //       idVinculoPessoa{type:'string',maxLength:'16'},
  //       nomeSolicitacao{type:'string'},
  //       sobrenomeSoliticacao{type:'string'},
  //       emailPessoa{type:'string',maxLength:'45'},
  //     }
  //   }
  // }

  static get relationMappings() {
    const Pessoas = require('./model_pessoas');

    return {
      solicitacoes: {
        relation: Model.BelongsToOneRelation,
        modelClass: Pessoas,
        join: {
          from: ['SolicitacaoLGPD.idPessoa','SolicitacaoLGPD.idVinculoPessoa'],
          to: ['Pessoas.idPessoa', 'Pessoas.idVinculoPessoa']
        }
      }
    }
  }

}
module.exports = SolicitacaoLGPD;
