const { Model } = require('objection');

class Reports extends Model {

  static get tableName() {
    return 'Reports';
  }

  static get idColumn() {
    return 'idReport';
  }

  // static get jsonSchema() {
  //   return {
  //     type: 'object',
  //     required: ['idReport'],
  //     properties: {
  //       idReport: { type: 'integer' },
  //       idPessoa: { type: 'string', maxLength: '16' },
  //       idVinculoPessoa: { type: 'string', maxLength: '16' },
  //       idPessoaResposta: { type: 'string', maxLength: '16' },
  //       idVinculoPessoaResposta: { type: 'string', maxLength: '16' },
  //       horaReport: { type: 'string', maxLength: 45 },    //datetime
  //       horaReportResposta: { type: 'string', maxLength: 45 },    //datetime
  //       report: { type: 'string' }
  //     }
  //   }
  // }

}

module.exports = Reports;