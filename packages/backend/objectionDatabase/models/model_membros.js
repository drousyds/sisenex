const { Model } = require('objection');

class Membros extends Model {

  static get tableName() {
    return 'Membros_Extensao';
  }

  static get idColumn() {
    return ['idProjeto'];
  }

  static get relationMappings() {
    const Projetos = require('./model_projetos');

    return {
      projeto: {
        relation: Model.BelongsToOneRelation,
        modelClass: Projetos,
        join: {
          from: 'Membros_Extensao.idProjeto',
          to: 'Projetos.idProjeto'
        }
      }

    };
  }
}

module.exports = Membros;