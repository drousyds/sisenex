const { Model } = require('objection');

class eventos_campus extends Model {

  static get tableName() {
    return 'Eventos_Campus';
  }

  static get idColumn() {
    return ['idEvento','idCampus'];
  }
  
  static get relationMappings() {
    const Eventos = require('./model_eventos');
    const Campus = require('./model_campus');

    return {
      eventos: {
        relation: Model.BelongsToOneRelation,
        modelClass: Eventos,
        join: {
          from: ['Eventos_Campus.idEvento'],
          to: ['Eventos.idEvento']
        }
      },
      campus: {
        relation: Model.BelongsToOneRelation,
        modelClass: Campus,
        join: {
          from: ['Eventos_Campus.idCampus'],
          to: ['Campus.idCampus']
        }
      }
    };
  }
}

module.exports = eventos_campus;