const { Model } = require('objection');
var _ = require('lodash')

class Projetos extends Model {

  static get tableName() {
    return 'Projetos';
  }

  static get idColumn() {
    return 'idProjeto';
  }

  // static get jsonSchema(){
  //   return{
  //     type:'object',
  //     properties:{
  //       idProjeto:{type:'integer'},
  //       tituloProjeto:{type:'string',maxLength:'512'},
  //       descricaoProjeto:{type:'string',maxLength:'32'},
  //       dataInicioProjeto:{type:'string'},
  //       dataFimProjeto:{type:'string'},
  //       anoProjeto:{type:'string'},
  //       dataApresentacaoProjeto:{type:['integer','null']},
  //       idAreaTematica:{type:'integer'},
  //       idUnidade:{type:'integer'},
  //       campusProjeto:{type:'integer'},
  //       idApresentacao:{type:['integer','null']},
  //       id2Apresentacao:{type:['string','null']}
  //     }
  //   }
  // }

  static get modifiers() {
    return {
      orderByIdProjeto: (builder) => {
        builder.orderBy('idProjeto');
      }
    }
  }

  $formatJson(json) {
    // Remember to call the super class's implementation.
    json = super.$formatJson(json);
    // Do your conversion here.

    if (json.avaliacoes && json.avaliacoes.length > 0) {
      var avaliacoesPorPessoa = [];
      _.chain(json.avaliacoes)
        .groupBy("idPessoa")
        .mapKeys((value, key) => {

          avaliacoesPorPessoa.push({
            'idPessoa': key,
            'idVinculoPessoa': value[0].idVinculoPessoa,
            'avaliacao': []
          });
          value.map(pergunta => {
            avaliacoesPorPessoa[avaliacoesPorPessoa.length - 1].avaliacao.push({
              'idPergunta': pergunta.idPergunta,
              'notaAvaliacao': pergunta.notaAvaliacao,
              'dataAvaliacao': pergunta.dataAvaliacao
            })
          })
        })
        .value();
      json.avaliacoes = avaliacoesPorPessoa;
    }

    return json;
  }

  static get relationMappings() {
    const Avaliacoes = require('./model_avaliacoes');
    const AreasTematicas = require('./model_areasTematicas');
    const Unidades = require('./model_unidades');
    const Apresentacoes = require('./model_apresentacoes');
    const Membros_Extensao = require('./model_membros');
    const Categorias = require('./model_categorias');

    return {
      avaliacoes: {
        relation: Model.HasManyRelation,
        modelClass: Avaliacoes,
        join: {
          from: 'Projetos.idProjeto',
          to: 'Avaliacoes.idProjeto'
        }
      },
      areaTematica: {
        relation: Model.BelongsToOneRelation,
        modelClass: AreasTematicas,
        join: {
          from: 'Projetos.idAreaTematica',
          to: 'AreasTematicas.idAreaTematica'
        }
      },
      unidade: {
        relation: Model.BelongsToOneRelation,
        modelClass: Unidades,
        join: {
          from: 'Projetos.idUnidade',
          to: 'Unidades.idUnidade'
        }
      },
      apresentacao: {
        relation: Model.BelongsToOneRelation,
        modelClass: Apresentacoes,
        join: {
          from: ['Projetos.idApresentacao', 'Projetos.id2Apresentacao'],
          to: ['Apresentacoes.idApresentacao', 'Apresentacoes.id2Apresentacao']
        }
      },
      coordenadores: {
        relation: Model.HasManyRelation,
        modelClass: Membros_Extensao,
        join: {
          from: ['Projetos.idProjeto'],
          to: ['Membros_Extensao.idProjeto']
        }
      },
      categorias: {
        relation: Model.BelongsToOneRelation,
        modelClass: Categorias,
        join: {
          from: 'Projetos.idCategoria',
          to: 'Categorias.idCategoria'
        }
      }
      // pessoas_projetos: {
      //     relation: Model.BelongsToOneRelation,
      //     modelClass: Pessoas_Projetos,
      //     join: {
      //         from: ['Projetos.idApresentacao', 'Projetos.id2Apresentacao'],
      //         to: ['Pessoas_Projetos.idApresentacao', 'Pessoas_Projetos.id2Apresentacao']
      //     }
      // }
    };
  }
}

module.exports = Projetos;
