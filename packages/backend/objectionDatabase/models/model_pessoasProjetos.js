const { Model } = require('objection');

class pessoas_projetos extends Model {

  static get tableName() {
    return 'Pessoas_Projetos';
  }

  static get idColumn() {
    return ['idPessoa', 'idVinculoPessoa', 'idProjeto'];
  }

  static get relationMappings() {
    const Pessoas = require('./model_pessoas');
    const Avaliacoes = require('./model_avaliacoes');
    const Apresentacoes = require('./model_apresentacoes');
    const Projetos = require('./model_projetos');

    return {
      avaliador: {
        relation: Model.BelongsToOneRelation,
        modelClass: Pessoas,
        join: {
          from: ['Pessoas_Projetos.idPessoa', 'Pessoas_Projetos.idVinculoPessoa'],
          to: ['Pessoas.idPessoa', 'Pessoas.idVinculoPessoa']
        }
      },
      projeto: {
        relation: Model.BelongsToOneRelation,
        modelClass: Projetos,
        join: {
          from: 'Pessoas_Projetos.idProjeto',
          to: 'Projetos.idProjeto'
        }
      },
      avaliacao: {
        relation: Model.HasManyRelation,
        modelClass: Avaliacoes,
        join: {
          from: ['Pessoas_Projetos.idPessoa', 'Pessoas_Projetos.idVinculoPessoa', 'Pessoas_Projetos.idProjeto'],
          to: ['Avaliacoes.idPessoa', 'Avaliacoes.idVinculoPessoa', 'Avaliacoes.idProjeto']
        }
      }

    };
  }
}

module.exports = pessoas_projetos;