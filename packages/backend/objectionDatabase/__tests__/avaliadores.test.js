// Setup
const { Model } = require("objection");
const Knex = require("knex");
const knexConfig = require("../../knexfile");
let knex = undefined;

// Queries
const QueryPessoa       = require("../queries/pessoas.js");
const QueryApresentacao = require("../queries/apresentacoes.js");
const QueryCategoria    = require("../queries/categorias.js"); 
const QueryMonitores    = require("../queries/monitores.js");
const QueryProjetos     = require("../queries/projetos.js");
const QueryAvaliadores  = require("../queries/avaliadores.js");

async function clearDb() {
  await knex.raw('TRUNCATE TABLE "AreasTematicas" CASCADE;');
  await knex.raw('TRUNCATE TABLE "Unidades" CASCADE;');
  await knex.raw('TRUNCATE TABLE "Campus" CASCADE;');

  await knex.raw('TRUNCATE TABLE "Perguntas" CASCADE;');
  await knex.raw('ALTER SEQUENCE "Perguntas_idPergunta_seq" RESTART;');

  await knex.raw('TRUNCATE TABLE "Pessoas" CASCADE;');
  await knex.raw('TRUNCATE TABLE "Apresentacoes" CASCADE;');
  await knex.raw('ALTER SEQUENCE "Apresentacoes_idApresentacao_seq" RESTART;');

  await knex.raw('TRUNCATE TABLE "Categorias" CASCADE;');
  await knex.raw('ALTER SEQUENCE "Categorias_idCategoria_seq" RESTART;');

  await knex.raw('TRUNCATE TABLE "Projetos" CASCADE;');

  await knex.raw('TRUNCATE TABLE "Avaliacoes" CASCADE;');
}

const campus1 = {
  idCampus: 1,
  nomeCampus: "campus1",
};

const unidade1 = {
  idUnidade: 1,
  codigoUnidade: "1",
  nomeUnidade: "unidade1",
  siglaUnidade: "UNI1",
  hierarquiaUnidade: "q?",
  tipoUnidade: "UNITIPO",
  unidadeGestora: 1,
  idCampus: "1",
};

const areaTematica1 = {
  idAreaTematica: 1,
  nomeAreaTematica: "area tematica 1",
};

const avaliador1 = {
  idPessoa: "1",
  idVinculoPessoa: "2",
  nomeSocialPessoa: "Pessoa Test",
  avaliadorPessoa: 1,
};

const categoria1 = {
  nomeCategoria: "Categoria 1", 
};

const apresentacao1 = {
  id2Apresentacao: "2019",
  salaApresentacao: "sala1",
  horaApresentacao: "2020-01-01 12:00:00",
  idCategoria: 1,
  disponibilidadeApresentacao: 1
};

const projeto1 = {
  idProjeto: 1,
  tituloProjeto: 'Curso de Curso',
  descricaoProjeto: 'CURSO',
  dataInicioProjeto: '2018-10-22',
  dataFimProjeto: '2018-10-23',
  anoProjeto: '2018-01-01',
  idAreaTematica: 1,
  idUnidade: 1,
  modalidadeProjeto: 'P',
  idCategoria: 1,
};

let perguntas = [
  { conteudoPergunta: "pergunta1" },
  { conteudoPergunta: "pergunta2" },
  { conteudoPergunta: "pergunta3" },
  { conteudoPergunta: "pergunta4" },
];

beforeAll(async () => {
  knex = Knex(knexConfig[process.env.NODE_ENV]);
  Model.knex(knex);
});

beforeEach(async () => await clearDb());

afterAll(async () => { 
  await clearDb();
  await knex.destroy();
});

async function insertDados() {
  await knex.insert(campus1).into('Campus');
  await knex.insert(unidade1).into('Unidades');
  await knex.insert(perguntas).into('Perguntas');
  await knex.insert(areaTematica1).into('AreasTematicas');

  await QueryCategoria.postCategoria(categoria1);
}

async function insertPessoa(pessoa) {
  await QueryPessoa.postPessoa({ input: pessoa });
}

async function liberareAbaterAvaliador(avaliador, projeto) {
  await QueryMonitores.baterAvaliador({
    idPessoa: avaliador.idPessoa,
    idVinculoPessoa: avaliador.idVinculoPessoa,
    idApresentacao: projeto.idApresentacao,
    id2Apresentacao: projeto.id2Apresentacao,
  });

  await QueryMonitores.liberarProjetoParaUmAvaliador({
    idProjeto: projeto.idProjeto,
    idPessoa: avaliador.idPessoa,
    idVinculoPessoa: avaliador.idVinculoPessoa,
  });
}

async function inicializarProjetoApresentacao(apresentacaoIn, projetoIn) {
  const apresentacao = await QueryApresentacao.postApresentacoes(apresentacaoIn);
  let projetoOut = {
    idApresentacao: apresentacao.idApresentacao,
    id2Apresentacao: apresentacao.id2Apresentacao,
    ...projetoIn
  };
  await QueryProjetos.postProjeto(projetoOut);
  return projetoOut
}

const avaliacao = {
  idPessoa: avaliador1.idPessoa,
  idVinculoPessoa: avaliador1.idVinculoPessoa,
  notas: [
    {
      idPergunta: 1,
      nota: 10.0,
    },
    {
      idPergunta: 2,
      nota: 4.0,
    },
    {
      idPergunta: 3,
      nota: 8.0,
    },
  ]
};

test('avaliar projeto', async () => {
  await insertDados();
  await insertPessoa(avaliador1);

  const projeto = 
    await inicializarProjetoApresentacao(apresentacao1, projeto1);

  await liberareAbaterAvaliador(avaliador1, projeto);

  let res = await QueryAvaliadores.avaliarProjeto({
    idPessoa: avaliador1.idPessoa,
    idVinculoPessoa: avaliador1.idVinculoPessoa,
    avaliacao: { idProjeto:projeto.idProjeto , ...avaliacao },
  });

  expect(res).toBe(true);
});

test('media avaliacao', async () => {
  await insertDados();
  await insertPessoa(avaliador1);

  const projeto = 
    await inicializarProjetoApresentacao(apresentacao1, projeto1);

  await liberareAbaterAvaliador(avaliador1, projeto);

  await QueryAvaliadores.avaliarProjeto({
    idPessoa: avaliador1.idPessoa,
    idVinculoPessoa: avaliador1.idVinculoPessoa,
    avaliacao: { idProjeto:projeto.idProjeto , ...avaliacao },
  });

  const [{ mediaProjeto }] = await knex('Pessoas_Projetos').select('mediaProjeto');

  let expectMedia = avaliacao.notas.reduce((acc, { nota }) => acc += nota, 0)
  expectMedia /= avaliacao.notas.length

  expect(mediaProjeto).toEqual(expectMedia);
})
