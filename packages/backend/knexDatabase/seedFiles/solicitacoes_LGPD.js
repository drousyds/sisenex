module.exports = String.raw`
INSERT INTO "SolicitacaoLGPD" ("tipoSolicitacao", "textoSolicitacao", "idPessoa", "idVinculoPessoa", "nomeSolicitacao", 
"sobrenomeSolicitacao", "emailPessoa","statusSolicitacao")
VALUES 
(4,'Testando essa feature das solicitacoes',1,2,'Fulano','Da Silva','fulano1@example.com','ABERTA'),
(1,NULL,2,3,'Cicrano','Pereira','Cicrano1@example.com','ABERTA'),
(2,NULL,4,5,'Beltrano','Santos','Beltrano1@example.com','ABERTA'),
(3,NULL,5,6,'Deuteranio','Oliveira','Deuteranio1@example.com','ABERTA'),
(1,NULL,6,7,'Trajanio','Gomes','Trajano1@example.com','RESPONDIDA'),
(3,NULL,8,9,'Dagoberto','Rodrigues','Dagoberto123@example.com','RESOLVIDA'),
(4,'Testando essa feature das solicitacoes',9,10,'Oliviano','Da Silva','Oliviano1@example.com','RESOLVIDA'),
(4,'Testando essa feature das solicitacoes',11,12,'Josiscleison','Ferreira','josiscleison1@example.com','RESPONDIDA'),
(3,NULL,12,13,'Ariverton','Souza','Ari1234@example.com','RESOLVIDA'),
(2,NULL,13,14,'Sonisclelson','Alves','SoniAlves@example.com','ARQUIVADA'),
(4,'Testando essa feature das solicitacoes',14,15,'Gilmardison','Ventura','GilmardisonVent@example.com','ARQUIVADA'),
(2,NULL,15,16,'Antonisson','Texouto','Antex1@example.com','RESOLVIDA'),
(2,NULL,16,17,'Josemar','Pontes','Josepon@example.com','ARQUIVADA'),
(1,NULL,17,18,'Sandro','Sales','sandroSales1@example.com','ARQUIVADA'),
(1,NULL,18,19,'Andre','lima','andlima@example.com','RESPONDIDA'),
(3,NULL,NULL,NULL,'Petrucio','Ribeiro','PetruBibeiro1@example.com','RESPONDIDA');
`;