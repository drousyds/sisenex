module.exports = String.raw` insert into "Perguntas" ("conteudoPergunta", "idCategoria", "dataPergunta") values
('Os resumos expandidos submetidos demonstram coerência e alinhamento com o projeto apresentado\?',3, '2024-9-18'),
('Demonstrou a interação de profissionais e/ou estudantes de outras áreas, estimulando à prática do diálogo interdisciplinar, multidisciplinar e interprofissional\?', 3, '2020-9-20'),
('Demonstrou como o projeto foi/vem sendo executado\?', 3, '2020-9-20'),
('Caracterizou o público-alvo do projeto e como tem sido a interação com esse público\?', 3, '2020-9-20'),
('Demonstrou coerência entre os objetivos e fundamentação teórica/metodológica do projeto\?', 3, '2020-9-20'),
('Demonstrou a contribuição da experiência extensionista na formação integral (profissional e cidadã) do discente (protagonismo discente na organização das atividades, na participação no planejamento, nas etapas do projeto e na interação com o público-alvo\?)', 3, '2020-9-20'),
('Demonstrou o impacto social do projeto no tocante ao compromisso com a transformação de problemática social e sua contribuição para empoderamento do cidadão, grupos, instituições, ONG''s e Organizações Sociais\?', 3, '2020-9-20'),
('Demonstrou como foi/está sendo o processo de autoavaliação e monitoramento das atividades do projeto\?', 3, '2020-9-20'),
('Demonstrou os desafios na realização do projeto, como garantiu/tem garantido sua exequibilidade em nível satisfatório\?', 3, '2020-9-20'),
('Demonstrou as formas de comunicação e difusão do projeto para as comunidades interna e externa\?', 3, '2020-9-20'),
('Comentários Gerais sobre o projeto apresentado. (Opcional)', 3, '2020-9-20');
`;