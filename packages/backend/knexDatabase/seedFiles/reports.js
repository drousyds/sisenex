module.exports = `insert into "Reports" ("idPessoa", "idVinculoPessoa", "idApresentacao", "id2Apresentacao", "conteudoReport", "statusReport",
"emailReport") values
(1,2,1,2019,'Problema 1','ABERTA', 'fulano1@example.com'),
(2,3,1,2019,'Problema 2','EM ANDAMENTO', 'fulano2@example.com'),
(1,2,2,2019,'Problema 3','CONCLUIDA', 'pessoa1@elpmaxe.org'),
(1,2,2,2019,'Problema 4','EM ANDAMENTO', 'pessoa2@elpmaxe.org'),
(3,4,1,2019,'Problema 5','EM ANDAMENTO', 'pessoa3@elpmaxe.org'),
(2,3,1,2019,'Problema 6','CONCLUIDA', 'fulano3@example.com'),
(2,3,1,2019,'Problema 7','EM ANDAMENTO', 'fulano4@example.com'),
(1,2,3,2019,'Problema 8','ABERTA', 'teste1@placeholder.ex'),
(1,2,2,2019,'Problema 9','CONCLUIDA', 'teste2@placeholder.ex'),
(1,2,2,2019,'Problema 10','ABERTA', 'teste3@placeholder.ex'),
(1,2,4,2019,'Problema 11','CONCLUIDA', 'pessoa4@elpmaxe.org');`;
