const unidades = require('../seedFiles/unidades')

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('Unidades').del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(unidades);
    })
};
