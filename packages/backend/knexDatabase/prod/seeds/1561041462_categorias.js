const categorias = require('../seedFiles/categorias')

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('Categorias').del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(categorias);
    })
};
