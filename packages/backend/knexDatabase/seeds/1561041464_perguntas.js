const perguntas = require('../seedFiles/perguntas')

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('Perguntas').del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(perguntas);
    })
};
