const areaTematica = require('../seedFiles/areaTematica')

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('AreasTematicas').del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(areaTematica);
    })
};
