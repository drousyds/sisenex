const solicitacoes = require("../seedFiles/solicitacoes_LGPD");

exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex("SolicitacaoLGPD")
      .del()
      .then(function () {
        // Inserts seed entries
        return knex.raw(solicitacoes);
      });
  };