const {updateProjects } = require('../seedFiles/projetos')

exports.seed = async function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('Projetos').del()
    .then(async() => {
      // Inserts seed entries
      return await updateProjects();
      // return knex.raw(projetos);
    })
};
