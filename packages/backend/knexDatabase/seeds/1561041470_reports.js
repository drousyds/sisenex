const reports = require("../seedFiles/reports");

exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex("Reports")
    .del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(reports);
    });
};
