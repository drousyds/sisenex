const pessoas_projetos = require('../seedFiles/pessoas_projetos')

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('Pessoas_Projetos').del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(pessoas_projetos);
    })
};
