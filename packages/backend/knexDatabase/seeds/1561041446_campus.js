const campus = require('../seedFiles/campus')

exports.seed = async function(knex, Promise) {
  // Deletes ALL existing entries

  await knex.raw('TRUNCATE TABLE "Pessoas" CASCADE;')
  await knex.raw('TRUNCATE TABLE "Unidades" CASCADE;')
  await knex.raw('TRUNCATE TABLE "Categorias" CASCADE;')
  await knex.raw('TRUNCATE TABLE "Perguntas" CASCADE;')
  await knex.raw('TRUNCATE TABLE "Eventos_Campus" CASCADE;')
  await knex.raw('TRUNCATE TABLE "Projetos" CASCADE;')
  await knex.raw('TRUNCATE TABLE "Membros_Extensao" CASCADE;')
  await knex.raw('TRUNCATE TABLE "Pessoas_Projetos" CASCADE;')
  await knex.raw('TRUNCATE TABLE "Pessoas_Apresentacoes" CASCADE;')
  await knex.raw('ALTER SEQUENCE "Apresentacoes_idApresentacao_seq" RESTART;')
  await knex.raw('ALTER SEQUENCE "Categorias_idCategoria_seq" RESTART;');
  await knex.raw('ALTER SEQUENCE "Eventos_idEvento_seq" RESTART;')
  await knex.raw('ALTER SEQUENCE "Perguntas_idPergunta_seq" RESTART;')
  await knex.raw('ALTER SEQUENCE "Reports_idReport_seq" RESTART;')
  return knex('Campus').del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(campus);
    })
};
