const membros_extensao = require('../seedFiles/membros_extensao')

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('Membros_Extensao').del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(membros_extensao);
    })
};
