const pessoas = require("../seedFiles/pessoas");

exports.seed = async function (knex, Promise) {
  // Deletes ALL existing entries
  await knex.raw('TRUNCATE TABLE "Pessoas" CASCADE;');
  await knex.raw('TRUNCATE TABLE "Reports" CASCADE;');
  await knex.raw('TRUNCATE TABLE "Unidades" CASCADE;');
  await knex.raw('TRUNCATE TABLE "Perguntas" CASCADE;');
  await knex.raw('TRUNCATE TABLE "Eventos_Campus" CASCADE;');
  await knex.raw('TRUNCATE TABLE "Configuracoes" CASCADE;');
  await knex.raw('TRUNCATE TABLE "Reports" CASCADE;');
  await knex.raw('ALTER SEQUENCE "Apresentacoes_idApresentacao_seq" RESTART;');
  await knex.raw('ALTER SEQUENCE "Eventos_idEvento_seq" RESTART;');
  await knex.raw('ALTER SEQUENCE "Perguntas_idPergunta_seq" RESTART;');
  await knex.raw('ALTER SEQUENCE "Reports_idReport_seq" RESTART;');
  return await knex("Pessoas")
    .then(function () {
      // Inserts seed entries
      return knex.raw(pessoas);
    });
};
