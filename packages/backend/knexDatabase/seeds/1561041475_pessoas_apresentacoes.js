const pessoas_apresentacoes = require('../seedFiles/pessoas_apresentacoes')

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('Pessoas_Apresentacoes').del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(pessoas_apresentacoes);
    })
};
