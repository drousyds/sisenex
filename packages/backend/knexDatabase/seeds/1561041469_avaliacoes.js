const avaliacoes = require('../seedFiles/avaliacoes')

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('Avaliacoes').del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(avaliacoes);
    })
};
