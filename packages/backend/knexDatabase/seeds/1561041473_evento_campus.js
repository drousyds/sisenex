const eventos_campus = require('../seedFiles/eventos_campus')

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('Eventos_Campus').del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(eventos_campus);
    })
};
