const eventos = require('../seedFiles/eventos')

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('Eventos').del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(eventos);
    })
};
