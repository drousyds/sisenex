const apresentacoes = require('../seedFiles/apresentacoes')

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('Apresentacoes').del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(apresentacoes);
    })
};
