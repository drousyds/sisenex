const categorias = require('../seedFiles/categorias')

exports.seed = async function(knex, Promise) {
  // Deletes ALL existing entries
  await knex.raw('TRUNCATE TABLE "Categorias" CASCADE;');
  await knex.raw('ALTER SEQUENCE "Categorias_idCategoria_seq" RESTART;');
  return knex('Categorias').del()
    .then(function () {
      // Inserts seed entries
      return knex.raw(categorias);
    })
};
