
exports.up = function (knex, Promise) {
  return knex.schema
    .createTable('Avaliacoes', table => {
      table.double('notaAvaliacao', 4, 2).notNullable();
      table.datetime('dataAvaliacao', 6).defaultTo(knex.fn.now(6)).notNullable();
      table.integer('idPergunta').unsigned().notNullable();
      table.integer('idProjeto', 11).notNullable();
      table.string('idPessoa', 16);
      table.string('idVinculoPessoa', 16);
      table.boolean('avaliacaoManual');
      table.string('nomeAvaliadorPapel');

      table.primary(['idPergunta', 'idProjeto', 'idPessoa', 'idVinculoPessoa']);
      table.unique(['idPergunta', 'idProjeto', 'idPessoa', 'idVinculoPessoa', 'nomeAvaliadorPapel']);


    });
};

exports.down = function (knex, Promise) {
  return knex.schema
    .dropTable('Avaliacoes');
};