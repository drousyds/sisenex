exports.up = function(knex, _Promise) {
    return knex.schema
        .alterTable('Apresentacoes', table => {
            table.integer('idCategoria').notNullable();
            table.string('linkApresentacao');

            table.foreign('idCategoria').references('idCategoria').inTable('Categorias');
        });
};

exports.down = function(knex, _Promise) {
    return knex.schema
        .alterTable('Apresentacoes', table => {
            table.dropForeign('idCategoria');
            table.dropColumns(['idCategoria', 'linkApresentacao']);
        });
};
