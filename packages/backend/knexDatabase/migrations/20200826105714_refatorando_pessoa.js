exports.up = function(knex, _Promise) {
  return knex.schema
    .alterTable('Pessoas', table => {
      table.integer('idCategoria').notNullable();

      table.foreign('idCategoria').references('idCategoria').inTable('Categorias');
    });
};

exports.down = function(knex, _Promise) {
  return knex.schema
    .alterTable('Pessoas', table => {
      table.dropForeign('idCategoria');
      table.dropColumn('idCategoria');
    });
};
