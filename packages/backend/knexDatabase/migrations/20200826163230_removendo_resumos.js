exports.up = function(knex, _Promise) {
  return knex.schema
    .dropTableIfExists('Resumos');
};

exports.down = function(knex, _Promise) {
  return knex.schema
    .createTable("Resumos", table => {
      table
        .integer("idResumo")
        .notNullable()
        .primary();
      table.string("tituloResumo").notNullable();
      table.string("nomeCoordenador").notNullable();
      table
        .string("codigoProjeto")
        .notNullable()
        .references("codigoProjeto")
        .inTable("Projetos");
    });
};
