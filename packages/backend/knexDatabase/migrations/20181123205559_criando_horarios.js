
exports.up = function (knex, Promise) {
  return knex.schema
    .createTable('Horarios', table => {
      table.integer('idEvento');
      table.integer('idAreaTematica');
      table.time('horaInicio', { precision: 6 });
      table.time('horaFim', { precision: 6 });

      table.primary(['idEvento', 'idAreaTematica', 'horaInicio', 'horaFim'])

    });
};

exports.down = function (knex, Promise) {
  return knex.schema
    .dropTable('Horarios');
};