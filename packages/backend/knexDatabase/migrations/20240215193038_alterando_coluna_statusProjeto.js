exports.up = function (knex, Promise) {
  return knex.schema
    .alterTable('Projetos', table => table.dropColumn('statusProjeto'))
    .alterTable('Projetos', table => table.string('statusProjeto',45).notNullable().defaultTo('NAO AVALIADO'))
};

exports.down = function (knex, Promise) {
};
