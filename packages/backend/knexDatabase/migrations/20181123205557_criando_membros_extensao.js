
exports.up = function (knex, Promise) {
  return knex.schema
    .createTable('Membros_Extensao', table => {
      table.integer('idProjeto', 11).notNullable()
      table.string('idPessoa', 16).notNullable();
      table.string('idVinculoPessoa', 16).notNullable();
      table.string('funcaoMembro', 36);

      table.primary(['idPessoa', 'idVinculoPessoa', 'idProjeto']);

    });
};

exports.down = function (knex, Promise) {
  return knex.schema
    .dropTable('Membros_Extensao');
};