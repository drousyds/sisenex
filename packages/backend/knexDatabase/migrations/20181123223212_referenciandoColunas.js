
exports.up = function (knex, Promise) {
  return knex.schema
    .table('Perguntas', function (table) {
      table.foreign('idCategoria').references('idCategoria').inTable('Categorias').onDelete('NO ACTION');
    })
    .table('Unidades', function (table) {
      table.foreign('idCampus').references('idCampus').inTable('Campus').onDelete('NO ACTION');
    })
    .table('Configuracoes', function (table) {
      table.foreign('idAreaTematica').references('idAreaTematica').inTable('AreasTematicas').onDelete('NO ACTION');
      table.foreign('idEvento').references('idEvento').inTable('Eventos').onDelete('NO ACTION');
    })
    .table('Horarios', function (table) {
      table.foreign(['idEvento','idAreaTematica']).references(['idEvento','idAreaTematica']).inTable('Configuracoes').onDelete('NO ACTION');
      // table.foreign('idEvento').references('idEvento').inTable('Configuracoes').onDelete('NO ACTION');
    })
    .table('Apresentacoes', function (table) {
      table.foreign(['idEvento','idAreaTematica','horaInicio','horaFim']).references(['idEvento','idAreaTematica','horaInicio','horaFim']).inTable('Horarios').onDelete('NO ACTION');
      // table.foreign('idEvento').references('idEvento').inTable('Horarios').onDelete('NO ACTION');
      // table.foreign('horaInicio').references('horaInicio').inTable('Horarios').onDelete('NO ACTION');
      // table.foreign('horaFim').references('horaFim').inTable('Horarios').onDelete('NO ACTION');
    })
    .table('Projetos', function (table) {
      table.foreign('idUnidade').references('idUnidade').inTable('Unidades').onDelete('NO ACTION');
      table.foreign(['idApresentacao', 'id2Apresentacao']).references(['idApresentacao', 'id2Apresentacao']).inTable('Apresentacoes').onDelete('NO ACTION');
      table.foreign('idAreaTematica').references('idAreaTematica').inTable('AreasTematicas').onDelete('NO ACTION');
    })
    .table('Avaliacoes', function (table) {
      table.foreign('idPergunta').references('idPergunta').inTable('Perguntas').onDelete('NO ACTION');
      table.foreign('idProjeto').references('idProjeto').inTable('Projetos').onDelete('NO ACTION');
      table.foreign(['idPessoa', 'idVinculoPessoa']).references(['idPessoa', 'idVinculoPessoa']).inTable('Pessoas').onDelete('NO ACTION');
    })
    .table('Reports', function (table) {
      table.foreign(['idPessoa', 'idVinculoPessoa']).references(['idPessoa', 'idVinculoPessoa']).inTable('Pessoas').onDelete('NO ACTION');
      table.foreign(['idPessoaResposta', 'idVinculoPessoaResposta']).references(['idPessoa', 'idVinculoPessoa']).inTable('Pessoas').onDelete('NO ACTION');
    })
    .table('Membros_Extensao', function (table) {
      table.foreign(['idPessoa', 'idVinculoPessoa']).references(['idPessoa', 'idVinculoPessoa']).inTable('Pessoas').onDelete('NO ACTION');
      table.foreign('idProjeto').references('idProjeto').inTable('Projetos').onDelete('NO ACTION');
    })
    .table('Pessoas_Apresentacoes', function (table) {
      table.foreign(['idApresentacao', 'id2Apresentacao']).references(['idApresentacao', 'id2Apresentacao']).inTable('Apresentacoes').onDelete('NO ACTION');
      table.foreign(['idPessoa', 'idVinculoPessoa']).references(['idPessoa', 'idVinculoPessoa']).inTable('Pessoas').onDelete('NO ACTION');
    })
    .table('Pessoas_Projetos', function (table) {
      table.foreign(['idPessoa', 'idVinculoPessoa']).references(['idPessoa', 'idVinculoPessoa']).inTable('Pessoas').onDelete('NO ACTION');
      table.foreign('idProjeto').references('idProjeto').inTable('Projetos').onDelete('NO ACTION');
    })
    .table('Eventos_Campus', function (table) {
      table.foreign('idCampus').references('idCampus').inTable('Campus').onDelete('NO ACTION');
      table.foreign('idEvento').references('idEvento').inTable('Eventos').onDelete('NO ACTION');
    })

};

exports.down = function (knex, Promise) {
  // return knex.raw(`SELECT concat('ALTER TABLE ', TABLE_NAME, ' DROP FOREIGN KEY ', CONSTRAINT_NAME, ';') 
  // FROM information_schema.key_column_usage 
  // WHERE CONSTRAINT_SCHEMA = 'db_name' 
  // AND referenced_table_name IS NOT NULL;`);
  return knex.schema
  .table('Perguntas', function (table) {
    table.dropForeign('idCategoria')
  })
  .table('Unidades', function (table) {
    table.dropForeign('idCampus')
  })
  .table('Configuracoes', function (table) {
    table.dropForeign('idAreaTematica')
    table.dropForeign('idEvento')
  })
  .table('Horarios', function (table) {
    table.dropForeign(['idEvento','idAreaTematica'])
    // table.dropForeign('idEvento')
  })
  .table('Apresentacoes', function (table) {
    table.dropForeign(['idEvento','idAreaTematica','horaInicio','horaFim'])
    // table.dropForeign('idEvento')
    // table.dropForeign('horaInicio')
    // table.dropForeign('horaFim')
  })
  .table('Projetos', function (table) {
    table.dropForeign('idUnidade')
    table.dropForeign(['idApresentacao', 'id2Apresentacao'])
    table.dropForeign('idAreaTematica')
  })
  .table('Avaliacoes', function (table) {
    table.dropForeign('idPergunta')
    table.dropForeign('idProjeto')
    table.dropForeign(['idPessoa', 'idVinculoPessoa'])
  })
  .table('Reports', function (table) {
    table.dropForeign(['idPessoa', 'idVinculoPessoa'])
    table.dropForeign(['idPessoaResposta', 'idVinculoPessoaResposta'])
  })
  .table('Membros_Extensao', function (table) {
    table.dropForeign(['idPessoa', 'idVinculoPessoa'])
    table.dropForeign('idProjeto')
  })
  .table('Pessoas_Apresentacoes', function (table) {
    table.dropForeign(['idApresentacao', 'id2Apresentacao'])
    table.dropForeign(['idPessoa', 'idVinculoPessoa'])
  })
  .table('Pessoas_Projetos', function (table) {
    table.dropForeign(['idPessoa', 'idVinculoPessoa'])
    table.dropForeign('idProjeto')
  })
  .table('Eventos_Campus', function (table) {
    table.dropForeign('idCampus')
    table.dropForeign('idEvento')
  })
};
