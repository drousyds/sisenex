exports.up = function (knex, Promise) {
  return knex.schema.createTable("Eventos_Campus", table => {
    table.integer("idEvento").unsigned().notNullable();
    table.integer("idCampus").unsigned().notNullable();

    table.primary([
      "idEvento",
      "idCampus",
    ]);
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("Eventos_Campus");
};
