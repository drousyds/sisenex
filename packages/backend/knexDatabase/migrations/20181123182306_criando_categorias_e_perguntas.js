
exports.up = function (knex, Promise) {
  return knex.schema
    .createTable('Categorias', table => {
      table.integer('idCategoria').notNullable().primary('idCategoria');
      table.string('nomeCategoria').notNullable();
    })
    .createTable('Perguntas', table => {
      table.increments('idPergunta');   //increments means 'id int unsigned not null auto_increment primary key'
      table.string('conteudoPergunta').notNullable();       //default lenght of string is 255
      table.integer('idCategoria');
      table.date('dataPergunta');
      table.unique(['idPergunta', 'dataPergunta'])
    });
};

exports.down = function (knex, Promise) {
  return knex.schema
    .dropTable('Perguntas')
    .dropTable('Categorias');
};