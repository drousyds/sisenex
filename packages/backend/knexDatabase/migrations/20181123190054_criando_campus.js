
exports.up = function(knex, Promise) {
  return knex.schema
  .createTable('Campus', table =>{
      table.integer('idCampus').notNullable().primary('idCampus');
      table.string('nomeCampus',64).notNullable();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema
  .dropTable('Campus');
};