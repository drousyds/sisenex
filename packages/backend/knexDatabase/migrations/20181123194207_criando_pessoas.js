
exports.up = function (knex, Promise) {
  return knex.schema
    .createTable('Pessoas', table => {
      table.string('idPessoa', 16).notNullable();
      table.string('idVinculoPessoa', 16).notNullable();
      table.string('matriculaPessoa', 45).unique();        //not notNullable()???
      table.string('nomePessoa');
      table.string('nomeSocialPessoa').notNullable();
      table.string('lotacaoPessoa');
      table.string('emailPessoa', 45);
      table.string('telefonePessoa', 32);
      table.specificType('aptidaoPessoa', 'SMALLINT').notNullable();
      table.specificType('avaliadorPessoa', 'SMALLINT');
      table.specificType('monitorPessoa', 'SMALLINT');
      table.specificType('gerentePessoa', 'SMALLINT');
      table.primary(['idPessoa', 'idVinculoPessoa']);

    });
};

exports.down = function (knex, Promise) {
  return knex.schema
    .dropTable('Pessoas');
};