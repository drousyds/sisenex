exports.up = function(knex, _Promise) {
    return knex.schema
        .alterTable('Perguntas', table => {
            table.dropForeign('idCategoria');
            table.dropColumn('idCategoria');
        })
        .alterTable('Categorias', table => {
            table.dropPrimary('idCategoria');
            table.dropColumn('idCategoria');
        })
        .alterTable('Categorias', table => {
            table.increments('idCategoria');
        })
        .alterTable('Perguntas', table => {
            table.integer('idCategoria');
            table.foreign('idCategoria').references('idCategoria').inTable('Categorias');
        });
};

exports.down = function(knex, _Promise) {
    return knex.schema
        .alterTable('Perguntas', table => {
            table.dropForeign('idCategoria');
            table.dropColumn('idCategoria');
        })
        .alterTable('Categorias', table => {
            table.dropPrimary('idCategoria');
            table.dropColumn('idCategoria');
        })
        .alterTable('Categorias', table => {
            table.integer('idCategoria').notNullable().primary();
        })
        .alterTable('Perguntas', table => {
            table.integer('idCategoria');
            table.foreign('idCategoria').references('idCategoria').inTable('Categorias');
        });
};
