
exports.up = function (knex, Promise) {
  return knex.schema
    .table('Reports', table => {
      table.dropColumn('nomeReport');
      table.dropColumn('horaReportResposta');
      table.dropColumn('idPessoaResposta');
      table.dropColumn('idVinculoPessoaResposta');
      table.dropColumn('reportText');
      table.string('conteudoReport', 512);
      table.boolean('visualizado')

    });

};


exports.down = function (knex, Promise) {
  return knex.schema
};
