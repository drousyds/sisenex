
exports.up = function (knex, Promise) {
  return knex.schema
    .createTable('Apresentacoes', table => {
      table.increments('idApresentacao')
      table.string('id2Apresentacao', 4);
      table.integer('idAreaTematica');
      table.integer('idEvento');
      table.string('codigoApresentacao', 8);
      table.string('salaApresentacao',12)
      table.string('modalidadeApresentacao', 1);
      table.datetime('horaApresentacao', 6).defaultTo(knex.fn.now(6));
      table.datetime('dataInicioApresentacao', 6).defaultTo(knex.fn.now(6));
      table.datetime('dataFimApresentacao', 6).defaultTo(knex.fn.now(6));
      table.specificType('disponibilidadeApresentacao', 'SMALLINT').notNullable();
      table.time('horaInicio', { precision: 6 });
      table.time('horaFim', { precision: 6 });
      table.unique(['idApresentacao', 'id2Apresentacao']);

    })
};

exports.down = function (knex, Promise) {
  return knex.schema
    .dropTable('Apresentacoes');
};