const { drop } = require("lodash");

exports.up = function(knex, Promise) {
    return knex.schema
    .alterTable('Pessoas', table => {
        table.datetime('created_at', { precision: 6 }).notNullable().defaultTo(knex.fn.now(6));
    });
};

exports.down = function(knex, Promise) {
    table.dropColumn('created_at');
};
