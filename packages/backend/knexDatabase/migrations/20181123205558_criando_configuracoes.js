
exports.up = function (knex, Promise) {
  return knex.schema
    .createTable('Configuracoes', table => {
      table.integer('idEvento').unsigned()
      table.integer('idAreaTematica');
      table.specificType('quantidadeSalas', 'SMALLINT');
      table.specificType('quantidadeApresentacoes', 'SMALLINT');
      table.boolean('configAtiva');

      table.primary(['idEvento', 'idAreaTematica'])

    });
};

exports.down = function (knex, Promise) {
  return knex.schema
    .dropTable('Configuracoes');
};