exports.up = function(knex, Promise) {
  return knex.schema
  .createTable('SolicitacaoLGPD', table =>{
      table.increments('idSolicitacao');
      table.integer('tipoSolicitacao').notNullable();
      table.string('textoSolicitacao',4096);
      table.string('idPessoa',16);
      table.string('idVinculoPessoa',16);
      table.string('nomeSolicitacao');
      table.string('sobrenomeSolicitacao');
      table.string('emailPessoa', 45);

      table.foreign(['idPessoa', 'idVinculoPessoa']).references(['idPessoa', 'idVinculoPessoa']).inTable('Pessoas').onDelete('NO ACTION');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema
  .dropTable('SolicitacaoLGPD');
};