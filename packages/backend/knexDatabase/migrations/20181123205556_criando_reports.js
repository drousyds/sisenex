
 exports.up = function(knex, Promise) {
  return knex.schema
 .createTable('Reports', table =>{
     table.increments('idReport');
     table.string('nomeReport',45).notNullable();
     table.string('reportText',512);
     table.datetime('horaReport',6);
     table.datetime('horaReportResposta',6);
     table.string('idPessoa',16);
     table.string('idVinculoPessoa',16);
     table.string('idPessoaResposta',16);
     table.string('idVinculoPessoaResposta',16);

 });
};

exports.down = function(knex, Promise) {
  return knex.schema
 .dropTable('Reports');
};