
exports.up = function (knex, Promise) {
  return knex.schema
    .createTable('Eventos', table => {
      table.increments('idEvento');
      table.string('nomeEvento', 45).notNullable();
      table.string('descricaoEvento', 256);
      table.string('localEvento', 64).notNullable();
      table.date('dataInicioEvento');
      table.date('dataFimEvento');

      // table.primary('idEvento')
    })
  // .raw("ALTER TABLE Eventos MODIFY idEvento INT NOT NULL SERIAL");
};

exports.down = function (knex, Promise) {
  return knex.schema
    .dropTable('Eventos');
};