exports.up = function (knex, _Promise) {
  return knex.schema
    .alterTable('Apresentacoes', table => table.dropColumn('modalidadeApresentacao'))
    .alterTable('Apresentacoes', table => {
      table.string('modalidadeApresentacao', 1).defaultTo('T');
    });
};

exports.down = function (knex, _Promise) {
  return knex.schema
};
