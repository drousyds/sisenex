exports.up = function(knex, Promise) {
  return knex.schema
    .alterTable("Projetos", table => {
      table.unique("codigoProjeto");
    })
    .createTable("Resumos", table => {
      table
        .integer("idResumo")
        .notNullable()
        .primary();
      table.string("tituloResumo").notNullable();
      table.string("nomeCoordenador").notNullable();
      table
        .string("codigoProjeto")
        .notNullable()
        .references("codigoProjeto")
        .inTable("Projetos");
    });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("Resumos");
};
