
exports.up = function (knex, Promise) {
  return knex.schema
    .table('Reports', table => {
      table.integer('idApresentacao')
      table.string('id2Apresentacao')
      table.foreign(['idApresentacao', 'id2Apresentacao']).references(['idApresentacao', 'id2Apresentacao']).inTable('Apresentacoes').onDelete('NO ACTION');
    });
};

exports.down = function (knex, Promise) {
  return knex.schema
    .table('Reports', table => {
      table.dropForeign(['idApresentacao', 'id2Apresentacao'])
    })
};