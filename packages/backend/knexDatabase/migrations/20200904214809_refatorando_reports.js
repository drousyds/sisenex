exports.up = function(knex, _Promise) {
    return knex.schema
        .alterTable('Reports', table => {
            table.string('emailReport', 512).notNullable();
        });
};

exports.down = function(knex, _Promise) {
    return knex.schema
        .alterTable('Reports', table => {
            table.dropColumn('Reports');
        });
};
