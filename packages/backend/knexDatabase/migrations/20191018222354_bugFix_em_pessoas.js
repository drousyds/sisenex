exports.up = function(knex, Promise) {
  return knex.schema
    .table('Pessoas', table => {
      table.dropColumn('administradorPessoa');
    })
    .table('Pessoas', table => {
      table.specificType('administradorPessoa', 'SMALLINT');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema;
};
