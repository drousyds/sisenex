
exports.up = function(knex, Promise) {
    return knex.schema
    .createTable('AreasTematicas', table =>{
        table.integer('idAreaTematica').notNullable().primary('idAreaTematica');
        table.string('nomeAreaTematica',56).notNullable();
    });
  };
  
  exports.down = function(knex, Promise) {
    return knex.schema
    .dropTable('AreasTematicas');
  };