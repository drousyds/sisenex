exports.up = function(knex, _Promise) {
  return knex.schema
    .alterTable('Projetos', table => {
      table.integer('idCategoria').notNullable();
      table.string('linkAterfato');

      table.foreign('idCategoria').references('idCategoria').inTable('Categorias');
    });
};

exports.down = function(knex, _Promise) {
  return knex.schema
    .alterTable('Projetos', table => {
      table.dropForeign('idCategoria');
      table.dropColumns(['idCategoria', 'linkAterfato']);
    });
};
