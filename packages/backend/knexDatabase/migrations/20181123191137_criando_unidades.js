
exports.up = function(knex, Promise) {
    return knex.schema
    .createTable('Unidades', table =>{
        table.integer('idUnidade').notNullable().primary();
        table.string('codigoUnidade',45).notNullable();
        table.string('nomeUnidade').notNullable();
        table.string('siglaUnidade',45).notNullable();
        table.string('hierarquiaUnidade',45).notNullable();
        table.string('tipoUnidade',45).notNullable();
        table.integer('unidadeGestora').notNullable();
        table.integer('idCampus').notNullable();

    });
  };
  
  exports.down = function(knex, Promise) {
    return knex.schema
    .dropTable('Unidades');
  };