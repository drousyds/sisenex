const { drop } = require("lodash");

exports.up = function(knex, Promise) {
    return knex.schema
    .alterTable('SolicitacaoLGPD', table => {
        table.string('statusSolicitacao', 45);
    });
};

exports.down = function(knex, Promise) {
    table.dropColumn('statusSolicitacao');
};