
exports.up = function (knex, Promise) {
  return knex.schema
    .table('Projetos', table => {
      table.integer('idEvento')
      table.foreign('idEvento').references('idEvento').inTable('Eventos').onDelete('NO ACTION');
    })

}

exports.down = function (knex, Promise) {
  return knex.schema
    .table('Projetos', function (table) {
      table.dropForeign('idEvento')
    })
};
