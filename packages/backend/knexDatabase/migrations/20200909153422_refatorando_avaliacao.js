exports.up = function(knex, _Promise) {
  return knex.schema
    .alterTable('Perguntas', table => table.dropColumn('repostaTextual'))
    .alterTable('Avaliacoes', table => {
      table.string('respostaTextual', 4096);
      table.double('notaAvaliacao', 4, 2).alter();
    });
};

exports.down = function(knex, _Promise) {
  return knex.schema
    .alterTable('Avaliacoes', table => {
      table.double('notaAvaliacao', 4, 2).notNullable().alter();
      table.dropColumn('respostaTextual');
    })
    .alterTable('Perguntas', table => table.string('respotaTextual', 4096));
};
