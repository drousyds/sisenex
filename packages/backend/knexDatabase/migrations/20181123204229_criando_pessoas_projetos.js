
 exports.up = function(knex, Promise) {
     return knex.schema
    .createTable('Pessoas_Projetos', table =>{
        table.boolean('aptidaoPessoa');
        table.integer('idProjeto',11).notNullable();
        table.string('idPessoa',16);
        table.string('idVinculoPessoa',16);

        table.primary(['idPessoa','idVinculoPessoa','idProjeto']);

    });
   };
  
   exports.down = function(knex, Promise) {
     return knex.schema
    .dropTable('Pessoas_Projetos');
   };