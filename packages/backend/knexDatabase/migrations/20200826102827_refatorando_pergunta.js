exports.up = function(knex, _Promise) {
  return knex.schema
    .alterTable('Perguntas', table => {
      table.string('repostaTextual', 4096);
    });
};

exports.down = function(knex, _Promise) {
  return knex.schema
    .alterTable('Perguntas', table => {
      table.dropColumn('repostaTextual');
    });
};
