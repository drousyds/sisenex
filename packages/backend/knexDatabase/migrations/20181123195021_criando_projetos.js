
exports.up = function (knex, Promise) {
  return knex.schema
    .createTable('Projetos', table => {
      table.integer('idProjeto', 11);
      table.string('tituloProjeto', 512).notNullable();
      table.string('descricaoProjeto', 32).notNullable();
      table.date('dataInicioProjeto').notNullable();
      table.date('dataFimProjeto').notNullable();
      table.date('anoProjeto').notNullable();
      table.datetime('liberacaoProjeto', 6).defaultTo(knex.fn.now(6));
      table.specificType('disponibilidadeProjeto', 'SMALLINT').defaultTo(0);
      table.integer('idAreaTematica').notNullable();
      table.integer('idUnidade').notNullable();
      table.integer('idApresentacao').unsigned()
      table.string('id2Apresentacao', 5).unsigned()

      table.primary(['idProjeto']);

    });
};

exports.down = function (knex, Promise) {
  return knex.schema
    .dropTable('Projetos');
};