exports.up = function (knex, Promise) {
  return knex.schema.createTable("Pessoas_Apresentacoes", table => {
    table.boolean("aptidaoPessoa");
    table.integer("idApresentacao").unsigned().notNullable();
    table.string("id2Apresentacao", 4).unsigned().notNullable();
    table.string("idPessoa", 16);
    table.string("idVinculoPessoa", 16);

    table.primary([
      "idApresentacao",
      "id2Apresentacao",
      "idPessoa",
      "idVinculoPessoa"
    ]);
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable("Pessoas_Apresentacoes");
};
