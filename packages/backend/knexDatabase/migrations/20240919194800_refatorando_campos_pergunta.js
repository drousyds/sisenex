exports.up = function(knex, _Promise) {
  return knex.schema
    .alterTable('Perguntas', table => {
      table.text('conteudoPergunta').alter();
    });
};

exports.down = function(knex, _Promise) {
  return knex.schema
    .alterTable('Perguntas', table => {
      table.dropColumn('conteudoPergunta');
    });
};