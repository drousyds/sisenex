var express = require("express");
var cors = require("cors");
const Knex = require("knex");
const { Model } = require("objection");
const knexConfig = require("./knexfile");
const errorHandler = require("./errorMiddleware");
const { typeDefs, resolvers } = require("./graphql/schema");
const { ApolloServer, PubSub } = require("apollo-server");
const buildLoaderApres = require('./graphql/loaders/loaderApresentacao');
const buildLoaderUni = require('./graphql/loaders/loaderUnidade');
const buildloaderAreasT = require('./graphql/loaders/loaderAreasTematicas');
const buildloaderEvento = require('./graphql/loaders/loaderEvento');

var path = require("path");

var bodyParser = require("body-parser"); //json helper

var sisenexRouter = require("./routes/sisenex");

var app = express();

app.use(errorHandler);

//Knex and Objection configurations
const knex = Knex(knexConfig[process.env.NODE_ENV]);
Model.knex(knex);

//configuring usage of bodyParser (json helper)
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
//app.use(cookieParser());
app.use(cors());
app.use("/api", sisenexRouter);

//Graphql setup

const { PostgresPubSub } = require('graphql-postgres-subscriptions');

const pubsub = new PostgresPubSub({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB
});

const apollo = new ApolloServer ({
  typeDefs,
  resolvers,
  context: async ({ req, connection }) => { 
    if (connection) {
      return connection.context;
    } else {
      return {
        loaderApresentacao : buildLoaderApres(),
        loaderUnidades : buildLoaderUni(),
        loaderAreasTematicas : buildloaderAreasT(),
        loaderEvento: buildloaderEvento(),
        authToken : req.headers.authorization,
        req: req,
        pubsub: pubsub
      };
    }
  },
  tracing: false,
  playground: {
    subscriptionEndpoint : 'ws://localhost/graphql/subscriptions'
  },
  subscriptions: {
    path: "/subscriptions",
    onConnect: async () => {
      return { pubsub };
    },
    onDisconnect: async () => {}
  },
  introspection:true,
  cors: true
});

apollo.listen().then(({ url , subscriptionsUrl }) => {
  console.log(`Server ready at ${url}`);
  console.log(`Subscriptions ready at ${subscriptionsUrl}`);
});

module.exports = app;
