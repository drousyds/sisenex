const express = require("express");
const router = express.Router();
const Categorias = require("../objectionDatabase/queries/categorias");

router.get("/", async (req, res, next) => {
  return Categorias.getCategorias(req.query)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.get("/:idCategoria/perguntas", async (req, res, next) => {
  return Categorias.getPerguntas(req.params)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.post("/", (req, res, next) => {
  return Categorias.postCategoria(req.body)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.put("/:idCategoria", (req, res, next) => {
  return Categorias.putCategoria(req.params, req.body)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.delete("/:idCategoria", async (req, res, next) => {
  return Categorias.delCategoria(req.params)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

module.exports = router;
