var express = require('express');
var router = express.Router();
var msgError = require('../helpers/errosSQL')
var _ = require('lodash')
var Projetos = require('../objectionDatabase/models/model_projetos')
var Avaliacoes = require('../objectionDatabase/models/model_avaliacoes')
var Pessoas_Projetos = require('../objectionDatabase/models/model_pessoasProjetos')
var knex = require('../knexinstance')

router.get("/", (req, res, next) => {

    Projetos
        // .getProjetos(req.query)
        .query()
        .then(resultados => res.status(200).json({ resultados }))
        .catch(e => next(e))
});

router.post("/", (req, res, next) => {
    Projetos
        .query()
        .insert(req.body)
        .then(() => {

            res.status(200).json({ resultados: 'OK' })
        })
        // .catch((error) => {
        //     res.status(417).json({ title: "error", status: error.errno, message: msgError(error), mensagemDetalhada: error });
        // });
        .catch(e => next(e))
});

router.get("/:idProjeto", (req, res, next) => {

    knex('Projetos').select().where({ 'idProjeto': req.params.idProjeto })
        .then(resultados => {
            res.status(200).json({ resultados })
        })
        // .catch((error) => {
        //     res.status(417).json({ title: 'error', status: error.errno, message: msgError(error) });
        // })
        .catch(e => next(e))
});

router.put("/:idProjeto", (req, res, next) => {
    Projetos
        .query()
        .patchAndFetchById(req.params.idProjeto, req.body)
        .then(resultados => {
            res.status(200).json({ resultados })
        })
        // .catch((error) => {
        //     res.status(417).json({ title: 'error', status: error.errno, message: msgError(error) });
        // })
        .catch(e => next(e))
});

router.delete("/:idProjeto", (req, res, next) => {
    Projetos
        .query()
        .delete()
        .where(req.params.idProjeto)
        .then(resultados => {
            res.status(200).json({ resultados })
        })
        // .catch((error) => {
        //     res.status(417).json({ title: 'error', status: error.errno, message: msgError(error) });
        // })
        .catch(e => next(e))
});

router.get("/:idProjeto/avaliadores/medias", (req, res, next) => {

    Projetos
        .mediaProjeto(req.params)
        .then(resultados => {
            res.status(200).json({ resultados })
        })
        .catch(e => next(e))

});

router.get("/avaliacoes/medias", (req, res, next) => {

    Avaliacoes
        .query()
        .select('projeto.idprojeto', 'projeto.tituloProjeto', 'projeto:areaTematica.nomeAreaTematica',
            Avaliacoes.relatedQuery('projeto').avg('notaAvaliacao').as('mediaAvaliacao'))
        .joinRelation('projeto.[areaTematica]')
        .groupBy('Avaliacoes.idProjeto')
        .where((q) => {
            _.mapKeys(req.query, (value, key) => {
                q.where(`projeto.${key}`, value)
            });
        })
        .orderBy('mediaAvaliacao', 'desc')
        .then(resultados => {
            res.status(200).json({ resultados })
        })
        // .catch((error) => {
        //     res.status(417).json({ title: 'error', status: error.errno, message: msgError(error) });
        // })
        .catch(e => next(e))
});

router.put("/:idProjeto/avaliadores", (req, res, next) => {
    Pessoas_Projetos
        .query()
        .update(req.body)
        .where('idApresentacao', function () {
            this
                .select('idApresentacao')
                .from('Projetos')
                .where('idProjeto', req.params.idProjeto)
        }
        )
        .where('id2Apresentacao', function () {
            this
                .select('id2Apresentacao')
                .from('Projetos')
                .where('idProjeto', req.params.idProjeto)
        }
        )
        .then(resultados => {
            res.status(200).json({ resultados })
        })
        // .catch((error) => {
        //     res.status(417).json({ title: 'error', status: error.errno, message: msgError(error) });
        // })
        .catch(e => next(e))
});



module.exports = router;