var express = require('express');
var router = express.Router();
const errorHandler = require('../errorMiddleware');

var categoriasRouter = require('./categorias');
var areastematicasRouter = require('./areastematicas')
var avaliadoresRouter = require('./avaliadores')
var gerentesRouter = require('./gerentes')
var monitoresRouter = require('./monitores')
var perguntasRouter = require('./perguntas')
var projetosRouter = require('./projetos')
var apresentacoesRouter = require('./apresentacoes')
var unidadesRouter = require('./unidades')
var pessoasRouter = require('./pessoas')


router.use('/categorias', categoriasRouter);
router.use('/areastematicas', areastematicasRouter);
router.use('/avaliadores', avaliadoresRouter);
router.use('/gerentes', gerentesRouter);
router.use('/monitores', monitoresRouter);
router.use('/perguntas', perguntasRouter);
router.use('/projetos', projetosRouter);
router.use('/apresentacoes', apresentacoesRouter);
router.use('/unidades', unidadesRouter);
router.use('/pessoas', pessoasRouter);

router.get("/", (req, res) => {
    res.json({ message: "Welcome to Sisenex API. \n May I take your order?" });
});

router.use(errorHandler);


module.exports = router;
