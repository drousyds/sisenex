const express = require("express");
const router = express.Router();
const Perguntas = require("../objectionDatabase/queries/perguntas");

router.get("/", (req, res, next) => {
  Perguntas.getPerguntas(req.params, req.query)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.get("/:idPergunta", (req, res, next) => {
  Perguntas.getPergunta(req.params, req.query)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.post("/", (req, res, next) => {
  Perguntas.postPergunta(req.body)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.put("/:idPergunta", (req, res, next) => {
  Perguntas.putPergunta(req.params, req.body)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.delete("/:idPergunta", (req, res, next) => {
  Perguntas.query()
    .delete()
    .where(req.params)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

module.exports = router;
