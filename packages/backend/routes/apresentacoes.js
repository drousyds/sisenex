
var express = require("express");
var router = express.Router();
var knex = require('../knexinstance');
var msgError = require('../helpers/errosSQL')
var _ = require('lodash')
var Apresentacoes = require('../objectionDatabase/models/model_apresentacoes')
var Pessoas_Apresentacoes = require('../objectionDatabase/models/model_pessoasApresentacoes')
var Pessoas_Projetos = require('../objectionDatabase/models/model_pessoasProjetos')
var Projetos = require('../objectionDatabase/models/model_projetos')

router.get("/", (req, res) => {

    Apresentacoes.query()
        .where((q) => {
            _.mapKeys(req.query, (value, key) => {
                q.where(`Apresentacoes.${key}`, value)
            });
        })
        .then((resposta) => {
            res.status(200).json({ resultados: resposta })
        })
        .catch((error) => {
            res.status(417).json({ title: "error", status: error.errno, message: msgError(error) });
        });
});

router.get("/projetos", (req, res) => {

    Apresentacoes
        .query()
        .eager('projetos.[areaTematica,unidade,avaliacoes]')
        .omit(Projetos, ['idApresentacao', 'id2Apresentacao'])
        .then(resposta => {
            for (let i = 0, j = resposta.length; i < j; i++) {
                for (let k = 0, l = resposta[i].projetos.length; k < l; k++) {
                    resposta[i].projetos[k].nomeUnidade = resposta[i].projetos[k].unidade.nomeUnidade;
                    resposta[i].projetos[k].nomeAreaTematica = resposta[i].projetos[k].areaTematica.nomeAreaTematica;

                    resposta[i].projetos[k]['areaTematica'] = undefined;
                    resposta[i].projetos[k]['unidade'] = undefined;
                }

            }

            return resposta;
        })
        .then((resposta) => {
            res.status(200).json({ resultados: resposta })
        })
        .catch((error) => {
            res.status(417).json({ title: "error", status: error.errno, message: msgError(error) });
        });
});

router.post("/", (req, res,next) => {
    knex('Apresentacoes')
        .insert(req.body)
        .then((insertedId) => {
            res.status(201).json({ resultados: insertedId })
        })
        .catch(e=>next(e))
});

router.delete("/", async (req, res) => {
    try {
        let ppt = await Pessoas_Projetos
            .query()
            .delete()
            .whereIn('idApresentacao',req.body.apresentacoes.map((e)=> e.idApresentacao))
            .whereIn('id2Apresentacao',req.body.apresentacoes.map((e)=> e.id2Apresentacao))
        let pt = await Pessoas_Apresentacoes
            .query()
            .delete()
            .whereIn('idApresentacao',req.body.apresentacoes.map((e)=> e.idApresentacao))
            .whereIn('id2Apresentacao',req.body.apresentacoes.map((e)=> e.id2Apresentacao))
        let p = await Projetos
            .query()
            .patch({
                idApresentacao: null,
                id2Apresentacao: null
            })
            .whereIn('idApresentacao',req.body.apresentacoes.map((e)=> e.idApresentacao))
            .whereIn('id2Apresentacao',req.body.apresentacoes.map((e)=> e.id2Apresentacao))
        let t = await Apresentacoes
            .query()
            .delete()
            .whereIn('idApresentacao',req.body.apresentacoes.map((e)=> e.idApresentacao))
            .whereIn('id2Apresentacao',req.body.apresentacoes.map((e)=> e.id2Apresentacao))

        res.status(200).json({
            resultados: `${t} colunas deletadas em Apresentacoes.${p} Projetos com apresentacoes anuladas.`+
                `${pt} colunas deletadas em Pessoas_Apresentacoes. ${ppt} colunas deletadas em Pessoas_Projetos.`
        })
    }
    catch(error){
        res.status(417).json({ title: 'error', status: error.errno, message: msgError(error) });
    }
});

router.get("/:idApresentacao/:id2Apresentacao", (req, res,next) => {

    knex('Apresentacoes')
        .select()
        .where({
            'idApresentacao': req.params.idApresentacao,
            'id2Apresentacao': req.params.id2Apresentacao
        })
        .then((resposta) => {
            res.status(200).json({ resultados: resposta })
        })
        .catch(e=>next(e))
});

router.put("/:idApresentacao/:id2Apresentacao", (req, res) => {

    knex('Apresentacoes')
        .update(req.body)
        .where({
            'idApresentacao': req.params.idApresentacao,
            'id2Apresentacao': req.params.id2Apresentacao
        })
        .then((result) => {
            res.status(200).json({ resultados: result })
        })
        .catch((error) => {
            res.status(417).json({ title: "error", status: error.errno, message: msgError(error) });
        });
});

router.delete("/:idApresentacao/:id2Apresentacao", (req, res) => {
    knex('Apresentacoes')
        .where({
            'idApresentacao': req.params.idApresentacao,
            'id2Apresentacao': req.params.id2Apresentacao
        })
        .del();
});

router.get("/:idApresentacao/:id2Apresentacao/projetos", (req, res) => {
    Apresentacoes
        .query()
        .eager('projetos.[areaTematica,unidade]')
        .omit(Projetos, ['idApresentacao', 'id2Apresentacao'])
        .where(req.params)
        .then(resposta => {
            for (let k = 0, l = resposta[0].projetos.length; k < l; k++) {
                resposta[0].projetos[k].nomeUnidade = resposta[0].projetos[k].unidade.nomeUnidade;
                resposta[0].projetos[k].nomeAreaTematica = resposta[0].projetos[k].areaTematica.nomeAreaTematica;

                // delete resposta[0].projetos[k]['unidades'];
                // delete resposta[0].projetos[k]['areaTematica'];
                resposta[0].projetos[k]['areaTematica'] = undefined;
                resposta[0].projetos[k]['unidade'] = undefined;
            }

            return resposta;
        })
        .then((resposta) => {
            res.status(200).json({ resultados: resposta })
        })
        .catch((error) => {
            res.status(417).json({ title: "error", status: error.errno, message: msgError(error) });
        });

});

router.put("/:idApresentacao/:id2Apresentacao/projetos/:idProjeto", (req, res) => {

    knex.transaction(function (trx) {
        knex
            .update({
                'liberacaoProjeto': knex.fn.now()
            })
            .table('Projetos')
            .where('idProjeto', req.params.idProjeto)
            .transacting(trx)
            .then(function () {
                return knex
                    .table('Pessoas_Projetos')
                    .update('aptidaoPessoa', 1)
                    .where('idProjeto', req.params.idProjeto)
                    .transacting(trx);
            })
            .then(trx.commit)
            .catch(trx.rollback);
    })
        .then((registrosAfetados) => {
            res.status(200).json({ resultados: registrosAfetados })

        })
        .catch((error) => {
            res.status(417).json({ title: 'error', status: error.errno, message: msgError(error) });
        });

});

// router.put("/:idApresentacao/:id2Apresentacao/projetos/:idProjeto/avaliadores", (req, res) => {
//     var data = [req.body.aptidaoPessoa, req.params.idProjeto, req.params.idApresentacao, req.params.id2Apresentacao];
//     execSQLQuery(
//         "UPDATE Pessoas_Projetos SET aptidaoPessoa = ?" +
//         'WHERE idProjeto = ? AND idApresentacao = ? AND id2Apresentacao = ?',
//         data,
//         res
//     );
// });

router.get("/:idApresentacao/:id2Apresentacao/projetos/avaliacoes", (req, res) => {
    Apresentacoes
        .query()
        .eager('projetos.avaliacoes')
        .where(req.params)
        .pick(Apresentacoes, ['projetos'])
        .pick(Projetos, ['idProjeto', 'tituloProjeto', 'avaliacoes'])
        .then((resultadoFormatado) => {
            res.status(200).json({ resultados: resultadoFormatado })
        })
        .catch((error) => {
            res.status(417).json({ title: 'error', status: error.errno, message: msgError(error) });
        })
});

router.get("/:idApresentacao/:id2Apresentacao/avaliadores", (req, res) => {
    Pessoas_Apresentacoes
        .query()
        .select('Pessoas_Apresentacoes.idPessoa', 'Pessoas_Apresentacoes.idVinculoPessoa', 'pessoa.nomeSocialPessoa')
        .joinRelation('pessoa')
        .where({
            ...req.params,
            'Pessoas_Apresentacoes.aptidaoPessoa': 1
        })
        .then((results) => {
            res.status(200).json({ resultados: results })
        })
        .catch((error) => {
            res.status(417).json({ title: 'error', status: error.errno, message: msgError(error) });
        })
});

module.exports = router;
