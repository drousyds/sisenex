var express = require('express');
var router = express.Router();
var AreasTematicas = require('../objectionDatabase/queries/areasTematicas')

router.get("/", (req, res, next) => {

    AreasTematicas
        .getAreasTematicas(req.query)
        .then((resultados) => res.status(200).json({ resultados }))
        .catch(e => next(e))
});

router.post("/", (req, res) => {
    AreasTematicas
        .postAreaTematica(req.body)
        .then((resultados) => res.status(200).json({ resultados }))
        .catch(e => next(e))
});

router.get("/:idAreaTematica", (req, res, next) => {
    AreasTematicas
        .getAreaTematica(req.params)
        .then((resultados) => res.status(200).json({ resultados }))
        .catch(e => next(e))
});

router.delete("/:idAreaTematica", (req, res) => {
    AreasTematicas
        .delAreaTematica(req.params)
        .then((resultados) => res.status(200).json({ resultados }))
        .catch(e => next(e))
});

module.exports = router;