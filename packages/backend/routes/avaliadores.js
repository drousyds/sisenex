var express = require("express");
var router = express.Router();
var knex = require("../knexinstance");
var msgError = require("../helpers/errosSQL");
var _ = require("lodash");
var Pessoas = require("../objectionDatabase/models/model_pessoas");
var Avaliacoes = require("../objectionDatabase/models/model_avaliacoes");
var Pessoas_Apresentacoes = require("../objectionDatabase/models/model_pessoasApresentacoes");
var Pessoas_Projetos = require("../objectionDatabase/models/model_pessoasProjetos");
var Monitores = require("../objectionDatabase/queries/monitores");
const Avaliadores = require("../objectionDatabase/queries/avaliadores");

router.get("/", (req, res, next) => {
  Avaliadores.getAvaliadores(req.query)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.post("/", (req, res, next) => {
  Pessoas.query()
    .insert(req.body)
    .then(insertedId => {
      res.status(201).json({ resultados: insertedId });
    })
    .catch(e => next(e));
});

router.get("/:idPessoa/:idVinculoPessoa", (req, res, next) => {
  Avaliadores.getAvaliador(req.params, req.query)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.get("/:idPessoa/:idVinculoPessoa/avaliacoes", (req, res, next) => {
  Avaliadores.getAvaliacoes(req.params, req.query)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.post("/:idPessoa/:idVinculoPessoa/avaliacoes", (req, res) => {
  /*
        Fluxo:
        1- Procura pela apresentacao que o projeto avaliado está alocado
        2- Verifica se o avaliador está apto a avaliar e se a apresentacao está aberta
        3- Avalia
        CATCH- Qualquer problema a transacao é cancelada e o erro reportado
    */

  let subqueryDisp = knex("Apresentacoes")
    .select("disponibilidadeApresentacao")
    .join("Projetos", "Apresentacoes.idApresentacao", "Projetos.idApresentacao")
    .where("Projetos.idProjeto", req.body.idProjeto)
    .as("disponibilidadeApresentacao");

  var simpleError;
  knex
    .transaction(function (trx) {
      knex("Projetos")
        .select("idApresentacao", "id2Apresentacao")
        .where("idProjeto", req.body.idProjeto)
        .transacting(trx)
        .then(ids => {
          if (ids[0])
            return knex("Pessoas_Apresentacoes")
              .select(
                "aptidaoPessoa as aptidaoPessoaApresentacao",
                subqueryDisp.transacting(trx)
              )
              .where(ids[0])
              .where({
                idPessoa: req.params.idPessoa,
                idVinculoPessoa: req.params.idVinculoPessoa
              })
              .on("query", data => {
                console.log(data.sql);
              });
        })
        .then(apts => {
          if (apts.length == 0)
            throw new Error("Avaliador não está na apresentacao");
          if (
            apts[0].aptidaoPessoaApresentacao == 1 &&
            apts[0].disponibilidadeApresentacao == 1
          ) {
            let avaliacao = [];
            req.body.avaliacao.map(p => {
              avaliacao.push({
                idPergunta: p.idPergunta,
                notaAvaliacao: p.notaAvaliacao,
                idProjeto: req.body.idProjeto,
                idPessoa: req.params.idPessoa,
                idVinculoPessoa: req.params.idVinculoPessoa,
                dataAvaliacao: knex.fn.now()
              });
            });
            return knex.raw(
              `${knex("Avaliacoes")
                .insert(avaliacao)
                .toQuery()} ON CONFLICT (idAvaliacao) DO UPDATE SET notaAvaliacao = EXCLUDED.notaAvaliacao, dataAvaliacao = ${knex.fn.now()}`
            );
          } else {
            simpleError =
              "Avaliador não está apto a avaliar ou a tertúlia está fechada";
            trx.rollback();
          }
        })
        .then(trx.commit)
        .catch(trx.rollback);
    })
    .then(affectedRows => {
      res.status(200).json({
        resultados: `Avaliacao feita. ${
          affectedRows[0].affectedRows
          } colunas alteradas`
      });
    })
    .catch(error => {
      if (!simpleError)
        res.status(417).json({
          title: "error",
          status: error.errno,
          message: msgError(error)
        });
      else res.status(417).json({ title: "error", message: simpleError });
    });
});

router.delete("/:idPessoa/:idVinculoPessoa/avaliacoes", (req, res) => {
  //deletar avaliacao de um avaliador
  Avaliacoes.query()
    .delete()
    .where({
      idProjeto: req.body.idProjeto,
      idPessoa: req.params.idPessoa,
      idVinculoPessoa: req.params.idVinculoPessoa
    })
    .then(results => {
      res.status(200).json({ resultados: results });
    })
    .catch(error => {
      res.status(417).json({
        title: "error",
        status: error.errno,
        message: msgError(error)
      });
    });
});

router.post(
  "/:idPessoa/:idVinculoPessoa/apresentacoes/:idApresentacao/:id2Apresentacao",
  (req, res) => {
    /*
        Associa um avaliador a uma apresentacao
        Fluxo:
        1- Cria o registro do avaliador na tabela de Pessoas_Apresentacoes
        2- Seleciona todos os projetos alocados àquela apresentacao
        3- Cria um registro na tabela de Pessoas_Projetos para cada projeto em (2)
        CATCH-Qualquer erro chama o rollback() e o mesmo é reportado ao cliente



        */

    // knex.transaction(function notRetPromise(trx) {
    //   async function returningPromise() {
    //     try {
    //       req.params.aptidaoPessoa = 1;
    //       await trx("Pessoas_Apresentacoes").insert(req.params);

    //       let projetos = await trx("Projetos")
    //         .select("idProjeto")
    //         .where({
    //           idApresentacao: req.params.idApresentacao,
    //           id2Apresentacao: req.params.id2Apresentacao
    //         });

    //       let insertValues = projetos.map(p => {
    //         return {
    //           idProjeto: p.idProjeto,
    //           idPessoa: req.params.idPessoa,
    //           idVinculoPessoa: req.params.idVinculoPessoa,
    //           idApresentacao: req.params.idApresentacao,
    //           id2Apresentacao: req.params.id2Apresentacao,
    //           aptidaoPessoa: 0
    //         };
    //       });
    //       let affectedRows = await trx("Pessoas_Projetos").insert(
    //         insertValues
    //       );

    //       trx.commit();
    //       res.status(200).json({ resultados: affectedRows });
    //     } catch (error) {
    //       trx.rollback();
    //       res.status(417).json({
    //         title: "error",
    //         status: error.errno,
    //         message: msgError(error)
    //       });
    //     }
    //   }

    //   returningPromise();
    // });

    Monitores.baterAvaliador(req.params);
  }
);

router.put(
  "/:idPessoa/:idVinculoPessoa/apresentacoes/:idApresentacao/:id2Apresentacao",
  (req, res) => {
    //atualiza aptidao de um avaliador numa apresentacao
    req.body.aptidaoPessoa == 1
      ? Monitores.baterAvaliador(req.params)
        .then(results => {
          res.status(200).json({ resultados: results });
        })
        .catch(error => {
          res.status(417).json({
            title: "error",
            status: error.errno,
            message: msgError(error)
          });
        })
      : Monitores.removerAvaliadorDaApresentacao(req.params)
        .then(results => {
          res.status(200).json({ resultados: results });
        })
        .catch(error => {
          res.status(417).json({
            title: "error",
            status: error.errno,
            message: msgError(error)
          });
        });
  }
);

router.get(
  //recupera os projetos da apresentacao que o avaliador está
  "/:idPessoa/:idVinculoPessoa/apresentacoes/:idApresentacao/:id2Apresentacao/projetos",
  (req, res, next) => {
    Avaliadores.getProjetosParaAvaliar(req.params)
      .then(results => res.status(200).json({ resultados: results }))
      .catch(e => next(e));
  }
);

router.get("/:idPessoa/:idVinculoPessoa/apresentacoes", (req, res, next) => {
  //pesquisa apresentacao ativa de um avaliador
  Avaliadores.getApresentacaoAtiva(req.params)
    .then(results => res.status(200).json({ resultados: results }))
    .catch(e => next(e));
});

module.exports = router;
