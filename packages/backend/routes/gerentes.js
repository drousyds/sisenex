var express = require('express');
var router = express.Router();
var execSQLQuery = require('../helpers/execSQLQuery');

router.get("/", (req, res) => {
    execSQLQuery(`SELECT * FROM Pessoas WHERE gerentePessoa = 1`, [], res);
});

router.post("/", (req, res) => {
    var data = [
        req.body.idPessoa,
        req.body.idVinculoPessoa,
        req.body.matriculaPessoa,
        req.body.nomePessoa,
        req.body.nomeSocialPessoa,
        req.body.lotacaoPessoa,
        req.body.emailPessoa,
        req.body.telefonePessoa,
        req.body.aptidaoPessoa,
        req.body.avaliadorPessoa,
        req.body.monitorPessoa
    ];
    execSQLQuery(
        `INSERT INTO Pessoas (idPessoa, idVinculoPessoa, matriculaPessoa, nomePessoa, nomeSocialPessoa, lotacaoPessoa, emailPessoa, telefonePessoa,
           aptidaoPessoa, avaliadorPessoa, monitorPessoa, gerentePessoa) VALUES (?,?,?,?,?,?,?,?,?,?,?,1)`,
        data,
        res
    );
});

router.get("/:idPessoa/:idVinculoPessoa", (req, res) => {
    data = [req.params.idPessoa, req.params.idVinculoPessoa];
    execSQLQuery(
        "SELECT * FROM Pessoas WHERE gerentePessoa = 1 AND idPessoa = ? AND idVinculoPessoa = ?",
        data,
        res
    );
});

router.put("/:idPessoa/:idVinculoPessoa", (req, res) => {
    var data = [
        req.body.matriculaPessoa,
        req.body.nomePessoa,
        req.body.nomeSocialPessoa,
        req.body.cpfPessoa,
        req.body.lotacaoPessoa,
        req.body.emailPessoa,
        req.body.telefonePessoa,
        req.body.aptidaoPessoa,
        req.body.avaliadorPessoa,
        req.body.monitorPessoa,
        req.body.gerentePessoa,
        req.params.idPessoa,
        req.params.idVinculoPessoa
    ];

    execSQLQuery(
        `UPDATE Pessoas SET(matriculaPessoa = ?, nomePessoa = ?, nomeSocialPessoa = ?, cpfPessoa = ?, 
          lotacaoPessoa = ?, emailPessoa = ?, telefonePessoa = ?, aptidaoPessoa = ?, avaliadorPessoa = ?, 
          monitorPessoa = ?, gerentePessoa = ?) WHERE idPessoa = ? AND idVinculoPessoa = ? AND gerentePessoa = 1`,
        data,
        res
    );
});
router.delete("/:idPessoa/:idVinculoPessoa", (req, res) => {
    execSQLQuery(
        "DELETE FROM Pessoas WHERE idPessoa = ? AND idVinculoPessoa = ?",
        [req.params.id],
        res
    );
});
module.exports = router;