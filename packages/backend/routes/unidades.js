var express = require('express');
var router = express.Router();
var execSQLQuery = require('../helpers/execSQLQuery');


router.get("/", (req, res) => {
    execSQLQuery("SELECT * FROM Unidades", [], res);
});

router.post("/", (req, res) => {
    var data = [
        req.body.idUnidade,
        req.body.codigoUnidade,
        req.body.nomeUnidade,
        req.body.siglaUnidade,
        req.body.hierarquiaUnidade,
        req.body.unidadeGestora,
        req.body.tipoUnidade
    ];
    execSQLQuery(
        "INSERT INTO Unidades (idUnidade, codigoUnidade, nomeUnidade," +
        "siglaUnidade, hierarquiaUnidade, unidadeGestora, tipoUnidade) VALUES (?,?,?,?,?,?,?)",
        data,
        res
    );
});

router.get("/:id", (req, res) => {
    execSQLQuery(
        "SELECT * FROM Unidades WHERE idUnidade = ?",
        [req.params.id],
        res
    );
});

router.put("/:id", (req, res) => {
    var data = [
        req.body.codigoUnidade,
        req.body.nomeUnidade,
        req.body.siglaUnidade,
        req.body.hierarquiaUnidade,
        req.body.unidadeGestora,
        req.body.tipoUnidade,
        req.params.id
    ];

    execSQLQuery(
        `UPDATE Unidades SET codigoUnidade = ?, nomeUnidade = ?,` +
        `siglaUnidade = ?, hierarquiaUnidade = ?, unidadeGestora = ?, tipoUnidade = ? WHERE idUnidade = ?`,
        data,
        res
    );
});

router.delete("/:id", (req, res) => {
    execSQLQuery(
        "DELETE FROM Unidades WHERE idUnidade = ?",
        [req.params.id],
        res
    );
});

router.delete("/:id", (req, res) => {
    execSQLQuery(
        "DELETE FROM Unidades WHERE idUnidade = ?",
        [req.params.id],
        res
    );
    // var bla = req.params.op;
    // console.log(bla);
});

module.exports = router;