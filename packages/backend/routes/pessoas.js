var express = require('express');
var router = express.Router();
var Pessoas = require('../objectionDatabase/queries/pessoas');

router.get('/', (req, res, next) => {
  Pessoas.getPessoas(req.query)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.get('/:idPessoa/:idVinculoPessoa', (req, res) => {
  Pessoas.getPessoa(req.params)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.put('/:idPessoa/:idVinculoPessoa', (req, res, next) => {
  Pessoas.putPessoa(req.params, req.body)
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

router.post('/', (req, res, next) => {
  //existe ? retornaOK : cria e retorna OK
  Pessoas.postPessoa({ input: req.body })
    .then(resultados => res.status(200).json({ resultados }))
    .catch(e => next(e));
});

module.exports = router;
