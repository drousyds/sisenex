const {
  wrapError,
  DBError,
  UniqueViolationError,
  NotNullViolationError,
  DataError
} = require('db-errors');

const { ValidationError, NotFoundError } = require('objection');

module.exports = function(err, req, res, next) {
  const wrapped = wrapError(err);
  console.log(err);

  //Parse objection errors
  if (wrapped instanceof ValidationError)
    res.status(400).json({
      message: err.message
    });
  else if (wrapped instanceof NotFoundError)
    res.status(404).json({
      message: err.message
    });
  //Parse db errors
  else if (wrapped instanceof UniqueViolationError)
    res.status(500).json({
      message: `Unique constraint failed`
    });
  else if (wrapped instanceof NotNullViolationError)
    res.status(500).json({
      message: `Not null contraint failed`
    });
  else if (wrapped instanceof DataError)
    res.status(500).json({
      message: `Data error`
    });
  else if (wrapped instanceof DBError)
    res.status(500).json({
      message: `Database error`,
      code: err.errno
    });
  //Parse generic errors
  else if (err.status == undefined)
    res.status(500).json({
      message: err.message
    });
  else
    res.status(err.status).json({
      message: err.message
    });
};
