import { AxiosRequestConfig, AxiosResponse, AxiosError } from "axios"
import axios from "axios"

const makeRequest = async <T>(
  config: AxiosRequestConfig
): Promise<AxiosResponse<T>> => {
  let res: AxiosResponse<T>
  res = await axios(config)
  return res
}

export default makeRequest
