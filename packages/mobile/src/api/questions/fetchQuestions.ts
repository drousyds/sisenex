import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import { Pergunta } from "generated/graphql"

export const fetchQuestions = (): AxiosRequestConfig => ({
  method: "GET",
  timeout: 2000,
  url: baseUrl + `/perguntas`
})

export default () => makeRequest<Pergunta>(fetchQuestions())
