import {
  Apresentacao,
  Projeto,
  AreaTematica,
  Unidade,
  Pessoa,
  NotaPergunta,
  Pergunta
} from "generated/graphql"
import { AxiosResponse } from "axios"

export type SuccessData<T> = AxiosResponse<{
  resultados: T
}>

export type StartReviewRequest = Pick<
  Apresentacao,
  "idApresentacao" | "id2Apresentacao"
> &
  Pick<Projeto, "idProjeto">

export type StartReviewResponse = SuccessData<number>

export type SubmitReviewRequest = {
  idPessoa: Pessoa["idPessoa"]
  idVinculoPessoa: Pessoa["idVinculoPessoa"]
  idProjeto: Projeto["idProjeto"]
  avaliacao: {
    idPergunta: number
    notaAvaliacao: number
  }[]
}

export type SubmitReviewResponse = SuccessData<string>

export type FetchCurrentApresentacaoRequest = Pick<
  Pessoa,
  "idPessoa" | "idVinculoPessoa"
>

export type FetchCurrentApresentacaoResponse = SuccessData<
  Pick<Apresentacao, "idApresentacao" | "id2Apresentacao">[]
>

export type FetchPendingProjectsRequest = Pick<
  Pessoa,
  "idPessoa" | "idVinculoPessoa"
> &
  Pick<Apresentacao, "idApresentacao" | "id2Apresentacao">

export type FetchPendingProjectsResponse = SuccessData<
  Pick<Projeto, "idProjeto" | "tituloProjeto">[]
>

export type FetchAvaliacoesRequest = Pick<
  Pessoa,
  "idPessoa" | "idVinculoPessoa"
>

export type FetchAvaliacoesResponse = SuccessData<
  {
    idProjeto: Projeto["idProjeto"]
    tituloProjeto: Projeto["tituloProjeto"]
    avaliacoes: ({
      idPergunta: Pergunta["idPergunta"]
      notaAvaliacao: NotaPergunta["nota"]
    })[]
  }[]
>

export type FetchCurrentUserRequest = Pick<
  Pessoa,
  "idPessoa" | "idVinculoPessoa"
>

export type FetchCurrentUserResponse = SuccessData<
  Pick<
    Pessoa,
    | "idPessoa"
    | "idVinculoPessoa"
    | "matriculaPessoa"
    | "nomePessoa"
    | "nomeSocialPessoa"
    | "lotacaoPessoa"
    | "emailPessoa"
    | "telefonePessoa"
    | "aptidaoPessoa"
    | "avaliadorPessoa"
    | "monitorPessoa"
    | "gerentePessoa"
  >
>

export type AddApresentacaoRequest = Pick<
  Apresentacao,
  "idApresentacao" | "id2Apresentacao"
>

export type AddApresentacaoResponse = SuccessData<
  Pick<
    Apresentacao,
    | "idApresentacao"
    | "id2Apresentacao"
    | "salaApresentacao"
    | "horaApresentacao"
    | "disponibilidadeApresentacao"
  > & {
    projetos: {
      idProjeto: number
      tituloProjeto: string
      descricaoProjeto: string
      dataInicioProjeto: string
      liberacaoProjeto: string
      dataFimProjeto: string
      anoProjeto: string
      idAreaTematica: number
      idUnidade: number
      //campusProjeto: number
      nomeUnidade: string
      nomeAreaTematica: string
    }[]
  }
>

export type FetchReviewerRequest = Pick<Pessoa, "idPessoa" | "idVinculoPessoa">

export type FetchReviewerResponse = SuccessData<
  Pick<Pessoa, "idPessoa" | "idVinculoPessoa" | "nomeSocialPessoa">
>

export type FetchApresentacaoReviewersRequest = Pick<
  Apresentacao,
  "id2Apresentacao" | "idApresentacao"
>

export type FetchApresentacaoReviewersResponse = SuccessData<
  Pick<Pessoa, "idPessoa" | "idVinculoPessoa" | "nomeSocialPessoa">[]
>

export type AddReviewerRequest = Pick<
  Apresentacao,
  "id2Apresentacao" | "idApresentacao"
> &
  Pick<Pessoa, "idPessoa" | "idVinculoPessoa">

export type AddReviewerResponse = Promise<boolean>

export type RemoveReviewerRequest = Pick<
  Apresentacao,
  "id2Apresentacao" | "idApresentacao"
> &
  Pick<Pessoa, "idPessoa" | "idVinculoPessoa">

export type RemoveReviewerResponse = SuccessData<boolean>

export type CloseApresentacaoRequest = Pick<
  Apresentacao,
  "id2Apresentacao" | "idApresentacao"
>

export type CloseApresentacaoResponse = SuccessData<number>

export type ApiLoginRequest = Pick<
  Pessoa,
  | "idPessoa"
  | "idVinculoPessoa"
  | "matriculaPessoa"
  | "nomePessoa"
  | "nomeSocialPessoa"
  | "lotacaoPessoa"
  | "emailPessoa"
  | "telefonePessoa"
  | "aptidaoPessoa"
  | "avaliadorPessoa"
  | "monitorPessoa"
  | "gerentePessoa"
>

export type ApiLoginResponse = SuccessData<
  Pick<
    Pessoa,
    | "idPessoa"
    | "idVinculoPessoa"
    | "matriculaPessoa"
    | "nomePessoa"
    | "nomeSocialPessoa"
    | "lotacaoPessoa"
    | "emailPessoa"
    | "telefonePessoa"
    | "aptidaoPessoa"
    | "avaliadorPessoa"
    | "monitorPessoa"
    | "gerentePessoa"
  >
>
export type OpenApresentacaoRequest = Pick<
  Apresentacao,
  "id2Apresentacao" | "idApresentacao"
>

export type OpenApresentacaoResponse = SuccessData<number>

export type OauthLoginResponse = {
  vinculos: Pick<
    Pessoa,
    | "idPessoa"
    | "idVinculoPessoa"
    | "matriculaPessoa"
    | "nomePessoa"
    | "nomeSocialPessoa"
    | "lotacaoPessoa"
    | "emailPessoa"
    | "telefonePessoa"
    | "aptidaoPessoa"
    | "avaliadorPessoa"
    | "monitorPessoa"
    | "gerentePessoa"
  >[]
  oauthToken: string | null
}

export interface FetchApresentacaoProjectsRequest {
  idApresentacao: Apresentacao["idApresentacao"]
  id2Apresentacao: Apresentacao["id2Apresentacao"]
}

export type FetchApresentacaoProjectsResponse = SuccessData<
  ApresentacaoResponseData & {
    projetos: ProjetosResponseData[]
  }
>

type ApresentacaoResponseData = Pick<
  Apresentacao,
  | "idApresentacao"
  | "id2Apresentacao"
  | "salaApresentacao"
  | "horaApresentacao"
  | "disponibilidadeApresentacao"
>

type ProjetosResponseData = Pick<
  Projeto,
  | "idProjeto"
  | "tituloProjeto"
  | "descricaoProjeto"
  | "dataInicioProjeto"
  | "dataFimProjeto"
  | "anoProjeto"
> & {
  nomeAreaTematica: AreaTematica["nomeAreaTematica"]
  nomeUnidade: Unidade["nomeUnidade"]
}
