import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import { FetchAvaliacoesRequest, FetchAvaliacoesResponse } from "../types"

export const fetchAvaliacaoes = (
  args: FetchAvaliacoesRequest
): AxiosRequestConfig => ({
  method: "GET",
  timeout: 2000,
  url:
    baseUrl + `/avaliadores/${args.idPessoa}/${args.idVinculoPessoa}/avaliacoes`
})

export default (args: FetchAvaliacoesRequest) =>
  makeRequest<FetchAvaliacoesResponse>(fetchAvaliacaoes(args))
