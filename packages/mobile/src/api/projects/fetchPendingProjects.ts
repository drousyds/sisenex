import makeRequest from "../makeRequest"
import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import {
  FetchPendingProjectsRequest,
  FetchPendingProjectsResponse
} from "api/types"

export const fetchPendingProjects = (
  args: FetchPendingProjectsRequest
): AxiosRequestConfig => ({
  method: "GET",
  timeout: 2000,
  url:
    baseUrl +
    `/avaliadores/${args.idPessoa}/${args.idVinculoPessoa}/apresentacoes/${
      args.idApresentacao
    }/${args.id2Apresentacao}/projetos`
})

export default (args: FetchPendingProjectsRequest) =>
  makeRequest<FetchPendingProjectsResponse>(fetchPendingProjects(args))
