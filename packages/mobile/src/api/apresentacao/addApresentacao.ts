import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import { AddApresentacaoRequest, AddApresentacaoResponse } from "../types"

export const addApresentacaoConfig = (
  args: AddApresentacaoRequest
): AxiosRequestConfig => ({
  method: "GET",
  timeout: 2000,
  url:
    baseUrl +
    `/apresentacoes/${args.idApresentacao}/${args.id2Apresentacao}/projetos`
})

export default (args: AddApresentacaoRequest) =>
  makeRequest<AddApresentacaoResponse>(addApresentacaoConfig(args))
