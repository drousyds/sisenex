import { AddReviewerRequest, AddReviewerResponse } from "../types"
import axios, { AxiosResponse } from "axios"
import { baseUrl } from "constants/constants"

// This particular request was not updated to the makeRequest
// format due to its composite nature

const addReviewer = async (args: AddReviewerRequest): AddReviewerResponse => {
  let res: AxiosResponse<any>
  let url =
    baseUrl +
    `/avaliadores/${args.idPessoa}/${args.idVinculoPessoa}/apresentacoes/${
      args.idApresentacao
    }/${args.id2Apresentacao}`
  try {
    res = await axios.post(url)
    if (res.status == 200) return true
    else return false
  } catch (e) {
    return false
  }
}


export default addReviewer
