import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import { OpenApresentacaoRequest, OpenApresentacaoResponse } from "../types"

export const openApresentacaoConfig = (
  args: OpenApresentacaoRequest
): AxiosRequestConfig => ({
  method: "PUT",
  timeout: 2000,
  data: {
    disponibilidadeApresentacao: 1
  },
  url: baseUrl + `/apresentacoes/${args.idApresentacao}/${args.id2Apresentacao}`
})

export default (args: OpenApresentacaoRequest) =>
  makeRequest<OpenApresentacaoResponse>(openApresentacaoConfig(args))
