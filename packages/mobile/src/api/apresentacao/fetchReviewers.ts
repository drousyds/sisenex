import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import {
  FetchApresentacaoReviewersRequest,
  FetchApresentacaoReviewersResponse
} from "../types"

export const fetchReviewers = (
  args: FetchApresentacaoReviewersRequest
): AxiosRequestConfig => ({
  method: "GET",
  timeout: 2000,
  url:
    baseUrl +
    `/apresentacoes/${args.idApresentacao}/${args.id2Apresentacao}/avaliadores`
})

export default (args: FetchApresentacaoReviewersRequest) =>
  makeRequest<FetchApresentacaoReviewersResponse>(fetchReviewers(args))
