import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import { FetchReviewerRequest, FetchReviewerResponse } from "../types"

export const fetchReviewerConfig = (
  args: FetchReviewerRequest
): AxiosRequestConfig => ({
  method: "GET",
  timeout: 2000,
  url: baseUrl + `/avaliadores/${args.idPessoa}/${args.idVinculoPessoa}`
})

export default (args: FetchReviewerRequest) =>
  makeRequest<FetchReviewerResponse>(fetchReviewerConfig(args))
