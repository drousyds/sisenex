import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import {
  FetchApresentacaoProjectsRequest,
  FetchApresentacaoProjectsResponse
} from "../types"

export const fetchProjectsConfig = (
  args: FetchApresentacaoProjectsRequest
): AxiosRequestConfig => ({
  method: "GET",
  timeout: 2000,
  url:
    baseUrl +
    `/apresentacoes/${args.idApresentacao}/${args.id2Apresentacao}/projetos`
})

export default (args: FetchApresentacaoProjectsRequest) =>
  makeRequest<FetchApresentacaoProjectsResponse>(fetchProjectsConfig(args))
