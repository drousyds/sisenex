import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import { CloseApresentacaoRequest, CloseApresentacaoResponse } from "../types"

export const closeApresentacaoConfig = (
  args: CloseApresentacaoRequest
): AxiosRequestConfig => ({
  method: "PUT",
  timeout: 2000,
  data: {
    disponibilidadeApresentacao: 0
  },
  url: baseUrl + `/apresentacoes/${args.idApresentacao}/${args.id2Apresentacao}`
})

export default (args: CloseApresentacaoRequest) =>
  makeRequest<CloseApresentacaoResponse>(closeApresentacaoConfig(args))
