import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import {
  FetchCurrentApresentacaoRequest,
  FetchCurrentApresentacaoResponse
} from "../types"

export const fetchCurrentApresentacaoConfig = (
  args: FetchCurrentApresentacaoRequest
): AxiosRequestConfig => ({
  method: "GET",
  timeout: 2000,
  url:
    baseUrl +
    `/avaliadores/${args.idPessoa}/${args.idVinculoPessoa}/apresentacoes`
})

export default (args: FetchCurrentApresentacaoRequest) =>
  makeRequest<FetchCurrentApresentacaoResponse>(
    fetchCurrentApresentacaoConfig(args)
  )
