import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import { RemoveReviewerRequest, RemoveReviewerResponse } from "../types"

export const removeReviewer = (
  args: RemoveReviewerRequest
): AxiosRequestConfig => ({
  method: "PUT",
  timeout: 2000,
  data: {
    aptidaoPessoa: 0
  },
  url:
    baseUrl +
    `/avaliadores/${args.idPessoa}/${args.idVinculoPessoa}/apresentacoes/${
      args.idApresentacao
    }/${args.id2Apresentacao}`
})

export default (args: RemoveReviewerRequest) =>
  makeRequest<RemoveReviewerResponse>(removeReviewer(args))
