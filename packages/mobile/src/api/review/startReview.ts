import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import { StartReviewRequest, StartReviewResponse } from "../types"

export const startReviewConfig = (
  args: StartReviewRequest
): AxiosRequestConfig => ({
  method: "PUT",
  timeout: 2000,
  data: {
    disponibilidadeProjeto: 1
  },
  url:
    baseUrl +
    `/apresentacoes/${args.idApresentacao}/${args.id2Apresentacao}/projetos/${
      args.idProjeto
    }`
})

export default (args: StartReviewRequest) =>
  makeRequest<StartReviewResponse>(startReviewConfig(args))
