import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import { SubmitReviewRequest, SubmitReviewResponse } from "../types"

export const submitReview = (
  args: SubmitReviewRequest
): AxiosRequestConfig => ({
  method: "POST",
  timeout: 2000,
  data: {
    idProjeto: args.idProjeto,
    avaliacao: args.avaliacao
  },
  url:
    baseUrl + `/avaliadores/${args.idPessoa}/${args.idVinculoPessoa}/avaliacoes`
})

export default (args: SubmitReviewRequest) =>
  makeRequest<SubmitReviewResponse>(submitReview(args))
