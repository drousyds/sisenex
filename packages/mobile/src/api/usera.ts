import { Pessoa, Apresentacao } from "generated/graphql"
import {
  REACT_NATIVE_API_URL,
  REACT_NATIVE_API_PREFIX
} from "constants/constants"
import axios from "axios"

const baseUrl = REACT_NATIVE_API_URL + REACT_NATIVE_API_PREFIX

export const fetchCurrentUser = async (
  args: Pick<Pessoa, "idPessoa" | "idVinculoPessoa">
): Promise<Pessoa | false> => {
  let url
  try {
    url = baseUrl + `/pessoas/${args.idPessoa}/${args.idVinculoPessoa}`
    let res = await axios.get(url)
    return res.data.resultados[0]
  } catch (e) {
    return false
  }
}

export const fetchCurrentApresentacao = async (
  args: Pick<Pessoa, "idPessoa" | "idVinculoPessoa">
): Promise<Apresentacao | false> => {
  const url =
    baseUrl +
    `/avaliadores/${args.idPessoa}/${args.idVinculoPessoa}/apresentacoes`
  try {
    let res = await axios.get(url)
    let resultados = res.data.resultados
    if (resultados.length == 0) return false
    return resultados[0]
  } catch (e) {
    return false
  }
}
