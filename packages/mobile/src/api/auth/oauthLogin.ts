import * as WebBrowser from "expo-web-browser"
import { AuthSessionOptions, AuthSessionResult } from "expo-auth-session"
import sessionUrlProvider from "expo-auth-session/build/SessionUrlProvider"
import { getQueryParams } from "expo-auth-session/build/QueryParams"
import AuthSession from "expo-auth-session/build/AuthSession"
import axios, { AxiosResponse } from "axios"
import { OauthLoginResponse } from "../types"
import getVinculos from "helpers/getVinculos"
import getChannel from "helpers/getChannel"
import { Pessoa } from "generated/graphql"

axios.defaults.withCredentials = true

// Configure o endpoint de descoberta
const discovery = {
  authorizationEndpoint: "https://sistemas.ufpb.br/auth-server/oauth/authorize",
  tokenEndpoint: "https://sistemas.ufpb.br/auth-server/oauth/token"
}

// Codigo copiado de implementacao previamente removida do SDK oficial nesse commit: https://github.com/expo/expo/commit/158d69135fee56355520cba299a68e2b97849dda
/**
 * Initiate a proxied authentication session with the given options. Only one AuthSession can be active at any given time in your application.
 * If you attempt to open a second session while one is still in progress, the second session will return a value to indicate that AuthSession is locked.
 *
 * @param options An object of type AuthSessionOptions.
 * @return Returns a Promise that resolves to an AuthSessionResult object.
 */
export async function startAsync(
  options: AuthSessionOptions
): Promise<AuthSessionResult> {
  const authUrl = options.authUrl
  // Prevent accidentally starting to an empty url
  if (!authUrl) {
    throw new Error(
      "No authUrl provided to AuthSession.startAsync. An authUrl is required -- it points to the page where the user will be able to sign in."
    )
  }

  const returnUrl =
    options.returnUrl || sessionUrlProvider.getDefaultReturnUrl()
  const startUrl = sessionUrlProvider.getStartUrl(
    authUrl,
    returnUrl,
    options.projectNameForProxy
  )
  const showInRecents = options.showInRecents || false

  let result: WebBrowser.WebBrowserAuthSessionResult
  result = await _openWebBrowserAsync(startUrl, returnUrl, showInRecents)

  // Handle failures
  if (!result) {
    throw new Error("Unexpected missing AuthSession result")
  }
  if (!("url" in result)) {
    if ("type" in result) {
      return result
    } else {
      throw new Error("Unexpected AuthSession result with missing type")
    }
  }

  const { params, errorCode } = getQueryParams(result.url)

  return {
    type: errorCode ? "error" : "success",
    params,
    errorCode,
    authentication: null,
    url: result.url
  }
}

async function _openWebBrowserAsync(
  startUrl: string,
  returnUrl: string,
  showInRecents: boolean
) {
  const result = await WebBrowser.openAuthSessionAsync(startUrl, returnUrl, {
    showInRecents
  })
  if (result.type === "cancel" || result.type === "dismiss") {
    return { type: result.type }
  }

  return result
}

async function oauthLogin(): Promise<OauthLoginResponse | false> {
  try {
    let redirectUri = "https%3A%2F%2Fauth.expo.io%2F%40v-in%2Fsisenex"
    let authUrl = `https://sistemas.ufpb.br/auth-server/oauth/authorize?response_type=code&client_id=sisenex&scope=projeto-extensao-read%20personal-read&redirect_uri=${redirectUri}`

    const result = (await startAsync({
      authUrl,
      projectNameForProxy: "@v-in/sisenex"
    })) as any

    let code = result.params.code

    // Resto do código permanece o mesmo
    const tokenUrl = `${discovery.tokenEndpoint}?code=${code}&grant_type=authorization_code&redirect_uri=${redirectUri}`
    let tokenRes: AxiosResponse<{ access_token: string }> = await axios({
      method: "post",
      url: tokenUrl,
      withCredentials: true,
      headers: {
        Authorization:
          "Basic c2lzZW5leDo5M2ZiODc4YzQ4MzM5ZDBlNWM4MzgwN2YxNjFjYjY1ZA"
      }
    })

    const token = tokenRes.data.access_token

    const baseOauthUrl_2 = "https://api.ufpb.br"
    let res = await axios.get(
      `${baseOauthUrl_2}/api/personal/vinculos/ativos`,
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    )

    console.log("Vinculos RES:", res.data)
    let data = res.data
    let vinculos = getVinculos(data)
    return {
      vinculos: vinculos,
      oauthToken: token
    }
  } catch (e) {
    console.log("Erro no oauth:", e)
    if (getChannel() == "devel") {
      // ... código de mock permanece o mesmo ...
      const fakeVinculos: Pessoa[] = [
        {
          idPessoa: "384574",
          idVinculoPessoa: "306643",
          nomeSocialPessoa: "VINICIUS MONITOR",
          matriculaPessoa: "20170200093",
          lotacaoPessoa: "CI-Centro de Informatica",
          monitorPessoa: 1,
          aptidaoPessoa: 1,
          avaliadorPessoa: 0,
          gerentePessoa: 0
        },
        {
          idPessoa: "384574",
          idVinculoPessoa: "392929",
          nomeSocialPessoa: "VINICIUS AVAVALIADOR",
          matriculaPessoa: "20170200095",
          lotacaoPessoa: "CCHLA",
          monitorPessoa: 0,
          aptidaoPessoa: 1,
          avaliadorPessoa: 1,
          gerentePessoa: 0
        }
      ]
      return {
        vinculos: fakeVinculos,
        oauthToken: null
      }
    }
    return false
  }
}

export default oauthLogin
