import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import { FetchCurrentUserRequest, FetchCurrentUserResponse } from "../types"

export const fetchCurrentUserConfig = (
  args: FetchCurrentUserRequest
): AxiosRequestConfig => ({
  method: "GET",
  timeout: 2000,
  url: baseUrl + `/pessoas/${args.idPessoa}/${args.idVinculoPessoa}`
})

export default (args: FetchCurrentUserRequest) =>
  makeRequest<FetchCurrentUserResponse>(fetchCurrentUserConfig(args))
