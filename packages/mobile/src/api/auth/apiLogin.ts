import { AxiosRequestConfig } from "axios"
import { baseUrl } from "constants/constants"
import makeRequest from "../makeRequest"
import { ApiLoginRequest, ApiLoginResponse } from "../types"

export const apiLoginConfig = (args: ApiLoginRequest): AxiosRequestConfig => ({
  method: "POST",
  timeout: 10000,
  data: {
    ...args,
    lotacaoPessoa: args.lotacaoPessoa || undefined
  },
  url: baseUrl + `/pessoas`
})

export default (args: ApiLoginRequest) =>
  makeRequest<ApiLoginResponse>(apiLoginConfig(args))
