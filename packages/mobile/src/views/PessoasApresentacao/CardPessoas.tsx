import * as React from "react"
import { Text, View, StyleSheet, Image, Dimensions } from "react-native"
import RoundedCard, {
  CardTitle,
  CardSubtitle,
  CardBody,
  CardFooter,
  CardTitleContainer,
  CardFooterContent
} from "components/Cards/RoundedCard"
import {
  Pessoa,
  useRemoverAvaliadorMutation,
  ApresentacaoAtual
} from "generated/graphql"
import { useDispatch } from "react-redux"
import { navigate } from "redux/Navigation/actions"
import Separator from "components/Separator/Separator"
import Typography from "assets/css/typography"
import { Button } from "components/Button"
import { useApolloClient } from "react-apollo"
import { apresentacaoAtual } from "graphql/queries/apresentacaoAtual"
import { pessoasApresentacao } from "graphql/queries/pessoasApresentacao"
import { removerAvaliador } from "graphql/mutations/removerAvaliador"
import {
  cacheRemoverAvaliador,
  getApresentacaoAtual
} from "graphql/cacheHelpers"
import Placeholder from "rn-placeholder"
import useConfirmationDialog from "hooks/useConfirmationDialog"
import { ConfirmationDialog } from "components/Dialogs/ConfirmationDialog"

interface CardPessoasProps {
  nomePessoa: Pessoa["nomePessoa"]
  idVinculoPessoa: Pessoa["idPessoa"]
  idPessoa: Pessoa["idVinculoPessoa"]
  lotacaoPessoa: Pessoa["lotacaoPessoa"]
  matriculaPessoa: Pessoa["matriculaPessoa"]
}

const CardPessoas: React.SFC<CardPessoasProps> = ({
  nomePessoa,
  lotacaoPessoa,
  matriculaPessoa,
  idPessoa,
  idVinculoPessoa
}) => {
  const apresentacao: ApresentacaoAtual = getApresentacaoAtual()
  const [removerPessoa, { data, loading }] = useRemoverAvaliadorMutation({
    variables: {
      idPessoa,
      idVinculoPessoa,
      idApresentacao: apresentacao.idApresentacao,
      id2Apresentacao: apresentacao.id2Apresentacao
    },
    update: cache => {
      cacheRemoverAvaliador(cache, {
        idPessoa,
        idVinculoPessoa
      })
    }
  })

  const [confirmaRemover, dialogOptions] = useConfirmationDialog({
    onConfirm: removerPessoa,
    message: "Tem certeza que deseja remover o avaliador?"
  })

  return (
    <React.Fragment>
      <RoundedCard style={{ minHeight: 0 }}>
        <CardTitleContainer
          imageSource={{
            uri:
              "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDxUSDw8VFRUVFRUVFRUVFRUVFRUVFRUWFxUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDw0NDysZHxkrKysrKystLSsrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEBAAMBAQEAAAAAAAAAAAAAAQIEBQMGB//EADQQAQEAAQICCAMIAAcAAAAAAAABAgMRBCEFEjFBUWFxgZGx0SIyM0KhweHwExUjcoKS8f/EABUBAQEAAAAAAAAAAAAAAAAAAAAB/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A/XAFQAAAAAAAAAAAAAAAAAAAAQQBFQBBAUTcB6gAAAAAAAAAAAAAAAAAAAAlAQQAEASlQAQB7gAAAAAAAA8+I18dPHfL2nffQHpbt2tHX6Twx5YzrX4T4udxXF5al58p3Tu9/Frg3NTpLVvZZPSfV4XidS/ny+NeQD1nEak/Pl/2r20+kdWfm39Y1AHX0OlMbyzm3nOcb+OUs3l3njHzL14fictO7431ndQfRDw4XisdSbzt754fw9gEogAIAlEoCblQFRFBsAAAAAAAAx1dSY43K9kcDide6mW99p4RtdLcRvl1J2Tt9XPAAUAAAAAQGelq3DKZY9s/uzvcNrzUx60954XwfPNro3iOpntezLlf2qDuJRAAS0EQtKCAxAVN/MBtAAAAAAMdXPq423ulvwZNXpTLbSvntP1Bw8srbve280BQAAAABAAQAAH0HCavX08b5c/Wcq9Wh0Pl9izwvzjeQLWKpQEE3ASlrHcFVj7ANwAAAAABpdMfhz/dPlW61OlMd9K+Vl/X+QcMBQAAAAQAQABAB0+huzP/AI/u6NaHQ8+xb435T+W8gWoJQKhugFQrGgow3AdAAAAAABjq4dbG4+MsZAPmcptdr3I3+ltDq5dadmXz/v7tAABQQAEABFQBBs8BodfPn2Tnf2gOrwen1dOTy3vreb2pUqCVFrEBKJQLWFXdLQBFB0AAAAAAAAYa2lM8bjey/wB3cDiNG4ZdXL/2eL6J5cTw+Opjtl7XvgPnUe/E8Nlp37U5d17q8FBAABAEVno6OWd2xm/ynqDHTwuV2k513OF0Jp47Tt7741OE4Wac8be2/Tye1QEogCbjGgVjatrHcBLRLQN7/dhNwHTAAAAAAAABp6/SOnj2XrXy+oNrKSzazeeFc/X6LxvPC7eV5x4Z9K578sZJ571saXSmF+9Lj+sBo6nAauP5d/Tn/Lwy0sp2434V9BhrY5fdyl9LGYPm5pZd2N+FeunwWrl+Sz15fN3q89TVxx7cpPW7A0NHoufny38p2fFvYYTGbYzaNXW6S052b5Xy5T41qf5pnv8Adm3hz+YOsjT0eksMu37N8+c+Lbll5ygVKWpQGNq2sQRFrECpS1jaBuHuA6oAAAAADw4vi8dOc+d7p3/w8+P4yac2nPK9nl51xM8rbvbvb3g9uJ4vPU7by8J2fy1xFAAEZTOzst+NYgLlqZXtyvxrBUAQQCvTQ4jLTv2b7d19nkgO1wvG46nLsy8Po2bXze7q8DxvW+zlftd18f5QbtQqAlSlS0E3SiABsgOwAAAA8uK15p43K+08a9XE6V1+tntOzHl79/0BqamdyttvOsQUQABAAQQBAoIgAgVAQl74IDtcHxH+Jj5zt+r33cTg9bqZy915X0dm1ArHdWIG6bralA3v9omyg7AAAAPPidXqYXLwn69z5u12emc9sJPG/L+xxgEBQQAEKgKggDGrUBA3QCoJQEEAdngtXrac8uV9nFrf6Kz+9PS/X9kHQ3CoCBuAbVUAdkAAEByumrzxnlf12+jmOj0197H0/dzgEVFBFQBBAEpUAQKCIAJQqAJSsQG10Zf9T2v1aja6O/E9qg61SrUARUA9/wBRPcB2wQCoAOT0z97H0/dznQ6Z+9j6fu5wAIoIFBEViACAWpRAEEARaxBKm62oBWz0b+J7Vqtno38T2oOuiogVFQF2F6oDsoAIgA5PTX3sfS/NzgAQFGNKAJSoAlRQGNIAMQARjQBKgAlbfRv4k9KgDrxP5BBFv9/QAAAf/9k="
          }}
        >
          <CardTitle
            style={{
              flexDirection: "row"
            }}
            textStyle={{
              textAlign: "left",
              marginRight: 50,
              flexWrap: "wrap"
            }}
          >
            {nomePessoa}
          </CardTitle>
          <CardSubtitle style={{ marginRight: 22 }}>
            {`${lotacaoPessoa ||
              "Lotação indisponível"}${"\n"}${matriculaPessoa}`}
          </CardSubtitle>
        </CardTitleContainer>
        <CardFooter style={{ justifyContent: "flex-end" }}>
          <Button
            title="Remover da Sala"
            loading={loading}
            secondary
            style={{ width: "100%", marginTop: 5 }}
            onPress={confirmaRemover}
          />
        </CardFooter>
      </RoundedCard>
      <Separator vertical size={14} />
      <ConfirmationDialog {...dialogOptions} />
    </React.Fragment>
  )
}

export const PlaceholderPessoa: React.SFC = () => {
  return (
    <React.Fragment>
      <RoundedCard style={{ minHeight: 155 }}>
        <CardTitleContainer
          imageSource={{
            uri:
              "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDxUSDw8VFRUVFRUVFRUVFRUVFRUVFRUWFxUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDw0NDysZHxkrKysrKystLSsrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEBAAMBAQEAAAAAAAAAAAAAAQIEBQMGB//EADQQAQEAAQICCAMIAAcAAAAAAAABAgMRBCEFEjFBUWFxgZGx0SIyM0KhweHwExUjcoKS8f/EABUBAQEAAAAAAAAAAAAAAAAAAAAB/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A/XAFQAAAAAAAAAAAAAAAAAAAAQQBFQBBAUTcB6gAAAAAAAAAAAAAAAAAAAAlAQQAEASlQAQB7gAAAAAAAA8+I18dPHfL2nffQHpbt2tHX6Twx5YzrX4T4udxXF5al58p3Tu9/Frg3NTpLVvZZPSfV4XidS/ny+NeQD1nEak/Pl/2r20+kdWfm39Y1AHX0OlMbyzm3nOcb+OUs3l3njHzL14fictO7431ndQfRDw4XisdSbzt754fw9gEogAIAlEoCblQFRFBsAAAAAAAAx1dSY43K9kcDide6mW99p4RtdLcRvl1J2Tt9XPAAUAAAAAQGelq3DKZY9s/uzvcNrzUx60954XwfPNro3iOpntezLlf2qDuJRAAS0EQtKCAxAVN/MBtAAAAAAMdXPq423ulvwZNXpTLbSvntP1Bw8srbve280BQAAAABAAQAAH0HCavX08b5c/Wcq9Wh0Pl9izwvzjeQLWKpQEE3ASlrHcFVj7ANwAAAAABpdMfhz/dPlW61OlMd9K+Vl/X+QcMBQAAAAQAQABAB0+huzP/AI/u6NaHQ8+xb435T+W8gWoJQKhugFQrGgow3AdAAAAAABjq4dbG4+MsZAPmcptdr3I3+ltDq5dadmXz/v7tAABQQAEABFQBBs8BodfPn2Tnf2gOrwen1dOTy3vreb2pUqCVFrEBKJQLWFXdLQBFB0AAAAAAAAYa2lM8bjey/wB3cDiNG4ZdXL/2eL6J5cTw+Opjtl7XvgPnUe/E8Nlp37U5d17q8FBAABAEVno6OWd2xm/ynqDHTwuV2k513OF0Jp47Tt7741OE4Wac8be2/Tye1QEogCbjGgVjatrHcBLRLQN7/dhNwHTAAAAAAAABp6/SOnj2XrXy+oNrKSzazeeFc/X6LxvPC7eV5x4Z9K578sZJ571saXSmF+9Lj+sBo6nAauP5d/Tn/Lwy0sp2434V9BhrY5fdyl9LGYPm5pZd2N+FeunwWrl+Sz15fN3q89TVxx7cpPW7A0NHoufny38p2fFvYYTGbYzaNXW6S052b5Xy5T41qf5pnv8Adm3hz+YOsjT0eksMu37N8+c+Lbll5ygVKWpQGNq2sQRFrECpS1jaBuHuA6oAAAAADw4vi8dOc+d7p3/w8+P4yac2nPK9nl51xM8rbvbvb3g9uJ4vPU7by8J2fy1xFAAEZTOzst+NYgLlqZXtyvxrBUAQQCvTQ4jLTv2b7d19nkgO1wvG46nLsy8Po2bXze7q8DxvW+zlftd18f5QbtQqAlSlS0E3SiABsgOwAAAA8uK15p43K+08a9XE6V1+tntOzHl79/0BqamdyttvOsQUQABAAQQBAoIgAgVAQl74IDtcHxH+Jj5zt+r33cTg9bqZy915X0dm1ArHdWIG6bralA3v9omyg7AAAAPPidXqYXLwn69z5u12emc9sJPG/L+xxgEBQQAEKgKggDGrUBA3QCoJQEEAdngtXrac8uV9nFrf6Kz+9PS/X9kHQ3CoCBuAbVUAdkAAEByumrzxnlf12+jmOj0197H0/dzgEVFBFQBBAEpUAQKCIAJQqAJSsQG10Zf9T2v1aja6O/E9qg61SrUARUA9/wBRPcB2wQCoAOT0z97H0/dznQ6Z+9j6fu5wAIoIFBEViACAWpRAEEARaxBKm62oBWz0b+J7Vqtno38T2oOuiogVFQF2F6oDsoAIgA5PTX3sfS/NzgAQFGNKAJSoAlRQGNIAMQARjQBKgAlbfRv4k9KgDrxP5BBFv9/QAAAf/9k="
          }}
        >
          <CardTitle textStyle={{ textAlign: "left", marginRight: 22 }} element>
            <Placeholder.Box
              animate="fade"
              color="#cccccc"
              radius={8}
              width={200}
              height={14}
            />
            <Placeholder.Box
              animate="fade"
              color="#cccccc"
              radius={8}
              width={120}
              height={14}
            />
          </CardTitle>
        </CardTitleContainer>
        <CardFooter style={{ justifyContent: "flex-end" }}>
          <Placeholder.Box
            animate="fade"
            color="#cccccc"
            radius={8}
            width={220}
            height={30}
          />
        </CardFooter>
      </RoundedCard>
      <Separator vertical size={14} />
    </React.Fragment>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    flex: 0
  },
  image: {
    width: 48,
    height: 60,
    borderRadius: 6,
    marginHorizontal: 5
  },
  textContainer: {
    paddingLeft: 2
  },
  cardTittleText: {
    ...Typography.header2,
    fontSize: 16,
    lineHeight: 16
  },
  cardBodyText: {
    ...Typography.subtitle,
    fontSize: 12,
    lineHeight: 16,
    color: "#A0A0A0",
    fontWeight: "100"
  }
})

export default CardPessoas
