import * as React from "react"
import { View, StyleSheet, TouchableHighlight, BackHandler } from "react-native"
import { ScreenContainer } from "components/ScreenContainer"
import { NavigationScreenProp } from "react-navigation"
import * as SplashScreen from 'expo-splash-screen'
import { useDispatch } from "react-redux"
import { navigate } from "redux/Navigation/actions"
import { ScreenTitle } from "components/ScreenTitle"
import Separator from "components/Separator/Separator"
import CardPessoas from "./CardPessoas"
import Colors from "assets/css/colors"
import { MaterialIcons } from "@expo/vector-icons"
import {
  usePessoasApresentacaoQuery,
  ApresentacaoAtual
} from "generated/graphql"
import { useApolloClient } from "react-apollo"
import { SmallLoadingState } from "components/StateIndicator/LoadingState"
import EmptyState from "components/StateIndicator/EmptyState"
import { getApresentacaoAtual } from "graphql/cacheHelpers"
import { AndroidBackHandler } from "react-navigation-backhandler"
import { useFocusEffect } from '@react-navigation/native';


interface PessoasApresentacaoScreenProps {
  navigation: NavigationScreenProp<{}>
}

const PessoasApresentacaoScreen: React.SFC<
  PessoasApresentacaoScreenProps
> = props => {
  React.useEffect(() => {
    SplashScreen.hideAsync()
  }, [])
  const client = useApolloClient()
  const apresentacao: ApresentacaoAtual = getApresentacaoAtual()
  const onBackPress = () => true
  const dispatch = useDispatch()
  const navigateToRegistroPessoa = () =>
    dispatch(
      navigate({
        routeName: "RegistroPessoa"
      })
    )

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [])
  );

  const { data, loading, error } = usePessoasApresentacaoQuery({
    variables: {
      idApresentacao: apresentacao.idApresentacao,
      id2Apresentacao: apresentacao.id2Apresentacao
    },
    pollInterval: 5000
  })

  if (loading) {
    return (
      <ScreenContainer>
        <ScreenTitle
          title="Pessoas"
          subtitle="Escaneie ou digitalize as credenciais dos avaliadores para adicioná-los a apresentação"
        />
        <SmallLoadingState />
      </ScreenContainer>
    )
  }

  if (error || !(data && data.apresentacao && data.apresentacao.avaliadores)) {
    return (
      <ScreenContainer>
        <ScreenTitle
          title="Pessoas"
          subtitle="Escaneie ou digitalize as credenciais dos avaliadores para adicioná-los a apresentação"
        />
        <EmptyState />
      </ScreenContainer>
    )
  }

  return (
    <React.Fragment>
      <BotaoAdicionarPessoa onPress={navigateToRegistroPessoa} />
      <ScreenContainer>
        <ScreenTitle
          title="Pessoas"
          subtitle="Escaneie ou digitalize as credenciais dos avaliadores para adicioná-los a apresentação"
        />
        {data.apresentacao.avaliadores &&
          data.apresentacao.avaliadores.map(
            (
              {
                idPessoa,
                idVinculoPessoa,
                nomePessoa,
                lotacaoPessoa,
                matriculaPessoa
              },
              index
            ) => (
                <CardPessoas
                  idPessoa={idPessoa}
                  idVinculoPessoa={idVinculoPessoa}
                  key={`${idPessoa}-${index}`}
                  nomePessoa={nomePessoa}
                  lotacaoPessoa={lotacaoPessoa}
                  matriculaPessoa={matriculaPessoa}
                />
              )
          )}
        <Separator vertical size={80} />
      </ScreenContainer>
    </React.Fragment>
  )
}

const BotaoAdicionarPessoa: React.SFC<{ onPress: () => void }> = props => {
  return (
    <View style={styles.floatingActionButton}>
      <TouchableHighlight onPress={props.onPress}>
        <MaterialIcons name="add" color="white" size={30} />
      </TouchableHighlight>
    </View>
  )
}

const styles = StyleSheet.create({
  floatingActionButton: {
    position: "absolute",
    bottom: 23,
    right: 32,
    width: 60,
    height: 60,
    zIndex: 10,
    backgroundColor: Colors.primary,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 30
  }
})

export default PessoasApresentacaoScreen
