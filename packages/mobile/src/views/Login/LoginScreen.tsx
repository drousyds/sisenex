import { StyleSheet, Animated, Image } from "react-native"
import React, { useRef, useEffect } from "react"
import { Button } from "components/Button"
import Colors from "assets/css/colors"
import { useSelector, useDispatch } from "react-redux"
import {
  mockMonitorLogin,
  mockReviewerLogin,
  mockReviewerLogin2,
  oauthLogin
} from "redux/Auth/actions"
import { RootState } from "redux/rootReducer"
import Separator from "components/Separator/Separator"
import Creditos from "./Creditos"
import * as SplashScreen from "expo-splash-screen"
import RadioDialog from "components/Dialogs/RadioDialog"
import getChannel from "helpers/getChannel"
import * as WebBrowser from "expo-web-browser"

WebBrowser.maybeCompleteAuthSession()

const LoginScreen = () => {
  useEffect(() => {
    SplashScreen.hideAsync()
  }, [])
  const loading = useSelector(
    (state: RootState) => state.auth.loginStatus.loading
  )
  const dispatch = useDispatch()
  const opacity = useRef(new Animated.Value(0))
  // Mostra botoes de login fake quando estamos no ambiente devel no expo deployed
  // ou caso esteja funcionando atraves do yarn start
  const channel = getChannel()
  const showMockLogin =
    channel === "devel" ||
    //channel === "staging" ||
    process.env.NODE_ENV === "development"

  useEffect(() => {
    Animated.timing(opacity.current, {
      toValue: 1,
      duration: 1002,
      useNativeDriver: true
    }).start()
  }, [])

  return (
    <React.Fragment>
      <Animated.View
        style={[styles.screenContainerStyle, { opacity: opacity.current }]}
      >
        <Image
          source={require("assets/MarcaSisEnexVertica.png")}
          style={styles.logoStyle}
        />
        <Separator vertical size={32} />
        {showMockLogin && (
          <Button
            large
            title={"Monitor Teste"}
            onPress={() => dispatch(mockMonitorLogin())}
          />
        )}
        <Separator vertical size={32} />
        {showMockLogin && (
          <Button
            large
            title={"Avaliador Teste"}
            onPress={() => dispatch(mockReviewerLogin2())}
          />
        )}
        <Separator vertical size={32} />
        <Button
          large
          title={"Login"}
          loading={loading}
          onPress={() => dispatch(oauthLogin.request())}
        />
        <Creditos />
        <RadioDialog />
      </Animated.View>
    </React.Fragment>
  )
}

const styles = StyleSheet.create({
  screenContainerStyle: {
    ...StyleSheet.absoluteFillObject,
    display: "flex",
    backgroundColor: "white",
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },

  logoStyle: {
    width: 200,
    height: 200,
    marginTop: "0%",
    marginBottom: "0%",
    resizeMode: "contain",
    zIndex: 10
  },

  buttonStyle: {
    width: 200,
    backgroundColor: Colors.primary,
    paddingVertical: 8,
    zIndex: 1
  },

  buttonTextStyle: {
    fontWeight: "500"
  }
})

export default LoginScreen