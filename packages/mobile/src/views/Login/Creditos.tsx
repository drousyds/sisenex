import * as React from "react"
import {
  View,
  Text,
  Image,
  StyleSheet,
  PixelRatio,
  Dimensions
} from "react-native"
import Separator from "components/Separator/Separator"
import Typography from "assets/css/typography"
import appConfig from "../../../app.config"

interface CreditosProps {}

const Creditos: React.SFC<CreditosProps> = props => {
  return (
    <React.Fragment>
      <View style={styles.containerStyle}>
        <View style={[styles.contentContainer, { paddingLeft: 15 }]}>
          <Text style={styles.textStyle}>Desenvolvido</Text>
          <View style={{ flexDirection: "row" }}>
            <Image
              style={styles.imagemProex}
              resizeMode="stretch"
              source={require("assets/LogoProex.png")}
            />
            <Separator size={8} />
            <Image
              style={styles.imagemLumo}
              resizeMode="stretch"
              source={require("assets/lumo.png")}
            />
          </View>
        </View>
        <Separator size={8} />
        <View style={[styles.contentContainer, { paddingHorizontal: 13 }]}>
          <Text style={styles.textStyle}>Apoio</Text>
          <View style={{ flexDirection: "row" }}>
            <Image
              style={styles.imagemSti}
              resizeMode="stretch"
              source={require("assets/sti.png")}
            />
          </View>
        </View>
      </View>
      <View
        style={{
          position: "absolute",
          bottom: 16,
          borderRadius: 2,
          flexDirection: "row"
        }}
      >
        <View>
          <Text style={[Typography.overline, { opacity: 0.2 }]}>
            Versão {appConfig.expo.android.versionCode}
          </Text>
        </View>
        <Separator size={40} />
        <View>
          <Text style={[Typography.overline, { opacity: 0.2 }]}>
            4e40c2ece924
          </Text>
        </View>
      </View>
    </React.Fragment>
  )
}

const pRatio = PixelRatio.get()
const logoHeight = pRatio == 1 ? 40 : 40 / (pRatio * 0.45)
const logoWidth = pRatio == 1 ? 100 : 100 / (pRatio * 0.45)

const windowWidth =
  Dimensions.get("screen").width * Dimensions.get("window").scale
const windowHeight =
  Dimensions.get("screen").height * Dimensions.get("window").scale
const selectLogoHeight = (windowHeight: number) => {
  if (windowHeight >= 1920) {
    return logoHeight
  } else if (windowHeight >= 1520) {
    return logoHeight * 0.66
  } else {
    return logoHeight / 2
  }
}
const selectLogoWidth = (windowWidth: number) => {
  if (windowWidth >= 1080) {
    return logoWidth
  } else if (windowWidth >= 720) {
    return logoWidth * 0.66
  } else {
    return logoWidth / 2
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    position: "absolute",
    bottom: 20,
    width: "auto",
    height: 80,
    // backgroundColor: "papayawhip",
    alignItems: "center",
    marginHorizontal: "auto",
    flexDirection: "row",
    justifyContent: "center"
  },

  textStyle: { fontSize: 10, color: "grey" },
  imagemProex: {
    height: selectLogoHeight(windowHeight),
    width: selectLogoWidth(windowWidth),
    marginLeft: 4,
    marginRight: 4,
    alignSelf: "flex-end"
  },
  imagemLumo: {
    height: selectLogoHeight(windowHeight),
    width: selectLogoWidth(windowWidth),
    marginBottom: 5,
    alignSelf: "flex-end"
  },
  imagemSti: {
    height: selectLogoHeight(windowHeight),
    width: selectLogoWidth(windowWidth),
    marginRight: 4,
    marginLeft: 4,
    alignSelf: "flex-end",
    bottom: -5
  },
  contentContainer: {
    flexDirection: "column",
    height: 60
  }
})

export default Creditos
