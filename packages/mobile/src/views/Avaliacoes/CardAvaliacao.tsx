import * as React from "react"
import { RoundedCard } from "components/Cards"
import { material } from "react-native-typography"
import { Text, View, StyleSheet } from "react-native"
import PrimaryButton from "components/Button/Button"
import Separator from "components/Separator/Separator"
import { Projeto } from "generated/graphql"
import {
  CardTitle,
  CardFooter,
  CardBody,
  CardFooterContent
} from "components/Cards/RoundedCard"
import getScoreColor from "helpers/getScoreColor"
import { useDispatch } from "react-redux"
import { navigate } from "redux/Navigation/actions"
import Typography from "assets/css/typography"

interface CardAvaliacaoProps {
  idProjeto: Projeto["idProjeto"]
  tituloProjeto: Projeto["tituloProjeto"]
  coordenadorProjeto: string
  media: number | null
}

const CardAvaliacao: React.FC<CardAvaliacaoProps> = ({
  idProjeto,
  tituloProjeto,
  media,
  coordenadorProjeto
}) => {
  const dispatch = useDispatch()
  const avaliarProjeto = () =>
    dispatch(
      navigate({
        routeName: "AvaliacaoProjeto",
        params: {
          projectId: idProjeto,
          podeAvaliar: false,
          tituloProjeto
        }
      })
    )
  if (!media) return <View />

  return (
    <React.Fragment key={idProjeto}>
      <RoundedCard>
        <CardTitle fullText>{tituloProjeto}</CardTitle>
        <CardBody>{`Coordenador(a): ${coordenadorProjeto}`}</CardBody>
        <CardFooter responsive>
          <React.Fragment>
            <CardFooterContent>
              <Text style={[material.headline, { fontSize: 18 }]}>Média </Text>
              <Text
                style={[
                  Typography.mediaAvaliacao,
                  {
                    paddingLeft: 6,
                    color: getScoreColor(media)
                  }
                ]}
              >
                {parseFloat(media.toFixed(2))}
              </Text>
            </CardFooterContent>
            <View>
              <PrimaryButton title="Visualizar" onPress={avaliarProjeto} />
            </View>
          </React.Fragment>
        </CardFooter>
      </RoundedCard>
      <Separator vertical />
    </React.Fragment>
  )
}

const styles = StyleSheet.create({})

export default CardAvaliacao
