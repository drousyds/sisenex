import * as React from "react"
import CardAvaliacao from "./CardAvaliacao"
import { ScreenContainer } from "components/ScreenContainer"
import { ScreenTitle } from "components/ScreenTitle"
import {
  useAvaliacoesRealizadasQuery,
  UsuarioAtualQuery
} from "generated/graphql"
import { RoundedCard } from "components/Cards"
import { useApolloClient } from "react-apollo"
import { usuarioAtual } from "graphql/queries/usuarioAtual"
import EmptyState from "components/StateIndicator/EmptyState"

interface AvaliacoesScreenProps {}

const dummyArray = new Array(8).fill(0)

const AvaliacoesScreen: React.FC<AvaliacoesScreenProps> = () => {
  const client = useApolloClient()
  const usuarioAtualData = client.readQuery<UsuarioAtualQuery>({
    query: usuarioAtual
  })
  const { data, loading, error } = useAvaliacoesRealizadasQuery({
    pollInterval: 5000,
    variables: {
      idPessoa: usuarioAtualData!.usuarioAtual.idPessoa,
      idVinculoPessoa: usuarioAtualData!.usuarioAtual.idVinculoPessoa
    }
  })

  const isEmpty =
    data &&
    data.pessoa &&
    (data.pessoa.__typename == "Gerente" ||
      data.pessoa.__typename == "Avaliador") &&
    data.pessoa.avaliacoes &&
    data.pessoa.avaliacoes.length == 0
  //const { data, loading, error } = mockAvaliacoesRealizadasQuery()

  if (loading)
    return (
      <ScreenContainer>
        <ScreenTitle
          title="Histórico"
          subtitle="Os projetos avaliados por você poderão ser consultados aqui."
        />
        {dummyArray.map((_, index) => (
          <RoundedCard key={index} placeholder />
        ))}
      </ScreenContainer>
    )

  if (error || isEmpty)
    return (
      <ScreenContainer>
        <ScreenTitle
          title="Histórico"
          subtitle="Os projetos avaliados por você poderão ser consultados aqui."
        />
        <EmptyState />
      </ScreenContainer>
    )

  return (
    <ScreenContainer>
      <ScreenTitle
        title="Histórico"
        subtitle="Os projetos avaliados por você poderão ser consultados aqui."
      />
      {(data &&
        data.pessoa &&
        (data.pessoa.__typename == "Gerente" ||
          data.pessoa.__typename == "Avaliador") &&
        data.pessoa.avaliacoes &&
        data.pessoa.avaliacoes.map(
          ({ media, projeto: { idProjeto, tituloProjeto, mediaIndividual } }) => (
            <CardAvaliacao
              key={idProjeto}
              coordenadorProjeto="Indisponivel"
              media={mediaIndividual?.mediaProjeto}
              idProjeto={idProjeto}
              tituloProjeto={tituloProjeto}
            />
          )
        )) ||
        false}
    </ScreenContainer>
  )
}

export default AvaliacoesScreen
