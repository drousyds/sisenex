import * as React from "react"
import { View, Text, StyleSheet } from "react-native"
import { ScreenContainer } from "components/ScreenContainer"
import { NavigationScreenProp } from "react-navigation"
import { ScreenTitle } from "components/ScreenTitle"
import { useDispatch } from "react-redux"
import { BackButton } from "components/Header"
import { navigate } from "redux/Navigation/actions"
import PrimaryButton from "components/Button/Button"
import Colors from "assets/css/colors"

interface AvisosLegaisScreenProps {
  navigation: NavigationScreenProp<{}>
}

const AvisosLegaisScreen: React.FC<AvisosLegaisScreenProps> = props => {
  const dispatch = useDispatch()
  const navigateToSolicitacaoLGPD = () =>
    dispatch(
      navigate({
        routeName: "SolicitacaoLGPD"
      })
    )

  return (
    <ScreenContainer>
      <BackButton />
      <ScreenTitle
        style={{
          paddingTop: 8
        }}
        title={"Avisos Legais"}
        subtitle={"Este aviso de privacidade deve ser lido em conjunto com nossa Política de Privacidade"}
      />
      <TextoAvisosLegais />
      <PrimaryButton
        large
        title="Continuar"
        style={{ marginVertical: 16, alignSelf: 'center' }}
        onPress={navigateToSolicitacaoLGPD}
      />

    </ScreenContainer>
  )
}

const TextoAvisosLegais: React.FC = props => {

  return (
    <View style={styles.container}>
      <Text style={styles.body}>
        O Sistema Sisenex é desenvolvido pelo Laboratório de Computação Ubíqua e
        Móvel (LUMO) do Centro de Informática da UFPB. O LUMO se compromete com
        os termos explicitados na Lei Geral de Proteção de Dados do Brasil (LGPD).
      </Text>
      <Text style={styles.body}>
        Seus dados pessoais são utilizados no Sisenex para ﬁns especíﬁcos do
        processo de avaliação dos programas de extensão da Universidade da Paraíba. Os
        tratamentos sob seus dados pessoais são justiﬁcados pelo seu consentimento
        explícito destes termos de condições e para cumprir obrigações legais determinadas
        pela administração pública.
      </Text>
      <Text style={styles.body}>
        O SisEnex faz uso de recursos de segurança reconhecidos para proteger as
        informações contidas nos servidores e base de dados. Qualquer incidente de
        segurança será divulgado na página do sisenex.ufpb.br e através dos e-mails dos
        titulares apresentando as informações de danos, direitos e ações dos titulares.
      </Text>
      <Text style={styles.body}>
        Todo o processo de recolhimento de credenciais funcionará sob
        responsabilidade da STI - Superintendência de Tecnologia da Informação da UFPB.
        Em nenhum momento o SisEnex terá acesso ao seu nome de usuário e senha
        utilizados para acesso ao sistema. O Sisenex provém atendimento aos direitos dos
        titulares expressos na LGPD através de requisição explícita no site ou aplicativo
        móvel.
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    minHeight: 200,
    display: "flex",
    backgroundColor: "white",
    borderRadius: 10,
    paddingHorizontal: 22,
    paddingVertical: 18,
    justifyContent: "flex-start",
    elevation: 2,
    marginHorizontal: 2,
    shadowColor: "#518091",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 16,
    shadowOpacity: 0.2
  },
  section: {
    fontSize: 14,
    fontWeight: "bold",
    letterSpacing: 0.4,
    color: Colors.primaryText
  },
  body: {
    fontSize: 14,
    fontWeight: "normal",
    letterSpacing: 0.4,
    color: Colors.secondaryText,
    paddingVertical: 10
  },
  direitos: {
    fontSize: 14,
    fontWeight: "normal",
    letterSpacing: 0.4,
    color: Colors.secondaryText,
    paddingVertical: 6
  }
})

export default AvisosLegaisScreen;
