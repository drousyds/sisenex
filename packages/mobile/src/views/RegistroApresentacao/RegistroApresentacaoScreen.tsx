import * as React from "react"
import { Dimensions, BackHandler } from "react-native"
import { ScreenContainer } from "components/ScreenContainer"
import { NavigationScreenProp } from "react-navigation"
import * as SplashScreen from "expo-splash-screen"
import { ClearButton } from "components/Button"
import { navigate } from "redux/Navigation/actions"
import { useDispatch } from "react-redux"
import { ScreenTitle } from "components/ScreenTitle"
import { CameraView } from "expo-camera"
import { BarcodeScanningResult } from "expo-camera/build/Camera.types"
import { useIniciarApresentacaoMutation } from "generated/graphql"
import { updateApresentacaoAtualCache } from "graphql/cacheHelpers"
import { withCameraPermission } from "helpers/withCameraPermission"
import { BackButton } from "components/Header"
import { logout } from "redux/Auth/actions"
import { useFocusEffect } from "@react-navigation/native"

interface RegistroApresentacaoScreenProps {
  navigation: NavigationScreenProp<{}>
}

const RegistroApresentacaoScreen: React.FC<RegistroApresentacaoScreenProps> = () => {
  React.useEffect(() => {
    SplashScreen.hideAsync()
  }, [])
  withCameraPermission()
  const dispatch = useDispatch()
  const onLogout = () => dispatch(logout())
  const { height } = Dimensions.get("window")
  const onBackPress = () => {
    onLogout()
    return true
  }

  const [
    iniciarApresentacao,
    { data, loading }
  ] = useIniciarApresentacaoMutation({
    onCompleted: data => {
      updateApresentacaoAtualCache(data)
      dispatch(
        navigate({
          routeName: "MonitorStack"
        })
      )
    },
    onError: error => {
      console.log(error)
    }
  })

  const onIdScanned = (result: BarcodeScanningResult) => {
    iniciarApresentacao({
      variables: {
        codigoApresentacao: result.data
      }
    })
  }
  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener("hardwareBackPress", onBackPress)
      return () =>
        BackHandler.removeEventListener("hardwareBackPress", onBackPress)
    }, [])
  )

  return (
    <ScreenContainer>
      <BackButton onPress={onLogout} />
      <ScreenTitle
        style={{ paddingTop: 8 }}
        title="Apresentação"
        subtitle="Escaneie o código para visualizar os projetos que foram alocados a essa apresentação."
      />
      <CameraView
        ratio="1:1"
        style={{
          marginHorizontal: -14,
          overflow: "hidden",
          height: height * 0.6,
          width: height * 0.6
        }}
        onBarcodeScanned={onIdScanned}
      />
      <ClearButton
        onPress={() =>
          dispatch(
            navigate({
              routeName: "RegistroApresentacaoInput"
            })
          )
        }
        title="Digitar Código"
      />
    </ScreenContainer>
  )
}

export default RegistroApresentacaoScreen
