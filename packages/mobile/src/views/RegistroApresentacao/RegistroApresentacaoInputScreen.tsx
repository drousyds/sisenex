import * as React from "react"
import { View, KeyboardAvoidingView } from "react-native"
import { ScreenContainer } from "components/ScreenContainer"
import { ScreenTitle } from "components/ScreenTitle"
import * as SplashScreen from "expo-splash-screen"
import { Button } from "components/Button"
import Separator from "components/Separator/Separator"
import { useDispatch } from "react-redux"
import { navigate } from "redux/Navigation/actions"
import { TextInput } from "components/TextInput"
import { useState } from "react"
import {
  useAtualizarDisponibilidadeApresentacaoMutation,
  useIniciarApresentacaoMutation
} from "generated/graphql"
import { updateApresentacaoAtualCache } from "graphql/cacheHelpers"
import { TextInputProps } from "components/TextInput/TextInput"
import { useMaskedInput } from "hooks/useMaskedInput"
import { NavigationScreenProp } from "react-navigation"
import useConfirmationDialog from "hooks/useConfirmationDialog"
import { ConfirmationDialog } from "components/Dialogs/ConfirmationDialog"
import { baseUrl } from "constants/constants"

interface RegistroApresentacaoInputScreenProps {
  navigation: NavigationScreenProp<{}>
}

export const RegistroApresentacaoInputScreen: React.FC<RegistroApresentacaoInputScreenProps> = props => {
  React.useEffect(() => {
    SplashScreen.hideAsync()
  }, [])
  const dispatch = useDispatch()
  const validator = (text: string) => text.match(/[a-zA-Z0-9\.]{8}/)
  const [dialog, dialogOptions] = useConfirmationDialog({
    message: "Código inválido"
  })
  const onConfirm = () => {
    if (valid) {
      iniciarApresentacao()
    }
  }
  const inputConfig = {
    maskType: "custom",
    maskOptions: {
      mask: "SSSSSSSS"
    },
    keyboardType: "default" as TextInputProps['keyboardType'],
    autoCapitalize: "characters" as TextInputProps['autoCapitalize'],
    validator,
    onSubmitEditing: onConfirm,
    focusOnMount: true,
    label: "Codigo"
  }

  const [inputProps, { valid, text, onSubmitEditing }] = useMaskedInput(inputConfig)

  const [iniciarApresentacao] = useIniciarApresentacaoMutation({
    variables: {
      codigoApresentacao: text
    },
    onError: err => {
      console.log('URL Base:', baseUrl)
      console.error('Erro ao iniciar apresentação:', err)
      dialog()
    },
    onCompleted: data => {
      if (data) {
        updateApresentacaoAtualCache(data)
        dispatch(
          navigate({
            routeName: "MonitorStack"
          })
        )
      }
    }
  })

  const onCancel = () => props.navigation.goBack()

  return (
    <ScreenContainer>
      <ScreenTitle
        subtitle="Digite o codigo da apresentação para continuar para a tela de projetos a serem apresentados"
        title="Apresentação"
      />
      <KeyboardAvoidingView behavior="padding">
        <Separator vertical size={32} />
        <TextInput {...inputProps} />
        <Separator vertical size={50} />
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center"
          }}
        >
          <Button title="Cancelar" grey onPress={onCancel} />
          <Separator size={12} />
          <Button
            title="Confirmar"
            onPress={() => onSubmitEditing(text)}
            disabled={!valid}
          />
        </View>
      </KeyboardAvoidingView>
      <ConfirmationDialog {...dialogOptions} />
    </ScreenContainer>
  )
}

export default RegistroApresentacaoInputScreen
