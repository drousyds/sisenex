import React from "react";
import { View, Text, StyleSheet } from "react-native"
import { material } from "react-native-typography"

interface InfoEventoProps {
    dataInicio: string
    dataFim: string
    nomeEvento: string
    status: string
}

const InforEvento: React.FC<InfoEventoProps> = ({
    nomeEvento,
    dataInicio,
    dataFim,
    status,
}) => {

    let aux
    let statusColor
    let statusTextColor

    if (status == 'CONFIRMADO') {
        statusColor = {
            backgroundColor: '#ffffcf',
            borderColor: '#ffff7a',
        }
        statusTextColor = {
            color: '#ffd700'
        }
    } else if (status == 'EM EXECUÇÃO') {
        aux = "EM EXECUÇÃO"
        statusColor = {
            backgroundColor: '#f6ffed',
            borderColor: '#b7eb8f',
        }
        statusTextColor = {
            color: '#52c41a'
        }
    } else {
        status = "FINALIZADO"
        statusColor = {
            backgroundColor: '#e9e9e9',
            borderColor: '#bdbebd',
        }
        statusTextColor = {
            color: '#3f3f3f'
        }
    }

    return (
        <View style={styles.containerFooter}>
            <View style={{ flexDirection: 'row' }}>
                <View style={styles.statusComp}>
                    <Text style={styles.textTitle}>{nomeEvento} -</Text>
                </View>
                <View style={[styles.statusText, statusColor]}>
                    <Text style={statusTextColor}>{status}</Text>
                </View>
            </View>
            <Text style={[material.caption, styles.textFooter]}>{dataInicio} - {dataFim}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    containerFooter: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    textTitle: {
        ...material.titleObject,
        textAlign: "center",
        lineHeight: 18,
        fontSize: 15,
        color: "#414141"
    },
    textFooter: {
        textAlign: "center",
        lineHeight: 18,
        fontSize: 15,
    },
    statusText: {
        marginLeft: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.4,
        width: 120,
        height: 30,
        marginBottom: 5,
        borderRadius: 2
    },
    statusComp: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 100,
        height: 30,
        marginBottom: 5
    }
})

export default InforEvento