import * as React from "react"
import {
  View,
  TouchableNativeFeedback,
  TouchableOpacity,
  Platform,
  Text,
  StyleSheet,
  ViewStyle,
  ActivityIndicator,
  Image
} from "react-native"
import Colors from "assets/css/colors"
import Typography from "assets/css/typography"
import Conferencia from "./Conferencia"

interface ExternalButtonProps {
  title: string
  onPress?: () => void
  loading?: boolean
  grey?: boolean
  style?: ViewStyle
  secondary?: ViewStyle
  large?: boolean
  disabled?: boolean
}

const ExternalButton: React.SFC<ExternalButtonProps> = props => {
  const onButtonPress = props.disabled ? undefined : props.onPress
  const backgroundColor =
    props.disabled || props.grey
      ? Colors.greyText
      : props.secondary
        ? "#A50100"
        : 'white'

  if (Platform.OS == "android")
    return (
      <TouchableNativeFeedback onPress={onButtonPress}>
        <View
          style={[
            styles.container,
            {
              width: props.large ? 228 : 144,
              backgroundColor,
              borderColor: 'gray',
              borderWidth: 1
            },
            props.style
          ]}
        >
          <Conferencia />
          {props.loading ? (
            <ActivityIndicator color="#fefefe" />
          ) : (
              <Text style={styles.buttonText}>{props.title}</Text>
            )}
        </View>
      </TouchableNativeFeedback>
    )
  else
    return (
      <TouchableOpacity onPress={onButtonPress}>
        <View
          style={[
            styles.container,
            {
              width: props.large ? 228 : 144,
              backgroundColor,
              borderColor: 'gray',
              borderWidth: 1,
            },
            props.style
          ]}
        >
          <Conferencia />
          <Text style={styles.buttonText}>{props.title}</Text>
        </View>
      </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    borderRadius: 8,
    paddingHorizontal: 8,
    height: 40,
  },

  buttonText: {
    ...Typography.button2,
    textAlign: "center",
    color: "gray",
    paddingLeft: 8
  }
})

export default ExternalButton
