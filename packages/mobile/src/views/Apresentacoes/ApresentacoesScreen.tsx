
import * as React from "react"
import { View, Image, Text, StyleSheet } from "react-native"
import { ScreenTitle } from "components/ScreenTitle"
import { ScreenContainer } from "components/ScreenContainer"
import CardApresentacao from "./CardApresentacao"
import { useApresentacaoAvaliadorQuery, useInfoEventoQuery, UsuarioAtualQuery } from "generated/graphql"
import RoundedCard from "components/Cards/RoundedCard"
import { useApolloClient } from "react-apollo"
import { usuarioAtual } from "graphql/queries/usuarioAtual"
import EmptyState from "components/StateIndicator/EmptyState"
import InfoEvento from "./InfoEvento"
import { mockApresentacaoAvaliador } from "graphql/queries/apresentacaoAvaliador"
import { material } from "react-native-typography"
import Separator from "components/Separator/Separator"

interface ApresentacoesScreenProps { }

const dummyArray = new Array(8).fill(0)

const ApresentacoesScreen: React.FC<ApresentacoesScreenProps> = () => {
  const client = useApolloClient()
  const usuarioAtualData = client.readQuery<UsuarioAtualQuery>({
    query: usuarioAtual
  })

  //const {data, loading, error} = mockApresentacaoAvaliador()

  const { data, loading, error } = useApresentacaoAvaliadorQuery({
    pollInterval: 5000,
    variables: {
      idPessoa: usuarioAtualData!.usuarioAtual.idPessoa,
      idVinculoPessoa: usuarioAtualData!.usuarioAtual.idVinculoPessoa
    }
  })

  const { data: eventData, loading: eventLoading, error: eventError } = useInfoEventoQuery({
    pollInterval: 5000
  })

  //const link = "https://meet.google.com/won-movm-tku"
  if (loading)
    return (
      <ScreenContainer>
        <ScreenTitle
          title="Avaliações"
          subtitle="Essas são as apresentações que você irá participar."
        />
        {dummyArray.map((_, index) => (
          <RoundedCard key={index} placeholder />
        ))}
      </ScreenContainer>
    )

  const isEmpty =
    data &&
    data.apresentacoesAvaliador &&
    (data.apresentacoesAvaliador.length == 0)

  const emptyEvents =
    eventData &&
    eventData.eventos &&
    (eventData.eventos.length == 0)

  if (
    !data ||
    data.apresentacoesAvaliador === null ||
    error ||
    isEmpty
  ) {
    return (
      <ScreenContainer>
        <ScreenTitle
          title="Avaliações"
          subtitle="Essas são as apresentações que você irá participar."
        />
        <EmptyState />
      </ScreenContainer>
    )
  }

  else return (
    <ScreenContainer>
      <ScreenTitle
        title="Avaliações"
        subtitle="Essas são as apresentações que você irá participar."
      />
      {data && data.apresentacoesAvaliador ?
        data!.apresentacoesAvaliador.map(
          ({ categoria, areaTematica, idApresentacao, linkApresentacao, horaApresentacao }, index) => (
            <CardApresentacao
              key={idApresentacao}
              idCategoria={categoria.idCategoria}
              categoriaApresentacao={categoria.nomeCategoria}
              temaApresentacao={areaTematica?.nomeAreaTematica}
              horaApresentacao={horaApresentacao!}
              idApresentacao={idApresentacao}
              tituloApresentacao={index + 1}
              link={linkApresentacao!}
            />
          )
        )
        : false}
      {
        !emptyEvents
          && eventData
          && eventData.eventos
          && eventData.eventos[0] ?
          <InfoEvento
            nomeEvento={eventData.eventos[0].nomeEvento}
            dataInicio={eventData.eventos[0].dataInicioEvento!}
            dataFim={eventData.eventos[0].dataFimEvento!}
            status={eventData.eventos[0].statusEvento}
          /> : false
      }
      <Separator vertical size={14} />
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  containerFooter: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  textTitle: {
    ...material.titleObject,
    textAlign: "center",
    lineHeight: 18,
    fontSize: 15,
    color: "#414141"
  },
  textFooter: {
    textAlign: "center",
    lineHeight: 18,
    fontSize: 15,
  }
})

export default ApresentacoesScreen