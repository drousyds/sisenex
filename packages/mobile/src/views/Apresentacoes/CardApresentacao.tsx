import * as React from "react"
import * as WebBrowser from "expo-web-browser"
import { RoundedCard } from "components/Cards"
import { StyleSheet } from "react-native"
import ExternalButton from "views/Apresentacoes/ExternalButton"
import PrimaryButton from "components/Button/Button"
import Separator from "components/Separator/Separator"
import {
  CardTitle,
  CardFooter,
  CardBody,
  CardFooterContent
} from "components/Cards/RoundedCard"
import { useDispatch } from "react-redux"
import { navigate } from "redux/Navigation/actions"
import { formatDate } from "helpers/formatDate"
import { useNavigation } from "@react-navigation/native"

interface CardApresentacoesProps {
  idCategoria: number
  idApresentacao: number
  tituloApresentacao: number | undefined
  categoriaApresentacao: string
  temaApresentacao: string | undefined
  link: string
  horaApresentacao: string
}

const CardApresentacao: React.SFC<CardApresentacoesProps> = ({
  idApresentacao,
  idCategoria,
  tituloApresentacao,
  categoriaApresentacao,
  horaApresentacao,
  temaApresentacao,
  link
}) => {
  const dispatch = useDispatch()
  function entrarNaSalaVirtual(link: string) {
    WebBrowser.openBrowserAsync(link, { showInRecents: true })
  }
  const navigation = useNavigation()

  const navagarProjeto = () =>
    navigation.navigate("ProjectsScreen", {
      screen: "ProjetosScreen",
      params: { idCategoria: idCategoria, idApresentacao: idApresentacao }
    })

  // useEffect(() => {
  //   console.log("idCategoria: ", idCategoria, "idApresentacao", idApresentacao)
  // }, [])

  return (
    <React.Fragment key={idApresentacao}>
      <RoundedCard>
        <CardTitle>{`Apresentação ${tituloApresentacao}`}</CardTitle>
        <CardBody>{`Hora: ${formatDate(horaApresentacao)}`}</CardBody>
        <CardBody>{`Categoria: ${categoriaApresentacao}`}</CardBody>
        <CardBody>{`Área temática: ${temaApresentacao}`}</CardBody>
        {idCategoria == 1 ? (
          <CardFooter responsive>
            <React.Fragment>
              <CardFooterContent style={{ alignItems: "baseline" }}>
                <ExternalButton
                  title="Entrar na sala virtual"
                  onPress={() => entrarNaSalaVirtual(link)}
                />
                <Separator />
                <PrimaryButton title="Visualizar" onPress={navagarProjeto} />
              </CardFooterContent>
            </React.Fragment>
          </CardFooter>
        ) : (
          <CardFooter style={{ alignItems: "center" }}>
            <PrimaryButton large title="Visualizar" onPress={navagarProjeto} />
          </CardFooter>
        )}
      </RoundedCard>
      <Separator vertical size={14} />
    </React.Fragment>
  )
}

const estilo = StyleSheet.create({})

export default CardApresentacao
