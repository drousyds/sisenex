import * as React from "react"
import { ScreenTitle } from "components/ScreenTitle"
import { ScreenContainer } from "components/ScreenContainer"
import { RoundedCard } from "components/Cards"
import Separator from "components/Separator/Separator"
import { View, Image, Text, StyleSheet } from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler"
import * as SplashScreen from 'expo-splash-screen'
import IconeConferencia from "./IconeConferencia"
import { useDispatch } from "react-redux"
import { navigate } from "redux/Navigation/actions"


interface TipoAvaliacaoScreenProps { }

const TipoAvaliacaoScreen: React.FC<TipoAvaliacaoScreenProps> = () => {
  const dispatch = useDispatch()

  React.useEffect(() => {
    SplashScreen.hideAsync()
  }, [])

  return (
    <ScreenContainer>
      <ScreenTitle
        title={"Tipo de Avaliação"}
        subtitle={"Escolha o tipo de projeto que você vai avaliar"}
      />
      <Separator vertical size={96} />
      <RoundedCard style={{ minHeight: 0 }}>
        <TouchableOpacity
          onPress={() => dispatch(navigate({ routeName: "ReviewerStack" }))}>
          <View style={styles.container}>
            <View>
              <Image
                style={styles.image}
                resizeMode="contain"
                source={require("assets/video.png")}
              />
            </View>
            <View style={styles.textContainer}>
              <Text style={{ fontSize: 32 }}>Vídeo</Text>
            </View>
          </View>
        </TouchableOpacity>
      </RoundedCard>
      <Separator vertical size={64} />
      <RoundedCard style={{ minHeight: 0 }}>
        <TouchableOpacity
          onPress={() => dispatch(navigate({ routeName: "ReviewerStack" }))}>
          <View style={styles.container}>
            <View style={{ marginRight: 16 }}>
              <IconeConferencia />
            </View>
            <View style={styles.textContainer}>
              <Text style={{ fontSize: 32 }}>e-Tertúlia</Text>
            </View>
          </View>
        </TouchableOpacity>
      </RoundedCard>
    </ScreenContainer>
  )
}
const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 8
  },
  image: {
    width: 64,
    height: 64,
    marginRight: 16
  },
  textContainer: {
    paddingLeft: 12,
    marginRight: 13,
  }
})

export default TipoAvaliacaoScreen