import Colors from 'assets/css/colors';
import { RoundedCard } from 'components/Cards';
import React, { useState } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Menu, List } from 'react-native-paper';

interface SolicitacaoSelectorProps {
  typeCaption: Array<String>,
  type: any,
  setType: Function,
}

const SolicitacaoSelector: React.FC<SolicitacaoSelectorProps> = props => {

  const [visible, setVisible] = useState(false);

  return (
    <View style={{ padding: 0, margin: 0, width: '100%' }}>
      <Menu
        visible={visible}
        onDismiss={() => setVisible(false)}
        style={{ width: '95%' }}
        anchor={
          <View style={{ alignItems: 'center' }}>
            <RoundedCard anyHeight={true} style={{ height: 70, width: '100%' }}>
              <TouchableOpacity onPress={() => setVisible(true)}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
                  <Text style={styles.textButton}>{props.typeCaption[props.type]}</Text>
                  <Image
                    source={visible ? (require('assets/collapse-icon.png')) : (require('assets/expand-icon.png'))}
                    resizeMode={"contain"}
                    style={{ width: 10, height: 10 }}
                  />
                </View>
              </TouchableOpacity>
            </RoundedCard>
          </View>
        }
      >
        <List.Item titleStyle={styles.selectorText} onPress={() => { props.setType(0); setVisible(false) }} title={props.typeCaption[0]} />
        <List.Item titleStyle={styles.selectorText} onPress={() => { props.setType(1); setVisible(false) }} title={props.typeCaption[1]} />
        <List.Item titleStyle={styles.selectorText} onPress={() => { props.setType(2); setVisible(false) }} title={props.typeCaption[2]} />
        <List.Item titleStyle={styles.selectorText} onPress={() => { props.setType(3); setVisible(false) }} title={props.typeCaption[3]} />
        <List.Item titleStyle={styles.selectorText} onPress={() => { props.setType(4); setVisible(false) }} title={props.typeCaption[4]} />
      </Menu>
    </View>
  );
}

const styles = StyleSheet.create({
  selectorText: {
    color: Colors.primaryText,
    fontSize: 14,
  },
  textButton: {
    fontSize: 14,
    color: Colors.primaryText
  },
  pickerContainer: {
    justifyContent: 'center',
    paddingLeft: 20,
    alignItems: 'center',
    height: 75,
    backgroundColor: '#fff',
    borderRadius: 8,
    elevation: 2,
    shadowColor: "#518091",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 16,
    shadowOpacity: 0.2
  },
  icon: {
    width: 20,
    height: 20,
    paddingRight: 20
  }
})


export default SolicitacaoSelector;