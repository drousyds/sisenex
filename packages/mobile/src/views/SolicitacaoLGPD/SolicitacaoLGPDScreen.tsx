import React, { useState, useEffect } from "react"
import { TextInput, KeyboardAvoidingView } from "react-native"
import Separator from "components/Separator/Separator"
import { ScreenContainer } from "components/ScreenContainer"
import { NavigationScreenProp } from "react-navigation"
import { BackButton } from "components/Header"
import useConfirmationDialog from "hooks/useConfirmationDialog"
import { ScreenTitle } from "components/ScreenTitle"
import { RoundedCard } from "components/Cards"
import { Button } from "components/Button"
import { ConfirmationDialog } from "components/Dialogs/ConfirmationDialog"
import SolicitacaoSelector from "./SolicitacaoSelector"
import { Provider as PaperProvider } from 'react-native-paper';
import PrimaryButton from "components/Button/Button"
import client from "graphql/client"
import { useCriarSolicitacaoLgpdMutation, UsuarioAtualQuery } from "generated/graphql"
import { usuarioAtual } from "graphql/queries/usuarioAtual"
import { useUsuarioAtualQuery } from "generated/graphql"


interface SolicitacaoLGPDScreenProps {
  navigation: NavigationScreenProp<{}>
}

// Seria bom adicionar um limite de caracteres na variável de text ?

const SolicitacaoLGPDScreen: React.FC<SolicitacaoLGPDScreenProps> = props => {

  const userDataQuery = useUsuarioAtualQuery()
  const [criarSolicitacaoMutation, { loading }] = useCriarSolicitacaoLgpdMutation()

  const nome = userDataQuery.data!.usuarioAtual.nomeSocialPessoa.split(' ')[0];
  const sobrenome = userDataQuery.data!.usuarioAtual.nomeSocialPessoa.split(' ')[1];
  const idPessoa = userDataQuery.data!.usuarioAtual.idPessoa;
  const idVinculoPessoa = userDataQuery.data!.usuarioAtual.idVinculoPessoa;

  const [email, setEmail] = useState("")
  const [text, setText] = useState("")
  const [message, setMessage] = useState("")
  const [controle, setControle] = useState(false)
  const [repete, setRepete] = useState(false)
  const [tipoSolicitacao, setTipoSolicitacao] = useState(0);

  useEffect(() => {
    if (controle) {
      emptyFieldProblem()
    }
  }, [message, repete])

  const onSubmission = () => {
    setControle(true)
    let aux = ''
    let c = 0

    if (tipoSolicitacao == 0) {
      fieldNoSelectedProblem()
      return 0
    }

    if (
      (text.length > 0 &&
        email.length > 0) ||
      (tipoSolicitacao < 4 &&
        email.length > 0)
    ) {
      criarSolicitacaoMutation({
        variables: {
          tipoSolicitacao: tipoSolicitacao,
          textoSolicitacao: (tipoSolicitacao == 4 ? (text) : (null)),
          idPessoa: idPessoa,
          idVinculoPessoa: idVinculoPessoa,
          nomeSolicitacao: nome,
          sobrenomeSolicitacao: sobrenome,
          emailPessoa: email
        }
      }).then(() => {
        setText("")
        setEmail("")
        setTipoSolicitacao(0)
        solicitacaoLGPD()
      })

    } else if (
      text.length == 0 &&
      email.length == 0 &&
      tipoSolicitacao == 4
    ) {
      createMessage('email', 'mensagem de solicitação')

    } else {
      if (email.length == 0) {
        aux += 'email'
        c++
      }
      if (text.length == 0 && tipoSolicitacao == 4) {
        aux += 'mensagem de solicitação'
        c++
      }

      // const array = aux.split(',')
      if (c == 1)
        createMessage(aux)

    }
  }

  function createMessage(text: string, ...rest: string[]) {
    if (rest.length == 0) {
      const aux = `Mensagem não enviada:\nCampo de ${text} vazio!`

      if (aux == message)
        setRepete(!repete)

      setMessage(aux)
    }
    else if (rest.length == 1) {
      const aux = `Mensagem não enviada:\nCampos de ${text} e ${rest[0]} vazios!`

      if (aux == message)
        setRepete(!repete)

      setMessage(aux)
    }
    else if (rest.length == 2) {
      const aux = `Mensagem não enviada:\nCampos de ${text}, ${rest[0]} e ${rest[1]} vazios!`

      if (aux == message)
        setRepete(!repete)

      setMessage(aux)
    }
    else if (rest.length == 3) {
      const aux = `Mensagem não enviada:\nCampos de ${text}, ${rest[0]}, ${rest[1]} e ${rest[2]} vazios!`

      if (aux == message)
        setRepete(!repete)

      setMessage(aux)
    }
  }

  const [emailInvalido, emailInvalidoOptions] = useConfirmationDialog({
    message: "Email inválido"
  })

  const [solicitacaoLGPD, dialogLGPDOptions] = useConfirmationDialog({
    message: "Solicitação enviada!"
  })

  const [fieldNoSelectedProblem, dialogNoSelectedoptions] = useConfirmationDialog({
    message: "Selecione um tipo de solicitação"
  })

  const [emptyFieldProblem, InvalidoOptions] = useConfirmationDialog({
    message: message
  })

  function validarEmail(email: string) {
    const usuario = email.substring(0, email.indexOf("@"))
    const dominio = email.substring(email.indexOf("@") + 1, email.length)
    if (email.length > 0) {
      if (
        usuario.length >= 1 &&
        dominio.length >= 3 &&
        usuario.search("@") == -1 &&
        dominio.search("@") == -1 &&
        usuario.search(" ") == -1 &&
        dominio.search(" ") == -1 &&
        dominio.search(".") != -1 &&
        dominio.indexOf(".") >= 1 &&
        dominio.lastIndexOf(".") < dominio.length - 2
      ) {
        return true
      } else {
        return false
      }
    } else {
      return true
    }
  }

  return (
    <PaperProvider>
      <ScreenContainer>
        <BackButton />
        <ScreenTitle
          style={{
            paddingTop: 8
          }}
          title={"Solicitação"}
          subtitle={"Solicite informações a respeito do uso dos seus dados pessoais pelo aplicativo"}
        />
        <Separator vertical size={14} />
        <SolicitacaoSelector typeCaption={["Selecione o tipo de Solicitação...",
          "Consultar existência dos meus dados",
          "Solicitar minhas informações pessoais",
          "Excluir minhas informações pessoais",
          "Outro tipo de solicitação"]} type={tipoSolicitacao} setType={setTipoSolicitacao} />
        <Separator vertical size={14} />
        <RoundedCard
          style={{
            justifyContent: "flex-start",
            alignItems: "flex-start",
            minHeight: 0
          }}
        >
          <KeyboardAvoidingView>
            <TextInput
              style={{ padding: 0, margin: 0, width: 350 }}
              value={email}
              onChangeText={newEmail => setEmail(newEmail)}
              multiline
              placeholder="Email"
            />
          </KeyboardAvoidingView>
        </RoundedCard>
        <Separator vertical size={14} />
        {(tipoSolicitacao == 4) && <RoundedCard
          style={{
            justifyContent: "flex-start",
            alignItems: "flex-start",
            maxHeight: 250
          }}
        >
          <KeyboardAvoidingView>
            <TextInput
              style={{ padding: 0, margin: 0, width: 350, height: "100%" }}
              value={text}
              onChangeText={newText => setText(newText)}
              multiline
              textAlignVertical="top"
              placeholder="Por favor explique a sua solicitação."
            />
          </KeyboardAvoidingView>
        </RoundedCard>}
        <Separator vertical size={14} />
        <PrimaryButton
          title="Enviar"
          large
          disabled={tipoSolicitacao === 0}
          loading={loading}
          style={{
            alignSelf: "center"
          }}
          onPress={() => {
            validarEmail(email)
              ? onSubmission()
              : emailInvalido()
          }
          }
        />
        <Separator vertical size={25} />
        <ConfirmationDialog {...dialogLGPDOptions} />
        <ConfirmationDialog {...emailInvalidoOptions} />
        <ConfirmationDialog {...dialogNoSelectedoptions} />
        <ConfirmationDialog {...InvalidoOptions} />
      </ScreenContainer>
    </PaperProvider>
  )
}

export default SolicitacaoLGPDScreen;