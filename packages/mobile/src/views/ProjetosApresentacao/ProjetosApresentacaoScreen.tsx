import * as React from "react"
import { ScreenContainer } from "components/ScreenContainer"
import { NavigationScreenProp } from "react-navigation"
import { useDispatch } from "react-redux"
import { navigate } from "redux/Navigation/actions"
import { Button } from "components/Button"
import { RoundedCard } from "components/Cards"
import { CardTitle, CardBody, CardFooter } from "components/Cards/RoundedCard"
import Separator from "components/Separator/Separator"
import { ScreenTitle } from "components/ScreenTitle"
import {
  useProjetosApresentacaoQuery,
  ApresentacaoAtual,
  Projeto
} from "generated/graphql"
import { LoadingState } from "components/StateIndicator/LoadingState"
import { getApresentacaoAtual } from "graphql/cacheHelpers"

interface ProjetosApresentacaoScreenProps {
  navigation: NavigationScreenProp<{}>
}

const ProjetosApresentacaoScreen: React.SFC<
  ProjetosApresentacaoScreenProps
> = props => {
  const dispatch = useDispatch()
  const apresentacao: ApresentacaoAtual = getApresentacaoAtual()
  const navigateToGerenciaProjeto = (
    idProjeto: number,
    tituloProjeto: string
  ) =>
    dispatch(
      navigate({
        routeName: "GerenciaProjeto",
        params: {
          idProjeto,
          tituloProjeto
        }
      })
    )

  // const { loading, data } = mockProjetosApresentacoesQuery()
  const { loading, data, error } = useProjetosApresentacaoQuery({
    variables: {
      idApresentacao: apresentacao.idApresentacao,
      id2Apresentacao: apresentacao.id2Apresentacao
    },
    pollInterval: 30000
  })

  if (loading)
    return (
      <ScreenContainer>
        <ScreenTitle
          title="Projetos"
          subtitle="Habilite os projetos de acordo com a ordem de apresentação."
        />
        <LoadingState />
      </ScreenContainer>
    )

  return (
    <ScreenContainer>
      <ScreenTitle
        title="Projetos"
        subtitle="Habilite os projetos de acordo com a ordem de apresentação."
      />
      {data!.apresentacao!.projetos!.map(projeto => (
        <CardProjetoApresentacao
          disponibilidadeProjeto={projeto.disponibilidadeProjeto}
          coordenador={
            projeto.coordenador &&
            projeto.coordenador.length > 0 &&
            projeto.coordenador[0].nomePessoa
              ? projeto.coordenador[0].nomePessoa
              : "Indisponivel"
          }
          key={projeto.idProjeto}
          idProjeto={projeto.idProjeto}
          tituloProjeto={projeto.tituloProjeto}
          onButtonClick={navigateToGerenciaProjeto}
        />
      ))}
    </ScreenContainer>
  )
}

export interface CardProjetoApresentacaoProps {
  idProjeto: Projeto["idProjeto"]
  disponibilidadeProjeto: Projeto["disponibilidadeProjeto"]
  tituloProjeto: string
  coordenador?: string
  onButtonClick: (idProjeto: number, tituloProjeto: string) => void
}

export const CardProjetoApresentacao: React.SFC<
  CardProjetoApresentacaoProps
> = props => {
  const onClick = () => {
    props.onButtonClick(props.idProjeto, props.tituloProjeto)
  }

  return (
    <React.Fragment key={props.idProjeto}>
      <RoundedCard>
        <CardTitle fullText>{props.tituloProjeto}</CardTitle>
        <CardBody>{`Coordenador: ${props.coordenador}${"\n"}Status: ${
          props.disponibilidadeProjeto ? "Habilitado" : "Desabilitado"
        }`}</CardBody>
        <CardFooter>
          <Button large title="Gerenciar" onPress={onClick} />
        </CardFooter>
      </RoundedCard>
      <Separator vertical size={14} />
    </React.Fragment>
  )
}

export default ProjetosApresentacaoScreen
