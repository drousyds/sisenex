import React from "react"
import {
  Apresentacao,
  useLiberarProjetoMutation,
  useFecharProjetoMutation
} from "generated/graphql"
import { gerenciaProjeto } from "graphql/queries/gerenciaProjeto"
import { View } from "react-native"
import { BackButton } from "components/Header"
import { CardTitle } from "components/Cards/RoundedCard"
import { Button } from "components/Button"
import { LoadingState } from "components/StateIndicator/LoadingState"

export const SessaoTituloProjeto: React.SFC<{
  disponivel?: boolean
  loading?: boolean
  idProjeto: number
  tituloProjeto: string
  idApresentacao: Apresentacao["idApresentacao"]
  id2Apresentacao: Apresentacao["id2Apresentacao"]
  disabled?: boolean
}> = props => {
  const liberarMutation = useLiberarProjetoMutation({
    variables: {
      idProjeto: props.idProjeto
    }
  })
  const fecharMutation = useFecharProjetoMutation({
    variables: {
      idProjeto: props.idProjeto
    }
  })

  const onPress = props.disponivel ? fecharMutation[0] : liberarMutation[0]

  return (
    <View
      style={{
        paddingHorizontal: 14,
        paddingBottom: 22,
        alignItems: "center",
        backgroundColor: "white"
      }}
    >
      <BackButton />
      <CardTitle fullText style={{ paddingTop: 6, paddingBottom: 10 }}>
        {props.tituloProjeto}
      </CardTitle>
      <Button
        title={props.disponivel ? "Encerrar Avaliação" : "Iniciar Avaliação"}
        secondary={props.disponivel}
        large
        disabled={props.disabled}
        loading={
          liberarMutation[1].loading ||
          fecharMutation[1].loading ||
          props.disabled
        }
        onPress={onPress}
      />
    </View>
  )
}
