import React from "react"
import { Pessoa, Projeto, Apresentacao, GerenciaProjetoQueryResult } from "generated/graphql"
import { ScrollView } from "react-native-gesture-handler"
import Separator from "components/Separator/Separator"
import CardAvaliador from "./CardAvaliador"
import { ScreenContainer } from "components/ScreenContainer"

export interface SessaoAvaliadoresProps {
  idProjeto: Projeto["idProjeto"]
  idApresentacao: Apresentacao["idApresentacao"]
  id2Apresentacao: Apresentacao["id2Apresentacao"]
  avaliadoresNaApresentacao: {
    idPessoa: Pessoa["idPessoa"]
    idVinculoPessoa: Pessoa["idVinculoPessoa"]
    nomePessoa?: Pessoa["nomePessoa"]
    lotacaoPessoa?: Pessoa["lotacaoPessoa"]
    matriculaPessoa?: Pessoa["matriculaPessoa"]
    avaliacoes?: {
      media: number
    }[]
  }[]

  avaliadoresHabilitados: {
    idPessoa: Pessoa["idPessoa"]
    idVinculoPessoa: Pessoa["idVinculoPessoa"]
  }[]
}

export const SessaoAvaliadores: React.SFC<SessaoAvaliadoresProps> = props => {
  const avaliadoresComStatus = props.avaliadoresNaApresentacao.map(
    avaliador => ({
      ...avaliador,
      idApresentacao: props.idApresentacao,
      id2Apresentacao: props.id2Apresentacao,
      idProjeto: props.idProjeto,
      habilitado:
        props.avaliadoresHabilitados.find(
          elem =>
            elem.idPessoa == avaliador.idPessoa &&
            elem.idVinculoPessoa == avaliador.idVinculoPessoa
        ) !== undefined
    })
  )

  return (
    <ScreenContainer contentContainerStyle={{paddingHorizontal: 0}}>
      <Separator vertical size={22} />
      {avaliadoresComStatus.map(avaliador => (
        <CardAvaliador
          key={avaliador.idPessoa}
          idProjeto={props.idProjeto}
          idPessoa={avaliador.idPessoa}
          idVinculoPessoa={avaliador.idVinculoPessoa}
          idApresentacao={props.idApresentacao}
          id2Apresentacao={props.id2Apresentacao}
          nomePessoa={avaliador.nomePessoa}
          matriculaPessoa={avaliador.matriculaPessoa}
          lotacaoPessoa={avaliador.lotacaoPessoa}
          habilitado={avaliador.habilitado}
          avaliou={avaliador.avaliacoes ? avaliador.avaliacoes.length > 0 : false}
        />
      ))}
    </ScreenContainer>
  )
}
