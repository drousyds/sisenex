import * as React from "react"
import { StyleSheet, View, Text } from "react-native"
import RoundedCard, {
  CardTitle,
  CardSubtitle,
  CardFooter,
  CardTitleContainer
} from "components/Cards/RoundedCard"
import {
  Pessoa,
  Projeto,
  Apresentacao,
  useLiberarProjetoParaUmAvaliadorMutation,
  useFecharProjetoParaUmAvaliadorMutation
} from "generated/graphql"
import { useDispatch } from "react-redux"
import { navigate } from "redux/Navigation/actions"
import Separator from "components/Separator/Separator"
import Typography from "assets/css/typography"
import { Button } from "components/Button"
import { gerenciaProjeto } from "graphql/queries/gerenciaProjeto"
import gql from "graphql-tag"
import { cardAvaliadorFragment } from "graphql/fragments/cardAvaliadorFragment"
import { updateStatusAvaliador } from "graphql/cacheHelpers"
import { MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons"
import Colors from "assets/css/colors"

interface CardAvaliadorProps {
  nomePessoa: Pessoa["nomePessoa"]
  idPessoa: Pessoa["idPessoa"]
  idVinculoPessoa: Pessoa["idVinculoPessoa"]
  idProjeto: Projeto["idProjeto"]
  idApresentacao: Apresentacao["idApresentacao"]
  id2Apresentacao: Apresentacao["id2Apresentacao"]
  lotacaoPessoa: Pessoa["lotacaoPessoa"]
  matriculaPessoa: Pessoa["matriculaPessoa"]
  habilitado?: boolean
  avaliou?: boolean
}

export const CardAvaliador: React.SFC<CardAvaliadorProps> = ({
  nomePessoa,
  lotacaoPessoa,
  matriculaPessoa,
  idPessoa,
  idVinculoPessoa,
  idProjeto,
  idApresentacao,
  id2Apresentacao,
  habilitado,
  avaliou
}) => {
  const variables = {
    idProjeto,
    idPessoa,
    idVinculoPessoa
  }

  const liberarMutation = useLiberarProjetoParaUmAvaliadorMutation({
    variables,
    update: cache =>
      updateStatusAvaliador(
        cache,
        { idProjeto, idPessoa, idVinculoPessoa },
        true
      )
  })

  const fecharMutation = useFecharProjetoParaUmAvaliadorMutation({
    variables,
    update: cache =>
      updateStatusAvaliador(
        cache,
        { idProjeto, idPessoa, idVinculoPessoa },
        false
      )
  })

  const onPress = habilitado ? fecharMutation[0] : liberarMutation[0]

  return (
    <React.Fragment>
      <RoundedCard style={{ minHeight: 0 }}>
        <CardTitleContainer
          imageSource={{
            uri:
              "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDxUSDw8VFRUVFRUVFRUVFRUVFRUVFRUWFxUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDw0NDysZHxkrKysrKystLSsrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEBAAMBAQEAAAAAAAAAAAAAAQIEBQMGB//EADQQAQEAAQICCAMIAAcAAAAAAAABAgMRBCEFEjFBUWFxgZGx0SIyM0KhweHwExUjcoKS8f/EABUBAQEAAAAAAAAAAAAAAAAAAAAB/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A/XAFQAAAAAAAAAAAAAAAAAAAAQQBFQBBAUTcB6gAAAAAAAAAAAAAAAAAAAAlAQQAEASlQAQB7gAAAAAAAA8+I18dPHfL2nffQHpbt2tHX6Twx5YzrX4T4udxXF5al58p3Tu9/Frg3NTpLVvZZPSfV4XidS/ny+NeQD1nEak/Pl/2r20+kdWfm39Y1AHX0OlMbyzm3nOcb+OUs3l3njHzL14fictO7431ndQfRDw4XisdSbzt754fw9gEogAIAlEoCblQFRFBsAAAAAAAAx1dSY43K9kcDide6mW99p4RtdLcRvl1J2Tt9XPAAUAAAAAQGelq3DKZY9s/uzvcNrzUx60954XwfPNro3iOpntezLlf2qDuJRAAS0EQtKCAxAVN/MBtAAAAAAMdXPq423ulvwZNXpTLbSvntP1Bw8srbve280BQAAAABAAQAAH0HCavX08b5c/Wcq9Wh0Pl9izwvzjeQLWKpQEE3ASlrHcFVj7ANwAAAAABpdMfhz/dPlW61OlMd9K+Vl/X+QcMBQAAAAQAQABAB0+huzP/AI/u6NaHQ8+xb435T+W8gWoJQKhugFQrGgow3AdAAAAAABjq4dbG4+MsZAPmcptdr3I3+ltDq5dadmXz/v7tAABQQAEABFQBBs8BodfPn2Tnf2gOrwen1dOTy3vreb2pUqCVFrEBKJQLWFXdLQBFB0AAAAAAAAYa2lM8bjey/wB3cDiNG4ZdXL/2eL6J5cTw+Opjtl7XvgPnUe/E8Nlp37U5d17q8FBAABAEVno6OWd2xm/ynqDHTwuV2k513OF0Jp47Tt7741OE4Wac8be2/Tye1QEogCbjGgVjatrHcBLRLQN7/dhNwHTAAAAAAAABp6/SOnj2XrXy+oNrKSzazeeFc/X6LxvPC7eV5x4Z9K578sZJ571saXSmF+9Lj+sBo6nAauP5d/Tn/Lwy0sp2434V9BhrY5fdyl9LGYPm5pZd2N+FeunwWrl+Sz15fN3q89TVxx7cpPW7A0NHoufny38p2fFvYYTGbYzaNXW6S052b5Xy5T41qf5pnv8Adm3hz+YOsjT0eksMu37N8+c+Lbll5ygVKWpQGNq2sQRFrECpS1jaBuHuA6oAAAAADw4vi8dOc+d7p3/w8+P4yac2nPK9nl51xM8rbvbvb3g9uJ4vPU7by8J2fy1xFAAEZTOzst+NYgLlqZXtyvxrBUAQQCvTQ4jLTv2b7d19nkgO1wvG46nLsy8Po2bXze7q8DxvW+zlftd18f5QbtQqAlSlS0E3SiABsgOwAAAA8uK15p43K+08a9XE6V1+tntOzHl79/0BqamdyttvOsQUQABAAQQBAoIgAgVAQl74IDtcHxH+Jj5zt+r33cTg9bqZy915X0dm1ArHdWIG6bralA3v9omyg7AAAAPPidXqYXLwn69z5u12emc9sJPG/L+xxgEBQQAEKgKggDGrUBA3QCoJQEEAdngtXrac8uV9nFrf6Kz+9PS/X9kHQ3CoCBuAbVUAdkAAEByumrzxnlf12+jmOj0197H0/dzgEVFBFQBBAEpUAQKCIAJQqAJSsQG10Zf9T2v1aja6O/E9qg61SrUARUA9/wBRPcB2wQCoAOT0z97H0/dznQ6Z+9j6fu5wAIoIFBEViACAWpRAEEARaxBKm62oBWz0b+J7Vqtno38T2oOuiogVFQF2F6oDsoAIgA5PTX3sfS/NzgAQFGNKAJSoAlRQGNIAMQARjQBKgAlbfRv4k9KgDrxP5BBFv9/QAAAf/9k="
          }}
        >
          <View>
            <View>
              <CardTitle textStyle={{ textAlign: "left", marginRight: 50}}>
                {nomePessoa}
              </CardTitle>
              <CardSubtitle style={{ marginRight: 22 }}>
                {`${lotacaoPessoa ||
                  "Lotação: Indisponível"}${"\n"}${matriculaPessoa}`}
              </CardSubtitle>
              <View style={{flexDirection: "row"}}>
                <Text style={{
                    ...Typography.subtitle,
                    textAlignVertical:"top",
                    fontSize: 15,
                    lineHeight: 18,
                    color: "#A0A0A0",
                    fontWeight: "100",
                }}>Avaliou:</Text>
                <MaterialIcons name={avaliou? "check" : "clear"} 
                               color={avaliou? Colors.success : Colors.error} 
                               size={22} 
                               style={{bottom:3}}/>
              </View>
            </View>
          </View>
        </CardTitleContainer>
        <CardFooter style={{ justifyContent: "flex-end" }}>
          <Button
            title={habilitado ? "Desabilitar Avaliador" : "Habilitar Avaliador"}
            secondary={habilitado}
            loading={liberarMutation[1].loading || fecharMutation[1].loading}
            large
            onPress={onPress}
          />
        </CardFooter>
      </RoundedCard>
      <Separator vertical size={14} />
    </React.Fragment>
  )
}

export default CardAvaliador
