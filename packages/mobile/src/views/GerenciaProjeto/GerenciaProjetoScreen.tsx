import React from "react"
import { View, StyleSheet } from "react-native"
import { ScreenContainer } from "components/ScreenContainer"
import { NavigationScreenProp } from "react-navigation"
import * as SplashScreen from 'expo-splash-screen';
import { useGerenciaProjetoQuery, ApresentacaoAtual } from "generated/graphql"
import { useApolloClient } from "react-apollo"
import {
  apresentacaoAtual,
  mockApresentacaoAtual
} from "graphql/queries/apresentacaoAtual"
import { SessaoTituloProjeto } from "./SessaoTituloProjeto"
import { SessaoAvaliadores } from "./SessaoAvaliadores"
import {
  LoadingState,
  SmallLoadingState
} from "components/StateIndicator/LoadingState"
import Separator from "components/Separator/Separator"
import { ScrollView } from "react-native-gesture-handler"
import { getApresentacaoAtual } from "graphql/cacheHelpers"
import { mockGerenciaProjeto } from "graphql/queries/gerenciaProjeto"
import { useRoute } from "@react-navigation/native"


interface GerenciaProjetoScreenProps {
  navigation: NavigationScreenProp<{
    idProjeto: number
    tituloProjeto: string
  }>
}

const GerenciaProjetoScreen: React.SFC<GerenciaProjetoScreenProps> = props => {
  React.useEffect(() => {
    SplashScreen.hideAsync()
  }, [])
  const route = useRoute()
  const idProjeto = route.params?.idProjeto
  const tituloProjeto = route.params?.tituloProjeto
  const apresentacao: ApresentacaoAtual = getApresentacaoAtual()

  const { data, loading, error, variables } = useGerenciaProjetoQuery({
    variables: {
      idProjeto: idProjeto!,
      idApresentacao: apresentacao.idApresentacao,
      id2Apresentacao: apresentacao.id2Apresentacao
    },
    pollInterval: 5000
  })

  console.log(variables)

  // const apresentacao: ApresentacaoAtual ={
  //   __typename: "ApresentacaoAtual",
  //   idApresentacao: 2,
  //   id2Apresentacao: "2019",
  //   nomeAreaTematica: "",
  //   horaApresentacao: "",
  //   salaApresentacao: "",
  //   codigoApresentacao: "40021964",
  // }

  // const { data, loading } = mockGerenciaProjeto()

  const projetoData = data && data.projeto
  const apresentacaoData = data && data.apresentacao

  console.log("As avaliacoes", apresentacaoData)
  console.log("O erro", error)

  if (loading || !(projetoData && apresentacaoData)) {
    return (
      <View style={{ flex: 1 }}>
        <SessaoTituloProjeto
          tituloProjeto={tituloProjeto}
          idProjeto={idProjeto}
          idApresentacao={apresentacao.idApresentacao}
          id2Apresentacao={apresentacao.id2Apresentacao}
          loading
          disabled
        />
        <ScreenContainer
          contentContainerStyle={{ paddingHorizontal: 0, marginHorizontal: 0 }}
        >
          <Overlay />
          <ScrollView
            contentContainerStyle={{ flex: 1 }}
            keyboardShouldPersistTaps="handled"
          >
            <Separator vertical size={22} />
            <SmallLoadingState />
          </ScrollView>
        </ScreenContainer>
      </View>
    )
  }

  return (
    <View style={{ flex: 1 }}>
      <SessaoTituloProjeto
        idProjeto={idProjeto}
        disponivel={projetoData.disponibilidadeProjeto == 1}
        tituloProjeto={tituloProjeto}
        idApresentacao={apresentacao.idApresentacao}
        id2Apresentacao={apresentacao.id2Apresentacao}
      />
      <ScreenContainer>
        {projetoData.disponibilidadeProjeto == 0 && <Overlay />}
        <SessaoAvaliadores
          idApresentacao={apresentacao.idApresentacao}
          id2Apresentacao={apresentacao.id2Apresentacao}
          idProjeto={idProjeto}
          avaliadoresHabilitados={projetoData.avaliadoresHabilitados || []}
          avaliadoresNaApresentacao={apresentacaoData.avaliadores || []}
        />
      </ScreenContainer>
    </View>
  )
}

const Overlay: React.SFC = props => (
  <View
    style={{
      ...StyleSheet.absoluteFillObject,
      backgroundColor: "grey",
      zIndex: 3,
      opacity: 0.5
    }}
  />
)

export default GerenciaProjetoScreen
