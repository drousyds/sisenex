import * as React from "react"
import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native"
import { RoundedCard } from "components/Cards"
import Typography from "assets/css/typography"
import { useDispatch } from "react-redux"
import { navigate } from "redux/Navigation/actions"
import { MaterialIcons } from "@expo/vector-icons"

interface CardProblemaProps {}

const CardProblema: React.SFC<CardProblemaProps> = props => {
  const dispatch = useDispatch()
  const navigateToReport = () =>
    dispatch(
      navigate({
        routeName: "ReportProblemas"
      })
    )

  return (
    <RoundedCard style={{ minHeight: 0 }}>
      <TouchableOpacity onPress={navigateToReport}>
        <View style={styles.container}>
          <View>
            <Image
              style={styles.image}
              resizeMode="contain"
              source={require("assets/problem.png")}
            />
          </View>
          <View style={styles.textContainer}>
            <Text style={styles.cardTitle}>Comunicar um problema</Text>
          </View>
          <MaterialIcons name="chevron-right" size={30} color="grey" />
        </View>
      </TouchableOpacity>
    </RoundedCard>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    paddingHorizontal: 8
  },
  image: {
    width: 48,
    height: 48,
    borderRadius: 4,
    marginRight: 16
  },
  textContainer: {
    paddingLeft: 12,
    marginRight: 13
  },
  cardTitle: {
    ...Typography.header3,
    fontSize: 16,
    lineHeight: 16
  }
})

export default CardProblema
