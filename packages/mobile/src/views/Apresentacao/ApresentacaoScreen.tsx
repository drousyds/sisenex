import * as React from "react"
import { StyleSheet, Text} from "react-native"
import { ScreenContainer } from "components/ScreenContainer"
import { NavigationScreenProp } from "react-navigation"
import { ScreenTitle } from "components/ScreenTitle"
import InfoApresentacao from "./InfoApresentacao"
import Separator from "components/Separator/Separator"
import CardIdApresentacao from "./IdApresentacao"
import CardProblema from "./CardProblema"
import { Button } from "components/Button"
import { ApresentacaoAtual } from "generated/graphql"
import { useDispatch } from "react-redux"
import { getApresentacaoAtual } from "graphql/cacheHelpers"
import { logout } from "redux/Auth/actions"
import Colors from "assets/css/colors"
import { TouchableOpacity } from "react-native-gesture-handler"
import { navigate } from "redux/Navigation/actions"

interface ApresentacaoScreenProps {
  navigation: NavigationScreenProp<{}>
}

const ApresentacaoScreen: React.SFC<ApresentacaoScreenProps> = props => {
  const dispatch = useDispatch()

  const apresentacao: ApresentacaoAtual = getApresentacaoAtual()

  const onLogout = () => dispatch(logout())

  const navigateToAvisosLegais = () =>
    dispatch(
      navigate({
        routeName: "AvisosLegais",
      })
    )

  return (
    <ScreenContainer>
      <ScreenTitle
        title="Apresentação"
        subtitle="Visualize as informações e encerre a apresentação ao seu término"
      />
      <InfoApresentacao
        idApresentacao={apresentacao.idApresentacao}
        nomeAreaTematica={apresentacao.nomeAreaTematica}
        horaApresentacao={apresentacao.horaApresentacao}
        salaApresentacao={apresentacao.salaApresentacao}
      />
      <Separator vertical />
      <CardIdApresentacao
        codigoApresentacao={apresentacao.codigoApresentacao}
      />
      <Separator vertical />
      <CardProblema />
      <Separator vertical size={23} />
      <TouchableOpacity onPress={navigateToAvisosLegais}>
        <Text style={styles.link}>Lei Geral de Proteção de Dados</Text>
      </TouchableOpacity>
      <Button
        large
        title="Sair da aplicação"
        style={{ alignSelf: "center", marginVertical: 20 }}
        onPress={onLogout}
      />
      <Separator vertical />
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  link: {
    fontSize: 15,
    fontWeight: "bold",
    letterSpacing: 0.4,
    color: Colors.blueText,
    textDecorationLine: 'underline',
    alignSelf: 'center'
  }
})

export default ApresentacaoScreen
