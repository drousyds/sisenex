import * as React from "react"
import { Pessoa, Apresentacao, AreaTematica } from "generated/graphql"
import { View, Image, Text, StyleSheet } from "react-native"
import { RoundedCard } from "components/Cards"
import { human } from "react-native-typography"
import Colors from "assets/css/colors"
import Typography from "assets/css/typography"

export interface InfoPessoaisProps {
  idApresentacao: Apresentacao["idApresentacao"]
  nomeAreaTematica: AreaTematica["nomeAreaTematica"]
  horaApresentacao: Apresentacao["horaApresentacao"]
  salaApresentacao: Apresentacao["salaApresentacao"]
}

const InfoApresentacao: React.SFC<InfoPessoaisProps> = props => {
  return (
    <RoundedCard style={{ minHeight: 0 }}>
      <View style={styles.container}>
        <View>
          <Image
            style={styles.image}
            resizeMode="contain"
            source={require("assets/apresenta.png")}
          />
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.cardTitleText}>{`Apresentação ${
            props.idApresentacao
          }`}</Text>
          <Text style={styles.cardBodyText}>{`Área Temática: ${
            props.nomeAreaTematica
          }`}</Text>
          <Text style={styles.cardBodyText}>{`Horário: ${
            props.horaApresentacao
          }`}</Text>
          <Text style={styles.cardBodyText}>{`Sala: ${
            props.salaApresentacao
          }`}</Text>
        </View>
      </View>
    </RoundedCard>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 8
  },
  image: {
    width: 48,
    height: 48,
    borderRadius: 4
  },
  textContainer: { paddingLeft: 12, marginRight: 13 },
  cardTitleText: {
    ...Typography.header3,
    fontSize: 16,
    lineHeight: 16
  },
  cardBodyText: {
    ...Typography.subtitle,
    fontSize: 12,
    lineHeight: 16,
    color: "#A0A0A0",
    fontWeight: "100"
  }
})

export default InfoApresentacao
