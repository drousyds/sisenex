import * as React from "react"
import { Text, View } from "react-native"
import QRCode from "react-native-qrcode-svg"
import { RoundedCard } from "components/Cards"
import { Button } from "components/Button"
import { CardFooter } from "components/Cards/RoundedCard"
import { useApolloClient } from "react-apollo"
import {
  ApresentacaoAtual,
  useFecharApresentacaoMutation
} from "generated/graphql"
import { apresentacaoAtual } from "graphql/queries/apresentacaoAtual"
import { useDispatch } from "react-redux"
import { navigate } from "redux/Navigation/actions"
import { getApresentacaoAtual } from "graphql/cacheHelpers"
import useConfirmationDialog from "hooks/useConfirmationDialog"
import { ConfirmationDialog } from "components/Dialogs/ConfirmationDialog"

interface CardIdApresentacaoProps {
  codigoApresentacao: string
}

const CardIdApresentacao: React.SFC<CardIdApresentacaoProps> = props => {
  const dispatch = useDispatch()
  const apresentacao: ApresentacaoAtual = getApresentacaoAtual()
  const [fecharApresentacao, { data, loading }] = useFecharApresentacaoMutation(
    {
      variables: {
        codigoApresentacao: apresentacao.codigoApresentacao
      }
    }
  )
  const onFecharApresentacao = () => {
    fecharApresentacao().then(() => {
      dispatch(
        navigate({
          routeName: "RegistroApresentacao"
        })
      )
    })
  }
  const [openFecharApresentacaoDialog, dialogProps] = useConfirmationDialog({
    message: "Tem certeza que deseja encerrar a apresentação atual?",
    onConfirm: onFecharApresentacao
  })

  return (
    <RoundedCard>
      <View style={{ padding: 20 }}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            paddingBottom: 16,
            marginRight: 20
            // flexWrap: "wrap"
          }}
        >
          <QRCode value={apresentacao.codigoApresentacao} size={140} />
          <Text
            style={{
              fontSize: 18,
              color: "#7e7e7e",
              paddingLeft: 20
            }}
          >
            {`ID: ${props.codigoApresentacao}`}
          </Text>
        </View>
        <CardFooter>
          <Button
            secondary
            large
            loading={loading}
            onPress={openFecharApresentacaoDialog}
            title="Encerrar apresentação"
          />
        </CardFooter>
      </View>
      <ConfirmationDialog {...dialogProps} />
    </RoundedCard>
  )
}

export default CardIdApresentacao
