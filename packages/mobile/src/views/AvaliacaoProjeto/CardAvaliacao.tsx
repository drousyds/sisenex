import * as React from "react"
import { RoundedCard } from "components/Cards"
import { material } from "react-native-typography"
import {
  Text,
  View,
  StyleSheet,
  KeyboardAvoidingView,
  TextInput
} from "react-native"
import PrimaryButton from "components/Button/Button"
import Separator from "components/Separator/Separator"
import { Pergunta, Projeto } from "generated/graphql"
import {
  CardTitle,
  CardFooter,
  CardFooterContent
} from "components/Cards/RoundedCard"
import getScoreColor from "helpers/getScoreColor"
import { updateRespostaTextual } from "graphql/cacheHelpers"
import { useApolloClient } from "react-apollo"

interface CardAvaliacaoProps {
  conteudoPergunta: string
  nomeCategoria: string
  nota: number | null
  idPergunta: Pergunta["idPergunta"]
  idProjeto: Projeto["idProjeto"]
  podeAvaliar?: boolean
  openModal?: (options: {
    idPergunta: Pergunta["idPergunta"]
    conteudoPergunta: string
    nomeCategoria: string
  }) => () => void
  respostaTextual: string
  usarCampoDeTexto?: boolean
}

const CardAvaliacao: React.FC<CardAvaliacaoProps> = ({
  conteudoPergunta,
  nomeCategoria,
  idPergunta,
  nota,
  podeAvaliar,
  openModal,
  idProjeto,
  respostaTextual,
  usarCampoDeTexto
}) => {
  const client = useApolloClient()
  const [comentario, setComentario] = React.useState("")

  const updateComentario = (comentario: string) => {
    updateRespostaTextual(client, {
      idProjeto: idProjeto,
      idPergunta: idPergunta,
      respostaTextual: comentario
    })
  }

  const saveComentario = (comentario: string) => {
    updateComentario(comentario)
    setComentario(comentario)
  }

  React.useEffect(() => {
    if (usarCampoDeTexto) {
      if (respostaTextual == null) {
        updateComentario("")
        // console.log("salvou auto")
      }
    }
  }, [])

  if (podeAvaliar)
    return (
      <React.Fragment key={idPergunta.toString()}>
        {usarCampoDeTexto ? (
          <RoundedCard style={{ justifyContent: "flex-start", flex: 1 }}>
            <CardTitle>{conteudoPergunta}</CardTitle>
            <Separator vertical />
            {respostaTextual != null ? (
              <RoundedCard
                style={{
                  justifyContent: "flex-start",
                  alignItems: "flex-start",
                  borderRadius: 12,
                  borderWidth: 1,
                  borderColor: "gray",
                  maxHeight: 200
                }}
              >
                <KeyboardAvoidingView>
                  <TextInput
                    style={{
                      padding: 0,
                      margin: 0,
                      height: "100%",
                      width: 350
                    }}
                    defaultValue={respostaTextual}
                    onChangeText={newComment => saveComentario(newComment)}
                    multiline
                    textAlignVertical="top"
                    placeholder="Digite seu comentário aqui."
                  />
                </KeyboardAvoidingView>
              </RoundedCard>
            ) : (
              <RoundedCard
                style={{
                  justifyContent: "flex-start",
                  alignItems: "flex-start",
                  borderRadius: 12,
                  borderWidth: 1,
                  borderColor: "gray",
                  maxHeight: 200
                }}
              >
                <KeyboardAvoidingView>
                  <TextInput
                    style={{
                      padding: 0,
                      margin: 0,
                      height: "100%",
                      width: 350
                    }}
                    value={comentario}
                    onChangeText={newComment => saveComentario(newComment)}
                    multiline
                    textAlignVertical="top"
                    placeholder="Digite seu comentário aqui."
                  />
                </KeyboardAvoidingView>
              </RoundedCard>
            )}
            {/* <View style={{ alignSelf: "center" }}>
              <Separator vertical />
              <PrimaryButton
                title={"Salvar comentário"}
                onPress={() => saveComentario(comentario)}
              />
            </View>
            <ConfirmationDialog {...dialogProps} /> */}
          </RoundedCard>
        ) : (
          <RoundedCard>
            <CardTitle>{conteudoPergunta}</CardTitle>
            <Separator vertical size={24} />
            <CardFooter style={{ justifyContent: "space-around" }}>
              {nota != undefined ? (
                <React.Fragment>
                  <CardFooterContent>
                    <Text style={[material.headline, { fontSize: 18 }]}>
                      Nota{" "}
                    </Text>
                    <Text
                      style={[
                        styles.mediaFont,
                        {
                          paddingLeft: 16,
                          color: getScoreColor(nota)
                        }
                      ]}
                    >
                      {parseFloat(nota.toFixed(2))}
                    </Text>
                  </CardFooterContent>
                  <PrimaryButton
                    title="Editar"
                    onPress={
                      (openModal &&
                        openModal({
                          idPergunta,
                          conteudoPergunta,
                          nomeCategoria
                        })) ||
                      (() => console.log("editando nota"))
                    }
                  />
                </React.Fragment>
              ) : (
                <PrimaryButton
                  style={{
                    alignSelf: "center"
                  }}
                  large
                  title="Inserir nota"
                  onPress={
                    (openModal &&
                      openModal({
                        idPergunta,
                        nomeCategoria,
                        conteudoPergunta
                      })) ||
                    (() => console.log("inserindo nota"))
                  }
                />
              )}
            </CardFooter>
          </RoundedCard>
        )}
        <Separator vertical />
      </React.Fragment>
    )
  else
    return (
      <React.Fragment key={idPergunta.toString()}>
        {usarCampoDeTexto ? (
          <RoundedCard style={{ justifyContent: "flex-start", flex: 1 }}>
            <CardTitle>{conteudoPergunta}</CardTitle>
            <Separator vertical />
            <RoundedCard
              style={{
                justifyContent: "flex-start",
                alignItems: "flex-start",
                borderRadius: 12,
                borderWidth: 1,
                borderColor: "gray"
              }}
            >
              <Text
                style={[
                  material.headlineObject,
                  { fontSize: 18, textAlign: "justify", lineHeight: 20 }
                ]}
              >
                {respostaTextual}
              </Text>
            </RoundedCard>
          </RoundedCard>
        ) : (
          <RoundedCard>
            <CardTitle>{conteudoPergunta}</CardTitle>
            <CardFooter>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: 30
                }}
              >
                <Text style={[material.headline, { fontSize: 18 }]}>Nota </Text>
                <Separator size={12} />
                <Text
                  style={[
                    styles.mediaFont,
                    {
                      color: getScoreColor(nota)
                    }
                  ]}
                >
                  {nota}
                </Text>
              </View>
            </CardFooter>
          </RoundedCard>
        )}
        <Separator vertical />
      </React.Fragment>
    )
}

const styles = StyleSheet.create({
  mediaFont: {
    ...material.headlineObject,
    fontSize: 32
  }
})

export default CardAvaliacao
