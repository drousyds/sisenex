import React, { useState, useRef } from "react"
import { material } from "react-native-typography"
import { View, Text, StyleSheet } from "react-native"
import { CardTitle, CardFooter } from "components/Cards/RoundedCard"
import { Button } from "components/Button"
import Separator from "components/Separator/Separator"
import { useApolloClient } from "react-apollo"
import { Pergunta, Projeto } from "generated/graphql"
import { TextInputMask } from "react-native-masked-text"
import Typography from "assets/css/typography"
import { updateNotaCache } from "graphql/cacheHelpers"
import { Dialog } from "components/Dialogs"
import Colors from "assets/css/colors"

export interface ModalNotaProps {
  idPergunta: Pergunta["idPergunta"]
  nomeCategoria: string
  idProjeto: Projeto["idProjeto"]
  conteudoPergunta: string
  dismiss: () => void
  visible?: boolean
}

const ModalNota: React.SFC<ModalNotaProps> = props => {
  const [text, setText] = useState("")
  const [error, setError] = useState<boolean>(false)
  const client = useApolloClient()
  const inputRef = useRef(null)

  const closeModal = () => {
    setError(false)
    setText("")
    props.dismiss()
  }

  const onShow = () => {
    //@ts-ignore
    inputRef.current._inputElement.focus()
  }

  const updateNota = (nota: number) => {
    if (nota != null && nota >= 1) {
      updateNotaCache(client, {
        idProjeto: props.idProjeto,
        idPergunta: props.idPergunta,
        nota: parseFloat(nota.toPrecision(2))
      })
      setError(false)
      closeModal()
    } else
      setError(true)
  }

  const onConfirm = () => updateNota(parseFloat(text.replace(",", ".")))

  return (
    <Dialog visible={props.visible} onShow={onShow}>
      <View
        style={{
          flexGrow: 0,
          alignItems: "center",
          paddingVertical: 36,
          flex: 0
        }}
      >
        <Text style={Typography.header2}>{props.conteudoPergunta}</Text>
      </View>
      <View
        style={{
          marginHorizontal: 8
        }}
      >
        {!error && <Text style={[Typography.caption, { paddingBottom: 8 }]}>Nota</Text>}
        {error && <Text style={[Typography.caption, { paddingBottom: 8, color: Colors.error }]}>
          Insira uma nota entre 1,0 e 10,0</Text>}
        <TextInputMask
          ref={inputRef}
          type={"money"}
          keyboardType="decimal-pad"
          onSubmitEditing={onConfirm}
          options={{
            precision: 2,
            separator: ",",
            delimiter: ".",
            unit: "",
            suffixUnit: ""
          }}
          value={text}
          onChangeText={(newText: string) => {
            if (parseFloat(newText.replace(",", ".")) <= 10) setText(newText)
          }}
          style={!error ? (styles.campoNota) : [{ ...styles.errorNota }, styles.campoNota]}
        />
      </View>
      <CardFooter style={{ justifyContent: "center" }}>
        <View
          style={{
            display: "flex",
            flex: 1,
            flexDirection: "row",
            justifyContent: "center",
            paddingBottom: 8,
            paddingTop: 24
          }}
        >
          <Button
            title="Cancelar"
            style={{ flex: 1, maxWidth: 122 }}
            secondary
            onPress={closeModal}
          />
          <Separator size={12} />
          <Button
            style={{ flex: 1, maxWidth: 122 }}
            title="Confirmar"
            onPress={onConfirm}
          />
        </View>
      </CardFooter>
    </Dialog>
  )
}

const styles = StyleSheet.create({
  campoNota: {
    borderStyle: "solid",
    borderWidth: 1,
    borderRadius: 8,
    textAlign: "center",
    paddingVertical: 12,
    fontSize: 32,
    color: "#56625A"
  },
  errorNota: {
    borderColor: Colors.error,
  }
})

export default ModalNota
