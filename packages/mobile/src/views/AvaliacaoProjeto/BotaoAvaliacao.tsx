import * as React from "react"
import {
  useAvaliarProjetoMutation,
  useAvaliacaoProjetoQuery,
  Projeto,
  Pessoa
} from "generated/graphql"
import { Text } from "react-native"
import { Button } from "components/Button"
import Colors from "assets/css/colors"
import { ConfirmationDialog } from "components/Dialogs/ConfirmationDialog"
import useConfirmationDialog from "hooks/useConfirmationDialog"
import Typography from "assets/css/typography"

interface BotaoAvaliacaoProps {
  idProjeto: Projeto["idProjeto"]
  idPessoa: Pessoa["idPessoa"]
  idVinculoPessoa: Pessoa["idVinculoPessoa"]
  onSuccess: () => void
}

const BotaoAvaliacao: React.SFC<BotaoAvaliacaoProps> = props => {
  const [avaliarProjeto, { data, loading, error }] = useAvaliarProjetoMutation()
  const [openDialog, dialogProps] = useConfirmationDialog({
    message:
      "Você precisa avaliar todos os ítens antes de finalizar a avaliação"
  })
  const [confirmarEnvio, dialogOptions] = useConfirmationDialog({
    message: "Avaliação enviada com sucesso!"
  })

  const avaliacaoProjeto = useAvaliacaoProjetoQuery({
    fetchPolicy: "cache-only",
    variables: {
      idProjeto: props.idProjeto,
      idPessoa: props.idPessoa,
      idVinculoPessoa: props.idVinculoPessoa
    }
  })

  return (
    <React.Fragment>
      {error && (
        <Text
          style={[
            Typography.subtitle,
            {
              color: Colors.error,
              opacity: 0.7,
              textAlign: "center",
              paddingBottom: 14
            }
          ]}
        >
          {`Não foi possivel avaliar o projeto!${"\n"}Tente novamente.`}
        </Text>
      )}
      <Button
        large
        title="Finalizar avaliação"
        loading={loading}
        style={{
          backgroundColor: Colors.green
        }}
        onPress={() => {
          if (
            avaliacaoProjeto.data &&
            avaliacaoProjeto.data.avaliacao &&
            avaliacaoProjeto.data.avaliacao.notas.find(
              elem => elem && elem.nota == null && elem.respostaTextual == null
            ) == undefined
          ) {
            const variables = {
              input: {
                avaliacao: {
                  idPessoa: props.idPessoa,
                  idVinculoPessoa: props.idVinculoPessoa,
                  idProjeto: props.idProjeto,
                  notas: avaliacaoProjeto.data.avaliacao.notas.map(elem => ({
                    idPergunta: elem!.pergunta.idPergunta,
                    nota: elem!.nota,
                    respostaTextual: elem!.respostaTextual
                  }))
                }
              }
            }

            avaliarProjeto({ variables }).then(() => {
              confirmarEnvio()
            })
          } else openDialog()
        }}
      />
      <ConfirmationDialog {...dialogProps} />
      <ConfirmationDialog {...dialogOptions} onConfirm={props.onSuccess} />
    </React.Fragment>
  )
}

export default BotaoAvaliacao
