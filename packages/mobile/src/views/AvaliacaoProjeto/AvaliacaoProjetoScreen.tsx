import * as React from "react"
import { View, Text } from "react-native"
import { NavigationScreenProp } from "react-navigation"
import { ScreenContainer } from "components/ScreenContainer"
import { ScreenTitle } from "components/ScreenTitle"
import CardAvaliacao from "./CardAvaliacao"
import ModalNota from "./ModalNota"
import { useSelector } from "react-redux"
import { RootState } from "redux/rootReducer"
import { MaterialIcons } from "@expo/vector-icons"
import {
  useAvaliacaoProjetoQuery,
  Pergunta,
  useUsuarioAtualQuery,
  AvaliacaoProjetoFragment
} from "generated/graphql"
import { RoundedCard } from "components/Cards"
import { Button } from "components/Button"
import { useApolloClient, useQuery, useMutation } from "react-apollo"
import { fillAvaliacaoProjetoCache } from "graphql/cacheHelpers"
import BotaoAvaliacao from "./BotaoAvaliacao"
import {
  avaliacaoProjetoFragment,
  mockAvaliacaoProjetoQuery
} from "graphql/queries/avaliacaoProjeto"
import { material } from "react-native-typography"
import Typography from "assets/css/typography"
import Colors from "assets/css/colors"
import { useRoute } from "@react-navigation/native"
import CardVideo from "./CardVideo"
import Separator from "components/Separator/Separator"

interface AvaliacaoProjetoScreenProps {
  navigation: NavigationScreenProp<{
    projectId: number
    tituloProjeto: string
    podeAvaliar: boolean
  }>
}

const dummyArray = new Array(8).fill(0)

const AvaliacaoProjetoScreen: React.FC<AvaliacaoProjetoScreenProps> = props => {
  const route: any = useRoute() //VER DEPOIS SOBRE ESSE TIPO ANY
  const idProjeto = route.params?.projectId
  const client = useApolloClient()
  const userDataQuery = useUsuarioAtualQuery()
  // Query local
  const usuarioAtual = userDataQuery!.data!.usuarioAtual
  const avaliacaoProjeto = useAvaliacaoProjetoQuery({
    variables: {
      idProjeto: idProjeto!,
      idPessoa: usuarioAtual.idPessoa,
      idVinculoPessoa: usuarioAtual.idVinculoPessoa
    },
    onCompleted: () =>
      // Popular avaliacao no caso de projeto que nunca foi
      // avaliado
      fillAvaliacaoProjetoCache(client, {
        idProjeto: idProjeto
      })
  })

  const avaliacaoProjetoCache = useAvaliacaoProjetoQuery({
    fetchPolicy: "cache-only",
    variables: {
      idProjeto: idProjeto!,
      idPessoa: usuarioAtual.idPessoa,
      idVinculoPessoa: usuarioAtual.idVinculoPessoa
    }
  })

  const goBack = () => props.navigation.goBack()
  const { podeAvaliar } = useSelector(
    (state: RootState) => state.navigation.params
  )
  const [modalState, setModalState] = React.useState({
    visible: false,
    conteudoPergunta: "",
    nomeCategoria: "",
    idPergunta: 0
  })

  const openModal = (options: {
    idPergunta: Pergunta["idPergunta"]
    conteudoPergunta: string
    nomeCategoria: string
  }) => () =>
    setModalState({
      ...options,
      visible: true
    })

  if (
    avaliacaoProjeto.loading ||
    (avaliacaoProjeto.data && !avaliacaoProjeto.data.avaliacao)
  )
    return (
      <ScreenContainer>
        <View
          style={{
            paddingTop: 14,
            paddingBottom: 0
          }}
        >
          <MaterialIcons
            name="arrow-back"
            color="black"
            size={25}
            onPress={goBack}
          />
        </View>
        <ScreenTitle
          title={podeAvaliar ? "Avalie" : "Sua Avaliação"}
          subtitle={route.params?.tituloProjeto}
          centerSubtitle
          style={{ paddingTop: 14 }}
        />

        {dummyArray.map((_, index) => (
          <RoundedCard key={index} placeholder />
        ))}
      </ScreenContainer>
    )

  if (avaliacaoProjeto.data && avaliacaoProjeto.data.avaliacao) {
    return (
      <ScreenContainer>
        <View
          style={{
            paddingTop: 14,
            paddingBottom: 0
          }}
        >
          <MaterialIcons
            name="arrow-back"
            color="black"
            size={25}
            onPress={goBack}
          />
        </View>
        <ScreenTitle
          title={podeAvaliar ? "Avalie" : "Sua Avaliação"}
          centerSubtitle
          subtitle={avaliacaoProjeto.data!.projeto!.tituloProjeto}
          style={{ paddingTop: 14 }}
        />
        {avaliacaoProjeto.data.projeto?.categoria.idCategoria == 2 ? (
          <CardVideo link={avaliacaoProjeto.data!.projeto!.linkArtefato!} />
        ) : null}
        {avaliacaoProjeto.data.avaliacao.notas
          .sort((a, b) => {
            return a!.pergunta!.idPergunta - b!.pergunta!.idPergunta
          })
          .map((avaliacaoPergunta, idx, total) => {
            const isLast = idx === total.length - 1
            //sorting in ascending order
            return (
              <CardAvaliacao
                key={`${avaliacaoPergunta!.pergunta.idPergunta}`}
                nota={avaliacaoPergunta!.nota!}
                idPergunta={avaliacaoPergunta!.pergunta.idPergunta}
                conteudoPergunta={avaliacaoPergunta!.pergunta.conteudoPergunta}
                nomeCategoria={
                  avaliacaoPergunta!.pergunta.categoria!.nomeCategoria
                }
                podeAvaliar={podeAvaliar}
                openModal={podeAvaliar && openModal}
                idProjeto={idProjeto}
                respostaTextual={avaliacaoPergunta!.respostaTextual!}
                usarCampoDeTexto={isLast}
              />
            )
          })}
        <ModalNota
          visible={modalState.visible}
          idProjeto={idProjeto}
          idPergunta={modalState.idPergunta}
          conteudoPergunta={modalState.conteudoPergunta}
          dismiss={() =>
            setModalState({
              ...modalState,
              idPergunta: 0,
              visible: false
            })
          }
          nomeCategoria={modalState.nomeCategoria}
        />
        <View
          style={{
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            paddingTop: 12,
            paddingBottom: 24
          }}
        >
          {podeAvaliar && (
            <BotaoAvaliacao
              idProjeto={idProjeto}
              idPessoa={usuarioAtual.idPessoa}
              idVinculoPessoa={usuarioAtual.idVinculoPessoa}
              onSuccess={() => props.navigation.goBack()}
            />
          )}
        </View>
      </ScreenContainer>
    )
  } else return <ScreenContainer />
}

export default AvaliacaoProjetoScreen
