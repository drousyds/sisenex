import React from "react"
import * as WebBrowser from "expo-web-browser"
import { RoundedCard } from "components/Cards"
import { TouchableOpacity } from "react-native-gesture-handler"
import { View, Image, Text, StyleSheet } from "react-native"
import { MaterialIcons } from "@expo/vector-icons"
import Typography from "assets/css/typography"
import Separator from "components/Separator/Separator"

interface CardVideoProps {
  link: string
}
// ver a forma correta de fazer essa tipagem da função
function AbrirLink(link: string) {
  WebBrowser.openBrowserAsync(link, { showInRecents: true })
}

const CardVideo: React.FC<CardVideoProps> = ({ link }) => {
  return (
    <View>
      <Text>Artefato </Text>
      <Separator vertical size={12} />
      <View style={{ alignSelf: "center" }}>
        <RoundedCard style={{ width: 250, minHeight: 0 }}>
          <TouchableOpacity onPress={() => AbrirLink(link)}>
            <View style={styles.container}>
              <View>
                <Image
                  style={styles.image}
                  resizeMode="contain"
                  source={require("assets/video.png")}
                />
              </View>
              <View style={styles.textContainer}>
                <Text>Assistir ao vídeo</Text>
              </View>
            </View>
          </TouchableOpacity>
        </RoundedCard>
      </View>
      <Separator vertical size={24} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 8
  },
  image: {
    width: 48,
    height: 48,
    marginRight: 16
  },
  textContainer: {
    paddingLeft: 12,
    marginRight: 13
  }
})

export default CardVideo
