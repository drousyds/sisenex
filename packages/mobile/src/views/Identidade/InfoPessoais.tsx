import * as React from "react"
import { Pessoa } from "generated/graphql"
import { View, Image, Text, StyleSheet } from "react-native"
import { RoundedCard } from "components/Cards"
import { human } from "react-native-typography"
import Colors from "assets/css/colors"
import Typography from "assets/css/typography"

export interface InfoPessoaisProps {
  nomeSocial: Pessoa["nomeSocialPessoa"]
  lotacao: Pessoa["lotacaoPessoa"]
  matricula: Pessoa["matriculaPessoa"]
  avatarUrl: string
}

const InfoPessoais: React.SFC<InfoPessoaisProps> = props => {
  return (
    <RoundedCard style={{ minHeight: 0 }}>
      <View style={styles.container}>
        <View>
          <Image source={{ uri: props.avatarUrl }} style={styles.image} />
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.nomeSocial}>{props.nomeSocial}</Text>
          {props.lotacao && (
            <Text style={styles.detalhesPessoa}>{props.lotacao}</Text>
          )}
          <Text style={styles.detalhesPessoa}>{`${props.lotacao ? "SIAPE" : "Matricula"
            }: ${props.matricula}`}</Text>
        </View>
      </View>
    </RoundedCard>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 8
  },
  image: {
    width: 48,
    height: 48,
    borderRadius: 4
  },
  textContainer: { paddingLeft: 12, marginRight: 13, paddingRight: 15 },
  nomeSocial: {
    ...Typography.header3,
    fontSize: 16,
    lineHeight: 16
  },
  detalhesPessoa: {
    ...Typography.subtitle,
    fontSize: 12,
    lineHeight: 16,
    color: "gray",
    fontWeight: "100"
  }
})

export default InfoPessoais
