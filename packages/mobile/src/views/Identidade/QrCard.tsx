import * as React from "react"
import QRCode from "react-native-qrcode-svg"
import { RoundedCard } from "components/Cards"
import { View, Text, StyleSheet } from "react-native"
import { Pessoa } from "generated/graphql"
import { human } from "react-native-typography"

interface QrCardProps {
  qrValue: string
  idPessoa: Pessoa["idPessoa"]
  idVinculoPessoa: Pessoa["idVinculoPessoa"]
}

export const PADDING = "62C693BA9F4952D869C6C7D37"

const QrCard: React.SFC<QrCardProps> = props => {
  return (
    <RoundedCard>
      <View style={styles.container}>
        <QRCode
          value={`${PADDING}-${props.idPessoa}-${props.idVinculoPessoa}`}
          size={200}
          logoSize={40}
          logoBorderRadius={20}
          logoMargin={4}
          logo={require("../../assets/app-icon.png")}
        />
      </View>
      <View style={styles.textContainer}>
        <Text style={styles.text}>{`ID: ${props.idPessoa}-${
          props.idVinculoPessoa
        }`}</Text>
      </View>
    </RoundedCard>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 24
  },
  textContainer: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 24
  },
  text: {
    ...human.bodyObject,
    color: "#777777"
  }
})

export default QrCard
