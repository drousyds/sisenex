import React, { useEffect } from "react"
import { BackHandler, StyleSheet, Text } from "react-native"
import { NavigationScreenProp } from "react-navigation"
import ScreenContainer from "components/ScreenContainer/ScreenContainer"
import { ScreenTitle } from "components/ScreenTitle"
import InfoPessoais from "./InfoPessoais"
import Separator from "components/Separator/Separator"
import QrCard from "./QrCard"
import { Button } from "components/Button"
import { useUsuarioAtualQuery } from "generated/graphql"
import { useDispatch } from "react-redux"
import { navigate } from "redux/Navigation/actions"
import * as SplashScreen from "expo-splash-screen"
import { logout } from "redux/Auth/actions"
import { useFocusEffect } from "@react-navigation/native"
import CardProblema from "views/Identidade/CardProblema"
import Colors from "assets/css/colors"
import { TouchableOpacity } from "react-native-gesture-handler"

export interface IdentidadeScreenProps {
  navigation: NavigationScreenProp<void>
}

const IdentidadeScreen = (props: IdentidadeScreenProps) => {
  useEffect(() => {
    SplashScreen.hideAsync()
  }, [])
  const { data } = useUsuarioAtualQuery()
  const onBackPress = () => true
  const dispatch = useDispatch()
  const onLogout = () => dispatch(logout())

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener("hardwareBackPress", onBackPress)
      return () =>
        BackHandler.removeEventListener("hardwareBackPress", onBackPress)
    }, [])
  )

  const navigateToAvisosLegais = () =>
    dispatch(
      navigate({
        routeName: "AvisosLegais"
      })
    )

  if (!(data && data.usuarioAtual)) return <ScreenContainer />

  const usuario = data.usuarioAtual
  return (
    <ScreenContainer>
      {/* <ScreenTitle
        title="Identifique-se"
        subtitle="Mostre as suas credenciais a um monitor para começar a avaliação."
      /> */}
      <ScreenTitle
        title="Início"
        subtitle="Essas são suas credenciais de participação no evento. Informe-as quando necessário"
      />
      <InfoPessoais
        nomeSocial={usuario.nomeSocialPessoa}
        lotacao={usuario.lotacaoPessoa}
        matricula={usuario.matriculaPessoa}
        avatarUrl={usuario.avatarUrl}
      />
      <Separator vertical />
      <CardProblema />
      <Separator vertical />
      <QrCard
        idPessoa={usuario.idPessoa}
        idVinculoPessoa={usuario.idVinculoPessoa}
        qrValue={`${usuario.idPessoa}-${usuario.idVinculoPessoa}`}
      />
      <Separator vertical size={23} />
      <TouchableOpacity onPress={navigateToAvisosLegais}>
        <Text style={styles.link}>Lei Geral de Proteção de Dados</Text>
      </TouchableOpacity>
      <Separator vertical size={8} />
      <Button
        secondary
        large
        title="Sair"
        onPress={onLogout}
        style={{ alignSelf: "center", marginVertical: 12 }}
      />
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  link: {
    fontSize: 15,
    fontWeight: "bold",
    letterSpacing: 0.4,
    color: Colors.blueText,
    textDecorationLine: "underline",
    alignSelf: "center"
  }
})

export default IdentidadeScreen
