import * as React from "react"
import { View, Text, StyleSheet, BackHandler } from "react-native"
import { ScreenContainer } from "components/ScreenContainer"
import { NavigationScreenProp } from "react-navigation"
import * as SplashScreen from 'expo-splash-screen'
import { ScreenTitle } from "components/ScreenTitle"
import Colors from "assets/css/colors"
import Separator from "components/Separator/Separator"
import PrimaryButton from "components/Button/Button"
import { CheckBox } from '@rneui/themed';
import { clearAssinouTermoCache, saveAssinouTermoCache } from "caching"
import { useDispatch, useSelector } from "react-redux"
import { navigate } from "redux/Navigation/actions"
import { useApolloClient } from "react-apollo"
import { useUsuarioAtualQuery } from "generated/graphql"
import { AndroidBackHandler } from "react-navigation-backhandler"
import { useFocusEffect } from "@react-navigation/native"
import { assinouTermo, logout } from "redux/Auth/actions"
import { TouchableOpacity } from "react-native-gesture-handler"

interface TermoDeUsoScreenProps {
  navigation: NavigationScreenProp<{}>
}

const TermoDeUsoScreen: React.FC<TermoDeUsoScreenProps> = props => {
  React.useEffect(() => {
    SplashScreen.hideAsync()
  }, [])
  const onBackPress = () => true
  const client = useApolloClient()
  const userDataQuery = useUsuarioAtualQuery()
  // Query local
  const usuarioAtual = userDataQuery!.data!.usuarioAtual
  const [checked, setChecked] = React.useState(false)
  const dispatch = useDispatch()
  const onConfirm = () => saveAssinouTermoCache().then(() => dispatch(assinouTermo()))

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener("hardwareBackPress", onBackPress)
      return () =>
        BackHandler.removeEventListener("hardwareBackPress", onBackPress)
    }, [])
  )

  const navigateToAvisosLegais = () =>
    dispatch(
      navigate({
        routeName: "AvisosLegais",
      })
    )

  return (
    <ScreenContainer>
      <ScreenTitle title={"Termo de Uso"} />
      <React.Fragment>
        <CorpoTermoDeUso navigateToAvisosLegais={navigateToAvisosLegais} />
        <View style={styles.confirmContainer}>
          <CheckBox
            title="Aceito os Termos de Uso"
            containerStyle={{
              backgroundColor: Colors.background,
              borderWidth: 0,
              paddingBottom: 20
            }}
            checkedColor={Colors.green}
            checked={checked}
            onPress={() => setChecked(!checked)}
            textStyle={{
              fontWeight: 'normal',
              fontSize: 14
            }}
          />
          <PrimaryButton
            disabled={!checked}
            large
            title="Continuar"
            onPress={onConfirm}
          />
        </View>
      </React.Fragment>
    </ScreenContainer>
  )
}

const CorpoTermoDeUso: React.FC<{ navigateToAvisosLegais: Function }> = props => {
  return (
     <View style={styles.container}>

  
      <Text style={styles.body}>
        O SisEnex é um sistema desenvolvido exclusivamente para apoiar o Encontro de Extensão (ENEX) da Universidade Federal da Paraíba (UFPB). Seu principal objetivo é automatizar o processo de avaliação e premiação das ações de extensão apresentadas durante o evento. O SisEnex foi projetado para executar apenas funcionalidades essenciais para a avaliação das apresentações, limitando-se a armazenar e processar os dados necessários para essa finalidade.
      </Text>

      <Separator vertical size={20} />
      <Text style={styles.section}>Compromisso com a Privacidade</Text>
      <Separator vertical size={8} />
      <Text style={styles.body}>
        Nosso compromisso é garantir a segurança, privacidade e proteção dos dados pessoais de todos os docentes, discentes e usuários que utilizam a plataforma. Todos os dados coletados e processados pelo SisEnex são tratados de acordo com a Lei Geral de Proteção de Dados Pessoais (LGPD - Lei nº 13.709/2018). Esses dados serão utilizados exclusivamente para os fins descritos, e não serão compartilhados com terceiros sem o consentimento explícito dos usuários, exceto quando exigido por lei.
      </Text>
      <Text style={styles.body}>
        Ao utilizar o SisEnex, o usuário concorda com a coleta e o uso de suas informações conforme descrito nesta política. Para dúvidas ou exercício dos direitos previstos na LGPD, como a solicitação de acesso, correção ou exclusão de dados, o usuário pode entrar em contato pelos canais disponíveis na plataforma.
      </Text>

      <Separator vertical size={20} />
      <Text style={styles.section}>Responsabilidades dos Usuários</Text>
      <Separator vertical size={8} />
      <Text style={styles.body}>
        O uso do SisEnex é restrito a estudantes e servidores da UFPB com vínculo ativo, que tenham participado das ações de extensão apresentadas no ENEX. Ao utilizar a plataforma, os usuários se comprometem a:
      </Text>
      <View style={{ paddingLeft: 20 }}>
        <Text style={styles.bodyItem}>• Usar suas contas pessoais e manter as senhas em sigilo;</Text>
        <Text style={styles.bodyItem}>• Não compartilhar senhas ou contas com terceiros;</Text>
        <Text style={styles.bodyItem}>• Respeitar os direitos de privacidade e segurança de outros usuários.</Text>
      </View>

      <Separator vertical size={20} />
      <Text style={styles.section}>Limitações de Responsabilidade</Text>
      <Separator vertical size={8} />
      <Text style={styles.body}>
        O SisEnex não se responsabiliza por problemas decorrentes de fatores externos ao sistema, como:
      </Text>
      <View style={{ paddingLeft: 20 }}>
        <Text style={styles.bodyItem}>• Equipamentos infectados ou comprometidos;</Text>
        <Text style={styles.bodyItem}>• Proteção inadequada dos dispositivos dos usuários;</Text>
        <Text style={styles.bodyItem}>• Monitoramento ilegal de dispositivos.</Text>
      </View>

      <Separator vertical size={30} />
      <Text style={[styles.section, { textAlign: 'center' }]}>Aviso de Política de Privacidade</Text>
      <Separator vertical size={30} />

      <Text style={styles.section}>Finalidade dos Dados Coletados</Text>
      <Separator vertical size={8} />
      <Text style={styles.body}>
        Os dados são coletados por meio da autenticação no Sistema de Controle Acadêmico (SIGAA/UFPB) para verificar o vínculo ativo dos participantes com a UFPB e garantir a integridade do processo avaliativo. Esses dados são usados exclusivamente para autenticação e alocação nas avaliações das ações de extensão, que determinam os projetos premiados no ENEX.
      </Text>

      <Separator vertical size={20} />
      <Text style={styles.section}>Dados Coletados</Text>
      <Separator vertical size={8} />
      <Text style={styles.body}>
        Os dados coletados incluem: nome social, matrícula, lotação, endereço de e-mail e telefone. Esses dados são obtidos automaticamente ao autenticar-se pela primeira vez no aplicativo via SIGAA/UFPB. O uso do SisEnex implica a concordância com esta Política de Privacidade.
      </Text>

      <Separator vertical size={20} />
      <Text style={styles.section}>Uso dos Dados</Text>
      <Separator vertical size={8} />
      <Text style={styles.body}>
        Os dados são utilizados para:
      </Text>
      <View style={{ paddingLeft: 20 }}>
        <Text style={styles.bodyItem}>• Facilitar o processo de avaliação das ações apresentadas no ENEX;</Text>
        <Text style={styles.bodyItem}>• Enviar comunicações relevantes relacionadas ao SisEnex e ao evento ENEX;</Text>
        <Text style={styles.bodyItem}>• Gerar relatórios internos para a Pró-Reitoria de Extensão (PROEX).</Text>
      </View>
      <Text style={styles.body}>
        Esses dados não serão compartilhados com terceiros fora da UFPB, exceto quando expressamente autorizado ou exigido por lei.
      </Text>

      <Separator vertical size={20} />
      <Text style={styles.section}>Armazenamento dos Dados</Text>
      <Separator vertical size={8} />
      <Text style={styles.body}>
        Os dados dos usuários são mantidos durante o processo de avaliação e divulgação dos premiados no ENEX. Após a finalização do evento, os dados são consolidados em relatórios internos enviados à PROEX para auditoria, e as informações pessoais são excluídas do sistema. O usuário pode solicitar a exclusão de seus dados a qualquer momento, conforme previsto na LGPD.
      </Text>

      <Separator vertical size={20} />
      <Text style={styles.section}>Segurança dos Dados</Text>
      <Separator vertical size={8} />
      <Text style={styles.body}>
        O SisEnex utiliza medidas de segurança reconhecidas para proteger os dados pessoais, embora nenhum sistema seja totalmente invulnerável. O processo de autenticação é de responsabilidade da Superintendência de Tecnologia da Informação (STI) da UFPB.
      </Text>
      <Text style={styles.body}>
        O SisEnex se compromete em manter este Aviso de Privacidade atualizado, observando suas determinações e zelando por seu cumprimento. Em caso de ataque, vazamento ou algum corrompimento de dados, se compromete em realizar um comunicado de incidente de segurança.
      </Text>
      <Text style={styles.body}>
        Qualquer comunicado de incidente de segurança será disponibilizado por contato direto com o e-mail de nossos usuários e com o auxílio dos meios de comunicação da PROEX.
      </Text>

      <Separator vertical size={20} />
      <Text style={styles.section}>Alterações na Política de Privacidade</Text>
      <Separator vertical size={8} />
      <Text style={styles.body}>
        Esta política pode ser alterada periodicamente, e os usuários serão informados sobre mudanças significativas. A continuação do uso do SisEnex após modificações implica a aceitação das alterações.
      </Text>

      <Separator vertical size={20} />
      <Text style={styles.section}>Contato</Text>
      <Separator vertical size={8} />
      <Text style={styles.body}>
        Para dúvidas, sugestões ou reclamações sobre a Política de Privacidade, entre em contato pelo e-mail: {"\n"}sisenex@lumo.ci.ufpb.br.
      </Text>

      <Separator vertical size={15} />
      <TouchableOpacity onPress={() => props.navigateToAvisosLegais()}>
        <Text style={styles.link}>Lei Geral de Proteção de Dados</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    minHeight: 200,
    display: "flex",
    backgroundColor: "white",
    borderRadius: 10,
    paddingHorizontal: 22,
    paddingVertical: 18,
    justifyContent: "flex-start",
    elevation: 2,
    marginHorizontal: 2,
    shadowColor: "#518091",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 16,
    shadowOpacity: 0.2
  },
  section: {
    fontSize: 14,
    fontWeight: "bold",
    letterSpacing: 0.4,
    color: Colors.primaryText
  },
  link: {
    fontSize: 15,
    fontWeight: "bold",
    letterSpacing: 0.4,
    color: Colors.blueText,
    textDecorationLine: 'underline',
    alignSelf: 'center'
  },
  body: {
    fontSize: 14,
    fontWeight: "normal",
    letterSpacing: 0.4,
    color: Colors.secondaryText,
    paddingVertical: 10
  },
  bodyItem: {
    fontSize: 14,
    fontWeight: "normal",
    letterSpacing: 0.4,
    color: Colors.secondaryText,
  
  },
  confirmContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 18
  }
})
export default TermoDeUsoScreen
