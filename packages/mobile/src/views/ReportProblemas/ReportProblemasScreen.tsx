import React, { useState } from "react"
import { Text, TextInput, KeyboardAvoidingView, View } from "react-native"
import { ScreenContainer } from "components/ScreenContainer"
import { ScreenTitle } from "components/ScreenTitle"
import { BackButton } from "components/Header"
import { Button } from "components/Button"
import { RoundedCard } from "components/Cards"
import Separator from "components/Separator/Separator"
import useConfirmationDialog from "hooks/useConfirmationDialog"
import { ConfirmationDialog } from "components/Dialogs/ConfirmationDialog"
import { useCriarReportMutation, useUsuarioAtualQuery } from "generated/graphql"
import { loadUsuarioAtualCache } from "caching"
import { useApolloClient } from "react-apollo"
import { getApresentacaoAtual } from "graphql/cacheHelpers"
import { useSelector } from "react-redux"
import { getAvaliador } from "redux/Auth/selectors"

interface ReportProblemasProps {}

export const ReportProblemas: React.SFC<ReportProblemasProps> = props => {
  const [reportarProblemaMutation, { loading }] = useCriarReportMutation()
  const [text, setText] = useState("")
  const [email, setEmail] = useState("")
  const avaliador = useSelector(getAvaliador)
  const userDataQuery = useUsuarioAtualQuery()
  const usuarioAtual = userDataQuery!.data!.usuarioAtual
  const apresentacaoAtual = avaliador ? false : getApresentacaoAtual()

  const onSubmission = () => {
    if (
      (text.length > 0 && email.length > 0) ||
      (text.length > 0 && avaliador == 0)
    )
      reportarProblemaMutation({
        variables: {
          idPessoa: usuarioAtual.idPessoa,
          idVinculoPessoa: usuarioAtual.idVinculoPessoa,
          conteudoReport: text,
          idApresentacao: apresentacaoAtual.idApresentacao,
          id2Apresentacao: apresentacaoAtual.id2Apresentacao,
          emailReport: email
        }
      }).then(() => {
        setText("")
        setEmail("")
        reportarProblema()
      })
    if (text.length > 0 && email.length == 0 && avaliador)
      emptyEmailFieldProblem()
    if (text.length == 0 && avaliador == 0) emptyTextFieldProblem()
    if (text.length == 0 && email.length > 0 && avaliador)
      emptyTextFieldProblem()
    if (text.length == 0 && email.length == 0 && avaliador) emptyFieldsProblem()
  }

  const [emailInvalido, emailInvalidoOptions] = useConfirmationDialog({
    message: "Email inválido"
  })

  const [reportarProblema, dialogOptions] = useConfirmationDialog({
    message: "Mensagem enviada!"
  })

  const [
    emptyEmailFieldProblem,
    emailErrorDialogOptions
  ] = useConfirmationDialog({
    message: "Mensagem não enviada:\nCampo de email vazio!"
  })

  const [emptyTextFieldProblem, textErrorDialogOptions] = useConfirmationDialog(
    {
      message: "Mensagem não enviada:\nCampo de mensagem vazio!"
    }
  )

  const [emptyFieldsProblem, emptyFieldsDialogOptions] = useConfirmationDialog({
    message: "Mensagem não enviada:\nCampos de email e mensagem vazios!"
  })

  function validarEmail(email: string) {
    const usuario = email.substring(0, email.indexOf("@"))
    const dominio = email.substring(email.indexOf("@") + 1, email.length)
    //console.log(email.length)
    if (email.length > 0) {
      if (
        usuario.length >= 1 &&
        dominio.length >= 3 &&
        usuario.search("@") == -1 &&
        dominio.search("@") == -1 &&
        usuario.search(" ") == -1 &&
        dominio.search(" ") == -1 &&
        dominio.search(".") != -1 &&
        dominio.indexOf(".") >= 1 &&
        dominio.lastIndexOf(".") < dominio.length - 2
      ) {
        return true
      } else {
        return false
      }
    } else {
      return true
    }
  }

  return (
    <ScreenContainer>
      <BackButton />
      <ScreenTitle
        style={{
          paddingTop: 8
        }}
        title="Ajuda"
        subtitle="Informe um problema a equipe de suporte para que possamos ajudar"
      />
      {avaliador ? (
        <View>
          <RoundedCard
            style={{
              justifyContent: "flex-start",
              alignItems: "flex-start",
              minHeight: 0
            }}
          >
            <KeyboardAvoidingView>
              <TextInput
                style={{ padding: 0, margin: 0, width: 350}}
                value={email}
                onChangeText={newEmail => setEmail(newEmail)}
                multiline
                placeholder="Email"
              />
            </KeyboardAvoidingView>
          </RoundedCard>
        </View>
      ) : null}
      <Separator vertical size={14} />
      <RoundedCard
        style={{
          justifyContent: "flex-start", 
          alignItems: "flex-start",
          maxHeight: 250
        }}   
      >
         <KeyboardAvoidingView>
          <TextInput
            style={{padding: 0, margin: 0, width: 350 , height: "100%"}}
            value={text}
            onChangeText={newText => setText(newText)}
            multiline
            textAlignVertical="top"
            placeholder="Insira aqui sua mensagem"
          />
        </KeyboardAvoidingView> 
      </RoundedCard>
      <Separator vertical size={14} />
      <Button
        title="Enviar"
        large
        loading={loading}
        style={{
          alignSelf: "center"
        }}
        onPress={
          avaliador
            ? validarEmail(email)
              ? onSubmission
              : emailInvalido
            : onSubmission
        }
      />
      <ConfirmationDialog {...emailInvalidoOptions} />
      <ConfirmationDialog {...textErrorDialogOptions} />
      <ConfirmationDialog {...emailErrorDialogOptions} />
      <ConfirmationDialog {...dialogOptions} />
      <ConfirmationDialog {...emptyFieldsDialogOptions} />
    </ScreenContainer>
  )
}

export default ReportProblemas
