import * as React from "react"
import { View, KeyboardAvoidingView } from "react-native"
import { ScreenContainer } from "components/ScreenContainer"
import { ScreenTitle } from "components/ScreenTitle"
import * as SplashScreen from 'expo-splash-screen'
import { Button } from "components/Button"
import Separator from "components/Separator/Separator"
import { useDispatch } from "react-redux"
import { navigate } from "redux/Navigation/actions"
import { useBaterAvaliadorMutation, ApresentacaoAtual } from "generated/graphql"
import { useInput } from "hooks/useInput"
import { SimpleTextInput } from "components/TextInput/SimpleTextInput"
import { cacheBaterAvaliador, getApresentacaoAtual } from "graphql/cacheHelpers"
import getErrorCode from "helpers/getErrorCode"
import useConfirmationDialog from "hooks/useConfirmationDialog"
import { ConfirmationDialog } from "components/Dialogs/ConfirmationDialog"
import { ApolloError } from "apollo-client"


interface RegistroPessoaInputScreenProps { }

export const RegistroPessoaInputScreen: React.SFC<
  RegistroPessoaInputScreenProps
> = props => {
  React.useEffect(() => {
    SplashScreen.hideAsync()
  }, [])
  const dispatch = useDispatch()
  const validator = (text: string) => text.match(/\d+-\d+/)
  const [dialog, dialogError] = useConfirmationDialog({
    message: "Código inválido"
  })
  const apresentacao: ApresentacaoAtual = getApresentacaoAtual()
  const onConfirm = () => {
    if (valid) {
      baterAvaliador()
    }
  }
  const config: any = {
    keyboardType: "default",
    validator: validator,
    onSubmitEditing: onConfirm,
    focusOnMount: true,
    label: "Codigo"
  }
  const [inputProps, { valid, text, errorState, onSubmitEditing }] = useInput(
    config
  )
  const [baterAvaliador, { data, loading, error }] = useBaterAvaliadorMutation({
    variables: {
      idPessoa: text.split("-")[0],
      idVinculoPessoa: text.split("-")[1],
      idApresentacao: apresentacao.idApresentacao,
      id2Apresentacao: apresentacao.id2Apresentacao
    },
    onCompleted: () => {
      dispatch(
        navigate({
          routeName: "PessoasApresentacao"
        })
      )
    },
    onError: (error: ApolloError) => {
      const errorCode = getErrorCode(error)

      if (errorCode)
        errorCode == 666 ? showErrorDialog() : dialog()
    },
    update: (cache, { data }) => {
      data && cacheBaterAvaliador(cache, data)
    }
  })

  const [showErrorDialog, dialogOptions] = useConfirmationDialog({
    message: "Avaliador não pode avaliar pois coordena um ou mais projetos nesta apresentação"
  })

  const onCancel = () =>
    dispatch(
      navigate({
        routeName: "RegistroPessoa"
      })
    )

  return (
    <ScreenContainer>
      <ScreenTitle
        subtitle="Digite as credenciais de um avaliador para adiciona-lo a apresentação."
        title="Pessoas"
      />
      <KeyboardAvoidingView behavior="padding">
        <Separator vertical size={32} />
        <SimpleTextInput {...inputProps} errorState={errorState} />
        <Separator vertical size={50} />
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center"
          }}
        >
          <Button title="Cancelar" grey onPress={onCancel} />
          <Separator size={12} />
          <Button
            title="Confirmar"
            onPress={() => {
              onSubmitEditing(text)
            }}
            loading={loading}
          />
        </View>
      </KeyboardAvoidingView>
      <ConfirmationDialog {...dialogOptions} />
      <ConfirmationDialog {...dialogError} />
    </ScreenContainer>
  )
}

export default RegistroPessoaInputScreen
