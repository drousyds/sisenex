import * as React from "react"
import { Dimensions } from "react-native"
import { ScreenContainer } from "components/ScreenContainer"
import { StackNavigationProp } from '@react-navigation/stack'
import * as SplashScreen from 'expo-splash-screen'
import { ClearButton } from "components/Button"
import { navigate } from "redux/Navigation/actions"
import { useDispatch } from "react-redux"
import { ScreenTitle } from "components/ScreenTitle"
import { CameraView } from "expo-camera"
import { BarcodeScanningResult } from "expo-camera/build/Camera.types"
import { useApolloClient } from "react-apollo"
import { apresentacaoAtual } from "graphql/queries/apresentacaoAtual"
import { BackButton } from "components/Header"
import { ApresentacaoAtual, useBaterAvaliadorMutation } from "generated/graphql"
import { pessoasApresentacao } from "graphql/queries/pessoasApresentacao"
import { withCameraPermission } from "helpers/withCameraPermission"
import { cacheBaterAvaliador, getApresentacaoAtual } from "graphql/cacheHelpers"
import { ApolloError } from "apollo-client"
import getErrorCode from "helpers/getErrorCode"
import useConfirmationDialog from "hooks/useConfirmationDialog"
import { ConfirmationDialog } from "components/Dialogs/ConfirmationDialog"

type RegistroPessoaScreenProps = {
  navigation: StackNavigationProp<any, 'RegistroPessoa'>
}

const RegistroPessoaScreen: React.FC<RegistroPessoaScreenProps> = ({ navigation }) => {
  React.useEffect(() => {
    SplashScreen.hideAsync()
  }, [])
  withCameraPermission()
  const dispatch = useDispatch()
  const { height } = Dimensions.get("window")
  const onButtonPressed = () => {
    navigation.navigate('RegistroPessoaInput')
  }

  const apresentacao: ApresentacaoAtual = getApresentacaoAtual()
  const [showErrorDialog, dialogOptions] = useConfirmationDialog({
    message: "Avaliador não pode avaliar pois coordena um ou mais projetos nesta apresentação"
  })
  const [baterAvaliador, { data, loading, error }] = useBaterAvaliadorMutation({
    onCompleted: () => {
      navigate({
        routeName: "PessoasApresentacao"
      })
    },
    onError: (error: ApolloError) => {
      const errorCode = getErrorCode(error)

      if (errorCode && errorCode == 666)
        showErrorDialog()
    },
    update: (cache, { data }) => {
      data && cacheBaterAvaliador(cache, data)
    }
  })

  const onIdScanned = (result: BarcodeScanningResult) => {
    const idPessoa = result.data.split("-")[1]
    const idVinculoPessoa = result.data.split("-")[2]

    baterAvaliador({
      variables: {
        idPessoa,
        idVinculoPessoa,
        idApresentacao: apresentacao.idApresentacao,
        id2Apresentacao: apresentacao.id2Apresentacao
      }
    })
  }

  return (
    <ScreenContainer>
      <BackButton />
      <ScreenTitle
        style={{ paddingTop: 8 }}
        title="Pessoas"
        subtitle="Escaneie as credenciais de um avaliador para adiciona-lo a apresentação."
      />
      <CameraView
        ratio="1:1"
        style={{
          marginHorizontal: -14,
          overflow: "hidden",
          height: height * 0.6,
          width: height * 0.6
        }}
        onBarcodeScanned={onIdScanned}
      />
      <ClearButton onPress={onButtonPressed} title="Digitar Código" />
      <ConfirmationDialog {...dialogOptions} />
    </ScreenContainer>
  )
}

export default RegistroPessoaScreen
