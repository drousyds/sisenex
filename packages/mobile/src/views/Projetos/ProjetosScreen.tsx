import * as React from "react"
import { View, Image } from "react-native"
import { ScreenTitle } from "components/ScreenTitle"
import { ScreenContainer } from "components/ScreenContainer"
import CardProjeto from "./CardProjeto"
import {
  useProjetosPendentesQuery,
  UsuarioAtualQuery
} from "generated/graphql"
import RoundedCard from "components/Cards/RoundedCard"
import { useApolloClient } from "react-apollo"
import { usuarioAtual } from "graphql/queries/usuarioAtual"
import EmptyState from "components/StateIndicator/EmptyState"
import { BackButton } from "components/Header"
import { useRoute } from "@react-navigation/native"
import { mockProjetosPendentesQuery } from "graphql/queries/projetosPendentes"
import { Text } from "react-native-paper"

interface ProjetosScreenProps { }

const dummyArray = new Array(8).fill(0)

const ProjetosScreen: React.FC<ProjetosScreenProps> = () => {
  const route: any = useRoute()
  const client = useApolloClient()
  const usuarioAtualData = client.readQuery<UsuarioAtualQuery>({
    query: usuarioAtual
  })

  //const { data, loading, error } = mockProjetosPendentesQuery()

  const { data, loading, error } = useProjetosPendentesQuery({
    pollInterval: 5000,
    variables: {
      idPessoa: usuarioAtualData!.usuarioAtual.idPessoa,
      idVinculoPessoa: usuarioAtualData!.usuarioAtual.idVinculoPessoa
    }
  })

  if (loading)
    return (
      <ScreenContainer>
        <BackButton />
        <ScreenTitle
          title="Projetos"
          subtitle="Os projetos estão disponibilizados na ordem de apresentação."
          style={{ paddingTop: 14 }}
        />
        {dummyArray.map((_, index) => (
          <RoundedCard key={index} placeholder />
        ))}
      </ScreenContainer>
    )

  const isEmpty =
    data &&
    data.pessoa &&
    (data.pessoa.__typename == "Avaliador" ||
      data.pessoa.__typename == "Gerente") &&
    data.pessoa.projetosParaAvaliar &&
    data.pessoa.projetosParaAvaliar.length == 0

  if (
    !data ||
    data.pessoa === null ||
    error ||
    isEmpty ||
    (data.pessoa.__typename != "Avaliador" &&
      data.pessoa.__typename != "Gerente")
  )
    return (
      <ScreenContainer>
        <BackButton />
        <ScreenTitle
          title="Projetos"
          subtitle="Os projetos são disponibilizados na ordem de apresentação."
          style={{ paddingTop: 14 }}
        />
        <EmptyState />
      </ScreenContainer>
    )


  return (
    <ScreenContainer>
      <BackButton />
      <ScreenTitle
        title="Projetos"
        subtitle="Os projetos são disponibilizados na ordem de apresentação. Avalie-os:"
        style={{ paddingTop: 14 }}
      />

      {data && data.pessoa && data.pessoa.projetosParaAvaliar
        ? data!.pessoa.projetosParaAvaliar.map(({ idProjeto, mediaIndividual, tituloProjeto, categoria, apresentacao }) =>
            (categoria.idCategoria === route.params?.idCategoria) ?
              ((apresentacao?.idApresentacao === route.params?.idApresentacao) ?
                (<CardProjeto
                  key={idProjeto}
                  idProjeto={idProjeto}
                  tituloProjeto={tituloProjeto}
                  media={mediaIndividual && mediaIndividual.mediaProjeto}
                  coordenadorProjeto="Indisponível" />
                ) : null
              ) : null
        ) : false
      }
    </ScreenContainer>
  )
}

export default ProjetosScreen
