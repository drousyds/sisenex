import * as React from "react"
import { RoundedCard } from "components/Cards"
import { material } from "react-native-typography"
import { Text, View, StyleSheet } from "react-native"
import PrimaryButton from "components/Button/Button"
import Separator from "components/Separator/Separator"
import { Projeto } from "generated/graphql"
import {
  CardTitle,
  CardFooter,
  CardBody,
  CardFooterContent
} from "components/Cards/RoundedCard"
import getScoreColor from "helpers/getScoreColor"
import { useDispatch } from "react-redux"
import { navigate } from "redux/Navigation/actions"
import Typography from "assets/css/typography"

interface CardProjetoProps {
  idProjeto: Projeto["idProjeto"]
  tituloProjeto: Projeto["tituloProjeto"]
  coordenadorProjeto: string
  media?: number | null
}

const CardProjeto: React.SFC<CardProjetoProps> = ({
  idProjeto,
  tituloProjeto,
  media,
  coordenadorProjeto
}) => {
  const dispatch = useDispatch()
  const avaliarProjeto = () =>
    dispatch(
      navigate({
        routeName: "AvaliacaoProjeto",
        params: {
          projectId: idProjeto,
          podeAvaliar: true,
          tituloProjeto
        }
      })
    )

  return (
    <React.Fragment key={idProjeto}>
      <RoundedCard>
        <CardTitle fullText>{tituloProjeto}</CardTitle>
        <CardBody>{`Coordenador(a): ${coordenadorProjeto}`}</CardBody>
        {media ? (
          <CardFooter responsive>
            <React.Fragment>
              <CardFooterContent style={{ alignItems: "baseline" }}>
                <Text style={[material.headline, { fontSize: 18 }]}>
                  Média{" "}
                </Text>
                <Text
                  style={[
                    Typography.mediaAvaliacao,
                    {
                      paddingLeft: 6,
                      color: getScoreColor(media)
                    }
                  ]}
                >
                  {media.toFixed(2)}
                </Text>
              </CardFooterContent>
              <View>
                <PrimaryButton title="Reavaliar" onPress={avaliarProjeto} />
              </View>
            </React.Fragment>
          </CardFooter>
        ) : (
            <CardFooter style={{ alignItems: "center" }}>
              <PrimaryButton
                large
                title="Iniciar Avaliação"
                onPress={avaliarProjeto}
              />
            </CardFooter>
          )}
      </RoundedCard>
      <Separator vertical size={14} />
    </React.Fragment>
  )
}

const styles = StyleSheet.create({})

export default CardProjeto
