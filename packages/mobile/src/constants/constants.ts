import getApiUrl from "helpers/getApiUrl"

export const MONITOR = 6
export const PENDING = 2
export const BLOCKED = 4
export const REVIEWER = 5
export const FINISHED = 3
export const CLIENT_ID = "sisenex"

export const REACT_NATIVE_API_URL = getApiUrl()

export const REACT_NATIVE_API_PREFIX = "/api"
export const baseUrl = REACT_NATIVE_API_URL + REACT_NATIVE_API_PREFIX
