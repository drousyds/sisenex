import { UsuarioAtual } from "generated/graphql"
import AsyncStorage from "@react-native-async-storage/async-storage"

///////////////Usuario Atual////////////////////

export const loadUsuarioAtualCache = async () => {
  const usuarioAtualStr = await AsyncStorage.getItem("@sisenex:user:v3")
  if (!usuarioAtualStr) return null
  console.log(`Loading from cache (@sisenex:user:v3): ${usuarioAtualStr}`)
  const usuarioAtual: UsuarioAtual = JSON.parse(usuarioAtualStr)
  return usuarioAtual
}

export const saveUsuarioAtualCache = async (usuario: UsuarioAtual) => {
  console.log(`Saving as @sisenex:user:v3: ${JSON.stringify(usuario, null, 2)}`)
  await AsyncStorage.setItem("@sisenex:user:v3", JSON.stringify(usuario))
}

export const clearUsuarioAtualCache = async () => {
  await AsyncStorage.removeItem("@sisenex:user:v3")
}

///////////////Termo compromisso/////////////////

export const loadAssinouTermoCache = async () => {
  const assinouTermoStr = await AsyncStorage.getItem("@sisenex:assinouTermo:v2")
  if (!assinouTermoStr) return false
  console.log(
    `Loading from cache (@sisenex:assinouTermo:v2): ${assinouTermoStr}`
  )
  return true
}

export const saveAssinouTermoCache = async () => {
  console.log(`Saving as @sisenex:assinouTermo:v2: ${JSON.stringify(true)}`)
  await AsyncStorage.setItem("@sisenex:assinouTermo:v2", JSON.stringify(true))
}

export const clearAssinouTermoCache = async () => {
  console.log(`Clearing @sisenex:assinouTermo:v2: ${JSON.stringify(true)}`)
  await AsyncStorage.removeItem("@sisenex:assinouTermo:v2")
}
