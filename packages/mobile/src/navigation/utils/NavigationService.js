import React from "react"

export const navigatorRef = React.createRef();

export const navigate = (routeName, params) => {
  navigatorRef.current?.navigate(routeName, params);
}

export default {
  navigate,
  navigatorRef
}