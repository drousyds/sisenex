import React from "react"
import { LoginScreen } from "views/Login"
import { MonitorRoutes } from "./monitor/MonitorRoutes"
import { createStackNavigator } from "@react-navigation/stack"
import { TermoDeUsoScreen } from "views/TermoDeUso"
import {
  NavigationContainer,
  NavigationContainerProps,
  NavigationContainerRef
} from "@react-navigation/native"
import { navigatorRef } from "navigation/utils/NavigationService"
import ReviewerRoutes from "./reviewer/ReviewerRoutes"
import { useSelector } from "react-redux"
import {
  getAssinouTermo,
  getLoggedIn,
  getAvaliador
} from "redux/Auth/selectors"
import AvisosLegaisScreen from "views/AvisosLegais/AvisosLegaisScreen"
import SolicitacaoLGPDScreen from "views/SolicitacaoLGPD/SolicitacaoLGPDScreen"

type RootNavigatorProps = Omit<
  NavigationContainerProps & {
    onReady?: (() => void) | undefined
  } & React.RefAttributes<NavigationContainerRef>,
  "children"
>

interface TermoRouteProps {}

const Stack = createStackNavigator()

const TermoStack: React.FC<TermoRouteProps> = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="TermoDeUso" component={TermoDeUsoScreen} />
      <Stack.Screen name="AvisosLegais" component={AvisosLegaisScreen} />
      <Stack.Screen name="SolicitacaoLGPD" component={SolicitacaoLGPDScreen} />
    </Stack.Navigator>
  )
}

export const RootNavigator: React.FC<RootNavigatorProps> = props => {
  const loggedIn = useSelector(getLoggedIn)
  const avaliador = useSelector(getAvaliador)
  const termo = useSelector(getAssinouTermo)

  return (
    <NavigationContainer ref={navigatorRef} {...props}>
      {loggedIn ? (
        termo ? (
          avaliador ? (
            <ReviewerRoutes />
          ) : (
            <MonitorRoutes />
          )
        ) : (
          <TermoStack />
        )
      ) : (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
        </Stack.Navigator>
      )}
    </NavigationContainer>
  )
}
