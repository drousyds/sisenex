import React from "react"
import { Dimensions } from "react-native"
import Colors from "assets/css/colors"
import { IdentidadeScreen } from "views/Identidade"
import { ProjetosScreen } from "views/Projetos"
import { ApresentacoesScreen } from "views/Apresentacoes"
import { AvaliacoesScreen } from "views/Avaliacoes"
import { AvaliacaoProjetoScreen } from "views/AvaliacaoProjeto"
import AvisosLegaisScreen from "views/AvisosLegais/AvisosLegaisScreen"
import SolicitacaoLGPDScreen from "views/SolicitacaoLGPD/SolicitacaoLGPDScreen"
import { createStackNavigator } from "@react-navigation/stack"
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs"
import ReportProblemas from "views/ReportProblemas/ReportProblemasScreen"

const Stack = createStackNavigator()
const Tabs = createMaterialTopTabNavigator()
const scale = Dimensions.get("window").scale

const selectFontSize = (windowScale: number) => {
  if (windowScale < 3) {
    return 13
  } else {
    return 12
  }
}

interface ReviewerRoutesProps {}

function IdentidadeStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="IdentidadeScreen" component={IdentidadeScreen} />
    </Stack.Navigator>
  )
}

function ProjetosStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="ProjetosScreen" component={ProjetosScreen} />
    </Stack.Navigator>
  )
}

function ApresentacoesStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="AvaliacoesScreen" component={ApresentacoesScreen} />
    </Stack.Navigator>
  )
}

function AvaliacoesStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="AvaliacoesScreen" component={AvaliacoesScreen} />
    </Stack.Navigator>
  )
}

function ReviewerTabs() {
  return (
    <Tabs.Navigator
      initialRouteName="Identidade"
      screenOptions={{
        swipeEnabled: false,
        lazy: false,
        tabBarActiveTintColor: "black",
        tabBarInactiveTintColor: "black",
        tabBarShowIcon: false,
        tabBarLabelStyle: {
          fontSize: selectFontSize(scale), //selectFontSize(windowHeight),
          paddingHorizontal: 0,
          marginHorizontal: 0,
          alignSelf: "center"
        },
        tabBarIndicatorStyle: {
          height: 6,
          backgroundColor: Colors.primary
        },
        tabBarStyle: {
          backgroundColor: "white"
        }
      }}
      tabBarPosition="bottom"
    >
      <Tabs.Screen
        name="Identidade"
        component={IdentidadeStack}
        options={{
          tabBarLabel: "INÍCIO"
        }}
      />
      <Tabs.Screen
        name="Apresentacoes"
        component={ApresentacoesStack}
        options={{
          tabBarLabel: "AVALIAÇÕES"
        }}
      />
      <Tabs.Screen
        name="Avaliacoes"
        component={AvaliacoesStack}
        options={{
          tabBarLabel: "HISTÓRICO"
        }}
      />
    </Tabs.Navigator>
  )
}

function ReviewerStack() {
  return (
    <Stack.Navigator
      initialRouteName="ReviewerTabs"
      screenOptions={{ headerShown: false }}
    >
      <Stack.Screen name="ReviewerTabs" component={ReviewerTabs} />
      <Stack.Screen
        name="AvaliacaoProjeto"
        component={AvaliacaoProjetoScreen}
      />
      <Stack.Screen name="ProjectsScreen" component={ProjetosStack} />
      <Stack.Screen name="ReportProblemas" component={ReportProblemas} />
      <Stack.Screen name="AvisosLegais" component={AvisosLegaisScreen} />
      <Stack.Screen name="SolicitacaoLGPD" component={SolicitacaoLGPDScreen} />
    </Stack.Navigator>
  )
}

const ReviewerRoutes: React.FC<ReviewerRoutesProps> = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="ReviewerStack" component={ReviewerStack} />
    </Stack.Navigator>
  )
}
export default ReviewerRoutes
