import React from "react"
import Colors from "assets/css/colors"
import {
  RegistroApresentacaoScreen,
  RegistroApresentacaoInputScreen
} from "views/RegistroApresentacao"
import { GerenciaProjetoScreen } from "views/GerenciaProjeto"
import { PessoasApresentacaoScreen } from "views/PessoasApresentacao"
import { ProjetosApresentacaoScreen } from "views/ProjetosApresentacao"
import { ApresentacaoScreen } from "views/Apresentacao"
import RegistroPessoaScreen from "views/RegistroPessoa/RegistroPessoaScreen"
import ReportProblemas from "views/ReportProblemas/ReportProblemasScreen"
import RegistroPessoaInputScreen from "views/RegistroPessoa/RegistroPessoaInputScreen"
import AvisosLegaisScreen from "views/AvisosLegais/AvisosLegaisScreen"
import SolicitacaoLGPDScreen from "views/SolicitacaoLGPD/SolicitacaoLGPDScreen"
import { createStackNavigator } from "@react-navigation/stack"
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs"

const Stack = createStackNavigator()
const Tabs = createMaterialTopTabNavigator()

interface MonitorRoutesProps {}

function PessoasApresentacaoStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen
        name="PessoasScreen"
        component={PessoasApresentacaoScreen}
      />
    </Stack.Navigator>
  )
}

function ApresentacaoStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="ApresentacaoScreen" component={ApresentacaoScreen} />
    </Stack.Navigator>
  )
}

function ProjetosApresentacaoStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen
        name="ProjetosApresentacao"
        component={ProjetosApresentacaoScreen}
      />
    </Stack.Navigator>
  )
}

function RegistroApresentacaoStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen
        name="RegistroApresentacao"
        component={RegistroApresentacaoScreen}
      />
      <Stack.Screen
        name="RegistroApresentacaoInput"
        component={RegistroApresentacaoInputScreen}
      />
    </Stack.Navigator>
  )
}

function MonitorTabs() {
  return (
    <Tabs.Navigator
      initialRouteName={"PessoasApresentacao"}
      tabBarPosition="bottom"
      screenOptions={{
        lazy: false,
        swipeEnabled: false,
        tabBarActiveTintColor: "black",
        tabBarInactiveTintColor: "black",
        tabBarShowIcon: false,
        tabBarLabelStyle: {
          fontSize: 12,
          paddingHorizontal: 0,
          marginHorizontal: 0,
          alignSelf: "center"
        },
        tabBarIndicatorStyle: {
          height: 6,
          backgroundColor: Colors.primary
        },
        tabBarStyle: {
          backgroundColor: "white"
        }
      }}
    >
      <Tabs.Screen
        name="PessoasApresentacao"
        component={PessoasApresentacaoStack}
        options={{
          tabBarLabel: "PESSOAS"
        }}
      />
      <Tabs.Screen
        name="ProjetosApresentacao"
        component={ProjetosApresentacaoStack}
        options={{
          tabBarLabel: "PROJETOS"
        }}
      />
      <Tabs.Screen
        name="Apresentacao"
        component={ApresentacaoStack}
        options={{
          tabBarLabel: "APRESENTAÇÃO"
        }}
      />
    </Tabs.Navigator>
  )
}

function MonitorStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="MonitorTabs" component={MonitorTabs} />
      <Stack.Screen name="GerenciaProjeto" component={GerenciaProjetoScreen} />
      <Stack.Screen name="ReportProblemas" component={ReportProblemas} />
      <Stack.Screen name="RegistroPessoa" component={RegistroPessoaScreen} />
      <Stack.Screen name="AvisosLegais" component={AvisosLegaisScreen} />
      <Stack.Screen name="SolicitacaoLGPD" component={SolicitacaoLGPDScreen} />
      <Stack.Screen
        name="RegistroPessoaInput"
        component={RegistroPessoaInputScreen}
      />
    </Stack.Navigator>
  )
}

export const MonitorRoutes: React.FC<MonitorRoutesProps> = () => {
  return (
    <Stack.Navigator
      initialRouteName={"RegistroApresentacaoStack"}
      screenOptions={{ headerShown: false }}
    >
      <Stack.Screen name="MonitorStack" component={MonitorStack} />
      <Stack.Screen
        name="RegistroApresentacaoStack"
        component={RegistroApresentacaoStack}
      />
    </Stack.Navigator>
  )
}

export default MonitorRoutes
