import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as React from 'react';
import * as ApolloReactComponents from '@apollo/react-components';
import * as ApolloReactHoc from '@apollo/react-hoc';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T | null;
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  Upload: any,
};


export type Administrador = Pessoa & {
   __typename?: 'Administrador',
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  matriculaPessoa?: Maybe<Scalars['String']>,
  nomePessoa?: Maybe<Scalars['String']>,
  nomeSocialPessoa: Scalars['String'],
  lotacaoPessoa?: Maybe<Scalars['String']>,
  emailPessoa?: Maybe<Scalars['String']>,
  telefonePessoa?: Maybe<Scalars['String']>,
  aptidaoPessoa: Scalars['Int'],
  avaliadorPessoa?: Maybe<Scalars['Int']>,
  monitorPessoa?: Maybe<Scalars['Int']>,
  gerentePessoa?: Maybe<Scalars['Int']>,
  administradorPessoa?: Maybe<Scalars['Int']>,
  avatarUrl?: Maybe<Scalars['String']>,
  projetosParaAvaliar?: Maybe<Array<Projeto>>,
  apresentacoesAlocacao?: Maybe<Array<Apresentacao>>,
  avaliacoes?: Maybe<Array<Avaliacao>>,
  categoria?: Maybe<Categoria>,
  created_at?: Maybe<Scalars['String']>,
};


export type AdministradorAvaliacoesArgs = {
  idProjeto?: Maybe<Scalars['Int']>
};

export type AlocarProjetoInput = {
  eventoHorarioInicial: Scalars['String'],
  eventoHorarioFinal: Scalars['String'],
  numeroDeSalas: Scalars['Int'],
  numeroProjetosSala: Scalars['Int'],
  horarios?: Maybe<Array<Scalars['String']>>,
  idEvento: Scalars['Int'],
  idAreaTematica: Scalars['Int'],
};

export type AlocarProjetoVideoInput = {
  horarioInicial: Scalars['String'],
  numeroProjetosApresentacao: Scalars['Int'],
  idAreaTematica: Scalars['Int'],
};

export type Apresentacao = {
   __typename?: 'Apresentacao',
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String'],
  salaApresentacao?: Maybe<Scalars['String']>,
  horaApresentacao?: Maybe<Scalars['String']>,
  dataInicioApresentacao?: Maybe<Scalars['String']>,
  dataFimApresentacao?: Maybe<Scalars['String']>,
  disponibilidadeApresentacao?: Maybe<Scalars['Int']>,
  codigoApresentacao: Scalars['String'],
  modalidadeApresentacao?: Maybe<Scalars['String']>,
  projetos?: Maybe<Array<Projeto>>,
  avaliadores?: Maybe<Array<Avaliador>>,
  areaTematica?: Maybe<AreaTematica>,
  evento?: Maybe<Evento>,
  categoria: Categoria,
  linkApresentacao?: Maybe<Scalars['String']>,
};

export type ApresentacaoAtual = {
   __typename?: 'ApresentacaoAtual',
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String'],
  nomeAreaTematica: Scalars['String'],
  horaApresentacao: Scalars['String'],
  salaApresentacao: Scalars['String'],
  codigoApresentacao: Scalars['String'],
};

export type ApresentacaoInput = {
  idApresentacao?: Maybe<Scalars['Int']>,
  id2Apresentacao?: Maybe<Scalars['String']>,
  codigoApresentacao?: Maybe<Scalars['String']>,
};

export type ApresentacoesInput = {
  idEvento?: Maybe<Scalars['Int']>,
  idAreaTematica?: Maybe<Scalars['Int']>,
  modalidadeApresentacao?: Maybe<Scalars['String']>,
};

export type AreaTematica = {
   __typename?: 'AreaTematica',
  idAreaTematica: Scalars['Int'],
  nomeAreaTematica: Scalars['String'],
  projetos?: Maybe<Array<Projeto>>,
};


export type AreaTematicaProjetosArgs = {
  idEvento?: Maybe<Scalars['Int']>
};

export type AtualizarApresentacaoInput = {
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String'],
  salaApresentacao?: Maybe<Scalars['String']>,
  horaApresentacao?: Maybe<Scalars['String']>,
  dataInicioApresentacao?: Maybe<Scalars['String']>,
  dataFimApresentacao?: Maybe<Scalars['String']>,
  disponibilidadeApresentacao?: Maybe<Scalars['Int']>,
  modalidadeApresentacao?: Maybe<Scalars['String']>,
  idAreaTematica?: Maybe<Scalars['Int']>,
  idEvento?: Maybe<Scalars['Int']>,
  idCategoria?: Maybe<Scalars['Int']>,
  linkApresentacao?: Maybe<Scalars['String']>,
};

export type AtualizarAreaTematica = {
  idAreaTematica: Scalars['Int'],
  nomeAreaTematica: Scalars['String'],
};

export type AtualizarCampusInput = {
  idCampus: Scalars['Int'],
  nomeCampus?: Maybe<Scalars['String']>,
};

export type AtualizarEventoInput = {
  idEvento: Scalars['Int'],
  nomeEvento?: Maybe<Scalars['String']>,
  areasTematicas?: Maybe<Array<Scalars['Int']>>,
  descricaoEvento?: Maybe<Scalars['String']>,
  dataInicioEvento?: Maybe<Scalars['String']>,
  dataFimEvento?: Maybe<Scalars['String']>,
  statusEvento?: Maybe<Scalars['String']>,
};

export type AtualizarPessoaInput = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  matriculaPessoa?: Maybe<Scalars['String']>,
  nomePessoa?: Maybe<Scalars['String']>,
  nomeSocialPessoa?: Maybe<Scalars['String']>,
  lotacaoPessoa?: Maybe<Scalars['String']>,
  emailPessoa?: Maybe<Scalars['String']>,
  telefonePessoa?: Maybe<Scalars['String']>,
  aptidaoPessoa?: Maybe<Scalars['Int']>,
  avaliadorPessoa?: Maybe<Scalars['Int']>,
  monitorPessoa?: Maybe<Scalars['Int']>,
  gerentePessoa?: Maybe<Scalars['Int']>,
  administradorPessoa?: Maybe<Scalars['Int']>,
  avatarUrl?: Maybe<Scalars['String']>,
  idCategoria?: Maybe<Scalars['Int']>,
};

export type AtualizarProjetoInput = {
  idProjeto: Scalars['Int'],
  tituloProjeto?: Maybe<Scalars['String']>,
  codigoProjeto?: Maybe<Scalars['String']>,
  descricaoProjeto?: Maybe<Scalars['String']>,
  dataInicioProjeto?: Maybe<Scalars['String']>,
  dataFimProjeto?: Maybe<Scalars['String']>,
  anoProjeto?: Maybe<Scalars['String']>,
  idAreaTematica?: Maybe<Scalars['Int']>,
  modalidadeProjeto?: Maybe<Scalars['String']>,
  idUnidade?: Maybe<Scalars['Int']>,
  disponibilidadeProjeto?: Maybe<Scalars['Int']>,
  liberacaoProjeto?: Maybe<Scalars['String']>,
  idApresentacao?: Maybe<Scalars['Int']>,
  id2Apresentacao?: Maybe<Scalars['String']>,
  idEvento?: Maybe<Scalars['Int']>,
  idCategoria?: Maybe<Scalars['Int']>,
  linkArtefato?: Maybe<Scalars['String']>,
};

export type AtualizarSolicitacaoLgpdInput = {
  idSolicitacao: Scalars['Int'],
  tipoSolicitacao?: Maybe<Scalars['Int']>,
  textoSolicitacao?: Maybe<Scalars['String']>,
  idPessoa?: Maybe<Scalars['String']>,
  idVinculoPessoa?: Maybe<Scalars['String']>,
  nomeSolicitacao?: Maybe<Scalars['String']>,
  sobrenomeSolicitacao?: Maybe<Scalars['String']>,
  emailPessoa?: Maybe<Scalars['String']>,
  statusSolicitacao?: Maybe<Scalars['String']>,
};

export type AtualizarStatusReportInput = {
  idReport: Scalars['Int'],
  statusReport: Scalars['String'],
};

export type AtualizarUnidadeInput = {
  idUnidade: Scalars['Int'],
  codigoUnidade?: Maybe<Scalars['String']>,
  nomeUnidade?: Maybe<Scalars['String']>,
  siglaUnidade?: Maybe<Scalars['String']>,
  hierarquiaUnidade?: Maybe<Scalars['String']>,
  unidadeGestora?: Maybe<Scalars['Int']>,
  tipoUnidade?: Maybe<Scalars['String']>,
  idCampus?: Maybe<Scalars['Int']>,
};

export type Avaliacao = {
   __typename?: 'Avaliacao',
  media: Scalars['Float'],
  notas: Array<Maybe<NotaPergunta>>,
  projeto: Projeto,
  avaliador: Pessoa,
  avaliacaoManual?: Maybe<Scalars['Int']>,
  nomeAvaliadorPapel?: Maybe<Scalars['String']>,
};

export type AvaliacaoInput = {
  notas: Array<Maybe<NotaPerguntaInput>>,
  idProjeto: Scalars['Int'],
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
};

export type Avaliador = Pessoa & {
   __typename?: 'Avaliador',
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  matriculaPessoa?: Maybe<Scalars['String']>,
  nomePessoa?: Maybe<Scalars['String']>,
  nomeSocialPessoa: Scalars['String'],
  lotacaoPessoa?: Maybe<Scalars['String']>,
  emailPessoa?: Maybe<Scalars['String']>,
  telefonePessoa?: Maybe<Scalars['String']>,
  aptidaoPessoa: Scalars['Int'],
  avaliadorPessoa?: Maybe<Scalars['Int']>,
  monitorPessoa?: Maybe<Scalars['Int']>,
  gerentePessoa?: Maybe<Scalars['Int']>,
  administradorPessoa?: Maybe<Scalars['Int']>,
  avatarUrl?: Maybe<Scalars['String']>,
  projetosParaAvaliar?: Maybe<Array<Projeto>>,
  apresentacoesAlocacao?: Maybe<Array<Apresentacao>>,
  avaliacoes?: Maybe<Array<Avaliacao>>,
  categoria?: Maybe<Categoria>,
  created_at?: Maybe<Scalars['String']>,
};


export type AvaliadorAvaliacoesArgs = {
  idProjeto?: Maybe<Scalars['Int']>
};

export type AvaliarProjetoInput = {
  avaliacao: AvaliacaoInput,
};

export type AvaliarProjetoPorUmAvaliadorInput = {
  idPessoaGerente: Scalars['String'],
  idVinculoPessoaGerente: Scalars['String'],
  avaliacao: AvaliacaoInput,
  nomeAvaliadorPapel: Scalars['String'],
};

export type BaterAvaliadorInput = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String'],
};

export enum CacheControlScope {
  Public = 'PUBLIC',
  Private = 'PRIVATE'
}

export type Campus = {
   __typename?: 'Campus',
  idCampus: Scalars['Int'],
  nomeCampus?: Maybe<Scalars['String']>,
  unidades?: Maybe<Array<Unidade>>,
};

export type Categoria = {
   __typename?: 'Categoria',
  idCategoria: Scalars['Int'],
  nomeCategoria: Scalars['String'],
  perguntas?: Maybe<Array<Pergunta>>,
};

export type CriarApresentacaoInput = {
  id2Apresentacao: Scalars['String'],
  salaApresentacao: Scalars['String'],
  horaApresentacao: Scalars['String'],
  dataInicioApresentacao?: Maybe<Scalars['String']>,
  dataFimApresentacao?: Maybe<Scalars['String']>,
  disponibilidadeApresentacao?: Maybe<Scalars['Int']>,
  modalidadeApresentacao?: Maybe<Scalars['String']>,
  idAreaTematica?: Maybe<Scalars['Int']>,
  idEvento?: Maybe<Scalars['Int']>,
  idCategoria: Scalars['Int'],
  linkApresentacao?: Maybe<Scalars['String']>,
};

export type CriarAreaTematica = {
  idAreaTematica: Scalars['Int'],
  nomeAreaTematica: Scalars['String'],
};

export type CriarCampusInput = {
  idCampus: Scalars['Int'],
  nomeCampus: Scalars['String'],
};

export type CriarEventoInput = {
  nomeEvento: Scalars['String'],
  campi: Array<Scalars['Int']>,
  areasTematicas?: Maybe<Array<Scalars['Int']>>,
  descricaoEvento?: Maybe<Scalars['String']>,
  dataInicioEvento: Scalars['String'],
  dataFimEvento: Scalars['String'],
  statusEvento: Scalars['String'],
};

export type CriarPerguntaInput = {
  idPergunta: Scalars['Int'],
  idCategoria: Scalars['Int'],
  conteudoPergunta: Scalars['String'],
  dataPergunta?: Maybe<Scalars['String']>,
};

export type CriarPessoaInput = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  matriculaPessoa?: Maybe<Scalars['String']>,
  nomePessoa?: Maybe<Scalars['String']>,
  nomeSocialPessoa: Scalars['String'],
  lotacaoPessoa?: Maybe<Scalars['String']>,
  emailPessoa?: Maybe<Scalars['String']>,
  telefonePessoa?: Maybe<Scalars['String']>,
  aptidaoPessoa?: Maybe<Scalars['Int']>,
  avaliadorPessoa?: Maybe<Scalars['Int']>,
  monitorPessoa?: Maybe<Scalars['Int']>,
  gerentePessoa?: Maybe<Scalars['Int']>,
  administradorPessoa?: Maybe<Scalars['Int']>,
  avatarUrl?: Maybe<Scalars['String']>,
  idCategoria?: Maybe<Scalars['Int']>,
};

export type CriarProjetoInput = {
  idProjeto: Scalars['Int'],
  tituloProjeto: Scalars['String'],
  descricaoProjeto: Scalars['String'],
  dataInicioProjeto: Scalars['String'],
  dataFimProjeto: Scalars['String'],
  anoProjeto: Scalars['String'],
  idAreaTematica: Scalars['Int'],
  idUnidade: Scalars['Int'],
  disponibilidadeProjeto?: Maybe<Scalars['Int']>,
  codigoProjeto?: Maybe<Scalars['String']>,
  modalidadeProjeto?: Maybe<Scalars['String']>,
  liberacaoProjeto?: Maybe<Scalars['String']>,
  idApresentacao?: Maybe<Scalars['Int']>,
  id2Apresentacao?: Maybe<Scalars['String']>,
  idCategoria: Scalars['Int'],
  linkArtefato?: Maybe<Scalars['String']>,
};

export type CriarReportInput = {
  conteudoReport: Scalars['String'],
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  emailReport: Scalars['String'],
  idApresentacao?: Maybe<Scalars['Int']>,
  id2Apresentacao?: Maybe<Scalars['String']>,
};

export type CriarSolicitacaoLgpdInput = {
  tipoSolicitacao: Scalars['Int'],
  textoSolicitacao?: Maybe<Scalars['String']>,
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  nomeSolicitacao: Scalars['String'],
  sobrenomeSolicitacao: Scalars['String'],
  emailPessoa: Scalars['String'],
};

export type CriarUnidadeInput = {
  idUnidade: Scalars['Int'],
  codigoUnidade: Scalars['String'],
  nomeUnidade: Scalars['String'],
  siglaUnidade: Scalars['String'],
  hierarquiaUnidade: Scalars['String'],
  unidadeGestora: Scalars['Int'],
  tipoUnidade: Scalars['String'],
  idCampus: Scalars['Int'],
};

export type EnviarEmailParaAvaliadoresInput = {
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String'],
};

export type Evento = {
   __typename?: 'Evento',
  idEvento: Scalars['Int'],
  nomeEvento: Scalars['String'],
  campi: Array<Campus>,
  descricaoEvento?: Maybe<Scalars['String']>,
  dataInicioEvento?: Maybe<Scalars['String']>,
  dataFimEvento?: Maybe<Scalars['String']>,
  areasTematicas: Array<AreaTematica>,
  apresentacoes?: Maybe<Array<Apresentacao>>,
  projetos?: Maybe<Array<Projeto>>,
  statusEvento: Scalars['String'],
};


export type EventoApresentacoesArgs = {
  idAreaTematica?: Maybe<Scalars['Int']>,
  modalidadeApresentacao?: Maybe<Scalars['String']>
};

export type ExcluirApresentacaoInput = {
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String'],
};

export type ExcluirCampusInput = {
  idCampus: Scalars['Int'],
};

export type ExcluirPessoaInput = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
};

export type ExcluirReportInput = {
  idReport: Scalars['Int'],
};

export type ExcluirUnidadeInput = {
  idUnidade: Scalars['Int'],
};

export type FecharApresentacaoInput = {
  codigoApresentacao: Scalars['String'],
};

export type FecharProjetoInput = {
  idProjeto: Scalars['Int'],
};

export type FecharProjetoParaUmAvaliadorInput = {
  idProjeto: Scalars['Int'],
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
};

export type FinalizarSolicitacoesLgpdInput = {
  idSolicitacao: Scalars['Int'],
};

export type Gerente = Pessoa & {
   __typename?: 'Gerente',
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  matriculaPessoa?: Maybe<Scalars['String']>,
  nomePessoa?: Maybe<Scalars['String']>,
  nomeSocialPessoa: Scalars['String'],
  lotacaoPessoa?: Maybe<Scalars['String']>,
  emailPessoa?: Maybe<Scalars['String']>,
  telefonePessoa?: Maybe<Scalars['String']>,
  aptidaoPessoa: Scalars['Int'],
  avaliadorPessoa?: Maybe<Scalars['Int']>,
  monitorPessoa?: Maybe<Scalars['Int']>,
  gerentePessoa?: Maybe<Scalars['Int']>,
  administradorPessoa?: Maybe<Scalars['Int']>,
  avatarUrl?: Maybe<Scalars['String']>,
  projetosParaAvaliar?: Maybe<Array<Projeto>>,
  apresentacoesAlocacao?: Maybe<Array<Apresentacao>>,
  avaliacoes?: Maybe<Array<Avaliacao>>,
  categoria?: Maybe<Categoria>,
  created_at?: Maybe<Scalars['String']>,
};


export type GerenteAvaliacoesArgs = {
  idProjeto?: Maybe<Scalars['Int']>
};

export type IniciarApresentacaoInput = {
  codigoApresentacao: Scalars['String'],
};

export type LiberarProjetoInput = {
  idProjeto: Scalars['Int'],
};

export type LiberarProjetoParaUmAvaliadorInput = {
  idProjeto: Scalars['Int'],
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
};

export type LoginInput = {
  digitoVerificador: Scalars['String'],
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  matriculaPessoa?: Maybe<Scalars['String']>,
  nomePessoa?: Maybe<Scalars['String']>,
  nomeSocialPessoa: Scalars['String'],
  lotacaoPessoa?: Maybe<Scalars['String']>,
  emailPessoa?: Maybe<Scalars['String']>,
  telefonePessoa?: Maybe<Scalars['String']>,
  aptidaoPessoa?: Maybe<Scalars['Int']>,
  avaliadorPessoa?: Maybe<Scalars['Int']>,
  monitorPessoa?: Maybe<Scalars['Int']>,
  gerentePessoa?: Maybe<Scalars['Int']>,
  administradorPessoa?: Maybe<Scalars['Int']>,
  avatarUrl?: Maybe<Scalars['String']>,
};

export type LoginOutput = {
   __typename?: 'loginOutput',
  token: Scalars['String'],
  pessoa: Pessoa,
};

export type MediaPorAvaliador = {
   __typename?: 'mediaPorAvaliador',
  avaliador?: Maybe<Pessoa>,
  mediaProjeto?: Maybe<Scalars['Float']>,
};

export type Membro = {
   __typename?: 'Membro',
  idProjeto: Scalars['Int'],
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  funcao: Scalars['String'],
  nomePessoa?: Maybe<Scalars['String']>,
};

export type Monitor = Pessoa & {
   __typename?: 'Monitor',
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  matriculaPessoa?: Maybe<Scalars['String']>,
  nomePessoa?: Maybe<Scalars['String']>,
  nomeSocialPessoa: Scalars['String'],
  lotacaoPessoa?: Maybe<Scalars['String']>,
  emailPessoa?: Maybe<Scalars['String']>,
  telefonePessoa?: Maybe<Scalars['String']>,
  aptidaoPessoa: Scalars['Int'],
  avaliadorPessoa?: Maybe<Scalars['Int']>,
  monitorPessoa?: Maybe<Scalars['Int']>,
  gerentePessoa?: Maybe<Scalars['Int']>,
  administradorPessoa?: Maybe<Scalars['Int']>,
  avatarUrl?: Maybe<Scalars['String']>,
  apresentacoesAlocacao?: Maybe<Array<Apresentacao>>,
  categoria?: Maybe<Categoria>,
  created_at?: Maybe<Scalars['String']>,
};

export type Mutation = {
   __typename?: 'Mutation',
  _dummy?: Maybe<Scalars['String']>,
  criarApresentacao?: Maybe<Apresentacao>,
  atualizarApresentacao?: Maybe<Apresentacao>,
  excluirApresentacao?: Maybe<Apresentacao>,
  enviarEmailParaAvaliadores?: Maybe<Scalars['Boolean']>,
  fecharApresentacoesPorArea?: Maybe<Scalars['Boolean']>,
  fecharApresentacoesPorEvento?: Maybe<Scalars['Boolean']>,
  liberarApresentacoesPorCategoria?: Maybe<Scalars['Boolean']>,
  iniciarApresentacao: Apresentacao,
  fecharApresentacao: Scalars['Boolean'],
  baterAvaliador: Avaliador,
  removerAvaliadorDaApresentacao: Scalars['Boolean'],
  login: LoginOutput,
  atualizarPessoa: Pessoa,
  excluirPessoa: Scalars['Boolean'],
  avaliarProjeto: Scalars['Boolean'],
  avaliarProjetoPorUmAvaliador: Scalars['Boolean'],
  liberarProjeto: Projeto,
  liberarProjetoParaUmAvaliador: Projeto,
  fecharProjeto: Projeto,
  fecharProjetoParaUmAvaliador: Projeto,
  removerProjetoDeApresentacao: Scalars['Boolean'],
  criarPessoa: Pessoa,
  alocarAvaliadoresVideo: Scalars['Boolean'],
  liberarAvaliadorProjetoPorCategoria: Scalars['Boolean'],
  criarProjeto?: Maybe<Projeto>,
  atualizarProjeto?: Maybe<Projeto>,
  atualizarListaDeProjetos?: Maybe<Scalars['Boolean']>,
  criarUnidade?: Maybe<Unidade>,
  atualizarUnidade?: Maybe<Unidade>,
  excluirUnidade?: Maybe<Scalars['Boolean']>,
  criarPergunta?: Maybe<Pergunta>,
  atualizarPergunta?: Maybe<Pergunta>,
  excluirPergunta?: Maybe<Scalars['Int']>,
  criarCategoria?: Maybe<Categoria>,
  atualizarCategoria?: Maybe<Categoria>,
  excluirCategoria?: Maybe<Scalars['Int']>,
  criarAreaTematica?: Maybe<AreaTematica>,
  atualizarAreaTematica?: Maybe<AreaTematica>,
  excluirAreaTematica?: Maybe<Scalars['Boolean']>,
  criarCampus?: Maybe<Campus>,
  atualizarCampus?: Maybe<Campus>,
  excluirCampus?: Maybe<Scalars['Boolean']>,
  criarEvento?: Maybe<Evento>,
  atualizarEvento?: Maybe<Evento>,
  excluirEvento?: Maybe<Evento>,
  alocarProjetos?: Maybe<Array<Apresentacao>>,
  alocarProjetosVideo?: Maybe<Array<Apresentacao>>,
  criarReport?: Maybe<Report>,
  excluirReport?: Maybe<Scalars['Boolean']>,
  atualizarStatusReport?: Maybe<Report>,
  autenticar: Token,
  criarSolicitacaoLGPD?: Maybe<Solicitacao>,
  finalizarSolicitacoesLGPD?: Maybe<Solicitacao>,
  atualizarSolicitacaoLGPD?: Maybe<Solicitacao>,
  updateNotaPergunta: Scalars['Boolean'],
};


export type MutationCriarApresentacaoArgs = {
  input: CriarApresentacaoInput
};


export type MutationAtualizarApresentacaoArgs = {
  input: AtualizarApresentacaoInput
};


export type MutationExcluirApresentacaoArgs = {
  input?: Maybe<ExcluirApresentacaoInput>
};


export type MutationEnviarEmailParaAvaliadoresArgs = {
  input?: Maybe<EnviarEmailParaAvaliadoresInput>
};


export type MutationFecharApresentacoesPorAreaArgs = {
  idAreaTematica: Scalars['Int']
};


export type MutationFecharApresentacoesPorEventoArgs = {
  idEvento: Scalars['Int']
};


export type MutationLiberarApresentacoesPorCategoriaArgs = {
  idCategoria: Scalars['Int']
};


export type MutationIniciarApresentacaoArgs = {
  input: IniciarApresentacaoInput
};


export type MutationFecharApresentacaoArgs = {
  input: FecharApresentacaoInput
};


export type MutationBaterAvaliadorArgs = {
  input: BaterAvaliadorInput
};


export type MutationRemoverAvaliadorDaApresentacaoArgs = {
  input: RemoverAvaliadorDaApresentacaoInput
};


export type MutationLoginArgs = {
  input: LoginInput
};


export type MutationAtualizarPessoaArgs = {
  input: AtualizarPessoaInput
};


export type MutationExcluirPessoaArgs = {
  input: ExcluirPessoaInput
};


export type MutationAvaliarProjetoArgs = {
  input: AvaliarProjetoInput
};


export type MutationAvaliarProjetoPorUmAvaliadorArgs = {
  input: AvaliarProjetoPorUmAvaliadorInput
};


export type MutationLiberarProjetoArgs = {
  input: LiberarProjetoInput
};


export type MutationLiberarProjetoParaUmAvaliadorArgs = {
  input: LiberarProjetoParaUmAvaliadorInput
};


export type MutationFecharProjetoArgs = {
  input: FecharProjetoInput
};


export type MutationFecharProjetoParaUmAvaliadorArgs = {
  input: FecharProjetoParaUmAvaliadorInput
};


export type MutationRemoverProjetoDeApresentacaoArgs = {
  input: RemoverProjetoDeApresentacaoInput
};


export type MutationCriarPessoaArgs = {
  input: CriarPessoaInput
};


export type MutationLiberarAvaliadorProjetoPorCategoriaArgs = {
  idCategoria: Scalars['Int']
};


export type MutationCriarProjetoArgs = {
  input: CriarProjetoInput
};


export type MutationAtualizarProjetoArgs = {
  input: AtualizarProjetoInput
};


export type MutationCriarUnidadeArgs = {
  input: CriarUnidadeInput
};


export type MutationAtualizarUnidadeArgs = {
  input: AtualizarUnidadeInput
};


export type MutationExcluirUnidadeArgs = {
  input: ExcluirUnidadeInput
};


export type MutationCriarPerguntaArgs = {
  input: CriarPerguntaInput
};


export type MutationAtualizarPerguntaArgs = {
  idPergunta: Scalars['Int']
};


export type MutationCriarCategoriaArgs = {
  idCategoria: Scalars['Int']
};


export type MutationAtualizarCategoriaArgs = {
  idCategoria: Scalars['Int']
};


export type MutationCriarAreaTematicaArgs = {
  input: CriarAreaTematica
};


export type MutationAtualizarAreaTematicaArgs = {
  input: AtualizarAreaTematica
};


export type MutationExcluirAreaTematicaArgs = {
  idAreaTematica: Scalars['Int']
};


export type MutationCriarCampusArgs = {
  input: CriarCampusInput
};


export type MutationAtualizarCampusArgs = {
  input: AtualizarCampusInput
};


export type MutationExcluirCampusArgs = {
  input: ExcluirCampusInput
};


export type MutationCriarEventoArgs = {
  input: CriarEventoInput
};


export type MutationAtualizarEventoArgs = {
  input: AtualizarEventoInput
};


export type MutationExcluirEventoArgs = {
  idEvento: Scalars['Int']
};


export type MutationAlocarProjetosArgs = {
  input?: Maybe<AlocarProjetoInput>
};


export type MutationAlocarProjetosVideoArgs = {
  input?: Maybe<AlocarProjetoVideoInput>
};


export type MutationCriarReportArgs = {
  input: CriarReportInput
};


export type MutationExcluirReportArgs = {
  input: ExcluirReportInput
};


export type MutationAtualizarStatusReportArgs = {
  input: AtualizarStatusReportInput
};


export type MutationAutenticarArgs = {
  code: Scalars['String']
};


export type MutationCriarSolicitacaoLgpdArgs = {
  input: CriarSolicitacaoLgpdInput
};


export type MutationFinalizarSolicitacoesLgpdArgs = {
  input: FinalizarSolicitacoesLgpdInput
};


export type MutationAtualizarSolicitacaoLgpdArgs = {
  input: AtualizarSolicitacaoLgpdInput
};


export type MutationUpdateNotaPerguntaArgs = {
  idProjeto: Scalars['Int'],
  idPergunta: Scalars['Int'],
  nota: Scalars['Float']
};

export type NotaPergunta = {
   __typename?: 'NotaPergunta',
  pergunta: Pergunta,
  nota?: Maybe<Scalars['Float']>,
  respostaTextual?: Maybe<Scalars['String']>,
  data: Scalars['String'],
};

export type NotaPerguntaInput = {
  idPergunta: Scalars['Int'],
  nota?: Maybe<Scalars['Float']>,
  respostaTextual?: Maybe<Scalars['String']>,
};

export type Ouvinte = Pessoa & {
   __typename?: 'Ouvinte',
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  matriculaPessoa?: Maybe<Scalars['String']>,
  nomePessoa?: Maybe<Scalars['String']>,
  nomeSocialPessoa: Scalars['String'],
  lotacaoPessoa?: Maybe<Scalars['String']>,
  emailPessoa?: Maybe<Scalars['String']>,
  telefonePessoa?: Maybe<Scalars['String']>,
  aptidaoPessoa: Scalars['Int'],
  avaliadorPessoa?: Maybe<Scalars['Int']>,
  monitorPessoa?: Maybe<Scalars['Int']>,
  gerentePessoa?: Maybe<Scalars['Int']>,
  administradorPessoa?: Maybe<Scalars['Int']>,
  avatarUrl?: Maybe<Scalars['String']>,
  categoria?: Maybe<Categoria>,
  apresentacoesAlocacao?: Maybe<Array<Apresentacao>>,
  created_at?: Maybe<Scalars['String']>,
};

export type Pergunta = {
   __typename?: 'Pergunta',
  idPergunta: Scalars['Int'],
  conteudoPergunta: Scalars['String'],
  dataPergunta?: Maybe<Scalars['String']>,
  categoria: Categoria,
};

export type Pessoa = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  matriculaPessoa?: Maybe<Scalars['String']>,
  nomePessoa?: Maybe<Scalars['String']>,
  nomeSocialPessoa: Scalars['String'],
  lotacaoPessoa?: Maybe<Scalars['String']>,
  emailPessoa?: Maybe<Scalars['String']>,
  telefonePessoa?: Maybe<Scalars['String']>,
  aptidaoPessoa: Scalars['Int'],
  avaliadorPessoa?: Maybe<Scalars['Int']>,
  monitorPessoa?: Maybe<Scalars['Int']>,
  gerentePessoa?: Maybe<Scalars['Int']>,
  administradorPessoa?: Maybe<Scalars['Int']>,
  avatarUrl?: Maybe<Scalars['String']>,
  categoria?: Maybe<Categoria>,
  apresentacoesAlocacao?: Maybe<Array<Apresentacao>>,
  created_at?: Maybe<Scalars['String']>,
};

export type Projeto = {
   __typename?: 'Projeto',
  idProjeto: Scalars['Int'],
  tituloProjeto: Scalars['String'],
  descricaoProjeto: Scalars['String'],
  dataInicioProjeto: Scalars['String'],
  dataFimProjeto: Scalars['String'],
  anoProjeto: Scalars['String'],
  modalidadeProjeto: Scalars['String'],
  disponibilidadeProjeto: Scalars['Int'],
  liberacaoProjeto: Scalars['String'],
  areaTematica: AreaTematica,
  unidade?: Maybe<Unidade>,
  codigoProjeto?: Maybe<Scalars['String']>,
  apresentacao?: Maybe<Apresentacao>,
  avaliacoes?: Maybe<Array<Avaliacao>>,
  media?: Maybe<Scalars['Float']>,
  avaliadoresHabilitados?: Maybe<Array<Avaliador>>,
  mediaIndividual?: Maybe<MediaPorAvaliador>,
  mediasIndividuais?: Maybe<Array<MediaPorAvaliador>>,
  coordenador?: Maybe<Array<Membro>>,
  evento?: Maybe<Evento>,
  categoria: Categoria,
  linkArtefato?: Maybe<Scalars['String']>,
};


export type ProjetoMediaIndividualArgs = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String']
};

export type ProjetoAvaliadoInput = {
  idProjeto: Scalars['Int'],
};

export type ProjetoLiberadoInput = {
  idProjeto: Scalars['Int'],
};

export type ProjetosInput = {
  idCampus?: Maybe<Scalars['Int']>,
  idEvento?: Maybe<Scalars['Int']>,
  idAreaTematica?: Maybe<Scalars['Int']>,
};

export type Query = {
   __typename?: 'Query',
  _dummy?: Maybe<Scalars['String']>,
  apresentacao?: Maybe<Apresentacao>,
  apresentacoes?: Maybe<Array<Apresentacao>>,
  pessoa?: Maybe<Pessoa>,
  pessoas?: Maybe<Array<Pessoa>>,
  avaliadores?: Maybe<Array<Avaliador>>,
  monitores?: Maybe<Array<Monitor>>,
  gerentes?: Maybe<Array<Gerente>>,
  apresentacoesAvaliador?: Maybe<Array<Apresentacao>>,
  projeto?: Maybe<Projeto>,
  projetos?: Maybe<Array<Projeto>>,
  unidade?: Maybe<Unidade>,
  unidades?: Maybe<Array<Maybe<Unidade>>>,
  pergunta?: Maybe<Pergunta>,
  perguntas?: Maybe<Array<Pergunta>>,
  categoria?: Maybe<Categoria>,
  categorias?: Maybe<Array<Categoria>>,
  avaliacao?: Maybe<Avaliacao>,
  areaTematica?: Maybe<AreaTematica>,
  areasTematicas?: Maybe<Array<AreaTematica>>,
  campus?: Maybe<Campus>,
  campi?: Maybe<Array<Campus>>,
  evento?: Maybe<Evento>,
  eventos?: Maybe<Array<Evento>>,
  report?: Maybe<Report>,
  reports?: Maybe<Array<Report>>,
  solicitacaoLGPD?: Maybe<Solicitacao>,
  pessoaSolicitacoesLGPD?: Maybe<Array<Maybe<Solicitacao>>>,
  solicitacoesLGPD?: Maybe<Array<Solicitacao2>>,
  relatoriosAvaliadores?: Maybe<Array<RelatorioAvaliador>>,
  relatoriosMedias?: Maybe<Array<RelatorioMedia>>,
  relatoriosNotas?: Maybe<Array<RelatorioNotas>>,
  usuarioAtual: UsuarioAtual,
  apresentacaoAtual?: Maybe<ApresentacaoAtual>,
};


export type QueryApresentacaoArgs = {
  input: ApresentacaoInput
};


export type QueryApresentacoesArgs = {
  input?: Maybe<ApresentacoesInput>
};


export type QueryPessoaArgs = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String']
};


export type QueryApresentacoesAvaliadorArgs = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String']
};


export type QueryProjetoArgs = {
  idProjeto: Scalars['Int']
};


export type QueryProjetosArgs = {
  input?: Maybe<ProjetosInput>
};


export type QueryUnidadeArgs = {
  idUnidade?: Maybe<Scalars['Int']>
};


export type QueryPerguntaArgs = {
  idPergunta: Scalars['Int']
};


export type QueryCategoriaArgs = {
  idCategoria: Scalars['Int']
};


export type QueryAvaliacaoArgs = {
  idProjeto: Scalars['Int'],
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String']
};


export type QueryAreaTematicaArgs = {
  idAreaTematica: Scalars['Int']
};


export type QueryCampusArgs = {
  idCampus: Scalars['Int']
};


export type QueryEventoArgs = {
  idEvento: Scalars['Int']
};


export type QueryReportArgs = {
  idReport: Scalars['Int']
};


export type QuerySolicitacaoLgpdArgs = {
  idSolicitacao: Scalars['Int']
};


export type QueryPessoaSolicitacoesLgpdArgs = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String']
};

export type RelatorioAvaliador = {
   __typename?: 'RelatorioAvaliador',
  avaliador: Pessoa,
  avaliou: Scalars['String'],
};

export type RelatorioMedia = {
   __typename?: 'RelatorioMedia',
  projeto: Projeto,
  media: Scalars['Float'],
  numAvaliadores: Scalars['Int'],
};

export type RelatorioNotas = {
   __typename?: 'RelatorioNotas',
  avaliador: Pessoa,
  projeto: Projeto,
  nota: Scalars['Float'],
};

export type RemoverAvaliadorDaApresentacaoInput = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String'],
};

export type RemoverProjetoDeApresentacaoInput = {
  idProjeto: Scalars['Int'],
};

export type Report = {
   __typename?: 'Report',
  idReport: Scalars['Int'],
  conteudoReport: Scalars['String'],
  avaliador: Pessoa,
  apresentacao?: Maybe<Apresentacao>,
  dataCriacao?: Maybe<Scalars['String']>,
  statusReport: Scalars['String'],
  emailReport: Scalars['String'],
};

export type Solicitacao = {
   __typename?: 'Solicitacao',
  idSolicitacao: Scalars['Int'],
  tipoSolicitacao: Scalars['Int'],
  textoSolicitacao?: Maybe<Scalars['String']>,
  pessoa: Pessoa,
  nomeSolicitacao: Scalars['String'],
  sobrenomeSolicitacao: Scalars['String'],
  emailPessoa: Scalars['String'],
  statusSolicitacao?: Maybe<Scalars['String']>,
};

export type Solicitacao2 = {
   __typename?: 'Solicitacao2',
  idSolicitacao: Scalars['Int'],
  tipoSolicitacao: Scalars['Int'],
  textoSolicitacao?: Maybe<Scalars['String']>,
  pessoa?: Maybe<Pessoa>,
  nomeSolicitacao: Scalars['String'],
  sobrenomeSolicitacao: Scalars['String'],
  emailPessoa: Scalars['String'],
  statusSolicitacao?: Maybe<Scalars['String']>,
};

export type Subscription = {
   __typename?: 'Subscription',
  projetoLiberado: Projeto,
  projetoFechado: Projeto,
  projetoLiberadoParaUmAvaliador: Projeto,
  projetoFechadoParaUmAvaliador: Projeto,
  subBaterAvaliador: Avaliador,
  apresentacaoFechada: Apresentacao,
  projetoAvaliado: Avaliacao,
  avaliadorRemovidoDaApresentacao: Apresentacao,
};


export type SubscriptionProjetoLiberadoArgs = {
  input?: Maybe<LiberarProjetoInput>
};


export type SubscriptionProjetoFechadoArgs = {
  input?: Maybe<FecharProjetoInput>
};


export type SubscriptionProjetoLiberadoParaUmAvaliadorArgs = {
  input?: Maybe<LiberarProjetoParaUmAvaliadorInput>
};


export type SubscriptionProjetoFechadoParaUmAvaliadorArgs = {
  input?: Maybe<FecharProjetoParaUmAvaliadorInput>
};


export type SubscriptionSubBaterAvaliadorArgs = {
  input?: Maybe<BaterAvaliadorInput>
};


export type SubscriptionApresentacaoFechadaArgs = {
  input?: Maybe<FecharApresentacaoInput>
};


export type SubscriptionProjetoAvaliadoArgs = {
  input?: Maybe<ProjetoLiberadoInput>
};


export type SubscriptionAvaliadorRemovidoDaApresentacaoArgs = {
  input?: Maybe<RemoverAvaliadorDaApresentacaoInput>
};

export type Token = {
   __typename?: 'Token',
  access_token: Scalars['String'],
  token_type: Scalars['String'],
  refresh_token: Scalars['String'],
  expires_in: Scalars['Int'],
  scope: Scalars['String'],
  foto: Scalars['String'],
  id_usuario: Scalars['Int'],
  nome: Scalars['String'],
  jti: Scalars['String'],
};

export type Unidade = {
   __typename?: 'Unidade',
  idUnidade: Scalars['Int'],
  codigoUnidade: Scalars['String'],
  nomeUnidade: Scalars['String'],
  siglaUnidade: Scalars['String'],
  hierarquiaUnidade: Scalars['String'],
  unidadeGestora: Scalars['Int'],
  tipoUnidade: Scalars['String'],
  idCampus: Scalars['Int'],
};


export type UsuarioAtual = {
   __typename?: 'UsuarioAtual',
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  nomeSocialPessoa: Scalars['String'],
  token?: Maybe<Scalars['String']>,
  matriculaPessoa: Scalars['String'],
  lotacaoPessoa: Scalars['String'],
  avatarUrl: Scalars['String'],
  avaliadorPessoa: Scalars['Int'],
  monitorPessoa: Scalars['Int'],
  gerentePessoa: Scalars['Int'],
};
export type CardAvaliadorFragmentFragment = (
  { __typename?: 'Avaliador' }
  & Pick<Avaliador, 'idPessoa' | 'idVinculoPessoa' | 'nomePessoa' | 'lotacaoPessoa' | 'matriculaPessoa'>
);

export type AtualizarDisponibilidadeApresentacaoMutationVariables = {
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String'],
  disponibilidade: Scalars['Int']
};


export type AtualizarDisponibilidadeApresentacaoMutation = (
  { __typename?: 'Mutation' }
  & { atualizarApresentacao: Maybe<(
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao' | 'horaApresentacao' | 'salaApresentacao' | 'codigoApresentacao'>
    & { areaTematica: Maybe<(
      { __typename?: 'AreaTematica' }
      & Pick<AreaTematica, 'nomeAreaTematica'>
    )> }
  )> }
);

export type AvaliarProjetoMutationVariables = {
  input: AvaliarProjetoInput
};


export type AvaliarProjetoMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'avaliarProjeto'>
);

export type BaterAvaliadorMutationVariables = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String']
};


export type BaterAvaliadorMutation = (
  { __typename?: 'Mutation' }
  & { baterAvaliador: (
    { __typename?: 'Avaliador' }
    & Pick<Avaliador, 'idPessoa' | 'idVinculoPessoa' | 'nomePessoa' | 'lotacaoPessoa' | 'matriculaPessoa'>
  ) }
);

export type CriarReportMutationVariables = {
  conteudoReport: Scalars['String'],
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  emailReport: Scalars['String'],
  idApresentacao?: Maybe<Scalars['Int']>,
  id2Apresentacao?: Maybe<Scalars['String']>
};


export type CriarReportMutation = (
  { __typename?: 'Mutation' }
  & { criarReport: Maybe<(
    { __typename?: 'Report' }
    & Pick<Report, 'idReport'>
  )> }
);

export type CriarSolicitacaoLgpdMutationVariables = {
  tipoSolicitacao: Scalars['Int'],
  textoSolicitacao?: Maybe<Scalars['String']>,
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  nomeSolicitacao: Scalars['String'],
  sobrenomeSolicitacao: Scalars['String'],
  emailPessoa: Scalars['String']
};


export type CriarSolicitacaoLgpdMutation = (
  { __typename?: 'Mutation' }
  & { criarSolicitacaoLGPD: Maybe<(
    { __typename?: 'Solicitacao' }
    & Pick<Solicitacao, 'idSolicitacao'>
  )> }
);

export type FecharApresentacaoMutationVariables = {
  codigoApresentacao: Scalars['String']
};


export type FecharApresentacaoMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'fecharApresentacao'>
);

export type FecharProjetoMutationVariables = {
  idProjeto: Scalars['Int']
};


export type FecharProjetoMutation = (
  { __typename?: 'Mutation' }
  & { fecharProjeto: (
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'idProjeto' | 'disponibilidadeProjeto'>
  ) }
);

export type FecharProjetoParaUmAvaliadorMutationVariables = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  idProjeto: Scalars['Int']
};


export type FecharProjetoParaUmAvaliadorMutation = (
  { __typename?: 'Mutation' }
  & { fecharProjetoParaUmAvaliador: (
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'idProjeto'>
  ) }
);

export type IniciarApresentacaoMutationVariables = {
  codigoApresentacao: Scalars['String']
};


export type IniciarApresentacaoMutation = (
  { __typename?: 'Mutation' }
  & { iniciarApresentacao: (
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'codigoApresentacao' | 'idApresentacao' | 'id2Apresentacao' | 'horaApresentacao' | 'salaApresentacao'>
    & { areaTematica: Maybe<(
      { __typename?: 'AreaTematica' }
      & Pick<AreaTematica, 'nomeAreaTematica'>
    )> }
  ) }
);

export type LiberarProjetoMutationVariables = {
  idProjeto: Scalars['Int']
};


export type LiberarProjetoMutation = (
  { __typename?: 'Mutation' }
  & { liberarProjeto: (
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'idProjeto' | 'disponibilidadeProjeto'>
  ) }
);

export type LiberarProjetoParaUmAvaliadorMutationVariables = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  idProjeto: Scalars['Int']
};


export type LiberarProjetoParaUmAvaliadorMutation = (
  { __typename?: 'Mutation' }
  & { liberarProjetoParaUmAvaliador: (
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'idProjeto'>
  ) }
);

export type RemoverAvaliadorMutationVariables = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String'],
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String']
};


export type RemoverAvaliadorMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'removerAvaliadorDaApresentacao'>
);

export type UpdateNotaPerguntaMutationVariables = {
  idProjeto: Scalars['Int'],
  idPergunta: Scalars['Int'],
  nota: Scalars['Float']
};


export type UpdateNotaPerguntaMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'updateNotaPergunta'>
);

export type ApresentacaoAtualQueryVariables = {};


export type ApresentacaoAtualQuery = (
  { __typename?: 'Query' }
  & { apresentacaoAtual: Maybe<(
    { __typename?: 'ApresentacaoAtual' }
    & Pick<ApresentacaoAtual, 'idApresentacao' | 'id2Apresentacao' | 'nomeAreaTematica' | 'horaApresentacao' | 'salaApresentacao' | 'codigoApresentacao'>
  )> }
);

export type ApresentacaoAvaliadorQueryVariables = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String']
};


export type ApresentacaoAvaliadorQuery = (
  { __typename?: 'Query' }
  & { apresentacoesAvaliador: Maybe<Array<(
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao' | 'codigoApresentacao' | 'horaApresentacao' | 'linkApresentacao'>
    & { categoria: (
      { __typename?: 'Categoria' }
      & Pick<Categoria, 'idCategoria' | 'nomeCategoria'>
    ), areaTematica: Maybe<(
      { __typename?: 'AreaTematica' }
      & Pick<AreaTematica, 'nomeAreaTematica'>
    )> }
  )>> }
);

export type ApresentacaoPorIdQueryVariables = {
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String']
};


export type ApresentacaoPorIdQuery = (
  { __typename?: 'Query' }
  & { apresentacao: Maybe<(
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao' | 'salaApresentacao' | 'horaApresentacao'>
    & { areaTematica: Maybe<(
      { __typename?: 'AreaTematica' }
      & Pick<AreaTematica, 'nomeAreaTematica'>
    )> }
  )> }
);

export type AvaliacaoProjetoFragment = (
  { __typename: 'Avaliacao' }
  & { projeto: (
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'idProjeto'>
  ), notas: Array<Maybe<(
    { __typename?: 'NotaPergunta' }
    & Pick<NotaPergunta, 'nota' | 'respostaTextual'>
    & { pergunta: (
      { __typename?: 'Pergunta' }
      & Pick<Pergunta, 'idPergunta' | 'conteudoPergunta'>
      & { categoria: (
        { __typename?: 'Categoria' }
        & Pick<Categoria, 'idCategoria' | 'nomeCategoria'>
      ) }
    ) }
  )>> }
);

export type AvaliacaoProjetoQueryVariables = {
  idProjeto: Scalars['Int'],
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String']
};


export type AvaliacaoProjetoQuery = (
  { __typename?: 'Query' }
  & { perguntas: Maybe<Array<(
    { __typename?: 'Pergunta' }
    & Pick<Pergunta, 'idPergunta' | 'conteudoPergunta'>
    & { categoria: (
      { __typename?: 'Categoria' }
      & Pick<Categoria, 'idCategoria' | 'nomeCategoria'>
    ) }
  )>>, projeto: Maybe<(
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'idProjeto' | 'tituloProjeto' | 'linkArtefato'>
    & { categoria: (
      { __typename?: 'Categoria' }
      & Pick<Categoria, 'idCategoria' | 'nomeCategoria'>
    ) }
  )>, avaliacao: Maybe<{ __typename?: 'Avaliacao' }
    & AvaliacaoProjetoFragment
  > }
);

export type AvaliacoesRealizadasQueryVariables = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String']
};


export type AvaliacoesRealizadasQuery = (
  { __typename?: 'Query' }
  & { pessoa: Maybe<(
    { __typename?: 'Avaliador' }
    & Pick<Avaliador, 'idPessoa' | 'idVinculoPessoa'>
    & { avaliacoes: Maybe<Array<(
      { __typename?: 'Avaliacao' }
      & Pick<Avaliacao, 'media'>
      & { projeto: (
        { __typename?: 'Projeto' }
        & Pick<Projeto, 'idProjeto' | 'tituloProjeto'>
        & { mediaIndividual: Maybe<(
          { __typename?: 'mediaPorAvaliador' }
          & Pick<MediaPorAvaliador, 'mediaProjeto'>
        )> }
      ) }
    )>> }
  ) | (
    { __typename?: 'Monitor' }
    & Pick<Monitor, 'idPessoa' | 'idVinculoPessoa'>
  ) | (
    { __typename?: 'Gerente' }
    & Pick<Gerente, 'idPessoa' | 'idVinculoPessoa'>
    & { avaliacoes: Maybe<Array<(
      { __typename?: 'Avaliacao' }
      & Pick<Avaliacao, 'media'>
      & { projeto: (
        { __typename?: 'Projeto' }
        & Pick<Projeto, 'idProjeto' | 'tituloProjeto'>
        & { mediaIndividual: Maybe<(
          { __typename?: 'mediaPorAvaliador' }
          & Pick<MediaPorAvaliador, 'mediaProjeto'>
        )> }
      ) }
    )>> }
  ) | (
    { __typename?: 'Administrador' }
    & Pick<Administrador, 'idPessoa' | 'idVinculoPessoa'>
  ) | (
    { __typename?: 'Ouvinte' }
    & Pick<Ouvinte, 'idPessoa' | 'idVinculoPessoa'>
  )> }
);

export type AvaliadoresProjetoQueryVariables = {
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String']
};


export type AvaliadoresProjetoQuery = (
  { __typename?: 'Query' }
  & { projeto: Maybe<(
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'idProjeto' | 'tituloProjeto' | 'disponibilidadeProjeto'>
  )>, apresentacao: Maybe<(
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao'>
    & { avaliadores: Maybe<Array<{ __typename?: 'Avaliador' }
      & CardAvaliadorFragmentFragment
    >> }
  )> }
);

export type GerenciaProjetoQueryVariables = {
  idProjeto: Scalars['Int'],
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String']
};


export type GerenciaProjetoQuery = (
  { __typename?: 'Query' }
  & { projeto: Maybe<(
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'idProjeto' | 'tituloProjeto' | 'disponibilidadeProjeto'>
    & { avaliadoresHabilitados: Maybe<Array<(
      { __typename?: 'Avaliador' }
      & Pick<Avaliador, 'idPessoa' | 'idVinculoPessoa'>
    )>> }
  )>, apresentacao: Maybe<(
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao'>
    & { avaliadores: Maybe<Array<(
      { __typename?: 'Avaliador' }
      & { avaliacoes: Maybe<Array<(
        { __typename?: 'Avaliacao' }
        & Pick<Avaliacao, 'media'>
        & { projeto: (
          { __typename?: 'Projeto' }
          & Pick<Projeto, 'idProjeto'>
        ) }
      )>> }
    )
      & CardAvaliadorFragmentFragment
    >> }
  )> }
);

export type InfoEventoQueryVariables = {};


export type InfoEventoQuery = (
  { __typename?: 'Query' }
  & { eventos: Maybe<Array<(
    { __typename?: 'Evento' }
    & Pick<Evento, 'nomeEvento' | 'dataInicioEvento' | 'dataFimEvento' | 'statusEvento'>
  )>> }
);

export type PessoasApresentacaoQueryVariables = {
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String']
};


export type PessoasApresentacaoQuery = (
  { __typename?: 'Query' }
  & { apresentacao: Maybe<(
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao'>
    & { avaliadores: Maybe<Array<{ __typename?: 'Avaliador' }
      & CardAvaliadorFragmentFragment
    >> }
  )> }
);

export type ProjetosApresentacaoQueryVariables = {
  idApresentacao: Scalars['Int'],
  id2Apresentacao: Scalars['String']
};


export type ProjetosApresentacaoQuery = (
  { __typename?: 'Query' }
  & { apresentacao: Maybe<(
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao'>
    & { projetos: Maybe<Array<(
      { __typename?: 'Projeto' }
      & Pick<Projeto, 'idProjeto' | 'tituloProjeto' | 'disponibilidadeProjeto'>
      & { coordenador: Maybe<Array<(
        { __typename?: 'Membro' }
        & Pick<Membro, 'nomePessoa'>
      )>> }
    )>> }
  )> }
);

export type ProjetosPendentesQueryVariables = {
  idPessoa: Scalars['String'],
  idVinculoPessoa: Scalars['String']
};


export type ProjetosPendentesQuery = (
  { __typename?: 'Query' }
  & { pessoa: Maybe<(
    { __typename?: 'Avaliador' }
    & Pick<Avaliador, 'idPessoa' | 'idVinculoPessoa'>
    & { projetosParaAvaliar: Maybe<Array<(
      { __typename?: 'Projeto' }
      & Pick<Projeto, 'idProjeto' | 'tituloProjeto'>
      & { apresentacao: Maybe<(
        { __typename?: 'Apresentacao' }
        & Pick<Apresentacao, 'idApresentacao'>
      )>, categoria: (
        { __typename?: 'Categoria' }
        & Pick<Categoria, 'idCategoria'>
      ), mediaIndividual: Maybe<(
        { __typename?: 'mediaPorAvaliador' }
        & Pick<MediaPorAvaliador, 'mediaProjeto'>
      )> }
    )>> }
  ) | (
    { __typename?: 'Monitor' }
    & Pick<Monitor, 'idPessoa' | 'idVinculoPessoa'>
  ) | (
    { __typename?: 'Gerente' }
    & Pick<Gerente, 'idPessoa' | 'idVinculoPessoa'>
    & { projetosParaAvaliar: Maybe<Array<(
      { __typename?: 'Projeto' }
      & Pick<Projeto, 'idProjeto' | 'tituloProjeto'>
      & { apresentacao: Maybe<(
        { __typename?: 'Apresentacao' }
        & Pick<Apresentacao, 'idApresentacao'>
      )>, categoria: (
        { __typename?: 'Categoria' }
        & Pick<Categoria, 'idCategoria'>
      ), mediaIndividual: Maybe<(
        { __typename?: 'mediaPorAvaliador' }
        & Pick<MediaPorAvaliador, 'mediaProjeto'>
      )> }
    )>> }
  ) | (
    { __typename?: 'Administrador' }
    & Pick<Administrador, 'idPessoa' | 'idVinculoPessoa'>
  ) | (
    { __typename?: 'Ouvinte' }
    & Pick<Ouvinte, 'idPessoa' | 'idVinculoPessoa'>
  )> }
);

export type UsuarioAtualQueryVariables = {};


export type UsuarioAtualQuery = (
  { __typename?: 'Query' }
  & { usuarioAtual: (
    { __typename: 'UsuarioAtual' }
    & Pick<UsuarioAtual, 'idPessoa' | 'idVinculoPessoa' | 'nomeSocialPessoa' | 'token' | 'matriculaPessoa' | 'lotacaoPessoa' | 'avatarUrl' | 'monitorPessoa' | 'avaliadorPessoa' | 'gerentePessoa'>
  ) }
);

export type ApresentacaoAtualFragmentFragment = (
  { __typename?: 'ApresentacaoAtual' }
  & Pick<ApresentacaoAtual, 'idApresentacao' | 'id2Apresentacao' | 'nomeAreaTematica' | 'horaApresentacao' | 'salaApresentacao' | 'codigoApresentacao'>
);
export const CardAvaliadorFragmentFragmentDoc = gql`
    fragment cardAvaliadorFragment on Avaliador {
  idPessoa
  idVinculoPessoa
  nomePessoa
  lotacaoPessoa
  matriculaPessoa
}
    `;
export const AvaliacaoProjetoFragmentDoc = gql`
    fragment AvaliacaoProjeto on Avaliacao {
  __typename
  projeto {
    idProjeto
  }
  notas {
    nota
    respostaTextual
    pergunta {
      idPergunta
      conteudoPergunta
      categoria {
        idCategoria
        nomeCategoria
      }
    }
  }
}
    `;
export const ApresentacaoAtualFragmentFragmentDoc = gql`
    fragment ApresentacaoAtualFragment on ApresentacaoAtual {
  idApresentacao
  id2Apresentacao
  nomeAreaTematica
  horaApresentacao
  salaApresentacao
  codigoApresentacao
}
    `;
export const AtualizarDisponibilidadeApresentacaoDocument = gql`
    mutation atualizarDisponibilidadeApresentacao($idApresentacao: Int!, $id2Apresentacao: String!, $disponibilidade: Int!) {
  atualizarApresentacao(input: {idApresentacao: $idApresentacao, id2Apresentacao: $id2Apresentacao, disponibilidadeApresentacao: $disponibilidade}) {
    idApresentacao
    id2Apresentacao
    areaTematica {
      nomeAreaTematica
    }
    horaApresentacao
    salaApresentacao
    codigoApresentacao
  }
}
    `;
export type AtualizarDisponibilidadeApresentacaoMutationFn = ApolloReactCommon.MutationFunction<AtualizarDisponibilidadeApresentacaoMutation, AtualizarDisponibilidadeApresentacaoMutationVariables>;
export type AtualizarDisponibilidadeApresentacaoComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AtualizarDisponibilidadeApresentacaoMutation, AtualizarDisponibilidadeApresentacaoMutationVariables>, 'mutation'>;

    export const AtualizarDisponibilidadeApresentacaoComponent = (props: AtualizarDisponibilidadeApresentacaoComponentProps) => (
      <ApolloReactComponents.Mutation<AtualizarDisponibilidadeApresentacaoMutation, AtualizarDisponibilidadeApresentacaoMutationVariables> mutation={AtualizarDisponibilidadeApresentacaoDocument} {...props} />
    );
    
export type AtualizarDisponibilidadeApresentacaoProps<TChildProps = {}> = ApolloReactHoc.MutateProps<AtualizarDisponibilidadeApresentacaoMutation, AtualizarDisponibilidadeApresentacaoMutationVariables> & TChildProps;
export function withAtualizarDisponibilidadeApresentacao<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  AtualizarDisponibilidadeApresentacaoMutation,
  AtualizarDisponibilidadeApresentacaoMutationVariables,
  AtualizarDisponibilidadeApresentacaoProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, AtualizarDisponibilidadeApresentacaoMutation, AtualizarDisponibilidadeApresentacaoMutationVariables, AtualizarDisponibilidadeApresentacaoProps<TChildProps>>(AtualizarDisponibilidadeApresentacaoDocument, {
      alias: 'atualizarDisponibilidadeApresentacao',
      ...operationOptions
    });
};

    export function useAtualizarDisponibilidadeApresentacaoMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AtualizarDisponibilidadeApresentacaoMutation, AtualizarDisponibilidadeApresentacaoMutationVariables>) {
      return ApolloReactHooks.useMutation<AtualizarDisponibilidadeApresentacaoMutation, AtualizarDisponibilidadeApresentacaoMutationVariables>(AtualizarDisponibilidadeApresentacaoDocument, baseOptions);
    }
export type AtualizarDisponibilidadeApresentacaoMutationHookResult = ReturnType<typeof useAtualizarDisponibilidadeApresentacaoMutation>;
export type AtualizarDisponibilidadeApresentacaoMutationResult = ApolloReactCommon.MutationResult<AtualizarDisponibilidadeApresentacaoMutation>;
export type AtualizarDisponibilidadeApresentacaoMutationOptions = ApolloReactCommon.BaseMutationOptions<AtualizarDisponibilidadeApresentacaoMutation, AtualizarDisponibilidadeApresentacaoMutationVariables>;
export const AvaliarProjetoDocument = gql`
    mutation AvaliarProjeto($input: avaliarProjetoInput!) {
  avaliarProjeto(input: $input)
}
    `;
export type AvaliarProjetoMutationFn = ApolloReactCommon.MutationFunction<AvaliarProjetoMutation, AvaliarProjetoMutationVariables>;
export type AvaliarProjetoComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AvaliarProjetoMutation, AvaliarProjetoMutationVariables>, 'mutation'>;

    export const AvaliarProjetoComponent = (props: AvaliarProjetoComponentProps) => (
      <ApolloReactComponents.Mutation<AvaliarProjetoMutation, AvaliarProjetoMutationVariables> mutation={AvaliarProjetoDocument} {...props} />
    );
    
export type AvaliarProjetoProps<TChildProps = {}> = ApolloReactHoc.MutateProps<AvaliarProjetoMutation, AvaliarProjetoMutationVariables> & TChildProps;
export function withAvaliarProjeto<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  AvaliarProjetoMutation,
  AvaliarProjetoMutationVariables,
  AvaliarProjetoProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, AvaliarProjetoMutation, AvaliarProjetoMutationVariables, AvaliarProjetoProps<TChildProps>>(AvaliarProjetoDocument, {
      alias: 'avaliarProjeto',
      ...operationOptions
    });
};

    export function useAvaliarProjetoMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AvaliarProjetoMutation, AvaliarProjetoMutationVariables>) {
      return ApolloReactHooks.useMutation<AvaliarProjetoMutation, AvaliarProjetoMutationVariables>(AvaliarProjetoDocument, baseOptions);
    }
export type AvaliarProjetoMutationHookResult = ReturnType<typeof useAvaliarProjetoMutation>;
export type AvaliarProjetoMutationResult = ApolloReactCommon.MutationResult<AvaliarProjetoMutation>;
export type AvaliarProjetoMutationOptions = ApolloReactCommon.BaseMutationOptions<AvaliarProjetoMutation, AvaliarProjetoMutationVariables>;
export const BaterAvaliadorDocument = gql`
    mutation baterAvaliador($idPessoa: String!, $idVinculoPessoa: String!, $idApresentacao: Int!, $id2Apresentacao: String!) {
  baterAvaliador(input: {idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa, idApresentacao: $idApresentacao, id2Apresentacao: $id2Apresentacao}) {
    idPessoa
    idVinculoPessoa
    nomePessoa
    lotacaoPessoa
    matriculaPessoa
  }
}
    `;
export type BaterAvaliadorMutationFn = ApolloReactCommon.MutationFunction<BaterAvaliadorMutation, BaterAvaliadorMutationVariables>;
export type BaterAvaliadorComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<BaterAvaliadorMutation, BaterAvaliadorMutationVariables>, 'mutation'>;

    export const BaterAvaliadorComponent = (props: BaterAvaliadorComponentProps) => (
      <ApolloReactComponents.Mutation<BaterAvaliadorMutation, BaterAvaliadorMutationVariables> mutation={BaterAvaliadorDocument} {...props} />
    );
    
export type BaterAvaliadorProps<TChildProps = {}> = ApolloReactHoc.MutateProps<BaterAvaliadorMutation, BaterAvaliadorMutationVariables> & TChildProps;
export function withBaterAvaliador<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  BaterAvaliadorMutation,
  BaterAvaliadorMutationVariables,
  BaterAvaliadorProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, BaterAvaliadorMutation, BaterAvaliadorMutationVariables, BaterAvaliadorProps<TChildProps>>(BaterAvaliadorDocument, {
      alias: 'baterAvaliador',
      ...operationOptions
    });
};

    export function useBaterAvaliadorMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<BaterAvaliadorMutation, BaterAvaliadorMutationVariables>) {
      return ApolloReactHooks.useMutation<BaterAvaliadorMutation, BaterAvaliadorMutationVariables>(BaterAvaliadorDocument, baseOptions);
    }
export type BaterAvaliadorMutationHookResult = ReturnType<typeof useBaterAvaliadorMutation>;
export type BaterAvaliadorMutationResult = ApolloReactCommon.MutationResult<BaterAvaliadorMutation>;
export type BaterAvaliadorMutationOptions = ApolloReactCommon.BaseMutationOptions<BaterAvaliadorMutation, BaterAvaliadorMutationVariables>;
export const CriarReportDocument = gql`
    mutation criarReport($conteudoReport: String!, $idPessoa: String!, $idVinculoPessoa: String!, $emailReport: String!, $idApresentacao: Int, $id2Apresentacao: String) {
  criarReport(input: {conteudoReport: $conteudoReport, idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa, emailReport: $emailReport, idApresentacao: $idApresentacao, id2Apresentacao: $id2Apresentacao}) {
    idReport
  }
}
    `;
export type CriarReportMutationFn = ApolloReactCommon.MutationFunction<CriarReportMutation, CriarReportMutationVariables>;
export type CriarReportComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CriarReportMutation, CriarReportMutationVariables>, 'mutation'>;

    export const CriarReportComponent = (props: CriarReportComponentProps) => (
      <ApolloReactComponents.Mutation<CriarReportMutation, CriarReportMutationVariables> mutation={CriarReportDocument} {...props} />
    );
    
export type CriarReportProps<TChildProps = {}> = ApolloReactHoc.MutateProps<CriarReportMutation, CriarReportMutationVariables> & TChildProps;
export function withCriarReport<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  CriarReportMutation,
  CriarReportMutationVariables,
  CriarReportProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, CriarReportMutation, CriarReportMutationVariables, CriarReportProps<TChildProps>>(CriarReportDocument, {
      alias: 'criarReport',
      ...operationOptions
    });
};

    export function useCriarReportMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CriarReportMutation, CriarReportMutationVariables>) {
      return ApolloReactHooks.useMutation<CriarReportMutation, CriarReportMutationVariables>(CriarReportDocument, baseOptions);
    }
export type CriarReportMutationHookResult = ReturnType<typeof useCriarReportMutation>;
export type CriarReportMutationResult = ApolloReactCommon.MutationResult<CriarReportMutation>;
export type CriarReportMutationOptions = ApolloReactCommon.BaseMutationOptions<CriarReportMutation, CriarReportMutationVariables>;
export const CriarSolicitacaoLgpdDocument = gql`
    mutation criarSolicitacaoLGPD($tipoSolicitacao: Int!, $textoSolicitacao: String, $idPessoa: String!, $idVinculoPessoa: String!, $nomeSolicitacao: String!, $sobrenomeSolicitacao: String!, $emailPessoa: String!) {
  criarSolicitacaoLGPD(input: {tipoSolicitacao: $tipoSolicitacao, textoSolicitacao: $textoSolicitacao, idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa, nomeSolicitacao: $nomeSolicitacao, sobrenomeSolicitacao: $sobrenomeSolicitacao, emailPessoa: $emailPessoa}) {
    idSolicitacao
  }
}
    `;
export type CriarSolicitacaoLgpdMutationFn = ApolloReactCommon.MutationFunction<CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables>;
export type CriarSolicitacaoLgpdComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables>, 'mutation'>;

    export const CriarSolicitacaoLgpdComponent = (props: CriarSolicitacaoLgpdComponentProps) => (
      <ApolloReactComponents.Mutation<CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables> mutation={CriarSolicitacaoLgpdDocument} {...props} />
    );
    
export type CriarSolicitacaoLgpdProps<TChildProps = {}> = ApolloReactHoc.MutateProps<CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables> & TChildProps;
export function withCriarSolicitacaoLgpd<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  CriarSolicitacaoLgpdMutation,
  CriarSolicitacaoLgpdMutationVariables,
  CriarSolicitacaoLgpdProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables, CriarSolicitacaoLgpdProps<TChildProps>>(CriarSolicitacaoLgpdDocument, {
      alias: 'criarSolicitacaoLgpd',
      ...operationOptions
    });
};

    export function useCriarSolicitacaoLgpdMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables>) {
      return ApolloReactHooks.useMutation<CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables>(CriarSolicitacaoLgpdDocument, baseOptions);
    }
export type CriarSolicitacaoLgpdMutationHookResult = ReturnType<typeof useCriarSolicitacaoLgpdMutation>;
export type CriarSolicitacaoLgpdMutationResult = ApolloReactCommon.MutationResult<CriarSolicitacaoLgpdMutation>;
export type CriarSolicitacaoLgpdMutationOptions = ApolloReactCommon.BaseMutationOptions<CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables>;
export const FecharApresentacaoDocument = gql`
    mutation fecharApresentacao($codigoApresentacao: String!) {
  fecharApresentacao(input: {codigoApresentacao: $codigoApresentacao})
}
    `;
export type FecharApresentacaoMutationFn = ApolloReactCommon.MutationFunction<FecharApresentacaoMutation, FecharApresentacaoMutationVariables>;
export type FecharApresentacaoComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<FecharApresentacaoMutation, FecharApresentacaoMutationVariables>, 'mutation'>;

    export const FecharApresentacaoComponent = (props: FecharApresentacaoComponentProps) => (
      <ApolloReactComponents.Mutation<FecharApresentacaoMutation, FecharApresentacaoMutationVariables> mutation={FecharApresentacaoDocument} {...props} />
    );
    
export type FecharApresentacaoProps<TChildProps = {}> = ApolloReactHoc.MutateProps<FecharApresentacaoMutation, FecharApresentacaoMutationVariables> & TChildProps;
export function withFecharApresentacao<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  FecharApresentacaoMutation,
  FecharApresentacaoMutationVariables,
  FecharApresentacaoProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, FecharApresentacaoMutation, FecharApresentacaoMutationVariables, FecharApresentacaoProps<TChildProps>>(FecharApresentacaoDocument, {
      alias: 'fecharApresentacao',
      ...operationOptions
    });
};

    export function useFecharApresentacaoMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<FecharApresentacaoMutation, FecharApresentacaoMutationVariables>) {
      return ApolloReactHooks.useMutation<FecharApresentacaoMutation, FecharApresentacaoMutationVariables>(FecharApresentacaoDocument, baseOptions);
    }
export type FecharApresentacaoMutationHookResult = ReturnType<typeof useFecharApresentacaoMutation>;
export type FecharApresentacaoMutationResult = ApolloReactCommon.MutationResult<FecharApresentacaoMutation>;
export type FecharApresentacaoMutationOptions = ApolloReactCommon.BaseMutationOptions<FecharApresentacaoMutation, FecharApresentacaoMutationVariables>;
export const FecharProjetoDocument = gql`
    mutation fecharProjeto($idProjeto: Int!) {
  fecharProjeto(input: {idProjeto: $idProjeto}) {
    idProjeto
    disponibilidadeProjeto
  }
}
    `;
export type FecharProjetoMutationFn = ApolloReactCommon.MutationFunction<FecharProjetoMutation, FecharProjetoMutationVariables>;
export type FecharProjetoComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<FecharProjetoMutation, FecharProjetoMutationVariables>, 'mutation'>;

    export const FecharProjetoComponent = (props: FecharProjetoComponentProps) => (
      <ApolloReactComponents.Mutation<FecharProjetoMutation, FecharProjetoMutationVariables> mutation={FecharProjetoDocument} {...props} />
    );
    
export type FecharProjetoProps<TChildProps = {}> = ApolloReactHoc.MutateProps<FecharProjetoMutation, FecharProjetoMutationVariables> & TChildProps;
export function withFecharProjeto<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  FecharProjetoMutation,
  FecharProjetoMutationVariables,
  FecharProjetoProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, FecharProjetoMutation, FecharProjetoMutationVariables, FecharProjetoProps<TChildProps>>(FecharProjetoDocument, {
      alias: 'fecharProjeto',
      ...operationOptions
    });
};

    export function useFecharProjetoMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<FecharProjetoMutation, FecharProjetoMutationVariables>) {
      return ApolloReactHooks.useMutation<FecharProjetoMutation, FecharProjetoMutationVariables>(FecharProjetoDocument, baseOptions);
    }
export type FecharProjetoMutationHookResult = ReturnType<typeof useFecharProjetoMutation>;
export type FecharProjetoMutationResult = ApolloReactCommon.MutationResult<FecharProjetoMutation>;
export type FecharProjetoMutationOptions = ApolloReactCommon.BaseMutationOptions<FecharProjetoMutation, FecharProjetoMutationVariables>;
export const FecharProjetoParaUmAvaliadorDocument = gql`
    mutation fecharProjetoParaUmAvaliador($idPessoa: String!, $idVinculoPessoa: String!, $idProjeto: Int!) {
  fecharProjetoParaUmAvaliador(input: {idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa, idProjeto: $idProjeto}) {
    idProjeto
  }
}
    `;
export type FecharProjetoParaUmAvaliadorMutationFn = ApolloReactCommon.MutationFunction<FecharProjetoParaUmAvaliadorMutation, FecharProjetoParaUmAvaliadorMutationVariables>;
export type FecharProjetoParaUmAvaliadorComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<FecharProjetoParaUmAvaliadorMutation, FecharProjetoParaUmAvaliadorMutationVariables>, 'mutation'>;

    export const FecharProjetoParaUmAvaliadorComponent = (props: FecharProjetoParaUmAvaliadorComponentProps) => (
      <ApolloReactComponents.Mutation<FecharProjetoParaUmAvaliadorMutation, FecharProjetoParaUmAvaliadorMutationVariables> mutation={FecharProjetoParaUmAvaliadorDocument} {...props} />
    );
    
export type FecharProjetoParaUmAvaliadorProps<TChildProps = {}> = ApolloReactHoc.MutateProps<FecharProjetoParaUmAvaliadorMutation, FecharProjetoParaUmAvaliadorMutationVariables> & TChildProps;
export function withFecharProjetoParaUmAvaliador<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  FecharProjetoParaUmAvaliadorMutation,
  FecharProjetoParaUmAvaliadorMutationVariables,
  FecharProjetoParaUmAvaliadorProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, FecharProjetoParaUmAvaliadorMutation, FecharProjetoParaUmAvaliadorMutationVariables, FecharProjetoParaUmAvaliadorProps<TChildProps>>(FecharProjetoParaUmAvaliadorDocument, {
      alias: 'fecharProjetoParaUmAvaliador',
      ...operationOptions
    });
};

    export function useFecharProjetoParaUmAvaliadorMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<FecharProjetoParaUmAvaliadorMutation, FecharProjetoParaUmAvaliadorMutationVariables>) {
      return ApolloReactHooks.useMutation<FecharProjetoParaUmAvaliadorMutation, FecharProjetoParaUmAvaliadorMutationVariables>(FecharProjetoParaUmAvaliadorDocument, baseOptions);
    }
export type FecharProjetoParaUmAvaliadorMutationHookResult = ReturnType<typeof useFecharProjetoParaUmAvaliadorMutation>;
export type FecharProjetoParaUmAvaliadorMutationResult = ApolloReactCommon.MutationResult<FecharProjetoParaUmAvaliadorMutation>;
export type FecharProjetoParaUmAvaliadorMutationOptions = ApolloReactCommon.BaseMutationOptions<FecharProjetoParaUmAvaliadorMutation, FecharProjetoParaUmAvaliadorMutationVariables>;
export const IniciarApresentacaoDocument = gql`
    mutation iniciarApresentacao($codigoApresentacao: String!) {
  iniciarApresentacao(input: {codigoApresentacao: $codigoApresentacao}) {
    codigoApresentacao
    idApresentacao
    id2Apresentacao
    areaTematica {
      nomeAreaTematica
    }
    horaApresentacao
    salaApresentacao
    codigoApresentacao
  }
}
    `;
export type IniciarApresentacaoMutationFn = ApolloReactCommon.MutationFunction<IniciarApresentacaoMutation, IniciarApresentacaoMutationVariables>;
export type IniciarApresentacaoComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<IniciarApresentacaoMutation, IniciarApresentacaoMutationVariables>, 'mutation'>;

    export const IniciarApresentacaoComponent = (props: IniciarApresentacaoComponentProps) => (
      <ApolloReactComponents.Mutation<IniciarApresentacaoMutation, IniciarApresentacaoMutationVariables> mutation={IniciarApresentacaoDocument} {...props} />
    );
    
export type IniciarApresentacaoProps<TChildProps = {}> = ApolloReactHoc.MutateProps<IniciarApresentacaoMutation, IniciarApresentacaoMutationVariables> & TChildProps;
export function withIniciarApresentacao<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  IniciarApresentacaoMutation,
  IniciarApresentacaoMutationVariables,
  IniciarApresentacaoProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, IniciarApresentacaoMutation, IniciarApresentacaoMutationVariables, IniciarApresentacaoProps<TChildProps>>(IniciarApresentacaoDocument, {
      alias: 'iniciarApresentacao',
      ...operationOptions
    });
};

    export function useIniciarApresentacaoMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<IniciarApresentacaoMutation, IniciarApresentacaoMutationVariables>) {
      return ApolloReactHooks.useMutation<IniciarApresentacaoMutation, IniciarApresentacaoMutationVariables>(IniciarApresentacaoDocument, baseOptions);
    }
export type IniciarApresentacaoMutationHookResult = ReturnType<typeof useIniciarApresentacaoMutation>;
export type IniciarApresentacaoMutationResult = ApolloReactCommon.MutationResult<IniciarApresentacaoMutation>;
export type IniciarApresentacaoMutationOptions = ApolloReactCommon.BaseMutationOptions<IniciarApresentacaoMutation, IniciarApresentacaoMutationVariables>;
export const LiberarProjetoDocument = gql`
    mutation liberarProjeto($idProjeto: Int!) {
  liberarProjeto(input: {idProjeto: $idProjeto}) {
    idProjeto
    disponibilidadeProjeto
  }
}
    `;
export type LiberarProjetoMutationFn = ApolloReactCommon.MutationFunction<LiberarProjetoMutation, LiberarProjetoMutationVariables>;
export type LiberarProjetoComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<LiberarProjetoMutation, LiberarProjetoMutationVariables>, 'mutation'>;

    export const LiberarProjetoComponent = (props: LiberarProjetoComponentProps) => (
      <ApolloReactComponents.Mutation<LiberarProjetoMutation, LiberarProjetoMutationVariables> mutation={LiberarProjetoDocument} {...props} />
    );
    
export type LiberarProjetoProps<TChildProps = {}> = ApolloReactHoc.MutateProps<LiberarProjetoMutation, LiberarProjetoMutationVariables> & TChildProps;
export function withLiberarProjeto<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  LiberarProjetoMutation,
  LiberarProjetoMutationVariables,
  LiberarProjetoProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, LiberarProjetoMutation, LiberarProjetoMutationVariables, LiberarProjetoProps<TChildProps>>(LiberarProjetoDocument, {
      alias: 'liberarProjeto',
      ...operationOptions
    });
};

    export function useLiberarProjetoMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LiberarProjetoMutation, LiberarProjetoMutationVariables>) {
      return ApolloReactHooks.useMutation<LiberarProjetoMutation, LiberarProjetoMutationVariables>(LiberarProjetoDocument, baseOptions);
    }
export type LiberarProjetoMutationHookResult = ReturnType<typeof useLiberarProjetoMutation>;
export type LiberarProjetoMutationResult = ApolloReactCommon.MutationResult<LiberarProjetoMutation>;
export type LiberarProjetoMutationOptions = ApolloReactCommon.BaseMutationOptions<LiberarProjetoMutation, LiberarProjetoMutationVariables>;
export const LiberarProjetoParaUmAvaliadorDocument = gql`
    mutation liberarProjetoParaUmAvaliador($idPessoa: String!, $idVinculoPessoa: String!, $idProjeto: Int!) {
  liberarProjetoParaUmAvaliador(input: {idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa, idProjeto: $idProjeto}) {
    idProjeto
  }
}
    `;
export type LiberarProjetoParaUmAvaliadorMutationFn = ApolloReactCommon.MutationFunction<LiberarProjetoParaUmAvaliadorMutation, LiberarProjetoParaUmAvaliadorMutationVariables>;
export type LiberarProjetoParaUmAvaliadorComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<LiberarProjetoParaUmAvaliadorMutation, LiberarProjetoParaUmAvaliadorMutationVariables>, 'mutation'>;

    export const LiberarProjetoParaUmAvaliadorComponent = (props: LiberarProjetoParaUmAvaliadorComponentProps) => (
      <ApolloReactComponents.Mutation<LiberarProjetoParaUmAvaliadorMutation, LiberarProjetoParaUmAvaliadorMutationVariables> mutation={LiberarProjetoParaUmAvaliadorDocument} {...props} />
    );
    
export type LiberarProjetoParaUmAvaliadorProps<TChildProps = {}> = ApolloReactHoc.MutateProps<LiberarProjetoParaUmAvaliadorMutation, LiberarProjetoParaUmAvaliadorMutationVariables> & TChildProps;
export function withLiberarProjetoParaUmAvaliador<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  LiberarProjetoParaUmAvaliadorMutation,
  LiberarProjetoParaUmAvaliadorMutationVariables,
  LiberarProjetoParaUmAvaliadorProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, LiberarProjetoParaUmAvaliadorMutation, LiberarProjetoParaUmAvaliadorMutationVariables, LiberarProjetoParaUmAvaliadorProps<TChildProps>>(LiberarProjetoParaUmAvaliadorDocument, {
      alias: 'liberarProjetoParaUmAvaliador',
      ...operationOptions
    });
};

    export function useLiberarProjetoParaUmAvaliadorMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LiberarProjetoParaUmAvaliadorMutation, LiberarProjetoParaUmAvaliadorMutationVariables>) {
      return ApolloReactHooks.useMutation<LiberarProjetoParaUmAvaliadorMutation, LiberarProjetoParaUmAvaliadorMutationVariables>(LiberarProjetoParaUmAvaliadorDocument, baseOptions);
    }
export type LiberarProjetoParaUmAvaliadorMutationHookResult = ReturnType<typeof useLiberarProjetoParaUmAvaliadorMutation>;
export type LiberarProjetoParaUmAvaliadorMutationResult = ApolloReactCommon.MutationResult<LiberarProjetoParaUmAvaliadorMutation>;
export type LiberarProjetoParaUmAvaliadorMutationOptions = ApolloReactCommon.BaseMutationOptions<LiberarProjetoParaUmAvaliadorMutation, LiberarProjetoParaUmAvaliadorMutationVariables>;
export const RemoverAvaliadorDocument = gql`
    mutation removerAvaliador($idPessoa: String!, $idVinculoPessoa: String!, $idApresentacao: Int!, $id2Apresentacao: String!) {
  removerAvaliadorDaApresentacao(input: {idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa, idApresentacao: $idApresentacao, id2Apresentacao: $id2Apresentacao})
}
    `;
export type RemoverAvaliadorMutationFn = ApolloReactCommon.MutationFunction<RemoverAvaliadorMutation, RemoverAvaliadorMutationVariables>;
export type RemoverAvaliadorComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<RemoverAvaliadorMutation, RemoverAvaliadorMutationVariables>, 'mutation'>;

    export const RemoverAvaliadorComponent = (props: RemoverAvaliadorComponentProps) => (
      <ApolloReactComponents.Mutation<RemoverAvaliadorMutation, RemoverAvaliadorMutationVariables> mutation={RemoverAvaliadorDocument} {...props} />
    );
    
export type RemoverAvaliadorProps<TChildProps = {}> = ApolloReactHoc.MutateProps<RemoverAvaliadorMutation, RemoverAvaliadorMutationVariables> & TChildProps;
export function withRemoverAvaliador<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  RemoverAvaliadorMutation,
  RemoverAvaliadorMutationVariables,
  RemoverAvaliadorProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, RemoverAvaliadorMutation, RemoverAvaliadorMutationVariables, RemoverAvaliadorProps<TChildProps>>(RemoverAvaliadorDocument, {
      alias: 'removerAvaliador',
      ...operationOptions
    });
};

    export function useRemoverAvaliadorMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<RemoverAvaliadorMutation, RemoverAvaliadorMutationVariables>) {
      return ApolloReactHooks.useMutation<RemoverAvaliadorMutation, RemoverAvaliadorMutationVariables>(RemoverAvaliadorDocument, baseOptions);
    }
export type RemoverAvaliadorMutationHookResult = ReturnType<typeof useRemoverAvaliadorMutation>;
export type RemoverAvaliadorMutationResult = ApolloReactCommon.MutationResult<RemoverAvaliadorMutation>;
export type RemoverAvaliadorMutationOptions = ApolloReactCommon.BaseMutationOptions<RemoverAvaliadorMutation, RemoverAvaliadorMutationVariables>;
export const UpdateNotaPerguntaDocument = gql`
    mutation updateNotaPergunta($idProjeto: Int!, $idPergunta: Int!, $nota: Float!) {
  updateNotaPergunta(idProjeto: $idProjeto, idPergunta: $idPergunta, nota: $nota) @client
}
    `;
export type UpdateNotaPerguntaMutationFn = ApolloReactCommon.MutationFunction<UpdateNotaPerguntaMutation, UpdateNotaPerguntaMutationVariables>;
export type UpdateNotaPerguntaComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<UpdateNotaPerguntaMutation, UpdateNotaPerguntaMutationVariables>, 'mutation'>;

    export const UpdateNotaPerguntaComponent = (props: UpdateNotaPerguntaComponentProps) => (
      <ApolloReactComponents.Mutation<UpdateNotaPerguntaMutation, UpdateNotaPerguntaMutationVariables> mutation={UpdateNotaPerguntaDocument} {...props} />
    );
    
export type UpdateNotaPerguntaProps<TChildProps = {}> = ApolloReactHoc.MutateProps<UpdateNotaPerguntaMutation, UpdateNotaPerguntaMutationVariables> & TChildProps;
export function withUpdateNotaPergunta<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  UpdateNotaPerguntaMutation,
  UpdateNotaPerguntaMutationVariables,
  UpdateNotaPerguntaProps<TChildProps>>) {
    return ApolloReactHoc.withMutation<TProps, UpdateNotaPerguntaMutation, UpdateNotaPerguntaMutationVariables, UpdateNotaPerguntaProps<TChildProps>>(UpdateNotaPerguntaDocument, {
      alias: 'updateNotaPergunta',
      ...operationOptions
    });
};

    export function useUpdateNotaPerguntaMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateNotaPerguntaMutation, UpdateNotaPerguntaMutationVariables>) {
      return ApolloReactHooks.useMutation<UpdateNotaPerguntaMutation, UpdateNotaPerguntaMutationVariables>(UpdateNotaPerguntaDocument, baseOptions);
    }
export type UpdateNotaPerguntaMutationHookResult = ReturnType<typeof useUpdateNotaPerguntaMutation>;
export type UpdateNotaPerguntaMutationResult = ApolloReactCommon.MutationResult<UpdateNotaPerguntaMutation>;
export type UpdateNotaPerguntaMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdateNotaPerguntaMutation, UpdateNotaPerguntaMutationVariables>;
export const ApresentacaoAtualDocument = gql`
    query apresentacaoAtual {
  apresentacaoAtual @client {
    idApresentacao
    id2Apresentacao
    nomeAreaTematica
    horaApresentacao
    salaApresentacao
    codigoApresentacao
  }
}
    `;
export type ApresentacaoAtualComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ApresentacaoAtualQuery, ApresentacaoAtualQueryVariables>, 'query'>;

    export const ApresentacaoAtualComponent = (props: ApresentacaoAtualComponentProps) => (
      <ApolloReactComponents.Query<ApresentacaoAtualQuery, ApresentacaoAtualQueryVariables> query={ApresentacaoAtualDocument} {...props} />
    );
    
export type ApresentacaoAtualProps<TChildProps = {}> = ApolloReactHoc.DataProps<ApresentacaoAtualQuery, ApresentacaoAtualQueryVariables> & TChildProps;
export function withApresentacaoAtual<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  ApresentacaoAtualQuery,
  ApresentacaoAtualQueryVariables,
  ApresentacaoAtualProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, ApresentacaoAtualQuery, ApresentacaoAtualQueryVariables, ApresentacaoAtualProps<TChildProps>>(ApresentacaoAtualDocument, {
      alias: 'apresentacaoAtual',
      ...operationOptions
    });
};

    export function useApresentacaoAtualQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ApresentacaoAtualQuery, ApresentacaoAtualQueryVariables>) {
      return ApolloReactHooks.useQuery<ApresentacaoAtualQuery, ApresentacaoAtualQueryVariables>(ApresentacaoAtualDocument, baseOptions);
    }
      export function useApresentacaoAtualLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ApresentacaoAtualQuery, ApresentacaoAtualQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<ApresentacaoAtualQuery, ApresentacaoAtualQueryVariables>(ApresentacaoAtualDocument, baseOptions);
      }
      
export type ApresentacaoAtualQueryHookResult = ReturnType<typeof useApresentacaoAtualQuery>;
export type ApresentacaoAtualQueryResult = ApolloReactCommon.QueryResult<ApresentacaoAtualQuery, ApresentacaoAtualQueryVariables>;
export const ApresentacaoAvaliadorDocument = gql`
    query apresentacaoAvaliador($idPessoa: String!, $idVinculoPessoa: String!) {
  apresentacoesAvaliador(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
    idApresentacao
    id2Apresentacao
    codigoApresentacao
    categoria {
      idCategoria
      nomeCategoria
    }
    horaApresentacao
    areaTematica {
      nomeAreaTematica
    }
    linkApresentacao
  }
}
    `;
export type ApresentacaoAvaliadorComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ApresentacaoAvaliadorQuery, ApresentacaoAvaliadorQueryVariables>, 'query'> & ({ variables: ApresentacaoAvaliadorQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const ApresentacaoAvaliadorComponent = (props: ApresentacaoAvaliadorComponentProps) => (
      <ApolloReactComponents.Query<ApresentacaoAvaliadorQuery, ApresentacaoAvaliadorQueryVariables> query={ApresentacaoAvaliadorDocument} {...props} />
    );
    
export type ApresentacaoAvaliadorProps<TChildProps = {}> = ApolloReactHoc.DataProps<ApresentacaoAvaliadorQuery, ApresentacaoAvaliadorQueryVariables> & TChildProps;
export function withApresentacaoAvaliador<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  ApresentacaoAvaliadorQuery,
  ApresentacaoAvaliadorQueryVariables,
  ApresentacaoAvaliadorProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, ApresentacaoAvaliadorQuery, ApresentacaoAvaliadorQueryVariables, ApresentacaoAvaliadorProps<TChildProps>>(ApresentacaoAvaliadorDocument, {
      alias: 'apresentacaoAvaliador',
      ...operationOptions
    });
};

    export function useApresentacaoAvaliadorQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ApresentacaoAvaliadorQuery, ApresentacaoAvaliadorQueryVariables>) {
      return ApolloReactHooks.useQuery<ApresentacaoAvaliadorQuery, ApresentacaoAvaliadorQueryVariables>(ApresentacaoAvaliadorDocument, baseOptions);
    }
      export function useApresentacaoAvaliadorLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ApresentacaoAvaliadorQuery, ApresentacaoAvaliadorQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<ApresentacaoAvaliadorQuery, ApresentacaoAvaliadorQueryVariables>(ApresentacaoAvaliadorDocument, baseOptions);
      }
      
export type ApresentacaoAvaliadorQueryHookResult = ReturnType<typeof useApresentacaoAvaliadorQuery>;
export type ApresentacaoAvaliadorQueryResult = ApolloReactCommon.QueryResult<ApresentacaoAvaliadorQuery, ApresentacaoAvaliadorQueryVariables>;
export const ApresentacaoPorIdDocument = gql`
    query apresentacaoPorId($idApresentacao: Int!, $id2Apresentacao: String!) {
  apresentacao(input: {idApresentacao: $idApresentacao, id2Apresentacao: $id2Apresentacao}) {
    idApresentacao
    id2Apresentacao
    salaApresentacao
    areaTematica {
      nomeAreaTematica
    }
    horaApresentacao
  }
}
    `;
export type ApresentacaoPorIdComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ApresentacaoPorIdQuery, ApresentacaoPorIdQueryVariables>, 'query'> & ({ variables: ApresentacaoPorIdQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const ApresentacaoPorIdComponent = (props: ApresentacaoPorIdComponentProps) => (
      <ApolloReactComponents.Query<ApresentacaoPorIdQuery, ApresentacaoPorIdQueryVariables> query={ApresentacaoPorIdDocument} {...props} />
    );
    
export type ApresentacaoPorIdProps<TChildProps = {}> = ApolloReactHoc.DataProps<ApresentacaoPorIdQuery, ApresentacaoPorIdQueryVariables> & TChildProps;
export function withApresentacaoPorId<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  ApresentacaoPorIdQuery,
  ApresentacaoPorIdQueryVariables,
  ApresentacaoPorIdProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, ApresentacaoPorIdQuery, ApresentacaoPorIdQueryVariables, ApresentacaoPorIdProps<TChildProps>>(ApresentacaoPorIdDocument, {
      alias: 'apresentacaoPorId',
      ...operationOptions
    });
};

    export function useApresentacaoPorIdQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ApresentacaoPorIdQuery, ApresentacaoPorIdQueryVariables>) {
      return ApolloReactHooks.useQuery<ApresentacaoPorIdQuery, ApresentacaoPorIdQueryVariables>(ApresentacaoPorIdDocument, baseOptions);
    }
      export function useApresentacaoPorIdLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ApresentacaoPorIdQuery, ApresentacaoPorIdQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<ApresentacaoPorIdQuery, ApresentacaoPorIdQueryVariables>(ApresentacaoPorIdDocument, baseOptions);
      }
      
export type ApresentacaoPorIdQueryHookResult = ReturnType<typeof useApresentacaoPorIdQuery>;
export type ApresentacaoPorIdQueryResult = ApolloReactCommon.QueryResult<ApresentacaoPorIdQuery, ApresentacaoPorIdQueryVariables>;
export const AvaliacaoProjetoDocument = gql`
    query avaliacaoProjeto($idProjeto: Int!, $idPessoa: String!, $idVinculoPessoa: String!) {
  perguntas {
    idPergunta
    conteudoPergunta
    categoria {
      idCategoria
      nomeCategoria
    }
  }
  projeto(idProjeto: $idProjeto) {
    idProjeto
    tituloProjeto
    linkArtefato
    categoria {
      idCategoria
      nomeCategoria
    }
  }
  avaliacao(idProjeto: $idProjeto, idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
    ...AvaliacaoProjeto
  }
}
    ${AvaliacaoProjetoFragmentDoc}`;
export type AvaliacaoProjetoComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<AvaliacaoProjetoQuery, AvaliacaoProjetoQueryVariables>, 'query'> & ({ variables: AvaliacaoProjetoQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const AvaliacaoProjetoComponent = (props: AvaliacaoProjetoComponentProps) => (
      <ApolloReactComponents.Query<AvaliacaoProjetoQuery, AvaliacaoProjetoQueryVariables> query={AvaliacaoProjetoDocument} {...props} />
    );
    
export type AvaliacaoProjetoProps<TChildProps = {}> = ApolloReactHoc.DataProps<AvaliacaoProjetoQuery, AvaliacaoProjetoQueryVariables> & TChildProps;
export function withAvaliacaoProjeto<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  AvaliacaoProjetoQuery,
  AvaliacaoProjetoQueryVariables,
  AvaliacaoProjetoProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, AvaliacaoProjetoQuery, AvaliacaoProjetoQueryVariables, AvaliacaoProjetoProps<TChildProps>>(AvaliacaoProjetoDocument, {
      alias: 'avaliacaoProjeto',
      ...operationOptions
    });
};

    export function useAvaliacaoProjetoQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<AvaliacaoProjetoQuery, AvaliacaoProjetoQueryVariables>) {
      return ApolloReactHooks.useQuery<AvaliacaoProjetoQuery, AvaliacaoProjetoQueryVariables>(AvaliacaoProjetoDocument, baseOptions);
    }
      export function useAvaliacaoProjetoLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AvaliacaoProjetoQuery, AvaliacaoProjetoQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<AvaliacaoProjetoQuery, AvaliacaoProjetoQueryVariables>(AvaliacaoProjetoDocument, baseOptions);
      }
      
export type AvaliacaoProjetoQueryHookResult = ReturnType<typeof useAvaliacaoProjetoQuery>;
export type AvaliacaoProjetoQueryResult = ApolloReactCommon.QueryResult<AvaliacaoProjetoQuery, AvaliacaoProjetoQueryVariables>;
export const AvaliacoesRealizadasDocument = gql`
    query avaliacoesRealizadas($idPessoa: String!, $idVinculoPessoa: String!) {
  pessoa(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
    idPessoa
    idVinculoPessoa
    ... on Avaliador {
      avaliacoes {
        projeto {
          idProjeto
          tituloProjeto
          mediaIndividual(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
            mediaProjeto
          }
        }
        media
      }
    }
    ... on Gerente {
      avaliacoes {
        projeto {
          idProjeto
          tituloProjeto
          mediaIndividual(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
            mediaProjeto
          }
        }
        media
      }
    }
  }
}
    `;
export type AvaliacoesRealizadasComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<AvaliacoesRealizadasQuery, AvaliacoesRealizadasQueryVariables>, 'query'> & ({ variables: AvaliacoesRealizadasQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const AvaliacoesRealizadasComponent = (props: AvaliacoesRealizadasComponentProps) => (
      <ApolloReactComponents.Query<AvaliacoesRealizadasQuery, AvaliacoesRealizadasQueryVariables> query={AvaliacoesRealizadasDocument} {...props} />
    );
    
export type AvaliacoesRealizadasProps<TChildProps = {}> = ApolloReactHoc.DataProps<AvaliacoesRealizadasQuery, AvaliacoesRealizadasQueryVariables> & TChildProps;
export function withAvaliacoesRealizadas<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  AvaliacoesRealizadasQuery,
  AvaliacoesRealizadasQueryVariables,
  AvaliacoesRealizadasProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, AvaliacoesRealizadasQuery, AvaliacoesRealizadasQueryVariables, AvaliacoesRealizadasProps<TChildProps>>(AvaliacoesRealizadasDocument, {
      alias: 'avaliacoesRealizadas',
      ...operationOptions
    });
};

    export function useAvaliacoesRealizadasQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<AvaliacoesRealizadasQuery, AvaliacoesRealizadasQueryVariables>) {
      return ApolloReactHooks.useQuery<AvaliacoesRealizadasQuery, AvaliacoesRealizadasQueryVariables>(AvaliacoesRealizadasDocument, baseOptions);
    }
      export function useAvaliacoesRealizadasLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AvaliacoesRealizadasQuery, AvaliacoesRealizadasQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<AvaliacoesRealizadasQuery, AvaliacoesRealizadasQueryVariables>(AvaliacoesRealizadasDocument, baseOptions);
      }
      
export type AvaliacoesRealizadasQueryHookResult = ReturnType<typeof useAvaliacoesRealizadasQuery>;
export type AvaliacoesRealizadasQueryResult = ApolloReactCommon.QueryResult<AvaliacoesRealizadasQuery, AvaliacoesRealizadasQueryVariables>;
export const AvaliadoresProjetoDocument = gql`
    query avaliadoresProjeto($idApresentacao: Int!, $id2Apresentacao: String!) {
  projeto(idProjeto: 2860) {
    idProjeto
    tituloProjeto
    disponibilidadeProjeto
  }
  apresentacao(input: {idApresentacao: $idApresentacao, id2Apresentacao: $id2Apresentacao}) {
    idApresentacao
    id2Apresentacao
    avaliadores {
      ...cardAvaliadorFragment
    }
  }
}
    ${CardAvaliadorFragmentFragmentDoc}`;
export type AvaliadoresProjetoComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<AvaliadoresProjetoQuery, AvaliadoresProjetoQueryVariables>, 'query'> & ({ variables: AvaliadoresProjetoQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const AvaliadoresProjetoComponent = (props: AvaliadoresProjetoComponentProps) => (
      <ApolloReactComponents.Query<AvaliadoresProjetoQuery, AvaliadoresProjetoQueryVariables> query={AvaliadoresProjetoDocument} {...props} />
    );
    
export type AvaliadoresProjetoProps<TChildProps = {}> = ApolloReactHoc.DataProps<AvaliadoresProjetoQuery, AvaliadoresProjetoQueryVariables> & TChildProps;
export function withAvaliadoresProjeto<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  AvaliadoresProjetoQuery,
  AvaliadoresProjetoQueryVariables,
  AvaliadoresProjetoProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, AvaliadoresProjetoQuery, AvaliadoresProjetoQueryVariables, AvaliadoresProjetoProps<TChildProps>>(AvaliadoresProjetoDocument, {
      alias: 'avaliadoresProjeto',
      ...operationOptions
    });
};

    export function useAvaliadoresProjetoQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<AvaliadoresProjetoQuery, AvaliadoresProjetoQueryVariables>) {
      return ApolloReactHooks.useQuery<AvaliadoresProjetoQuery, AvaliadoresProjetoQueryVariables>(AvaliadoresProjetoDocument, baseOptions);
    }
      export function useAvaliadoresProjetoLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AvaliadoresProjetoQuery, AvaliadoresProjetoQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<AvaliadoresProjetoQuery, AvaliadoresProjetoQueryVariables>(AvaliadoresProjetoDocument, baseOptions);
      }
      
export type AvaliadoresProjetoQueryHookResult = ReturnType<typeof useAvaliadoresProjetoQuery>;
export type AvaliadoresProjetoQueryResult = ApolloReactCommon.QueryResult<AvaliadoresProjetoQuery, AvaliadoresProjetoQueryVariables>;
export const GerenciaProjetoDocument = gql`
    query gerenciaProjeto($idProjeto: Int!, $idApresentacao: Int!, $id2Apresentacao: String!) {
  projeto(idProjeto: $idProjeto) {
    idProjeto
    tituloProjeto
    disponibilidadeProjeto
    avaliadoresHabilitados {
      idPessoa
      idVinculoPessoa
    }
  }
  apresentacao(input: {idApresentacao: $idApresentacao, id2Apresentacao: $id2Apresentacao}) {
    idApresentacao
    id2Apresentacao
    avaliadores {
      ...cardAvaliadorFragment
      avaliacoes(idProjeto: $idProjeto) {
        projeto {
          idProjeto
        }
        media
      }
    }
  }
}
    ${CardAvaliadorFragmentFragmentDoc}`;
export type GerenciaProjetoComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GerenciaProjetoQuery, GerenciaProjetoQueryVariables>, 'query'> & ({ variables: GerenciaProjetoQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GerenciaProjetoComponent = (props: GerenciaProjetoComponentProps) => (
      <ApolloReactComponents.Query<GerenciaProjetoQuery, GerenciaProjetoQueryVariables> query={GerenciaProjetoDocument} {...props} />
    );
    
export type GerenciaProjetoProps<TChildProps = {}> = ApolloReactHoc.DataProps<GerenciaProjetoQuery, GerenciaProjetoQueryVariables> & TChildProps;
export function withGerenciaProjeto<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  GerenciaProjetoQuery,
  GerenciaProjetoQueryVariables,
  GerenciaProjetoProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, GerenciaProjetoQuery, GerenciaProjetoQueryVariables, GerenciaProjetoProps<TChildProps>>(GerenciaProjetoDocument, {
      alias: 'gerenciaProjeto',
      ...operationOptions
    });
};

    export function useGerenciaProjetoQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GerenciaProjetoQuery, GerenciaProjetoQueryVariables>) {
      return ApolloReactHooks.useQuery<GerenciaProjetoQuery, GerenciaProjetoQueryVariables>(GerenciaProjetoDocument, baseOptions);
    }
      export function useGerenciaProjetoLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GerenciaProjetoQuery, GerenciaProjetoQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<GerenciaProjetoQuery, GerenciaProjetoQueryVariables>(GerenciaProjetoDocument, baseOptions);
      }
      
export type GerenciaProjetoQueryHookResult = ReturnType<typeof useGerenciaProjetoQuery>;
export type GerenciaProjetoQueryResult = ApolloReactCommon.QueryResult<GerenciaProjetoQuery, GerenciaProjetoQueryVariables>;
export const InfoEventoDocument = gql`
    query infoEvento {
  eventos {
    nomeEvento
    dataInicioEvento
    dataFimEvento
    statusEvento
  }
}
    `;
export type InfoEventoComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<InfoEventoQuery, InfoEventoQueryVariables>, 'query'>;

    export const InfoEventoComponent = (props: InfoEventoComponentProps) => (
      <ApolloReactComponents.Query<InfoEventoQuery, InfoEventoQueryVariables> query={InfoEventoDocument} {...props} />
    );
    
export type InfoEventoProps<TChildProps = {}> = ApolloReactHoc.DataProps<InfoEventoQuery, InfoEventoQueryVariables> & TChildProps;
export function withInfoEvento<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  InfoEventoQuery,
  InfoEventoQueryVariables,
  InfoEventoProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, InfoEventoQuery, InfoEventoQueryVariables, InfoEventoProps<TChildProps>>(InfoEventoDocument, {
      alias: 'infoEvento',
      ...operationOptions
    });
};

    export function useInfoEventoQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<InfoEventoQuery, InfoEventoQueryVariables>) {
      return ApolloReactHooks.useQuery<InfoEventoQuery, InfoEventoQueryVariables>(InfoEventoDocument, baseOptions);
    }
      export function useInfoEventoLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<InfoEventoQuery, InfoEventoQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<InfoEventoQuery, InfoEventoQueryVariables>(InfoEventoDocument, baseOptions);
      }
      
export type InfoEventoQueryHookResult = ReturnType<typeof useInfoEventoQuery>;
export type InfoEventoQueryResult = ApolloReactCommon.QueryResult<InfoEventoQuery, InfoEventoQueryVariables>;
export const PessoasApresentacaoDocument = gql`
    query pessoasApresentacao($idApresentacao: Int!, $id2Apresentacao: String!) {
  apresentacao(input: {idApresentacao: $idApresentacao, id2Apresentacao: $id2Apresentacao}) {
    idApresentacao
    id2Apresentacao
    avaliadores {
      ...cardAvaliadorFragment
    }
  }
}
    ${CardAvaliadorFragmentFragmentDoc}`;
export type PessoasApresentacaoComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<PessoasApresentacaoQuery, PessoasApresentacaoQueryVariables>, 'query'> & ({ variables: PessoasApresentacaoQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const PessoasApresentacaoComponent = (props: PessoasApresentacaoComponentProps) => (
      <ApolloReactComponents.Query<PessoasApresentacaoQuery, PessoasApresentacaoQueryVariables> query={PessoasApresentacaoDocument} {...props} />
    );
    
export type PessoasApresentacaoProps<TChildProps = {}> = ApolloReactHoc.DataProps<PessoasApresentacaoQuery, PessoasApresentacaoQueryVariables> & TChildProps;
export function withPessoasApresentacao<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  PessoasApresentacaoQuery,
  PessoasApresentacaoQueryVariables,
  PessoasApresentacaoProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, PessoasApresentacaoQuery, PessoasApresentacaoQueryVariables, PessoasApresentacaoProps<TChildProps>>(PessoasApresentacaoDocument, {
      alias: 'pessoasApresentacao',
      ...operationOptions
    });
};

    export function usePessoasApresentacaoQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<PessoasApresentacaoQuery, PessoasApresentacaoQueryVariables>) {
      return ApolloReactHooks.useQuery<PessoasApresentacaoQuery, PessoasApresentacaoQueryVariables>(PessoasApresentacaoDocument, baseOptions);
    }
      export function usePessoasApresentacaoLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<PessoasApresentacaoQuery, PessoasApresentacaoQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<PessoasApresentacaoQuery, PessoasApresentacaoQueryVariables>(PessoasApresentacaoDocument, baseOptions);
      }
      
export type PessoasApresentacaoQueryHookResult = ReturnType<typeof usePessoasApresentacaoQuery>;
export type PessoasApresentacaoQueryResult = ApolloReactCommon.QueryResult<PessoasApresentacaoQuery, PessoasApresentacaoQueryVariables>;
export const ProjetosApresentacaoDocument = gql`
    query projetosApresentacao($idApresentacao: Int!, $id2Apresentacao: String!) {
  apresentacao(input: {idApresentacao: $idApresentacao, id2Apresentacao: $id2Apresentacao}) {
    idApresentacao
    id2Apresentacao
    projetos {
      idProjeto
      tituloProjeto
      disponibilidadeProjeto
      coordenador {
        nomePessoa
      }
    }
  }
}
    `;
export type ProjetosApresentacaoComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ProjetosApresentacaoQuery, ProjetosApresentacaoQueryVariables>, 'query'> & ({ variables: ProjetosApresentacaoQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const ProjetosApresentacaoComponent = (props: ProjetosApresentacaoComponentProps) => (
      <ApolloReactComponents.Query<ProjetosApresentacaoQuery, ProjetosApresentacaoQueryVariables> query={ProjetosApresentacaoDocument} {...props} />
    );
    
export type ProjetosApresentacaoProps<TChildProps = {}> = ApolloReactHoc.DataProps<ProjetosApresentacaoQuery, ProjetosApresentacaoQueryVariables> & TChildProps;
export function withProjetosApresentacao<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  ProjetosApresentacaoQuery,
  ProjetosApresentacaoQueryVariables,
  ProjetosApresentacaoProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, ProjetosApresentacaoQuery, ProjetosApresentacaoQueryVariables, ProjetosApresentacaoProps<TChildProps>>(ProjetosApresentacaoDocument, {
      alias: 'projetosApresentacao',
      ...operationOptions
    });
};

    export function useProjetosApresentacaoQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ProjetosApresentacaoQuery, ProjetosApresentacaoQueryVariables>) {
      return ApolloReactHooks.useQuery<ProjetosApresentacaoQuery, ProjetosApresentacaoQueryVariables>(ProjetosApresentacaoDocument, baseOptions);
    }
      export function useProjetosApresentacaoLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ProjetosApresentacaoQuery, ProjetosApresentacaoQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<ProjetosApresentacaoQuery, ProjetosApresentacaoQueryVariables>(ProjetosApresentacaoDocument, baseOptions);
      }
      
export type ProjetosApresentacaoQueryHookResult = ReturnType<typeof useProjetosApresentacaoQuery>;
export type ProjetosApresentacaoQueryResult = ApolloReactCommon.QueryResult<ProjetosApresentacaoQuery, ProjetosApresentacaoQueryVariables>;
export const ProjetosPendentesDocument = gql`
    query projetosPendentes($idPessoa: String!, $idVinculoPessoa: String!) {
  pessoa(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
    idPessoa
    idVinculoPessoa
    ... on Gerente {
      projetosParaAvaliar {
        idProjeto
        tituloProjeto
        apresentacao {
          idApresentacao
        }
        categoria {
          idCategoria
        }
        mediaIndividual(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
          mediaProjeto
        }
      }
    }
    ... on Avaliador {
      projetosParaAvaliar {
        idProjeto
        tituloProjeto
        apresentacao {
          idApresentacao
        }
        categoria {
          idCategoria
        }
        mediaIndividual(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
          mediaProjeto
        }
      }
    }
  }
}
    `;
export type ProjetosPendentesComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ProjetosPendentesQuery, ProjetosPendentesQueryVariables>, 'query'> & ({ variables: ProjetosPendentesQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const ProjetosPendentesComponent = (props: ProjetosPendentesComponentProps) => (
      <ApolloReactComponents.Query<ProjetosPendentesQuery, ProjetosPendentesQueryVariables> query={ProjetosPendentesDocument} {...props} />
    );
    
export type ProjetosPendentesProps<TChildProps = {}> = ApolloReactHoc.DataProps<ProjetosPendentesQuery, ProjetosPendentesQueryVariables> & TChildProps;
export function withProjetosPendentes<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  ProjetosPendentesQuery,
  ProjetosPendentesQueryVariables,
  ProjetosPendentesProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, ProjetosPendentesQuery, ProjetosPendentesQueryVariables, ProjetosPendentesProps<TChildProps>>(ProjetosPendentesDocument, {
      alias: 'projetosPendentes',
      ...operationOptions
    });
};

    export function useProjetosPendentesQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ProjetosPendentesQuery, ProjetosPendentesQueryVariables>) {
      return ApolloReactHooks.useQuery<ProjetosPendentesQuery, ProjetosPendentesQueryVariables>(ProjetosPendentesDocument, baseOptions);
    }
      export function useProjetosPendentesLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ProjetosPendentesQuery, ProjetosPendentesQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<ProjetosPendentesQuery, ProjetosPendentesQueryVariables>(ProjetosPendentesDocument, baseOptions);
      }
      
export type ProjetosPendentesQueryHookResult = ReturnType<typeof useProjetosPendentesQuery>;
export type ProjetosPendentesQueryResult = ApolloReactCommon.QueryResult<ProjetosPendentesQuery, ProjetosPendentesQueryVariables>;
export const UsuarioAtualDocument = gql`
    query usuarioAtual {
  usuarioAtual @client {
    __typename
    idPessoa
    idVinculoPessoa
    nomeSocialPessoa
    token
    matriculaPessoa
    lotacaoPessoa
    avatarUrl
    monitorPessoa
    avaliadorPessoa
    gerentePessoa
  }
}
    `;
export type UsuarioAtualComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<UsuarioAtualQuery, UsuarioAtualQueryVariables>, 'query'>;

    export const UsuarioAtualComponent = (props: UsuarioAtualComponentProps) => (
      <ApolloReactComponents.Query<UsuarioAtualQuery, UsuarioAtualQueryVariables> query={UsuarioAtualDocument} {...props} />
    );
    
export type UsuarioAtualProps<TChildProps = {}> = ApolloReactHoc.DataProps<UsuarioAtualQuery, UsuarioAtualQueryVariables> & TChildProps;
export function withUsuarioAtual<TProps, TChildProps = {}>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  UsuarioAtualQuery,
  UsuarioAtualQueryVariables,
  UsuarioAtualProps<TChildProps>>) {
    return ApolloReactHoc.withQuery<TProps, UsuarioAtualQuery, UsuarioAtualQueryVariables, UsuarioAtualProps<TChildProps>>(UsuarioAtualDocument, {
      alias: 'usuarioAtual',
      ...operationOptions
    });
};

    export function useUsuarioAtualQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<UsuarioAtualQuery, UsuarioAtualQueryVariables>) {
      return ApolloReactHooks.useQuery<UsuarioAtualQuery, UsuarioAtualQueryVariables>(UsuarioAtualDocument, baseOptions);
    }
      export function useUsuarioAtualLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<UsuarioAtualQuery, UsuarioAtualQueryVariables>) {
        return ApolloReactHooks.useLazyQuery<UsuarioAtualQuery, UsuarioAtualQueryVariables>(UsuarioAtualDocument, baseOptions);
      }
      
export type UsuarioAtualQueryHookResult = ReturnType<typeof useUsuarioAtualQuery>;
export type UsuarioAtualQueryResult = ApolloReactCommon.QueryResult<UsuarioAtualQuery, UsuarioAtualQueryVariables>;