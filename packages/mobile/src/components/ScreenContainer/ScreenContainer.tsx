import * as React from "react"
import { StyleSheet, ScrollView, View, ViewStyle } from "react-native"
import PlainHeader from "components/Header/PlainHeader"
import Colors from "assets/css/colors"

interface ScreenContainerProps {
  whiteBackground?: boolean,
  contentContainerStyle?: ViewStyle
}

const ScreenContainer: React.SFC<ScreenContainerProps> = props => {
  return (
    <ScrollView
      keyboardShouldPersistTaps="handled"
      contentContainerStyle={[
        styles.contentContainer,
        {
          backgroundColor: props.whiteBackground ? "white" : Colors.background
        },
        props.contentContainerStyle
      ]}
      style={{
        flex: 1,
        backgroundColor: props.whiteBackground ? "white" : Colors.background
      }}
    >
      {props.children}
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  contentContainer: {
    backgroundColor: Colors.background,
    paddingHorizontal: 14,
    flexGrow: 1
  }
})

export default ScreenContainer
