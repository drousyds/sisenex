import * as React from "react"
import {
  View,
  TouchableNativeFeedback,
  TouchableOpacity,
  Platform,
  Text,
  StyleSheet,
  ViewStyle,
  ActivityIndicator,
  Dimensions
} from "react-native"
import Colors from "assets/css/colors"
import Typography from "assets/css/typography"

interface ClearButtonProps {
  title: string
  onPress: () => void
  loading?: boolean
  style?: ViewStyle
  secondary?: boolean
  large?: boolean
}

const ClearButton: React.SFC<ClearButtonProps> = props => {
  if (Platform.OS == "android")
    return (
      <TouchableNativeFeedback onPress={props.onPress}>
        <View style={[styles.container, props.style]}>
          {props.loading ? (
            <ActivityIndicator color="#fefefe" />
          ) : (
            <Text style={styles.buttonText}>{props.title}</Text>
          )}
        </View>
      </TouchableNativeFeedback>
    )
  else
    return (
      <TouchableOpacity onPress={props.onPress}>
        <View style={[styles.container, props.style]}>
          <Text style={styles.buttonText}>{props.title}</Text>
        </View>
      </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    width: Dimensions.get("window").width,
    borderBottomWidth: 2,
    borderColor: "#eeeeee",
    marginHorizontal: -14,
    paddingVertical: 22
  },
  buttonText: {
    ...Typography.button,
    textAlign: "center",
    color: Colors.primary,
    fontWeight: "300"
  }
})

export default ClearButton
