import React from "react"
import {
  View,
  TouchableOpacity,
  ActivityIndicator,
  Text,
  TextStyle,
  ViewStyle,
  StyleSheet,
  StyleProp
} from "react-native"
import Colors from "assets/css/colors"

export interface ButtonProps {
  title: string
  hitSlop?: { top: number; left: number; bottom: number; right: number }
  loading?: boolean
  onPress(): void
  containerStyle?: ViewStyle
  buttonTextStyle?: TextStyle
}

interface Styles {
  containerStyle: ViewStyle
  buttonTextStyle: TextStyle
}

const Button = ({
  title,
  onPress,
  hitSlop,
  loading,
  containerStyle,
  buttonTextStyle
}: ButtonProps) => {
  return (
    <View>
      {loading && (
        <ActivityIndicator color={Colors.primary} style={{ padding: 8 }} />
      )}
      {!loading && (
        <TouchableOpacity
          hitSlop={hitSlop}
          style={[defaultProps.containerStyle, containerStyle]}
          onPress={() => requestAnimationFrame(onPress)}
        >
          <Text style={[defaultProps.buttonTextStyle, buttonTextStyle]}>
            {title}
          </Text>
        </TouchableOpacity>
      )}
    </View>
  )
}

const defaultProps = StyleSheet.create<Styles>({
  containerStyle: {
    backgroundColor: "#35457e",
    borderRadius: 6,
    height: null,
    width: null,
    marginTop: 20,
    marginLeft: 0,
    marginRight: 0,
    alignSelf: "flex-end",
    justifyContent: "center"
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    alignSelf: "center",
    justifyContent: "center",
    fontSize: 15
  }
})

export default Button
