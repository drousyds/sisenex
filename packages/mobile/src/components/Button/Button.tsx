import * as React from "react"
import {
  View,
  TouchableNativeFeedback,
  TouchableOpacity,
  Platform,
  Text,
  StyleSheet,
  ViewStyle,
  ActivityIndicator
} from "react-native"
import Colors from "assets/css/colors"
import Typography from "assets/css/typography"

interface PrimaryButtonProps {
  title: string
  onPress?: () => void
  loading?: boolean
  grey?: boolean
  style?: ViewStyle
  secondary?: boolean
  large?: boolean
  disabled?: boolean
}

const PrimaryButton: React.SFC<PrimaryButtonProps> = props => {
  const onButtonPress = props.disabled ? undefined : props.onPress
  const backgroundColor =
    props.disabled || props.grey
      ? Colors.greyText
      : props.secondary
        ? "#A50100"
        : Colors.primary

  if (Platform.OS == "android")
    return (
      <TouchableNativeFeedback onPress={onButtonPress}>
        <View
          style={[
            styles.container,
            {
              width: props.large ? 228 : 144,
              backgroundColor
            },
            props.style
          ]}
        >
          {props.loading ? (
            <ActivityIndicator color="#fefefe" />
          ) : (
              <Text style={styles.buttonText}>{props.title}</Text>
            )}
        </View>
      </TouchableNativeFeedback>
    )
  else
    return (
      <TouchableOpacity onPress={onButtonPress}>
        <View
          style={[
            styles.container,
            {
              width: props.large ? 228 : 144,
              backgroundColor
            },
            props.style
          ]}
        >
          <Text style={styles.buttonText}>{props.title}</Text>
        </View>
      </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    paddingVertical: 10
  },
  buttonText: {
    ...Typography.button,
    textAlign: "center"
  }
})

export default PrimaryButton
