import React, { Children } from "react"
import {
  Modal,
  Platform,
  KeyboardAvoidingView,
  StyleSheet,
  ViewStyle
} from "react-native"
import { BlurView } from "expo-blur"
import { ScrollView } from "react-native-gesture-handler"
import { RoundedCard } from "components/Cards"
import { useDispatch } from "react-redux"
import { hideVinculoSelection } from "redux/Auth/actions"

export interface DialogProps {
  visible?: boolean
  onShow?: () => void
  cardContainerStyle?: ViewStyle
  onRequestClose?: () => void
}

export const Dialog: React.SFC<DialogProps> = ({
  visible,
  onShow,
  children,
  cardContainerStyle,
  onRequestClose
}) => {
  const mergedCardContainerStyle = {
    backgroundColor: "white",
    padding: 16,
    ...cardContainerStyle
  }

  return (
    <Modal
      visible={visible}
      transparent
      onShow={onShow}
      onRequestClose={onRequestClose}
    >
      <BlurView
        tint={Platform.OS == "ios" ? "light" : "dark"}
        intensity={95}
        style={{
          ...StyleSheet.absoluteFillObject,
          paddingHorizontal: 12,
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column"
        }}
      >
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: "center"
          }}
        >
          <KeyboardAvoidingView
            behavior="padding"
            contentContainerStyle={{ paddingBottom: 12 }}
          >
            <RoundedCard style={mergedCardContainerStyle}>
              {children}
            </RoundedCard>
          </KeyboardAvoidingView>
        </ScrollView>
      </BlurView>
    </Modal>
  )
}

export default Dialog
