import React from "react"
import { View } from "react-native"
// @ts-ignore
import {
  selectVinculo,
  apiLogin,
  hideVinculoSelection,
  submitVinculoSelection
} from "redux/Auth/actions"
import { connect, useSelector, useDispatch } from "react-redux"
import { Pessoa } from "generated/graphql"
import { RootState } from "redux/rootReducer"
import { Dispatch } from "redux"
import Dialog from "./Dialog"
import { CheckBox } from '@rneui/themed';
import { getVinculoSelectionData } from "redux/Auth/selectors"
import { CardFooter } from "components/Cards/RoundedCard"
import Separator from "components/Separator/Separator"
import { Button } from "components/Button"

export interface RadioDialogWrapperProps {
  vinculos: Pessoa[]
  selectedVinculo: number
  visible: boolean
  dispatch: Dispatch
  pickVinculo(vinculo: number): () => any
  apiLogin(selectedVinculo: Pessoa): () => any
}

const RadioDialogWrapper = ({
  pickVinculo,
  apiLogin
}: RadioDialogWrapperProps) => {
  const dispatch = useDispatch()
  const hideModal = () => dispatch(hideVinculoSelection())
  const select = (index: number) => () => dispatch(selectVinculo(index))
  const submitSelection = () => dispatch(submitVinculoSelection())
  const { vinculos, selectedVinculo, visible } = useSelector(
    getVinculoSelectionData
  )

  return (
    <Dialog
      visible={visible}
      onRequestClose={hideModal}
      cardContainerStyle={{ marginHorizontal: 30 }}
    >
      {vinculos.map((elem, index) => {
        return (
          <CheckBox
            key={elem.idVinculoPessoa}
            onPress={select(index)}
            title={`${
              elem.avaliadorPessoa == 1 || elem.gerentePessoa == 1
                ? "Avaliador"
                : "Monitor"
            } ${elem.matriculaPessoa} ${elem.lotacaoPessoa || ""}`}
            checkedIcon="dot-circle-o"
            containerStyle={{
              backgroundColor: "white",
              borderWidth: 0
            }}
            uncheckedIcon="circle-o"
            checked={index == selectedVinculo}
          />
        )
      })}
      <CardFooter style={{ justifyContent: "center" }}>
        <View
          style={{
            display: "flex",
            flex: 1,
            flexDirection: "row",
            justifyContent: "center",
            paddingBottom: 8,
            paddingTop: 24
          }}
        >
          <Button
            title="Cancelar"
            style={{ flex: 1, maxWidth: 122 }}
            secondary
            onPress={hideModal}
          />
          <Separator size={12} />
          <Button
            style={{ flex: 1, maxWidth: 122 }}
            title="Confirmar"
            onPress={submitSelection}
          />
        </View>
      </CardFooter>
    </Dialog>
  )
}

const mapStateToProps = (state: RootState) => ({
  vinculos: state.auth.vinculoSelection.vinculos,
  selectedVinculo: state.auth.vinculoSelection.selectedVinculo,
  visible: state.auth.vinculoSelection.visible
})

const mapDispathToProps = (dispatch: Dispatch) => ({
  dispatch,
  pickVinculo: (vinculo: number) => () => dispatch(selectVinculo(vinculo)),
  apiLogin: (vinculo: Pessoa) => () => dispatch(apiLogin.request(vinculo))
})

export default connect(
  mapStateToProps,
  mapDispathToProps
)(RadioDialogWrapper)
