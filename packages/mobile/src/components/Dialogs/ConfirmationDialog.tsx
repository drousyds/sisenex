import React from "react"
import { Dialog } from "."
import {
  CardFooter,
  CardTitle,
  CardFooterContent
} from "components/Cards/RoundedCard"
import { Button } from "components/Button"
import Separator from "components/Separator/Separator"
import { View } from "react-native"

export interface ConfirmationDialogProps {
  visible?: boolean
  message: string
  onConfirm: () => void
  onCancel?: () => void
}

export const ConfirmationDialog: React.SFC<ConfirmationDialogProps> = ({
  visible,
  message,
  onConfirm,
  onCancel
}) => {
  return (
    <Dialog
      visible={visible}
      cardContainerStyle={{
        minHeight: 0,
        paddingVertical: 35
      }}
    >
      <CardTitle>{message}</CardTitle>
      <Separator vertical size={25} />
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
          alignSelf: "center"
        }}
      >
        {onCancel ? (
          <React.Fragment>
            <Button onPress={onCancel} secondary title="Cancelar" />
            <Separator />
            <Button onPress={onConfirm} title="Confirmar" />
          </React.Fragment>
        ) : (
          <Button onPress={onConfirm} title="Ok" />
        )}
      </View>
    </Dialog>
  )
}
