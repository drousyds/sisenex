import {
  View,
  Text,
  Modal,
  StyleSheet,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  ViewStyle
} from "react-native"
// @ts-ignore
// import { Dialog, DialogDefaultActions } from "react-native-material-ui"
// import React, { ReactNode, Component } from "react"

export interface MaterialDialogProps {
  toggle(): void
  actionName?: string
  onActionPress(): void
  visible: boolean
  content?: React.ReactNode
  headerText?: string
  options?: {
    actionName: boolean
  }
  hideCancel?: boolean
}

// interface Styles {
//   modalScreenStyle: ViewStyle
// }

// const MaterialDialog = ({
//   toggle,
//   actionName,
//   onActionPress,
//   visible,
//   content,
//   headerText,
//   options,
//   hideCancel
// }: MaterialDialogProps) => {
//   let handleActionPress = (actionName: string) => {
//     if (actionName == "cancelar") toggle()
//     else onActionPress()
//   }

//   return (
//     <Modal
//       visible={visible}
//       transparent={true}
//       animationType="fade"
//       onRequestClose={toggle}
//     >
//       <TouchableWithoutFeedback>
//         <KeyboardAvoidingView
//           style={styles.modalScreenStyle}
//           behavior="padding"
//         >
//           <Dialog>
//             <Dialog.Title>
//               <Text>{headerText}</Text>
//             </Dialog.Title>
//             <Dialog.Content>{content}</Dialog.Content>
//             <Dialog.Actions>
//               <DialogDefaultActions
//                 actions={hideCancel ? [actionName] : ["cancelar", actionName]}
//                 options={options}
//                 onActionPress={handleActionPress}
//               />
//             </Dialog.Actions>
//           </Dialog>
//         </KeyboardAvoidingView>
//       </TouchableWithoutFeedback>
//     </Modal>
//   )
// }

// const styles = StyleSheet.create<Styles>({
//   modalScreenStyle: {
//     backgroundColor: "rgba(0,0,0,0.80)",
//     display: "flex",
//     justifyContent: "center",
//     alignItems: "center",
//     flex: 1
//   }
// })

// export default MaterialDialog
// export { MaterialDialog }
