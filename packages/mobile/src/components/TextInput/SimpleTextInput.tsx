import React from "react"
import { Text, View, KeyboardType, TextInput } from "react-native"
import { TextInputMask } from "react-native-masked-text"
import Typography from "assets/css/typography"
import { useRef, useState, useEffect } from "react"
import Colors from "assets/css/colors"

export interface SimpleTextInputProps {
  onSubmitEditing: (text: string) => void
  keyboardType?: KeyboardType
  errorState?: boolean
  onChangeText?: (text: string) => void
  validator?: (text: string) => boolean
  inputRef?: React.Ref<any>
  focusOnMount?: boolean
  label: string
}

export const SimpleTextInput: React.SFC<SimpleTextInputProps> = props => {
  const inputRef = useRef<TextInputMask>(null)
  const [focused, setFocused] = useState(false)
  const [text, setText] = useState("")
  const onFocus = () => setFocused(true)
  const onBlur = () => setFocused(false)
  useEffect(() => {
    if (props.focusOnMount && inputRef.current)
      //@ts-ignore
      inputRef.current.focus()
  }, [])

  return (
    <View
      style={{
        marginHorizontal: 8
      }}
    >
      <Text style={[Typography.caption, { paddingBottom: 8 }]}>
        {props.label}
      </Text>
      <TextInput
        ref={props.inputRef || inputRef}
        onFocus={onFocus}
        onBlur={onBlur}
        keyboardType={props.keyboardType || "default"}
        onSubmitEditing={() => props.onSubmitEditing(text)}
        value={text}
        onChangeText={text => {
          props.onChangeText && props.onChangeText(text)
          setText(text)
        }}
        style={{
          borderStyle: "solid",
          borderColor: props.errorState
            ? Colors.errorHighlight
            : focused
            ? Colors.primary
            : Colors.greyText,
          borderWidth: 2,
          borderRadius: 8,
          textAlign: "center",
          paddingVertical: 12,
          fontSize: 32,
          color: "#56625A"
        }}
      />
    </View>
  )
}

export default TextInput
