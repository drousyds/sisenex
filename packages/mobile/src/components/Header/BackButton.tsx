import * as React from "react"
import { View } from "react-native"
import { MaterialIcons } from "@expo/vector-icons"
import { NavigationScreenProp } from "react-navigation"
import { useNavigation } from "@react-navigation/native"

interface BackButtonProps {
  //navigation: NavigationScreenProp<any> é necessário tipar isso?
  onPress?: () => void
}

const BackButton: React.SFC<BackButtonProps> = props => {
  const navigation = useNavigation()
  return (
    <View
      style={{
        flexDirection: "row",
        width: "100%",
        paddingTop: 14,
        paddingBottom: 0,
        justifyContent: "flex-start"
      }}
    >
      <MaterialIcons
        name="arrow-back"
        color="black"
        size={25}
        onPress={props.onPress || (() => navigation.goBack())}
      />
    </View>
  )
}

export default BackButton
