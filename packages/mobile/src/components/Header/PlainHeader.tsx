import * as React from "react"
import { View, StyleSheet, Dimensions, Platform } from "react-native"
import Colors from "assets/css/colors"

interface PlainHeaderProps {}

const PlainHeader: React.FC<PlainHeaderProps> = props => {
  return <View style={styles.headerStyle} />
}

const styles = StyleSheet.create({
  headerStyle: {
    height: Platform.OS == "ios" ? 45 : 0,
    width: Dimensions.get("screen").width,
    backgroundColor: Colors.primary,
    position: Platform.OS == "ios" ? "absolute" : "relative",
    top: 0
  }
})

export default PlainHeader
