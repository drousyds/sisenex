import React, { Children } from "react"
import Placeholder from "rn-placeholder"
import {
  View,
  StyleSheet,
  ViewStyle,
  Text,
  TextStyle,
  ImageURISource,
  Image,
  ImageSourcePropType,
  Dimensions,
  PixelRatio
} from "react-native"
import { material } from "react-native-typography"
import Typography from "assets/css/typography"
import Separator from "components/Separator/Separator"

const FONT_SCALE = PixelRatio.getFontScale()

export interface RoundedCardProps {
  style?: ViewStyle
  placeholder?: boolean
  anyHeight?: boolean
}

const RoundedCard: React.SFC<RoundedCardProps> = props => {
  return props.placeholder ? (
    <PlaceholderCard />
  ) : (
    <View
      style={[
        styles.cardContainer,
        props.style,
        props.anyHeight && {
          minHeight: undefined
        }
      ]}
    >
      {props.children}
    </View>
  )
}

const CardTitle: React.SFC<{
  style?: ViewStyle
  textStyle?: TextStyle
  fullText?: boolean
  subtitle?: React.ReactNode
  element?: boolean
}> = props => {
  const numberOfLines = props.fullText ? undefined : 6
  return (
    <View style={[styles.titleContainer, props.style]}>
      {props.element ? (
        props.children
      ) : (
        <Text
          numberOfLines={numberOfLines}
          style={[Typography.header2, props.textStyle]}
        >
          {props.children}
        </Text>
      )}
    </View>
  )
}

const CardTitleContainer: React.SFC<{
  imageSource?: ImageSourcePropType
}> = props => {
  return (
    <View style={{ flexDirection: "row" }}>
      {props.imageSource && (
        <View
          style={{
            width: 48,
            height: 60,
            top: 8,
            marginRight: 12,
            borderRadius: 12
          }}
        >
          <Image
            source={props.imageSource}
            style={{
              width: 48,
              borderRadius: 12,
              height: 60
            }}
          />
        </View>
      )}
      <View
        style={{
          paddingBottom: 16
        }}
      >
        {props.children}
      </View>
    </View>
  )
}

const CardSubtitle: React.SFC<{ style?: ViewStyle }> = props => {
  return (
    <Text
      style={[
        {
          ...Typography.subtitle,
          fontSize: 15,
          lineHeight: 18,
          color: "#A0A0A0",
          fontWeight: "100"
        },
        props.style
      ]}
    >
      {props.children}
    </Text>
  )
}

const CardBody: React.SFC<{
  textStyle?: TextStyle
  style?: ViewStyle
}> = props => {
  return (
    <View style={[styles.bodyContainer, props.style]}>
      <Text
        style={[
          material.caption,
          {
            textAlign: "center",
            lineHeight: 18,
            fontSize: 15
          },
          props.textStyle
        ]}
      >
        {props.children}
      </Text>
    </View>
  )
}

const CardFooter: React.SFC<{
  style?: ViewStyle
  responsive?: Boolean
}> = props => {
  return (
    <View
      style={[
        styles.footerContainer,
        { justifyContent: props.responsive ? "space-around" : "center" },
        props.style
      ]}
    >
      {props.children}
    </View>
  )
}

export const CardFooterContent: React.SFC<{
  style?: ViewStyle
  responsive?: Boolean
}> = props => {
  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: 144,
        alignSelf: "center"
      }}
    >
      {props.children}
    </View>
  )
}

export const PlaceholderCard: React.SFC<{
  style?: ViewStyle
  small?: boolean
}> = props => {
  return (
    <React.Fragment>
      <View style={[styles.cardContainer, { minHeight: 200 }, props.style]}>
        <View style={styles.titleContainer}>
          <Placeholder.Paragraph
            lineNumber={3}
            width="80%"
            firstLineWidth="95%"
            lastLineWidth="95%"
            animate="fade"
            color="#cccccc"
          />
        </View>
        {!props.small && (
          <View style={styles.bodyContainer}>
            <Placeholder.Line animate="fade" color="#cccccc" width="100%" />
          </View>
        )}
        <View
          style={[
            styles.footerContainer,
            {
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center"
            }
          ]}
        >
          <Placeholder.Box
            animate="fade"
            color="#cccccc"
            radius={8}
            width={220}
            height={30}
          />
        </View>
      </View>
      <Separator vertical />
    </React.Fragment>
  )
}

const styles = StyleSheet.create({
  titleContainer: {
    flex: 0,
    paddingBottom: 4,
    backgroundColor: "transparent",
    flexShrink: 1
  },
  footerContainer: {
    padding: 0,
    margin: 0,
    flex: 0,
    paddingTop: 4,
    flexDirection: "row",
    alignContent: "space-between",
    flexWrap: "wrap",
    backgroundColor: "white",
    justifyContent: "flex-end"
  },
  bodyContainer: {
    justifyContent: "center",
    backgroundColor: "white",
    flexGrow: 1,
    flex: 1
  },
  cardContainer: {
    minHeight: 200,
    display: "flex",
    backgroundColor: "white",
    borderRadius: 10,
    paddingHorizontal: 22,
    paddingVertical: 18,
    justifyContent: "center",
    elevation: 2,
    shadowColor: "#518091",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 16,
    shadowOpacity: 0.2
  }
})

export { CardTitle, CardFooter, CardBody, CardSubtitle, CardTitleContainer }

export default RoundedCard
