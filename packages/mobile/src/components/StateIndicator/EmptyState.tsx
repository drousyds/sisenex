import React from "react"
import { View, Image } from "react-native"

const EmptyState: React.FC<{}> = props => {
  return (
    <View
      style={{
        flexGrow: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <Image
        style={{
          width: 200,
          height: 200,
          opacity: 0.05
        }}
        resizeMode="contain"
        source={require("assets/sisenexEmpty.png")}
      />
    </View>
  )
}

export default EmptyState
