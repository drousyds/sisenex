import * as React from "react"
import RoundedCard, { PlaceholderCard } from "components/Cards/RoundedCard"
import Separator from "components/Separator/Separator"

const dummyArray = new Array(8).fill(0)

export const LoadingState = () => {
  return (
    <React.Fragment>
      {dummyArray.map((_, index) => (
        <RoundedCard key={index} placeholder />
      ))}
    </React.Fragment>
  )
}

export const SmallLoadingState = () => {
  return (
    <React.Fragment>
      {dummyArray.map((_, index) => (
        <React.Fragment key={index}>
          <PlaceholderCard style={{ minHeight: 155 }} small />
          <Separator vertical size={6} />
        </React.Fragment>
      ))}
    </React.Fragment>
  )
}
