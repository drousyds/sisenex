import * as React from "react"
import { StyleSheet, Text, View, ViewStyle } from "react-native"
import { human } from "react-native-typography"
import Colors from "assets/css/colors"
import Typography from "assets/css/typography"

interface ScreenContainerProps {
  title: string
  centerSubtitle?: boolean
  subtitle?: string
  style?: ViewStyle
}

const ScreenTitle: React.FC<ScreenContainerProps> = ({ title, centerSubtitle, subtitle, style }) => {
  return (
    <View style={{
      ...styles.titleContainer,
      ...style
    }}>
      <Text style={Typography.header}>{title}</Text>
      {subtitle && (
        <Text
          style={
            {
              ...Typography.subtitle,
              paddingVertical: 8,
              textAlign: centerSubtitle ? "center" : undefined,
              color: centerSubtitle ? Colors.primaryText : undefined,
              fontWeight: centerSubtitle ? "bold" : undefined,
              fontSize: 16
            }
          }
        >
          {subtitle}
        </Text>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  subtitle: {
    ...Typography.subtitle,
    paddingVertical: 8
  },
  titleContainer: {
    paddingTop: 30,
    paddingBottom: 16
  }
})

export default ScreenTitle
