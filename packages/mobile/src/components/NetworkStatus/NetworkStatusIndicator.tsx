import * as React from "react"
import { View, Text } from "react-native"
import NetInfo from "@react-native-community/netinfo"
import Colors from "assets/css/colors"
import { store } from "redux/store"
import { notifyNetworkChange } from "redux/NetworkStatus/actions"
import { useSelector } from "react-redux"
import { getNetworkStatus } from "redux/NetworkStatus/selectors"
import { NetworkStatusState } from "redux/NetworkStatus/reducers"

interface NetworkStatusIndicatorProps {}

const NetworkStatusIndicator: React.SFC<
  NetworkStatusIndicatorProps
> = props => {
  React.useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      store.dispatch(notifyNetworkChange(state.isConnected))
    })
    return () => {
      unsubscribe()
    }
  }, [])

  const state: NetworkStatusState = useSelector(getNetworkStatus)

  return (
    <View
      style={{
        backgroundColor: state.status == "good" ? Colors.success : "#E53935",
        height: state.visible ? 16 : 0
      }}
    >
      <Text
        style={{
          lineHeight: 16,
          fontSize: 14,
          color: "white",
          textAlign: "center"
        }}
      >
        {state.status == "bad"
          ? "Você está offline!"
          : "Reconectado com sucesso"}
      </Text>
    </View>
  )
}

export default NetworkStatusIndicator
