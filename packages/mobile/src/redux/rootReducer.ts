import { combineReducers } from "redux"
import {
  sessionReducer,
  loginStatusReducer,
  userReducer,
  vinculoSelectionDialogReducer
} from "./Auth/reducers"
import { navigationReducer } from "./Navigation/reducers"
import { StateType } from "typesafe-actions"
import { networkStatusReducer } from "./NetworkStatus/reducers"

const RootReducer = combineReducers({
  auth: combineReducers({
    session: sessionReducer,
    loginStatus: loginStatusReducer,
    user: userReducer,
    vinculoSelection: vinculoSelectionDialogReducer
  }),
  navigation: navigationReducer,
  networkStatus: networkStatusReducer
})

export type RootState = StateType<typeof RootReducer>

export default RootReducer
