import { createStore, applyMiddleware } from "redux"
import createSagaMiddleware from "redux-saga"
import { composeWithDevTools } from "redux-devtools-extension"
import "regenerator-runtime/runtime"
import rootSaga from "./rootSaga"
import rootReducer from "./rootReducer"
// @ts-ignore
import { createLogger } from "redux-logger"
import { persistStore, persistReducer } from "redux-persist"
import storage from "redux-persist/lib/storage"
import AsyncStorage from "@react-native-async-storage/async-storage"

const persistConfig = {
  key: "auth",
  storage: AsyncStorage
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const logger = createLogger({
  level: {
    prevState: false,
    action: (action: any) => console.log(action),
    nextState: false
  }
})

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware()
  //var middlewares = [logger, sagaMiddleware]
  var middlewares = [sagaMiddleware]
  var store = createStore(
    persistedReducer,
    composeWithDevTools(applyMiddleware(...middlewares))
  )
  var persistor = persistStore(store)

  sagaMiddleware.run(rootSaga)

  // @ts-ignore
  if (module.hot) {
    // @ts-ignore
    module.hot.accept(() => {
      const nextrootReducer = require("./rootReducer").default
      store.replaceReducer(nextrootReducer)
    })
  }

  return { store, persistor }
}

export default configureStore
