import { RootState } from "./rootReducer"

const initialState: RootState = {
  auth: {
    session: {
      reviewerCode: "11-2434",
      currentApresentacao: {
        idApresentacao: 1,
        id2Apresentacao: "2018"
      }
    },
    loginStatus: {
      loggedIn: true,
      loading: false
    },
    user: {
      idPessoa: "11",
      idVinculoPessoa: "2423",
      matriculaPessoa: "7305002061710",
      nomePessoa: "Claiborne Crockley",
      nomeSocialPessoa: "Raimundo Greeve",
      lotacaoPessoa: "CCA - DEPARTAMENTO DE CONTABILIDADE E FINANÇAS",
      emailPessoa: "rgreevea@accuweather.com",
      telefonePessoa: "(863) 4443108",
      aptidaoPessoa: 1,
      avaliadorPessoa: 0,
      monitorPessoa: 1,
      gerentePessoa: 1
    },
    vinculoSelection: {
      visible: false,
      selectedVinculo: 0,
      vinculos: [
        {
          idPessoa: "11",
          idVinculoPessoa: "2423",
          matriculaPessoa: "7305002061710",
          nomePessoa: "Claiborne Crockley",
          nomeSocialPessoa: "Raimundo Greeve",
          lotacaoPessoa: "CCA - DEPARTAMENTO DE CONTABILIDADE E FINANÇAS",
          emailPessoa: "rgreevea@accuweather.com",
          telefonePessoa: "(863) 4443108",
          aptidaoPessoa: 1,
          avaliadorPessoa: 0,
          monitorPessoa: 1,
          gerentePessoa: 1
        },
        {
          idPessoa: "10",
          idVinculoPessoa: "2129",
          matriculaPessoa: "1039567343118",
          nomePessoa: "Rheba Johantges",
          nomeSocialPessoa: "Truman Cossar",
          lotacaoPessoa:
            "HOSP - HULW SERVIÇO DA ATENDIMENTO ESPECIAL MATERNO INFANTIL (SAE/MI)",
          emailPessoa: "tcossar9@bandcamp.com",
          telefonePessoa: "(290) 5417286",
          aptidaoPessoa: 0,
          avaliadorPessoa: 1,
          monitorPessoa: 0,
          gerentePessoa: 1
        }
      ]
    }
  },
  toast: {
    visible: false,
    type: "SUCCESS",
    message: ""
  },
  navigation: {
    routeName: "LoginScreen"
  },
  pendingProjects: {
    showWireframes: false,
    data: []
  },
  review: {
    data: [
      {
        idProjeto: 1,
        tituloProjeto: "Projeto legal",
        avaliacoes: []
      }
    ],
    submitting: false,
    submittingAbscence: false,
    loading: false
  },
  questions: [
    {
      idPergunta: 1,
      conteudoPergunta:
        "Apresentou domínio do conteúdo considerando as questões formuladas pelos avaliadores e os pares",
      nomeCategoria: "Quanto a apresentação do Resumo/participação no Projeto"
    },
    {
      idPergunta: 2,
      conteudoPergunta:
        "Demonstrou objetividade, sequência e clareza na apresentação.",
      nomeCategoria: "Quanto a apresentação do Resumo/participação no Projeto"
    },
    {
      idPergunta: 3,
      conteudoPergunta:
        "Explicitou a contribuição da experiência de extensão na sua formação acadêmico-profissional e cidadã.",
      nomeCategoria: "Quanto a apresentação do Resumo/participação no Projeto"
    },
    {
      idPergunta: 4,
      conteudoPergunta:
        "Os resumos apresentados possuem relação com a temática do projeto.",
      nomeCategoria: "Quanto a apresentação do Resumo/participação no Projeto"
    },
    {
      idPergunta: 5,
      conteudoPergunta:
        "Evidedenciou articulação do Projeto com o ensino e a pesquisa da  UFPB",
      nomeCategoria: "Quanto ao engajamento no Projeto"
    },
    {
      idPergunta: 6,
      conteudoPergunta:
        "Explanou a coerência e adequação entre os objetivos propostos pelo Projeto e resultados obtidos/esperados. ",
      nomeCategoria: "Quanto ao engajamento no Projeto"
    },
    {
      idPergunta: 7,
      conteudoPergunta:
        "Demonstrou sua visão quanto ao impacto social do projeto.",
      nomeCategoria: "Quanto ao engajamento no Projeto"
    },
    {
      idPergunta: 8,
      conteudoPergunta:
        "Expôs o empoderamento da comunidade público-alvo acerca dos objetivos propostos no projeto.",
      nomeCategoria: "Quanto ao engajamento no Projeto"
    }
  ]
}

export default initialState
