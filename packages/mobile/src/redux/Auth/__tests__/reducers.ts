import {
  apiLogin,
  oauthLogin,
  logout,
  mockMonitorLogin,
  mockReviewerLogin,
  selectVinculo,
  submitVinculoSelection
} from "../actions"
import {
  loginStatusReducer,
  sessionReducer,
  UserState,
  userReducer,
  VinculoSelectionDialogState,
  vinculoSelectionDialogReducer
} from "../reducers"
import { Pessoa } from "generated/graphql"

const mockPerson: Pessoa = {
  idPessoa: "2000",
  idVinculoPessoa: "2000",
  matriculaPessoa: "7305002061710",
  nomePessoa: "Claiborne Crockley",
  nomeSocialPessoa: "Raimundo Greeve",
  lotacaoPessoa: "CCA - DEPARTAMENTO DE CONTABILIDADE E FINANÇAS",
  emailPessoa: "rgreevea@accuweather.com",
  telefonePessoa: "(863) 4443108",
  aptidaoPessoa: 1,
  avaliadorPessoa: 0,
  monitorPessoa: 1,
  gerentePessoa: 1
}

describe("LoginStatusReducer works", () => {
  let initialState
  let expected
  let action
  let actions

  it("Sets loading on login request", () => {
    initialState = {
      loading: false,
      loggedIn: false
    }
    expected = {
      loading: true,
      loggedIn: false
    }
    actions = [apiLogin.request(mockPerson), oauthLogin.request()]
    expect(loginStatusReducer(initialState, actions[0])).toEqual(expected)
    expect(loginStatusReducer(initialState, actions[1])).toEqual(expected)
  })

  it("Sets logged in on success", () => {
    initialState = {
      loading: false,
      loggedIn: false
    }
    expected = {
      loading: false,
      loggedIn: true
    }
    actions = [
      mockMonitorLogin(),
      mockReviewerLogin(),
      apiLogin.success(mockPerson)
    ]
    expect(loginStatusReducer(initialState, actions[0])).toEqual(expected)
    expect(loginStatusReducer(initialState, actions[1])).toEqual(expected)
    expect(loginStatusReducer(initialState, actions[2])).toEqual(expected)
  })

  it("Sets error on failure", () => {
    initialState = {
      loading: true,
      loggedIn: false
    }
    expected = {
      loading: false,
      loggedIn: false
    }
    actions = [oauthLogin.failure(), apiLogin.failure()]
    expect(loginStatusReducer(initialState, actions[0])).toEqual(expected)
    expect(loginStatusReducer(initialState, actions[1])).toEqual(expected)
  })

  it("Sets logged out on logout", () => {
    initialState = {
      loading: false,
      loggedIn: true
    }
    expected = {
      loading: false,
      loggedIn: false
    }
    action = logout()
    expect(loginStatusReducer(initialState, action)).toEqual(expected)
  })
})

describe("SessionReducer works", () => {
  const initialState = {
    reviewerCode: null,
    currentApresentacao: null
  }
  let expected
  let action

  it("Sets reviewer code on success", () => {
    expected = {
      reviewerCode: "2000-2000",
      currentApresentacao: null
    }
    action = apiLogin.success(mockPerson)

    expect(sessionReducer(initialState, action)).toEqual(expected)
  })
})

describe("UserReducer works", () => {
  it("Should update user data on mock monitor login requested", () => {
    const initialState: UserState = null
    const action = mockMonitorLogin()
    const expected = {
      idPessoa: "11",
      idVinculoPessoa: "2423",
      matriculaPessoa: "7305002061710",
      nomePessoa: "Claiborne Crockley",
      nomeSocialPessoa: "Raimundo Greeve",
      lotacaoPessoa: "CCA - DEPARTAMENTO DE CONTABILIDADE E FINANÇAS",
      emailPessoa: "rgreevea@accuweather.com",
      telefonePessoa: "(863) 4443108",
      aptidaoPessoa: 1,
      avaliadorPessoa: 0,
      monitorPessoa: 1,
      gerentePessoa: 1
    }

    expect(userReducer(initialState, action)).toEqual(expected)
  })
  it("Should update user data on mock reviewer login requested", () => {
    const initialState: UserState = null
    const action = mockReviewerLogin()
    const expected = {
      idPessoa: "10",
      idVinculoPessoa: "2129",
      matriculaPessoa: "1039567343118",
      nomePessoa: "Rheba Johantges",
      nomeSocialPessoa: "Truman Cossar",
      lotacaoPessoa:
        "HOSP - HULW SERVIÇO DA ATENDIMENTO ESPECIAL MATERNO INFANTIL (SAE/MI)",
      emailPessoa: "tcossar9@bandcamp.com",
      telefonePessoa: "(290) 5417286",
      aptidaoPessoa: 0,
      avaliadorPessoa: 1,
      monitorPessoa: 0,
      gerentePessoa: 1
    }

    expect(userReducer(initialState, action)).toEqual(expected)
  })

  it("Should update user data on successful api login", () => {
    const initialState: UserState = null
    const action = apiLogin.success(mockPerson)
    const expected = mockPerson

    expect(userReducer(initialState, action)).toEqual(expected)
  })
})

describe("VinculoSelectionDialog works", () => {})

describe("SelectedVinculoReducer works", () => {
  it("Should work for simple workflow: pick vinculo -> submit vinculo", () => {
    const initialState: VinculoSelectionDialogState = {
      vinculos: [],
      selectedVinculo: null,
      visible: false
    }

    const afterLogin = vinculoSelectionDialogReducer(
      initialState,
      oauthLogin.success([mockPerson, mockPerson])
    )

    expect(afterLogin).toEqual({
      vinculos: [mockPerson, mockPerson],
      selectedVinculo: 0,
      visible: true
    })

    const afterVinculoSelection = vinculoSelectionDialogReducer(
      afterLogin,
      selectVinculo(1)
    )

    expect(afterVinculoSelection).toEqual({
      visible: true,
      selectedVinculo: 1,
      vinculos: [mockPerson, mockPerson]
    })

    const afterSelectionSubmited = vinculoSelectionDialogReducer(
      afterVinculoSelection,
      submitVinculoSelection()
    )

    expect(afterSelectionSubmited).toEqual({
      visible: false,
      selectedVinculo: 1,
      vinculos: [mockPerson, mockPerson]
    })
  })
})
