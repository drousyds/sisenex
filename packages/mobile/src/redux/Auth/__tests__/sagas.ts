import { oauthLoginSaga, mockLoginSaga } from "../sagas"
import {
  oauthLogin,
  apiLogin,
  submitVinculoSelection,
  mockReviewerLogin,
  mockMonitorLogin
} from "../actions"
import * as Api from "api"
import { put, call, take } from "redux-saga/effects"
import { Pessoa } from "generated/graphql"
import { getType } from "typesafe-actions"

const mockPerson = {
  idPessoa: "2000",
  idVinculoPessoa: "2000",
  matriculaPessoa: "7305002061710",
  nomePessoa: "Claiborne Crockley",
  nomeSocialPessoa: "Raimundo Greeve",
  lotacaoPessoa: "CCA - DEPARTAMENTO DE CONTABILIDADE E FINANÇAS",
  emailPessoa: "rgreevea@accuweather.com",
  telefonePessoa: "(863) 4443108",
  aptidaoPessoa: 1,
  avaliadorPessoa: 0,
  monitorPessoa: 1,
  gerentePessoa: 1
}

describe("Auth related sagas work", () => {
  it("Should try to authenticate users through api after oauth login is successful and response has a single vinculo", () => {
    let gen = oauthLoginSaga()
    //Expect oauthLogin to be started
    expect(gen.next().value).toEqual(call(Api.oauthLogin))
    //Mock valid response from api
    const oAuthResponse = [mockPerson]
    //Expect saga to yield oauthLogin.success action
    expect(gen.next(oAuthResponse).value).toEqual(
      put(oauthLogin.success(oAuthResponse))
    )
    //Since response has only one vinculo, expect saga to yield apiLogin.request
    expect(gen.next().value).toEqual(put(apiLogin.request(mockPerson)))
  })
  it("Should wait for submitVinculoSelection when there are multiple vinculos", () => {
    let gen = oauthLoginSaga()
    const oAuthResponse: Pessoa[] = [
      {
        ...mockPerson,
        nomeSocialPessoa: "George"
      },
      {
        ...mockPerson,
        nomeSocialPessoa: "Martin"
      }
    ]

    // Expect saga to make request through the api
    expect(gen.next().value).toEqual(call(Api.oauthLogin))
    // Inject mock response
    gen.next(oAuthResponse)
    // Expect saga to wait for vinculo selection
    expect(gen.next().value).toEqual(take(getType(submitVinculoSelection)))
    // Expect saga to use selected vinculo to make a request
    expect(gen.next(submitVinculoSelection()))
  })
})

describe("Authentication mocking", () => {
  it("Should authenticate a reviewer when mockReviewerLogin called", () => {
    let gen = mockLoginSaga(mockReviewerLogin())
    expect(gen.next().value.PUT.action.payload.avaliadorPessoa).toEqual(1)
  })
  it("Should authenticate a monitor when mockReviewerLogin called", () => {
    let gen = mockLoginSaga(mockMonitorLogin())
    expect(gen.next().value.PUT.action.payload.monitorPessoa).toEqual(1)
  })
})
