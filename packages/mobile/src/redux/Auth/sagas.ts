import { takeEvery, call, put, take, select } from "redux-saga/effects"
import * as Api from "api"
import {
  oauthLogin,
  apiLogin,
  mockReviewerLogin,
  mockMonitorLogin,
  submitVinculoSelection,
  logout,
  setOauthToken
} from "./actions"
import { getType, Action } from "typesafe-actions"
import { Pessoa, UsuarioAtual } from "generated/graphql"
import { getSelectedVinculo } from "./selectors"
import { ApiLoginResponse, OauthLoginResponse } from "api/types"
import client from "graphql/client"
import { usuarioAtual } from "graphql/queries/usuarioAtual"
import {
  saveUsuarioAtualCache,
  loadAssinouTermoCache,
  clearUsuarioAtualCache
} from "caching"
import { navigate } from "redux/Navigation/actions"

export function* apiLoginSaga(action: ReturnType<typeof apiLogin.request>) {
  try {
    let res: ApiLoginResponse = yield call(Api.apiLogin, action.payload)
    let assinouTermo: boolean = yield call(loadAssinouTermoCache)
    const pessoa = res.data.resultados
    if (res) {
      const usuario: UsuarioAtual = {
        __typename: "UsuarioAtual",
        idPessoa: pessoa.idPessoa,
        idVinculoPessoa: pessoa.idVinculoPessoa,
        nomeSocialPessoa: pessoa.nomeSocialPessoa,
        token: "AK31K2312F",
        matriculaPessoa: pessoa.matriculaPessoa || "Indisponivel",
        lotacaoPessoa: pessoa.lotacaoPessoa!,
        avatarUrl:
          "http://www.mhcsa.org.au/wp-content/uploads/2016/08/default-non-user-no-photo.jpg",
        monitorPessoa: pessoa.monitorPessoa || 0,
        avaliadorPessoa: pessoa.avaliadorPessoa || 0,
        gerentePessoa: pessoa.gerentePessoa || 0
      }
      client.cache.writeQuery({
        query: usuarioAtual,
        data: {
          usuarioAtual: usuario
        }
      })
      yield call(saveUsuarioAtualCache, usuario)
      yield put(apiLogin.success({ ...res.data.resultados, assinouTermo }))
    } else {
      yield put(apiLogin.failure())
    }
  } catch (e) {
    console.log(e)
    yield put(apiLogin.failure())
  }
}

export function* logoutSaga() {
  clearUsuarioAtualCache()
}

export function* oauthLoginSaga() {
  let res: OauthLoginResponse = yield call(Api.oauthLogin)
  const vinculos = res.vinculos
  if (vinculos) {
    if (res.oauthToken) yield put(setOauthToken(res.oauthToken))
    yield put(oauthLogin.success(vinculos))
    if (vinculos.length == 1) yield put(apiLogin.request(vinculos[0]))
    else {
      yield take(getType(submitVinculoSelection))
      const selectedVinculo: Pessoa = yield select(getSelectedVinculo)
      yield put(apiLogin.request(selectedVinculo))
    }
  } else {
    yield put(oauthLogin.failure())
  }
}

export function* mockLoginSaga(
  action:
    | ReturnType<typeof mockReviewerLogin>
    | ReturnType<typeof mockMonitorLogin>
) {
  const usuario: UsuarioAtual = {
    __typename: "UsuarioAtual",
    idPessoa: action.payload.idPessoa,
    idVinculoPessoa: action.payload.idVinculoPessoa,
    nomeSocialPessoa: action.payload.nomeSocialPessoa,
    token: "AK31K2312F",
    matriculaPessoa: action.payload.matriculaPessoa || "Indisponivel",
    lotacaoPessoa: action.payload.lotacaoPessoa!,
    avatarUrl:
      "http://www.mhcsa.org.au/wp-content/uploads/2016/08/default-non-user-no-photo.jpg",
    monitorPessoa: action.payload.monitorPessoa || 0,
    avaliadorPessoa: action.payload.avaliadorPessoa || 0,
    gerentePessoa: action.payload.gerentePessoa || 0
  }
  client.cache.writeQuery({
    query: usuarioAtual,
    data: {
      usuarioAtual: usuario
    }
  })
  yield call(saveUsuarioAtualCache, usuario)

  yield put(apiLogin.success(action.payload))
}

const loginSagas = [
  takeEvery(getType(oauthLogin.request), oauthLoginSaga),
  takeEvery(getType(apiLogin.request), apiLoginSaga),
  takeEvery(getType(logout), logoutSaga),
  takeEvery(
    (action: Action) =>
      action.type == getType(mockReviewerLogin) ||
      action.type == getType(mockMonitorLogin),
    mockLoginSaga
  )
]

export default loginSagas
