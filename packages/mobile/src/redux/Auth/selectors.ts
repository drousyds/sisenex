import { createSelector } from "reselect"
import { RootState } from "redux/rootReducer"

export const getUser = createSelector(
  [(state: RootState) => state.auth.user],
  user => user
)

export const getSelectedVinculo = createSelector(
  [
    (state: RootState) => state.auth.vinculoSelection.vinculos,
    (state: RootState) => state.auth.vinculoSelection.selectedVinculo
  ],
  (vinculos, selectedVinculo) => vinculos[selectedVinculo]
)

export const getVinculoSelectionData = createSelector(
  [(state: RootState) => state.auth.vinculoSelection],
  vinculoSelection => vinculoSelection
)

export const getAssinouTermo = createSelector(
  [(state: RootState) => state.auth.loginStatus.assinouTermo],
  assinouTermo => assinouTermo
)

export const getLoggedIn = createSelector(
  [(state: RootState) => state.auth.loginStatus.loggedIn],
  loggedIn => loggedIn
)

export const getAvaliador = createSelector(
  [(state: RootState) => state.auth.loginStatus.avaliador],
  avaliador => avaliador
)
