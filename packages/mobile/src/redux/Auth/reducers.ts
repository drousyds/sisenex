import { createReducer, ActionType, action } from "typesafe-actions"
import {
  oauthLogin,
  apiLogin,
  logout,
  mockReviewerLogin,
  mockMonitorLogin,
  selectVinculo,
  submitVinculoSelection,
  hideVinculoSelection,
  setOauthToken,
  clearOauthToken,
  assinouTermo
} from "./actions"
import { Pessoa, Apresentacao } from "generated/graphql"

export interface LoginStatusState {
  loggedIn: boolean
  loading: boolean
  assinouTermo: boolean
  avaliador: number | null | undefined
}

export type AuthActions = ActionType<
  | typeof oauthLogin
  | typeof apiLogin
  | typeof mockReviewerLogin
  | typeof assinouTermo
  | typeof mockMonitorLogin
  | typeof selectVinculo
  | typeof submitVinculoSelection
  | typeof logout
  | typeof hideVinculoSelection
  | typeof setOauthToken
  | typeof clearOauthToken
>

export const loginStatusReducer = createReducer<LoginStatusState, AuthActions>({
  loggedIn: false,
  loading: false,
  assinouTermo: false,
  avaliador: 0
})
  .handleAction(oauthLogin.request, (state, action) => ({
    ...state,
    loading: true
  }))
  .handleAction(oauthLogin.success, (state, action) => ({
    ...state,
    loggedIn: false,
    loading: false
  }))
  .handleAction(oauthLogin.failure, (state, action) => ({
    ...state,
    loading: false
  }))
  .handleAction(apiLogin.request, (state, action) => ({
    ...state,
    loading: true
  }))
  .handleAction(apiLogin.failure, (state, action) => ({
    ...state,
    loading: false
  }))
  .handleAction(apiLogin.success, (state, action) => ({
    loggedIn: true,
    loading: false,
    assinouTermo: action.payload.assinouTermo,
    avaliador: action.payload.avaliadorPessoa
  }))
  .handleAction(logout, (state, action) => ({
    ...state,
    loggedIn: false
  }))
  .handleAction(mockReviewerLogin, (state, action) => ({
    ...state,
    loading: false,
    loggedIn: true,
    avaliador: 1
  }))
  .handleAction(mockMonitorLogin, (state, action) => ({
    ...state,
    loading: false,
    loggedIn: true,
    avaliador: 0
  }))
  .handleAction(assinouTermo, (state, action) => ({
    ...state,
    assinouTermo: true
  }))

export interface SessionState {
  reviewerCode: string | null
  oauthToken: string | null
  currentApresentacao: Pick<
    Apresentacao,
    "idApresentacao" | "id2Apresentacao"
  > | null
}

export const sessionReducer = createReducer<SessionState, AuthActions>({
  reviewerCode: null,
  oauthToken: null,
  currentApresentacao: null
})
  .handleAction(
    [apiLogin.success, mockMonitorLogin, mockReviewerLogin],
    (state, action) => ({
      ...state,
      reviewerCode: `${action.payload.idPessoa}-${
        action.payload.idVinculoPessoa
      }`
    })
  )
  .handleAction(setOauthToken, (state, action) => ({
    ...state,
    oauthToken: action.payload
  }))
  .handleAction(clearOauthToken, (state, action) => ({
    ...state,
    oauthToken: null
  }))

export type UserState = Pessoa | null

export const userReducer = createReducer<UserState, AuthActions>({
  idPessoa: "25",
  idVinculoPessoa: "2960",
  matriculaPessoa: "1039567343118",
  nomePessoa: "Rheba Johantges",
  nomeSocialPessoa: "Truman Cossar",
  lotacaoPessoa:
    "HOSP - HULW SERVIÇO DA ATENDIMENTO ESPECIAL MATERNO INFANTIL (SAE/MI)",
  emailPessoa: "tcossar9@bandcamp.com",
  telefonePessoa: "(290) 5417286",
  aptidaoPessoa: 0,
  avaliadorPessoa: 1,
  monitorPessoa: 0,
  gerentePessoa: 1
}).handleAction(
  [apiLogin.success, mockMonitorLogin, mockReviewerLogin],
  (state, action) => action.payload
)

export interface VinculoSelectionDialogState {
  visible: boolean
  selectedVinculo: number
  vinculos: Pessoa[]
}

export const vinculoSelectionDialogReducer = createReducer<
  VinculoSelectionDialogState,
  AuthActions
>({
  visible: false,
  selectedVinculo: 0,
  vinculos: []
})
  .handleAction(oauthLogin.success, (state, action) => ({
    visible: action.payload.length > 1,
    selectedVinculo: 0,
    vinculos: action.payload
  }))
  .handleAction(selectVinculo, (state, action) => ({
    ...state,
    selectedVinculo: action.payload
  }))
  .handleAction(submitVinculoSelection, (state, action) => ({
    ...state,
    visible: false
  }))
  .handleAction(hideVinculoSelection, (state, action) => ({
    ...state,
    visible: false
  }))
