import { createAsyncAction, createAction } from "typesafe-actions"
import { Pessoa, Apresentacao } from "generated/graphql"

export const apiLogin = createAsyncAction(
  "@session/api_login",
  "@session/api_login/success",
  "@session/api_login/failure"
)<Pessoa, Pessoa & { assinouTermo: boolean }, undefined>()

export const oauthLogin = createAsyncAction(
  "@session/oauth_login",
  "@session/oauth_login/success",
  "@session/oauth_login/failure"
)<undefined, Pessoa[], undefined>()

export const logout = createAction("@session/logout")

export const mockReviewerLogin = createAction(
  "@session/mock_reviewer_login",
  action => () =>
    action({
      idPessoa: "54",
      idVinculoPessoa: "54",
      matriculaPessoa: "84-327-7723",
      nomePessoa: "Dayane Freitas",
      nomeSocialPessoa: "Dayane Freitas",
      lotacaoPessoa: "Sisenex",
      emailPessoa: "epottes1d@java.com",
      telefonePessoa: "6879035219",
      aptidaoPessoa: 1,
      avaliadorPessoa: 1,
      monitorPessoa: 0,
      gerentePessoa: 1,
      // idPessoa: "52",
      // idVinculoPessoa: "52",
      // matriculaPessoa: "84-327-7721",
      // nomePessoa: "Vinicius Misael",
      // nomeSocialPessoa: "Vinicius Misael",
      // lotacaoPessoa: "Sisenex",
      // emailPessoa: "epottes1d@java.com",
      // telefonePessoa: "6879035219",
      // aptidaoPessoa: 1,
      // avaliadorPessoa: 1,
      // monitorPessoa: 0,
      // gerentePessoa: 0,
      assinouTermo: false
    })
)

export const mockReviewerLogin2 = createAction(
  "@session/mock_reviewer_login",
  action => () =>
    action({
      idPessoa: "463731",
      idVinculoPessoa: "315042",
      matriculaPessoa: "20180120138",
      nomePessoa: "EMERSON SANTOS BARBOSA",
      nomeSocialPessoa: "EMERSON SANTOS BARBOSA",
      lotacaoPessoa: null,
      emailPessoa: "emersonbarbosa@cc.ci.ufpb.br",
      telefonePessoa: "81989374990",
      aptidaoPessoa: 1,
      avaliadorPessoa: 1,
      monitorPessoa: 0,
      gerentePessoa: 1,
      // idPessoa: "52",
      // idVinculoPessoa: "52",
      // matriculaPessoa: "84-327-7721",
      // nomePessoa: "Vinicius Misael",
      // nomeSocialPessoa: "Vinicius Misael",
      // lotacaoPessoa: "Sisenex",
      // emailPessoa: "epottes1d@java.com",
      // telefonePessoa: "6879035219",
      // aptidaoPessoa: 1,
      // avaliadorPessoa: 1,
      // monitorPessoa: 0,
      // gerentePessoa: 0,
      assinouTermo: false
    })
)

export const mockReviewerLogin3 = createAction(
  "@session/mock_reviewer_login",
  action => () =>
    action({
      idPessoa: "7",
      idVinculoPessoa: "8", 
      matriculaPessoa: "05-442-1441",
      nomePessoa: "Jordana",
      nomeSocialPessoa: "Jordana Juanes",
      lotacaoPessoa: "Edgeclub",
      emailPessoa: "jjuanes6@buzzfeed.com",
      telefonePessoa: "3127772372",
      aptidaoPessoa: 1,
      avaliadorPessoa: 1,
      monitorPessoa: 0,
      gerentePessoa: 0,
      assinouTermo: false
    })
)

export const mockMonitorLogin = createAction(
  "@session/mock_monitor_login",
  action => () =>
    action({
      idPessoa: "53",
      idVinculoPessoa: "53",
      matriculaPessoa: "84-327-7722",
      nomePessoa: "Luciano Jr",
      nomeSocialPessoa: "Luciano Jr",
      lotacaoPessoa: "Sisenex",
      emailPessoa: "epottes1d@java.com",
      telefonePessoa: "6879035219",
      aptidaoPessoa: 1,
      avaliadorPessoa: 0,
      monitorPessoa: 1,
      gerentePessoa: 0,
      assinouTermo: false
    })
)

export const assinouTermo = createAction(
  "@session/assinou_termo",
  action => () =>
    action({
      assinouTermo: true
    })
)

export const selectVinculo = createAction(
  "@session/select_vinculo",
  action => (vinculo: number) => action(vinculo)
)

export const submitVinculoSelection = createAction(
  "@session/submit_vinculo_selection"
)

export const hideVinculoSelection = createAction(
  "@session/hide_vinculo_selection"
)

export const setOauthToken = createAction(
  "@session/set_oauth_token",
  action => (token: string) => action(token)
)

export const clearOauthToken = createAction("@session/clear_oauth_token")