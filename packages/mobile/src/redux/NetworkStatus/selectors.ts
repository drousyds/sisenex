import { createSelector } from "reselect"
import { RootState } from "redux/rootReducer"

export const getNetworkStatus = createSelector(
  [(state: RootState) => state.networkStatus],
  networkStatus => networkStatus
)
