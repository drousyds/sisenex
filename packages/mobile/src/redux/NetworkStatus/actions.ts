import { NetworkStatus } from "apollo-client"
import { createAction, action } from "typesafe-actions"

export const notifyNetworkChange = createAction(
  "@networkStatus/status_changed",
  action => (isConnected: boolean) => action(isConnected)
)

export const showBadConnectionStatus = createAction(
  "@networkStatus/show_bad_connection_status"
)

export const hideBadConnectionStatus = createAction(
  "@networkStatus/hide_bad_connection_status"
)

export const showGoodConnectionStatus = createAction(
  "@networkStatus/show_good_connection_status"
)

export const hideGoodConnectionStatus = createAction(
  "@networkStatus/hide_good_connection_status"
)
