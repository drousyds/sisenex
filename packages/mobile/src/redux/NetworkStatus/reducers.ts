import {
  hideBadConnectionStatus,
  hideGoodConnectionStatus,
  showBadConnectionStatus,
  showGoodConnectionStatus,
  notifyNetworkChange
} from "./actions"
import { createReducer, ActionType } from "typesafe-actions"

export type NetworkStatusActions =
  | ActionType<typeof showGoodConnectionStatus>
  | ActionType<typeof hideGoodConnectionStatus>
  | ActionType<typeof showBadConnectionStatus>
  | ActionType<typeof hideBadConnectionStatus>
  | ActionType<typeof notifyNetworkChange>

export interface NetworkStatusState {
  visible: boolean
  status: "good" | "bad"
}

export const networkStatusReducer = createReducer<
  NetworkStatusState,
  NetworkStatusActions
>({
  visible: false,
  status: "good"
})
  .handleAction(showGoodConnectionStatus, (state, action) => ({
    visible: true,
    status: "good"
  }))
  .handleAction(showBadConnectionStatus, (state, action) => ({
    visible: true,
    status: "bad"
  }))
  .handleAction(hideGoodConnectionStatus, (state, action) => ({
    visible: false,
    status: "good"
  }))
  .handleAction(hideBadConnectionStatus, (state, action) => ({
    visible: false,
    status: "good"
  }))
