import { put, takeLatest, select } from "redux-saga/effects"
import { delay } from 'redux-saga/effects'
import {
  notifyNetworkChange,
  showBadConnectionStatus,
  showGoodConnectionStatus,
  hideBadConnectionStatus,
  hideGoodConnectionStatus
} from "./actions"
import { getType } from "typesafe-actions"
import { getNetworkStatus } from "./selectors"
import { NetworkStatusState } from "./reducers"

export function* networkStatusChangedSaga(
  action: ReturnType<typeof notifyNetworkChange>
) {
  const currentState: NetworkStatusState = yield select(getNetworkStatus)
  const connectionChanged = action.payload != (currentState.status == "good")
  if (connectionChanged && !action.payload) {
    yield put(showBadConnectionStatus())
    yield delay(12000)
    yield put(hideBadConnectionStatus())
  }
  if (connectionChanged && action.payload) {
    yield put(showGoodConnectionStatus())
    yield delay(4000)
    yield put(hideGoodConnectionStatus())
  }
}

export default [
  takeLatest(getType(notifyNetworkChange), networkStatusChangedSaga)
]
