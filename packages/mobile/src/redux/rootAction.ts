import * as AuthActions from "redux/Auth/actions"
import * as NavigationActions from "redux/Navigation/actions"
import * as NetworkStatusAction from "redux/NetworkStatus/actions"

export default {
  AuthActions,
  NavigationActions,
  NetworkStatusAction
}
