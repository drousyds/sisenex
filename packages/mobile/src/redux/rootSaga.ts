import AuthSagas from "redux/Auth/sagas"
import NavigationSagas from "redux/Navigation/sagas"
import NetworkStatusSagas from "redux/NetworkStatus/sagas"
import { all } from "redux-saga/effects"

const allSagas = [...AuthSagas, ...NavigationSagas, ...NetworkStatusSagas]

export default function* rootSaga() {
  try {
    yield all(allSagas)
  } catch (e) {
    console.error("Root saga caugth unhandled exception: ", e)
  }
}
