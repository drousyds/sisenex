import { navigate } from "./actions"
import { takeLatest, put } from "redux-saga/effects"
import { getType } from "typesafe-actions"
import NavigationService from "navigation/utils/NavigationService"
import { apiLogin } from "../Auth/actions"

export function* navigationSaga(action: ReturnType<typeof navigate>) {
  NavigationService.navigate(action.payload.routeName, action.payload.params)
  yield null
}

const navigationSagas = [takeLatest(getType(navigate), navigationSaga)]

export default navigationSagas
