import { createAction } from "typesafe-actions"
import { Projeto } from "generated/graphql"

export type GenericNavigationPayload<
  T extends string,
  P extends object = {}
  > = {
    routeName: T
    params?: P
  }

export type NavigationPayload =
  | GenericNavigationPayload<"ProjectsScreen", {}>
  | GenericNavigationPayload<"ReviewersScreen", {}>
  | GenericNavigationPayload<"ReviewerStack", {}>
  | GenericNavigationPayload<"MonitorStack", {}>
  | GenericNavigationPayload<"ApresentacoesScreen", {}>
  | GenericNavigationPayload<"PendingProjectsScreen", {}>
  | GenericNavigationPayload<"FinishedProjectsScreen", {}>
  | GenericNavigationPayload<"LoginScreen", {}>
  | GenericNavigationPayload<"RegistroApresentacao", {}>
  | GenericNavigationPayload<"RegistroApresentacaoStack", {}>
  | GenericNavigationPayload<"MonitorTabs", {}>
  | GenericNavigationPayload<"Apresentacao", {}>
  | GenericNavigationPayload<"PessoasApresentacao", {}>
  | GenericNavigationPayload<"ProjetosApresentacao", {}>
  | GenericNavigationPayload<"RegistroPessoa", {}>
  | GenericNavigationPayload<"TermoScreen", {}>
  | GenericNavigationPayload<"ReportProblemas", {}>
  | GenericNavigationPayload<"MonitorRoutes", {}>
  | GenericNavigationPayload<"QrVerificationScreen", {}>
  | GenericNavigationPayload<"RegistroApresentacaoInput", {}>
  | GenericNavigationPayload<"RegistroPessoaInput", {}>
  | GenericNavigationPayload<"AvisosLegais", {}>
  | GenericNavigationPayload<"SolicitacaoLGPD", {}>
  | GenericNavigationPayload<
    "GerenciaProjeto",
    {
      idProjeto: Projeto["idProjeto"]
      tituloProjeto: Projeto["tituloProjeto"]
    }
  >
  | GenericNavigationPayload<
    "StartReviewScreen",
    {
      projectId: Projeto["idProjeto"]
    }
  >
  | GenericNavigationPayload<
    "QuestionsScreen",
    {
      projectId: Projeto["idProjeto"]
    }
  >
  | GenericNavigationPayload<
    "AvaliacaoProjeto",
    {
      projectId: Projeto["idProjeto"]
      tituloProjeto: string
      podeAvaliar: boolean
    }
  >
  | GenericNavigationPayload<"StartReviewScreen", {}>

export type ScreenName = NavigationPayload extends { routeName: string }
  ? NavigationPayload["routeName"]
  : never

export const navigate = createAction(
  "@navigation/navigate",
  action => (config: NavigationPayload) => action(config)
)
