import { createReducer, ActionType } from "typesafe-actions"
import { NavigationPayload, navigate } from "./actions"

export type NavigationState = (
  | Pick<NavigationPayload, "routeName">
  | { routeName: null }) & {
  params?: any
}

export type NavigationActions = ActionType<typeof navigate>

export const navigationReducer = createReducer<
  NavigationState,
  NavigationActions
>({
  routeName: null
}).handleAction(navigate, (state, action) => action.payload)
