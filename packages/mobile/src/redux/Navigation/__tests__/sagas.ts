import { navigate } from "../actions"
import {
  apiLogin,
  mockMonitorLogin,
  mockReviewerLogin
} from "redux/Auth/actions"
import { userLoggedInSaga } from "../sagas"
import { put } from "redux-saga/effects"

describe("NavigationSagas work", () => {
  it("Should navigate to ReviewerStack when reviewer is authenticated", () => {
    const mockPerson = mockReviewerLogin()
    const gen = userLoggedInSaga(apiLogin.success(mockPerson.payload))
    expect(gen.next().value).toEqual(
      put(navigate({ routeName: "ReviewerStack" }))
    )
  })

  it("Should navigate to MonitorStack when monitor is authenticated", () => {
    const mockPerson = mockMonitorLogin()
    const gen = userLoggedInSaga(apiLogin.success(mockPerson.payload))
    expect(gen.next().value).toEqual(
      put(navigate({ routeName: "MonitorStack" }))
    )
  })
})
