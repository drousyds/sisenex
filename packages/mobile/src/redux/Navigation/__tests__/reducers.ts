import { NavigationState, navigationReducer } from "../reducers"
import { navigate } from "../actions"

describe("NavigationReducer works", () => {
  it("Should set new currentRoute after navigation action", () => {
    const initialState: NavigationState = {
      routeName: "LoginScreen"
    }
    const action = navigate({ routeName: "PendingProjectsScreen" })
    const expected: NavigationState = {
      routeName: "PendingProjectsScreen"
    }
    expect(navigationReducer(initialState, action)).toEqual(expected)
  })
})
