import React from "react"
import { useState } from "react"
import {
  ConfirmationDialog,
  ConfirmationDialogProps
} from "components/Dialogs/ConfirmationDialog"

export interface ConfirmationDialogOptions {
  message: string
  onConfirm?: () => void
}

const useConfirmationDialog = (
  options: ConfirmationDialogOptions
): [() => void, ConfirmationDialogProps] => {
  const [visible, setVisible] = useState(false)
  const openDialog = () => setVisible(true)
  const onCancel = options.onConfirm && (() => setVisible(false))
  const onConfirm = () => {
    setVisible(false)
    if (options.onConfirm) options.onConfirm()
  }

  const dialogOptions: ConfirmationDialogProps = {
    visible: visible,
    message: options.message,
    onCancel: onCancel,
    onConfirm: onConfirm
  }

  return [openDialog, dialogOptions]
}

export default useConfirmationDialog
