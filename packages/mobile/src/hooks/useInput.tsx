import { useState } from "react"
import { SimpleTextInputProps } from "components/TextInput/SimpleTextInput"

export const useInput = (
  props: SimpleTextInputProps
): [
  SimpleTextInputProps,
  {
    valid: boolean
    errorState: boolean
    onSubmitEditing: (text: string) => void
    text: string
  }
] => {
  const [text, setText] = useState("")
  const [errorState, setError] = useState(false)

  const textInputProps: SimpleTextInputProps = {
    ...props,
    errorState: errorState,
    onSubmitEditing: text => {
      if (props.validator) {
        if (props.validator(text)) {
          setError(false)
          props.onSubmitEditing(text)
        } else setError(true)
      } else {
        props.onSubmitEditing(text)
      }
    },
    onChangeText: setText
  }

  return [
    textInputProps,
    {
      valid: (props.validator && props.validator(text)) || true,
      onSubmitEditing: textInputProps.onSubmitEditing,
      errorState,
      text
    }
  ]
}
