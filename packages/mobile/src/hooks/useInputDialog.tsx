import React, { useState } from "react"
import { MaterialDialogProps } from "components/Dialogs/MaterialDialog"
// @ts-ignore
import { TextField } from "react-native-material-textfield"

export interface InputDialogHookConfig {
  onSubmit: (text: string, extraData: any) => void
  validator: (text: string) => boolean
  headerText: string
  label: string
}

const useInputDialog = ({
  onSubmit,
  headerText,
  label,
  validator
}: InputDialogHookConfig): [(extraData: any) => void, MaterialDialogProps] => {
  const [state, setState] = useState({
    inputValue: "",
    headerText: "",
    label: "",
    visible: false,
    valid: true,
    validator: (text: string): boolean => true,
    extraData: null
  })

  const openInputDialog = (extraData: any) =>
    setState({
      headerText,
      label,
      inputValue: "",
      visible: true,
      valid: true,
      validator: validator || state.validator,
      extraData
    })

  const closeInputDialog = () => {
    if (state.valid) {
      onSubmit(state.inputValue, state.extraData)
      setState({
        ...state,
        visible: false
      })
    }
  }

  const onChangeText = (newText: string) => {
    let valid = state.validator(newText)
    setState({
      ...state,
      inputValue: newText,
      valid: valid
    })
  }

  const dialogProps: MaterialDialogProps = {
    visible: state.visible,
    actionName: "Confirmar",
    headerText: state.headerText,
    onActionPress: closeInputDialog,
    toggle: () =>
      setState({
        ...state,
        visible: !state.visible
      }),
    content: (
      <TextField
        label={state.label}
        value={state.inputValue}
        keyboardType="numeric"
        onChangeText={onChangeText}
        error={state.valid ? undefined : "Entrada invalida!"}
      />
    )
  }

  return [openInputDialog, dialogProps]
}

export default useInputDialog
