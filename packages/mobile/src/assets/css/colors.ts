/*const Colors = {
primary: '#1565c0', //light material indigo
  //  primary: '#35457E',
  cardColor: '#F3F3F3',
  blueText: '#1a2947',
  greyText: '#6e6e6e',
  red: '#c60000',
  green: '#7aa048',
  yellow: '#f4b323'
}*/

//Material blue pallete
const Colors = {
  primaryDark: "#1976D2",
  primaryLight: "#BBDEFB",
  //primary: "#303F9F",
  primary: "#443DD0",
  primaryText: "#414141",
  secondaryText: "#606060",
  dividerColor: "#BDBDBD",
  icons: "#FFFFFF",

  error: "#ff0033",
  errorHighlight: "#D8000C",
  success: "#2ecc71",
  warn: "#f4d03f",
  info: "#17202a",

  red: "#A5A1A1",
  green: "#1D8D18",
  darkGreen: "#0B6A28",
  yellow: "#f4b323",

  blueText: "#1a2947",
  greyText: "#6e6e6e",

  background: "#f7f7f7"
}

export default Colors
