import { StyleSheet } from "react-native"
import { material } from "react-native-typography"
import Colors from "./colors"

const Typography = StyleSheet.create({
  overline: {
    fontSize: 10,
    letterSpacing: 1.5
  },

  caption: {
    ...material.captionObject,
    fontWeight: "400",
    fontSize: 16,
    letterSpacing: 0.4
  },

  button: {
    fontSize: 14,
    fontWeight: "500",
    color: "#ffffffef"
  },
  button2: {
    fontSize: 12,
    fontWeight: "500",
    color: "#ffffffef"

  },
  body2: {
    fontSize: 14,
    letterSpacing: 0.25
  },

  body: {
    fontSize: 12,
    letterSpacing: 0.5
  },

  subtitle2: {
    fontSize: 14,
    fontWeight: "500",
    letterSpacing: 0.1,
    color: Colors.secondaryText
  },

  subtitle: {
    fontSize: 15,
    letterSpacing: 0.15,
    color: Colors.secondaryText
  },

  header: {
    ...material.headlineObject,
    lineHeight: 40,
    fontSize: 40,
    letterSpacing: 0,
    color: Colors.primaryText
  },

  header2: {
    ...material.titleObject,
    textAlign: "center",
    lineHeight: 18,
    fontSize: 15,
    color: Colors.primaryText
  },

  header3: {
    ...material.headlineObject,
    fontWeight: "normal",
    color: Colors.primaryText
  },

  // Fonte da media em cards da avaliacao
  mediaAvaliacao: {
    ...material.headlineObject,
    fontSize: 26
  }
})

export default Typography
