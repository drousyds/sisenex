import Colors from "./colors"
import { StyleSheet } from "react-native"

const Layouts = StyleSheet.create({
  headerOuterContainerStyle: {
    display: "flex",
    width: "100%",
    height: null,
    backgroundColor: Colors.primary,
    flexDirection: "row",
    paddingLeft: 16,
    paddingRight: 8,
    paddingTop: 34,
    paddingBottom: 16,
    justifyContent: "space-between"
  },

  cardContainerStyle: {
    display: "flex",
    flexDirection: "column",
    height: 140,
    width: null,
    alignItems: "stretch",
    justifyContent: "flex-start",
    paddingBottom: 2,
    paddingTop: 12,
    paddingLeft: 8,
    paddingRight: 8,
    backgroundColor: "white"
    // shadowColor: "#0000",
    // shadowOffset: { width: 1, height: 1 },
    // shadowOpacity: 0.15,
    // shadowRadius: 2
  },

  iosCardContainerStyle: {
    shadowColor: "black",
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.15,
    marginLeft: 8,
    height: 140,
    marginRight: 8,
    backgroundColor: "white",
    marginHorizontal: 2
  },

  screenContainerStyle: {
    display: "flex",
    flex: 1,
    paddingTop: 16,
    paddingHorizontal: 0,
    backgroundColor: "#FFF"
  }
})

export default Layouts
