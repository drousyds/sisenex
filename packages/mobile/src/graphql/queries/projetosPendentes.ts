import gql from "graphql-tag"

export const projetosPendentes = gql`
  query projetosPendentes($idPessoa: String!, $idVinculoPessoa: String!) {
    pessoa(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
      idPessoa
      idVinculoPessoa
      ... on Gerente {
        projetosParaAvaliar {
          idProjeto
          tituloProjeto
          apresentacao{
            idApresentacao
          }
          categoria {
            idCategoria
          }
          # Ainda nao temos coordenador
          #coordenadorProjeto
          # Seria interessante media individual de um avaliador
          # especifico, por enquanto posso filtrar client-side
          mediaIndividual(
            idPessoa: $idPessoa
            idVinculoPessoa: $idVinculoPessoa
          ) {
            mediaProjeto
          }
        }
      }
      ... on Avaliador {
        projetosParaAvaliar {
          idProjeto
          tituloProjeto
          apresentacao{
            idApresentacao
          }
          categoria {
            idCategoria
          }
          # Ainda nao temos coordenador
          #coordenadorProjeto
          # Seria interessante media individual de um avaliador
          # especifico, por enquanto posso filtrar client-side
          mediaIndividual(
            idPessoa: $idPessoa
            idVinculoPessoa: $idVinculoPessoa
          ) {
            mediaProjeto
          }
        }
      }
    }
  }
`

export const mockProjetosPendentesQuery = (_: any) => ({
  loading: false,
  error: false,
  data: {
    pessoa: {
      __typename: "Gerente",
      projetosParaAvaliar: [
        {
          idProjeto: 2662,
          categoria: {
            idCategoria: 1
          },
          apresentacao:{
            idApresentacao: 4
          },
          tituloProjeto: "Família, escola e aprendizagem",
          coordenadorProjeto: "Milene Felix de Almeida",
          mediaIndividual: {
            mediaProjeto: 7.5
          }
        },
        {
          idProjeto: 3136,
          categoria: {
            idCategoria: 1
          },
          apresentacao:{
            idApresentacao: 2
          },
          tituloProjeto: "Ciclo de Conferências de Letras 2018",
          coordenadorProjeto: "Milene Felix de Almeida",
          mediaIndividual: null
        },
        {
          idProjeto: 3088,
          categoria: {
            idCategoria: 2
          },
          apresentacao:{
            idApresentacao: 3
          },
          tituloProjeto: "Grupo de choro da UFPB",
          coordenadorProjeto: "Milene Felix de Almeida",
          mediaIndividual: null
        },
        {
          idProjeto: 3154,
          categoria: {
            idCategoria: 2
          },
          apresentacao:{
            idApresentacao: 1
          },
          tituloProjeto: "Projeto Ensino de Idiomas no CCHSA",
          coordenadorProjeto: "Mileni Feliz de Almeida",
          mediaIndividual: null
        }
      ]
    }
  }
})
