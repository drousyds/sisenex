import gql from "graphql-tag"

export const projetosApresentacao = gql`
  query projetosApresentacao($idApresentacao: Int!, $id2Apresentacao: String!) {
    apresentacao(
      input: {
        idApresentacao: $idApresentacao
        id2Apresentacao: $id2Apresentacao
      }
    ) {
      idApresentacao
      id2Apresentacao
      projetos {
        idProjeto
        tituloProjeto
        disponibilidadeProjeto
        coordenador {
          nomePessoa
        }
      }
    }
  }
`

export const mockProjetosApresentacoesQuery = () => ({
  loading: false,
  error: false,
  data: {
    apresentacao: {
      projetos: [
        {
          idProjeto: 123,
          tituloProjeto: "Um teste sobre como projetos aparecem na tela",
          disponibilidadeProjeto: 1,
          coordenadorProjeto: "Indisponivel"
        },
        {
          idProjeto: 124,
          tituloProjeto: "Um teste sobre como projetos aparecem na tela",
          disponibilidadeProjeto: 1,
          coordenadorProjeto: "Indisponivel"
        },
        {
          idProjeto: 125,
          tituloProjeto: "Um teste sobre como projetos aparecem na tela",
          disponibilidadeProjeto: 1,
          coordenadorProjeto: "Indisponivel"
        },
        {
          idProjeto: 126,
          tituloProjeto: "Um teste sobre como projetos aparecem na tela",
          disponibilidadeProjeto: 1,
          coordenadorProjeto: "Indisponivel"
        }
      ]
    }
  }
})
