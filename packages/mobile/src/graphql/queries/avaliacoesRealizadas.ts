import gql from "graphql-tag"

export const avaliacoesRealizadas = gql`
  query avaliacoesRealizadas($idPessoa: String!, $idVinculoPessoa: String!) {
    pessoa(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
      idPessoa
      idVinculoPessoa
      ... on Avaliador {
        avaliacoes {
          projeto {
            idProjeto
            tituloProjeto
            #media individual colocada na query pois estava
            #retornando um valor diferente, por causa do comentario
            mediaIndividual(
              idPessoa: $idPessoa
              idVinculoPessoa: $idVinculoPessoa
            ) {
              mediaProjeto
            }
          }
          media
        }
      }
      ... on Gerente {
        avaliacoes {
          projeto {
            idProjeto
            tituloProjeto
            #media individual colocada na query pois estava
            #retornando um valor diferente, por causa do comentario
            mediaIndividual(
              idPessoa: $idPessoa
              idVinculoPessoa: $idVinculoPessoa
            ) {
              mediaProjeto
            }
          }
          media
        }
      }
    }
  }
`

export const mockAvaliacoesRealizadasQuery = () => ({
  loading: false,
  error: false,
  data: {
    pessoa: {
      __typename: "Gerente",
      avaliacoes: [
        {
          projeto: {
            idProjeto: 1,
            tituloProjeto:
              "Monitoramento da producao comercializada e fortalecimento das estrategias de planejamento da producao na rede de feiras",
            coordenadorProjeto: "Milene Felix de Almeida"
          },
          media: 7.5
        },
        {
          projeto: {
            tituloProjeto:
              "Desenvolvimento de hostas educativas junto a populacao",
            coordenadorProjeto: "Milene Felix de Almeida",
            idProjeto: 2
          },
          media: 4
        },
        ,
        {
          projeto: {
            idProjeto: 3,
            tituloProjeto:
              "Centro de Ciencias Agrarias - CCA Atraves de Documentos: A (RE) Construcao da Memoria Institucional, Academica, Cultura, Socical e Historica",
            coordenadorProjeto: "Milene Felix de Almeida"
          },
          media: 10
        },
        {
          projeto: {
            idProjeto: 4,
            tituloProjeto:
              "Educacao, Memoria e Narrativas da Historia da Luta Camponesa no Brejo Paraibano.",
            coordenadorProjeto: "Mileni Feliz de Almeida"
          },
          media: 8
        }
      ]
    }
  }
})
