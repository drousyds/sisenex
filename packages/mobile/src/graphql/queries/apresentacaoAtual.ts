import gql from "graphql-tag"

export const apresentacaoAtual = gql`
  query apresentacaoAtual {
    apresentacaoAtual @client {
      idApresentacao
      id2Apresentacao
      nomeAreaTematica
      horaApresentacao
      salaApresentacao
      codigoApresentacao
    }
  }
`

export const mockApresentacaoAtual = {}
