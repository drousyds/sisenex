import gql from "graphql-tag"
import { cardAvaliadorFragment } from "graphql/fragments/cardAvaliadorFragment"

export const avaliadoresProjeto = gql`
  ${cardAvaliadorFragment}

  query avaliadoresProjeto($idApresentacao: Int!, $id2Apresentacao: String!) {
    projeto(idProjeto: 2860) {
      idProjeto
      tituloProjeto
      disponibilidadeProjeto
    }

    apresentacao(input: {idApresentacao: $idApresentacao, id2Apresentacao: $id2Apresentacao}) {
      idApresentacao
      id2Apresentacao
      avaliadores {
        ...cardAvaliadorFragment
      }
    }
  }
`
export const mockAvaliadoresProjeto = () => ({
  loading: false,
  error: undefined,
  data: {
    avaliadoresProjeto: {
      projeto: {
        idProjeto: 2860,
        tituloProjeto:
          "Projeto: qualificação das ações de vigilância em saúde para arboviroses, sífilis congênita e mortalidade materna na Paraíba.",
        disponibilidadeProjeto: 0
      },
      apresentacao: {
        idApresentacao: 1,
        id2Apresentacao: "2019",
        avaliadores: [
          {
            idPessoa: 1,
            nomePessoa: "Danielle Rousy Dias da Silva",
            lotacaoPessoa: "CI - Departamento de Informatica",
            matriculaPessoa: "1246917",
            habilitado: false
          },
          {
            idPessoa: 2,
            nomePessoa: "Eudisley Gomes dos Anjos",
            lotacaoPessoa: "CI - Departamento de Informatica",
            matriculaPessoa: "1246817",
            habilitado: false
          },
          {
            idPessoa: 3,
            nomePessoa: "Fernando Menezes Matos",
            lotacaoPessoa: "CI - Departamento de Informatica",
            matriculaPessoa: "1246817",
            habilitado: false
          },
          {
            idPessoa: 4,
            nomePessoa: "Marconilia Maria Dias Arnaud",
            lotacaoPessoa: "PU - Prefeitura Universitaria",
            matriculaPessoa: "1246817",
            habilitado: false
          }
        ]
      }
    }
  }
})

export default avaliadoresProjeto
