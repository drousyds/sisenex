import gql from "graphql-tag"

export const avaliacaoProjetoFragment = gql`
  fragment AvaliacaoProjeto on Avaliacao {
    __typename
    projeto {
      idProjeto
    }
    notas {
      nota
      respostaTextual
      pergunta {
        idPergunta
        conteudoPergunta
        categoria {
          idCategoria
          nomeCategoria
        }
      }
    }
  }
`

export const avaliacaoProjeto = gql`
  query avaliacaoProjeto(
    $idProjeto: Int!
    $idPessoa: String!
    $idVinculoPessoa: String!
  ) {
    perguntas {
      idPergunta
      conteudoPergunta
      categoria {
        idCategoria
        nomeCategoria
      }
    }

    projeto(idProjeto: $idProjeto) {
      idProjeto
      tituloProjeto
      linkArtefato
      categoria {
        idCategoria
        nomeCategoria
      }
    }

    avaliacao(
      idProjeto: $idProjeto
      idPessoa: $idPessoa
      idVinculoPessoa: $idVinculoPessoa
    ) {
      ...AvaliacaoProjeto
    }
  }

  ${avaliacaoProjetoFragment}
`

export const mockAvaliacaoProjetoQuery = () => ({
  loading: false,
  error: false,
  data: {
    projeto: {
      idProjeto: 1,
      tituloProjeto:
        "Monitoramento da producao comercializada e fortalecimento das estrategias de planejamento da producao na rede de feiras",
      linkArtefato: "https://www.pelando.com.br/",
      categoria: {
        idCategoria: 1,
        nomeCategoria: "e-Tertúlia"
      }
    },

    avaliacao: {
      notas: [
        {
          nota: 7.5,
          respostaTextual: null,
          pergunta: {
            idPergunta: 1,
            conteudoPergunta:
              "Demonstrou a interação de profissionais e/ou estudantes de outras áreas, estimulando à prática do diálogo interdisciplinar, multidisciplinar e interprofissional?",
            categoria: {
              nomeCategoria:
                "Quanto a apresentação do Resumo/participação no Projeto"
            }
          }
        },
        {
          nota: null,
          respostaTextual: null,
          pergunta: {
            idPergunta: 2,
            conteudoPergunta:
              "Demonstrou como o projeto vem sendo executado durante o período pandêmico (uso das TIC’s, dentre outros recursos) ?",
            categoria: {
              nomeCategoria:
                "Quanto a apresentação do Resumo/participação no Projeto"
            }
          }
        },
        {
          nota: null,
          respostaTextual: null,
          pergunta: {
            idPergunta: 3,
            conteudoPergunta:
              "Demonstrou ser destinado o projeto à comunidade externa a UFPB e como tem sido a interação com o público alvo?",
            categoria: {
              nomeCategoria:
                "Quanto a apresentação do Resumo/participação no Projeto"
            }
          }
        },
        {
          nota: null,
          respostaTextual: null,
          pergunta: {
            idPergunta: 4,
            conteudoPergunta:
              "Demonstrou coerência entre os objetivos e fundamentação teórica/metodológica do projeto?",
            categoria: {
              nomeCategoria:
                "Quanto a apresentação do Resumo/participação no Projeto"
            }
          }
        },
        {
          nota: 8,
          respostaTextual: null,
          pergunta: {
            idPergunta: 5,
            conteudoPergunta:
              "Demonstrou a contribuição da experiência extensionista na formação integral (profissional e cidadã) do discente?",
            categoria: {
              nomeCategoria: "Quanto ao engajamento no Projeto"
            }
          }
        },
        {
          nota: 6,
          respostaTextual: null,
          pergunta: {
            idPergunta: 6,
            conteudoPergunta:
              "Demonstrou o impacto social do projeto no tocante ao compromisso com a transformação de problemática social e sua contribuição para empoderamento do cidadão, grupos, instituições, ONG’s e Organizações Sociais?",
            categoria: {
              nomeCategoria: "Quanto ao engajamento no Projeto"
            }
          }
        },
        {
          nota: 4,
          respostaTextual: null,
          pergunta: {
            idPergunta: 7,
            conteudoPergunta:
              "Demonstrou o protagonismo discente na execução do projeto?",
            categoria: {
              nomeCategoria: "Quanto ao engajamento no Projeto"
            }
          }
        },
        {
          nota: 10,
          respostaTextual: null,  
          pergunta: {
            idPergunta: 8,
            conteudoPergunta:
              "Demonstrou os desafios na realização do projeto, como tem garantido sua exequibilidade em nível satisfatório?",
            categoria: {
              nomeCategoria: "Quanto ao engajamento no Projeto"
            }
          }
        },
        {
          nota: null,
          respostaTextual: "",
          pergunta: {
            idPergunta: 9,
            conteudoPergunta: "Comentários Gerais sobre o projeto apresentado. (Opcional)",
            categoria: {
              idCategoria: 3,
              nomeCategoria: "Comum"
            }
          }
        }
      ]
    }
  }
})
