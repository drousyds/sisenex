import gql from "graphql-tag"

export const apresentacaoPorId = gql`
  query apresentacaoPorId($idApresentacao: Int!, $id2Apresentacao: String!) {
    apresentacao(
      input: {
        idApresentacao: $idApresentacao
        id2Apresentacao: $id2Apresentacao
      }
    ) {
      idApresentacao
      id2Apresentacao
      salaApresentacao
      areaTematica {
        nomeAreaTematica
      }
      horaApresentacao
    }
  }
`
