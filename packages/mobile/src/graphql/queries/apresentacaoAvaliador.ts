import gql from "graphql-tag"

export const apresentacaoAvaliador = gql`
  query apresentacaoAvaliador($idPessoa: String!, $idVinculoPessoa: String!) {
    apresentacoesAvaliador(
      idPessoa: $idPessoa
      idVinculoPessoa: $idVinculoPessoa
    ) {
      idApresentacao
      id2Apresentacao
      codigoApresentacao
      categoria {
        idCategoria
        nomeCategoria
      }
      horaApresentacao
      areaTematica {
        nomeAreaTematica
      }
      linkApresentacao
    }
  }
`
export const mockApresentacaoAvaliador = () => ({
  loading: false,
  error: false,
  data: {
    apresentacoesAvaliador: [
    {
      idApresentacao: 1,
      id2Apresentacao: "2020",
      codigoApresentacao: "80548384",
      categoria: {
        idCategoria: 2,
        nomeCategoria: "Vídeo"
      },
      horaApresentacao: "1602292373000",
      areaTematica: {
        nomeAreaTematica: "DIREITOS HUMANOS E JUSTIÇA"
      },
      linkApresentacao: "https://www.youtube.com/watch?v=4-JlooqIyFM&ab_channel=CineFix"
    },
    {
      idApresentacao: 2,
      id2Apresentacao: "2020",
      codigoApresentacao: "79DB28A4",
      categoria: {
        idCategoria: 1,
        nomeCategoria: "e-Tertúlia"
      },
      horaApresentacao: "1602292373000",
      areaTematica: {
        nomeAreaTematica: "DIREITOS HUMANOS E JUSTIÇA"
      },
      linkApresentacao: "https://www.youtube.com/watch?v=4-JlooqIyFM&ab_channel=CineFix"
    },
    {
      idApresentacao: 3,
      id2Apresentacao: "2020",
      codigoApresentacao: "80548384",
      categoria: {
        idCategoria: 2,
        nomeCategoria: "Vídeo"
      },
      horaApresentacao: "1602292373000",
      areaTematica: {
        nomeAreaTematica: "DIREITOS HUMANOS E JUSTIÇA"
      },
      linkApresentacao: "https://www.youtube.com/watch?v=4-JlooqIyFM&ab_channel=CineFix"
    },
    {
      idApresentacao: 4,
      id2Apresentacao: "2020",
      codigoApresentacao: "79DB28A4",
      categoria: {
        idCategoria: 1,
        nomeCategoria: "e-Tertúlia"
      },
      horaApresentacao: "1602292373000",
      areaTematica: {
        nomeAreaTematica: "DIREITOS HUMANOS E JUSTIÇA"
      },
      linkApresentacao: "https://www.youtube.com/watch?v=4-JlooqIyFM&ab_channel=CineFix"
    }
    ]
  }
})