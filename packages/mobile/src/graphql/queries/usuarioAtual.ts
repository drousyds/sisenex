import gql from "graphql-tag"

export const usuarioAtual = gql`
  query usuarioAtual {
    # Cache populado na hora do login, evitando realizar 2 requisicoes
    usuarioAtual @client {
      __typename
      idPessoa
      idVinculoPessoa
      nomeSocialPessoa
      token
      matriculaPessoa
      lotacaoPessoa
      avatarUrl
      monitorPessoa
      avaliadorPessoa
      gerentePessoa
    }
  }
`

export const mockUseUsuarioAtualQuery = () => ({
  loading: false,
  data: {
    idPessoa: "10",
    idVinculoPessoa: "2129",
    nomeSocialPessoa: "Danielle Rousy Dias da Silva",
    token: "AK31K2312F",
    matriculaPessoa: "1246817",
    lotacaoPessoa: "CI - Centro de informatica",
    avatarUrl:
      "https://upload.wikimedia.org/wikipedia/commons/d/d0/Monroe_in_Don%27t_Bother_to_Knock_%281952%29.jpg",
    monitorPessoa: 1,
    avaliadorPessoa: 0,
    gerentePessoa: 0
  }
})
