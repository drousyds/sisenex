import gql from "graphql-tag"
import { cardAvaliadorFragment } from "graphql/fragments/cardAvaliadorFragment"

export const pessoasApresentacao = gql`
  ${cardAvaliadorFragment}
  query pessoasApresentacao($idApresentacao: Int!, $id2Apresentacao: String!) {
    apresentacao(
      input: {
        idApresentacao: $idApresentacao
        id2Apresentacao: $id2Apresentacao
      }
    ) {
      idApresentacao
      id2Apresentacao
      avaliadores {
        ...cardAvaliadorFragment
      }
    }
  }
`

export const mockPessoasApresentacaoQuery = () => ({
  loading: false,
  error: false,
  data: {
    apresentacao: {
      pessoas: [
        {
          idPessoa: 1,
          nomePessoa: "Danielle Rousy Dias da Silva",
          lotacaoPessoa: "CI - Departamento de Informatica",
          matriculaPessoa: "1246917"
        },
        {
          idPessoa: 2,
          nomePessoa: "Eudisley Gomes dos Anjos",
          lotacaoPessoa: "CI - Departamento de Informatica",
          matriculaPessoa: "1246817"
        },
        {
          idPessoa: 3,
          nomePessoa: "Fernando Menezes Matos",
          lotacaoPessoa: "CI - Departamento de Informatica",
          matriculaPessoa: "1246817"
        },
        {
          idPessoa: 4,
          nomePessoa: "Marconilia Maria Dias Arnaud",
          lotacaoPessoa: "PU - Prefeitura Universitaria",
          matriculaPessoa: "1246817"
        }
      ]
    }
  }
})
