import gql from "graphql-tag"
import { cardAvaliadorFragment } from "graphql/fragments/cardAvaliadorFragment"
import { GerenciaProjetoQueryResult } from "generated/graphql"

export const gerenciaProjeto = gql`
  ${cardAvaliadorFragment}
  query gerenciaProjeto(
    $idProjeto: Int!
    $idApresentacao: Int!
    $id2Apresentacao: String!
  ) {
    projeto(idProjeto: $idProjeto) {
      idProjeto
      tituloProjeto
      disponibilidadeProjeto
      avaliadoresHabilitados {
        idPessoa
        idVinculoPessoa
      }
    }

    apresentacao(
      input: {
        idApresentacao: $idApresentacao
        id2Apresentacao: $id2Apresentacao
      }
    ) {
      idApresentacao
      id2Apresentacao
      avaliadores {
        ...cardAvaliadorFragment
        avaliacoes(idProjeto: $idProjeto) {
          projeto {
            idProjeto
          }
          media
        }
      }
    }
  }
`

export const mockGerenciaProjeto = (): Partial<GerenciaProjetoQueryResult> => ({
  loading: false,
  error: undefined,
  data: {
    projeto: {
      idProjeto: 1,
      tituloProjeto: "Projeto teste 2",
      disponibilidadeProjeto: 1,
      avaliadoresHabilitados: []
    },
    apresentacao: {
      idApresentacao: 1,
      id2Apresentacao: "2019",
      avaliadores: [
        {
          idPessoa: "1",
          idVinculoPessoa: "2",
          nomePessoa: "Vinicius Misael Mendes de Moura",
          lotacaoPessoa: "CI - DCI",
          matriculaPessoa: "20170200093",
          avaliacoes: [
            {
              projeto: {
                idProjeto: 1
              },
              media: 4
            }
          ]
        },
        {
          idPessoa: "2",
          idVinculoPessoa: "2",
          nomePessoa: "Luciano Junior",
          lotacaoPessoa: "CI - DCI",
          matriculaPessoa: "20170200093",
          avaliacoes: []
        }
      ]
    }
  }
})
