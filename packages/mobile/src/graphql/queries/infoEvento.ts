import gql from "graphql-tag"

// yarn codegen:update

export const infoEvento = gql`
    query infoEvento {
        eventos {
            nomeEvento
            dataInicioEvento
            dataFimEvento
            statusEvento
        }
    }
`