import gql from "graphql-tag"

export const fecharProjeto = gql`
  mutation fecharProjeto($idProjeto: Int!) {
    fecharProjeto(input: { idProjeto: $idProjeto }) {
      idProjeto
      disponibilidadeProjeto
    }
  }
`
