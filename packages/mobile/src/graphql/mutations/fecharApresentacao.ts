import gql from "graphql-tag"

export const fecharApresentacao = gql`
  mutation fecharApresentacao($codigoApresentacao: String!) {
    fecharApresentacao(input: { codigoApresentacao: $codigoApresentacao })
  }
`
