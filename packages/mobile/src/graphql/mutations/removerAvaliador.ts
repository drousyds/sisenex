import gql from "graphql-tag"

export const removerAvaliador = gql`
  mutation removerAvaliador(
    $idPessoa: String!
    $idVinculoPessoa: String!
    $idApresentacao: Int!
    $id2Apresentacao: String!
  ) {
    removerAvaliadorDaApresentacao(
      input: {
        idPessoa: $idPessoa
        idVinculoPessoa: $idVinculoPessoa
        idApresentacao: $idApresentacao
        id2Apresentacao: $id2Apresentacao
      }
    )
  }
`
