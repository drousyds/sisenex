import gql from "graphql-tag"

export const baterAvaliador = gql`
  mutation baterAvaliador(
    $idPessoa: String!
    $idVinculoPessoa: String!
    $idApresentacao: Int!
    $id2Apresentacao: String!
  ) {
    baterAvaliador(
      input: {
        idPessoa: $idPessoa
        idVinculoPessoa: $idVinculoPessoa
        idApresentacao: $idApresentacao
        id2Apresentacao: $id2Apresentacao
      }
    ) {
      idPessoa
      idVinculoPessoa
      nomePessoa
      lotacaoPessoa
      matriculaPessoa
    }
  }
`
