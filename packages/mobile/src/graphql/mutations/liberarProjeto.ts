import gql from "graphql-tag"

export const liberarProjeto = gql`
  mutation liberarProjeto($idProjeto: Int!) {
    liberarProjeto(input: { idProjeto: $idProjeto }) {
      idProjeto
      disponibilidadeProjeto
    }
  }
`
