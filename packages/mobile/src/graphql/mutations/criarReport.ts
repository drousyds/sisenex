import gql from "graphql-tag"

export const criarReport = gql`
  mutation criarReport(
    $conteudoReport: String!
    $idPessoa: String!
    $idVinculoPessoa: String!
    $emailReport: String!
    $idApresentacao: Int
    $id2Apresentacao: String
  ) {
    criarReport(
      input: {
        conteudoReport: $conteudoReport
        idPessoa: $idPessoa
        idVinculoPessoa: $idVinculoPessoa
        emailReport: $emailReport
        idApresentacao: $idApresentacao
        id2Apresentacao: $id2Apresentacao
      }
    ) {
      idReport
    }
  }
`
