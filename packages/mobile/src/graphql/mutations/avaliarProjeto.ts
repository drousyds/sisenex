import gql from "graphql-tag"

export const avaliarProjeto = gql`
  mutation AvaliarProjeto($input: avaliarProjetoInput!) {
    avaliarProjeto(input: $input)
  }
`
