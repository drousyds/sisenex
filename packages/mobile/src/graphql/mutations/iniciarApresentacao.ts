import gql from "graphql-tag"

export const iniciarApresentacao = gql`
  mutation iniciarApresentacao($codigoApresentacao: String!) {
    iniciarApresentacao(input: { codigoApresentacao: $codigoApresentacao }) {
      codigoApresentacao
      idApresentacao
      id2Apresentacao
      areaTematica {
        nomeAreaTematica
      }
      horaApresentacao
      salaApresentacao
      codigoApresentacao
    }
  }
`
