import gql from "graphql-tag"

export const liberarProjetoParaAvaliador = gql`
  mutation liberarProjetoParaUmAvaliador(
    $idPessoa: String!
    $idVinculoPessoa: String!
    $idProjeto: Int!
  ) {
    liberarProjetoParaUmAvaliador(
      input: {
        idPessoa: $idPessoa
        idVinculoPessoa: $idVinculoPessoa
        idProjeto: $idProjeto
      }
    ) {
      idProjeto
    }
  }
`
