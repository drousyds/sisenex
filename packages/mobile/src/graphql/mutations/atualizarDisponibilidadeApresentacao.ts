import gql from "graphql-tag"

export const atualizarDisponibilidadeApresentacao = gql`
  mutation atualizarDisponibilidadeApresentacao(
    $idApresentacao: Int!
    $id2Apresentacao: String!
    $disponibilidade: Int!
  ) {
    atualizarApresentacao(
      input: {
        idApresentacao: $idApresentacao
        id2Apresentacao: $id2Apresentacao
        disponibilidadeApresentacao: $disponibilidade
      }
    ) {
      idApresentacao
      id2Apresentacao
      areaTematica {
        nomeAreaTematica
      }
      horaApresentacao
      salaApresentacao
      codigoApresentacao
    }
  }
`

export const mockAtualizarDisponibilidadeApresentacao = {
  data: {},
  loading: false
}
