import gql from "graphql-tag"

export const fecharProjetoParaAvaliador = gql`
  mutation fecharProjetoParaUmAvaliador(
    $idPessoa: String!
    $idVinculoPessoa: String!
    $idProjeto: Int!
  ) {
    fecharProjetoParaUmAvaliador(
      input: {
        idPessoa: $idPessoa
        idVinculoPessoa: $idVinculoPessoa
        idProjeto: $idProjeto
      }
    ) {
      idProjeto
    }
  }
`
