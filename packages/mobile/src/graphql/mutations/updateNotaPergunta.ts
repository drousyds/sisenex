import gql from "graphql-tag"

export const updateNotaPergunta = gql`
  mutation updateNotaPergunta(
    $idProjeto: Int!
    $idPergunta: Int!
    $nota: Float!
  ) {
    updateNotaPergunta(
      idProjeto: $idProjeto
      idPergunta: $idPergunta
      nota: $nota
    ) @client
  }
`
