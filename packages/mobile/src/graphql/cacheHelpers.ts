import ApolloClient from "apollo-client"
import { DataProxy } from "apollo-cache"
import {
  AvaliacaoProjetoQuery,
  AvaliacaoProjetoQueryVariables,
  UsuarioAtualQuery,
  AvaliacaoProjetoFragment,
  Projeto,
  AtualizarApresentacaoInput,
  AtualizarDisponibilidadeApresentacaoMutation,
  ApresentacaoAtual,
  IniciarApresentacaoMutation,
  Pessoa,
  CardAvaliadorFragmentFragment,
  Apresentacao
} from "generated/graphql"
import { usuarioAtual as usuarioAtualDocument } from "./queries/usuarioAtual"
import { avaliacaoProjeto } from "./queries/avaliacaoProjeto"
import client from "./client"
import { apresentacaoAtual } from "./queries/apresentacaoAtual"
import { formatDate } from "helpers/formatDate"
import gql from "graphql-tag"
import { cardAvaliadorFragment } from "./fragments/cardAvaliadorFragment"

type Client = ApolloClient<any> | DataProxy

export const getApresentacaoAtual = () => {
  const apresentacao: ApresentacaoAtual = client.readQuery({
    query: apresentacaoAtual
  }).apresentacaoAtual!
  return apresentacao
}

export const fillAvaliacaoProjetoCache = (
  client: Client,
  args: Pick<Projeto, "idProjeto">
) => {
  const usuarioAtualData = client.readQuery<UsuarioAtualQuery>({
    query: usuarioAtualDocument
  })
  if (!usuarioAtualData) return false
  const data = client.readQuery<
    AvaliacaoProjetoQuery,
    AvaliacaoProjetoQueryVariables
  >({
    query: avaliacaoProjeto,
    variables: {
      idPessoa: usuarioAtualData.usuarioAtual.idPessoa,
      idVinculoPessoa: usuarioAtualData.usuarioAtual.idVinculoPessoa,
      idProjeto: args.idProjeto
    }
  })
  if (!data) return false
  if (!data.avaliacao) {
    // Setando nota como null apesar de isso nao ser compativel com o schema
    // @ts-ignore
    const notasDefault: AvaliacaoProjetoFragment["notas"] = data.perguntas!.map(
      pergunta => {
        return {
          __typename: "NotaPergunta",
          nota: null,
          respostaTextual: null,
          pergunta: {
            __typename: "Pergunta",
            idPergunta: pergunta!.idPergunta,
            conteudoPergunta: pergunta!.conteudoPergunta,
            categoria: {
              __typename: "Categoria",
              idCategoria: pergunta!.categoria!.idCategoria,
              nomeCategoria: pergunta!.categoria!.nomeCategoria
            }
          }
        }
      }
    )
    data.avaliacao = {
      __typename: "Avaliacao",
      projeto: {
        __typename: "Projeto",
        idProjeto: args.idProjeto
      },
      notas: notasDefault
    }
    client.writeQuery({
      query: avaliacaoProjeto,
      variables: {
        idPessoa: usuarioAtualData.usuarioAtual.idPessoa,
        idVinculoPessoa: usuarioAtualData.usuarioAtual.idVinculoPessoa,
        idProjeto: args.idProjeto
      },
      data
    })
  }
}

export const updateNotaCache = (
  client: Client,
  args: Pick<Projeto, "idProjeto"> & { idPergunta: number; nota: number }
) => {
  const usuarioAtualData = client.readQuery<UsuarioAtualQuery>({
    query: usuarioAtualDocument
  })
  if (!usuarioAtualData) return false

  const avaliacaoProjetoCached = client.readQuery<AvaliacaoProjetoQuery>({
    query: avaliacaoProjeto,
    variables: {
      idProjeto: args.idProjeto,
      idPessoa: usuarioAtualData.usuarioAtual.idPessoa,
      idVinculoPessoa: usuarioAtualData.usuarioAtual.idVinculoPessoa
    }
  })

  if (!(avaliacaoProjetoCached && avaliacaoProjetoCached.avaliacao))
    return false

  const index = avaliacaoProjetoCached.avaliacao.notas.findIndex(
    elem => elem!.pergunta.idPergunta == args.idPergunta
  )

  avaliacaoProjetoCached.avaliacao.notas[index]!.nota = args.nota

  client.writeQuery<AvaliacaoProjetoQuery>({
    query: avaliacaoProjeto,
    variables: {
      idProjeto: args.idProjeto,
      idPessoa: usuarioAtualData.usuarioAtual.idPessoa,
      idVinculoPessoa: usuarioAtualData.usuarioAtual.idVinculoPessoa
    },
    data: avaliacaoProjetoCached
  })

  return true
}

export const updateRespostaTextual = (
  client: Client,
  args: Pick<Projeto, "idProjeto"> & {
    idPergunta: number
    respostaTextual: string
  }
) => {
  const usuarioAtualData = client.readQuery<UsuarioAtualQuery>({
    query: usuarioAtualDocument
  })
  if (!usuarioAtualData) return false

  const avaliacaoProjetoCached = client.readQuery<AvaliacaoProjetoQuery>({
    query: avaliacaoProjeto,
    variables: {
      idProjeto: args.idProjeto,
      idPessoa: usuarioAtualData.usuarioAtual.idPessoa,
      idVinculoPessoa: usuarioAtualData.usuarioAtual.idVinculoPessoa
    }
  })

  if (!(avaliacaoProjetoCached && avaliacaoProjetoCached.avaliacao))
    return false

  const index = avaliacaoProjetoCached.avaliacao.notas.findIndex(
    elem => elem!.pergunta.idPergunta == args.idPergunta
  )

  avaliacaoProjetoCached.avaliacao.notas[index]!.respostaTextual =
    args.respostaTextual

  client.writeQuery<AvaliacaoProjetoQuery>({
    query: avaliacaoProjeto,
    variables: {
      idProjeto: args.idProjeto,
      idPessoa: usuarioAtualData.usuarioAtual.idPessoa,
      idVinculoPessoa: usuarioAtualData.usuarioAtual.idVinculoPessoa
    },
    data: avaliacaoProjetoCached
  })

  return true
}

export const updateApresentacaoAtualCache = (
  data: IniciarApresentacaoMutation
) => {
  const apresentacaoAtualizada = data.iniciarApresentacao!
  const temp: ApresentacaoAtual = {
    __typename: "ApresentacaoAtual",
    idApresentacao: apresentacaoAtualizada.idApresentacao,
    id2Apresentacao: apresentacaoAtualizada.id2Apresentacao,
    codigoApresentacao:
      apresentacaoAtualizada.codigoApresentacao || "Indisponivel",
    salaApresentacao: apresentacaoAtualizada.salaApresentacao || "Indisponivel",
    nomeAreaTematica:
      (apresentacaoAtualizada.areaTematica &&
        apresentacaoAtualizada.areaTematica.nomeAreaTematica) ||
      "Indisponivel",
    horaApresentacao:
      (apresentacaoAtualizada.horaApresentacao &&
        formatDate(apresentacaoAtualizada.horaApresentacao)) ||
      "Indisponivel"
  }

  client.writeQuery({
    query: apresentacaoAtual,
    data: {
      apresentacaoAtual: temp
    }
  })
}

export const updateStatusAvaliador = (
  cache: DataProxy,
  data: {
    idProjeto: Projeto["idProjeto"]
    idPessoa: Pessoa["idPessoa"]
    idVinculoPessoa: Pessoa["idVinculoPessoa"]
  },
  habilitado: boolean
) => {
  const avaliadoresHabilitadosFragment = gql`
    fragment AvaliadoresAbilitados on Projeto {
      avaliadoresHabilitados {
        idPessoa
        idVinculoPessoa
      }
    }
  `

  const idProjeto = `Projeto-${data.idProjeto}`

  const projetoFromCache: any = cache.readFragment({
    id: idProjeto,
    fragment: avaliadoresHabilitadosFragment
  })

  const novoAvalidoresHabilitados = habilitado
    ? [
        ...projetoFromCache.avaliadoresHabilitados,
        {
          __typename: "Avaliador",
          idPessoa: data.idPessoa,
          idVinculoPessoa: data.idVinculoPessoa
        }
      ]
    : projetoFromCache.avaliadoresHabilitados.filter(
        (elem: any) =>
          elem.idPessoa != data.idPessoa ||
          elem.idVinculoPessoa != data.idVinculoPessoa
      )

  cache.writeFragment({
    id: idProjeto,
    fragment: avaliadoresHabilitadosFragment,
    data: {
      __typename: "Projeto",
      avaliadoresHabilitados: novoAvalidoresHabilitados
    }
  })
}

export const cacheBaterAvaliador = (
  cache: DataProxy,
  data: {
    baterAvaliador?: CardAvaliadorFragmentFragment
  }
) => {
  const apresentacao: ApresentacaoAtual = client.readQuery({
    query: apresentacaoAtual
  }).apresentacaoAtual!

  if (data && data.baterAvaliador) {
    const fragment = gql`
      fragment avaliadoresApresentacao on Apresentacao {
        avaliadores {
          idPessoa
          idVinculoPessoa
          nomePessoa
          lotacaoPessoa
          matriculaPessoa
        }
      }
    `
    const apresentacaoCacheId = `Apresentacao-${apresentacao.idApresentacao}-${
      apresentacao.id2Apresentacao
    }`

    const cachedData: Apresentacao | null = cache.readFragment({
      id: apresentacaoCacheId,
      fragment
    })

    const avaliadoresAtuais = (cachedData && cachedData.avaliadores) || []

    if (
      !data.baterAvaliador ||
      data.baterAvaliador.idPessoa == undefined ||
      data.baterAvaliador.idVinculoPessoa == undefined
    )
      return

    const avaliadorExiste =
      avaliadoresAtuais.find(
        elem =>
          elem.idPessoa == data.baterAvaliador!.idPessoa &&
          data.baterAvaliador!.idVinculoPessoa
      ) != undefined

    if (!avaliadorExiste) {
      cache.writeFragment({
        id: apresentacaoCacheId,
        fragment,
        data: {
          __typename: "Apresentacao",
          avaliadores: [
            ...avaliadoresAtuais,
            { __typename: "Avaliador", ...data.baterAvaliador }
          ]
        }
      })
    }
  }
}

export const cacheRemoverAvaliador = (
  cache: DataProxy,
  data: {
    idVinculoPessoa: Pessoa["idPessoa"]
    idPessoa: Pessoa["idVinculoPessoa"]
  }
) => {
  const apresentacao: ApresentacaoAtual = client.readQuery({
    query: apresentacaoAtual
  }).apresentacaoAtual!

  const fragment = gql`
    fragment avaliadoresApresentacao on Apresentacao {
      avaliadores {
        idPessoa
        idVinculoPessoa
        nomePessoa
        lotacaoPessoa
        matriculaPessoa
      }
    }
  `
  const apresentacaoCacheId = `Apresentacao-${apresentacao.idApresentacao}-${
    apresentacao.id2Apresentacao
  }`

  const cachedData: Apresentacao | null = cache.readFragment({
    id: apresentacaoCacheId,
    fragment
  })

  const avaliadoresAtuais = (cachedData && cachedData.avaliadores) || []

  cache.writeFragment({
    id: apresentacaoCacheId,
    fragment,
    data: {
      __typename: "Apresentacao",
      avaliadores: avaliadoresAtuais.filter(
        elem =>
          elem.idPessoa != data.idPessoa ||
          elem.idVinculoPessoa != data.idVinculoPessoa
      )
    }
  })
}
