import gql from "graphql-tag"

export const cardAvaliadorFragment = gql`
  fragment cardAvaliadorFragment on Avaliador {
    idPessoa
    idVinculoPessoa
    nomePessoa
    lotacaoPessoa
    matriculaPessoa
  }
`
