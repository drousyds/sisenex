import { ApolloClient } from "apollo-client"
import { BatchHttpLink } from "apollo-link-batch-http"
import {
  InMemoryCache,
  IntrospectionFragmentMatcher,
  defaultDataIdFromObject
} from "apollo-cache-inmemory"
import { onError } from "apollo-link-error"
import { ApolloLink } from "apollo-link"
import {
  Apresentacao,
  Projeto,
  Avaliacao,
  Monitor,
  Gerente,
  Pergunta,
  Categoria,
  Avaliador
} from "generated/graphql"
import { REACT_NATIVE_API_URL } from "constants/constants"
//import Toast from "react-native-simple-toast"

const introspectionQueryResultData = require("../generated/fragmentTypes.json")
const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData
})

const cache = new InMemoryCache({
  fragmentMatcher,
  dataIdFromObject: object => {
    switch (object.__typename) {
      case "Apresentacao": {
        const obj = object as Apresentacao
        return `${obj.__typename}-${obj.idApresentacao}-${obj.id2Apresentacao}`
      }
      case "Avaliacao": {
        const obj = object as Avaliacao
        return `${obj.__typename}-${obj.projeto.idProjeto}`
      }
      // case "NotaPergunta": {
      //   const obj = object as NotaPergunta
      //   return `${obj.__typename}-${obj.pergunta.idPergunta}`
      // }
      case "Pergunta": {
        const obj = object as Pergunta
        return `${obj.__typename}-${obj.idPergunta}`
      }
      case "Categoria": {
        const obj = object as Categoria
        return `${obj.__typename}-${obj.idCategoria}`
      }
      case "Projeto": {
        const obj = object as Projeto
        return `${obj.__typename}-${obj.idProjeto}`
      }
      case "Avaliador": {
        const obj = object as Avaliador
        return `${obj.__typename}-${obj.idPessoa}-${obj.idVinculoPessoa}`
      }
      case "Monitor": {
        const obj = object as Monitor
        return `${obj.__typename}-${obj.idPessoa}-${obj.idVinculoPessoa}`
      }
      case "Gerente": {
        const obj = object as Gerente
        return `${obj.__typename}-${obj.idPessoa}-${obj.idVinculoPessoa}`
      }
      default:
        return defaultDataIdFromObject(object) // fall back to default handling
    }
  }
})

const client = new ApolloClient({
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path }) =>
          console.log(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
          )
        )
      if (networkError) {
        //Toast.show("Problemas de conexão!")
      }
    }),
    new BatchHttpLink({
      batchInterval: 60,
      uri: REACT_NATIVE_API_URL + "/graphql/"
    })
  ]),
  resolvers: {
    Mutation: {
      updateNotaPergunta: (_root, variables, { cache, getCacheKey }) => {}
    },
    Query: {}
  },
  cache
})

export default client
