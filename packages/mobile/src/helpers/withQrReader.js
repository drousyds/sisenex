import React from "react"
import { View } from "react-native"
import { Camera } from "expo-camera"
import { CustomActionButton } from "components/ActionButton"

export default Component =>
  class QrReaderWrapper extends React.Component {
    state = {
      hasCameraPermissions: null,
      cameraOpen: false
    }

    onBarCodeScanned = ({ data }) => {
      this.setState({ cameraOpen: false }, () => {
        this.state.onQr(data)
      })
    }

    async componentWillMount() {
      const { status } = await Camera.requestCameraPermissionsAsync()
      this.setState({ hasCameraPermissions: status === "granted" })
    }

    toggleCameraVisibility = () => {
      this.setState({ cameraOpen: !this.state.cameraOpen })
    }

    openQrScanner = onQr => {
      this.setState({ onQr, cameraOpen: true })
    }

    onPress = () => {
      this.toggleCameraVisibility()
    }

    render() {
      let { hasCameraPermissions, cameraOpen } = this.state
      if (hasCameraPermissions === null)
        return (
          <Component
            {...this.props}
            toggleCamera={this.toggleCameraVisibility}
          />
        )
      else if (hasCameraPermissions === false) {
        return (
          <Component
            {...this.props}
            toggleCamera={this.toggleCameraVisibility}
          />
        )
      } else if (cameraOpen) {
        return (
          <View
            style={{
              flex: 1
            }}
          >
            <Camera
              style={{ flex: 1 }}
              onBarCodeScanned={this.onBarCodeScanned}
            >
              <CustomActionButton
                icon="arrow-back"
                actions={undefined}
                onPress={this.onPress}
              />
            </Camera>
          </View>
        )
      } else {
        return <Component {...this.props} openQrScanner={this.openQrScanner} />
      }
    }
  }

