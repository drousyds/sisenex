/**
 * Filters pending projects, discarding projects that are already visible as finished projects
 * @param {[Project]} pendingProjects Array of pending projects
 * @param {[Project]} finishedProjects Array of finished projects
 */
const filterVisibility = (pendingProjects, finishedProjects) => {
  const inFinished = pending =>
    finishedProjects.find(elem => elem.idProjeto == pending.idProjeto) !==
    undefined
  return pendingProjects.filter(elem => elem.wireframe || !inFinished(elem))
}

export { filterVisibility }
export default filterVisibility
