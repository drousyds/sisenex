import { useEffect } from "react"
import { Camera } from "expo-camera"

export const withCameraPermission = () => {
  useEffect(() => {
    const getPermission = async () => {
      const { status } = await Camera.getCameraPermissionsAsync()
      if (status != "granted") Camera.requestCameraPermissionsAsync()
    }
    getPermission()
  })
}
