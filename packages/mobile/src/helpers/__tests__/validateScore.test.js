import validateScore from "../validateScore"

describe("Validate score works", () => {
  it("Rejects non numeric strings", () => {
    expect(validateScore("vinicius")).toBe(false)
  })
  it("Accepts string integer", () => {
    expect(validateScore("6")).toBe(true)
  })
  it("Accepts integer", () => {
    expect(validateScore(6)).toBe(true)
  })
  it("Accepts float string", () => {
    expect(validateScore("1,3")).toBe(true)
  })
  it("Reject five digits", () => {
    expect(validateScore("10,333")).toBe(false)
  })
  it("Accept less than 0", () => {
    expect(validateScore("0,01")).toBe(true)
  })
  it("Reject float bigger than 10 ", () => {
    expect(validateScore("10,01")).toBe(false)
  })
})
