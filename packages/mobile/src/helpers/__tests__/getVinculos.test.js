import getVinculos from "../getVinculos"

const mockData = {
  servidor: [],
  discente: [
    {
      curso: {
        nome: "CIÊNCIA DA COMPUTAÇÃO",
        nivel: "G",
        idCurso: 1626669
      },
      matricula: 20170200093,
      pessoa: {
        nome: "VINICIUS MISAEL MENDES DE MOURA",
        sexo: "M",
        email: "vinicius_gbapb@hotmail.com",
        celular: "8388897680",
        telefone: "8332715092",
        tipo: "F",
        nomeResumido: "VINICIUS MISAEL",
        primeiroNome: "VINICIUS",
        nomeSocialOficial: "VINICIUS MISAEL MENDES DE MOURA",
        idPessoa: 384574
      },
      email: "vinicius_gbapb@hotmail.com",
      telefone: "8332715092",
      matriculaNome: "20170200093 - VINICIUS MISAEL MENDES DE MOURA",
      tipoString: "REGULAR",
      idVinculo: 306643
    }
  ]
}

const expected = [
  {
    idPessoa: 384574,
    idVinculoPessoa: 306643,
    nomePessoa: "VINICIUS MISAEL MENDES DE MOURA",
    nomeSocialPessoa: "VINICIUS MISAEL MENDES DE MOURA",
    telefonePessoa: "8332715092",
    emailPessoa: "vinicius_gbapb@hotmail.com",
    lotacaoPessoa: null,
    matriculaPessoa: 20170200093,
    aptidaoPessoa: 0,
    monitorPessoa: 1,
    avaliadorPessoa: 0,
    gerentePessoa: 0
  }
]

describe("getVinculos works", () => {
  it("Works for mock data", () => {
    expect(getVinculos(mockData)).toEqual(expected)
  })
})
