export const formatDate = (timestamp: string) => {
  var a = new Date(parseInt(timestamp))
  var year = a.getUTCFullYear()
  var month = a.getUTCMonth() + 1
  var date = a.getUTCDate()
  var hour = a.getUTCHours()
  var min = a.getUTCMinutes()
  var sec = a.getUTCSeconds()
  var time = `${date}/${month}/${year} ${hour}:${String(min).padStart(2, "0")}`
  return time
}
