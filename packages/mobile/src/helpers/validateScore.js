export default value => {
  if (value > 10 || value < 0) return false
  return /^(\d{1,2}|\d(,|\.)\d{1,2})$/.test(value.toString())
}
