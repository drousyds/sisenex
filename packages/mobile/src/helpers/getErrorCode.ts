import { ApolloError } from "apollo-client";

export default (error: ApolloError | undefined) : number | undefined => {
  return error && error.graphQLErrors[0] && error.graphQLErrors[0].extensions && error.graphQLErrors[0].extensions.code
}