import { Pessoa } from "generated/graphql"

export default (data: any): Pessoa[] => {
  let vinculosDiscente = data.discente
  let vinculosServidor = data.servidor
  let vinculos: Pessoa[] = []

  const getDataFromVinculoDiscente = (vinculo: any) => ({
    idPessoa: `${vinculo.pessoa.idPessoa}`,
    idVinculoPessoa: `${vinculo.idVinculo}`,
    nomePessoa: vinculo.pessoa.nome,
    nomeSocialPessoa: vinculo.pessoa.nomeSocialOficial,
    telefonePessoa: vinculo.telefone,
    emailPessoa: vinculo.email,
    lotacaoPessoa: (vinculo.curso && vinculo.curso.nome) || undefined,
    matriculaPessoa: `${vinculo.matricula}`,
    aptidaoPessoa: 0,
    monitorPessoa: 1,
    avaliadorPessoa: 0,
    gerentePessoa: 0
  })

  const getDataFromVinculoServidor = (vinculo: any) => ({
    idPessoa: `${vinculo.pessoa.idPessoa}`,
    idVinculoPessoa: `${vinculo.idVinculo}`,
    nomePessoa: vinculo.pessoa.nome,
    nomeSocialPessoa: vinculo.pessoa.nomeSocialOficial,
    telefonePessoa: vinculo.telefone,
    emailPessoa: vinculo.pessoa.email,
    lotacaoPessoa: (vinculo.unidade && vinculo.unidade.sigla) || undefined,
    matriculaPessoa: `${vinculo.siape}`,
    aptidaoPessoa: 0,
    monitorPessoa: 0,
    avaliadorPessoa: 1,
    gerentePessoa: 0
  })

  vinculosDiscente.map((elem: any) =>
    vinculos.push(getDataFromVinculoDiscente(elem))
  )
  vinculosServidor.map((elem: any) =>
    vinculos.push(getDataFromVinculoServidor(elem))
  )

  return vinculos
}
