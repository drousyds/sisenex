import { MaterialDialog } from "components/Dialogs"
import React from "react"

export default Component =>
  class ConfirmationDialogWrapper extends React.Component {
    state = {
      modalVisible: false,
      headerText: null,
      content: null,
      onConfirm: () => false
    }

    openDialog = ({ headerText, content, onConfirm }) => {
      this.setState({
        headerText,
        content,
        onConfirm: () => {
          this.setState(
            {
              modalVisible: false
            },
            onConfirm
          )
        },
        modalVisible: true
      })
    }

    toggle = () => {
      this.setState({
        modalVisible: !this.state.modalVisible
      })
    }

    render() {
      return (
        <React.Fragment>
          <MaterialDialog
            actionName="CONFIRMAR"
            toggle={this.toggle}
            visible={this.state.modalVisible}
            onActionPress={this.state.onConfirm}
            headerText={this.state.headerText}
            content={this.state.content}
          />
          <Component openConfirmationDialog={this.openDialog} {...this.props} />
        </React.Fragment>
      )
    }
  }
