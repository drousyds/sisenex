import React from "react"
import { Text, View } from "react-native"
import { MaterialCommunityIcons } from "@expo/vector-icons"
import Colors from "assets/css/colors"
import { MaterialDialog } from "components/Dialogs"

const StatusLine = ({ done, name }) => (
  <View style={{ flexDirection: "row", alignItems: "center" }}>
    <MaterialCommunityIcons
      name={done ? "checkbox-marked-circle" : "close-circle"}
      size={22}
      color={done ? Colors.success : Colors.error}
    />
    <Text
      numberOfLines={1}
      style={{ paddingLeft: 9, fontSize: 16, paddingRight: 10 }}
    >
      {name}
    </Text>
  </View>
)

export default Component =>
  class StatusDialog extends React.Component {
    state = {
      headerText: "Status de avaliação",
      data: [],
      dialogOpen: false
    }

    toggleModal = () => {
      this.setState({
        dialogOpen: !this.state.dialogOpen
      })
    }

    closeDialog = () => {
      this.setState({
        dialogOpen: false
      })
    }

    renderContent = () => {
      return this.state.data.map((elem, index) => (
        <StatusLine key={index} done={elem.done} name={elem.name} />
      ))
    }

    openStatusModal = ({ data }) => {
      this.setState({
        data,
        dialogOpen: true
      })
    }

    render() {
      return (
        <React.Fragment>
          <Component {...this.props} openStatusModal={this.openStatusModal} />
          <MaterialDialog
            visible={this.state.dialogOpen}
            actionName="OK"
            hideCancel
            toggle={this.toggleModal}
            headerText={this.state.headerText}
            content={<React.Fragment>{this.renderContent()}</React.Fragment>}
            onActionPress={this.closeDialog}
          />
        </React.Fragment>
      )
    }
  }
