import * as Updates from "expo-updates"

export default () => {
  const channel = Updates.channel
  const devUrl = "https://150.165.202.220/"
  const stagingUrl = "http://150.165.202.197" 

  if (process.env.NODE_ENV == "development") return devUrl

  switch (channel) {
    case "devel":
      return devUrl
    case "staging":
      return stagingUrl
    case "production":
      return "http://sisenex.ufpb.br"
    default:
      console.warn("No release channel, falling back to devel")
      return devUrl
  }
}