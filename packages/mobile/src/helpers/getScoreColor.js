import Colors from "assets/css/colors"

export default score => {
  if (score < 5) return Colors.error
  else if (score < 7) return Colors.warn
  else return Colors.success
}
