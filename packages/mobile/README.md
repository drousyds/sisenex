# Desenvolvimento

## Revendo Redux

Essa aplicação utiliza do Redux para gerência de estado no lado do cliente. Nessa biblioteca são implementados os principios de fluxo unidirecional de dados e de fonte unica de verdade: todo estado da aplicação será encapsulado num unico objeto que será visível para todos os níveis da interface e toda mutação será descrita através de redutores. Essas são funções puras que retornam um novo estado a partir de um estado inicial de uma ação emitida.

## Sobre a estrutura da aplicação

As versões mobile e web são estruturadas de forma semelhante, tendo todo codigo relacionado ao modelo e lógica de negocio em ./src/redux e todo codigo pertinente apenas à camada de visualização em ./src/components.

## Modelo:

### ./src/redux/*/actions

Esse diretório contém as definições de quais ações - síncronas e assíncronas - serão lançadas pela camada de visualização, bem como funções responsaveis por criar cada objeto de ação.\
\
A responsabilidade do processamento de ações assíncronas será dos geradores de ações e toda definição de tipo de ação deve ser exportada em **./src/redux/actions/types.js**

Exemplo de arquivos de tipos:

```javascript
const LOGIN = "LOGIN";
const ADD_PROJECT = "BEGIN_AVALIATION";

export { ADD_PROJECT, LOGIN };
```

Exemplo de geradores de ação

```javascript
import {
  ADD_PROJECT,
  GET_PROJECT_INFO,
  GET_PROJECT_INFO_SUCCESS,
  GET_PROJECT_INFO_ERROR
} from "./types";

//Gerador de ação sincrona
function addProject(project) {
  return {
    type: ADD_PROJECT,
    payload: project
  };
}

//Gerador de ação assincrona
function getProjectInfo(id) {
  return dispatch => {
    dispatch({ type: GET_PROJECT_INFO });
    fetch("http://api/project_info?id=" + id)
      .then(
        response => response.json(),
        error =>
          dispatch({
            type: GET_PROJECT_INFO_ERROR,
            payload: error.toString()
          })
      )
      .then(json =>
        dispatch({
          type: GET_PROJECT_INFO_SUCCESS,
          payload: json
        })
      );
  };
}
```

### ./src/redux/*/reducers

Nesse diretorio serão definidos os redutores, funções do tipo **(state, action) -> state** responsáveis por descreverem mutações do estado da aplicação. Todo redutor será, obrigatoriamente, uma função pura. A falha em seguir tal padrão resultará em bugs.
\
Exemplo de redutor:

```javascript
const initialState = {
    loggedIn: false,
    userInfo: undefined
}
const authReducer(state = initialState, action) => {
    switch(action.type){
        case USER_LOGGED_IN:
            return {
                loggedIn: true,
                userInfo: action.payload
            }
        case USER_LOGGED_OUT:
            return {
                loggedIn: false,
                userInfo: undefined
            }
        default:
            return state
    }
}
```

### ./src/redux/*/selectors

Contém seletores de estado memorizados, que são funções do tipo **(state)->data** que retornam uma fatia do estado da aplicação e possuem cache para memorização do resultado da seleção, recalculando apenas caso o estado sofra mutação.
\
Exemplo:

```javascript
import { createSelector } from "reselect";

const inputSelectors = [state => state.data.projects];
const endSelector = projects => {
  return projects.filter(elem => elem.finished == true);
};
const getFinishedProjects = createSelector(
  inputSelectors,
  endSelector
);

export { getFinieshedProjects };
```

### ./src/navigation

Esse diretório contém as definições gerais de navegação da aplicação. Seu objetivo é centralizar essas definições e encapsular a lógica de navegação.

### ./src/helpers

Esse diretório contém funções que servem como auxiliares para a implementação da telas da aplicação. O objetivo é centralizar funções auxiliares que são utilizadas em mais de uma parte da aplicação.

### ./src/graphql

Esse diretório contém as querys, as mutations e os fragments que são utilizados para fazer as requisições graphql na implementação das telas. O objetivo é encapsular a definição do código de requisição para possibilitar uma melhor organização na implementação das telas.

### ./src/hooks

Esse diretório contém hooks personalizados que são utilizados ao longo da aplicação. Seu principal objetivo é centralizar hooks que são usados em diferentes pontos da aplicação e ajudar na implementação do sistema a partir do encapsulamento de sua implementação.

## Visualização:

### ./src/views

Nesse diretorio estão localizadas todas as telas da aplicação, essas utilizam de muitos dos componentes definidos em **./src/components** e são responsaveis por definir layout e permitir navegação para outras telas.

### ./src/components

Esses componentes são presentacionais e devem ser exportados por **./src/components/index.js**. Dada sua natureza, possuem estilo e são geralmente funcionais puros.

## Inicialização

    $ cd ./mobile
    $ yarn start ou expo start # Pode necessitar de permição sudo
