# Monitor model

## State shape
``` javascript
//Application state tree
{
    //...rest of the state tree

    "apresentacao": {
      idApresentacao: integer,
      id2Apresentacao: integer
    }

    "finishedProjects" : [{
        "idProjeto": integer,
        "tituloProjeto": string,
        "avaliacoes": [{
            "idPergunta": integer,
            "notaAvaliacao": float,
        }]
        //Show spinner?
        "loading": bool,
    }],

    "errorReducer": {
        "errorState": bool,
        "error": string
    },

    "pendingProjects": [{
        "idProjeto": integer,
        "tituloProjeto": string,
        "aptidaoPessoa": bool
        //Show spinner?
        "loading": bool,
    }]

    "reviews": [{
        "idProjeto": [{
            "idPergunta": integer,
            "notaAvaliacao": float
        }]
    }]

    "questions": [{
        "idPergunta": string,
        "conteudoPergunta": string,
        "categoriaPergunta": string,
    }],
}
```

## Actions

![alt text](../assets/mobile-bpmn.svg "BPMN diagram")

### MA1 - Get apresentacao
Get identifier of apresentacao currently associated with idPessoa and idVinculoPessoa
#### Arguments
> - idPessoa: string // Mock pessoa = 1
> - idVinculoPessoa: string, // Mock pessoa = 3121

#### Request
//TODO
> GET /sisenex/avaliadores/{idPessoa}/{idVinculoPessoa}/avaliacoes

#### Response
``` javascript
//Success
{
    "idApresentacao": integer,
    "id2Apresentacao": integer,
}
//Case without apresentacao
{
    "status": 204
}
//Error
{
"erro": {
    "mensagem": string,
}
}
```

### MA2 - Fetch reviewed projects
Fetch projects previously reviewed
#### Arguments
> - idPessoa: string // Mock pessoa = 1
> - idVinculoPessoa: string, // Mock pessoa = 3121

#### Request
> GET /sisenex/avaliadores/{idPessoa}/{idVinculoPessoa}/avaliacoes

#### Response
``` javascript
//Success
{
    "resultados": [{
        "idProjeto": integer,
        "tituloProjeto": string,
        "dataAvaliacao": Date,
        "avaliacoes": [
            {
                "idPergunta": integer,
                "notaAvaliacao": float,
            }
        ]
    }]
}
//Error
{
    "erro": {
        "mensagem": string,
    }
}
```

### MA3 - Get open projects
Fetch projects that are open for review. Loop every N seconds
#### Arguments
> - idPessoa: string // Mock pessoa = 1
> - idVinculoPessoa: string, // Mock pessoa = 3121
> - idApresentacao: integer,
> - id2Apresentacao: integer

#### Request
> GET /sisenex/avaliadores/{idPessoa}/{idVinculoPessoa}/avaliacoes

#### Response
``` javascript
//Success
{
    "resultados": [{
        "idProjeto": integer,
        "tituloProjeto": string,
        "aptidaoPessoa": bool,
        ]
    }]
}
//Error
//Go back to QRVerificationScreen
{
    "erro": {
        "mensagem": string,
    }
}
```
#### Effect
> - Populates PendingProjectsScreen
> - On error, navigates to QRVerificationScreen

### MA4 - Submit review
Submits review for a specific project
#### Arguments
> - idPessoa: string
> - idVinculoPessoa: string
> - idProjeto: integer,
> - avaliacoes: [{
>       idPergunta: integer,
>       notaAvaliacao: float
>   }]

##### Request
POST ./sisenex/avaliadores/{idPessoa}/{idVinculoPessoa}/avaliacoes 
DATA: {
      idProjeto,
      avaliacoes: [{
          idPergunta: integer,
          notaAvaliacao: float
       }]
}
##### Response
Check http status
##### Effect
> - Submits project review
> - If successful, hide project in PendingProjectsScreen and show it in FinishedProjectsScreen

### MA5 - Fetch questions
Get questions
#### Arguments
> none

#### Request
> GET /sisenex/perguntas

#### Response
```javascript
//Success
{
    "resultados": [{
        "idPergunta": string,
        "conteudoPergunta": string,
        "nomeCategoria": string,
    }]
}

//Error
{
    "erro": {
        "mensagem": string,
    }
}
```

## Selectors
### SA1 Get pending projects 
### SA2 Get finished projects
### SA3 Get reviews
Get a specific project's scores
