# Reviewer model

The model layer for the monitor matches as closely as possible with the backend schema for Apresentacoes and Projects.

## State shape

```javascript
//Application state tree
{
    //...rest of the state
    "apresentacoes": [{
        "idApresentacao": integer,
        "id2Apresentacao": string,
        "salaApresentacao": string,
        "dataFimApresentacao": Date,
        "dataInicioApresentacao": Date,
        "disponibilidadeApresentacao": bool,
        "wireframe": bool,
    }],

    "reviewers": [{
        "idPessoa": string,
        "idVinculo": string,
        "nomeSocialPessoa":string,
        "idApresentacao": integer,
        "wireframe": bool,
        "loading": bool
    }],

    "projects" : [{
        "idProjeto": integer,
        "nomeUnidade": string,
        "nomeAreaTematica": string,
        "disponibilidadeProjeto": bool,
        "idApresentacao": integer,
        "tituloProjeto": string,
        "wireframe": bool,
        "loading": bool
    }],

    "reviewStatus": [{
        "idPessoa": string,
        "idVinculoPessoa": string,
        "idProjeto": string,
        "nomeSocialPessoa": string,
        "finished": bool
    }]
}
```

## Actions

![BPMN Diagram](../assets/mobile-bpmn.svg "BPMN diagram")

### MR1 - Add Apresentacao

#### Arguments

> - idApresentacao: integer
> - id2Apresentacao: string,

#### Request

> GET http://sisenex.ufpb.br/apresentacao/idApresentacao

#### Response

```javascript
//Success
[{
    "idProjeto": integer,
    "nomeUnidade": string,
    "nomeAreaTematica": string,
    "disponibilidade": bool,
    "tituloProjeto": string,
}]

//Error
{
    "reason": string
}
```

#### Effect

> - Adds a apresentacao object to state.apresentacoes

### MA1B - Close Apresentacao

#### Arguments

> - IdApresentacao

#### Request

#### Response

```
{
    "status_code": integer
}
```

#### Effect

### MA2 - Add reviewer

#### Arguments

> - idPessoa: integer
> - idVinculo: integer
> - idApresentacao: integer
> - id2Apresentacao: string

#### Request

> POST http://sisenex.ufpb.br/avaliador/idPessoa=idPessoa&idVinculo=idVinculo

#### Response

```javascript
//Success
{
    "result": {
        "idPessoa": string,
        "idVinculoPessoa": string,
        "nome_social": string
    },
}
```

```javascript
//Error
{
    "status_code": integer
}
```

#### Effect

> - Adds a reviewer under a apresentacao

### MA3 - Start Review

#### Arguments

> - idProjeto: integer,
> - idApresentacao: integer,
> - id2Apresentacao: string,
> - aptidao: 1

#### Request

> POST http://sisenex.ufpb.br/projetos/idProjeto?avaliadores=avaliadores&idApresentacao=idApresentacao

#### Response

```javascript
{
    "status_code": integer
}
```

#### Effect

> - On the backend: links a reviewer to a project and enables him to submit reviews

### MA4 - Remove reviewer

#### Arguments

> - idApresentacao: integer
> - idPessoa: integer
> - idVinculo: integer
> - aptidao: 0

#### Request

> DELETE http://sisenex.ufpb.br/avaliadores/idPessoa/idVinculo?apresentacao_id=apresentacao_id

#### Response

```javascript
{
    "status_code": integer
}
```

#### Effect

> Removes a reviewer from state tree

### MA5 - Get review status

#### Arguments

> - idApresentacao

#### Request

> GET http://sisenex.ufpb.br/avaliadores/idPessoa/idVinculo/idProjeto

#### Response

```javascript
{
    "projetoId": [{
        "avaliadorId": string,
        "status": boolean
    }]
    //...
}
```

#### Effect

> Updates a review status under state.reviewStatus

### MA6 - Fetch apresentacao reviewers

#### Arguments

> - idApresentacao
> - id2Apresentacao

#### Request

> GET http://sisenex.ufpb.br/avaliadores/idPessoa/idVinculo/idProjeto

#### Response

```javascript
{
  [
    ("idPessoa": string),
    ("idVinculoPessoa": string),
    ("nomeSocialPessoa": string)
  ];
}
```

#### Effect

> Fetches reviews linked to given apresentacao
