import React, { useState, createRef } from "react"
import { StatusBar, View, Text } from "react-native"
import { SafeAreaProvider, SafeAreaView } from "react-native-safe-area-context"
import { Provider, useSelector } from "react-redux"
import AppLoading from "expo-app-loading"
import * as SplashScreen from 'expo-splash-screen';
import { Asset } from "expo-asset"
import * as Font from "expo-font"
import { store, persistor } from "redux/store"
import {
  MaterialIcons,
  MaterialCommunityIcons,
  Entypo,
  FontAwesome
} from "@expo/vector-icons"
import NavigationService, {
  navigatorRef
} from "navigation/utils/NavigationService"
import Colors from "assets/css/colors"
import * as Sentry from "sentry-expo"
import { ApolloProvider } from "react-apollo"
import client from "./src/graphql/client"
import PlainHeader from "components/Header/PlainHeader"
import {
  loadUsuarioAtualCache,
  loadAssinouTermoCache,
  clearUsuarioAtualCache,
  clearAssinouTermoCache,
  saveUsuarioAtualCache
} from "caching"
import { usuarioAtual } from "graphql/queries/usuarioAtual"
import RadioDialog from "components/Dialogs/RadioDialog"
import { NetworkStatusIndicator } from "components/NetworkStatus"
import { logout } from "redux/Auth/actions"

import { NavigationContainer, StackActions } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import ReviewerRoutes from "navigation/routes/reviewer/ReviewerRoutes"
import MonitorRoutes from "navigation/routes/monitor/MonitorRoutes"
import { LoginScreen } from "views/Login"
import { ProjetosScreen } from "views/Projetos"
import ReportProblemas from "views/ReportProblemas/ReportProblemasScreen"
import {
  RegistroApresentacaoInputScreen,
  RegistroApresentacaoScreen
} from "views/RegistroApresentacao"
import { AvaliacoesScreen } from "views/Avaliacoes"
import { IdentidadeScreen } from "views/Identidade"
import { ApresentacaoScreen } from "views/Apresentacao"
import { PessoasApresentacaoScreen } from "views/PessoasApresentacao"
import { TermoDeUsoScreen } from "views/TermoDeUso"
import { getAssinouTermo } from "redux/Auth/selectors"
import { RootNavigator } from "navigation/routes/RootNavigator"
import { PersistGate } from "redux-persist/integration/react"
import configureStore from "redux/configureStore"

const Stack = createStackNavigator()

Sentry.init({
  dsn: "https://0e2217ba3ab345a683875f006d77b32e@sentry.io/1422628",
  enableInExpoDevelopment: true
})

export default class oldApp extends React.Component {
  state = {
    splashIsReady: false,
    assinouTermo: false,
    appIsReady: false,
    loggedIn: false,
    avaliadorPessoa: 0,
    monitorPessoa: 0
  }

  splash = React.createRef<AppLoading>()

  cacheResources = async () => {
    //Load fonts
    await Font.loadAsync({
      Roboto: require("assets/fonts/Roboto-Regular.ttf"),
    })
    //Load images
    const images = [require("./src/assets/MarcaSisEnexVertica.png")]
    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync()
    })

    const arrayPromises: Array<Promise<Asset | void>> = [...cacheImages,
    Font.loadAsync(MaterialIcons.font),
    Font.loadAsync(MaterialCommunityIcons.font),
    Font.loadAsync(Entypo.font)]

    await Promise.all(arrayPromises)

    await this.loadApp()
  }

  loadApp = async () => {
    // await clearAssinouTermoCache()
    // await clearUsuarioAtualCache()
    const usuarioAtualFromCache = await loadUsuarioAtualCache()
    const assinouTermoFromCache = await loadAssinouTermoCache()

    if (usuarioAtualFromCache)
      client.writeQuery({
        query: usuarioAtual,
        data: {
          usuarioAtual: usuarioAtualFromCache
        }
      })
    this.setState(
      {
        appIsReady: true,
        loggedIn: !!usuarioAtualFromCache,
        assinouTermo: assinouTermoFromCache,
        monitorPessoa:
          usuarioAtualFromCache && usuarioAtualFromCache.monitorPessoa,
        avaliadorPessoa:
          usuarioAtualFromCache && usuarioAtualFromCache.avaliadorPessoa
      },
      () => {
        console.log("Atualizou", this.state)
      }
    )

    console.log("Apos carregamento:", this.state)
  }

  render() {
    if (!(this.state.splashIsReady && this.state.appIsReady)) {
      return (
        <AppLoading
          ref={this.splash}
          startAsync={this.cacheResources}
          onFinish={() => {
            this.setState({ splashIsReady: true })
          }}
          onError={console.warn}
        />
      )
    } else {
      return (
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <ApolloProvider client={client}>
              <React.Fragment>
                <SafeAreaProvider>
                  <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                    <StatusBar barStyle="light-content" />
                    <PlainHeader />
                    <RootNavigator />
                  </SafeAreaView>
                  <NetworkStatusIndicator />
                </SafeAreaProvider>
              </React.Fragment>
            </ApolloProvider>
          </PersistGate>
        </Provider>
      )
    }
  }
}
