import React, { useState, useCallback, useEffect } from "react"
import { StatusBar } from "react-native"
import { SafeAreaProvider, SafeAreaView } from "react-native-safe-area-context"
import { Provider } from "react-redux"
import * as Font from "expo-font"
import { Asset } from "expo-asset"
import {
  MaterialIcons,
  MaterialCommunityIcons,
  Entypo
} from "@expo/vector-icons"
import { loadUsuarioAtualCache, loadAssinouTermoCache } from "caching"
import client from "./src/graphql/client"
import { usuarioAtual } from "graphql/queries/usuarioAtual"
import { RootNavigator } from "navigation/routes/RootNavigator"
import { PersistGate } from "redux-persist/integration/react"
import { ApolloProvider } from "react-apollo"
import PlainHeader from "components/Header/PlainHeader"
import { NetworkStatusIndicator } from "components/NetworkStatus"
import { store, persistor } from "redux/store"
import * as SplashScreen from "expo-splash-screen"
import * as WebBrowser from 'expo-web-browser';

// Manter a splash visível por padrão. Iremos esconde-la manualmente após
// carregar recursos
WebBrowser.maybeCompleteAuthSession();
SplashScreen.preventAutoHideAsync()

const App: React.FC<{}> = () => {
  const [assinouTermo, setAssinouTermo] = useState<boolean>(false)
  const [appIsReady, setAppIsReady] = useState<boolean>(false)
  const [loggedIn, setLoggedIn] = useState<boolean>(false)
  const [avaliadorPessoa, setAvaliadorPessoa] = useState<number | null>(0)
  const [monitorPessoa, setMonitorPessoa] = useState<number | null>(0)

  const stateObject: object = {
    assinouTermo: assinouTermo,
    appIsReady: appIsReady,
    loggedIn: loggedIn,
    avaliadorPessoa: avaliadorPessoa,
    monitorPessoa: monitorPessoa
  }

  const cacheResources = async () => {
    //Load fonts
    await Font.loadAsync({
      Roboto: require("assets/fonts/Roboto-Regular.ttf")
    })
    //Load images
    const images = [require("./src/assets/MarcaSisEnexVertica.png")]
    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync()
    })

    const arrayPromises: Array<Promise<Asset | void>> = [
      ...cacheImages,
      Font.loadAsync(MaterialIcons.font),
      Font.loadAsync(MaterialCommunityIcons.font),
      Font.loadAsync(Entypo.font)
    ]

    await Promise.all(arrayPromises)

    await loadApp()
  }

  const loadApp = async () => {
    const usuarioAtualFromCache = await loadUsuarioAtualCache()
    const assinouTermoFromCache = await loadAssinouTermoCache()

    if (usuarioAtualFromCache)
      client.writeQuery({
        query: usuarioAtual,
        data: {
          usuarioAtual: usuarioAtualFromCache
        }
      })

    setAppIsReady(true)
    setLoggedIn(!!usuarioAtualFromCache)
    setAssinouTermo(assinouTermoFromCache)
    setMonitorPessoa(
      usuarioAtualFromCache && usuarioAtualFromCache.monitorPessoa
    )
    setAvaliadorPessoa(
      usuarioAtualFromCache && usuarioAtualFromCache.avaliadorPessoa
    )

    console.log("Atualizou v2", stateObject)
  }

  const onLayoutRootView = useCallback(async () => {
    if (appIsReady) {
      await SplashScreen.hideAsync()
    }
  }, [appIsReady])

  useEffect(() => {
    cacheResources()
  }, [])

  if (!appIsReady) {
    return null
  }

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ApolloProvider client={client}>
          <React.Fragment>
            <SafeAreaProvider>
              <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                <StatusBar barStyle="light-content" />
                <PlainHeader />
                <RootNavigator onReady={onLayoutRootView} />
              </SafeAreaView>
              <NetworkStatusIndicator />
            </SafeAreaProvider>
          </React.Fragment>
        </ApolloProvider>
      </PersistGate>
    </Provider>
  )
}

export default App
