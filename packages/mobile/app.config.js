/* eslint-disable no-console */

const variant = process.env.APP_VARIANT || "devel"

if (!["devel", "staging", "production"].includes(variant)) {
  throw new Error("APP_VARIANT não configurado")
}

// Variantes precisam ter identificadores diferentes para serem instaladas simultaneamente
function getIdentifier(variant) {
  const androidIdentifiers = {
    devel: "com.lumo.sisenex.devel",
    staging: "com.lumo.sisenex.staging",
    production: "com.lumo.sisenex"
  }

  return androidIdentifiers[variant]
}

// Como usuário pode ter multiplas variantes instaladas, mostrar nome da variante no app
function getDisplayName(variant) {
  const displayNames = {
    devel: "Sisenex - DEV",
    staging: "Sisenex - STAGING",
    production: "Sisenex"
  }

  return displayNames[variant]
}

// Em builds não produtivas, permitir conexão sem https
function handleClearText(config) {
  // if (variant === "production") return config

  const draft = JSON.parse(JSON.stringify(config))
  const plugin = [
    "expo-build-properties",
    {
      android: {
        usesCleartextTraffic: true
      }
    }
  ]

  draft.expo.plugins.push(plugin)

  return draft
}

const config = handleClearText({
  owner: "v-in",
  updates: {
    enabled: true
  },
  expo: {
    privacy: "public",
    scheme: "sisenex",
    splash: {
      image: "./src/assets/splash.png",
      backgroundColor: "#303F9F",
      resizeMode: "contain"
    },
    icon: "./src/assets/launcher-ios.png",
    orientation: "portrait",
    name: getDisplayName(variant),
    version: "4.2.0",
    slug: "sisenex",
    androidStatusBar: {
      backgroundColor: "#453DCE"
    },
    description: "Avaliação ENEX UFPB",
    android: {
      versionCode: 15,
      package: getIdentifier(variant),
      adaptiveIcon: {
        foregroundImage: "./src/assets/launcher-android.png",
        backgroundColor: "#34447B"
      },
      permissions: ["CAMERA"]
    },
    ios: {
      icon: "./src/assets/launcher-ios.png",
      bundleIdentifier: getIdentifier(variant)
    },
    hooks: {
      postPublish: [
        {
          file: "sentry-expo/upload-sourcemaps",
          config: {
            organization: "sisenex",
            project: "sisenex-mobile",
            authToken:
              "50ee4a0ca0414bea9bb280eca95f2a53f5b2182241c74a8c9870d367e89477cf"
          }
        }
      ]
    },
    plugins: [
      "expo-font",
      "sentry-expo",
      [
        "expo-updates",
        {
          username: "v-in"
        }
      ]
    ],
    extra: {
      eas: {
        projectId: "b7cc9cf0-cbfb-11e8-a9e2-017a51454256"
      }
    },
    runtimeVersion:  "4.3.0",
    updates: {
      url: "https://u.expo.dev/b7cc9cf0-cbfb-11e8-a9e2-017a51454256"
    }
  }
})

export default config
