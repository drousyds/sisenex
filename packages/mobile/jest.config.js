module.exports = {
  preset: "jest-expo",
  verbose: true,
  transformIgnorePatterns: ["node_modules/^(?!rn-placeholder)/"],
  moduleDirectories: [".", "src", "src/util", "node_modules"]
}
