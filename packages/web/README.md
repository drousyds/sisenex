# SisEnex Módulo de Gerência Web

## Mapeamento queries
Queries e Mutations do módulo de gerência do SisEnex. #30

### Apresentações
```gql
query {
  apresentacoes {
    idApresentacao
    id2Apresentacao
    idAreaTematica
    idEvento
    codigoApresentacao
    salaApresentacao
    modalidadeApresentacao
    horaApresentacao
    disponibilidadeApresentacao
    projetos {
      ...ProjetoFragment
      mediaAvaliacao
    }
  }
}
```
- É necessário/possível receber a lista de avaliações ao invés da média (?)

```gql
mutation {
  updateApresentacao(idApresentacao: number, id2Apresentacao: string, input: apresentacaoUpdates) {
    ...ApresentacaoFragment
  }
  createApresentacao(input: createApresentacaoInput){
    ...ApresentacaoFragement
  }
}
```
- Verificar se o termo `input` é o correto.

### Projetos
```gql
query {
  projetos {
    idProjeto
    tituloProjeto
    descricaoProjeto
    anoProjeto
    dataApresentacaoProjeto
    idAreaTematica
    idUnidade
    campusProjeto
    apresentacao {
      ...ApresentacaoFragement
    }
  }
}
```

```gql
mutation {
  updateProjeto(idProjeto: number, input: projetoUpdates) {
    ...ProjetoFragment
  }
}
```

### Pessoas
```gql
query {
  pessoas {
    idPessoa
    idVinculoPessoa
    matriculaPessoa
    nomePessoa
    nomeSocialPessoa
    lotacaoPessoa
    emailPessoa
    telefonePessoa
    aptidaoPessoa
    avaliadorPessoa
    monitorPessoa
    gerentePessoa
  }
}
```

```gql
mutation {
  updatePessoa(idPessoa: number, idVinculoPessoa: number, input: pessoaUpdates) {
    ...PessoaFragment
  }
}
```

### Avaliações
```gql
query {
  avaliacoes {
    ...ProjetoFragment
    nomeAreaTematica
    mediaAvaliacao
  }
}
```

### Áreas Temáticas
```gql
query {
  areasTematicas {
    idAreaTematica
    nomeAreaTematica
  }
}
```

### Unidades
```gql
query {
  unidades {
    idUnidade
    codigoUnidade
    nomeUnidade
    siglaUnidade
    hierarquiaUnidade
    tipoUnidade
    unidadeGestora
  }
}
```

### Perguntas
```gql
query {
  perguntas {
    ...PerguntaFragment
  }
}
```
- Tem uma mutation para adicionar uma avalicao a um projeto