import moment from 'moment'
import 'moment/locale/pt-br'

const renderUnixDate = (timestamp: string) => {
  const timestampN = Number(timestamp) / 1000
  if (timestampN) {
    return moment
      .unix(timestampN)
      .utc()
      .format('DD/MM/YYYY HH:mm')
  }
  return undefined
}

const renderLongUnixDate = (timestamp: string) => {
  const timestampN = Number(timestamp) / 1000
  if (timestampN) {
    return moment
      .unix(timestampN)
      .utc()
      .format('LLLL')
  }
  return undefined
}

const toMoment = (timestamp: string) => {
  const timestampN = Number(timestamp)
  if (timestampN) {
    return moment(timestampN)
  }
  return undefined
}

export { renderUnixDate, renderLongUnixDate, toMoment }
