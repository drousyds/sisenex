import { Pessoa } from '../generated'
import { UserTypes } from '../types'

const getUserRoles = (user: Pessoa) => {
  const roles: string[] = []
  if (user.avaliadorPessoa) {
    roles.push(UserTypes.REVIEWER)
  }
  if (user.monitorPessoa) {
    roles.push(UserTypes.MONITOR)
  }
  if (user.gerentePessoa) {
    roles.push(UserTypes.MANAGER)
  }
  if (user.administradorPessoa) {
    roles.push(UserTypes.ADMINISTRATOR)
  }
  return roles
}

export default getUserRoles
