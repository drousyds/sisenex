import { notification } from 'antd'

const openNotificationWithIcon = (
  type: string,
  message: string,
  duration = 4.5
) => {
  // @ts-ignore
  notification[type]({
    message,
    duration
  })
}

export default openNotificationWithIcon
