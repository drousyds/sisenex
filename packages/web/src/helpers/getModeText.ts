const getModeText = (modalidade: string) =>
  modalidade === 'T' ? 'Tertúlia' : 'Performance'

export default getModeText
