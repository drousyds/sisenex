import { Apresentacao } from '../generated'

const getPresentationId = ({
  idApresentacao,
  id2Apresentacao
}: Apresentacao): string => `${idApresentacao}-${id2Apresentacao}`

export default getPresentationId
