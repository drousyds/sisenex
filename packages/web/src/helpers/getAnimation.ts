interface Input {
  from: {
    opacity: number
    x: number
    y: number
    z: number
  }
  to: {
    opacity: number
    x: number
    y: number
    z: number
  }
}

const getAnimation = ({ from, to }: Input) => {
  return {
    from: {
      opacity: from.opacity,
      transform: `translate3d(${from.x}px,${from.y}px,${from.z}px)`
    },
    to: {
      opacity: to.opacity,
      transform: `translate3d(${to.x}px,${to.y}px,${to.z}px)`
    }
  }
}

export default getAnimation
