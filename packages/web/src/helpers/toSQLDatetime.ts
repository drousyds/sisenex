const toSQLDatetime = (date: string, time: string): string => `${date} ${time}`

export default toSQLDatetime
