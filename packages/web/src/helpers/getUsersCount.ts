import { Pessoa } from '../generated'

const getUsersCount = (users: Pessoa[]) => {
  const counter = {
    avaliadores: 0,
    monitores: 0,
    gerentes: 0,
    administrador: 0
  }

  users.forEach(
    ({
      avaliadorPessoa,
      monitorPessoa,
      gerentePessoa,
      administradorPessoa
    }) => {
      if (avaliadorPessoa) {
        counter.avaliadores++
      }
      if (monitorPessoa) {
        counter.monitores++
      }
      if (gerentePessoa) {
        counter.gerentes++
      }
      if (administradorPessoa) {
        counter.administrador++
      }
    }
  )

  return counter
}
export default getUsersCount
