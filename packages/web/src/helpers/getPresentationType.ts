import { Apresentacao } from '../generated'

const getPresentationType = (presentation: Apresentacao) => {
  let type = 'Sem Modalidade'
  if (presentation) {
    switch (presentation.modalidadeApresentacao) {
      case 'T':
        type = 'Tertúlia'
        break
      case 'P':
        type = 'Peformance'
        break
    }
  }

  return type
}

export default getPresentationType
