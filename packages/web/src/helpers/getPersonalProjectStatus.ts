import { Projeto } from '../generated'
import { PersonalProjectStatus, ProjectStatus } from '../types'

const getPersonalProjectStatus = mediaIndividual => {
  /**
   * Os Projetos terão três estados.
   * Não apresentado: para o caso de não comparecimento de nenhum integrante.
   * Não avaliado: para o caso de estar pendente a avaliação e
   * Apresentado: no caso de já ter sido avaliado.
   */
  if (mediaIndividual) {
    if (mediaIndividual < 0) {
      return PersonalProjectStatus.NOT_ATTENDED
    }
    return PersonalProjectStatus.EVALUATED
  }
  return PersonalProjectStatus.NOT_EVALUATED
}

export default getPersonalProjectStatus
