import { Apresentacao } from '../generated'

const getPresentationsInfo = (presentations: Apresentacao[]) => {
  const info = {
    status: {
      open: 0,
      closed: 0
    },
    type: {
      tertulias: 0,
      peformances: 0
    },
    total: 0
  }

  presentations.forEach(
    ({ disponibilidadeApresentacao, modalidadeApresentacao }) => {
      if (disponibilidadeApresentacao === 1) {
        info.status.open++
      } else {
        info.status.closed++
      }

      if (modalidadeApresentacao === 'T') {
        info.type.tertulias++
      } else {
        info.type.peformances++
      }
      info.total++
    }
  )

  return info
}
export default getPresentationsInfo
