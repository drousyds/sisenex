export interface AvatarColor {
  color: string
  backgroundColor: string
}

const colors: AvatarColor[] = [
  { backgroundColor: '#fde3cf', color: '#f56a00' },
  { backgroundColor: '#ffecb3', color: '#ffbf00' },
  { backgroundColor: '#b3faff', color: '#00a2ae' },
  { backgroundColor: '#c3bdf4', color: '#7265e6' }
]

export const getAvatarColor = (index: number) => {
  return colors[index % colors.length]
}

export const getAvatarRandomColor = () =>
  colors[Math.floor(Math.random() * colors.length)]
