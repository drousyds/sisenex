import { Campus } from '../generated'

const getCampusNumber = (idCampus: Campus['idCampus']) => {
  if (idCampus === 1) return 'I'
  if (idCampus === 2) return 'II'
  if (idCampus === 3) return 'III'
  if (idCampus === 4) return 'IV'
  return ''
}

export default getCampusNumber
