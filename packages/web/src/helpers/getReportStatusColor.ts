import { Report } from '../generated'

export default (statusReport: Report['statusReport']) => {
  switch (statusReport) {
    case 'ABERTA':
      return 'red'
    case 'CONCLUIDA':
      return 'green'
    case 'EM ANDAMENTO':
      return 'geekblue'
    default:
      return 'geekblue'
  }
}
