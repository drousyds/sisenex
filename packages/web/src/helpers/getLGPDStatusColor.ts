import { Solicitacao2 } from '../generated'

export default (status: Solicitacao2['statusSolicitacao']) =>{
    switch(status){
        case 'ABERTA':
            return 'red'
        case 'RESOLVIDA':
            return 'green'
        case 'RESPONDIDA':
            return 'geekblue'
        case 'ARQUIVADA':
            return 'purple'
        default:
            return 'geekblue'
    }
}