import { Pessoa } from '../generated'

const getUserId = ({ idPessoa, idVinculoPessoa }: Pessoa) =>
  `${idPessoa} - ${idVinculoPessoa}`

export default getUserId
