import { getPresentationStatus, getProjectStatus } from '.'

// Types
import { Apresentacao, Projeto } from '../generated'
import { PresentationStatus, ProjectStatus } from '../types'

const getStatusTagColor = (item: Apresentacao | Projeto) => {
  if (item.__typename === 'Apresentacao') {
    const statusText = getPresentationStatus(item)
    switch (statusText) {
      case PresentationStatus.completed:
        return 'blue'
      case PresentationStatus.pending:
        return 'volcano'
      case PresentationStatus.onGoing:
        return 'green'
      default:
        return 'volcano'
    }
  }
  if (item.__typename === 'Projeto') {
    const statusText = getProjectStatus(item as Projeto)
    switch(statusText){
      case 'AVALIADO':
        return 'blue'
      case 'NAO AVALIADO':
        return 'volcano'
      case 'AUSENTE':
        return 'red'
    }
  }
  return undefined
}

export default getStatusTagColor
