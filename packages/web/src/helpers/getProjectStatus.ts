import { useEffect, useState } from 'react';
import { Projeto, useGetProjectDetailsQuery } from '../generated'

const getProjectStatus = (project: Projeto) => {
  return project.statusProjeto;
}

export default getProjectStatus;
