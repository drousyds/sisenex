// @ts-nocheck
import { Pessoa } from '../generated'

export default (data): Pessoa[] => {
  const vinculosDiscente = data.discente
  const vinculosServidor = data.servidor
  const vinculos = []

  const getDataFromVinculoDiscente = vinculo => ({
    idPessoa: `${vinculo.pessoa.idPessoa}`,
    idVinculoPessoa: `${vinculo.idVinculo}`,
    nomePessoa: vinculo.pessoa.nome,
    nomeSocialPessoa: vinculo.pessoa.nomeSocialOficial,
    telefonePessoa: vinculo.telefone,
    emailPessoa: vinculo.email,
    lotacaoPessoa: null,
    matriculaPessoa: `${vinculo.matricula}`,
    aptidaoPessoa: 0,
    monitorPessoa: 1,
    avaliadorPessoa: 0,
    gerentePessoa: 0
  })

  const getDataFromVinculoServidor = vinculo => ({
    idPessoa: `${vinculo.pessoa.idPessoa}`,
    idVinculoPessoa: `${vinculo.idVinculo}`,
    nomePessoa: vinculo.pessoa.nome,
    nomeSocialPessoa: vinculo.pessoa.nomeSocialOficial,
    telefonePessoa: vinculo.telefone,
    emailPessoa: vinculo.pessoa.email,
    lotacaoPessoa: null,
    matriculaPessoa: `${vinculo.siape}`,
    aptidaoPessoa: 0,
    monitorPessoa: 0,
    avaliadorPessoa: 1,
    gerentePessoa: 0
  })

  // @ts-ignore
  vinculosDiscente.map(elem => vinculos.push(getDataFromVinculoDiscente(elem)))
  // @ts-ignore
  vinculosServidor.map(elem => vinculos.push(getDataFromVinculoServidor(elem)))

  return vinculos
}
