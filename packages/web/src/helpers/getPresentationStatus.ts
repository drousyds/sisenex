import { Apresentacao } from '../generated'
import { PresentationStatus } from '../types'

const getPresentationStatus = ({
  disponibilidadeApresentacao
}: Apresentacao): string => {
  /**
   * As Apresentações terão três estados.
   * 0 - Pendente: no caso de não estarem abertas.
   * 1 - Em execução: para o caso de estarem em andamento e
   * 2 - Concluída: após a finalização de todos os projetos pertencentes à elas.
   */
  if (disponibilidadeApresentacao) {
    return disponibilidadeApresentacao === 2
      ? PresentationStatus.completed
      : PresentationStatus.onGoing
  }
  return PresentationStatus.pending
}

export const getPresentationStatusNumber = (value: string | undefined) => {
  if (value) {
    switch (value) {
      case PresentationStatus.completed:
        return 2
      case PresentationStatus.onGoing:
        return 1
      case PresentationStatus.pending:
        return 0
      default:
        return 0
    }
  }
  return undefined
}
export default getPresentationStatus
