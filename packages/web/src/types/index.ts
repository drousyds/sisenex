import { AppStateTypes } from '../redux/store/getReducers'

export type AppStateType = AppStateTypes

/**
 * ACTIONS
 */
export interface ErrorPayloadType {
  message: string
}

export enum ProjectStatus {
  EVALUATED = 'APRESENTADO',
  NOT_EVALUATED = 'NAO AVALIADO',
  NOT_ATTENDED = 'AUSENTE'
}

export enum PersonalProjectStatus {
  EVALUATED = 'AVALIADO',
  NOT_EVALUATED = 'NÃO AVALIADO',
  NOT_ATTENDED = 'NÃO COMPARECEU'
}

export enum UserTypes {
  MONITOR = 'MONITOR',
  REVIEWER = 'REVIEWER',
  MANAGER = 'MANAGER',
  ADMINISTRATOR = 'ADMINISTRATOR',
  NO_ROLE = 'NO_ROLE'
}

export enum PresentationStatus {
  onGoing = 'EM EXECUÇÃO',
  pending = 'PENDENTE',
  completed = 'FINALIZADA'
}

export enum NotificationType {
  ERROR = 'error',
  SUCCESS = 'success'
}

export interface ModeType {
  id: 1 | 2
  name: string
}
