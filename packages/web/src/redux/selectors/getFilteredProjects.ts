import { createSelector } from 'reselect'
import { Projeto } from '../../generated'

/**
 * Projects Data Flow
 * getFilteredProjects <== getProjectsByField
 */

// Selector
interface SelectorState {
  projects: Projeto[]
  filterText: string
}

const getProjects = (state: SelectorState) => state.projects
const getFilterText = (state: SelectorState) => state.filterText

const finalSelector = (projects: Projeto[], filterText: string) =>
  filterText
    ? projects.filter(project => {
        const projectString = `${project.idProjeto}-${project.tituloProjeto}`
        return projectString.toLowerCase().includes(filterText.toLowerCase())
      })
    : projects

export default createSelector(
  [getProjects, getFilterText],
  finalSelector
)
