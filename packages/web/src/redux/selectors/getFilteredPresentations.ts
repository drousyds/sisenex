import { createSelector } from 'reselect'
import { Apresentacao } from '../../generated'

/**
 * Presentations Data Flow
 * getFilteredPresentations <== state
 */

// Selector
interface SelectorState {
  presentations: Apresentacao[]
  filterText: string
}

const getPresentations = (state: SelectorState) => state.presentations
const getFilterText = (state: SelectorState) => state.filterText

const finalSelector = (presentations: Apresentacao[], filterText: string) =>
  filterText
    ? presentations.filter(
        ({ idApresentacao, id2Apresentacao, salaApresentacao }) => {
          let presentationString = `${idApresentacao}-${id2Apresentacao}`
          if (salaApresentacao) {
            presentationString += salaApresentacao
          }

          return presentationString
            .toLowerCase()
            .includes(filterText.toLowerCase())
        }
      )
    : presentations

export default createSelector(
  [getPresentations, getFilterText],
  finalSelector
)
