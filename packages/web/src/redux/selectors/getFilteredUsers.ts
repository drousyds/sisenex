import { createSelector } from 'reselect'
import { Pessoa } from '../../generated'

/**
 * Users Data Flow
 * getFilteredUsers <== state
 */

// Selector
interface SelectorState {
  users: Pessoa[]
  filterText: string
}

const getUsers = (state: SelectorState) => state.users
const getFilterText = (state: SelectorState) => state.filterText

const finalSelector = (users: Pessoa[], filterText: string) =>
  filterText
    ? users.filter(user => {
        const userString = user.nomePessoa!
        return userString
          .toLowerCase()
          .trim()
          .includes(filterText.toLowerCase().trim())
      })
    : users

export default createSelector([getUsers, getFilterText], finalSelector)
