import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
// reducers
import getReducers from './getReducers'

// middlewares
import logger from '../middlewares/logger'

// rootSaga
import rootSaga from './rootSaga'

const sagaMiddleware = createSagaMiddleware()

const configureStore = () => {
  let middlewares = [sagaMiddleware]

  if (process.env.NODE_ENV === 'development') {
    // @ts-ignore
    middlewares = [logger, ...middlewares]
  }

  const store = createStore(
    getReducers(),
    composeWithDevTools(applyMiddleware(...middlewares))
  )
  sagaMiddleware.run(rootSaga)
  return store
}

export default configureStore
