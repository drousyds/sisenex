import { all } from 'redux-saga/effects'

// Sagas
export default function* rootSaga() {
  try {
    yield all([])
    /* eslint-disable-next-line */
    console.log('Ok')
  } catch (e) {
    /* eslint-disable-next-line */
    console.error('Root saga caugth unhandled exception: ', e)
  }
}
