import { combineReducers } from 'redux'

import authReducer from '../ducks/auth/authReducer'
import categoryDialogReducer from '../ducks/dialogs/category/categoryDialogReducer'
import eventsDialogsReducer from '../ducks/dialogs/events/eventsDialogsReducer'
import presentationsDialogReducer from '../ducks/dialogs/presentations/presentationsDialogReducer'
import projectsDialogReducer from '../ducks/dialogs/projects/projectsDialogReducer'
import questionsDialogReducer from '../ducks/dialogs/questions/questionsDialogReducer'
import reportDialogReducer from '../ducks/dialogs/report/reportDialogReducer'
import usersDialogReducer from '../ducks/dialogs/users/usersDialogReducer'
import filtersReducer from '../ducks/filters/filtersReducer'

const rootReducer = combineReducers({
  dialogs: combineReducers({
    presentationsDialog: presentationsDialogReducer,
    projectsDialog: projectsDialogReducer,
    usersDialog: usersDialogReducer,
    events: eventsDialogsReducer,
    report: reportDialogReducer,
    questions: questionsDialogReducer,
    category: categoryDialogReducer
  }),
  filters: filtersReducer,
  auth: authReducer
})

const getReducers = () => rootReducer

export type AppStateTypes = ReturnType<typeof rootReducer>

export default getReducers
