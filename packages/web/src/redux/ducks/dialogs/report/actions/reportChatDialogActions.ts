import { action } from 'typesafe-actions'

// types
import { Report } from '../../../../../generated'
import { DialogsActionTypes } from '../../dialogsTypes'

export const openReportChatDialogAction = (report: Report) =>
  action(DialogsActionTypes.OPEN_REPORT_CHAT_DIALOG, report)

export const closeReportChatDialogAction = () =>
  action(DialogsActionTypes.CLOSSE_REPORT_CHAT_DIALOG)
