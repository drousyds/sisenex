import { Reducer } from 'redux'

// Types
import { Report } from '../../../../generated'
import { DialogsActionTypes } from '../dialogsTypes'

/**
 * usersDialogReducer Type definition
 */
export interface ReportsDialogReducerType {
  chat: { open: boolean; report?: Report }
}

/**
 * Reducer's initial state
 */
const initialState: ReportsDialogReducerType = {
  chat: {
    open: false,
    report: undefined
  }
}

const reportDialogReducer: Reducer<ReportsDialogReducerType> = (
  state = initialState,
  action
) => {
  const { type, payload } = action
  let newState: ReportsDialogReducerType
  /**
   * Update User Dialog
   */
  switch (type) {
    case DialogsActionTypes.OPEN_REPORT_CHAT_DIALOG:
      newState = {
        ...state,
        chat: {
          open: true,
          report: payload
        }
      }
      return newState
    case DialogsActionTypes.CLOSSE_REPORT_CHAT_DIALOG:
      newState = {
        ...state,
        chat: {
          open: false,
          report: undefined
        }
      }
      return newState
    default:
      return state
  }
}

export default reportDialogReducer
