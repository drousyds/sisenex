export enum DialogsActionTypes {
  // USERS

  // update User
  OPEN_UPDATE_USER_DIALOG = '@dialogs/user/update/OPEN',
  CLOSE_UPDATE_USER_DIALOG = '@dialogs/user/update/CLOSE',

  // PROJECT

  // update project
  OPEN_UPDATE_PROJECT_DIALOG = '@dialogs/project/update/OPEN',
  CLOSE_UPDATE_PROJECT_DIALOG = '@dialogs/project/update/CLOSE',

  // project detais
  OPEN_PROJECT_DETAILS_DIALOG = '@dialogs/project/details/OPEN',
  CLOSE_PROJECT_DETAILS_DIALOG = '@dialogs/project/details/CLOSE',

  // PRESENTATION

  // create presentation
  OPEN_CREATE_PRESENTATION_DIALOG = '@dialogs/presentation/create/OPEN',
  CLOSE_CREATE_PRESENTATION_DIALOG = '@dialogs/presentation/create/CLOSE',

  // update presentation
  OPEN_UPDATE_PRESENTATION_DIALOG = '@dialogs/presentation/update/OPEN',
  CLOSE_UPDATE_PRESENTATION_DIALOG = '@dialogs/presentation/update/CLOSE',

  // presentation details
  OPEN_PRESENTATION_DETAILS_DIALOG = '@dialogs/presentation/details/OPEN',
  CLOSE_PRESENTATION_DETAILS_DIALOG = '@dialogs/presentation/details/CLOSE',

  // EVENT

  // create event
  OPEN_CREATE_EVENT_DIALOG = '@dialogs/events/create/OPEN',
  CLOSE_CREATE_EVENT_DIALOG = '@dialogs/events/create/CLOSE',

  // update event
  OPEN_UPDATE_EVENT_DIALOG = '@dialogs/events/update/OPEN',
  CLOSE_UPDATE_EVENT_DIALOG = '@dialogs/events/update/CLOSE',

  // allocation
  OPEN_ALLOC_DIALOG = '@dialog/events/alloc/OPEN',
  CLOSE_ALLOC_DIALOG = '@dialogs/events/alloc/CLOSE',

  // report chat
  OPEN_REPORT_CHAT_DIALOG = '@dialogs/report/chat/OPEN',
  CLOSSE_REPORT_CHAT_DIALOG = '@dialogs/report/chat/CLOSE',

  // questions
  OPEN_CREATE_QUESTION_DIALOG = '@dialogs/question/create/OPEN',
  CLOSE_CREATE_QUESTION_DIALOG = '@dialogs/question/create/CLOSE',
  OPEN_UPDATE_QUESTION_DIALOG = '@dialogs/question/update/OPEN',
  CLOSE_UPDATE_QUESTION_DIALOG = '@dialogs/question/update/CLOSE',

  // category
  OPEN_CREATE_CATEGORY_DIALOG = '@dialogs/category/create/OPEN',
  CLOSE_CREATE_CATEGORY_DIALOG = '@dialogs/category/create/CLOSE'
}
