import { action } from 'typesafe-actions'

// types
import { DialogsActionTypes } from '../../dialogsTypes'
import { Evento } from '../../../../../generated'

/**
 * Open Update Event Dialog
 */
export const openUpdateEventDialogAction = (event: Evento) =>
  action(DialogsActionTypes.OPEN_UPDATE_EVENT_DIALOG, event)

/**
 * Close Update Event Dialog
 */
export const closeUpdateEventDialogAction = () =>
  action(DialogsActionTypes.CLOSE_UPDATE_EVENT_DIALOG)
