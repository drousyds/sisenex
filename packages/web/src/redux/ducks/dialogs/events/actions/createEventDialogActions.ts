import { action } from 'typesafe-actions'

// types
import { DialogsActionTypes } from '../../dialogsTypes'

/**
 * Open Create Event Dialog
 */
export const openCreateEventDialogAction = () =>
  action(DialogsActionTypes.OPEN_CREATE_EVENT_DIALOG)

/**
 * Close Add Event Dialog
 */
export const closeCreateEventDialogAction = () =>
  action(DialogsActionTypes.CLOSE_CREATE_EVENT_DIALOG)
