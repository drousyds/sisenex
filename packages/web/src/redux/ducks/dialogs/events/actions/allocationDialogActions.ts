import { action } from 'typesafe-actions'

// types
import { DialogsActionTypes } from '../../dialogsTypes'
import { Evento } from '../../../../../generated'

/**
 * Open Allocation Dialog
 */

export interface OpenType {
  event: Evento
  idAreaTematica: number
}

export const openAllocationDialogAction = ({
  event,
  idAreaTematica
}: OpenType) =>
  action(DialogsActionTypes.OPEN_ALLOC_DIALOG, { event, idAreaTematica })

/**
 * Close Allocation Dialog
 */
export const closeAllocationDialogAction = () =>
  action(DialogsActionTypes.CLOSE_ALLOC_DIALOG)
