import { Reducer } from 'redux'
import { DialogsActionTypes } from '../dialogsTypes'
import { Evento } from '../../../../generated'

export interface EventsDialogsReducerType {
  create: {
    open: boolean
  }
  update: {
    open: boolean
    event?: Evento
  }
  allocation: {
    open: boolean
    event?: Evento
    idAreaTematica?: number
  }
}

const initialState: EventsDialogsReducerType = {
  create: {
    open: false
  },
  update: {
    open: false,
    event: undefined
  },
  allocation: {
    open: false,
    event: undefined,
    idAreaTematica: undefined
  }
}

const eventsDialogsReducer: Reducer<EventsDialogsReducerType> = (
  state = initialState,
  action
) => {
  const { type, payload } = action
  let newState: EventsDialogsReducerType

  switch (type) {
    // Create
    case DialogsActionTypes.OPEN_CREATE_EVENT_DIALOG:
      newState = {
        ...state,
        create: {
          open: true
        }
      }
      return newState

    case DialogsActionTypes.CLOSE_CREATE_EVENT_DIALOG:
      newState = {
        ...state,
        create: {
          open: false
        }
      }
      return newState

    // Update
    case DialogsActionTypes.OPEN_UPDATE_EVENT_DIALOG:
      newState = {
        ...state,
        update: {
          open: true,
          event: payload
        }
      }
      return newState

    case DialogsActionTypes.CLOSE_UPDATE_EVENT_DIALOG:
      newState = {
        ...state,
        update: {
          open: false,
          event: undefined
        }
      }
      return newState

    // allocation
    case DialogsActionTypes.OPEN_ALLOC_DIALOG:
      newState = {
        ...state,
        allocation: {
          ...state.allocation,
          open: true,
          event: payload.event,
          idAreaTematica: payload.idAreaTematica
        }
      }
      return newState

    case DialogsActionTypes.CLOSE_ALLOC_DIALOG:
      newState = {
        ...state,
        allocation: {
          ...state.allocation,
          open: false,
          event: undefined,
          idAreaTematica: undefined
        }
      }
      return newState

    default:
      return state
  }
}

export default eventsDialogsReducer
