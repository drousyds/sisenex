import { Reducer } from 'redux'

import { DialogsActionTypes } from '../dialogsTypes'

// Reducer Interface
export interface CategoryDialogReducerType {
  create: {
    open: boolean
  }
}

// Reducer's Initial State
const initialState: CategoryDialogReducerType = {
  create: {
    open: false
  }
}

const categoryDialogReducer: Reducer<CategoryDialogReducerType> = (
  state = initialState,
  action
) => {
  const { type } = action
  let newState: CategoryDialogReducerType

  switch (type) {
    // CREATE DIALOG
    case DialogsActionTypes.OPEN_CREATE_CATEGORY_DIALOG:
      newState = {
        ...state,
        create: {
          open: true
        }
      }

      return newState
    case DialogsActionTypes.CLOSE_CREATE_CATEGORY_DIALOG:
      newState = {
        ...state,
        create: {
          open: false
        }
      }
      return newState

    // default
    default:
      return state
  }
}

export default categoryDialogReducer
