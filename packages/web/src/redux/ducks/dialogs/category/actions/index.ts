import { action } from 'typesafe-actions'

import { DialogsActionTypes } from '../../dialogsTypes'

export const openCreateCategoryDialogAction = () =>
  action(DialogsActionTypes.OPEN_CREATE_CATEGORY_DIALOG)
export const closeCreateCategoryDialogAction = () =>
  action(DialogsActionTypes.CLOSE_CREATE_CATEGORY_DIALOG)
