import { action } from 'typesafe-actions'

import { Pergunta } from '../../../../../generated'
import { DialogsActionTypes } from '../../dialogsTypes'

export const openCreateQuestionDialogAction = () =>
  action(DialogsActionTypes.OPEN_CREATE_QUESTION_DIALOG)
export const closeCreateQuestionDialogAction = () =>
  action(DialogsActionTypes.CLOSE_CREATE_QUESTION_DIALOG)

export const openUpdateQuestionDialogAction = (pergunta: Pergunta) =>
  action(DialogsActionTypes.OPEN_UPDATE_QUESTION_DIALOG, pergunta)
export const closeUpdateQuestionDialogAction = () =>
  action(DialogsActionTypes.CLOSE_UPDATE_QUESTION_DIALOG)
