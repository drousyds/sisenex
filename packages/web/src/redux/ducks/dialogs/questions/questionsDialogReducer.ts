import { Reducer } from 'redux'

import { Pergunta } from '../../../../generated'
import { DialogsActionTypes } from '../dialogsTypes'

// Reducer Interface
export interface QuestionsDialogReducerType {
  create: {
    open: boolean
  }
  update: {
    open: boolean
    question?: Pergunta
  }
}

// Reducer's Initial State
const initialState: QuestionsDialogReducerType = {
  create: {
    open: false
  },
  update: {
    open: false,
    question: undefined
  }
}

const questionsDialogReducer: Reducer<QuestionsDialogReducerType> = (
  state = initialState,
  action
) => {
  const { type, payload } = action
  let newState: QuestionsDialogReducerType

  switch (type) {
    // CREATE DIALOG
    case DialogsActionTypes.OPEN_CREATE_QUESTION_DIALOG:
      newState = {
        ...state,
        create: {
          open: true
        }
      }

      return newState
    case DialogsActionTypes.CLOSE_CREATE_QUESTION_DIALOG:
      newState = {
        ...state,
        create: {
          open: false
        }
      }
      return newState

    // UPDATE
    case DialogsActionTypes.OPEN_UPDATE_QUESTION_DIALOG:
      newState = {
        ...state,
        update: {
          open: true,
          question: payload
        }
      }

      return newState
    case DialogsActionTypes.CLOSE_UPDATE_QUESTION_DIALOG:
      newState = {
        ...state,
        update: {
          open: false,
          question: undefined
        }
      }
      return newState

    // default
    default:
      return state
  }
}

export default questionsDialogReducer
