import { action } from 'typesafe-actions'

// types
import { DialogsActionTypes } from '../../dialogsTypes'
import { Apresentacao } from '../../../../../generated'

/**
 * Open Update Presensentation Dialog
 */
export const openUpdatePresentationDialogAction = (
  presentation: Apresentacao
) => action(DialogsActionTypes.OPEN_UPDATE_PRESENTATION_DIALOG, presentation)

/**
 * Close Update Presensentation Dialog
 */
export const closeUpdatePresentationDialogAction = () =>
  action(DialogsActionTypes.CLOSE_UPDATE_PRESENTATION_DIALOG)
