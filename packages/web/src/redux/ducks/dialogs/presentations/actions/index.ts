import {
  openCreatePresentationDialogAction,
  closeCreatePresentationDialogAction
} from './createPresentationDialogActions'

import {
  openUpdatePresentationDialogAction,
  closeUpdatePresentationDialogAction
} from './updatePresentationDialogActions'

import {
  openPresentationDetailsDialogAction,
  closePresentationDetailsDialogAction
} from './PresentationDetailsDialogActions'

export {
  openCreatePresentationDialogAction,
  closeCreatePresentationDialogAction,
  openUpdatePresentationDialogAction,
  closeUpdatePresentationDialogAction,
  openPresentationDetailsDialogAction,
  closePresentationDetailsDialogAction
}
