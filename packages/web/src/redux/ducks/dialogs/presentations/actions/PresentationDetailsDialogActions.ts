import { action } from 'typesafe-actions'

// types
import { DialogsActionTypes } from '../../dialogsTypes'
import { Apresentacao } from '../../../../../generated'

/**
 * Open Presensentation Details Dialog
 */
export const openPresentationDetailsDialogAction = (
  presentation: Apresentacao
) => action(DialogsActionTypes.OPEN_PRESENTATION_DETAILS_DIALOG, presentation)

/**
 * Close Presensentation Details Dialog
 */
export const closePresentationDetailsDialogAction = () =>
  action(DialogsActionTypes.CLOSE_PRESENTATION_DETAILS_DIALOG)
