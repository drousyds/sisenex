import { action } from 'typesafe-actions'

// types
import { DialogsActionTypes } from '../../dialogsTypes'

/**
 * Open Create Presentation Dialog
 */
export const openCreatePresentationDialogAction = () =>
  action(DialogsActionTypes.OPEN_CREATE_PRESENTATION_DIALOG)

/**
 * Close Add Presentation Dialog
 */
export const closeCreatePresentationDialogAction = () =>
  action(DialogsActionTypes.CLOSE_CREATE_PRESENTATION_DIALOG)
