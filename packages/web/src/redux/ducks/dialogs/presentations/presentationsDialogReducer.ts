import { Reducer } from 'redux'
import { DialogsActionTypes } from '../dialogsTypes'
import { Apresentacao } from '../../../../generated'

export interface PresentationDialogsReducerType {
  create: {
    open: boolean
  }
  update: {
    open: boolean
    presentation?: Apresentacao
  }
  details: {
    open: boolean
    presentation?: Apresentacao
  }
}

const initialState: PresentationDialogsReducerType = {
  create: {
    open: false
  },
  update: {
    open: false,
    presentation: undefined
  },
  details: {
    open: false,
    presentation: undefined
  }
}

const presentationsDialogReducer: Reducer<PresentationDialogsReducerType> = (
  state = initialState,
  action
) => {
  const { type, payload } = action
  let newState: PresentationDialogsReducerType

  switch (type) {
    // Create Presentation Dialog
    case DialogsActionTypes.OPEN_CREATE_PRESENTATION_DIALOG:
      newState = {
        ...state,
        create: {
          open: true
        }
      }
      return newState

    case DialogsActionTypes.CLOSE_CREATE_PRESENTATION_DIALOG:
      newState = {
        ...state,
        create: {
          open: false
        }
      }
      return newState

    // Update Presentations Dialog
    case DialogsActionTypes.OPEN_UPDATE_PRESENTATION_DIALOG:
      newState = {
        ...state,
        update: {
          ...state.update,
          open: true,
          presentation: payload
        }
      }
      return newState

    case DialogsActionTypes.CLOSE_UPDATE_PRESENTATION_DIALOG:
      newState = {
        ...state,
        update: {
          open: false,
          presentation: undefined
        }
      }
      return newState

    // Presentation Details Dialog
    case DialogsActionTypes.OPEN_PRESENTATION_DETAILS_DIALOG:
      newState = {
        ...state,
        details: {
          open: true,
          presentation: payload
        }
      }
      return newState

    case DialogsActionTypes.CLOSE_PRESENTATION_DETAILS_DIALOG:
      newState = {
        ...state,
        details: {
          open: false,
          presentation: undefined
        }
      }
      return newState

    default:
      return state
  }
}

export default presentationsDialogReducer
