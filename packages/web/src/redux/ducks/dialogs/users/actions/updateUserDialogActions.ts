import { action } from 'typesafe-actions'

// types
import { DialogsActionTypes } from '../../dialogsTypes'
import { Pessoa } from '../../../../../generated'

/**
 * Open Update User Dialog Action
 * @param user
 */
export const openUpdateUserDialogAction = (user: Pessoa) =>
  action(DialogsActionTypes.OPEN_UPDATE_USER_DIALOG, user)

/**
 * Close Update User Dialog Action
 */
export const closeUpdateUserDialogAction = () =>
  action(DialogsActionTypes.CLOSE_UPDATE_USER_DIALOG)
