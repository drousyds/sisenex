import { Reducer } from 'redux'

// Types
import { DialogsActionTypes } from '../dialogsTypes'
import { Pessoa } from '../../../../generated'

/**
 * usersDialogReducer Type definition
 */
export interface UsersDialogReducerType {
  update: { open: boolean; user?: Pessoa }
}

/**
 * Reducer's initial state
 */
const initialState: UsersDialogReducerType = {
  update: {
    open: false,
    user: undefined
  }
}

/**
 * UsersDialog Reducer
 * @param state
 * @param action
 */
const usersDialogReducer: Reducer<UsersDialogReducerType> = (
  state = initialState,
  action
) => {
  const { type, payload } = action
  let newState: UsersDialogReducerType
  /**
   * Update User Dialog
   */
  switch (type) {
    case DialogsActionTypes.OPEN_UPDATE_USER_DIALOG:
      newState = {
        ...state,
        update: {
          open: true,
          user: payload
        }
      }
      return newState
    case DialogsActionTypes.CLOSE_UPDATE_USER_DIALOG:
      newState = {
        ...state,
        update: {
          open: false,
          user: undefined
        }
      }
      return newState
    default:
      return state
  }
}

export default usersDialogReducer
