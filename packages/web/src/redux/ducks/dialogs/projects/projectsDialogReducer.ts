import { Reducer } from 'redux'
import { DialogsActionTypes } from '../dialogsTypes'

// Types
import { Projeto } from '../../../../generated'

// Reducer Interface
export interface ProjectsDialogReducerType {
  update: {
    open: boolean
    project?: Projeto
  }
  details: {
    open: boolean
    project?: Projeto
  }
}

// Reducer's Initial State
const initialState: ProjectsDialogReducerType = {
  update: {
    open: false,
    project: undefined
  },
  details: {
    open: false,
    project: undefined
  }
}

const projectsDialogReducer: Reducer<ProjectsDialogReducerType> = (
  state = initialState,
  action
) => {
  const { type, payload } = action
  let newState: ProjectsDialogReducerType

  switch (type) {
    // UPDATE PROJECT DIALOG
    case DialogsActionTypes.OPEN_UPDATE_PROJECT_DIALOG:
      newState = {
        ...state,
        update: {
          open: true,
          project: payload
        }
      }

      return newState
    case DialogsActionTypes.CLOSE_UPDATE_PROJECT_DIALOG:
      newState = {
        ...state,
        update: {
          open: false,
          project: undefined
        }
      }
      return newState

    // PROJECT DETAILS DIALOG
    case DialogsActionTypes.OPEN_PROJECT_DETAILS_DIALOG:
      newState = {
        ...state,
        details: {
          open: true,
          project: payload.project
        }
      }
      return newState

    case DialogsActionTypes.CLOSE_PROJECT_DETAILS_DIALOG:
      newState = {
        ...state,
        details: {
          open: false,
          project: undefined
        }
      }

      return newState
    // Default
    default:
      return state
  }
}

export default projectsDialogReducer
