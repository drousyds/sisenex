import { action } from 'typesafe-actions'

// types
import { DialogsActionTypes } from '../../dialogsTypes'
import { Projeto } from '../../../../../generated'

/**
 * Open Project Details Dialog Action
 */
export const openProjectDetailsDialogAction = (project: Projeto) =>
  action(DialogsActionTypes.OPEN_PROJECT_DETAILS_DIALOG, {
    project
  })

/**
 * Close Project Details Dialog Action
 */
export const closeProjectDetailsDialogAction = () =>
  action(DialogsActionTypes.CLOSE_PROJECT_DETAILS_DIALOG)
