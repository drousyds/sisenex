import { action } from 'typesafe-actions'

// Types
import { DialogsActionTypes } from '../../dialogsTypes'
import { Projeto } from '../../../../../generated'

/**
 * Open Update Project Dialog Action
 */
export const openUpdateProjectDialogAction = (project: Projeto) =>
  action(DialogsActionTypes.OPEN_UPDATE_PROJECT_DIALOG, project)

/**
 * Close Update Project Dialog Action
 */
export const closeUpdateProjectDialogAction = () =>
  action(DialogsActionTypes.CLOSE_UPDATE_PROJECT_DIALOG)
