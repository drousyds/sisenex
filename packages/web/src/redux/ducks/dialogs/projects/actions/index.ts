import {
  openProjectDetailsDialogAction,
  closeProjectDetailsDialogAction
} from './projectDetailsDialogActions'

import {
  openUpdateProjectDialogAction,
  closeUpdateProjectDialogAction
} from './updateProjectDialogActions'

export {
  openProjectDetailsDialogAction,
  closeProjectDetailsDialogAction,
  openUpdateProjectDialogAction,
  closeUpdateProjectDialogAction
}
