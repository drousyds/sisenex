export enum FiltersActionTypes {
  ON_CHANGE_FILTER_TEXT = '@filters/text/change',
  SET_USER_FUNCTION_FILTER = '@filters/user/function/set',
  SET_PROJECTS_FIELD_FILTER = '@filters/projects/field/set',
  SET_PROJECTS_UNIT_FILTER = '@filters/projects/unit/set'
}
