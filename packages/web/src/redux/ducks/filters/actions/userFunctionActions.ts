import { action } from 'typesafe-actions'
import { FiltersActionTypes } from '../filtersTypes'

interface FilterValues {
  monitor: boolean
  reviewer: boolean
  manager: boolean
}

export const setUserFunctionFilterAction = (values: FilterValues) =>
  action(FiltersActionTypes.SET_USER_FUNCTION_FILTER, values)
