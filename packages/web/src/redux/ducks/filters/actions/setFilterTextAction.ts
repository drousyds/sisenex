import { action } from 'typesafe-actions'
import { FiltersActionTypes } from '../filtersTypes'

const setFilterTextAction = (value: string) =>
  action(FiltersActionTypes.ON_CHANGE_FILTER_TEXT, value)

export default setFilterTextAction
