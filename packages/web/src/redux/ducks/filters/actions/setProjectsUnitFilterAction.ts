import { action } from 'typesafe-actions'
import { FiltersActionTypes } from '../filtersTypes'

const setProjectsUnitFilterAction = (value?: number) =>
  action(FiltersActionTypes.SET_PROJECTS_UNIT_FILTER, value)

export default setProjectsUnitFilterAction
