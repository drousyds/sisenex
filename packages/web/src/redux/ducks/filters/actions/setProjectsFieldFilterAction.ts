import { action } from 'typesafe-actions'
import { FiltersActionTypes } from '../filtersTypes'

const setProjectsFieldFilterAction = (value?: number) =>
  action(FiltersActionTypes.SET_PROJECTS_FIELD_FILTER, value)

export default setProjectsFieldFilterAction
