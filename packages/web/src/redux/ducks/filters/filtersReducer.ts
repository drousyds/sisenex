import { Reducer } from 'redux'
import { FiltersActionTypes } from './filtersTypes'

export interface FiltersReducerStateType {
  filterText: string
  users: {
    function: {
      monitor: boolean
      reviewer: boolean
      manager: boolean
    }
  }
  projects: {
    fieldId?: number
    unitId?: number
  }
}
const initialState: FiltersReducerStateType = {
  filterText: '',
  users: {
    function: {
      monitor: false,
      reviewer: false,
      manager: false
    }
  },
  projects: {
    fieldId: undefined,
    unitId: undefined
  }
}

const filtersReducer: Reducer<FiltersReducerStateType> = (
  state = initialState,
  action
) => {
  const { type, payload } = action
  let newState: FiltersReducerStateType

  switch (type) {
    /**
     * FilterText
     */
    case FiltersActionTypes.ON_CHANGE_FILTER_TEXT:
      newState = {
        ...state,
        filterText: payload
      }
      return newState

    /**
     * Users Filters
     */
    case FiltersActionTypes.SET_USER_FUNCTION_FILTER:
      newState = {
        ...state,
        users: {
          ...state.users,
          function: {
            ...state.users.function,
            ...payload
          }
        }
      }
      return newState

    /**
     * Projects
     */
    case FiltersActionTypes.SET_PROJECTS_FIELD_FILTER:
      newState = {
        ...state,
        projects: {
          ...state.projects,
          fieldId: payload
        }
      }
      return newState

    case FiltersActionTypes.SET_PROJECTS_UNIT_FILTER:
      newState = {
        ...state,
        projects: {
          ...state.projects,
          unitId: payload
        }
      }
      return newState
    default:
      return state
  }
}

export default filtersReducer
