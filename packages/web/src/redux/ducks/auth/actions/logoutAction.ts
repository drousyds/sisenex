import { action } from 'typesafe-actions'
import { AuthActionTypes } from '../authTypes'
import { ErrorPayloadType } from '../../../../types'

// Logout User Action
const logoutUserAction = () => action(AuthActionTypes.LOGOUT_USER)

// On Sucess
export const logoutUserSuccessAction = () =>
  action(AuthActionTypes.LOGOUT_USER_SUCCESS)

// On Error
export const logoutUserErrorAction = (errorPayload: ErrorPayloadType) =>
  action(AuthActionTypes.LOGOUT_USER_ERROR, errorPayload)

export default logoutUserAction
