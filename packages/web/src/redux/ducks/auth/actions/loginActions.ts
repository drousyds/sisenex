import { action } from 'typesafe-actions'

// Types
import { AuthActionTypes } from '../authTypes'
import { Pessoa } from '../../../../generated'

// Logout User Action
export const loginUserAction = (user: Pessoa) =>
  action(AuthActionTypes.LOGIN_USER_SUCCESS, user)
