import { action } from 'typesafe-actions'
import { AuthActionTypes } from '../authTypes'
import { Pessoa } from '../../../../generated'

// Set Vinculos Actions
export const setVinculosAction = (vinculos: Pessoa[]) =>
  action(AuthActionTypes.AUTH_STAGE_SET_VINCULOS, vinculos)

export const selectVinculoAction = (vinculo: Pessoa) =>
  action(AuthActionTypes.AUTH_STAGE_SELECT_VINCULO, vinculo)

export const clearAuthStageAction = () =>
  action(AuthActionTypes.AUTH_STAGE_CLEAR)
