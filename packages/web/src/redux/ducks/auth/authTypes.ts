export enum AuthActionTypes {
  LOGOUT_USER = '@auth/logout',
  LOGOUT_USER_SUCCESS = '@auth/logout/SUCCESS',
  LOGOUT_USER_ERROR = '@auth/logout/ERROR',

  LOGIN_USER_SUCCESS = '@auth/login/SUCCESS',

  OAUTH_SINGUP = '@auth/singup',
  OAUTH_SINGUP_SUCCESS = '@auth/singup/SUCCESS',
  OAUTH_SINGUP_ERROR = '@auth/singup/ERROR',

  OAUTH_LOGIN = '@auth/login/oauth/',
  OAUTH_LOGIN_SUCCESS = '@auth/login/oauth/SUCCESS',
  OAUTH_LOGIN_ERROR = '@auth/login/oauth/ERROR',

  API_LOGIN = '@auth/login/api',
  API_LOGIN_SUCCESS = '@auth/login/api/SUCCESS',
  API_LOGIN_ERROR = '@auth/login/api/ERROR',

  AUTH_STAGE_SET_VINCULOS = '@auth/stage/set/vinculos',
  AUTH_STAGE_SELECT_VINCULO = '@auth/stage/select/vinculo',
  AUTH_STAGE_CLEAR = '@auth/stage/clear'
}
