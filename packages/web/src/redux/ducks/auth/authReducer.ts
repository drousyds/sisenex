import { Reducer } from 'redux'
import { AuthActionTypes } from './authTypes'
import { Pessoa } from '../../../generated'

export interface AuthReducerStateType {
  loading: boolean
  currentUser?: Pessoa
  stage: {
    vinculos: Pessoa[]
    selected?: Pessoa
  }
}

const FAKE_USER = {
  idPessoa: '7820',
  idVinculoPessoa: '7820',
  matriculaPessoa: '336628',
  nomePessoa: 'MARCONILIA MARIA DIAS ARNOUD SILVA',
  nomeSocialPessoa: 'MARCONILIA MARIA DIAS ARNOUD SILVA',
  lotacaoPessoa: undefined,
  emailPessoa: 'marconilia@prac.ufpb.br',
  telefonePessoa: undefined,
  aptidaoPessoa: 1,
  avaliadorPessoa: 1,
  monitorPessoa: 0,
  gerentePessoa: 1
}

const INITAL_STATE: AuthReducerStateType = {
  loading: false,
  currentUser:
    process.env.REACT_APP_NODE_ENV === 'local' ? FAKE_USER : undefined,
  stage: {
    vinculos: [],
    selected: undefined
  }
}

const authReducer: Reducer<AuthReducerStateType> = (
  state = INITAL_STATE,
  action
) => {
  const { type, payload } = action
  let newState: AuthReducerStateType

  switch (type) {
    case AuthActionTypes.LOGOUT_USER:
      newState = {
        ...state,
        loading: true
      }
      return newState
    case AuthActionTypes.LOGOUT_USER_SUCCESS:
      newState = {
        ...state,
        loading: false,
        currentUser: undefined
      }
      return newState

    case AuthActionTypes.AUTH_STAGE_SET_VINCULOS: {
      newState = {
        ...state,
        stage: {
          vinculos: payload
        }
      }
      return newState
    }

    case AuthActionTypes.AUTH_STAGE_SELECT_VINCULO: {
      newState = {
        ...state,
        stage: {
          ...state.stage,
          selected: payload
        }
      }
      return newState
    }

    case AuthActionTypes.AUTH_STAGE_CLEAR: {
      newState = {
        ...state,
        stage: {
          vinculos: [],
          selected: undefined
        }
      }
      return newState
    }

    case AuthActionTypes.LOGIN_USER_SUCCESS: {
      newState = {
        ...state,
        currentUser: payload,
        stage: {
          vinculos: [],
          selected: undefined
        }
      }
      return newState
    }

    default:
      return state
  }
}

export default authReducer
