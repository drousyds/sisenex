import React from 'react'

import { Apresentacao } from '../../generated'
import { usePresentationsInfo } from '../../hooks'
import CounterItem from '../common/CounterItem'
import RoundedDiv from '../common/RoundedDiv'

interface Props {
  presentations: Apresentacao[]
}

const PresentationsCard: React.FC<Props> = ({ presentations }) => {
  const info = usePresentationsInfo(presentations)
  return (
    <RoundedDiv shadow style={{ height: '100%' }}>
      <CounterItem
        counter={info.total}
        strong
        pre="Apresentações:"
        fallback="Sem Apresentações"
      />
      <CounterItem
        counter={info.type.tertulias}
        strong
        pre="Tertúlias:"
        fallback="Sem Tertúlias"
      />
      <CounterItem
        counter={info.type.peformances}
        strong
        pre="Performances:"
        fallback="Sem Performances"
      />
      <CounterItem
        counter={info.status.open}
        strong
        pre="Apresentações em Execução:"
        fallback="Não há apresentações em execução"
      />
      <CounterItem
        counter={info.status.closed}
        strong
        pre="Apresentações Fechadas:"
        fallback="Não há apresentações fechadas"
      />
    </RoundedDiv>
  )
}

export default PresentationsCard
