import { Typography, Table } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import _ from 'lodash'
import React, { useMemo } from 'react'
import { Grid, Cell } from 'styled-css-grid'

import { Projeto } from '../../generated'
import { useGetProjectStatusCounter } from '../../hooks'
import { HorizontalFade } from '../animation'
import RoundedDiv from '../common/RoundedDiv'

interface Props {
  areaTematica: string
  projects: Projeto[]
}

const { Title, Text } = Typography

const FieldCard = ({ areaTematica, projects }: Props) => {
  const total = projects.length
  const counter = useGetProjectStatusCounter(projects)
  const avgScore = useMemo(
    () => _.meanBy(projects.filter(p => p.media), ({ media }) => media),
    [projects]
  )

  const cardTitle = (
    <Grid
      columns="repeat(auto-fit, minmax(250px, 1fr))"
      coljustifyContent="start"
      style={{ marginTop: 10 }}
    >
      <Cell>
        <Title level={4} style={{ margin: 0, marginBottom: 4 }}>
          {areaTematica}
        </Title>
        <div>
          <Text strong>Projetos Avaliados: </Text>
          <Text>{`${counter.evaluated} / ${total}`}</Text>
        </div>
      </Cell>
      <Cell>
        <div
          style={{ height: '100%', display: 'flex', alignItems: 'flex-end' }}
        >
          <div>
            <Text strong>{`Concluídos: `}</Text>
            <Text>{`${((counter.evaluated / counter.total) * 100).toFixed(
              2
            )}%`}</Text>
          </div>
        </div>
      </Cell>
      <Cell>
        <div
          style={{
            height: '100%',
            display: 'flex',
            alignItems: 'flex-end',
            justifyContent: 'center'
          }}
        >
          <div>
            <Text strong>Média: </Text>
            <Text>
              {avgScore ? (
                avgScore.toFixed(3)
              ) : (
                <span style={{ marginLeft: 5 }}>{`-`}</span>
              )}
            </Text>
          </div>
        </div>
      </Cell>
    </Grid>
  )

  const columns: ColumnProps<Projeto & { index: number }>[] = [
    {
      title: 'Nº',
      key: 'projectIndex',
      render: (a: any, project) => <span>{`${project.index + 1}°`}</span>
    },
    {
      title: 'Código',
      dataIndex: 'idprojeto'
    },
    {
      title: 'Nome',
      dataIndex: 'tituloProjeto'
    },
    {
      title: 'Nota',
      key: 'projectAverageScore',
      render: (_, project) => <span>{project.media!.toFixed(3)}</span>
    }
  ]

  return (
    <HorizontalFade direction="right" amount={20} delay={100}>
      <RoundedDiv style={{ marginTop: 10 }}>
        {cardTitle}
        <Table
          style={{ marginTop: 10, overflowX: 'auto' }}
          columns={columns}
          rowKey={project => `${project.idProjeto}`}
          dataSource={projects
            .filter(({ media }) => !!media)
            .sort(({ media: mediaA }, { media: mediaB }) => {
              return mediaB! - mediaA!
            })
            .slice(0, 5)
            .map((project, i) => ({
              ...project,
              index: i
            }))}
          pagination={false}
        />
      </RoundedDiv>
    </HorizontalFade>
  )
}

export default FieldCard
