import React from 'react'

import CounterItem from '../../components/common/CounterItem'
import RoundedDiv from '../../components/common/RoundedDiv'
import { Pessoa } from '../../generated'
import { useUsersCounter } from '../../hooks'

interface Props {
  users: Pessoa[]
}

const UsersCard: React.FC<Props> = ({ users }) => {
  const counter = useUsersCounter(users)
  return (
    <RoundedDiv shadow style={{ height: '100%' }}>
      <CounterItem
        counter={counter.monitores}
        pre="Monitores:"
        strong
        fallback="Sem monitores"
      />
      <CounterItem
        counter={counter.avaliadores}
        pre="Avaliadores:"
        strong
        fallback="Sem avaliadores"
      />
      <CounterItem
        counter={counter.gerentes}
        pre="Gerentes:"
        strong
        fallback="Sem gerentes"
      />
      <CounterItem
        counter={counter.administrador}
        pre="Administradores:"
        strong
        fallback="Sem administradores"
      />
    </RoundedDiv>
  )
}

export default UsersCard
