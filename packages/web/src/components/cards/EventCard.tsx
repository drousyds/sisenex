import React from 'react'

import { Evento } from '../../generated'
import { getCampusNumber } from '../../helpers'
import InlineStrongText from '../common/InlineStrongText'
import RoundedDiv from '../common/RoundedDiv'

interface Props {
  event: Evento
}

const EventCard: React.FC<Props> = ({ event }) => {
  return (
    <RoundedDiv shadow style={{ height: '100%' }}>
      <InlineStrongText
        pre="Evento: "
        text={event.nomeEvento}
        strong={true}
      />
      <InlineStrongText
        pre="Descrição: "
        text={event.descricaoEvento ? event.descricaoEvento : 'Sem Descrição'}
        strong={true}
      />
      <InlineStrongText
        pre="Local: "
        text={event.campi.map(e => getCampusNumber(e.idCampus)).join()}
        strong={true}
      />
    </RoundedDiv>
  )
}

export default EventCard
