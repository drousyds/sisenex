import React from 'react'
import { useHistory } from 'react-router'
import { Result, Button } from 'antd'
import { connect } from 'react-redux'

import { VerticalFade } from '../animation'
import RoundedDiv from '../common/RoundedDiv'
import { Evento, AreaTematica, useGetAreaTematicaQuery } from '../../generated'
import { openNotificationWithIcon } from '../../helpers'
import { Dispatch } from 'redux'
import { openAllocationDialogAction } from '../../redux/ducks/dialogs/events/actions'

interface OwnProps {
  event: Evento
  idAreaTematica: string
}

interface DispatchProps {
  openAllocationDialog: (event: Evento, idAreaTematica: number) => void
}

type Props = OwnProps & DispatchProps

const NoPresentationsResult: React.FC<Props> = ({
  event,
  idAreaTematica,
  openAllocationDialog
}) => {
  const history = useHistory()

  const id = parseInt(idAreaTematica, 10)

  const { data } = useGetAreaTematicaQuery({
    variables: { idAreaTematica: id },
    onError: () => {
      openNotificationWithIcon(
        'error',
        'Ocorreu um erro ao buscar a Área Temática.',
        5.5
      )
    }
  })
  let area: AreaTematica | undefined = undefined

  if (data && data.areaTematica) {
    area = data.areaTematica
  }

  return (
    <VerticalFade direction="up" amount={20}>
      <RoundedDiv shadow style={{ marginTop: 10 }}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
            marginTop: 10
          }}
        >
          <Result
            status="warning"
            title={`Sem Apresentações`}
            subTitle={`As apresentações da área tematica ${
              area ? area.nomeAreaTematica : null
            } do evento ${event.nomeEvento} não foram distribuídas.`}
            extra={[
              <Button
                type="primary"
                key="event-area-distribuir-btn"
                onClick={() => openAllocationDialog(event, id)}
              >
                Distribuir Apresentações
              </Button>,
              <Button
                key="event-area-goback-btn"
                onClick={() =>
                  history.push(`/eventos/${event.idEvento}/tertulias`)
                }
              >
                Voltar
              </Button>
            ]}
          />
        </div>
      </RoundedDiv>
    </VerticalFade>
  )
}

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  openAllocationDialog: (event: Evento, idAreaTematica: number) =>
    dispatch(openAllocationDialogAction({ event, idAreaTematica }))
})

export default connect<{}, DispatchProps, OwnProps>(
  null,
  mapDispatchToProps
)(NoPresentationsResult)
