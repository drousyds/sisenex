import { Table } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import React from 'react'

import { Evento } from '../../generated'
import { AnimatedContent } from '../animation'
import RoundedDiv from '../common/RoundedDiv'

interface Props {
  events: Evento[]
  loading: boolean
  columns: ColumnProps<Evento>[]
}

const EventsTable: React.FC<Props> = ({ events, loading, columns }) => {
  return (
    <AnimatedContent>
      <RoundedDiv shadow style={{ marginTop: 10 }}>
        <div style={{ overflowX: 'auto' }}>
          <Table
            loading={loading}
            dataSource={events}
            rowKey={e => `${e.idEvento}`}
            columns={columns}
          />
        </div>
      </RoundedDiv>
    </AnimatedContent>
  )
}

export default EventsTable
