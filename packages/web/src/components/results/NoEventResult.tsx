import { Result } from 'antd'
import React from 'react'

import RoundedDiv from '../common/RoundedDiv'

const NoEventResult: React.FC = () => {
  return (
    <RoundedDiv shadow style={{ marginTop: 10 }}>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
          marginTop: 10
        }}
      >
        <Result
          status="warning"
          title='Nenhum evento foi selecionado'
          subTitle="Selecione no filtro acima o Evento desejado."
        />
      </div>
    </RoundedDiv>
  )
}

export default NoEventResult
