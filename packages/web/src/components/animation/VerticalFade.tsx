import React from 'react'
import { animated, useSpring } from 'react-spring'
import { getAnimation } from '../../helpers'

interface Props {
  direction: 'up' | 'down'
  amount: number
  delay?: number
}

const VerticalFade: React.FC<Props> = ({
  direction,
  amount,
  children,
  delay
}) => {
  const fromY =
    direction === 'up' ? Math.abs(amount) : amount < 0 ? amount : -amount
  const props = useSpring({
    delay: delay !== undefined ? delay : undefined,
    ...getAnimation({
      from: { opacity: 0, x: 0, y: fromY, z: 0 },
      to: { opacity: 1, x: 0, y: 0, z: 0 }
    })
  })
  return (
    <animated.div style={{ ...props, height: '100%' }}>{children}</animated.div>
  )
}

export default VerticalFade
