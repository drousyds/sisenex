import React from 'react'
import { animated, useSpring } from 'react-spring'
import { getAnimation } from '../../helpers'

interface Props {
  direction: 'left' | 'right'
  amount: number
  delay?: number
}

const HorizontalFade: React.FC<Props> = ({
  direction,
  amount,
  children,
  delay
}) => {
  const fromX =
    direction === 'left' ? Math.abs(amount) : amount < 0 ? amount : -amount
  const props = useSpring({
    delay: delay !== undefined ? delay : undefined,
    ...getAnimation({
      from: { opacity: 0, x: fromX, y: 0, z: 0 },
      to: { opacity: 1, x: 0, y: 0, z: 0 }
    })
  })
  return (
    <animated.div style={{ ...props, height: '100%' }}>{children}</animated.div>
  )
}

export default HorizontalFade
