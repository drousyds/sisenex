import React from 'react'
import VerticalFade from './VerticalFade'

const AnimatedContent: React.FC = ({ children }) => {
  return (
    <VerticalFade direction="up" amount={20}>
      {children}
    </VerticalFade>
  )
}

export default AnimatedContent
