import React from 'react'
import VerticalFade from './VerticalFade'

const AnimatedHeader: React.FC = ({ children }) => {
  return (
    <VerticalFade direction="down" amount={20}>
      {children}
    </VerticalFade>
  )
}

export default AnimatedHeader
