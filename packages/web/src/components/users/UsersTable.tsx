import { EditOutlined, EyeOutlined } from '@ant-design/icons'
import { Table, Button, Tag, Tooltip, Result } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import _ from 'lodash'
import React from 'react'
import { useDispatch } from 'react-redux'

import { Pessoa, Projeto } from '../../generated'
import {
  getProjectStatus,
  getStatusTagColor,
  getUserRoles,
  renderLongUnixDate
} from '../../helpers'
import {
  openProjectDetailsDialogAction,
  openUpdateProjectDialogAction
} from '../../redux/ducks/dialogs/projects/actions'
import { openUpdateUserDialogAction } from '../../redux/ducks/dialogs/users/actions'
import { AnimatedContent } from '../animation'
import RoundedDiv from '../common/RoundedDiv'
import UserProjectsTable from './UserProjectsTable'

interface Props {
  loading: boolean
  tableFooter?: React.ReactNode
  users: Pessoa[]
}

const UsersTable: React.FC<Props> = ({ users, loading, tableFooter }) => {
  const dispatch = useDispatch()
  const columns: ColumnProps<Pessoa>[] = [
    {
      title: 'Matrícula',
      key: 'matriculaPessoa',
      render: (_, { matriculaPessoa }) => (
        <span>{matriculaPessoa || ' - '}</span>
      )
    },
    {
      title: 'Nome',
      dataIndex: 'nomePessoa',
      key: 'nomePessoa'
    },
    {
      title: 'Email',
      key: 'emailPessoa',
      render: (_, { emailPessoa }) => emailPessoa && <span>{emailPessoa}</span>
    },
    {
      title: 'Data de login',
      key: 'created_at',
      render: (_, { created_at }) =>
        created_at && <span>{renderLongUnixDate(created_at)}</span>
    },
    {
      title: 'Atribuição',
      key: 'atribuicaoPessoa',
      render: (_, user) => (
        <span>
          {(user.administradorPessoa && (
            <Tag
              color="geekblue"
              key={`${user.idPessoa + user.idVinculoPessoa}Administrador`}
            >
              ADMINISTRADOR
            </Tag>
          )) ||
            null}
          {(user.avaliadorPessoa && (
            <Tag
              color="geekblue"
              key={`${user.idPessoa + user.idVinculoPessoa}Avaliador`}
            >
              AVALIADOR
            </Tag>
          )) ||
            null}
          {(user.monitorPessoa && (
            <Tag
              color="geekblue"
              key={`${user.idPessoa + user.idVinculoPessoa}Monitor`}
            >
              MONITOR
            </Tag>
          )) ||
            null}
          {(user.gerentePessoa && (
            <Tag
              color="geekblue"
              key={`${user.idPessoa + user.idVinculoPessoa}Gerente`}
            >
              GERENTE
            </Tag>
          )) ||
            null}
          {getUserRoles(user).length === 0 && (
            <Tag key="SemAtribuicao">SEM ATRIBUIÇÃO</Tag>
          )}
        </span>
      )
    },
    {
      title: 'Editar',
      key: 'editarUsuatio',
      render: (_, user) => (
        <span>
          <Tooltip title="Editar Pessoa">
            <Button
              onClick={() => dispatch(openUpdateUserDialogAction(user))}
              icon={<EditOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    }
  ]
  const expandedRowRender = () => {
    const columns = [
      { title: 'Projeto', dataIndex: 'tituloProjeto', key: 'project' }
    ]
    const data = [{ tituloProjeto: 'Teste' }]

    return <Table columns={columns} dataSource={data} pagination={false} />
  }

  return (
    <AnimatedContent>
      <RoundedDiv shadow style={{ marginTop: 10 }}>
        <div style={{ overflowX: 'auto' }}>
          <Table
            loading={loading}
            columns={columns}
            rowKey={(user): string =>
              `${user.idPessoa}-${user.idVinculoPessoa}`
            }
            dataSource={users}
            footer={() => tableFooter || null}
            expandedRowRender={(user, _, __, expand) =>
              expand ? <UserProjectsTable user={user} /> : null
            }
          />
        </div>
      </RoundedDiv>
    </AnimatedContent>
  )
}

export default UsersTable
