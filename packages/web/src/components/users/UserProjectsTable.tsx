import React from 'react'
import { DeleteOutlined, EditOutlined, EyeOutlined } from '@ant-design/icons'
import { Table, Tag, Button, Tooltip, Result } from 'antd'
import { ColumnProps } from 'antd/lib/table'

// Helpers
import {
  getProjectStatus,
  getStatusTagColor,
  openNotificationWithIcon
} from '../../helpers'
import {
  Apresentacao,
  Pessoa,
  Projeto,
  useGetPresentationProjectsQuery,
  useProjetosAvaliadorQuery,
  useUpdateProjectMutation
} from '../../generated'
import { NotificationType } from '../../types'
import { MUTATIONS_MESSAGES } from '../../constants/messages'
import getPersonalProjectStatus from '../../helpers/getPersonalProjectStatus'
import getPersonalProjectColor from '../../helpers/getPersonalProjectColor'

interface Props {
  user: Pessoa
}

const UserProjectsTable = ({ user }: Props) => {
  // Query
  const { data, loading, refetch } = useProjetosAvaliadorQuery({
    variables: {
      idPessoa: user.idPessoa,
      idVinculoPessoa: user.idVinculoPessoa
    },
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon(
        'error',
        `Ocorreu um erro ao buscar os projetos`,
        6.5
      )
    }
  })

  let projects: any = []
  if (data && data.apresentacoesAvaliador) {
    projects = data.apresentacoesAvaliador.flatMap(p => p.projetos)
  }

  const presentationsProjectsTable: ColumnProps<Projeto>[] = [
    {
      title: 'Código',
      dataIndex: 'codigoProjeto'
    },
    {
      title: 'Projeto',
      dataIndex: 'tituloProjeto'
    },
    {
      title: 'Estado',
      key: 'projectStatus',
      render: (_, project) => {
        return (
          <span>
            <Tag
              color={getPersonalProjectColor(
                project.mediaIndividual?.mediaProjeto!
              )}
              key="projetStatusTag"
            >
              {getPersonalProjectStatus(project.mediaIndividual?.mediaProjeto!)}
            </Tag>
          </span>
        )
      }
    }
  ]

  return (
    <Table
      loading={loading}
      columns={presentationsProjectsTable}
      rowKey={(projects, i) => `${projects.idProjeto}${i! * 2}`}
      dataSource={projects || []}
      pagination={false}
      locale={{
        emptyText: (
          <span>
            <Result
              title="Este avaliador ainda não possui projetos"
              status="info"
            />
          </span>
        )
      }}
    />
  )
}

export default UserProjectsTable
