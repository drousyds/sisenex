import React from 'react'
import { QrcodeOutlined } from '@ant-design/icons';
import { Button } from 'antd'
import { Apresentacao } from '../../generated'
import jsPDF from 'jspdf'
import QRCode from 'qrcode/build/qrcode'
import { getModeText } from '../../helpers'

interface Props {
  loading: boolean
  data: Apresentacao[] | Apresentacao
  style?: React.CSSProperties
}

const parsePDFText = ({
  modalidadeApresentacao,
  areaTematica,
  evento,
  salaApresentacao
}: Apresentacao) => {
  const text: string[] = []

  if (evento) text.push(`Evento: ${evento.nomeEvento}`)
  if (areaTematica) text.push(`Área Temática: ${areaTematica.nomeAreaTematica}`)
  if (modalidadeApresentacao)
    text.push(`Modalidade: ${getModeText(modalidadeApresentacao)}`)
  if (salaApresentacao) text.push(`Sala: ${salaApresentacao}`)

  return text
}

const generatePDF = async (data: Apresentacao[] | Apresentacao) => {
  // const isArray = data instanceof Array
  const presentations = data instanceof Array ? data : [data]
  // const presentations = isArray ? data : [data!]
  // console.log('Gerando PDF')
  const doc = new jsPDF({
    // unit: 'px',
    format: 'a4'
  })
  const QR_SIZE = 100
  const width = doc.internal.pageSize.getWidth()
  const height = doc.internal.pageSize.getHeight()
  const QR_X = Math.floor(width / 2 - QR_SIZE / 2)
  const QR_Y = Math.floor(height / 2 - QR_SIZE / 2)
  // console.log('PRESENTATIONS: ', presentations.length)
  presentations.forEach((p, i) => {
    // const key = `${p.idApresentacao}-${p.id2Apresentacao}`

    doc.setTextColor(50)
    doc.setFontSize(20)
    doc.text(parsePDFText(p), width / 2, 40, {
      lineHeightFactor: 1.5,
      align: 'center'
    })

    // QR CODE
    QRCode.toDataURL(
      p.codigoApresentacao,
      {
        width: 256,
        height: 256,
        scale: 8,
        margin: 2
      },
      function(err, url) {
        // console.log('URL', url)
        // console.log('X: ', QR_X)
        if (!err) doc.addImage(url, 'PNG', QR_X, QR_Y, QR_SIZE, QR_SIZE)
      }
    )
    doc.setTextColor(0)
    doc.setFontSize(30)
    doc.text(
      `Apresentação: ${p.codigoApresentacao}`,
      width / 2,
      QR_Y + QR_SIZE + 15,
      {
        lineHeightFactor: 1.5,
        align: 'center'
      }
    )

    // FOOTER
    doc.setTextColor(100)
    doc.setFontSize(10)
    doc.text(
      `SisEnex - Sistema de Avaliação do ENEX`,
      width / 2,
      height - 10,
      'center'
    )
    if (i !== presentations.length - 1) doc.addPage()
  })
  doc.save(`apresentacoes.pdf`)
}

const GenerateQRButton: React.FC<Props> = ({ loading, data, style }) => {
  const isArray = data instanceof Array

  const handleClick = () => {
    generatePDF(data)
  }

  return (
    <Button
      style={style}
      onClick={() => {
        handleClick()
      }}
      icon={<QrcodeOutlined />}
      type={!isArray ? 'dashed' : undefined}
      shape={!isArray ? 'circle' : undefined}
      loading={loading}
      disabled={loading}
    >
      {isArray ? "Gerarar QR's" : null}
    </Button>
  );
}

export default GenerateQRButton
