import { Select } from 'antd'
import React from 'react'

import { FETCH_MESSAGES } from '../../constants/messages'
import { useAreasTematicasQuery, AreaTematica } from '../../generated'
import { openNotificationWithIcon } from '../../helpers'
import { NotificationType } from '../../types'

interface Props {
  loading?: boolean
  handleChange: (value: number | undefined) => void
  style?: React.CSSProperties
}

const ThematicsSelect: React.FC<Props> = ({ loading, handleChange, style }) => {
  const { data: atData, loading: atLoading } = useAreasTematicasQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        FETCH_MESSAGES.ERROR.THEMATICS,
        5.5
      )
    }
  })

  // at data handler
  let areas: AreaTematica[] = []
  if (atData && atData.areasTematicas) {
    areas = atData.areasTematicas
  }

  return (
    <Select
      disabled={atLoading || loading}
      loading={atLoading || loading}
      onChange={value => {
        handleChange(value as number | undefined)
      }}
      placeholder="Área Temática"
      allowClear
      defaultValue={undefined}
      style={style}
    >
      {areas.map(({ idAreaTematica, nomeAreaTematica }) => (
        <Select.Option key={idAreaTematica} value={idAreaTematica}>
          {nomeAreaTematica}
        </Select.Option>
      ))}
    </Select>
  )
}

export default ThematicsSelect
