import { Select } from 'antd'
import React from 'react'

interface Props {
  loading: boolean
  handleChange: (value: string | undefined) => void
  style?: React.CSSProperties
}

const ModeSelect: React.FC<Props> = ({ loading, handleChange, style }) => {
  return (
    <Select
      disabled={loading}
      loading={loading}
      onChange={value => {
        handleChange(value as string | undefined)
      }}
      placeholder="Modalidade"
      allowClear
      defaultValue={undefined}
      style={style}
    >
      <Select.Option
        key="tertulia-presentations-select"
        value="T"
      >{`Tertúlia`}</Select.Option>
      <Select.Option
        key="performance-presentations-select"
        value="P"
      >{`Performance`}</Select.Option>
    </Select>
  )
}

export default ModeSelect
