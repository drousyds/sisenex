import { Select } from 'antd';
import React, { useMemo } from 'react';

import { Categoria, useGetCategoriesQuery } from '../../generated';

interface Props {
  hasAllItems?: boolean;
  isModalidade?: boolean;
  loading?: boolean;
  disabled?: boolean;
  allowClear?: boolean;
  placeholder?: string;
  style?: React.CSSProperties;
  onChange?: (idCategoria: Categoria['idCategoria']) => void;
}

const CategorySelect: React.FC<Props> = ({
  hasAllItems = true,
  placeholder = '',
  isModalidade = true,
  onChange = () => {},
  loading = false,
  disabled = false,
  allowClear = false,
  style = {},
}) => {
  const { data, loading: queryLoading } = useGetCategoriesQuery();
      placeholder = isModalidade ? 'Selecione a modalidade' : 'Selecione a categoria';
  const categorias = useMemo(() => {
    const { categorias } = data || {};
    return ( !hasAllItems ? (categorias || []).filter(categoria => categoria.idCategoria === 3 || categoria.idCategoria === 4) : (categorias || [] ))
  }, [data]);

  return (
    <Select
      style={style}
      allowClear={allowClear}
      loading={queryLoading || loading}
      disabled={queryLoading || disabled}
      
      placeholder={placeholder}
      onChange={onChange}
    >
      {categorias.map(categoria => (
        <Select.Option
          value={categoria.idCategoria}
          key={`select-categoria-${categoria.idCategoria}`}
        >
          {categoria.nomeCategoria}
        </Select.Option>
      ))}
    </Select>
  );
};

export default CategorySelect;
