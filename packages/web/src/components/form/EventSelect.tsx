import { Select } from 'antd'
import React from 'react'

import { FETCH_MESSAGES } from '../../constants/messages'
import { useGetEventsQuery, Evento } from '../../generated'
import { openNotificationWithIcon } from '../../helpers'
import { NotificationType } from '../../types'

interface Props {
  loading?: boolean
  allowClear?: boolean
  handleChange: (value: number | undefined) => void
  style?: React.CSSProperties
}

const EventSelect: React.FC<Props> = ({
  loading,
  handleChange,
  style,
  allowClear
}) => {
  // Query Hook
  const { data: eventsData, loading: eventsLoading } = useGetEventsQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        FETCH_MESSAGES.ERROR.EVENTS,
        5.5
      )
    }
  })

  // events data handler
  let events: Evento[] = []
  if (eventsData && eventsData.eventos) {
    events = eventsData.eventos
  }

  return (
    <Select
      disabled={eventsLoading || loading}
      loading={eventsLoading || loading}
      onChange={value => {
        handleChange(value as number | undefined)
      }}
      placeholder="Evento"
      allowClear={allowClear || false}
      defaultValue={undefined}
      style={style}
    >
      {events.map(({ idEvento, nomeEvento }) => (
        <Select.Option key={idEvento} value={idEvento}>
          {nomeEvento}
        </Select.Option>
      ))}
    </Select>
  )
}

export default EventSelect
