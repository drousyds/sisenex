import { Select } from 'antd'
import React from 'react'

import { PresentationStatus } from '../../types'

interface Props {
  handleChange: (value: string | undefined) => void
  style?: React.CSSProperties
  loading: boolean
}

const PresentationStatusSelect: React.FC<Props> = ({
  handleChange,
  style,
  loading
}) => {
  return (
    <Select
      onChange={value => {
        handleChange(value as string | undefined)
      }}
      placeholder="Status"
      allowClear
      style={style}
      defaultValue={undefined}
      disabled={loading}
      loading={loading}
    >
      <Select.Option key="running-select" value={PresentationStatus.onGoing}>
        {PresentationStatus.onGoing}
      </Select.Option>
      <Select.Option key="pending-select" value={PresentationStatus.pending}>
        {PresentationStatus.pending}
      </Select.Option>
      <Select.Option key="pending-select" value={PresentationStatus.completed}>
        {PresentationStatus.completed}
      </Select.Option>
    </Select>
  )
}

export default PresentationStatusSelect
