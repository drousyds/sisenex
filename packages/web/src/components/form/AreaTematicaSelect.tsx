import { Select } from 'antd'
import React, { useMemo } from 'react'

import { AreaTematica, useAreasTematicasQuery } from '../../generated'

interface Props {
  loading?: boolean
  disabled?: boolean
  placeholder?: string
  onChange?: (idAreaTematica: AreaTematica['idAreaTematica']) => void
}

const AreaTematicaSelect: React.FC<Props> = ({
  placeholder = 'Selecione a Área Temática',
  onChange = () => {},
  loading = false,
  disabled = false
}) => {
  const { data, loading: queryLoading } = useAreasTematicasQuery()

  const areasTematicas = useMemo(() => {
    const { areasTematicas } = data || {}
    return areasTematicas || []
  }, [data])

  return (
    <Select
      loading={queryLoading || loading}
      disabled={queryLoading || disabled}
      placeholder={placeholder}
      onChange={onChange}>
      {areasTematicas.map(({ idAreaTematica, nomeAreaTematica }) => {
        return (
          <Select.Option key={idAreaTematica} value={idAreaTematica}>
            {nomeAreaTematica}
          </Select.Option>
        )
      })}

    </Select>
  )
}

export default AreaTematicaSelect
