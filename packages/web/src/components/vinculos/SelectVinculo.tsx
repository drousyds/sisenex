import { Result, Button } from 'antd'
import React from 'react'

import { Pessoa } from '../../generated'

interface Props {
  vinculos: Pessoa[]
  onVinculoClick: (vinculo: Pessoa) => Promise<void>
}

const SelectVinculo: React.FC<Props> = ({ vinculos, onVinculoClick }) => {
  const handleClick = async (
    e: React.MouseEvent<HTMLElement, MouseEvent>,
    selectecVinculo: Pessoa
  ) => {
    e.preventDefault()
    await onVinculoClick(selectecVinculo)
  }

  const getVinculoText = (vinculo: Pessoa) => {
    if (vinculo.monitorPessoa) {
      return `Monitor - Matricula: ${vinculo.matriculaPessoa}`
    } else if (vinculo.avaliadorPessoa) {
      return `Avaliador - Matricula: ${vinculo.matriculaPessoa}`
    }
    return ''
  }

  return (
    <div>
      <Result
        status="warning"
        title="Mais de um vínculo foi identificado."
        subTitle=" Selecione qual deseja utilizar"
      >
        {vinculos.map(v =>
          v.matriculaPessoa ? (
            <Button
              key={v.matriculaPessoa}
              size="large"
              style={{ width: '100%', marginBottom: 15 }}
              onClick={e => handleClick(e, v)}
            >
              {getVinculoText(v)}
            </Button>
          ) : null
        )}
      </Result>
    </div>
  )
}

export default SelectVinculo
