import React from 'react'
import { Alert } from 'antd'
import { useTransition, animated } from 'react-spring'

interface Props {
  show: boolean
  showIcon?: boolean
  style?: React.CSSProperties
  type: 'success' | 'info' | 'warning' | 'error'
  title: string
  description?: string
}

const AnimatedAlert: React.FC<Props> = ({
  show,
  showIcon,
  style,
  type,
  title,
  description
}) => {
  const transitions = useTransition(show, null, {
    from: { transform: 'translate3d(-30px, 0, 0)', opacity: 0 },
    enter: { transform: 'translate3d(0, 0, 0)', opacity: 1 },
    leave: { transform: 'translate3d(30px, 0, 0)', opacity: 0 }
  })
  return (
    <React.Fragment>
      {transitions.map(
        ({ item, key, props }) =>
          item && (
            <animated.div key={key} style={props}>
              <Alert
                style={style}
                message={title}
                description={description || ''}
                type={type}
                showIcon={showIcon || false}
                closable
              />
            </animated.div>
          )
      )}
    </React.Fragment>
  )
}

export default AnimatedAlert
