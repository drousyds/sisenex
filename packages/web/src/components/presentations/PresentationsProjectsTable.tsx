import React from 'react'
import { DeleteOutlined, EditOutlined, EyeOutlined } from '@ant-design/icons'
import { Table, Tag, Button, Tooltip, Result } from 'antd'
import { ColumnProps } from 'antd/lib/table'

// Helpers
import {
  getProjectStatus,
  getStatusTagColor,
  openNotificationWithIcon
} from '../../helpers'
import {
  Apresentacao,
  Projeto,
  useGetPresentationProjectsQuery,
  useUpdateProjectMutation
} from '../../generated'
import { NotificationType } from '../../types'
import { MUTATIONS_MESSAGES } from '../../constants/messages'

interface Props {
  presentation: Apresentacao
  openProjectDetailsDialog: (project: Projeto) => void
  openUpdateProjectDialog: (project: Projeto) => void
}

const PresentationsProjectsTable = ({
  presentation,
  openProjectDetailsDialog,
  openUpdateProjectDialog
}: Props) => {
  // Query
  const { data, loading, refetch } = useGetPresentationProjectsQuery({
    variables: {
      input: {
        idApresentacao: presentation.idApresentacao,
        id2Apresentacao: presentation.id2Apresentacao
      }
    },
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon(
        'error',
        `Ocorreu um erro ao buscar os projetos da Apresentação ${presentation.codigoApresentacao}.`,
        6.5
      )
    }
  })

  // mutation
  const [mutate] = useUpdateProjectMutation({
    onCompleted: () => {
      openNotificationWithIcon(
        NotificationType.SUCCESS,
        MUTATIONS_MESSAGES.PROJECT.REMOVE.SUCCESS,
        5.5
      )
      refetch()
    },
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        MUTATIONS_MESSAGES.PROJECT.REMOVE.ERROR,
        5.5
      )
      refetch()
    }
  })

  let projects: Projeto[] = []

  if (data && data.apresentacao && data.apresentacao.projetos) {
    projects = data.apresentacao.projetos
  }

  const presentationsProjectsTable: ColumnProps<Projeto>[] = [
    {
      title: 'Código',
      dataIndex: 'codigoProjeto'
    },
    {
      title: 'Projeto',
      dataIndex: 'tituloProjeto'
    },
    {
      title: 'Estado',
      key: 'projectStatus',
      render: (_, project) => {
        return (
          <span>
            <Tag color={getStatusTagColor(project)} key="projetStatusTag">
              {getProjectStatus(project)}
            </Tag>
          </span>
        )
      }
    },
    {
      title: 'Editar',
      key: 'editProjectButton',
      align: 'center',
      render: (_, project) => (
        <span>
          <Tooltip title="Editar">
            <Button
              onClick={() => openUpdateProjectDialog(project)}
              icon={<EditOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    },
    {
      title: 'Visualizar',
      key: 'viewProject',
      align: 'center',
      render: (_, project) => (
        <span>
          <Tooltip title="Visualizar Projeto">
            <Button
              onClick={() => openProjectDetailsDialog(project)}
              icon={<EyeOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    },
    {
      title: 'Remover Projeto',
      key: 'remove-project',
      align: 'center',
      render: (_, { idProjeto }) => (
        <span style={{ margin: 10 }}>
          <Tooltip title="Remover Projeto">
            <Button
              onClick={async () => {
                console.log('REMOVER PROJETO ', idProjeto)
                await mutate({
                  variables: {
                    input: {
                      idProjeto: idProjeto,
                      idApresentacao: null,
                      id2Apresentacao: null,
                      idEvento: null
                    }
                  }
                })
              }}
              icon={<DeleteOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    }
  ]

  return (
    <Table
      loading={loading}
      columns={presentationsProjectsTable}
      rowKey={project => `${project.idProjeto}`}
      dataSource={projects || []}
      pagination={false}
      locale={{
        emptyText: (
          <span>
            <Result
              title="Esta apresentação ainda não possui projetos"
              status="info"
            />
          </span>
        )
      }}
    />
  )
}

export default PresentationsProjectsTable
