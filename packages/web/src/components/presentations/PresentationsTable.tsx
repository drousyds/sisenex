import React from 'react'
import { DeleteOutlined, EditOutlined, EyeOutlined } from '@ant-design/icons';
import { Table, Tag, Button, Tooltip } from 'antd'
import { ColumnProps } from 'antd/lib/table'

// Helpers
import {
  renderLongUnixDate,
  getPresentationStatus,
  getStatusTagColor,
  openNotificationWithIcon
} from '../../helpers'

// Types
import {
  Apresentacao,
  Projeto,
  useExcluirApresentacaoMutation,
  ApresentacoesInput
} from '../../generated'

import PresentationsProjectsTable from './PresentationsProjectsTable'
import RoundedDiv from '../common/RoundedDiv'
import { AnimatedContent } from '../animation'
import { GenerateQRButton } from '../qr'
import { NotificationType } from '../../types'
import { MUTATIONS_MESSAGES } from '../../constants/messages'
import { removePresentation } from '../../services/cache.services'

interface Props {
  loading: boolean
  tableFooter?: React.ReactNode
  presentations: Apresentacao[]
  queryVariables: ApresentacoesInput
  handleRefetch: () => void
  openProjectDetailsDialog: (project: Projeto) => void
  openUpdateProjectDialog: (project: Projeto) => void
  openUpdatePresentationDialog: (presentation: Apresentacao) => void
  openPresentationDetailsDialog: (presentation: Apresentacao) => void
}

const PresentationsTable = ({
  loading: queryLoading,
  presentations,
  openProjectDetailsDialog,
  openUpdatePresentationDialog,
  openPresentationDetailsDialog,
  openUpdateProjectDialog,
  handleRefetch,
  queryVariables,
  tableFooter
}: Props) => {
  const [remove, { loading: mLoading }] = useExcluirApresentacaoMutation({
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        MUTATIONS_MESSAGES.PRESENTATION.REMOVE.ERROR,
        5.5
      )
    },
    onCompleted: () => {
      openNotificationWithIcon(
        NotificationType.SUCCESS,
        MUTATIONS_MESSAGES.PRESENTATION.REMOVE.SUCCESS,
        5.5
      )
    },
    update: (proxy, { data }) => {
      if (data && data.excluirApresentacao) {
        removePresentation(proxy, data.excluirApresentacao, queryVariables)
      }
    }
  })
  const loading = queryLoading || mLoading

  const presentationsTable: ColumnProps<Apresentacao>[] = [
    {
      title: 'Código',
      key: 'presentationId',
      render: (_, { idApresentacao, id2Apresentacao, codigoApresentacao }) => (
        <span>
          {codigoApresentacao
            ? codigoApresentacao
            : `${idApresentacao}-${id2Apresentacao}`}
        </span>
      )
    },
    {
      title: 'Local',
      key: 'presentationRoom',
      render: (_, presentation) => (
        <span>
          {presentation.salaApresentacao
            ? presentation.salaApresentacao
            : 'Sem Sala'}
        </span>
      )
    },
    {
      title: 'Modalidade',
      dataIndex: 'modalidadeApresentacao',
      render: (_, presentation) => (
        <span>
          {presentation.modalidadeApresentacao === 'T'
            ? 'Tertúlia'
            : 'Performance'}
        </span>
      )
    },
    {
      title: 'Hora',
      key: 'presentationTime',
      render: (_, presentation) => (
        <span>
          {presentation.horaApresentacao
            ? renderLongUnixDate(presentation.horaApresentacao)
            : 'Sem Data'}
        </span>
      )
    },
    {
      title: 'Estado',
      key: 'presentationStatus',
      render: (_, presentation) => {
        return (
          <span>
            <Tag
              color={getStatusTagColor(presentation)}
              key="presentationStatusTag"
            >
              {getPresentationStatus(presentation)}
            </Tag>
          </span>
        )
      }
    },
    {
      title: 'Editar',
      key: 'editPresentationButton',
      align: 'center',
      render: (_, presentation) => (
        <span style={{ margin: 10 }}>
          <Tooltip title="Editar Apresentação">
            <Button
              onClick={() => openUpdatePresentationDialog(presentation)}
              icon={<EditOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    },
    {
      title: 'Visualizar',
      key: 'presentationDetailsButton',
      align: 'center',
      render: (_, presentation) => (
        <span style={{ margin: 10 }}>
          <Tooltip title="Visualizar Apresentação">
            <Button
              onClick={() => openPresentationDetailsDialog(presentation)}
              icon={<EyeOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    },
    {
      title: 'Gerar QR',
      key: 'generateQRCode',
      align: 'center',
      render: (_, presentation) => (
        <span style={{ margin: 10 }}>
          <Tooltip title="Visualizar Apresentação">
            <GenerateQRButton
              loading={loading}
              data={presentation}
            ></GenerateQRButton>
          </Tooltip>
        </span>
      )
    },
    {
      title: 'Excluir',
      key: 'deletePresentation',
      align: 'center',
      render: (_, { idApresentacao, id2Apresentacao }) => (
        <span style={{ margin: 10 }}>
          <Tooltip title="Excluir Apresentacao">
            <Button
              onClick={async () => {
                await remove({
                  variables: { input: { idApresentacao, id2Apresentacao } }
                })
                // handleRefetch()
              }}
              icon={<DeleteOutlined />}
              shape="circle"
              type="dashed"
              disabled={true}
            />
          </Tooltip>
        </span>
      )
    }
  ]

  return (
    <AnimatedContent>
      <RoundedDiv shadow style={{ marginTop: 10 }}>
        <div style={{ overflowX: 'auto' }}>
          <Table
            loading={loading}
            columns={presentationsTable}
            rowKey={presentation =>
              `${presentation.idApresentacao}-${presentation.id2Apresentacao}`
            }
            footer={() => (tableFooter ? tableFooter : null)}
            expandedRowRender={(presentation, _, __, expand) =>
              expand ? (
                <PresentationsProjectsTable
                  presentation={presentation}
                  openProjectDetailsDialog={openProjectDetailsDialog}
                  openUpdateProjectDialog={openUpdateProjectDialog}
                />
              ) : null
            }
            dataSource={presentations}
          />
        </div>
      </RoundedDiv>
    </AnimatedContent>
  )
}

export default PresentationsTable
