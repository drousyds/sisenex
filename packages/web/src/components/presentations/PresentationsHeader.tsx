import React, { useState, useMemo } from 'react'
import { Grid, Cell } from 'styled-css-grid'
import { LeftOutlined, PlusOutlined, ReloadOutlined } from '@ant-design/icons';
import { Input, Tooltip, Button } from 'antd'
import { useHistory } from 'react-router'

// Components
import RoundedDiv from '../common/RoundedDiv'
import { AnimatedHeader } from '../animation'
import { PresentationsFiltersType } from '../../hooks'

import './PresentationsHeader.css'
import { PresentationStatusSelect } from '../form'
import { getPresentationStatusNumber } from '../../helpers'

// Component Interface
export interface Props {
  loading: boolean
  isPeformance?: boolean
  handleRefetch: () => void
  openCreatePresentationDialog?: () => void
  style?: React.CSSProperties
  idEvento: number
  filters: PresentationsFiltersType
  setFilters: (e: PresentationsFiltersType) => void
  goBackUrl: string
}

// Component
const PresentationsPageHeader: React.FC<Props> = ({
  handleRefetch,
  loading,
  openCreatePresentationDialog,
  style,
  idEvento,
  isPeformance,
  filters,
  setFilters,
  goBackUrl
}) => {
  const history = useHistory()
  const [text, setText] = useState('')
  const createButtonText = useMemo(() => {
    return isPeformance ? 'Criar Performance' : 'Criar Tertúlia'
  }, [isPeformance])
  return (
    <AnimatedHeader>
      <RoundedDiv
        shadow
        style={{ ...style, display: 'flex', flexDirection: 'row' }}
      >
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Tooltip title="Voltar">
            <Button
              shape="circle"
              icon={<LeftOutlined />}
              onClick={() => history.push(goBackUrl)}
              style={{ marginRight: 10 }}
            />
          </Tooltip>
        </div>
        <div
          style={{ display: 'flex', flexDirection: 'column', width: '100%' }}
        >
          <div style={{ display: 'flex', flexDirection: 'row' }}>
            <Input.Search
              style={{ marginRight: 10 }}
              placeholder="Pequisar..."
              value={text}
              onChange={e => {
                setText(e.target.value)
                if (!e.target.value) {
                  setFilters({ ...filters, text: '' })
                }
              }}
              onSearch={value => {
                setFilters({ ...filters, text: value })
              }}
            />

            {openCreatePresentationDialog && (
              <Tooltip title={createButtonText}>
                <Button
                  onClick={() => openCreatePresentationDialog()}
                  icon={<PlusOutlined />}
                  style={{ marginRight: 5 }}
                  disabled={loading}
                >
                  {createButtonText}
                </Button>
              </Tooltip>
            )}

            <Tooltip title="Atualizar a página">
              <Button
                onClick={() => {
                  handleRefetch()
                }}
                icon={<ReloadOutlined />}
                loading={loading}
                style={{ maxWidth: 248, float: 'right' }}
              >
                Atualizar
              </Button>
            </Tooltip>
          </div>
          <Grid
            columns="repeat(auto-fill, minmax(250px, 1fr))"
            coljustifyContent="start"
            style={{ marginTop: 10 }}
          >
            <Cell>
              <PresentationStatusSelect
                loading={loading}
                style={{ width: '100%' }}
                handleChange={(value: string | undefined) => {
                  const type = getPresentationStatusNumber(value)
                  setFilters({
                    ...filters,
                    disponibilidadeApresentacao: type
                  })
                }}
              />
            </Cell>
          </Grid>
        </div>
      </RoundedDiv>
    </AnimatedHeader>
  );
}

export default PresentationsPageHeader
