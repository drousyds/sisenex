import { Spin } from 'antd'
import React from 'react'

const FullSpinner: React.FC = () => {
  return (
    <div style={{ width: '100%', height: '100%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
      <Spin spinning={true} />
    </div>
  )
}

export default FullSpinner
