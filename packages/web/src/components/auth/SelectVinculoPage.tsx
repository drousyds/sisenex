import React from 'react'
import { Result, Button } from 'antd'
import { Redirect, useHistory } from 'react-router-dom'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'

// Actions
import {
  selectVinculoAction,
  clearAuthStageAction,
  loginUserAction
} from '../../redux/ducks/auth/actions'

// Typs
import { AppStateType } from '../../types'

// GraphQL
import { VerticalFade } from '../animation'
import RoundedDiv from '../common/RoundedDiv'
import { Pessoa } from '../../generated'
import { loginAuthUser } from '../../auth'

interface Props {
  vinculos: Pessoa[]
  selectVinculo: (vinculo: Pessoa) => void
  loginUser: (user: Pessoa) => void
  clearStage: () => void
}

const SelectVinculoPage: React.FC<Props> = ({
  vinculos,
  selectVinculo,
  clearStage,
  loginUser
}) => {
  const history = useHistory()

  const handleClick = async (
    e: React.MouseEvent<HTMLElement, MouseEvent>,
    v: Pessoa
  ) => {
    e.preventDefault()
    selectVinculo(v)
    clearStage()
    loginAuthUser(v, history, loginUser)
  }

  if (vinculos && vinculos.length > 1) {
    return (
      <VerticalFade direction="up" amount={20}>
        <RoundedDiv>
          <Result
            status="warning"
            title="Mais de um vínculo foi identificado."
            subTitle=" Selecione qual deseja utilizar"
          >
            {vinculos.map(v =>
              v.matriculaPessoa ? (
                <Button
                  key={v.matriculaPessoa}
                  size="large"
                  style={{ width: '100%', marginBottom: 15 }}
                  onClick={e => handleClick(e, v)}
                >
                  {v.matriculaPessoa}
                </Button>
              ) : null
            )}
          </Result>
        </RoundedDiv>
      </VerticalFade>
    )
  }
  return <Redirect to="/" />
}

const mapStateToProps = (state: AppStateType) => ({
  vinculos: state.auth.stage.vinculos
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  selectVinculo: (vinculo: Pessoa) => dispatch(selectVinculoAction(vinculo)),
  loginUser: (user: Pessoa) => dispatch(loginUserAction(user)),
  clearStage: () => dispatch(clearAuthStageAction())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectVinculoPage)
