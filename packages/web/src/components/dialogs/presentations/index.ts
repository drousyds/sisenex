import CreatePresentationDialog from './CreatePresentationDialog'
import UpdatePresentationDialog from './UpdatePresentationDialog'
import PresentationDetailsDialog from './PresentationDetailsDialog'

export {
  CreatePresentationDialog,
  UpdatePresentationDialog,
  PresentationDetailsDialog
}
