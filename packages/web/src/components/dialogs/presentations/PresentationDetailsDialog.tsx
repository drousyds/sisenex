import { Modal, Typography, Tag, List, Result } from 'antd'
import React from 'react'

import {
  Apresentacao,
  useGetPresentationReviewersQuery
} from '../../../generated'
import {
  getPresentationStatus,
  getPresentationType,
  renderLongUnixDate,
  getAvatarColor,
  openNotificationWithIcon,
  getStatusTagColor
} from '../../../helpers'
import UserInfo from '../../layout/UserInfo'

interface Props {
  open: boolean
  presentation: Apresentacao
  handleClose(): void
}

const { Text, Title } = Typography

const PresentationDetailsDialog = ({
  handleClose,
  open,
  presentation
}: Props) => {
  // Query Hook
  const { data } = useGetPresentationReviewersQuery({
    variables: {
      input: {
        idApresentacao: presentation.idApresentacao,
        id2Apresentacao: presentation.id2Apresentacao
      }
    },
    onError: () => {
      openNotificationWithIcon(
        'error',
        'Ocorreu um Erro ao buscar os avaliadores na sala.',
        5.5
      )
    },
    pollInterval: 15 * 1000,
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only'
  })

  const renderTitle = () => {
    const statusText = getPresentationStatus(presentation)

    const {
      salaApresentacao,
      horaApresentacao,
      areaTematica,
      codigoApresentacao
    } = presentation

    return (
      <div>
        {codigoApresentacao && (
          <div>
            <span>
              <Text strong>Apresentação: </Text>
              <Text>{codigoApresentacao}</Text>
            </span>
          </div>
        )}
        <div>
          <span>
            <Text strong>Sala: </Text>
            <Text>{salaApresentacao || 'Sem Local'}</Text>
          </span>
        </div>
        <div>
          <span>
            <Text strong>Hora Apresentação: </Text>
            <Text>
              {horaApresentacao
                ? renderLongUnixDate(horaApresentacao)
                : 'Sem Horário Informado'}
            </Text>
          </span>
        </div>
        <div>
          <span style={{ marginTop: 5 }}>
            <Text strong> Status: </Text>
            <span>
              <Tag
                color={getStatusTagColor(presentation)}
                key="presentationStatusTag"
              >
                {statusText}
              </Tag>
            </span>
          </span>
        </div>

        <div>
          <span>
            <Text strong>Modalidade: </Text>
            <Text>{getPresentationType(presentation)}</Text>
          </span>
        </div>
        <div>
          <span>
            <Text strong>Área Tematica: </Text>
            <Text>
              {areaTematica
                ? areaTematica.nomeAreaTematica
                : 'Sem Área Tematica'}
            </Text>
          </span>
        </div>
      </div>
    )
  }

  const title = (
    <Title level={4} style={{ margin: 0 }}>
      Avaliadores Na Sala
    </Title>
  )

  const renderReviewers = () => {
    if (
      data &&
      data.apresentacao &&
      data.apresentacao.avaliadores &&
      data.apresentacao.avaliadores.length > 0
    ) {
      return (
        <List
          header={title}
          itemLayout="horizontal"
          dataSource={data.apresentacao.avaliadores}
          renderItem={(r, i) => (
            <List.Item style={{ width: '100%' }}>
              <UserInfo
                user={r}
                avatarStyle={{ marginRight: 10, ...getAvatarColor(i) }}
              />
            </List.Item>
          )}
          style={{ margin: 0 }}
        />
      )
    }
    return (
      <div style={{ marginTop: 10 }}>
        <Result
          title="Sem Avaliadores"
          subTitle="Não há avaliadores cadastrados na Apresentação."
          status="info"
        />
      </div>
    )
  }

  return (
    <Modal
      visible={open}
      onCancel={handleClose}
      centered
      destroyOnClose
      footer={null}
      closable={false}
    >
      {renderTitle()}
      {renderReviewers()}
    </Modal>
  )
}

export default PresentationDetailsDialog
