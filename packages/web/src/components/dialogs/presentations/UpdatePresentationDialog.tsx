import { Modal, DatePicker, Select, Input, Radio, Form } from 'antd'
import moment from 'moment'
import React from 'react'

import {
  EMPTY_FIELD_ERROR_MESSAGE,
  FETCH_FIELDS_ERROR_MESSAGE,
  UPDATE_PRESENTATION_ERROR_MESSAGE,
  UPDATE_PRESENTATION_SUCESS_MESSAGE
} from '../../../constants/messages'
import {
  Apresentacao,
  AreaTematica,
  useAreasTematicasQuery,
  useAtualizarApresentacaoMutation,
  Evento,
  ApresentacoesInput
} from '../../../generated'
import { openNotificationWithIcon } from '../../../helpers'
import { removePresentation } from '../../../services/cache.services'
import { PresentationStatus } from '../../../types'

interface Props {
  open: boolean
  presentation: Apresentacao
  event: Evento
  handleClose(): void
  isPeformance: boolean
  refetch(): void
  queryVariables: ApresentacoesInput
}

const UpdatePresentationDialog: React.FC<Props> = ({
  open,
  presentation,
  handleClose,
  event,
  isPeformance,
  queryVariables
  // refetch,
}) => {
  const [form] = Form.useForm()

  // Query Hook
  const { data: qData, loading: qLoading } = useAreasTematicasQuery({
    notifyOnNetworkStatusChange: true,
    skip: !isPeformance,
    onError: () => {
      openNotificationWithIcon('error', FETCH_FIELDS_ERROR_MESSAGE, 5.5)
      handleClose()
    }
  })

  // Mutation Hook
  const [
    updatePresentationMutation,
    { loading: mLoading }
  ] = useAtualizarApresentacaoMutation({
    onCompleted: () => {
      openNotificationWithIcon(
        'success',
        UPDATE_PRESENTATION_SUCESS_MESSAGE,
        5.5
      )
      handleClose()
      // refetch()
    },
    onError: () => {
      openNotificationWithIcon('error', UPDATE_PRESENTATION_ERROR_MESSAGE, 5.5)
      handleClose()
    },
    update: (proxy, { data }) => {
      if (data && data.atualizarApresentacao && data.atualizarApresentacao) {
        const a = data.atualizarApresentacao
        if (
          !isPeformance &&
          a.areaTematica &&
          a.areaTematica.idAreaTematica &&
          presentation.areaTematica &&
          presentation.areaTematica.idAreaTematica !==
            a.areaTematica.idAreaTematica
        ) {
          // console.log('TROCOU DE AREA TEMATICA')
          removePresentation(proxy, a, queryVariables)
        }
      }
    }
  })

  let areasTematicas: AreaTematica[] = []

  if (qData && qData.areasTematicas && isPeformance) {
    // eslint-disable-next-line
    areasTematicas = qData.areasTematicas || []
  } else {
    areasTematicas = event.areasTematicas
  }

  const submitForm = () => {
    form
      .validateFields()
      .then(values => {
        updatePresentationMutation({
          variables: {
            input: {
              idApresentacao: presentation.idApresentacao,
              id2Apresentacao: presentation.id2Apresentacao,
              idAreaTematica: values.areaTematica,
              disponibilidadeApresentacao: values.estadoApresentacao,
              salaApresentacao: `${values.salaApresentacao}`,
              horaApresentacao: values.horaApresentacao.format(
                'YYYY/MM/DD HH:mm'
              )
            }
          }
        })
      })
      .catch(() => {})
  }

  const renderForm = () => {
    return (
      <Form
        layout="vertical"
        form={form}
        name="UpdatePresentationForm"
        initialValues={{
          salaApresentacao: presentation.salaApresentacao,
          horaApresentacao: presentation.horaApresentacao
            ? moment.unix(Number(presentation.horaApresentacao) / 1000).utc()
            : null,
          areaTematica: presentation.areaTematica
            ? presentation.areaTematica.idAreaTematica
            : undefined,
          estadoApresentacao: presentation.disponibilidadeApresentacao
        }}
      >
        <Form.Item
          label="Local"
          name="salaApresentacao"
          rules={[
            {
              required: true,
              message: EMPTY_FIELD_ERROR_MESSAGE
            }
          ]}
        >
          <Input placeholder="Sala 10" style={{ width: '100%' }} />
        </Form.Item>
        <Form.Item
          label="Hora"
          name="horaApresentacao"
          rules={[
            {
              type: 'object',
              required: true,
              message: EMPTY_FIELD_ERROR_MESSAGE
            }
          ]}
        >
          <DatePicker
            showTime
            // format="YYYY-MM-DD HH:mm:ss"
            format="LLLL"
            style={{ width: '100%' }}
          />
        </Form.Item>
        <Form.Item
          label="Área Temática"
          name="areaTematica"
          rules={[{ required: true, message: EMPTY_FIELD_ERROR_MESSAGE }]}
        >
          <Select placeholder="Selecione a área temática">
            {areasTematicas.map(({ idAreaTematica, nomeAreaTematica }) => (
              <Select.Option key={idAreaTematica} value={idAreaTematica}>
                {nomeAreaTematica}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label="Estado da Apresentação"
          name="estadoApresentacao"
          rules={[{ required: true, message: EMPTY_FIELD_ERROR_MESSAGE }]}
        >
          <Radio.Group>
            <Radio.Button value={0}>{PresentationStatus.pending}</Radio.Button>
            <Radio.Button value={1}>{PresentationStatus.onGoing}</Radio.Button>
            <Radio.Button value={2}>
              {PresentationStatus.completed}
            </Radio.Button>
          </Radio.Group>
        </Form.Item>
      </Form>
    )
  }

  return (
    <Modal
      visible={open}
      title="Editar Apresentação"
      onCancel={handleClose}
      onOk={() => submitForm()}
      centered
      okText="Salvar"
      confirmLoading={mLoading || qLoading}
      destroyOnClose
    >
      {renderForm()}
    </Modal>
  )
}

export default UpdatePresentationDialog
