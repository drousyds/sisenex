import { DatePicker, Modal, Input, Form, Select } from 'antd'
import moment from 'moment-timezone'
import React, { useMemo, useRef, useState } from 'react'
import {
  EMPTY_FIELD_ERROR_MESSAGE, FETCH_PROJECTS_ERROR_MESSAGE
} from '../../../constants/messages'
import { CriarApresentacaoInput, Maybe, Projeto, useProjectsPageContainerQuery } from '../../../generated'
import AreaTematicaSelect from '../../form/AreaTematicaSelect'
import CategorySelect from '../../form/CategorySelect'
import { ProjectsFiltersType, useFilteredProjects, useFilters } from '../../../hooks'
import { openNotificationWithIcon } from '../../../helpers'
import AddProjectsPageHeader from '../../../pages/gerenciapage/projects/AddProjectsPageHeader'
import AddProjectsTable from '../../projects/AddProjectsTable'


export interface FormData {
  id2Apresentacao: CriarApresentacaoInput['id2Apresentacao']
  salaApresentacao: CriarApresentacaoInput['salaApresentacao']
  horaApresentacao: CriarApresentacaoInput['horaApresentacao']
  idCategoria: CriarApresentacaoInput['idCategoria']
  linkApresentacao?: CriarApresentacaoInput['linkApresentacao']
  idAreaTematica?: CriarApresentacaoInput['idAreaTematica']
  projects: Maybe<Array<Projeto>>
  idEvento: number;
}


interface Props {
  visible: boolean
  handleClose(): void
  loading?: boolean
  handleSubmit(formData: FormData): Promise<void>
}


const CreatePresentationDialog: React.FC<Props> = (props) => {


  const projectsRef = useRef<HTMLDivElement>(null);
  const [idCategoria, setIdCategoria] = useState<number | undefined>(undefined)
  const [form] = Form.useForm<FormData>()
  const [projectSelected, setProjectSelected] = useState<Array<any>>([])

  const submitForm = () => {
    form
      .validateFields()
      .then(async values => {
        console.log('Valores do formulário:', values);
        await props.handleSubmit(
          {
            ...values,
            projects: projectSelected,
            horaApresentacao: values.horaApresentacao.format('YYYY/MM/DD HH:mm'),
          } as FormData);
      })
      .catch(() => { });
  }

  const [filters, setFilters] = useFilters<ProjectsFiltersType>({
    text: '',
    areasTematicas: [],
    categoriaProjeto: undefined,
    allocation: ''
  })

  // Query Hook
  const { data, refetch, loading } = useProjectsPageContainerQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon('error', FETCH_PROJECTS_ERROR_MESSAGE, 5.5)
    }
  })

  const { projects } = useMemo(() => {
    const { projetos, areasTematicas } = data || {}
    return {
      projects: projetos || [],
      areasTematicas: areasTematicas || []
    }
  }, [data])

  const filteredProjects = useFilteredProjects({ filters, projects })

  return (
    <Modal
      visible={props.visible}
      onCancel={props.handleClose}
      onOk={() => submitForm()}
      confirmLoading={props.loading ? props.loading : false}
      okText="Salvar"
      title={'Criar Apresentação'}
      destroyOnClose
      centered
      width={'35vw'}
    >
      <div ref={projectsRef} style={{ maxHeight: '80vh', overflowY: 'auto', padding: '0px 20px' }}>
        <Form
          layout={'vertical'}
          form={form}
          name="CreatePresentationForm"
          initialValues={{
            horaApresentacao: moment.tz('America/Recife')
          }}
        >
          <Form.Item
            label="Campus"
            name="idEvento"
            rules={[{ required: true, message: 'Selecione um campus' }]}
          >
            <Select placeholder="Selecione o campus">
              <Select.Option value={1}>Campus I</Select.Option>
              <Select.Option value={2}>Campus II e III</Select.Option>
              <Select.Option value={3}>Campus IV</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="Sala"
            name="salaApresentacao"
            rules={[
              {
                required: true,
                message: EMPTY_FIELD_ERROR_MESSAGE
              }
            ]}
          >
            <Input placeholder="Sala 10" style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item
            label="Dia e Hora"
            name="horaApresentacao"
            rules={[
              {
                type: 'object',
                required: true,
                message: EMPTY_FIELD_ERROR_MESSAGE
              }
            ]}
          >
            <DatePicker showTime format="LLLL" style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item
            label="Área Temática:"
            name="idAreaTematica"
          >
            <AreaTematicaSelect />
          </Form.Item>

          <Form.Item
            label="Categoria"
            name="idCategoria"
            rules={[{ required: true, message: EMPTY_FIELD_ERROR_MESSAGE }]}

          >
            <CategorySelect hasAllItems={false} isModalidade={false} onChange={(idCategoria) => setIdCategoria(idCategoria)} />
          </Form.Item>

          {idCategoria !== undefined && idCategoria === 1 && (
            <Form.Item
              label="Link Apresentação"
              name="linkApresentacao"
            >
              <Input placeholder="Link apresentação" style={{ width: '100%' }} />
            </Form.Item>
          )}
        </Form>
        <AddProjectsPageHeader
          filters={filters}
          setFilters={setFilters}
        />
        {
          <AddProjectsTable
            loading={loading}
            projects={filteredProjects}
            projectSelected={projectSelected}
            setProjectSelected={setProjectSelected}
            filtersText={filters.text}
            tableFooter={
              <div>
                Projetos: {`${filteredProjects.length} / ${projects.length}`}
              </div>
            }
            projectsRef={projectsRef}
          />
        }
      </div>
    </Modal>
  )
}

export default CreatePresentationDialog
