import { Modal } from 'antd'
import React from 'react'

interface Props {
  open: boolean
  handleClose: () => void
}

const CreateCategoryDialog: React.FC<Props> = ({ open, handleClose }) => {
  return (
    <Modal title="Criar Categoria" onCancel={handleClose} visible={open}>
      Form Aqui
    </Modal>
  )
}

export default CreateCategoryDialog
