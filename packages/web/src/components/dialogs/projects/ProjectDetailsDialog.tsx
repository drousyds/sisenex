import {
  Modal,
  Typography,
  Row,
  Col,
  Tag,
  Progress,
  Divider,
  List,
  Avatar,
  Result
} from 'antd'
import React from 'react'

import { Projeto, useGetProjectDetailsQuery } from '../../../generated'
import {
  openNotificationWithIcon,
  getAvatarColor,
  getModeText
} from '../../../helpers'
import { NotificationType } from '../../../types'

const { Title } = Typography
const { Text } = Typography

interface Props {
  open: boolean
  project: Projeto
  handleClose(): void
}

const ProjectDetailsDialog = ({ open, handleClose, project }: Props) => {
  const { data } = useGetProjectDetailsQuery({
    notifyOnNetworkStatusChange: true,
    variables: {
      idProjeto: project.idProjeto
    },
    pollInterval: 5000,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        'Ocorreu um erro ao buscar as informações do projeto.',
        5.5
      )
    }
  })

  const renderTitle = () => {
    return (
      <div>
        <div>
          <Title level={4}>{project.tituloProjeto}</Title>
        </div>
        <div>
          {project.apresentacao ? (
            <div>
              <React.Fragment>
                {project.apresentacao.codigoApresentacao && (
                  <React.Fragment>
                    <Text strong>Apresentação: </Text>
                    <Text>{project.apresentacao.codigoApresentacao}</Text>
                  </React.Fragment>
                )}
                <Text strong style={{ marginLeft: 10 }}>
                  Sala:
                </Text>
                <Text>
                  {project.apresentacao.salaApresentacao
                    ? ` ${project.apresentacao.salaApresentacao}`
                    : 'Sem Sala'}
                </Text>
              </React.Fragment>
              <React.Fragment>
                <Text strong style={{ marginLeft: 10 }}>
                  Estado:
                </Text>
                <Tag
                  color={project.media ? 'blue' : 'volcano'}
                  style={{
                    marginLeft: 5
                  }}
                >
                  {project.media ? 'APRESENTADO' : 'NÃO APRESENTADO'}
                </Tag>
              </React.Fragment>
            </div>
          ) : (
            <div>
              <Text strong>
                O Projeto não está vinculado a nenhuma Apresentação.
              </Text>
            </div>
          )}
        </div>
        <Divider />
      </div>
    )
  }

  const title = (
    <Title level={4} style={{ margin: 0 }}>
      Avaliações
    </Title>
  )

  const renderContent = () => {
    return (
      <div style={{ marginTop: 0 }}>
        <Row gutter={16} style={{ flexWrap: 'wrap' }}>
          <Col span={14}>
            <div style={{ marginBottom: 5 }}>
              <Text strong style={{ fontSize: 20 }}>
                Informações Gerais
              </Text>
            </div>
            <ul style={{ listStyleType: 'none' }}>
              <div>
                <Text strong>Modalidade: </Text>
                <Text>{getModeText(project.modalidadeProjeto)}</Text>
              </div>
              <div>
                <Text strong>Área Temática: </Text>
                <Text>{project.areaTematica.nomeAreaTematica}</Text>
              </div>
              {project.unidade && (
                <div>
                  <Text strong>Nome Unidade: </Text>
                  <Text>
                    {project.unidade.nomeUnidade
                      ? project.unidade.nomeUnidade
                      : 'Sem Unidade'}
                  </Text>
                </div>
              )}
            </ul>
          </Col>
          <Col span={10}>
            {data && data.projeto && data.projeto.media && (
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center'
                }}
              >
                <div style={{ marginBottom: 5 }}>
                  <Text strong style={{ fontSize: 20 }}>
                    Média Geral:
                  </Text>
                </div>
                <Progress
                  type="circle"
                  percent={
                    data && data.projeto && data.projeto.media
                      ? data.projeto.media * 10
                      : undefined
                  }
                  width={80}
                  format={media =>
                    media ? `${(media / 10.0).toFixed(3)}` : 'S/N'
                  }
                />
              </div>
            )}
          </Col>
        </Row>
        <div>
          {data &&
          data.projeto &&
          data.projeto.mediasIndividuais &&
          data.projeto.mediasIndividuais.length > 0 ? (
            <List
              header={title}
              itemLayout="horizontal"
              dataSource={data.projeto.mediasIndividuais}
              renderItem={(m, i) => {
                return (
                  <List.Item
                    key={`${project.idProjeto}-review-${i * 1}`}
                    style={{
                      display: 'flex',
                      flexDirection: 'row'
                    }}
                  >
                    <div style={{ flex: 1 }}>
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          alignItems: 'center'
                        }}
                      >
                        <Avatar
                          size="large"
                          shape="square"
                          style={{
                            color: '#f56a00',
                            ...getAvatarColor(i)
                          }}
                        >
                          {i + 1 || null}
                        </Avatar>
                        <div style={{ marginLeft: 10 }}>
                          {`Avaliador ${i + 1}`}
                        </div>
                      </div>
                    </div>
                    <div style={{ fontWeight: 'bold' }}>
                      {m.mediaProjeto ? m.mediaProjeto.toFixed(3) : null}
                    </div>
                  </List.Item>
                )
              }}
              style={{ margin: 0 }}
            />
          ) : (
            <Result subTitle="Sem Avaliações." />
          )}
        </div>
      </div>
    )
  }

  return (
    <Modal
      visible={open}
      onCancel={handleClose}
      centered
      destroyOnClose
      footer={null}
      closable={false}
      width={600}
    >
      {renderTitle()}
      {renderContent()}
    </Modal>
  )
}

export default ProjectDetailsDialog
