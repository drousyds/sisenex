import React from 'react'
import { Typography, Tag, Divider } from 'antd'
import { Projeto } from '../../../generated'

interface Props {
  project: Projeto
  style?: React.CSSProperties
}

const { Title, Text } = Typography

const ProjectTitle: React.FC<Props> = ({ project, style }) => {
  return (
    <div style={style}>
      <div>
        <Title level={4}>{project.tituloProjeto}</Title>
      </div>
      <div>
        {project.apresentacao ? (
          <div>
            <React.Fragment>
              {project.apresentacao.codigoApresentacao && (
                <React.Fragment>
                  <Text strong>Apresentação: </Text>
                  <Text>{project.apresentacao.codigoApresentacao}</Text>
                </React.Fragment>
              )}
              <Text strong style={{ marginLeft: 10 }}>
                Sala:
              </Text>
              <Text>
                {project.apresentacao.salaApresentacao
                  ? ` ${project.apresentacao.salaApresentacao}`
                  : 'Sem Sala'}
              </Text>
            </React.Fragment>
            <React.Fragment>
              <Text strong style={{ marginLeft: 10 }}>
                Estado:
              </Text>
              <Tag
                color={project.media ? 'blue' : 'volcano'}
                style={{
                  marginLeft: 5
                }}
              >
                {project.media ? 'APRESENTADO' : 'NÃO APRESENTADO'}
              </Tag>
            </React.Fragment>
          </div>
        ) : (
          <div>
            <Text strong>
              O Projeto não está vinculado a nenhuma Apresentação.
            </Text>
          </div>
        )}
      </div>
      <Divider />
    </div>
  )
}

export default ProjectTitle
