import { Modal, Select, Tag, Typography, Divider, Form } from 'antd'
import React, { useMemo } from 'react'
import { MutationResult } from 'react-apollo'

import {
  EMPTY_FIELD_ERROR_MESSAGE,
  FETCH_MESSAGES,
  MUTATIONS_MESSAGES
} from '../../../constants/messages'
import {
  Projeto,
  GetPresentationProjectsDocument,
  useGetPresentationsOptionsQuery,
  AtualizarProjetoInput,
  useUpdateProjectMutation,
  UpdateProjectMutation
} from '../../../generated'
import {
  openNotificationWithIcon,
  getStatusTagColor,
  getModeText
} from '../../../helpers'
import { AnimatedAlert } from '../../alerts'

const { Text } = Typography

interface Props {
  open: boolean
  project: Projeto
  handleClose(): void
  general?: boolean
}

const UpdateProjectDialog: React.FC<Props> = ({
  open,
  project,
  handleClose
}) => {
  // form Hook
  const [form] = Form.useForm()

  // Query Hook
  const { data: qData, loading: qLoading } = useGetPresentationsOptionsQuery({
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon('error', FETCH_MESSAGES.ERROR.PRESENTATIONS, 5.5)
      handleClose()
    }
  })

  const presentations = useMemo(() => {
    const { apresentacoes } = qData || {}
    return apresentacoes || []
  }, [qData])

  // Mutation Hook
  const [updateProjectMutation, { loading, error }] = useUpdateProjectMutation({
    notifyOnNetworkStatusChange: true,
    onCompleted: () => {
      openNotificationWithIcon(
        'success',
        MUTATIONS_MESSAGES.PROJECT.UPDATE.SUCCESS,
        5.5
      )
      handleClose()
    },
    onError: () => {
      openNotificationWithIcon(
        'error',
        MUTATIONS_MESSAGES.PROJECT.UPDATE.ERROR,
        5.5
      )
      // handleClose()
    },
    refetchQueries: ({ data }: MutationResult<UpdateProjectMutation>) => {
      const refetchs: { query: any; variables: any }[] = []

      if (project.apresentacao) {
        refetchs.push({
          query: GetPresentationProjectsDocument,
          variables: {
            input: {
              idApresentacao: project.apresentacao.idApresentacao,
              id2Apresentacao: project.apresentacao.id2Apresentacao
            }
          }
        })
      }

      if (data && data.atualizarProjeto && data.atualizarProjeto.apresentacao) {
        refetchs.push({
          query: GetPresentationProjectsDocument,
          variables: {
            input: {
              idApresentacao: data.atualizarProjeto.apresentacao.idApresentacao,
              id2Apresentacao:
                data.atualizarProjeto.apresentacao.id2Apresentacao
            }
          }
        })
      }
      return refetchs
    }
  })

  const submitForm = () => {
    form
      .validateFields()
      .then(values => {
        let input: AtualizarProjetoInput = {
          idProjeto: project.idProjeto,
          modalidadeProjeto: values.mode,
          statusProjeto: values.status
        }
        if (values.newPresentation) {
          const [
            idApresentacao,
            id2Apresentacao
          ] = values.newPresentation.split('-')
          const p = presentations.find(
            p =>
              `${p.idApresentacao}` === idApresentacao &&
              `${p.id2Apresentacao}` === id2Apresentacao
          )
          input = {
            ...input,
            idEvento: p && p.evento ? p.evento.idEvento : undefined,
            idApresentacao: parseInt(idApresentacao, 10),
            id2Apresentacao
          }
        } else {
          input = {
            ...input
          }
        }
        updateProjectMutation({
          variables: {
            input
          }
        })
      })
      .catch(() => {})
  }
  const renderForm = () => {
    console.log(project)

    return (
      <Form
        layout={'vertical'}
        form={form}
        name="updateProjectForm"
        initialValues={{
          newPresentation: project.apresentacao
            ? `${project.apresentacao.idApresentacao}-${project.apresentacao.id2Apresentacao}`
            : undefined,
          mode: project.modalidadeProjeto || undefined,
          status: project.statusProjeto || undefined
        }}
      >
        <Form.Item
          label="Nova Apresentação"
          name="newPresentation"
          rules={[{ message: EMPTY_FIELD_ERROR_MESSAGE }]}
        >
          <Select
            placeholder={qLoading ? 'Carregando...' : 'Nova Apresentação: '}
            showSearch
            allowClear
            // filterOption
            loading={qLoading}
            disabled={qLoading}
            optionFilterProp="children"
          >
            {presentations.map(
              ({
                idApresentacao,
                id2Apresentacao,
                projetos,
                modalidadeApresentacao,
                codigoApresentacao,
                evento
              }) => {
                let text = `Código: ${codigoApresentacao ||
                  `${idApresentacao}-${id2Apresentacao}`} - Quantidade de Projetos: ${
                  projetos ? projetos.length : 0
                } - ${
                  modalidadeApresentacao
                    ? getModeText(modalidadeApresentacao)
                    : ''
                }`
                if (evento) text = `Evento: ${evento.nomeEvento} - ` + text
                return (
                  <Select.Option
                    key={codigoApresentacao}
                    value={`${idApresentacao}-${id2Apresentacao}`}
                  >
                    {text}
                  </Select.Option>
                )
              }
            )}
          </Select>
        </Form.Item>
        <Form.Item
          label="Modalidade Projeto"
          name="mode"
          rules={[{ required: true, message: EMPTY_FIELD_ERROR_MESSAGE }]}
        >
          <Select
            placeholder="Modalidade projeto"
            showSearch
            filterOption
            disabled={qLoading}
          >
            <Select.Option key="tertulia-mode" value="T">
              Tertúlia
            </Select.Option>
            <Select.Option key="performance-mode" value="P">
              Performance
            </Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          label="Status do Projeto"
          name="status"
        >
          <Select
            placeholder={project.statusProjeto}
          >
            <Select.Option key="avaliado-status" value="AVALIADO">
              AVALIADO
            </Select.Option>
            <Select.Option key="naoavaliado-status" value="NAO AVALIADO">
              NÃO AVALIADO
            </Select.Option>
            <Select.Option key="ausente-status" value="AUSENTE">
              AUSENTE
            </Select.Option>
          </Select>
        </Form.Item>
      </Form>
    )
  }

  const renderProjectInfo = () => {
    const {
      tituloProjeto,
      areaTematica,
      modalidadeProjeto,
      apresentacao
    } = project
    const marginTop = 5
    return (
      <React.Fragment>
        <div>
          <div>
            <span>
              <Text strong> Projeto: </Text>
              <Text>{tituloProjeto}</Text>
            </span>
          </div>
          <div style={{ marginTop }}>
            <span>
              <Text strong>Apresentação: </Text>
              <Text>
                {apresentacao
                  ? apresentacao.codigoApresentacao
                  : 'Sem Apresentação.'}
              </Text>
            </span>
          </div>
          <div style={{ marginTop }}>
            <span>
              <Text strong>Área Temárica: </Text>
              <Text>{areaTematica.nomeAreaTematica}</Text>
            </span>
          </div>
          <div style={{ marginTop }}>
            <span>
              <Text strong>Modalidade: </Text>
              <Text>{getModeText(modalidadeProjeto)}</Text>
            </span>
          </div>
          <div style={{ marginTop }}>
            <span>
              <Text strong> Status: </Text>
              <span>
                <Tag color={getStatusTagColor(project)}>
                  {project.statusProjeto}
                </Tag>
              </span>
            </span>
          </div>
        </div>
        <Divider />
      </React.Fragment>
    )
  }

  return (
    <Modal
      width={800}
      title="Alocar Projeto à Apresentação"
      okText="Salvar"
      centered
      destroyOnClose
      visible={open}
      onCancel={handleClose}
      confirmLoading={loading}
      onOk={() => {
        submitForm()
      }}
    >
      {renderProjectInfo()}
      {renderForm()}
      {error && (
        <AnimatedAlert
          style={{ marginTop: 10 }}
          title="Ocorreu um erro"
          description={error.message.split('GraphQL error:')[1]}
          type="error"
          showIcon
          show
        />
      )}
    </Modal>
  )
}

export default UpdateProjectDialog
