import { DatePicker, Modal, Input, Form, Select } from 'antd'
import moment from 'moment-timezone'
import React, { useEffect, useMemo, useRef, useState } from 'react'
import {
  EMPTY_FIELD_ERROR_MESSAGE, FETCH_PROJECTS_ERROR_MESSAGE
} from '../../../constants/messages'
import { CriarApresentacaoInput, Scalars, useProjectsPageContainerQuery } from '../../../generated'
import AreaTematicaSelect from '../../form/AreaTematicaSelect'
import CategorySelect from '../../form/CategorySelect'
import { ProjectsFiltersType, useFilteredProjects, useFilters } from '../../../hooks'
import { openNotificationWithIcon } from '../../../helpers'
import AddProjectsPageHeader from '../../../pages/gerenciapage/projects/AddProjectsPageHeader'
import AddProjectsTable from '../../projects/AddProjectsTable'
import axios from 'axios'
import FormItem from 'antd/lib/form/FormItem'

export interface FormData {
  tituloProjeto:  Scalars['String']
  idCategoria: Scalars['Int']
  codigoProjeto:  Scalars['String']
  idAreaTematica?:  Scalars['Int']
  idUnidade:  Scalars['String']
  dataInicioProjeto: Scalars['String']
  anoProjeto: Scalars['String']
  descricaoProjeto: Scalars['String']
  
}

interface Props {
  visible: boolean
  handleClose(): void
  loading?: boolean
  handleSubmit(formData: FormData): Promise<void>
}

const CreateProjectDialog: React.FC<Props> = (props) => {

  const projectsRef = useRef<HTMLDivElement>(null);
  const [idCategoria, setIdCategoria] = useState<number | undefined>(undefined)
  const [form] = Form.useForm<FormData>()
  const [unidades,setUnidades] = useState<{id:number, nome: string}[]>([])
  const [f, setF] = useState({});
  const [charCount, setCharCount] = useState(0);
  const maxCharLimit = 32;

  const handleInputChange = (e) => {
    setCharCount(e.target.value.length);
  };
  useEffect(() => {
    const fetchUnidades = async () => {
      const query = `
        query {
          unidades {
            siglaUnidade,
            idUnidade
          }
        }
      `;
    
      try {
        const response = await axios.post(process.env.REACT_APP_BASE_URL + '/graphql', {
          query: query,
        });
    
        const { data } = response.data;
        const dataUnidade = data.unidades.map(u => {
            return({id: u.idUnidade, nome: u.siglaUnidade})
        })
        setUnidades(dataUnidade)
        return data.unidades;
      } catch (error) {
        console.error('Erro ao buscar unidades:', error);
        throw error;
      }
    };

    // Uso da função fetchUnidades
    fetchUnidades().then(unidades => {
      // // Faça algo com as unidades
    }).catch(error => {
      // Trate erros aqui
    });

  },[])

  const submitForm = () => {
    form
      .validateFields()
      .then(async values => {
        await props.handleSubmit(
          {
            ...values
          } as FormData)
      })
      .catch(() => { })
  }
  const [filters, setFilters] = useFilters<ProjectsFiltersType>({
    text: '',
    areasTematicas: [],
    categoriaProjeto: undefined,
    allocation: ''
  })

  // Query Hook
  const { data, refetch, loading } = useProjectsPageContainerQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon('error', FETCH_PROJECTS_ERROR_MESSAGE, 5.5)
    }
  })

  const { projects } = useMemo(() => {
    const { projetos, areasTematicas } = data || {}
    return {
      projects: projetos || [],
      areasTematicas: areasTematicas || []
    }
  }, [data])

  const filteredProjects = useFilteredProjects({ filters, projects })

  return (
    <Modal
      visible={props.visible}
      onCancel={props.handleClose}
      onOk={() => submitForm()}
      confirmLoading={props.loading ? props.loading : false}
      okText="Salvar"
      title={'Adicionar Projeto'}
      destroyOnClose
      centered
      width={'35vw'}
     
    >
      <div ref={projectsRef} style={{ maxHeight: '80vh', overflowY: 'auto',padding: '0px 20px' }}>
        <Form
          layout={'vertical'}
          form={form}
          name="CreatePresentationForm"
          initialValues={{
            horaApresentacao: moment.tz('America/Recife')
          }}
        >
          <Form.Item
            label="Nome do Projeto"
            name="tituloProjeto"
            rules={[
              {
                required: true,
                message: EMPTY_FIELD_ERROR_MESSAGE
              }
            ]}
          >
            <Input placeholder="Digite o nome projeto" style={{ width: '100%' }} />
          </Form.Item>
            
          
          <Form.Item 
          label="Unidades"
          name="idUnidade"
          rules={[
            {
              required: true,
              message: EMPTY_FIELD_ERROR_MESSAGE
            }
          ]}
        >
         
          <Select
            showSearch
            defaultValue={undefined}
            placeholder="Selecione as unidades"
            onChange={value => {
              setF({
                ...f,
                idUnidade: value || []
              });
            }}
            allowClear
            disabled={loading}
            style={{ width: '100%' }}
          >
            {unidades.map(unidade => (
              <Select.Option
                key={unidade.id}
                value={JSON.stringify({ id: unidade.id, nome: unidade.nome })}
                >
                        
                {unidade.nome}
              </Select.Option>
            ))}
          </Select>
    </Form.Item>
         

          <Form.Item
            label="Modalidade do Projeto"
            name="idCategoria"
            rules={[{ required: true, message: EMPTY_FIELD_ERROR_MESSAGE }]}

          >
            <CategorySelect hasAllItems={false} isModalidade={true} onChange={(idCategoria) => setIdCategoria(idCategoria)} />
          </Form.Item>

          {idCategoria !== undefined && idCategoria === 1 && (
            <Form.Item
              label="Link Apresentação"
              name="linkApresentacao"
            >
              <Input placeholder="Link apresentação" style={{ width: '100%' }} />
            </Form.Item>
          )}

          <Form.Item
            label="Código do Projeto"
            name="codigoProjeto"
            rules={[
              {
                required: true,
                message: EMPTY_FIELD_ERROR_MESSAGE
              }
            ]}
          >
            <Input placeholder="Digite o código do projeto" style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item
            label="Área Temática:"
            name="idAreaTematica"
            rules={[
              {
                required: true,
                message: EMPTY_FIELD_ERROR_MESSAGE
              }
            ]}
          >
            <AreaTematicaSelect />
          </Form.Item>

              <FormItem
              label="Data Inicio e Data Fim:"
              name="dataInicioProjeto"
              rules={[
                {
                  required: true,
                  message: EMPTY_FIELD_ERROR_MESSAGE
                }
              ]}
              >
              <DatePicker.RangePicker
                placeholder={['Data de início', 'Data de fim']}
                format="DD/M/YYYY"
                style={{ width: '100%' }}
              />
              </FormItem>

              <Form.Item
                  label="Ano do Projeto"
                  name="anoProjeto"
                  rules={[
                    {
                      type: 'object',
                      required: true,
                      message: EMPTY_FIELD_ERROR_MESSAGE
                    }
                  ]}
          >
            <DatePicker.YearPicker showTime format="YYYY" style={{ width: '100%' }} />
          </Form.Item>

          <Form.Item
  label="Descrição do Projeto"
  name="descricaoProjeto"
  rules={[
    {
      required: true,
      message: EMPTY_FIELD_ERROR_MESSAGE
    }
  ]}
>
  <Input.TextArea
    placeholder="Digite a descrição do projeto"
     rows={3}
    style={{  width: '100%' }}
    maxLength={maxCharLimit}
    onChange={handleInputChange}
   
  />
</Form.Item>
    <div style={{ textAlign: 'right', marginTop: "-20px" }}>
        {charCount}/{maxCharLimit}
      </div>

        </Form>
      </div>
    </Modal>
  )
}

export default CreateProjectDialog
 /* LOgica: verificar como o onchange esta verificando o value, tem q ver como onsubmit o data 
 id de unidade do onsubmit*/