import { Modal } from 'antd'
import React from 'react'

import { Report } from '../../../generated'

interface Props {
  isOpen: boolean
  handleClose: () => void
  report: Report
}

interface MessageType {
  content: string
  createdAt: string
  owner: boolean
}

// put this in a state
const FAKE_MESSAGES: MessageType[] = [
  {
    content: 'Isso daqui é um teste.',
    createdAt: '10:36',
    owner: false
  },
  {
    content: 'Isso daqui é um teste.',
    createdAt: '10:36',
    owner: false
  },
  {
    content: 'Isso daqui é um teste.',
    createdAt: '10:36',
    owner: true
  },
  {
    content:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
    createdAt: '10:36',
    owner: true
  },
  {
    content: 'Isso daqui é um teste.',
    createdAt: '10:36',
    owner: false
  },
  {
    content: 'Isso daqui é um teste.',
    createdAt: '10:36',
    owner: false
  },
  {
    content:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
    createdAt: '10:36',
    owner: false
  }
]

const ReportChatDialog: React.FC<Props> = ({ handleClose, isOpen, report }) => {
  return (
    <Modal
      visible={isOpen}
      onCancel={() => handleClose()}
      onOk={() => handleClose()}
    >
      {JSON.stringify(report, null, 2)}
      <div style={{ marginTop: '2rem' }}>
        {JSON.stringify(FAKE_MESSAGES, null, 2)}
      </div>
    </Modal>
  )
}

export default ReportChatDialog
