import React, { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { closePresentationDetailsDialogAction } from '../../../redux/ducks/dialogs/presentations/actions/PresentationDetailsDialogActions'
import { AppStateType } from '../../../types'
import { PresentationDetailsDialog } from '../presentations'

const PresentationsDialogsWrapper: React.FC = () => {
  const dispatch = useDispatch()
  const presentationsDialog = useSelector((state: AppStateType) => {
    return state.dialogs.presentationsDialog
  })

  const closeUpdateUserDialog = useCallback(() => {
    return dispatch(closePresentationDetailsDialogAction())
  }, [dispatch, closePresentationDetailsDialogAction])

  return (
    <>
      {presentationsDialog.details.open &&
          presentationsDialog.details.presentation && (
        <PresentationDetailsDialog
          open={presentationsDialog.details.open}
          presentation={presentationsDialog.details.presentation}
          handleClose={closeUpdateUserDialog}
        />
      )}

    </>
  )
}

export default PresentationsDialogsWrapper
