import React from 'react'

import PresentationsDialogsWrapper from './PresentationsDialogsWrapper'
import ProjectDialogsWrapper from './ProjectDialogsWrapper'
import UsersDialogsWrapper from './UsersDialogsWrapper'

const DialogsWrapper: React.FC = () => {
  return (
    <>
      <PresentationsDialogsWrapper />
      <ProjectDialogsWrapper />
      <UsersDialogsWrapper />
    </>
  )
}

export default DialogsWrapper
