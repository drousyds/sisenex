import React, { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import {
  closeUpdateProjectDialogAction,
  closeProjectDetailsDialogAction
} from '../../../redux/ducks/dialogs/projects/actions'
import { AppStateType } from '../../../types'
import ProjectDetailsDialog from '../projects/ProjectDetailsDialog'
import UpdateProjectDialog from '../projects/UpdateProjectDialog'

const ProjectDialogsWrapper: React.FC = () => {
  const projectDialogs = useSelector((state: AppStateType) => {
    return state.dialogs.projectsDialog
  })

  const dispatch = useDispatch()

  const closeProjectDetailsDialog = useCallback(() => {
    return dispatch(closeProjectDetailsDialogAction())
  }, [dispatch, closeProjectDetailsDialogAction])

  const closeUpdateProjectDialog = useCallback(() => {
    return dispatch(closeUpdateProjectDialogAction())
  }, [])

  return (
    <>
      {projectDialogs.details.open && projectDialogs.details.project && (
        <ProjectDetailsDialog
          open={projectDialogs.details.open}
          handleClose={closeProjectDetailsDialog}
          project={projectDialogs.details.project}
        />
      )}
      {projectDialogs.update.open && projectDialogs.update.project && (
        <UpdateProjectDialog
          open={projectDialogs.update.open}
          project={projectDialogs.update.project}
          handleClose={closeUpdateProjectDialog}
        />
      )}
    </>
  )
}

export default ProjectDialogsWrapper
