import React, { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { closeUpdateUserDialogAction } from '../../../redux/ducks/dialogs/users/actions'
import { AppStateType } from '../../../types'
import { UpdateUserDialog } from '../users'

const UsersDialogsWrapper: React.FC = () => {
  const dispatch = useDispatch()
  const usersDialogs = useSelector((state: AppStateType) => {
    return state.dialogs.usersDialog
  })

  const closeUpdateUserDialog = useCallback(() => {
    return dispatch(closeUpdateUserDialogAction())
  }, [])

  return (
    <>
      {usersDialogs.update.open && usersDialogs.update.user && (
        <UpdateUserDialog
          open={usersDialogs.update.open}
          user={usersDialogs.update.user}
          handleClose={closeUpdateUserDialog}
        />
      )}
    </>
  )
}

export default UsersDialogsWrapper
