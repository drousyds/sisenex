import { Modal, Typography, Checkbox, Form } from 'antd'
import React from 'react'
import { useMutation } from 'react-apollo'

import {
  UPDATE_USERS_ERROR_MESSAGE,
  UPDATE_USER_SUCCESS_MESSAGE
} from '../../../constants/messages'
import {
  Pessoa,
  AtualizarPessoaMutation,
  AtualizarPessoaMutationVariables,
  AtualizarPessoaDocument
} from '../../../generated'
import {
  openNotificationWithIcon,
  getUserId,
  getUserRoles
} from '../../../helpers'
import { UserTypes } from '../../../types'

// Helpers

// Messages

const { Text } = Typography

interface Props {
  user: Pessoa
  open: boolean
  handleClose: () => void
}

interface RolesType {
  avaliadorPessoa: Pessoa['avaliadorPessoa']
  gerentePessoa: Pessoa['gerentePessoa']
  monitorPessoa: Pessoa['monitorPessoa']
  administradorPessoa: Pessoa['administradorPessoa']
}

const UpdateUserDialog = ({ open, user, handleClose }: Props) => {
  const [form] = Form.useForm()

  const [mutate, { loading }] = useMutation<
    AtualizarPessoaMutation,
    AtualizarPessoaMutationVariables
  >(AtualizarPessoaDocument, {
    onCompleted: () => {
      openNotificationWithIcon('success', UPDATE_USER_SUCCESS_MESSAGE, 5.5)
      handleClose()
    },
    onError: () => {
      openNotificationWithIcon('error', UPDATE_USERS_ERROR_MESSAGE, 5.5)
      handleClose()
    }
  })

  const submitForm = () => {
    form
      .validateFields()
      .then(values => {
        const roles = (values.roles as string[]) || []
        const newRoles: RolesType = {
          avaliadorPessoa: 0,
          monitorPessoa: 0,
          gerentePessoa: 0,
          administradorPessoa: 0
        }

        if (roles.includes(UserTypes.ADMINISTRATOR)) {
          newRoles.administradorPessoa = 1
        }
        if (roles.includes(UserTypes.MANAGER)) {
          newRoles.gerentePessoa = 1
        }
        if (roles.includes(UserTypes.MONITOR)) {
          newRoles.monitorPessoa = 1
        }
        if (roles.includes(UserTypes.REVIEWER)) {
          newRoles.avaliadorPessoa = 1
        }

        mutate({
          variables: {
            input: {
              idPessoa: user.idPessoa,
              idVinculoPessoa: user.idVinculoPessoa,
              ...newRoles
            }
          }
        })
      })
      .catch(() => {})
  }

  const renderUserInfo = () => {
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <span>
          <Text strong>Nome: </Text>
          <Text>{user.nomePessoa}</Text>
        </span>
        <span>
          <Text strong>Identificação: </Text>
          <Text>{getUserId(user)}</Text>
        </span>
        <span>
          <Text strong>Matrícula: </Text>
          <Text>{user.matriculaPessoa}</Text>
        </span>
        <span>
          <Text strong>Email: </Text>
          <Text>{user.emailPessoa}</Text>
        </span>
        <span>
          <Text strong>Telefone: </Text>
          <Text>{user.telefonePessoa}</Text>
        </span>
        <span>
          <Text strong>Lotação: </Text>
          <Text>{user.lotacaoPessoa ? user.lotacaoPessoa : 'Sem Lotação'}</Text>
        </span>
      </div>
    )
  }

  const renderForm = () => {
    return (
      <Form
        layout="vertical"
        form={form}
        name="updateUserForm"
        initialValues={{
          roles: getUserRoles(user)
        }}
      >
        <Form.Item
          label="Atribuição"
          name="roles"
          rules={[
            {
              validator: async (_, values: string[]) => {
                if (
                  values.includes(UserTypes.REVIEWER) &&
                  values.includes(UserTypes.MONITOR)
                ) {
                  throw new Error(
                    'Um Usuário não pode ser monitor e gerente simultaneamente.'
                  )
                }
              }
            }
          ]}
        >
          <Checkbox.Group>
            <Checkbox value={UserTypes.ADMINISTRATOR}>Administrador</Checkbox>
            <Checkbox value={UserTypes.REVIEWER}>Avaliador</Checkbox>
            <Checkbox value={UserTypes.MANAGER}>Gerente</Checkbox>
            <Checkbox value={UserTypes.MONITOR}>Monitor</Checkbox>
          </Checkbox.Group>
        </Form.Item>
      </Form>
    )
  }

  return (
    <Modal
      visible={open}
      title="Editar Informações do Usuário"
      onCancel={handleClose}
      centered
      okText="Salvar"
      confirmLoading={loading}
      destroyOnClose
      onOk={() => {
        submitForm()
      }}
    >
      {renderUserInfo()}
      {renderForm()}
    </Modal>
  )
}

export default UpdateUserDialog
