import { Typography } from 'antd'
import React from 'react'

import { Pessoa } from '../../../generated'

interface Props {
  user: Pessoa
  style?: React.CSSProperties
}

const { Text } = Typography

const UserDetails: React.FC<Props> = ({ user, style }) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', ...style }}>
      <span>
        <Text strong>Nome: </Text>
        <Text>{user.nomePessoa}</Text>
      </span>
      <span>
        <Text strong>Matrícula: </Text>
        <Text>{user.matriculaPessoa}</Text>
      </span>
      <span>
        <Text strong>Email: </Text>
        <Text>{user.emailPessoa}</Text>
      </span>
      <span>
        <Text strong>Telefone: </Text>
        <Text>{user.telefonePessoa}</Text>
      </span>
      <span>
        <Text strong>Lotação: </Text>
        <Text>{user.lotacaoPessoa ? user.lotacaoPessoa : 'Sem Lotação'}</Text>
      </span>
    </div>
  )
}

export default UserDetails
