import { Modal } from 'antd'
import React from 'react'

interface Props {
  open: boolean
  handleClose: () => void
}

const CreateQuestionDialog: React.FC<Props> = ({ open, handleClose }) => {
  return (
    <Modal title="Criar Pergunta" onCancel={handleClose} visible={open}>
      Form Aqui
    </Modal>
  )
}

export default CreateQuestionDialog
