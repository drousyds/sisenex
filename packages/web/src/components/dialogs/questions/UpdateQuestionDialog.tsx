import { Modal } from 'antd'
import React from 'react'

import { Pergunta } from '../../../generated'

interface Props {
  open: boolean
  question: Pergunta
  handleClose: () => void
}

const UpdateQuestionDialog: React.FC<Props> = ({
  open,
  handleClose,
  question
}) => {
  return (
    <Modal title="Editar Pergunta" onCancel={handleClose} visible={open}>
      Form Aqui
      {JSON.stringify(question, null, 2)}
    </Modal>
  )
}

export default UpdateQuestionDialog
