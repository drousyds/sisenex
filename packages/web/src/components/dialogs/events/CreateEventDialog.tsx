import {
  Modal,
  Input,
  DatePicker,
  Select,
  Checkbox,
  Row,
  Col,
  Form
} from 'antd'
import { Moment } from 'moment'
import React, { useMemo } from 'react'

import {
  MUTATIONS_MESSAGES,
  FIELDS_MESSAGES,
  FETCH_MESSAGES
} from '../../../constants/messages'
import {
  useCreateEventMutation,
  useAreasTematicasQuery,
  AreaTematica
} from '../../../generated'
import { openNotificationWithIcon } from '../../../helpers'
import { writeEvent } from '../../../services/cache.services'
import { NotificationType } from '../../../types'

interface Props {
  open: boolean
  handleClose: () => void
}

interface FormInputType {
  nomeEvento: string
  localEvento: number[]
  descricaoEvento: string
  dataEvento: Moment[]
  areasTematicasEvento: any
  statusEvento: string
}

const CreateEventDialog: React.FC<Props> = ({ open, handleClose }) => {
  const [createEventMutation, { loading }] = useCreateEventMutation({
    onCompleted: () => {
      openNotificationWithIcon(
        NotificationType.SUCCESS,
        MUTATIONS_MESSAGES.EVENT.CREATE.SUCCESS,
        5.5
      )
      handleClose()
    },
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        MUTATIONS_MESSAGES.EVENT.CREATE.ERROR,
        5.5
      )
    },
    update: (client, { data }) => {
      if (data && data.criarEvento) {
        writeEvent(client, data.criarEvento)
      }
    }
  })

  const [form] = Form.useForm()

  const { data: qData } = useAreasTematicasQuery({
    notifyOnNetworkStatusChange: true,
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        FETCH_MESSAGES.ERROR.THEMATICS,
        5.5
      )
      handleClose()
    }
  })

  const areasTematicas = useMemo(() => {
    let ats: AreaTematica[] = []
    if (qData && qData.areasTematicas) {
      ats = qData.areasTematicas
    }
    return ats
  }, [qData])

  const renderForm = () => {
    return (
      <Form form={form} name="createEventForm">
        <Form.Item
          label="Nome Evento"
          name="nomeEvento"
          rules={[
            {
              type: 'string',
              required: true,
              message: FIELDS_MESSAGES.CREATE.EVENT.EMPTY.NAME
            }
          ]}
        >
          <Input placeholder="ENEX 2019" allowClear />
        </Form.Item>

        <Form.Item
          label="Descrição Evento"
          name="descricaoEvento"
          rules={[
            {
              type: 'string',
              required: true,
              message: FIELDS_MESSAGES.CREATE.EVENT.EMPTY.DESCRIPTION
            }
          ]}
        >
          <Input placeholder="Descrição do evento..." allowClear />
        </Form.Item>

        <Form.Item
          label="Local Evento"
          name="localEvento"
          rules={[
            {
              required: true,
              message: FIELDS_MESSAGES.CREATE.EVENT.EMPTY.LOCATION
            }
          ]}
        >
          <Checkbox.Group style={{ width: '100%' }}>
            <Row>
              <Col span={8}>
                <Checkbox value={1}>Campus I</Checkbox>
              </Col>
              <Col span={8}>
                <Checkbox value={3}>Campus III</Checkbox>
              </Col>
            </Row>
            <Row style={{ marginTop: 10 }}>
              <Col span={8}>
                <Checkbox value={2}>Campus II</Checkbox>
              </Col>
              <Col span={8}>
                <Checkbox value={4}>Campus IV</Checkbox>
              </Col>
            </Row>
          </Checkbox.Group>
        </Form.Item>

        <Form.Item
          label="Área Temática:"
          name="areasTematicasEvento"
          rules={[
            {
              required: true,
              message: FIELDS_MESSAGES.CREATE.EVENT.EMPTY.THEMATICS
            }
          ]}
        >
          <Select
            placeholder="Selecione a área temática"
            mode="multiple"
            showSearch={false}
            disabled={areasTematicas.length === 0}
          >
            {areasTematicas.map(({ idAreaTematica, nomeAreaTematica }) => (
              <Select.Option key={idAreaTematica} value={idAreaTematica}>
                {nomeAreaTematica}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          label="Data Evento"
          name="dataEvento"
          rules={[
            {
              type: 'array',
              required: true,
              message: FIELDS_MESSAGES.CREATE.EVENT.EMPTY.DATE
            }
          ]}
        >
          <DatePicker.RangePicker
            style={{ width: '100%' }}
            format="DD/M/YYYY"
          />
        </Form.Item>
      </Form>
    )
  }

  const submitForm = () => {
    form
      .validateFields([
        'nomeEvento',
        'descricaoEvento',
        'localEvento',
        'areasTematicasEvento',
        'dataEvento'
      ])
      .then(values => {
        const data: FormInputType = {
          nomeEvento: values.nomeEvento,
          descricaoEvento: values.descricaoEvento,
          localEvento: values.localEvento,
          areasTematicasEvento: values.areasTematicasEvento,
          dataEvento: values.dataEvento,
          statusEvento: values.statusEvento
        }

        // Format dates
        data.dataEvento[0].startOf('day')
        data.dataEvento[1].endOf('day')

        // run mutation
        createEventMutation({
          variables: {
            input: {
              nomeEvento: data.nomeEvento,
              campi: [...data.localEvento] || [],
              descricaoEvento: data.descricaoEvento,
              dataInicioEvento: `${data.dataEvento[0].utc().valueOf()}`,
              dataFimEvento: `${data.dataEvento[1].utc().valueOf()}`,
              areasTematicas: [...data.areasTematicasEvento],
              statusEvento: data.statusEvento
            }
          }
        })
      })
      .catch(() => {})
  }

  return (
    <Modal
      width={800}
      visible={open}
      onCancel={handleClose}
      okText="Salvar"
      title="Criar Evento"
      destroyOnClose
      onOk={() => {
        submitForm()
      }}
      confirmLoading={loading}
      centered
    >
      {renderForm()}
    </Modal>
  )
}

export default CreateEventDialog
