import { PlusOutlined, DeleteOutlined } from '@ant-design/icons'
import { Modal, InputNumber, Button, Typography, Form, DatePicker } from 'antd'
import { Moment } from 'moment'
import React from 'react'

import { FIELDS_MESSAGES } from '../../../constants/messages'
import {
  Evento,
  useAlocatProjetosMutation,
  useGetAreaTematicaQuery,
  AreaTematica
} from '../../../generated'
import { openNotificationWithIcon } from '../../../helpers'
import { toMoment } from '../../../helpers/dates'
import { NotificationType } from '../../../types'
import { AnimatedAlert } from '../../alerts'

interface Props {
  open: boolean
  event: Evento
  idAreaTematica: number
  handleClose: () => void
  handleRefetch: () => void
}

const AllocationEventDialog: React.FC<Props> = ({
  open,
  event,
  idAreaTematica,
  handleClose,
  handleRefetch
}) => {
  const [form] = Form.useForm()

  const [
    allocationEventMutation,
    { loading: mutationLoading, error: mutationError }
  ] = useAlocatProjetosMutation({
    notifyOnNetworkStatusChange: true,
    onCompleted: () => {
      openNotificationWithIcon(
        NotificationType.SUCCESS,
        'A alocação foi feita com sucesso.',
        5.5
      )
      handleClose()
      handleRefetch()
    }
  })

  const { data: atData, loading: atLoading } = useGetAreaTematicaQuery({
    notifyOnNetworkStatusChange: true,
    variables: { idAreaTematica }
  })

  let areaTematica: AreaTematica | undefined

  if (atData && atData.areaTematica) {
    areaTematica = atData.areaTematica
  }

  const submitForm = () => {
    form
      .validateFields()
      .then(values => {
        const input = {
          idEvento: event.idEvento,
          idAreaTematica: idAreaTematica,
          numeroDeSalas: values.availableRooms,
          numeroProjetosSala: values.projectsPerRoom,
          horarios: values.timeKeys.map((time: Moment) =>
            time.format('YYYY-MM-DD HH:mm')
          ),
          eventoHorarioInicial: event.dataInicioEvento!,
          eventoHorarioFinal: event.dataFimEvento!
        }

        allocationEventMutation({
          variables: {
            input
          }
        })
      })
      .catch(() => {})
  }

  const renderEventInfo = () => {
    const { nomeEvento, dataInicioEvento, dataFimEvento } = event
    const inicio = dataInicioEvento ? toMoment(dataInicioEvento) : undefined
    const fim = dataFimEvento ? toMoment(dataFimEvento) : undefined
    return (
      <div>
        <div>
          <span>
            <Typography.Text strong>Evento: </Typography.Text>
            <Typography.Text>{nomeEvento}</Typography.Text>
          </span>
        </div>
        {inicio && (
          <div>
            <span>
              <Typography.Text strong>Início: </Typography.Text>
              <Typography.Text>
                {inicio.format('dddd[,] D [de] MMMM [de] YYYY')}
              </Typography.Text>
            </span>
          </div>
        )}
        {fim && (
          <div>
            <span>
              <Typography.Text strong>Término: </Typography.Text>
              <Typography.Text>
                {fim.format('dddd[,] D [de] MMMM [de] YYYY')}
              </Typography.Text>
            </span>
          </div>
        )}
      </div>
    )
  }

  const renderForm = () => (
    <Form layout={'vertical'} form={form} name="allocationEventForm">
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: '1fr 1fr',
          gap: '1rem',
          marginTop: '1rem'
        }}
      >
        <Form.Item
          label="Quantidades de Salas"
          name="availableRooms"
          rules={[
            {
              required: true,
              message: FIELDS_MESSAGES.ALLOCATION.ROOMS.EMPTY
            }
          ]}
        >
          <InputNumber
            placeholder="10 Salas"
            min={1}
            max={255}
            style={{ width: '100%' }}
          />
        </Form.Item>
        <Form.Item
          label="Quantidade de projetos por sala"
          name="projectsPerRoom"
          rules={[
            {
              required: true,
              message: FIELDS_MESSAGES.ALLOCATION.ROOMS.EMPTY
            }
          ]}
        >
          <InputNumber
            placeholder="4 Projetos"
            min={1}
            max={255}
            style={{ width: '100%' }}
          />
        </Form.Item>
      </div>

      <Form.List name="timeKeys">
        {(fields, { add, remove }) => {
          return (
            <div
              style={{
                display: 'grid',
                gridTemplateColumns: '1fr 1fr 1fr',
                gap: '1rem',
                marginTop: '1rem'
              }}
            >
              {fields.map((field, _) => (
                <Form.Item required={false} key={field.key}>
                  <Form.Item
                    {...field}
                    key={`picker-${field.key}`}
                    rules={[
                      {
                        type: 'object',
                        required: true,
                        message: 'Please select time!'
                      }
                    ]}
                    noStyle
                  >
                    <DatePicker
                      showTime
                      format="YYYY-MM-DD HH:mm"
                      style={{ width: '100%' }}
                    />
                  </Form.Item>
                  {fields.length > 1 ? (
                    <Button
                      type="dashed"
                      icon={<DeleteOutlined />}
                      onClick={() => {
                        remove(field.name)
                      }}
                      style={{ marginTop: 10, width: '100%' }}
                    >
                      Remover
                    </Button>
                  ) : null}
                </Form.Item>
              ))}

              <Form.Item>
                <Button
                  type="dashed"
                  onClick={() => {
                    add()
                  }}
                  style={{ width: '100%' }}
                >
                  <PlusOutlined /> Adicionar Horário
                </Button>
              </Form.Item>
            </div>
          )
        }}
      </Form.List>
    </Form>
  )

  return (
    <Modal
      width={800}
      visible={open}
      onCancel={handleClose}
      okText="Salvar"
      // confirmLoading={loading}
      title={`Distribuir ${
        !atLoading && areaTematica
          ? areaTematica.nomeAreaTematica
          : 'Área Tematica'
      } do ${event.nomeEvento}`}
      destroyOnClose
      onOk={() => {
        submitForm()
      }}
      confirmLoading={atLoading || mutationLoading}
      centered
    >
      {renderEventInfo()}
      {renderForm()}
      {mutationError && (
        <AnimatedAlert
          style={{ marginTop: 10 }}
          title="Ocorreu um erro"
          description={mutationError.message.split('GraphQL error:')[1]}
          type="error"
          showIcon
          show
        />
      )}
    </Modal>
  )
}

export default AllocationEventDialog
