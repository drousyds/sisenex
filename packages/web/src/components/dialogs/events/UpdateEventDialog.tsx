import { Modal, Form, Input } from 'antd'
import React from 'react'

import { Evento, useUpdateEventMutation } from '../../../generated'
import { openNotificationWithIcon } from '../../../helpers'

interface Props {
  open: boolean
  event: Evento
  handleClose: () => void
}

interface FormInputType {
  nomeEvento: string
  localEvento?: number[]
  descricaoEvento: string
}

const UpdateEventDialog: React.FC<Props> = ({ event, open, handleClose }) => {
  const [form] = Form.useForm()

  const [updateEventMutation, { loading }] = useUpdateEventMutation({
    notifyOnNetworkStatusChange: true,
    onCompleted: () => {
      openNotificationWithIcon(
        'success',
        'O Evento foi atualizado com sucesso.',
        5.5
      )
      handleClose()
    },
    onError: () => {
      openNotificationWithIcon(
        'error',
        'Ocorreu um erro ao atualizar o Evento.',
        5.5
      )
      handleClose()
    }
  })

  const submitForm = () => {
    form
      .validateFields(['nomeEvento', 'descricaoEvento'])
      .then(values => {
        const data: FormInputType = {
          nomeEvento: values.nomeEvento,
          descricaoEvento: values.descricaoEvento
        }

        updateEventMutation({
          variables: {
            input: {
              idEvento: event.idEvento,
              nomeEvento: data.nomeEvento,
              descricaoEvento: data.descricaoEvento
            }
          }
        })
      })
      .catch(() => {})
  }

  const renderForm = () => {
    return (
      <Form
        form={form}
        name="updateEventForm"
        initialValues={{
          nomeEvento: event.nomeEvento || '',
          descricaoEvento: event.descricaoEvento || ''
        }}
      >
        <Form.Item
          label="Nome Evento"
          name="nomeEvento"
          rules={[
            {
              type: 'string',
              required: true,
              message: 'Você precisa inserir o nome do evento.'
            }
          ]}
        >
          <Input placeholder="ENEX 2019" allowClear />
        </Form.Item>
        <Form.Item
          label="Descrição Evento"
          name="descricaoEvento"
          rules={[
            {
              type: 'string',
              required: true,
              message: 'Você precisa inserir a descrição do evento.'
            }
          ]}
        >
          <Input placeholder="Descrição do evento..." allowClear />
        </Form.Item>
      </Form>
    )
  }

  return (
    <Modal
      visible={open}
      onCancel={handleClose}
      okText="Salvar"
      title="Atualizar Evento"
      destroyOnClose
      onOk={() => {
        submitForm()
      }}
      confirmLoading={loading}
      centered
    >
      {renderForm()}
    </Modal>
  )
}

export default UpdateEventDialog
