import { CheckOutlined } from '@ant-design/icons'
import { Table, Tooltip, Button, Tag, Select } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import React from 'react'
import { render } from 'react-dom'

import { MUTATIONS_MESSAGES } from '../../constants/messages'
import { useAtualizarSolicitacaoLgpdMutation } from '../../generated'
import { openNotificationWithIcon } from '../../helpers'
import getLGPDStatusColor from '../../helpers/getLGPDStatusColor'
import getLGPDStatusText from '../../helpers/getLGPDStatusText'
import { SolicitacaoTableItem} from '../../pages/gerente/lgpd/SolicitacoesLGPD'
import { NotificationType } from '../../types'
import { VerticalFade } from '../animation'
import RoundedDiv from '../common/RoundedDiv'

interface Props {
  solicitacoesLGPD: SolicitacaoTableItem[];
  loading: boolean;
  tableFooter?: React.ReactNode;
  handleRefetch: () => void;
}

const LGPDTable: React.FC<Props> = ({
  solicitacoesLGPD,
  loading,
  handleRefetch,
  tableFooter
}) => {
  const [mutate, { loading: mLoading }] = useAtualizarSolicitacaoLgpdMutation({
    notifyOnNetworkStatusChange: true,
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        MUTATIONS_MESSAGES.LGPD.SOLVE.ERROR,
        5.5
      )
    },
    onCompleted: () => {
      openNotificationWithIcon(
        NotificationType.SUCCESS,
        MUTATIONS_MESSAGES.LGPD.SOLVE.SUCCESS,
        5.5
      )
      handleRefetch()
    }
  })

  const textoSolicitacao = {
    1: "Consulta de dados",
    2: "Solicitação de dados",
    3: "Exclusão de dados",
    4: "Outros"
  }

  const columns: ColumnProps<SolicitacaoTableItem>[] = [
    {
      title: 'Código',
      dataIndex: 'idSolicitacao'
    },
    {
      title: 'Tipo',
      key: 'tipoSolicitacao',
      render (_, { tipoSolicitacao }) {
        return <span>{textoSolicitacao[tipoSolicitacao]}</span>
      }
    },
    {
        title: 'Nome',
        key: 'nomeSolicitacao',
        render (_, { nomeSolicitacao, sobrenomeSolicitacao }) {
          return <span>{nomeSolicitacao} {sobrenomeSolicitacao}</span>
        }
    },
    {
      title: 'Email',
      dataIndex: 'emailPessoa'
    },
    {
      title: 'Texto Solicitacao',
      key: 'textoSolicitacao',
      render: (_, { textoSolicitacao }) => (
        <span style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>
          {textoSolicitacao ? textoSolicitacao : 'Esse tipo de solicitação não possui texto.'}
        </span>
      )
    },
    {
      title: 'Estado',
      key: 'solicitacao-status',
      render: (_, { statusSolicitacao }) => {
        const s = getLGPDStatusText(statusSolicitacao)
        return (
          <Tag color={getLGPDStatusColor(s)}>{s}</Tag>
        )
      }
    },{
      title: 'Alterar Estado',
      key: 'solve-report',
      align: 'center',
      render: (_, { idSolicitacao, statusSolicitacao }) => (
        <Select
          placeholder={getLGPDStatusText(statusSolicitacao)}
          onChange={value => {
            mutate({
              variables: {
                idSolicitacao,
                statusSolicitacao: value.toString()
              }
            })
          }}
          allowClear
          style={{ width: '100%' }}
          disabled={statusSolicitacao == 'RESOLVIDA' || statusSolicitacao == 'ARQUIVADA'}
        >
          <Select.Option key="report-filter-solved" value="RESPONDIDA">
            Respondida
          </Select.Option>
          <Select.Option key="report-filter-solved" value="RESOLVIDA">
            Resolvida
          </Select.Option>
          <Select.Option key="report-filter-open" value="ARQUIVADA">
            Arquivada
          </Select.Option>
        </Select>
     )
    }
  ]

  return (
    <VerticalFade direction="up" amount={20}>
      <RoundedDiv shadow style={{ marginTop: 10, height: '100%' }}>
        <Table
          style={{ overflowX: 'auto' }}
          dataSource={solicitacoesLGPD}
          rowKey={({ idSolicitacao }) => `${idSolicitacao}`}
          columns={columns}
          loading={loading || mLoading}
          footer={() => tableFooter || null}
        />
      </RoundedDiv>
    </VerticalFade>
  )
}

export default LGPDTable
