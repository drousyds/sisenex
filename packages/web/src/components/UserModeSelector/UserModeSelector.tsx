import { Result, Button } from 'antd'
import React from 'react'

import { ModeType } from '../../types'

const MODES: ModeType[] = [
  { id: 1, name: 'e-tertúlia' },
  { id: 2, name: 'Vídeo' }
]

interface Props {
  onModeClick: (mode: ModeType) => Promise<void>
}

const UserModeSelector: React.FC<Props> = ({ onModeClick }) => {
  const handleClick = async (
    mode: ModeType
  ) => {
    await onModeClick(mode)
  }

  return (
    <div>
      <Result
        status="warning"
        title="Modo de Avaliação"
        subTitle="Qual tipo de projetos você gostaria de avaliar?"
      >
        {MODES.map(mode =>
          (<Button
            key={`${mode.name}-${mode.id}`}
            size="large"
            style={{ width: '100%', marginBottom: 15 }}
            onClick={() => handleClick(mode)}
          >
            {mode.name}
          </Button>)
        )}
      </Result>
    </div>
  )
}

export default UserModeSelector
