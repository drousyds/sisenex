import React from 'react'
import { Typography } from 'antd'

interface Props {
  text: string
  pre: string
  strong?: boolean
}

const InlineStrongText: React.FC<Props> = ({ strong, pre, text }) => {
  return (
    <div style={{ wordWrap: 'break-word' }}>
      <span>
        <Typography.Text strong={strong ? true : false}>{pre}</Typography.Text>
        <Typography.Text>{text}</Typography.Text>
      </span>
    </div>
  )
}

export default InlineStrongText
