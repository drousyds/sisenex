import React, { ReactNode } from 'react'
import { Row, Col } from 'antd'

export interface Props {
  left?: () => ReactNode
  right?: () => ReactNode
  leftSpan?: number
  rightSpan?: number
  gutter?: number
}

const TwoColumnRow = ({ left, right, leftSpan, rightSpan, gutter }: Props) => (
  <Row gutter={gutter || 16} style={{ width: '100%' }}>
    <Col span={leftSpan || 12}>{left ? left() : null}</Col>
    <Col span={rightSpan || 12}>{right ? right() : null}</Col>
  </Row>
)

export default TwoColumnRow
