import React, { CSSProperties } from 'react'

interface Props {
  style?: CSSProperties
  shadow?: boolean
}

const RoundedDiv: React.FC<Props> = ({ children, style, shadow }) => (
  <div
    style={{
      backgroundColor: '#fff',
      borderRadius: 5,
      padding: 10,
      boxShadow:
        shadow === false ? undefined : '0 2px 4px 0 rgba(0, 0, 0, 0.15)',
      ...style
    }}
  >
    {children}
  </div>
)

export default RoundedDiv
