import React from 'react'
import { Typography } from 'antd'

interface Props {
  counter: number
  pre: string
  strong?: boolean
  fallback: string
}

const CounterItem: React.FC<Props> = ({ strong, pre, counter, fallback }) => {
  return (
    <div>
      <span>
        <Typography.Text strong={strong ? true : false}>{pre}</Typography.Text>
        <Typography.Text>
          {counter > 0 ? ` ${counter}` : ` ${fallback}`}
        </Typography.Text>
      </span>
    </div>
  )
}

export default CounterItem
