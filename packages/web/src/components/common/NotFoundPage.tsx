import React from 'react'
import { Result, Button } from 'antd'
import { useHistory } from 'react-router-dom'
import { VerticalFade } from '../animation'
import RoundedDiv from './RoundedDiv'

const NotFoundPage = () => {
  const history = useHistory()
  return (
    <VerticalFade direction="up" amount={20}>
      <RoundedDiv style={{ height: '100%' }}>
        <Result
          status="404"
          title="404"
          subTitle="Desculpe, essa página não existe."
          extra={
            <Button type="primary" onClick={() => history.push('/')}>
              Página Principal
            </Button>
          }
        />
      </RoundedDiv>
    </VerticalFade>
  )
}

export default NotFoundPage
