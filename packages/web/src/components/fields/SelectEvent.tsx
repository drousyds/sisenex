import { Select, Form } from 'antd'
import React from 'react'

import { useGetEventsQuery, Evento } from '../../generated'
import { openNotificationWithIcon } from '../../helpers'

interface Props {
  formItem?: {
    label: string
  }
  style?: React.CSSProperties
  getValue: (value: number | undefined) => void
  allowClear: boolean
}

const SelectEvent: React.FC<Props> = ({
  formItem,
  style,
  getValue,
  allowClear
}) => {
  const { data, loading } = useGetEventsQuery({
    notifyOnNetworkStatusChange: true,
    onError: () => {
      openNotificationWithIcon(
        'error',
        'Ocorreu um erro ao buscar os eventos.',
        5.5
      )
    }
  })

  let events: Evento[] = []

  if (data && data.eventos) {
    events = data.eventos
  }

  const select = (
    <Select
      disabled={loading}
      loading={loading}
      style={{ minWidth: 225, ...style }}
      placeholder="Selecione o Evento"
      allowClear={!!allowClear}
      onSelect={value => getValue(value as number | undefined)}
    >
      {events.map(e => (
        <Select.Option key={`select-${e.idEvento}`} value={e.idEvento}>
          {e.nomeEvento}
        </Select.Option>
      ))}
    </Select>
  )

  return formItem ? <Form.Item {...formItem}>{select}</Form.Item> : select
}

export default SelectEvent
