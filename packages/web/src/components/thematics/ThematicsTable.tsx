import React from 'react'
import { RightOutlined } from '@ant-design/icons';
import { Table, Tooltip, Button, Result } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import { withRouter, RouteComponentProps } from 'react-router-dom'
import { AreaTematica, Evento } from '../../generated'
import RoundedDiv from '../common/RoundedDiv'
import { AnimatedContent } from '../animation'
interface Props extends RouteComponentProps {
  event: Evento
  loading: boolean
}

const ThematicsTable: React.FC<Props> = ({ event, loading, history }) => {
  let thematics: AreaTematica[] = []
  if (event.areasTematicas) {
    thematics = event.areasTematicas || []
  }

  const columns: ColumnProps<AreaTematica>[] = [
    {
      title: `Areas Temáticas`,
      key: 'nomeAreaTematica',
      render: (_, area) => (
        <span
          style={{ cursor: 'pointer' }}
          onClick={() =>
            history.push(
              `/eventos/${event.idEvento}/tertulias/${area.idAreaTematica}`
            )
          }
        >
          {area.nomeAreaTematica}
        </span>
      )
    },
    {
      title: 'Visualizar',
      align: 'right',
      render: (_, area) => {
        return (
          <span>
            <Tooltip title="Visualizar Área Temática">
              <Button
                onClick={() => {
                  history.push(
                    `/eventos/${event.idEvento}/tertulias/${area.idAreaTematica}`
                  )
                }}
                icon={<RightOutlined />}
                shape="circle"
                type="dashed"
              />
            </Tooltip>
          </span>
        );
      }
    }
  ]

  return (
    <AnimatedContent>
      <RoundedDiv shadow style={{ marginTop: 10 }}>
        {thematics.length > 0 ? (
          <div style={{ overflowX: 'auto' }}>
            <Table
              dataSource={thematics}
              loading={loading}
              columns={columns}
              rowKey={thematic => `${thematic.idAreaTematica}`}
              pagination={false}
            />
          </div>
        ) : (
          <Result
            status="warning"
            title={`Sem Áreas Temáticas`}
            subTitle={`O Evento ${event.nomeEvento} não apresenta nenhuma área temática. Tente adicionar uma.`}
            extra={[
              <Button
                key="event-area-goback-btn"
                onClick={() => history.push(`/eventos/${event.idEvento}`)}
              >
                Voltar
              </Button>
            ]}
          />
        )}
      </RoundedDiv>
    </AnimatedContent>
  )
}

export default withRouter(ThematicsTable)
