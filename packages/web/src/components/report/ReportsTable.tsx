import { CheckOutlined } from '@ant-design/icons'
import { Table, Tooltip, Button, Tag, Select } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import moment from 'moment'
import React from 'react'

import { MUTATIONS_MESSAGES } from '../../constants/messages'
import { useAtualizarStatusReportMutation } from '../../generated'
import { openNotificationWithIcon, getReportStatusText } from '../../helpers'
import getLGPDStatusColor from '../../helpers/getLGPDStatusColor'
import getReportStatusColor from '../../helpers/getReportStatusColor'
import { ReportTableItem } from '../../pages/gerente/reports'
import { NotificationType } from '../../types'
import { VerticalFade } from '../animation'
import RoundedDiv from '../common/RoundedDiv'

interface Props {
  reports: ReportTableItem[]
  loading: boolean
  tableFooter?: React.ReactNode
  handleRefetch: () => void
}

const ReportsTable: React.FC<Props> = ({
  reports,
  loading,
  handleRefetch,
  tableFooter
}) => {
  const [mutate, { loading: mLoading }] = useAtualizarStatusReportMutation({
    notifyOnNetworkStatusChange: true,
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        MUTATIONS_MESSAGES.REPORT.SOLVE.ERROR,
        5.5
      )
    },
    onCompleted: () => {
      openNotificationWithIcon(
        NotificationType.SUCCESS,
        MUTATIONS_MESSAGES.REPORT.SOLVE.SUCCESS,
        5.5
      )
      handleRefetch()
    }
  })

  const columns: ColumnProps<ReportTableItem>[] = [
    {
      title: 'Código',
      dataIndex: 'idReport'
    },
    {
      title: 'Sala',
      key: 'sala-report',
      render(_, { apresentacao }) {
        return <span>{apresentacao?.salaApresentacao}</span>
      }
    },
    {
      title: 'Email',
      dataIndex: 'emailReport'
    },
    {
      title: 'Pessoa',
      key: 'pessoa-report',
      render(_, { avaliador }) {
        return <span>{avaliador.nomePessoa}</span>
      }
    },
    {
      title: 'Conteúdo',
      key: 'conteudoReport',
      render: (_, { conteudoReport }) => (
        <span style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>
          {conteudoReport}
        </span>
      )
    },
    {
      title: 'Hora',
      key: 'time',
      render: (_, { dataCriacao }) => (
        <span>
          {dataCriacao
            ? moment(Number(dataCriacao)).format(
                'D [de] MMMM [de] YYYY [às] HH:mm'
              )
            : null}
        </span>
      )
    },
    {
      title: 'Estado',
      key: 'report-status',
      render: (_, { statusReport }) => {
        const s = getReportStatusText(statusReport)
        return <Tag color={getReportStatusColor(s)}>{s}</Tag>
      }
    },
    {
      title: 'Alterar Estado',
      key: 'solve-report',
      align: 'center',
      render: (_, { idReport, statusReport }) => (
        <Select
          placeholder={getReportStatusText(statusReport)}
          onChange={value => {
            mutate({
              variables: {
                idReport,
                statusReport: value.toString()
              }
            })
          }}
          allowClear
          style={{ width: '100%' }}
          disabled={statusReport == 'CONCLUIDA'}
        >
          <Select.Option key="report-filter-solved" value="EM ANDAMENTO">
            EM ANDAMENTO
          </Select.Option>
          <Select.Option key="report-filter-solved" value="CONCLUIDA">
            CONCLUÍDA
          </Select.Option>
        </Select>
      )
    }
  ]

  return (
    <VerticalFade direction="up" amount={20}>
      <RoundedDiv shadow style={{ marginTop: 10, height: '100%' }}>
        <Table
          style={{ overflowX: 'auto' }}
          dataSource={reports}
          rowKey={({ idReport }) => `${idReport}`}
          columns={columns}
          loading={loading || mLoading}
          footer={() => tableFooter || null}
        />
      </RoundedDiv>
    </VerticalFade>
  )
}

export default ReportsTable
