import { Layout, Button, Menu, Dropdown } from 'antd'
import React from 'react'
import { useHistory } from 'react-router-dom'

import { OAUTH_URL } from '../../constants/constants'
import { getAvatarRandomColor } from '../../helpers'
import useAuth from '../../hooks/useAuth'
import UserInfo from './UserInfo'

const { Header } = Layout


const TopBar: React.FC = () => {
  const { authState, localLogout, isAuthenticated, isAvaliador, isGerente, setScreenMode } = useAuth()
  const history = useHistory()

  const dropdownMenu = (
    <Menu>
      {isAuthenticated() && isAvaliador() && isGerente() && (
        <Menu.Item onClick={() => {
          const newScreenMode = authState.mode === 'AVALIADOR' ? 'GERENTE' : 'AVALIADOR'
          const redirectURL = `/${newScreenMode.toLowerCase()}/home`
          setScreenMode(newScreenMode)
          history.push(redirectURL)
        }}>
          {authState.mode === 'AVALIADOR' ? 'Modo Gerente' : 'Modo Avaliação'}
        </Menu.Item>
      )}
      <Menu.Item onClick={() => localLogout()}>
        Sair
      </Menu.Item>

    </Menu>
  )

  return (
    <Header
      style={{
        background: '#fff',
        padding: '0 1rem',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end'
      }}
    >

          {authState.currentUser ? (

        <Dropdown overlay={dropdownMenu} placement="bottomCenter">
          <div>
            <UserInfo
              user={authState.currentUser!}
              avatarStyle={{ marginRight: 10, ...getAvatarRandomColor() }}
            />
          </div>
        </Dropdown>
      ) : (
        <div style={{ marginRight: 16 }}>
          <a href={OAUTH_URL}>
            <Button style={{ marginRight: 10 }} type="primary">
              Fazer Login
            </Button>
          </a>
        </div>
      )}
    </Header>
        
  )
  
}


export default TopBar
