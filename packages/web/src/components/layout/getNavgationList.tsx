import {
  HomeFilled,
  ProjectFilled,
  PlaySquareFilled,
  TeamOutlined,
  WarningFilled,
  SnippetsFilled,
  DatabaseFilled,
  PlusOutlined,
  OrderedListOutlined,
  QuestionCircleFilled,
  UserOutlined
} from '@ant-design/icons'

import { ScreenModeType } from '../../contexts/AuthContext'

export interface NavigationItem {
  key: string
  pathname: string
  icon?: any
}

export function getNavigationList(mode?: ScreenModeType) {
  const defaultList: NavigationItem[] = [
    {
      key: 'Início',
      pathname: '/',
      icon: HomeFilled
    }
  ]

  if (mode === 'GERENTE') {
    const gerenteList: NavigationItem[] = [
      {
        key: 'Início',
        pathname: '/gerente/home',
        icon: HomeFilled
      },
      {
        key: 'Apresentações',
        pathname: '/gerente/apresentacoes',
        icon: PlaySquareFilled
      },
      {
        key: 'Projetos',
        pathname: '/gerente/projetos',
        icon: ProjectFilled
      },
      {
        key: 'Pessoas',
        pathname: '/gerente/pessoas',
        icon: TeamOutlined
      },
      {
        key: 'Chamados',
        pathname: '/gerente/reports',
        icon: WarningFilled
      },
      {
        key: 'Solicitações LGPD',
        pathname: '/gerente/solicitacoeslgpd',
        icon: DatabaseFilled
      },

      {
        key: 'Relatório',
        pathname: '/gerente/relatorio',
        icon: SnippetsFilled
      },
      {
        key: 'Avaliação',
        pathname: '/gerente/avaliacao',
        icon: PlusOutlined
      }
    ]
    return gerenteList
  } else if (mode === 'AVALIADOR') {
    return [
      {
        key: 'Início',
        pathname: '/avaliador/home',
        icon: HomeFilled
      },
      {
        key: 'Avaliações',
        pathname: '/avaliador/projetos',
        icon: ProjectFilled
      },
      {
        key: 'Histórico',
        pathname: '/avaliador/avaliacoes',
        icon: OrderedListOutlined
      },
      {
        key: 'Identidade',
        pathname: '/avaliador/identidade',
        icon: UserOutlined
      },
      {
        key: 'Preciso de ajuda',
        pathname: '/avaliador/reports',
        icon: QuestionCircleFilled
      }
    ]
  }
  return defaultList
}

export function getNavigationListFooter(mode?: ScreenModeType) {
  const defaultList: NavigationItem[] = [
    {
      key: 'Política de Privacidade',
      pathname: '/politica'
    }
  ]

  if (mode === 'GERENTE') {
    const gerenteList: NavigationItem[] = [
      {
        key: 'Política de Privacidade',
        pathname: '/gerente/politica'
      },
      {
        key: 'Lei Geral de Proteção de Dados',
        pathname: '/gerente/solicitacao'
      }
    ]
    return gerenteList
  } else if (mode === 'AVALIADOR') {
    return [
      {
        key: 'Política de Privacidade',
        pathname: '/avaliador/politica'
      },
      {
        key: 'Lei Geral de Proteção de Dados',
        pathname: '/avaliador/solicitacao'
      }
    ]
  }
  return defaultList
}
