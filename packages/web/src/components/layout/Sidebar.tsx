import { Button, Layout, Menu } from 'antd'
import React, { useMemo } from 'react'
import { NavLink, useLocation } from 'react-router-dom'

import useAuth from '../../hooks/useAuth'
import { getNavigationList } from './getNavgationList'

const { Header, Sider } = Layout

const Sidebar: React.FC = () => {
  const { authState } = useAuth()
  const location = useLocation()

  const navigationList = useMemo(() => {
    return getNavigationList(authState.mode)
  }, [authState.mode])

  return (
    <Sider trigger={null} collapsible collapsed={false} theme="dark">
      <Layout>
        <Header
          style={{
            color: '#fff',
            fontSize: 25,
            fontWeight: 'bold'
          }}
        >
          SisEnex
        </Header>
      </Layout>
      <Menu
        theme="dark"
        mode="inline"
        selectedKeys={[location.pathname]}
      >
        {navigationList.map(item => (
          <Menu.Item key={item.pathname}>
            <NavLink to={item.pathname} className="nav-text">
              <span>
                {React.createElement(item.icon)}
                <span>{item.key}</span>
              </span>
            </NavLink>
          </Menu.Item>
        ))}
      </Menu>
    </Sider>
  )
}

export default Sidebar
