import { Layout} from 'antd'
import React from 'react'

import { THEME } from '../../constants'
import Sidebar from './Sidebar'
import TopBar from './TopBar'
import FooterNav from './FooterNav'


const { Content } = Layout


const AppLayout: React.FC = ({ children }) => {
  return (
    <Layout>
      <Sidebar />
      <Layout style={{ minHeight: '100vh' }}>
        <TopBar />
        <Content
          style={{
            ...THEME.layout.content,
            minHeight: 280,
            borderRadius: 5,
            flex: 1
          }}
        >
          {children}
        </Content>
        <FooterNav/>
      </Layout>
    </Layout>

      
   
  )
}

export default AppLayout
