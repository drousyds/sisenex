import React from 'react'
import { Avatar, Typography } from 'antd'

// Types
import { Pessoa } from '../../generated'

const { Text } = Typography

interface Props {
  user: Pessoa
  textStyle?: {
    marginTop?: number
    marginBottom?: number
    marginLeft?: number
    marginRight?: number
    backgroundColor?: string
  }
  avatarStyle?: {
    color?: string
    backgroundColor?: string
    marginLeft?: number
    marginRight?: number
    marginTop?: number
    marginBottom?: number
  }
}

const UserInfo = (props: Props) => (
  <div style={{ display: 'flex', flexDirection: 'row' }}>
    <div style={{ marginLeft: 10 }}>
      <Avatar
        size="large"
        shape="square"
        style={{
          color: '#f56a00',
          backgroundColor: '#fde3cf',
          ...props.avatarStyle
        }}
      >
        {props.user.nomePessoa!.charAt(0)}
      </Avatar>
    </div>
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <Text strong>{props.user.nomePessoa}</Text>
    </div>
  </div>
)

export default UserInfo
