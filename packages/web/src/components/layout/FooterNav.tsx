import React, { useMemo } from 'react'
import { Layout } from 'antd'
import { NavLink } from 'react-router-dom'
import useAuth from '../../hooks/useAuth'
import { getNavigationListFooter } from './getNavgationList'

export interface NavigationItem {
  key: string
  pathname: string
  icon?: any
}

const FooterNav: React.FC = () => {
  const { Footer } = Layout
  const { authState } = useAuth()
  const navigationList = useMemo(() => {
    return getNavigationListFooter(authState.mode)
  }, [authState.mode])

  return (
    <Footer
      style={{
        background: '#fff',
        padding: 10,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <div style={{ display: 'flex', flexDirection: 'row' }}>
        {navigationList.map(item => (
          <NavLink
            key={item.pathname}
            to={item.pathname}
            className="nav-text"
            style={{ marginLeft: 10, marginRight: 10 }}
          >
            <span>{item.key}</span>
          </NavLink>
        ))}
      </div>

      <p>
        SisEnex - Sistema de Avaliação do Enex | Copyright © 2018-2022 - UFPB
      </p>
    </Footer>
  )
}

export default FooterNav
