import { CloseCircleFilled, CloseOutlined, EditOutlined, EyeOutlined, PlusCircleFilled } from '@ant-design/icons'
import { Table, Tooltip } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import React, { useEffect } from 'react'
import { Projeto } from '../../generated'
import { openNotificationWithIcon } from '../../helpers'
import { AnimatedContent } from '../animation'
import RoundedDiv from '../common/RoundedDiv'
import { NotificationType } from '../../types'
import { ADD_PROJECT_SUCCESS_MESSAGE, FETCH_MESSAGES } from '../../constants/messages'

interface Props {
  loading: boolean
  projects: Projeto[]
  tableFooter?: React.ReactNode
  projectSelected: Array<any>
  setProjectSelected: Function
  projectsRef: React.RefObject<HTMLDivElement>
  filtersText: string | undefined
}

const AddProjectsTable = ({ loading, projects, tableFooter, projectSelected, setProjectSelected, projectsRef, filtersText }: Props) => {
  
  const handleRemoveProject = (index) => {
    const updatedProjects = [...projectSelected];
    updatedProjects.splice(index, 1);
    setProjectSelected(updatedProjects);
  };

  useEffect(() => {
    
    if (projectsRef.current) {
      projectsRef.current.scrollTop = projectsRef.current.scrollHeight
    }
  }, [projectSelected]

  )
  const columns: ColumnProps<Projeto>[] = [
    {
      title: 'Adicionar',
      dataIndex: 'adicionarProjeto',
      render: (_, project) => {

        return (
          <Tooltip title="Adicionar projeto">
            <PlusCircleFilled
              style={{ cursor: 'pointer' }}
              onClick={() => {
                let auxControl = true
                projectSelected.forEach((item) => {
                  if (item.idProjeto === project.idProjeto) {
                    auxControl = false
                    return
                  }
                })
                if (auxControl) {
                  const updatedProjects = [...projectSelected, project];
                  setProjectSelected(updatedProjects);
                  openNotificationWithIcon('success', ADD_PROJECT_SUCCESS_MESSAGE, 5.5)

                } else {
                  openNotificationWithIcon(
                    NotificationType.ERROR,
                    FETCH_MESSAGES.ERROR.PROJECTEQUAL,
                    5.5
                  )
                }
              }} />
          </Tooltip>
        );
      }
    },
    {
      title: 'Código',
      dataIndex: 'codigoProjeto'
    },
    {
      title: 'Projeto',
      dataIndex: 'tituloProjeto'
    },
  ]
  return (
    <div>
      {filtersText ?
        <AnimatedContent>
          <RoundedDiv shadow style={{ marginTop: 10 }}>
            <div >
              <Table
                className="rounded-table"
                loading={loading}
                columns={columns}
                rowKey={(project): string => `${project.idProjeto}}`}
                dataSource={projects}
                footer={() => (tableFooter || null)}
              />
            </div>
          </RoundedDiv>
        </AnimatedContent> : null}
      {
        projectSelected.length > 0 ? (
          <div style={{
            width: '100%',
            marginTop: '20px',
            backgroundColor: '#C5F299',
            borderRadius: '5px',
            padding: '5px 10px',
            fontFamily: 'Arial, sans-serif',
            fontSize: '14px',
            fontWeight: 100,
            color: '#5FBF1B'
          }}>
            {projectSelected.map((item, key) => (
              <div key={key} style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', paddingBottom: key !== projectSelected.length - 1 ? '5px' : '0', borderBottom: key !== projectSelected.length - 1 ? '1px dashed #5FBF1B' : 'none' }}>
                <div style={{ marginRight: '10px' }}>{item.tituloProjeto}</div>
                <span><CloseCircleFilled style={{ cursor: 'pointer' }} onClick={() => handleRemoveProject(key)} /></span>
              </div>
            ))}
          </div>) : null
      }
    </div>
  )
}

export default AddProjectsTable