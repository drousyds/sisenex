import { EditOutlined, EyeOutlined } from '@ant-design/icons'
import { Table, Tag, Button, Tooltip } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import React from 'react'
import { useDispatch } from 'react-redux'

import { Projeto } from '../../generated'
import { getProjectStatus, getModeText, getStatusTagColor } from '../../helpers'
import { openProjectDetailsDialogAction, openUpdateProjectDialogAction } from '../../redux/ducks/dialogs/projects/actions'
import { AnimatedContent } from '../animation'
import RoundedDiv from '../common/RoundedDiv'

interface Props {
  loading: boolean
  projects: Projeto[]
  tableFooter?: React.ReactNode
}

const ProjectsTable = ({ loading, projects, tableFooter }: Props) => {
  const dispatch = useDispatch()

  const columns: ColumnProps<Projeto>[] = [
    {
      title: 'Código',
      dataIndex: 'codigoProjeto'
    },
    {
      title: 'Projeto',
      dataIndex: 'tituloProjeto'
    },
    {
      title: 'Estado',
      key: 'projectStatus',
      render: (_, project) => {
        return (
          <span>
            <Tag color={getStatusTagColor(project)}>
              {project.statusProjeto}
            </Tag>
          </span>
        );
      }
    },
    {
      title: 'Categoria',
      key: 'projectCategory',
      render: (_, { categoria }) =>
        categoria.nomeCategoria ? <span>{categoria.nomeCategoria}</span> : null
    },
    {
      title: 'Apresentação',
      align: 'center',
      key: 'projetctPresentation',
      render: (_, project) => {
        return (
          <span>
            {project?.apresentacao?.codigoApresentacao
              ? project.apresentacao.codigoApresentacao
              : 'Sem Apresentação'}
          </span>
        )
      }
    },
    {
      title: 'Local',
      key: 'salaApresentacao',
      align: 'center',
      render: (_, project) => {
        if (project.apresentacao) {
          return <span>{project.apresentacao.salaApresentacao}</span>
        }
        return null
      }
    },
    {
      title: 'Editar',
      key: 'alocarProjeto',
      align: 'center',
      render: (_, project) => (
        <Tooltip title="Editar Projeto">
          <span>
            <Button
              onClick={() => dispatch(openUpdateProjectDialogAction(project))}
              icon={<EditOutlined />}
              shape="circle"
              type="dashed"
            />
          </span>
        </Tooltip>
      )
    },
    {
      title: 'Visualizar',
      key: 'viewProject',
      align: 'center',
      render: (_, project) => (
        <Tooltip title="Visualizar Projeto">
          <span>
            <Button
              onClick={() => dispatch(openProjectDetailsDialogAction(project))}
              icon={<EyeOutlined />}
              shape="circle"
              type="dashed"
            />
          </span>
        </Tooltip>
      )
    }
  ]

  return (
    <AnimatedContent>
      <RoundedDiv shadow style={{ marginTop: 10 }}>
        <div style={{ overflowX: 'auto' }}>
          <Table
            className="rounded-table"
            loading={loading}
            columns={columns}
            rowKey={(project): string => `${project.idProjeto}`}
            dataSource={projects}
            footer={() => (tableFooter || null)}
          />
        </div>
      </RoundedDiv>
    </AnimatedContent>
  )
}

export default ProjectsTable
