import { Button, Form, Input, PageHeader } from 'antd'
import React, { useMemo } from 'react'
import { useHistory } from 'react-router-dom'

import FullSpinner from '../../components/FullSpinner/FullSpinner'
import UserInfo from '../../components/layout/UserInfo'
import { ScreenModeType } from '../../contexts/AuthContext'
import {
  NotaPerguntaInput,
  useGetAddReviewPageDataQuery,
  Categoria,
  Pergunta
} from '../../generated'
import { openNotificationWithIcon } from '../../helpers'
import { NotificationType } from '../../types'
import AddAvaliacaoFormTable from './AddAvaliacaoFormTable'

export interface SimplifiedPergunta {
  idPergunta: Pergunta['idPergunta']
  conteudoPergunta: Pergunta['conteudoPergunta']
  categoria: {
    idCategoria: Categoria['idCategoria']
  }
}

interface Props {
  idProjeto: number
  idPessoa: string
  idVinculoPessoa: string
  loading: boolean
  handleSubmit: (notas: NotaPerguntaInput[]) => Promise<void>
  screenMode: ScreenModeType
}

interface NotaInput {
  [key: string]: number
}

interface NotaTextualInput {
  [key: string]: string
}

const AddAvaliacaoForm: React.FC<Props> = ({
  idPessoa,
  idVinculoPessoa,
  idProjeto,
  loading: mutationLoading,
  handleSubmit,
  screenMode
}) => {
  const history = useHistory()
  const { data, loading, error } = useGetAddReviewPageDataQuery({
    variables: {
      idPessoa,
      idProjeto,
      idVinculoPessoa
    },

    onError () {
      openNotificationWithIcon(
        NotificationType.SUCCESS,
        'Ocorreu um erro ao buscar as informações.'
      )
    }
  })

  const { pessoa, projeto } = useMemo(() => {
    const { pessoa, projeto } = data || {}
    return {
      pessoa,
      projeto,
      perguntas: projeto && projeto.categoria && projeto.categoria.perguntas ? projeto.categoria.perguntas : []
    }
  }, [data])

  const perguntas: SimplifiedPergunta[] = useMemo(() => {
    const { projeto } = data || {}
    if (projeto) {
      return projeto.categoria?.perguntas || []
    }
    return []
  }, [data])

  const parseNotasInput = (notas: NotaInput): NotaPerguntaInput[] => {
    const initialValues: NotaPerguntaInput[] = []
    Object.entries(notas).forEach(notaEntry => {
      initialValues.push({
        idPergunta: Number(notaEntry[0]),
        nota: notaEntry[1]
      })
    })
    return initialValues
  }

  const parseTextualInput = (notas: NotaTextualInput): NotaPerguntaInput[] => {
    const initialValues: NotaPerguntaInput[] = []
    Object.entries(notas).forEach(reposta => {
      initialValues.push({
        idPergunta: Number(reposta[0]),
        respostaTextual: reposta[1]
      })
    })
    return initialValues
  }

  const handleFinish = data => {
    const notasNumericasToMutationInput = parseNotasInput(data.numerico)
    const respostaTextuaisMutationInput = parseTextualInput(data.textual)
    if (handleSubmit) {
      handleSubmit([
        ...notasNumericasToMutationInput,
        ...respostaTextuaisMutationInput
      ])
    }
  }

  if (loading) {
    return <FullSpinner />
  }

  if (error) {
    return <Button>Voltar</Button>
  }

  return (
    <>
      {pessoa && projeto && perguntas && (
        <>
          <PageHeader
            title={projeto.tituloProjeto}
            onBack={() => history.goBack()}
          />

          <Form
            name="basic"
            layout="vertical"
            onFinish={handleFinish}
          >
            <AddAvaliacaoFormTable
              nomeCategoria={projeto.categoria.nomeCategoria}
              perguntas={perguntas.slice(0, perguntas.length - 1)}
              screenMode={screenMode}
            >
              { screenMode === 'GERENTE' && (
                <UserInfo key="user" user={pessoa} avatarStyle={{ marginRight: 10 }} />
              )}
            </AddAvaliacaoFormTable>

            <div style={{ marginTop: '1rem' }}>
              {perguntas.slice(-1).map(pergunta => {
                return (
                  <Form.Item
                    label={pergunta.conteudoPergunta}
                    style={{ margin: 0 }}
                    name={['textual', `${pergunta.idPergunta}`]}
                    key={`${pergunta.idPergunta}`}
                  >
                    <Input.TextArea />
                  </Form.Item>
                )
              })}
            </div>

            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
                padding: '2rem 0'
              }}
            >
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loading && mutationLoading}
                  disabled={loading && mutationLoading}
                >
                  Enviar
                </Button>
              </Form.Item>
            </div>
          </Form>
        </>
      )}
    </>
  )
}

export default AddAvaliacaoForm
