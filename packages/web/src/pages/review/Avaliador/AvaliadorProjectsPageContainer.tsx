import React from 'react'

import RoundedDiv from '../../../components/common/RoundedDiv'

const AvaliadorProjectsPageContainer: React.FC = () => {
  return (
    <RoundedDiv shadow style={{ height: '100%' }}>
      <h1>Projetos</h1>
    </RoundedDiv>
  )
}

export default AvaliadorProjectsPageContainer
