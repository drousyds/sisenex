import { ExclamationCircleOutlined } from '@ant-design/icons'
import { Modal } from 'antd'
import React from 'react'
import { useHistory, useParams } from 'react-router-dom'

import RoundedDiv from '../../../components/common/RoundedDiv'
import {
  NotaPerguntaInput,
  useAvaliarProjetoMutation,
  useUpdateProjectMutation,
} from '../../../generated'
import { openNotificationWithIcon } from '../../../helpers'
import useAuth from '../../../hooks/useAuth'
import { NotificationType } from '../../../types'
import AddAvaliacaoForm from '../AddAvaliacaoForm'

const { confirm } = Modal

export interface AvaliacaoInformationPageParams {
  idProjeto: string
}

const AddReviewAvaliador: React.FC = () => {
  const { authState } = useAuth()
  const { idProjeto: idProjetoStr } = useParams<
    AvaliacaoInformationPageParams
  >()
  const idProjeto = Number(idProjetoStr)

  const [updateProjectMutation] = useUpdateProjectMutation();
  const estado = 'AVALIADO'

  const history = useHistory()

  const [mutate, { loading }] = useAvaliarProjetoMutation({
    onError() {
      openNotificationWithIcon(
        NotificationType.ERROR,
        'Ocorreu um erro ao fazer a avaliação'
      )
    },

    onCompleted() {
      openNotificationWithIcon(
        NotificationType.SUCCESS,
        'Avaliação feita com sucesso.'
      )
      history.goBack()
    }
  })

  const handleSubmit = async (notas: NotaPerguntaInput[]) => {
    if (idProjeto > 0 && authState.currentUser) {
      confirm({
        title: 'Enviar Avaliação',
        content: 'Você deseja enviar a avaliação?',
        icon: <ExclamationCircleOutlined />,
        async onOk() {
          await mutate({
            variables: {
              input: {
                avaliacao: {
                  idPessoa: authState.currentUser!.idPessoa,
                  idVinculoPessoa: authState.currentUser!.idVinculoPessoa,
                  idProjeto,
                  notas
                }
              }
            }
          })
          await updateProjectMutation({
            variables: {
              input: {
                idProjeto,
                statusProjeto: estado
              }
            }
          })
        }
      })
    }
  }

  return (
    <RoundedDiv shadow style={{ height: '100%' }}>
      {idProjeto > 0 && authState.currentUser && (
        <AddAvaliacaoForm
          loading={loading}
          idPessoa={authState.currentUser.idPessoa}
          idVinculoPessoa={authState.currentUser.idVinculoPessoa}
          idProjeto={idProjeto}
          handleSubmit={handleSubmit}
          screenMode="AVALIADOR"
        />
      )}
    </RoundedDiv>
  )
}

export default AddReviewAvaliador
