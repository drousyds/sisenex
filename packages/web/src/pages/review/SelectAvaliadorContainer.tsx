import { Select, Form } from 'antd'
import React, { useMemo } from 'react'

import { FETCH_USERS_ERROR_MESSAGE } from '../../constants/messages'
import { Pessoa, useGetReviewersQuery } from '../../generated'
import { openNotificationWithIcon } from '../../helpers'

const SelectAvaliadorContainer: React.FC = () => {
  // Query Hook
  const { data, loading } = useGetReviewersQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon('error', FETCH_USERS_ERROR_MESSAGE, 5.5)
    }
  })

  const users: Pessoa[] = useMemo(() => {
    const { avaliadores } = data || {}
    return avaliadores || []
  }, [data])

  return (
    <Form.Item
      label="Avaliadores"
      name="idsPessoa"
      rules={[
        { required: true, message: 'Você precisa selecionar algum avaliador' }
      ]}
      style={{ width: '80%' }}
    >
      <Select
        disabled={loading}
        loading={loading}
        showSearch
        optionFilterProp="children"
        placeholder="Selecionar Avaliador"
      >
        {users.map(pessoa => {
          const text = `Matrícula: ${pessoa.matriculaPessoa} - Nome: ${pessoa.nomePessoa}`
          const usersId = `${pessoa.idPessoa}-${pessoa.idVinculoPessoa}`
          return (
            <Select.Option key={usersId} value={usersId}>
              {text}
            </Select.Option>
          )
        })}
      </Select>
    </Form.Item>
  )
}
export default SelectAvaliadorContainer
