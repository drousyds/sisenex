import { Table, Form, InputNumber, Typography } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import React from 'react'

import { ScreenModeType } from '../../contexts/AuthContext'
import { SimplifiedPergunta } from './AddAvaliacaoForm'

interface Props {
  perguntas: SimplifiedPergunta[]
  nomeCategoria: string
  screenMode: ScreenModeType
}

interface PerguntaWithIndex extends SimplifiedPergunta {
  index: number
}

const AddAvaliacaoFormTable: React.FC<Props> = ({
  perguntas,
  nomeCategoria,
  ...props
}) => {
  const columns: ColumnProps<PerguntaWithIndex>[] = [
    {
      title: 'Nº',
      key: 'pergunta-n',
      render (_, { index }) {
        return <span>{`${index}º`}</span>
      }
    },
    {
      title: 'Conteúdo',
      dataIndex: 'conteudoPergunta'
    },
    {
      title: 'Nota',
      key: 'nota-input',
      align: 'center',
      render (_, pergunta) {
        return (
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Form.Item
              style={{ margin: 0 }}
              name={['numerico', `${pergunta.idPergunta}`]}
              key={`${pergunta.idPergunta}`}
              rules={[
                {
                  required: true,
                  message: 'A pergunta está sem nota',
                  type: 'number'
                }
              ]}
            >
              <InputNumber step={0.1} min={1} max={10} decimalSeparator="," />
            </Form.Item>
          </div>
        )
      }
    }
  ]
  return (
    <Table
      columns={columns}
      dataSource={perguntas.sort((a, b )=> {return a.idPergunta - b.idPergunta}).map((pergunta, index) => ({
        ...pergunta,
        index: index + 1
      }))}
      pagination={false}
      rowKey={({ idPergunta, categoria }) =>
        `${categoria.idCategoria}-${idPergunta}`
      }
      title={() => (
        <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
          <div>
            <Typography.Title level={4}>{props.screenMode === 'AVALIADOR'
              ? 'Avaliação dos Discentes' : 'Preencha com as notas fornecidas pelo avaliador na folha'}</Typography.Title>
            <Typography.Text>{`Perguntas da modalidade ${nomeCategoria}`}</Typography.Text>
          </div>
          {props.children}
        </div>
      )}
      bordered
    />
  )
}

export default AddAvaliacaoFormTable
