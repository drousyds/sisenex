import { Select, Form } from 'antd'
import React, { useMemo } from 'react'

import { FETCH_PROJECTS_ERROR_MESSAGE } from '../../constants/messages'
import { Projeto, useProjectsPageContainerQuery } from '../../generated'
import { openNotificationWithIcon } from '../../helpers'

const SelectProjectContainer: React.FC = () => {
  // Query Hook
  const { data, loading } = useProjectsPageContainerQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon('error', FETCH_PROJECTS_ERROR_MESSAGE, 5.5)
    }
  })

  const projetos: Projeto[] = useMemo(() => {
    const { projetos } = data || {}
    return projetos || []
  }, [data])

  return (
    <Form.Item
      label="Projetos"
      name="idProjeto"
      rules={[
        { required: true, message: 'Você precisa selecionar algum projeto' }
      ]}
      style={{ width: '80%' }}
    >
      <Select
        disabled={loading}
        loading={loading}
        showSearch
        optionFilterProp="children"
        placeholder="Selecionar Projeto"
      >
        {projetos.map(projeto => {
          const text = `Código: ${projeto.codigoProjeto} - Titulo: ${projeto.tituloProjeto}`
          return (
            <Select.Option key={projeto.idProjeto} value={projeto.idProjeto}>
              {text}
            </Select.Option>
          )
        })}
      </Select>
    </Form.Item>
  )
}

export default SelectProjectContainer
