import { Button, Form, Typography } from 'antd'
import React from 'react'

import RoundedDiv from '../../../components/common/RoundedDiv'
import SelectAvaliadorContainer from '../SelectAvaliadorContainer'
import SelectProjectContainer from '../SelectProjectContainer'
import { AvaliacaoInformation } from './SelectAvaliacaoInformationPage'

interface Props {
  handleSetAvaliacaoInformation
}

interface FormData {
  idProjeto: number
  idsPessoa: string
}

const SelectAvaliacaoInformation: React.FC<Props> = ({
  handleSetAvaliacaoInformation
}) => {
  const onFinish = (data: FormData) => {
    const [idPessoa, idVinculoPessoa] = data.idsPessoa.split('-')
    const values: AvaliacaoInformation = {
      idProjeto: data.idProjeto,
      idPessoa,
      idVinculoPessoa
    }
    handleSetAvaliacaoInformation(values)
  }

  return (
    <Form
      name="basic"
      layout="vertical"
      onFinish={onFinish}
      style={{ padding: '5rem' }}
    >
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <RoundedDiv shadow style={{ width: '100%' }}>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Typography.Text
              strong
              style={{
                fontFamily: 'Roboto',
                fontSize: '20px',
                color: '#707070'
              }}
            >
              Insira os dados referente ao projeto avaliado
            </Typography.Text>
          </div>
          <div style={{ marginTop: '2rem', marginLeft: '7rem' }}>
            <SelectProjectContainer />
            <SelectAvaliadorContainer />
          </div>
        </RoundedDiv>
      </div>
      <div
        style={{ display: 'flex', justifyContent: 'center', marginTop: '2rem' }}
      >
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Próximo
          </Button>
        </Form.Item>
      </div>
    </Form>
  )
}

export default SelectAvaliacaoInformation
