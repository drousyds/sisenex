import { ExclamationCircleOutlined } from '@ant-design/icons'
import { Modal } from 'antd'
import React from 'react'
import { useHistory, useParams } from 'react-router-dom'

import RoundedDiv from '../../../components/common/RoundedDiv'
import {
  NotaPerguntaInput,
  useAvaliarProjetoPorUmAvaliadorMutation
} from '../../../generated'
import { openNotificationWithIcon } from '../../../helpers'
import useAuth from '../../../hooks/useAuth'
import { NotificationType } from '../../../types'
import AddAvaliacaoForm from '../AddAvaliacaoForm'

const { confirm } = Modal

export interface AvaliacaoInformationPageParams {
  idPessoa: string
  idVinculoPessoa: string
  idProjeto: string
}

const AddAvaliacaoGerente: React.FC = () => {
  const { authState } = useAuth()
  const { idProjeto: idProjetoStr, idPessoa, idVinculoPessoa } = useParams<
    AvaliacaoInformationPageParams
  >()
  const idProjeto = Number(idProjetoStr)

  const history = useHistory()

  const [mutate, { loading }] = useAvaliarProjetoPorUmAvaliadorMutation({
    onError() {
      openNotificationWithIcon(
        NotificationType.ERROR,
        'Ocorreu um erro ao fazer a avaliação'
      )
    },

    onCompleted() {
      openNotificationWithIcon(
        NotificationType.SUCCESS,
        'Avaliação feita com sucesso.'
      )
      history.goBack()
    }
  })

  const handleSubmit = async (notas: NotaPerguntaInput[]) => {
    if (idProjeto > 0 && idPessoa && idVinculoPessoa && authState.currentUser) {
      confirm({
        title: 'Enviar Avaliação',
        content: 'Você deseja enviar a avaliação?',
        icon: <ExclamationCircleOutlined />,
        async onOk() {
          await mutate({
            variables: {
              input: {
                idPessoaGerente: authState.currentUser!.idPessoa,
                idVinculoPessoaGerente: authState.currentUser!.idVinculoPessoa,
                nomeAvaliadorPapel: 'PLACEHOLDER_TO_REMOVE_LATER',
                avaliacao: {
                  idPessoa,
                  idVinculoPessoa,
                  idProjeto,
                  notas
                }
              }
            }
          })
        }
      })
    }
  }

  return (
    <RoundedDiv shadow style={{ height: '100%' }}>
      {idProjeto > 0 && idPessoa && idVinculoPessoa && (
        <AddAvaliacaoForm
          loading={loading}
          idPessoa={idPessoa}
          idVinculoPessoa={idVinculoPessoa}
          idProjeto={idProjeto}
          handleSubmit={handleSubmit}
          screenMode="GERENTE"
        />
      )}
    </RoundedDiv>
  )
}

export default AddAvaliacaoGerente
