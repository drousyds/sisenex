import React from 'react'
import { useHistory } from 'react-router-dom'

import RoundedDiv from '../../../components/common/RoundedDiv'
import SelectAvaliacaoInformation from './SelectAvaliacaoInformation'

export interface AvaliacaoInformation {
  idPessoa: string
  idVinculoPessoa: string
  idProjeto: number
}

const SelectAvaliacaoInformationPage: React.FC = () => {
  const history = useHistory()

  const handleSetAvaliacaoInformation = (values: AvaliacaoInformation) => {
    const { idPessoa, idProjeto, idVinculoPessoa } = values
    const url = `/gerente/avaliacao/${idProjeto}/${idPessoa}/${idVinculoPessoa}`
    history.push(url)
  }

  return (
    <RoundedDiv shadow style={{ height: '100%' }}>
      <SelectAvaliacaoInformation
        handleSetAvaliacaoInformation={handleSetAvaliacaoInformation} />
    </RoundedDiv>
  )
}

export default SelectAvaliacaoInformationPage
