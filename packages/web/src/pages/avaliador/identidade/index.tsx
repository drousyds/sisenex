import { Typography } from 'antd'
import React, { useEffect, useRef } from 'react'

import RoundedDiv from '../../../components/common/RoundedDiv'
import { Card } from 'antd'
import QRCode from 'qrcode/build/qrcode'
import useAuth from '../../../hooks/useAuth'

const { Title } = Typography
const { Meta } = Card

const AvaliadorIdentidadePage: React.FC = () => {
  const {
    authState: { currentUser }
  } = useAuth()

  const canvasRef = useRef(null)
  const PADDING = "62C693BA9F4952D869C6C7D37"

  const generateQR = () => {
    let str = `${PADDING}-${currentUser?.idPessoa}-${currentUser?.idVinculoPessoa}`
    QRCode.toCanvas(
      canvasRef.current,
      str,
      {
        width: 380,
        height: 380,
        scale: 8,
        margin: 2
      },
      (error: any) => {
        if (error) console.error(error)
      }
    )
  }

  useEffect(() => {
    generateQR()
  })

  return (
    <RoundedDiv
      shadow
      style={{
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
      }}
    >
      <div style={{ flex: 0.5 }}>
        <Title level={3}>
          {`Apresente as informações abaixo para o monitor da sala no momento da avaliação.`}
        </Title>
      </div>
      <Card
        cover={
          <div style={{padding: 5}}>
          <canvas
            ref={canvasRef}
            id="canvas"
            style={{ height: 380, width: 380 }}
          />
          </div>
        }
      >
        <Meta
          title={`${currentUser?.nomePessoa}`}
          description={`ID: ${currentUser?.idPessoa}-${currentUser?.idVinculoPessoa}`}
        />
      </Card>
    </RoundedDiv>
  )
}

export default AvaliadorIdentidadePage
