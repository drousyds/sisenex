import { ReloadOutlined } from '@ant-design/icons'
import { Button, PageHeader, Table } from 'antd'
import React, { useMemo } from 'react'

import RoundedDiv from '../../../components/common/RoundedDiv'
import { useAvaliacoesAvaliadorQuery } from '../../../generated'
import useAuth from '../../../hooks/useAuth'

const AvaliadorAvaliacoesPage: React.FC = () => {
  const { authState: { currentUser } } = useAuth()
  const { data, loading, refetch } = useAvaliacoesAvaliadorQuery({
    fetchPolicy: 'no-cache',
    variables: {
      idPessoa: currentUser?.idPessoa!,
      idVinculoPessoa: currentUser?.idVinculoPessoa!
    }
  })

  const avaliacoes = useMemo(() => {
    const { pessoa } = data || {}
    if (pessoa && (pessoa.__typename === 'Avaliador' || pessoa.__typename === 'Gerente')) {
      if (!pessoa.avaliacoes) return []
      return pessoa.avaliacoes
    }
    return []
  }, [data])

  return (
    <RoundedDiv shadow style={{ height: '100%' }}>
      <PageHeader
        title="Avaliações"
        extra={[
          <Button
            key="refresh-page"
            onClick={() => {
              refetch()
            }}
            icon={<ReloadOutlined />}
            loading={loading}
          >
            Atualizar
          </Button>
        ]}
      />

      <Table
        loading={loading}
        rowKey={(a) => `${a.projeto.idProjeto}`}
        dataSource={avaliacoes}
        columns={[
          { title: 'Projeto', dataIndex: ['projeto', 'tituloProjeto'] },
          {
            title: 'Nota (Média)',
            key: 'media',
            render (_, a) { return a.media.toFixed(2) }
          }
        ]}
      />
    </RoundedDiv>
  )
}

export default AvaliadorAvaliacoesPage
