import { Result, Button } from 'antd'
import React from 'react'
import { Redirect, useHistory, useLocation } from 'react-router-dom'

import RoundedDiv from '../../components/common/RoundedDiv'

interface LocationState {
  error: boolean
    message: {
      title: string
      subtitle?: string
    }
}

const LoginErrorPage = () => {
  const history = useHistory()
  const { state } = useLocation<LocationState>()

  if (state && state.error) {
    return (
      <RoundedDiv shadow>
        <Result
          status="error"
          title={state.message.title}
          subTitle={state.message.subtitle || null}
          extra={[
            <Button key="exit-button" onClick={() => history.push('/')}>
                Voltar ao Início
            </Button>
          ]}
        />
      </RoundedDiv>
    )
  }
  return <Redirect to="/" />
}

export default LoginErrorPage
