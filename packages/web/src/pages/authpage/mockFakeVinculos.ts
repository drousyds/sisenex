import { Pessoa } from '../../generated'

export function mockFakeVinculo (): Pessoa {
  return {
    idPessoa: '52',
    idVinculoPessoa: '52',
    nomePessoa: 'VINICIUS MISAEL',
    nomeSocialPessoa: 'VINICIUS MISAEL',
    telefonePessoa: '83999999999',
    emailPessoa: 'email@gmail.com',
    lotacaoPessoa: null,
    matriculaPessoa: '20170000000',
    aptidaoPessoa: 0,
    monitorPessoa: 0,
    avaliadorPessoa: 1,
    gerentePessoa: 1
  }
}

export function mockFakeVinculos (): Pessoa[] {
  return [
    {
      ...mockFakeVinculo(),
      idPessoa: '111111',
      idVinculoPessoa: '1111111',
      matriculaPessoa: '3472384',
      gerentePessoa: 0,
      monitorPessoa: 0,
      avaliadorPessoa: 1
    },
    {
      ...mockFakeVinculo(),
      idPessoa: '2222',
      idVinculoPessoa: '22222',
      matriculaPessoa: '42423424',
      gerentePessoa: 0,
      monitorPessoa: 1,
      avaliadorPessoa: 0
    }
  ]
}
