import { CheckCircleOutlined } from '@ant-design/icons'
import { Result, Button, Typography } from 'antd'
import React from 'react'
import { Redirect, useLocation, useHistory } from 'react-router-dom'

import { VerticalFade } from '../../components/animation'
import RoundedDiv from '../../components/common/RoundedDiv'
import { Pessoa } from '../../generated'

const { Paragraph, Text } = Typography

const SuccessSignupPage: React.FC = () => {
  const location = useLocation<{ user: Pessoa }>()
  const history = useHistory()

  if (location.state && location.state.user) {
    const user = location.state.user as Pessoa
    return (
      <VerticalFade direction="up" amount={20}>
        <RoundedDiv>
          <Result
            status="success"
            title="Usuário Cadastrado com successo!"
            subTitle={`${user.nomePessoa} você foi cadastrado no sistema.`}
            extra={[
              <Button
                type="primary"
                key="console"
                onClick={() => history.push('/')}
              >
                Voltar
              </Button>
            ]}
          >
            <div className="desc">
              <Paragraph>
                <Text
                  strong
                  style={{
                    fontSize: 16
                  }}
                >
                  Informações:
                </Text>
              </Paragraph>
              <Paragraph>
                <CheckCircleOutlined
                  style={{ color: 'green', marginRight: 10 }}
                />
                Se você for um gerente. Contate a coordenação para liberar o seu
                acesso ao módulo.
              </Paragraph>
            </div>
          </Result>
        </RoundedDiv>
      </VerticalFade>
    )
  }
  return <Redirect to="/" />
}

export default SuccessSignupPage
