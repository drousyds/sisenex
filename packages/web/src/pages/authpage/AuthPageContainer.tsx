import queryString from 'query-string'
import React, { useCallback, useEffect } from 'react'
import { useLocation, useHistory } from 'react-router-dom'

import RoundedDiv from '../../components/common/RoundedDiv'
import { openNotificationWithIcon } from '../../helpers'
import useAuth from '../../hooks/useAuth'
import { useLogger } from '../../hooks/useLogger'

function getTokenFromURLQueryString (string: string) {
  const parsed = queryString.parse(string)
  const { code } = parsed || {}
  return `${code}`
}

const AuthPageContainer: React.FC = () => {
  const location = useLocation()
  const history = useHistory()
  const logger = useLogger('AuthPageContainer')
  const { authPageLogin } = useAuth()

  const gotError = useCallback((message = 'Ocorreu um erro ao fazer o login. Tente novamente.') => {
    openNotificationWithIcon(
      'error',
      message
    )
    history.push('/')
  }, [history])

  const getCodeFromURL = useCallback(() => {
    const code = getTokenFromURLQueryString(location.search)
    return code || null
  }, [location.search])

  useEffect(() => {
    const oAuthRegister = async () => {
      try {
        const code = getCodeFromURL()

        if (!code) {
          logger.error('Query String is empty')
          gotError()
          return
        }

        await authPageLogin(code)
      } catch (error) {
        console.log('DEU ERRO AQUI, ', error)
        gotError()
      }
    }

    oAuthRegister()
  }, [gotError, getCodeFromURL, logger, authPageLogin])

  return (
    <RoundedDiv
      style={{
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    />
  )
}

export default AuthPageContainer
