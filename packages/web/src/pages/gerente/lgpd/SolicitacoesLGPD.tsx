import React, { useMemo } from 'react'
import RoundedDiv from '../../../components/common/RoundedDiv'
import LGPDTable from '../../../components/lgpd/LGPDTable'
import { FETCH_MESSAGES } from '../../../constants/messages'
import { Solicitacao2, useGetSolicitacoesLgpdQuery } from '../../../generated'
import { openNotificationWithIcon } from '../../../helpers'
import {
  ReportsFilterType,
  SolicitacoesFilterType,
  useFilteredSolicitacoes,
  useFilters
} from '../../../hooks'
import { NotificationType } from '../../../types'
import SolicitacoesLGPDHeader from './SolicitacoesLGPDHeader'

export interface SolicitacaoTableItem {
  idSolicitacao: Solicitacao2['idSolicitacao']
  tipoSolicitacao: Solicitacao2['tipoSolicitacao']
  nomeSolicitacao: Solicitacao2['nomeSolicitacao']
  sobrenomeSolicitacao: Solicitacao2['sobrenomeSolicitacao']
  emailPessoa: Solicitacao2['emailPessoa']
  textoSolicitacao?: Solicitacao2['textoSolicitacao']
  statusSolicitacao?: Solicitacao2['statusSolicitacao']
}

const SolicitacoesLGPD: React.FC = () => {
  const [filters, setFilters] = useFilters<SolicitacoesFilterType>({
    text: '',
    statusSolicitacao: undefined
  })

  //query
  const { data, loading, refetch } = useGetSolicitacoesLgpdQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        FETCH_MESSAGES.ERROR.REQUESTS,
        5.5
      )
    }
  })

  const solicitacoesLGPD: SolicitacaoTableItem[] = useMemo(() => {
    const { solicitacoesLGPD } = data || {}
    return solicitacoesLGPD || []
  }, [data])

  const filteredSolicitacoes = useFilteredSolicitacoes({
    solicitacoesLGPD,
    filters
  })
  console.log(filters)

  return (
    <div>
      <SolicitacoesLGPDHeader
        filters={filters}
        setFilters={setFilters}
        loading={loading}
        handleRefetch={() => refetch()}
      />
      <LGPDTable
        solicitacoesLGPD={filteredSolicitacoes}
        loading={loading}
        handleRefetch={() => refetch()}
        tableFooter={
          <div>{`Solicitacoes: ${filteredSolicitacoes.length} / ${solicitacoesLGPD.length}`}</div>
        }
      />
    </div>
  )
}

export default SolicitacoesLGPD
