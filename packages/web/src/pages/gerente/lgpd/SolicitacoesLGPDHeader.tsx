import { ReloadOutlined } from '@ant-design/icons'
import { Input, Tooltip, Button, Select } from 'antd'
import React from 'react'
import { VerticalFade } from '../../../components/animation'
import RoundedDiv from '../../../components/common/RoundedDiv'
import { SolicitacoesFilterType } from '../../../hooks'

interface Props {
  loading: boolean
  handleRefetch: () => void
  filters: SolicitacoesFilterType
  setFilters: (e: SolicitacoesFilterType) => void
}
const SolicitacoesLGPDHeader: React.FC<Props> = ({
  handleRefetch,
  loading,
  filters,
  setFilters
}) => {
  return (
    <VerticalFade direction="down" amount={20}>
      <RoundedDiv shadow>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <Input.Search
            style={{ marginRight: 10 }}
            placeholder="Pesquisar..."
            onChange={e => {
              if (!e.target.value) {
                setFilters({ ...filters, text: '' })
              }
            }}
            onSearch={value => {
              setFilters({ ...filters, text: value })
            }}
          />
          <Tooltip title="Atualizar Página">
            <Button
              onClick={() => {
                handleRefetch()
              }}
              icon={<ReloadOutlined />}
              loading={loading}
              style={{ maxWidth: 248, float: 'right' }}
            >
              Atualizar
            </Button>
          </Tooltip>
        </div>
        <div
          style={{
            display: 'grid',
            gridTemplateColumns: 'repeat(auto-fill, minmax(250px, 1fr))',
            justifyContent: 'start',
            marginTop: 10
          }}
        >
          <div>
            <Select
              placeholder="Estado"
              onChange={value => {
                let s: string | undefined
                if (value) {
                  s = value.toString()
                }
                setFilters({ ...filters, statusSolicitacao: s })
              }}
              allowClear
              style={{ width: '100%' }}
            >
              <Select.Option key="report-filter-open" value="ABERTA">
                Aberta
              </Select.Option>
              <Select.Option key="report-filter-respondida" value="RESPONDIDA">
                Respondida
              </Select.Option>
              <Select.Option key="report-filter-arquivada" value="ARQUIVADA">
                Arquivada
              </Select.Option>
              <Select.Option key="report-filter-solved" value="RESOLVIDA">
                Resolvida
              </Select.Option>
            </Select>
          </div>
        </div>
      </RoundedDiv>
    </VerticalFade>
  )
}

export default SolicitacoesLGPDHeader
