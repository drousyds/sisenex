import React, { useMemo } from 'react'

import ReportsTable from '../../../components/report/ReportsTable'
import { FETCH_MESSAGES } from '../../../constants/messages'
import {
  useGetReportsQuery,
  Report,
  Pessoa,
  Apresentacao
} from '../../../generated'
import { openNotificationWithIcon } from '../../../helpers'
import {
  useFilters,
  ReportsFilterType,
  useFilteredReports
} from '../../../hooks'
import { NotificationType } from '../../../types'
import ReportPageHeader from './ReportPageHeader'

export interface ReportTableItem {
  idReport: Report['idReport']
  conteudoReport: Report['conteudoReport']
  emailReport: Report['emailReport']
  dataCriacao?: Report['dataCriacao']
  statusReport: Report['statusReport']
  avaliador: {
    nomePessoa?: Pessoa['nomePessoa']
  }
  apresentacao?: {
    salaApresentacao?: Apresentacao['salaApresentacao'] | null
  } | null
}

const ReportPageContainer: React.FC = () => {
  const [filters, setFilters] = useFilters<ReportsFilterType>({
    text: '',
    statusReport: undefined
  })

  // query
  const { data, loading, refetch } = useGetReportsQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        FETCH_MESSAGES.ERROR.REPORTS,
        5.5
      )
    }
  })

  const reports: ReportTableItem[] = useMemo(() => {
    const { reports } = data || {}
    return reports || []
  }, [data])

  const filteredReports = useFilteredReports({ reports, filters })

  console.log(filters)

  return (
    <div>
      <ReportPageHeader
        filters={filters}
        setFilters={setFilters}
        loading={loading}
        handleRefetch={() => refetch()}
      />
      <ReportsTable
        reports={filteredReports}
        loading={loading}
        handleRefetch={() => refetch()}
        tableFooter={
          <div>{`Reports: ${filteredReports.length} / ${reports.length}`}</div>
        }
      />
    </div>
  )
}

export default ReportPageContainer
