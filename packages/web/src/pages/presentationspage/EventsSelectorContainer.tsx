import { RightOutlined } from '@ant-design/icons'
import { Tooltip, Button } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'

import { EventsTable } from '../../components/events'
import { useGetEventsQuery, Evento } from '../../generated'
import { openNotificationWithIcon, getCampusNumber } from '../../helpers'
import { toMoment } from '../../helpers/dates'
import { EventsFilterType, useFilteredEvents } from '../../hooks'
import EventsListPageHeader from '../eventspage/events/EventsListPageHeader'

const EventsSelectorContainer: React.FC = () => {
  const history = useHistory()
  const [filters, setFilters] = useState<EventsFilterType>({
    text: '',
    campiIds: []
  })

  // Query Hook
  const { data, loading, refetch } = useGetEventsQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon(
        'error',
        'Ocorreu um erro ao buscar os eventos.',
        5.5
      )
    }
  })

  let events: Evento[] = []

  if (data && data.eventos) {
    events = data.eventos || []
  }
  const filteredEvents = useFilteredEvents({ events, filters })

  const columns: ColumnProps<Evento>[] = [
    {
      title: 'Nome',
      key: 'event-name',
      render: (_, { nomeEvento, idEvento }) => (
        <span
          style={{ cursor: 'pointer' }}
          onClick={() => history.push(`/apresentacoes/${idEvento}`)}
        >
          {nomeEvento}
        </span>
      )
    },
    {
      title: 'Local',
      key: 'localEvento',
      render: (_, event) => (
        <span>
          {event.campi.map(({ idCampus }, index) => (
            <span key={idCampus}>{`${getCampusNumber(idCampus)}${
              event.campi.length - 1 !== index ? ', ' : ''
            } `}</span>
          ))}
        </span>
      )
    },
    {
      title: 'Descrição',
      dataIndex: 'descricaoEvento'
    },
    {
      title: 'Início',
      key: 'dataInicio',
      render: (_, { dataInicioEvento }) => {
        if (dataInicioEvento) {
          const m = toMoment(dataInicioEvento)
          return (
            <span>
              {m ? `${m.format('dddd[,] D [de] MMMM [de] YYYY')}` : ' - '}
            </span>
          )
        }
        return <span> - </span>
      }
    },
    {
      title: 'Encerramento',
      key: 'dataFim',
      render: (_, { dataFimEvento }) => {
        if (dataFimEvento) {
          const m = toMoment(dataFimEvento)
          return (
            <span>
              {m ? `${m.format('dddd[,] D [de] MMMM [de] YYYY')}` : ' - '}
            </span>
          )
        }
        return <span> - </span>
      }
    },
    {
      title: 'Abrir',
      key: 'openEventoButton',
      align: 'center',
      render: (_, { idEvento }) => (
        <span style={{ margin: 10 }}>
          <Tooltip title="Abrir">
            <Button
              onClick={() => history.push(`/apresentacoes/${idEvento}`)}
              icon={<RightOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    }
  ]

  return (
    <div>
      <EventsListPageHeader
        handleRefetch={() => refetch()}
        loading={loading}
        filters={filters}
        setFilters={setFilters}
      />
      <EventsTable
        loading={loading}
        events={filteredEvents}
        columns={columns}
      />
    </div>
  )
}

export default EventsSelectorContainer
