import { ReloadOutlined, PlusOutlined } from '@ant-design/icons'
import { Input, Tooltip, Button, DatePicker } from 'antd'
import React from 'react'
import styled from 'styled-components'
import { Grid, Cell } from 'styled-css-grid'

import { VerticalFade } from '../../components/animation'
import RoundedDiv from '../../components/common/RoundedDiv'
import {
  ModeSelect,
  ThematicsSelect,
  PresentationStatusSelect
} from '../../components/form'
import CategorySelect from '../../components/form/CategorySelect'
import { getPresentationStatusNumber } from '../../helpers'
import { PresentationsFiltersType } from '../../hooks'

export interface Props {
  loading: boolean
  filters: PresentationsFiltersType
  setFilters: (e: PresentationsFiltersType) => void
}

const FilterSelectContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  gap: 1rem;
  margin-top: 1rem;
`

const AllPresentationsHeader: React.FC<Props> = ({
  loading,
  filters,
  setFilters,
  children
}) => {
  return (
    <VerticalFade direction="down" amount={20}>
      <RoundedDiv>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <Input.Search
            style={{ marginRight: 20 }}
            placeholder="Pesquisar..."
            onChange={e => {
              if (!e.target.value) {
                setFilters({ ...filters, text: '' })
              }
            }}
            onSearch={value => {
              setFilters({ ...filters, text: value })
            }}
          />
          {children}

        </div>
        <FilterSelectContainer>
          <div>
            <CategorySelect
              hasAllItems={true}
              style={{ width: '100%' }}
              allowClear
              loading={loading}
              disabled={loading}
              onChange={(idCategoria) => {
                setFilters({
                  ...filters,
                  idCategoria
                })
              }}
            />
          </div>
          <div>
            <ThematicsSelect
              loading={loading}
              style={{ width: '100%' }}
              handleChange={(value: number | undefined) => {
                setFilters({
                  ...filters,
                  idAreaTematica: value
                })
              }}
            />
          </div>
          <div>
            <ModeSelect
              handleChange={(value: string | undefined) => {
                setFilters({
                  ...filters,
                  modalidadeApresentacao: value
                })
              }}
              loading={loading}
              style={{ width: '100%' }}
            />
          </div>
          <div>
            <PresentationStatusSelect
              loading={loading}
              style={{ width: '100%' }}
              handleChange={(value: string | undefined) => {
                const type = getPresentationStatusNumber(value)
                setFilters({
                  ...filters,
                  disponibilidadeApresentacao: type
                })
              }}
            />
          </div>
        <div>
              <DatePicker 
                placeholder='Selecione o dia do evento'
                onChange={(date) => {
                  
                  setFilters({ ...filters, hora_Apresentacao: date?.format('YYYY-MM-DD') })
                }}
                
                style={{ width: '100%' }}
              />
            </div>
        </FilterSelectContainer>
      </RoundedDiv>
   
    </VerticalFade>
  )
}

export default AllPresentationsHeader
