import { Row, Col, Spin } from 'antd'
import React, { useMemo, useState } from 'react'

import { UsersCard, EventCard, PresentationsCard } from '../../components/cards'
import { NoEventResult } from '../../components/results'
import {
  useReportPageContainerQuery
} from '../../generated'
import { openNotificationWithIcon } from '../../helpers'
import Fields from './Fields'
import InfoPageHeader from './InfoPageHeader'

import './InfoPageContainer.css'

const InfoPageContainer: React.FC = () => {
  const [eventId, setEventId] = useState<number | undefined>(undefined)

  const { data, loading, refetch } = useReportPageContainerQuery({
    notifyOnNetworkStatusChange: true,
    skip: !eventId,
    fetchPolicy: 'network-only',
    variables: { idEvento: eventId! },
    onError: () => {
      openNotificationWithIcon(
        'error',
        'Ocorreu um erro ao buscar as informações.',
        5.5
      )
    }
  })

  const { event, users, presentations } = useMemo(() => {
    const { evento, pessoas } = data || {}
    const { apresentacoes } = evento || {}
    return {
      event: evento,
      users: pessoas || [],
      presentations: apresentacoes || []
    }
  }, [data])

  return (
    <div>
      <InfoPageHeader
        setEvent={(value: number | undefined) => setEventId(value)}
        eventId={eventId}
        loading={false}
        handleRefetch={() => refetch()}
      />
      <Spin spinning={loading} size="large">
        {!loading && eventId && data && event ? (
          <React.Fragment>
            <Row
              gutter={16}
              style={{
                marginTop: 10,
                height: '100%',
                display: 'flex',
                flexWrap: 'wrap'
              }}
            >
              <Col xs={24} sm={24} md={8}>
                <EventCard event={event} />
              </Col>
              <Col xs={24} sm={24} md={8} className="rep-col">
                <PresentationsCard presentations={presentations} />
              </Col>
              <Col xs={24} sm={24} md={8} className="rep-col">
                <UsersCard users={users} />
              </Col>
            </Row>
            <Fields event={event} />
          </React.Fragment>
        ) : (
          <NoEventResult />
        )}
      </Spin>
    </div>
  )
}

export default InfoPageContainer
