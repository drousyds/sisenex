import { ReloadOutlined } from '@ant-design/icons'
import { Row, Col, Tooltip, Button } from 'antd'
import React from 'react'

import RoundedDiv from '../../components/common/RoundedDiv'
import { SelectEvent } from '../../components/fields'

interface Props {
  setEvent: (value: number | undefined) => void
  eventId?: number
  loading: boolean
  handleRefetch: () => void
}

const InfoPageHeader: React.FC<Props> = ({
  setEvent,
  eventId,
  loading,
  handleRefetch
}) => {
  return (
    <RoundedDiv shadow style={{ marginTop: 10 }}>
      <Row justify="space-between">
        <Row gutter={8}>
          <Col>
            <SelectEvent allowClear={false} getValue={e => setEvent(e)} />
          </Col>
        </Row>
        <Col>
          <Tooltip title="Atualizar a página">
            <Button
              onClick={() => {
                handleRefetch()
              }}
              icon={<ReloadOutlined />}
              disabled={!eventId}
              loading={loading}
              style={{ maxWidth: 248, float: 'right' }}
            >
                Atualizar
            </Button>
          </Tooltip>
        </Col>
      </Row>
    </RoundedDiv>
  )
}

export default InfoPageHeader
