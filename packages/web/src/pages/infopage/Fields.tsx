
import { Row, Typography, Result } from 'antd'
import _ from 'lodash'
import React, { useMemo } from 'react'

import FieldCard from '../../components/cards/FieldCard'
import RoundedDiv from '../../components/common/RoundedDiv'
import { Evento, Projeto, Apresentacao } from '../../generated'

interface Props {
  event: Evento
}

const useGetProjetosFromPresentations = (
  presentations?: Apresentacao[] | null
) => {
  const projects = useMemo(
    () =>
      presentations
        ? presentations.reduce((acc: Projeto[], { projetos }) => {
          return projetos ? acc.concat(projetos) : acc
        }, [])
        : [],
    [presentations]
  )

  return useMemo(() => {
    return _.groupBy(
      projects,
      ({ areaTematica }) => areaTematica.nomeAreaTematica
    )
  }, [projects])
}

const Field: React.FC<Props> = ({ event }) => {
  const projects = useGetProjetosFromPresentations(event.apresentacoes)
  const keys = Object.keys(projects)
  return (
    <React.Fragment>
      <Row>
        <Typography.Title level={4} style={{ margin: '1rem 0' }}>
              Informações por Área Temática
        </Typography.Title>
      </Row>
      {keys.length > 0 ? (
        keys.map(e => {
          return <FieldCard key={e} areaTematica={e} projects={projects[e]} />
        })
      ) : (
        <RoundedDiv>
          <Result status="info" title="Esse evento não tem área temáticas." />
        </RoundedDiv>
      )}
    </React.Fragment>
  )
}

export default Field
