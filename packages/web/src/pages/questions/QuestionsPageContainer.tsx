import { Button } from 'antd'
import React from 'react'
import { useDispatch } from 'react-redux'

import RoundedDiv from '../../components/common/RoundedDiv'
// import { Pergunta } from '../../generated'
import { openCreateCategoryDialogAction } from '../../redux/ducks/dialogs/category/actions'
import {
  openCreateQuestionDialogAction
} from '../../redux/ducks/dialogs/questions/actions'
import QuestionsDialogsWrapper from './QuestionsDialogsWrapper'

const QuestionsPageContainer: React.FC = () => {
  const dispatch = useDispatch()

  return (
    <>
      <QuestionsDialogsWrapper />
      <RoundedDiv>
        <h1>Edite, organize ou crie perguntas</h1>
        <Button onClick={() => dispatch(openCreateQuestionDialogAction())}>
          Criar Pergunta
        </Button>
        <Button onClick={() => dispatch(openCreateCategoryDialogAction())}>
          Criar Categoria
        </Button>
        <Button
          // onClick={() =>
          //   dispatch(openUpdateQuestionDialogAction(FAKE_QUESTION))
          // }
        >
          Editar Pergunta
        </Button>
      </RoundedDiv>
    </>
  )
}

export default QuestionsPageContainer
