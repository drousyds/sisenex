import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import CreateCategoryDialog from '../../components/dialogs/category/CreateCategoryDialog'
import CreateQuestionDialog from '../../components/dialogs/questions/CreateQuestionDialog'
import UpdateQuestionDialog from '../../components/dialogs/questions/UpdateQuestionDialog'
import { closeCreateCategoryDialogAction } from '../../redux/ducks/dialogs/category/actions'
import {
  closeCreateQuestionDialogAction,
  closeUpdateQuestionDialogAction
} from '../../redux/ducks/dialogs/questions/actions'
import { AppStateType } from '../../types'

const QuestionsDialogsWrapper: React.FC = () => {
  const { questions, category } = useSelector(
    (state: AppStateType) => state.dialogs
  )

  const dispatch = useDispatch()

  return (
    <>
      <h1>Dialogs Wrapper</h1>
      {questions.create.open && (
        <CreateQuestionDialog
          open={questions.create.open}
          handleClose={() => {
            dispatch(closeCreateQuestionDialogAction())
          }}
        />
      )}
      {questions.update.open && questions.update.question && (
        <UpdateQuestionDialog
          open={questions.update.open}
          handleClose={() => dispatch(closeUpdateQuestionDialogAction())}
          question={questions.update.question}
        />
      )}
      {category.create.open && (
        <CreateCategoryDialog
          open={category.create.open}
          handleClose={() => {
            dispatch(closeCreateCategoryDialogAction())
          }}
        />
      )}
    </>
  )
}

export default QuestionsDialogsWrapper
