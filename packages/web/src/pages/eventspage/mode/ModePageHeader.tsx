import { LeftOutlined } from '@ant-design/icons'
import { Button, Typography, Row, Col } from 'antd'
import React from 'react'
import { useHistory } from 'react-router'

import { AnimatedHeader } from '../../../components/animation'
import RoundedDiv from '../../../components/common/RoundedDiv'

interface Props {
  idEvento: number
}

const ModePageHeader: React.FC<Props> = () => {
  const history = useHistory()

  return (
    <AnimatedHeader>
      <RoundedDiv shadow style={{ marginTop: 10, marginBottom: 10 }}>
        <Row justify="space-between" style={{ alignItems: 'center' }}>
          <Row gutter={8} style={{ alignItems: 'center' }}>
            <Col>
              <Button
                shape="circle"
                icon={<LeftOutlined />}
                onClick={() => history.push('/eventos')}
                style={{ marginRight: 10 }}
              />
            </Col>
            <Col>
              <Typography.Title level={4} style={{ margin: 0 }}>
                Modalidades
              </Typography.Title>
            </Col>
          </Row>
        </Row>
      </RoundedDiv>
    </AnimatedHeader>
  )
}
export default ModePageHeader
