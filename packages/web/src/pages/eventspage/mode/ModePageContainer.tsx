import { RightOutlined } from '@ant-design/icons'
import { Table, Tooltip, Button } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import React from 'react'
import { useHistory, useParams } from 'react-router'

import { AnimatedContent } from '../../../components/animation'
import RoundedDiv from '../../../components/common/RoundedDiv'
import ModePageHeader from './ModePageHeader'

const MODES = [
  {
    name: 'Tertúlias',
    key: 'tertulias'
  },
  { name: 'Performances', key: 'performances' }
]

const ModePageContainer: React.FC = () => {
  const history = useHistory()
  const params = useParams<{ idEvento: string }>()
  const idEvento = parseInt(params.idEvento, 10)

  const eventError = () => {
    history.push('/eventos')
  }
  if (!params.idEvento || !idEvento) {
    eventError()
  }

  const columns: ColumnProps<{ name: string; key: string }>[] = [
    {
      title: 'Nome',
      key: 'mode-name',
      render: (_, mode) => (
        <span
          style={{ cursor: 'pointer' }}
          onClick={() =>
            history.push(`/eventos/${params.idEvento}/${mode.key}`)
          }
        >
          {mode.name}
        </span>
      )
    },
    {
      title: 'Acessar',
      align: 'right',
      key: 'acces_event_type',
      render: (_, el) => (
        <span style={{ margin: 10 }}>
          <Tooltip title="Acessar">
            <Button
              onClick={() =>
                history.push(`/eventos/${params.idEvento}/${el.key}`)
              }
              icon={<RightOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    }
  ]
  return (
    <div>
      <ModePageHeader idEvento={idEvento} />
      <AnimatedContent>
        <RoundedDiv style={{ marginTop: 10 }}>
          <div style={{ overflowX: 'auto' }}>
            <Table dataSource={MODES} columns={columns} pagination={false} />
          </div>
        </RoundedDiv>
      </AnimatedContent>
    </div>
  )
}
export default ModePageContainer
