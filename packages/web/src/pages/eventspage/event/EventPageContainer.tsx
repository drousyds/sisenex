import React from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { openNotificationWithIcon } from '../../../helpers'
import { useGetEventQuery } from '../../../generated'
import { ThematicsTable } from '../../../components/thematics'
import EventPageHeader from './EventPageHeader'
import { FETCH_MESSAGES } from '../../../constants/messages'

const EventPageContainer: React.FC = () => {
  const history = useHistory()
  const params = useParams<{ idEvento: string }>()
  const idEvento = parseInt(params.idEvento, 10)

  const eventError = () => {
    history.push('/eventos')
  }
  if (!idEvento) {
    eventError()
  }

  const { data, loading } = useGetEventQuery({
    notifyOnNetworkStatusChange: true,
    variables: { idEvento },
    onError: () => {
      openNotificationWithIcon('error', FETCH_MESSAGES.ERROR.EVENT, 5.5)
      eventError()
    },
    onCompleted: a => {
      if (!a.evento) {
        openNotificationWithIcon('error', FETCH_MESSAGES.ERROR.EVENT, 5.5)
        eventError()
      }
    }
  })

  return (
    <div>
      {data && data.evento && (
        <React.Fragment>
          <EventPageHeader event={data.evento} />
          <ThematicsTable loading={loading} event={data.evento} />
        </React.Fragment>
      )}
    </div>
  )
}

export default EventPageContainer
