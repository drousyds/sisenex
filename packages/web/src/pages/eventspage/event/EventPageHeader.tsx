import { LeftOutlined } from '@ant-design/icons'
import { Button, Typography, Row, Col } from 'antd'
import React from 'react'
import { useHistory } from 'react-router'

import { AnimatedHeader } from '../../../components/animation'
import RoundedDiv from '../../../components/common/RoundedDiv'
// import { GenerateQRButton } from '../../../components/qr'
import {
  Evento
} from '../../../generated'

interface Props {
  event: Evento
}

const EventPageHeader: React.FC<Props> = ({ event }) => {
  // const { data, loading } = useGeneretaeQrPresentationsQuery({
  //   variables: {
  //     idEvento: event.idEvento,
  //     modalidadeApresentacao: 'T'
  //   },
  //   onError: () => {
  //     openNotificationWithIcon(
  //       'error',
  //       'Ocorreu um erro ao buscar as apresentações do evento.',
  //       5.5
  //     )
  //   }
  // })

  // let presentations: Apresentacao[] = []

  // if (data && data.apresentacoes) {
  //   presentations = data.apresentacoes || []
  // }
  const history = useHistory()
  return (
    <AnimatedHeader>
      <RoundedDiv
        shadow
        style={{
          marginTop: 10,
          marginBottom: 10
        }}
      >
        <Row justify="space-between">
          <Row gutter={8} style={{ alignItems: 'center' }}>
            <Col>
              <Button
                shape="circle"
                icon={<LeftOutlined />}
                onClick={() => history.push(`/eventos/${event.idEvento}`)}
                style={{ marginRight: 10 }}
              />
            </Col>
            <Col>
              <Typography.Title level={4} style={{ margin: 0 }}>
                {`${event.nomeEvento} - Áreas Temáticas`}
              </Typography.Title>
            </Col>
          </Row>
          {/* <Col>
            <GenerateQRButton data={presentations} loading={loading} />
          </Col> */}
        </Row>
      </RoundedDiv>
    </AnimatedHeader>
  )
}

export default EventPageHeader
