import React from 'react'
import { connect } from 'react-redux'
import { useHistory, useParams, useLocation } from 'react-router-dom'
import { Dispatch } from 'redux'

import { NoPresentationsResult } from '../../../components/events'
import { PresentationsHeader } from '../../../components/presentations'
import PresentationsTable from '../../../components/presentations/PresentationsTable'
import { FETCH_MESSAGES } from '../../../constants/messages'
import {
  Apresentacao,
  Projeto,
  useGetEventPresentationsQuery,
  useGetEventQuery,
  Evento,
  ApresentacoesInput
} from '../../../generated'
import { openNotificationWithIcon } from '../../../helpers'
import {
  useFilters,
  PresentationsFiltersType,
  useFilteredPresentations
} from '../../../hooks'
import { closeAllocationDialogAction } from '../../../redux/ducks/dialogs/events/actions'
import {
  openCreatePresentationDialogAction,
  closeCreatePresentationDialogAction,
  openUpdatePresentationDialogAction,
  closeUpdatePresentationDialogAction,
  openPresentationDetailsDialogAction,
  closePresentationDetailsDialogAction
} from '../../../redux/ducks/dialogs/presentations/actions'
import {
  openProjectDetailsDialogAction,
  closeProjectDetailsDialogAction,
  openUpdateProjectDialogAction,
  closeUpdateProjectDialogAction
} from '../../../redux/ducks/dialogs/projects/actions'
import { AppStateType, NotificationType } from '../../../types'

interface CProps {
  openProjectDetailsDialog: (project: Projeto) => void
  closeProjectDetailsDialog: () => void
  openCreatePresentationDialog: () => void
  closeCreatePresentationDialog: () => void
  openUpdatePresentationDialog: (presentation: Apresentacao) => void
  closeUpdatePresentationDialog: () => void
  openPresentationDetailsDialog: (presentation: Apresentacao) => void
  closePresentationDetailsDialog: () => void
  openUpdateProjectDialog: (project: Projeto) => void
  closeUpdateProjectDialog: () => void
  closeAllocationDialog: () => void
  projectDialogs: AppStateType['dialogs']['projectsDialog']
  presentationsDialog: AppStateType['dialogs']['presentationsDialog']
  eventsDialogs: AppStateType['dialogs']['events']
}

type Props = CProps

// Component
const EventsPresentationsPageContainer: React.FC<Props> = ({
  openProjectDetailsDialog,

  openCreatePresentationDialog,
  openUpdatePresentationDialog,
  openPresentationDetailsDialog,
  openUpdateProjectDialog
}) => {
  // router hooks
  const history = useHistory()
  const params = useParams<{ idEvento: string; idAreaTematica?: string }>()
  const location = useLocation()

  // filters hook
  const [filters, setFilters] = useFilters<PresentationsFiltersType>({
    text: '',
    disponibilidadeApresentacao: undefined
  })

  // isPerformance flag
  const isPeformance = location.pathname.includes('performances')

  // parses event id
  const idEvento = parseInt(params.idEvento, 10)
  if (!idEvento) {
    history.push('/eventos')
  }

  // get event query hook
  const { data: eventData, loading: eventLoading } = useGetEventQuery({
    notifyOnNetworkStatusChange: true,
    variables: { idEvento },
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        FETCH_MESSAGES.ERROR.EVENT,
        5.5
      )
    }
  })

  // base query variables
  const queryVariables: ApresentacoesInput = {
    idEvento,
    modalidadeApresentacao: 'P'
  }

  // define query variables
  if (!isPeformance && params.idAreaTematica) {
    const idAreaTematica = parseInt(params.idAreaTematica, 10)
    if (!idAreaTematica) {
      history.push(`/eventos/${idEvento}`)
    }
    queryVariables.idAreaTematica = idAreaTematica
    queryVariables.modalidadeApresentacao = 'T'
  }

  // Query Presentations Hooks
  const {
    data: presentationsData,
    loading: presentationsLoading,
    refetch
  } = useGetEventPresentationsQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    variables: { input: queryVariables },
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        FETCH_MESSAGES.ERROR.PRESENTATIONS,
        5.5
      )
    }
  })

  // Handle Query data
  let event: Evento | undefined
  let presentations: Apresentacao[] = []

  if (eventData && eventData.evento) {
    event = eventData.evento
  }

  if (presentationsData && presentationsData.apresentacoes) {
    presentations = presentationsData.apresentacoes
  }

  // Loading Flag
  const loading = eventLoading || presentationsLoading

  // Filtered Presentations Memo
  const filteredPresentations = useFilteredPresentations({
    filters,
    presentations
  })

  return (
    <div>

      <PresentationsHeader
        loading={loading}
        handleRefetch={() => refetch()}
        openCreatePresentationDialog={openCreatePresentationDialog}
        style={{ marginTop: 10 }}
        idEvento={idEvento}
        isPeformance={isPeformance}
        filters={filters}
        setFilters={setFilters}
        goBackUrl={`/eventos/${idEvento}/${!isPeformance ? 'tertulias' : ''}`}
      />
      {(presentations.length > 0 || isPeformance) && (
        <PresentationsTable
          loading={loading}
          presentations={filteredPresentations}
          openPresentationDetailsDialog={openPresentationDetailsDialog}
          openProjectDetailsDialog={openProjectDetailsDialog}
          openUpdatePresentationDialog={openUpdatePresentationDialog}
          openUpdateProjectDialog={openUpdateProjectDialog}
          handleRefetch={() => refetch()}
          queryVariables={queryVariables}
          tableFooter={
            <div>{`Apresentações: ${filteredPresentations.length} / ${presentations.length}`}</div>
          }
        />
      )}
      {!loading &&
        !isPeformance &&
        presentations.length === 0 &&
        params.idAreaTematica &&
        event && (
        <NoPresentationsResult
          event={event}
          idAreaTematica={params.idAreaTematica}
        />
      )}
    </div>
  )
}
const mapStateToProps = (state: AppStateType, ownProps: Props) => ({
  ...ownProps,
  projectDialogs: state.dialogs.projectsDialog,
  presentationsDialog: state.dialogs.presentationsDialog,
  eventsDialogs: state.dialogs.events
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  openProjectDetailsDialog: (project: Projeto) =>
    dispatch(openProjectDetailsDialogAction(project)),
  closeProjectDetailsDialog: () => dispatch(closeProjectDetailsDialogAction()),
  openCreatePresentationDialog: () =>
    dispatch(openCreatePresentationDialogAction()),
  closeCreatePresentationDialog: () =>
    dispatch(closeCreatePresentationDialogAction()),
  openUpdatePresentationDialog: (presentation: Apresentacao) =>
    dispatch(openUpdatePresentationDialogAction(presentation)),
  closeUpdatePresentationDialog: () =>
    dispatch(closeUpdatePresentationDialogAction()),
  openPresentationDetailsDialog: (presentation: Apresentacao) =>
    dispatch(openPresentationDetailsDialogAction(presentation)),
  closePresentationDetailsDialog: () =>
    dispatch(closePresentationDetailsDialogAction()),
  openUpdateProjectDialog: (project: Projeto) =>
    dispatch(openUpdateProjectDialogAction(project)),
  closeUpdateProjectDialog: () => dispatch(closeUpdateProjectDialogAction()),
  closeAllocationDialog: () => dispatch(closeAllocationDialogAction())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EventsPresentationsPageContainer)
