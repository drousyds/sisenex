import React from 'react'
import { useLocation } from 'react-router'
import { Breadcrumb } from 'antd'
import { Link } from 'react-router-dom'

const EventsBreadcumbs: React.FC = () => {
  const location = useLocation()
  const pathSnippets = location.pathname.split('/').filter(i => i)
  const extraBreadcrumbItems = pathSnippets.map((_, index) => {
    const url = `/${pathSnippets.slice(0, index + 1).join('/')}`
    return (
      <Breadcrumb.Item key={url}>
        <Link to={url}>
          {pathSnippets[index].charAt(0).toUpperCase() +
            pathSnippets[index].substr(1).toLowerCase()}
        </Link>
      </Breadcrumb.Item>
    )
  })
  const breadcrumbItems = [
    <Breadcrumb.Item key="home">
      <Link to="/">Início</Link>
    </Breadcrumb.Item>
  ].concat(extraBreadcrumbItems)

  return <Breadcrumb>{breadcrumbItems}</Breadcrumb>
}

export default EventsBreadcumbs
