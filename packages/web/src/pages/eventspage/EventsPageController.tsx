import React from 'react'
import { Switch, Route } from 'react-router-dom'
import EventsListPageContainer from './events/EventsListPageContainer'
import EventPageContainer from './event/EventPageContainer'
import EventPresentationsPageContainer from './event/EventPresentationsPageContainer'
import EventsBreadcrumbs from './EventsBreadcrumbs'
import ModePageContainer from './mode/ModePageContainer'

const EventsPageController: React.FC = () => {
  return (
    <div>
      <EventsBreadcrumbs />
      <Switch>
        <Route exact path="/eventos" component={EventsListPageContainer} />
        <Route exact path="/eventos/:idEvento" component={ModePageContainer} />
        <Route
          exact
          path="/eventos/:idEvento/tertulias/:idAreaTematica"
          component={EventPresentationsPageContainer}
        />
        <Route
          exact
          path="/eventos/:idEvento/performances"
          component={EventPresentationsPageContainer}
        />
        <Route
          exact
          path="/eventos/:idEvento/tertulias/"
          component={EventPageContainer}
        />
      </Switch>
    </div>
  )
}

export default EventsPageController
