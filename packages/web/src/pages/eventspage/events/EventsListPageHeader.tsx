import { PlusOutlined, ReloadOutlined } from '@ant-design/icons'
import { Button, Tooltip, Input, Select } from 'antd'
import React from 'react'
import { Grid, Cell } from 'styled-css-grid'

import { AnimatedHeader } from '../../../components/animation'
import RoundedDiv from '../../../components/common/RoundedDiv'
import { useGetCampiQuery, Campus } from '../../../generated'
import { openNotificationWithIcon } from '../../../helpers'
import { EventsFilterType } from '../../../hooks'

// css
import './EventsList.css'

interface Props {
  openCreateEventDialog?: () => void
  handleRefetch: () => void
  loading: boolean
  filters: EventsFilterType
  setFilters: React.Dispatch<React.SetStateAction<EventsFilterType>>
}

const EventsListPageHeader: React.FC<Props> = ({
  openCreateEventDialog,
  handleRefetch,
  loading,
  filters,
  setFilters
}) => {
  // Campi Query Hook
  const { data, loading: campiLoading } = useGetCampiQuery({
    notifyOnNetworkStatusChange: true,
    onError: () => {
      openNotificationWithIcon(
        'error',
        'Ocorreu um erro ao buscar os campus.',
        5.5
      )
    }
  })

  // Query data handler
  let campi: Campus[] = []
  if (data && data.campi) {
    campi = data.campi
  }

  return (
    <AnimatedHeader>
      <RoundedDiv shadow style={{ marginTop: 10 }}>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <Input.Search
            placeholder="Pequisar..."
            onChange={e => {
              if (!e.target.value) {
                setFilters({ ...filters, text: '' })
              }
            }}
            onSearch={value => {
              setFilters({ ...filters, text: value })
            }}
            style={{ marginRight: 10 }}
            disabled={loading}
          />

          {openCreateEventDialog && (
            <Tooltip title="Criar Evento">
              <Button
                icon={<PlusOutlined />}
                style={{ marginRight: 5 }}
                onClick={() => openCreateEventDialog()}
              >
                Criar Evento
              </Button>
            </Tooltip>
          )}
          <Tooltip title="Atualizar a página">
            <Button
              onClick={() => {
                handleRefetch()
              }}
              icon={<ReloadOutlined />}
              loading={loading}
              style={{ maxWidth: 248, float: 'right' }}
            >
              Atualizar
            </Button>
          </Tooltip>
        </div>
        <Grid
          columns="repeat(auto-fill, minmax(250px, 1fr))"
          coljustifyContent="start"
          style={{ marginTop: 10 }}
        >
          <Cell>
            <Select
              defaultValue={undefined}
              placeholder="Filtrar por campus..."
              mode="multiple"
              style={{ width: '100%' }}
              onChange={values => {
                setFilters({ ...filters, campiIds: (values as number[]) || [] })
              }}
              allowClear
              disabled={loading || campiLoading}
              showSearch={false}
              optionFilterProp="children"
            >
              {campi.map(({ idCampus, nomeCampus }) => (
                <Select.Option key={idCampus} value={idCampus}>
                  {nomeCampus || idCampus}
                </Select.Option>
              ))}
            </Select>
          </Cell>
        </Grid>
      </RoundedDiv>
    </AnimatedHeader>
  )
}

export default EventsListPageHeader
