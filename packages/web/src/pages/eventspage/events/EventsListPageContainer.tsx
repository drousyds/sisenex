import { EditOutlined, RightOutlined } from '@ant-design/icons'
import { Tooltip, Button } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import React, { useState, useCallback } from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps, useHistory } from 'react-router-dom'
import { Dispatch } from 'redux'

import CreateEventDialog from '../../../components/dialogs/events/CreateEventDialog'
import UpdateEventDialog from '../../../components/dialogs/events/UpdateEventDialog'
import EventsTable from '../../../components/events/EventsTable'
import { useGetEventsQuery, Evento } from '../../../generated'
import { openNotificationWithIcon, getCampusNumber } from '../../../helpers'
import { toMoment } from '../../../helpers/dates'
import { useFilteredEvents, EventsFilterType } from '../../../hooks'
import {
  openCreateEventDialogAction,
  closeCreateEventDialogAction,
  openUpdateEventDialogAction,
  closeUpdateEventDialogAction
} from '../../../redux/ducks/dialogs/events/actions'
import { AppStateType } from '../../../types'
import EventsListPageHeader from './EventsListPageHeader'

interface Props extends RouteComponentProps {
  eventsDialogs: AppStateType['dialogs']['events']
  openCreateEventDialog: () => void
  closeCreateEventDialog: () => void
  openUpdateEventDialog: (event: Evento) => void
  closeUpdateEventDialog: () => void
}

const EventsListPageContainer: React.FC<Props> = ({
  eventsDialogs,
  openCreateEventDialog,
  closeCreateEventDialog,
  openUpdateEventDialog,
  closeUpdateEventDialog
}) => {
  // Router hooks
  const history = useHistory()

  // Filters Hooks
  const [filters, setFilters] = useState<EventsFilterType>({
    text: '',
    campiIds: []
  })

  // Query Hook
  const { data, loading, refetch } = useGetEventsQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon(
        'error',
        'Ocorreu um erro ao buscar os eventos.',
        5.5
      )
    }
  })

  // data handlers
  let events: Evento[] = []

  if (data && data.eventos) {
    events = data.eventos || []
  }

  // Render Dialogs
  const renderDialogs = useCallback(() => {
    const { update, create } = eventsDialogs
    return (
      <React.Fragment>
        {create.open && (
          <CreateEventDialog
            open={create.open}
            handleClose={closeCreateEventDialog}
          />
        )}
        {update.open && update.event && (
          <UpdateEventDialog
            open={update.open}
            handleClose={closeUpdateEventDialog}
            event={update.event}
          />
        )}
      </React.Fragment>
    )
  }, [eventsDialogs, closeCreateEventDialog, closeUpdateEventDialog])

  const filteredEvents = useFilteredEvents({ events, filters })

  const columns: ColumnProps<Evento>[] = [
    {
      title: 'Nome',
      key: 'event-name',
      render: (_, { nomeEvento, idEvento }) => (
        <span
          style={{ cursor: 'pointer' }}
          onClick={() => history.push(`/eventos/${idEvento}`)}
        >
          {nomeEvento}
        </span>
      )
    },
    {
      title: 'Local',
      key: 'localEvento',
      render: (_, event) => (
        <span
          style={{ cursor: 'pointer' }}
          onClick={() => history.push(`/eventos/${event.idEvento}`)}
        >
          {event.campi.map(({ idCampus }, index) => (
            <span key={idCampus}>{`${getCampusNumber(idCampus)}${
              event.campi.length - 1 !== index ? ', ' : ''
            } `}</span>
          ))}
        </span>
      )
    },
    {
      title: 'Descrição',
      key: 'descricaoEvento',
      render: (_, { descricaoEvento, idEvento }) => (
        <span
          style={{ cursor: 'pointer' }}
          onClick={() => history.push(`/eventos/${idEvento}`)}
        >
          {descricaoEvento}
        </span>
      )
    },
    {
      title: 'Início',
      key: 'dataInicio',
      render: (_, { dataInicioEvento, idEvento }) => {
        if (dataInicioEvento) {
          const m = toMoment(dataInicioEvento)
          return (
            <span
              style={{ cursor: 'pointer' }}
              onClick={() => history.push(`/eventos/${idEvento}`)}
            >
              {m ? `${m.format('dddd[,] D [de] MMMM [de] YYYY')}` : ' - '}
            </span>
          )
        }
        return <span> - </span>
      }
    },
    {
      title: 'Encerramento',
      key: 'dataFim',
      render: (_, { dataFimEvento, idEvento }) => {
        if (dataFimEvento) {
          const m = toMoment(dataFimEvento)
          return (
            <span
              style={{ cursor: 'pointer' }}
              onClick={() => history.push(`/eventos/${idEvento}`)}
            >
              {m ? `${m.format('dddd[,] D [de] MMMM [de] YYYY')}` : ' - '}
            </span>
          )
        }
        return <span> - </span>
      }
    },
    {
      title: 'Editar',
      key: 'editEventoButton',
      align: 'center',
      render: (_, event) => (
        <span style={{ margin: 10 }}>
          <Tooltip title="Editar Evento">
            <Button
              onClick={() => openUpdateEventDialog(event)}
              icon={<EditOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    },
    {
      title: 'Abrir',
      key: 'openEventoButton',
      align: 'center',
      render: (_, event) => (
        <span style={{ margin: 10 }}>
          <Tooltip title="Abrir">
            <Button
              onClick={() => history.push(`/eventos/${event.idEvento}`)}
              icon={<RightOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    }
  ]

  return (
    <div>
      {renderDialogs()}
      <EventsListPageHeader
        openCreateEventDialog={openCreateEventDialog}
        handleRefetch={() => refetch()}
        loading={loading}
        filters={filters}
        setFilters={setFilters}
      />
      <EventsTable
        loading={loading}
        events={filteredEvents}
        columns={columns}
      />
    </div>
  )
}

const mapStateToProps = (state: AppStateType, ownProps: Props) => ({
  ...ownProps,
  eventsDialogs: state.dialogs.events,
  filterText: state.filters.filterText
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  openCreateEventDialog: () => dispatch(openCreateEventDialogAction()),
  closeCreateEventDialog: () => dispatch(closeCreateEventDialogAction()),
  openUpdateEventDialog: (event: Evento) =>
    dispatch(openUpdateEventDialogAction(event)),
  closeUpdateEventDialog: () => dispatch(closeUpdateEventDialogAction())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EventsListPageContainer)
