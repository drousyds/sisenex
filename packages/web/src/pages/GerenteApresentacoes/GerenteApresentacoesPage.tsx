import {
  EditOutlined,
  EyeOutlined,
  PlusOutlined,
  ReloadOutlined
} from '@ant-design/icons'
import { Table, Tag, Result, Tooltip, Button } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import React, { useMemo } from 'react'
import { useDispatch } from 'react-redux'

import { VerticalFade } from '../../components/animation'
import RoundedDiv from '../../components/common/RoundedDiv'
import CreatePresentationDialog from '../../components/dialogs/presentations/CreatePresentationDialog'
import { GenerateQRButton } from '../../components/qr'
import {
  CREATE_ADDPROJECT_ERROR_MESSAGE,
  CREATE_ADDPROJECT_SUCCESS_MESSAGE,
  CREATE_PRESENTATION_ERROR_MESSAGE,
  CREATE_PRESENTATION_SUCCESS_MESSAGE,
  FETCH_MESSAGES
} from '../../constants/messages'
import {
  Apresentacao,
  Projeto,
  useCriarApresentacaoMutation,
  useAdicionarProjetosMutation,
  useGetPresentationsWithProjectsQuery
} from '../../generated'
import {
  openNotificationWithIcon,
  renderLongUnixDate,
  getStatusTagColor,
  getPresentationStatus,
  getProjectStatus
} from '../../helpers'
import {
  useFilters,
  PresentationsFiltersType,
  useFilteredPresentations
} from '../../hooks'
import { useGenericDialog } from '../../hooks/useGenericDialog'
import { openPresentationDetailsDialogAction } from '../../redux/ducks/dialogs/presentations/actions'
import {
  openProjectDetailsDialogAction,
  openUpdateProjectDialogAction
} from '../../redux/ducks/dialogs/projects/actions'
import { NotificationType } from '../../types'
import AllPresentationsHeader from '../presentationspage/AllPresentationsHeader'


export const GerenteApresentacoesPage: React.FC = () => {
  const dispatch = useDispatch()
  const createPresentationDialog = useGenericDialog(
    'CREATE_PRESENTATION_DIALOG'
  )
  const [
    criarApresentacaoMutation,
    { loading: mutationLoading }
  ] = useCriarApresentacaoMutation({
    onError() {
      openNotificationWithIcon('error', CREATE_PRESENTATION_ERROR_MESSAGE)
      createPresentationDialog.close()
    },
    onCompleted() {
      openNotificationWithIcon('success', CREATE_PRESENTATION_SUCCESS_MESSAGE)
      createPresentationDialog.close()
    }
  })


  const [
    adicionarProjetosMutation,
    { loading: mutationLoading2 }
  ] = useAdicionarProjetosMutation({
    onError() {
      // openNotificationWithIcon('error', CREATE_ADDPROJECT_ERROR_MESSAGE)
      // createPresentationDialog.close()
    },
    onCompleted() {
      // openNotificationWithIcon('success',CREATE_ADDPROJECT_SUCCESS_MESSAGE)
      // createPresentationDialog.close()
    }
  })

  const [filters, setFilters] = useFilters<PresentationsFiltersType>({
    text: '',
    disponibilidadeApresentacao: undefined,
    idCategoria: undefined
  })

  // Presentations Query Hooks
  const { data, loading, refetch } = useGetPresentationsWithProjectsQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        FETCH_MESSAGES.ERROR.PRESENTATIONS,
        5.5
      )
    }
  })

  const presentations = useMemo(() => {
    const { apresentacoes } = data || {}
    return apresentacoes || []
  }, [data])

  const filteredPresentations = useFilteredPresentations({
    presentations,
    filters
  })

  const presentationTableColumns: ColumnProps<Apresentacao>[] = [
    {
      title: 'Código',
      key: 'presentationId',
      render: (_, { idApresentacao, id2Apresentacao, codigoApresentacao }) => (
        <span>
          {codigoApresentacao || `${idApresentacao}-${id2Apresentacao}`}
        </span>
      )
    },
    {
      title: 'Local',
      key: 'presentationRoom',
      render: (_, presentation) => (
        <span>
          {presentation.salaApresentacao
            ? presentation.salaApresentacao
            : 'Sem Sala'}
        </span>
      )
    },
    {
      title: 'Categoria',
      dataIndex: ['categoria', 'nomeCategoria']
    },
    {
      title: 'Área Temática',
      key: 'area-tematica-presentations',
      render: (_, { areaTematica }) => (
        <span>{areaTematica ? areaTematica.nomeAreaTematica : null}</span>
      )
    },
    {
      title: 'Hora',
      key: 'presentationTime',
      render: (_, presentation) => (
        <span>
          {presentation.horaApresentacao
            ? renderLongUnixDate(presentation.horaApresentacao)
            : 'Sem Data'}
        </span>
      )
    },
    {
      title: 'Nº Projetos',
      key: 'projects-counter',
      sortDirections: ['ascend', 'descend'],
      sorter: ({ projetos: pA }, { projetos: pB }) =>
        pA && pB ? pA.length - pB.length : -1,
      render: (_, { projetos }) => <span>{projetos ? projetos.length : 0}</span>
    },
    {
      title: 'Estado',
      key: 'presentationStatus',
      render: (_, presentation) => {
        return (
          <span>
            <Tag
              color={getStatusTagColor(presentation)}
              key="presentationStatusTag"
            >
              {getPresentationStatus(presentation)}
            </Tag>
          </span>
        )
      }
    },
    {
      title: 'Visualizar',
      key: 'presentationDetailsButton',
      align: 'center',
      render: (_, presentation) => (
        <span style={{ margin: 10 }}>
          <Tooltip title="Visualizar Apresentação">
            <Button
              onClick={() =>
                dispatch(openPresentationDetailsDialogAction(presentation))
              }
              icon={<EyeOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    },
    {
      title: 'Gerar QR',
      key: 'generateQRCode',
      align: 'center',
      render: (_, presentation) => (
        <span style={{ margin: 10 }}>
          <Tooltip title="Visualizar Apresentação">
            <GenerateQRButton
              loading={loading}
              data={presentation}
            ></GenerateQRButton>
          </Tooltip>
        </span>
      )
    }
  ]

  const projectsColumns: ColumnProps<Projeto>[] = [
    {
      title: 'Código',
      dataIndex: 'codigoProjeto'
    },
    {
      title: 'Projeto',
      dataIndex: 'tituloProjeto'
    },
    {
      title: 'Estado',
      key: 'projectStatus',
      render: (_, project) => {
        return (
          <span>
            <Tag color={getStatusTagColor(project)} key="projetStatusTag">
              {getProjectStatus(project)}
            </Tag>
          </span>
        )
      }
    },
    {
      title: 'Editar',
      key: 'editProjectButton',
      align: 'center',
      render: (_, project) => (
        <span>
          <Tooltip title="Visualizar Projeto">
            <Button
              onClick={() => dispatch(openUpdateProjectDialogAction(project))}
              icon={<EditOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    },
    {
      title: 'Visualizar',
      key: 'viewProject',
      align: 'center',
      render: (_, project) => (
        <span>
          <Tooltip title="Visualizar Projeto">
            <Button
              onClick={() => dispatch(openProjectDetailsDialogAction(project))}
              icon={<EyeOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    }
  ]

  return (
    <div>
      {createPresentationDialog.visible && (
        <CreatePresentationDialog
          loading={mutationLoading}
          visible={createPresentationDialog.visible}
          handleClose={createPresentationDialog.close}
          handleSubmit={async data => {
            const dateIdApresentacao2 = `${new Date().getFullYear()}`
            const aux = await criarApresentacaoMutation({
              variables: {
                input: {
                  salaApresentacao: data.salaApresentacao,
                  horaApresentacao: data.horaApresentacao,
                  id2Apresentacao: dateIdApresentacao2,
                  idCategoria: data.idCategoria,
                  idAreaTematica: data.idAreaTematica,
                  disponibilidadeApresentacao: 0,
                  linkApresentacao: data.linkApresentacao,
                  modalidadeApresentacao: `${
                    data.idCategoria === 4 ? 'P' : 'T'
                  }`,
                  idEvento: data.idEvento
                }
              }
            })

            const idApresentacaoaux = aux?.data?.criarApresentacao?.idApresentacao ?  aux?.data?.criarApresentacao?.idApresentacao : -1 ;
            data.projects?.forEach(async(item)=>{
              //console.log(dateIdApresentacao2, item.idProjeto, idApresentacaoaux)
              await adicionarProjetosMutation({
                variables: {
                  input: {
                    id2Apresentacao: dateIdApresentacao2,
                    idProjeto: item.idProjeto,
                    idApresentacao: idApresentacaoaux,
                  }
                }
              })
              })
               
            await refetch()
          }}
        />
      )}
      <AllPresentationsHeader
        loading={loading}
        filters={filters}
        setFilters={setFilters}
      >
        <Button
          style={{ marginRight: 10 }}
          onClick={() => {
            createPresentationDialog.open()
          }}
          icon={<PlusOutlined />}
          loading={loading}
          disabled={loading}
        >
          Criar Apresentação
        </Button>
        <Tooltip title="Atualizar a página">
          <Button
            onClick={() => {
              refetch()
            }}
            icon={<ReloadOutlined />}
            loading={loading}
            disabled={loading}
          >
            Atualizar
          </Button>
        </Tooltip>
      </AllPresentationsHeader>
      <VerticalFade direction="up" amount={20}>
        <RoundedDiv shadow style={{ marginTop: 15 }}>
          <Table
            style={{ overflowX: 'auto' }}
            loading={loading}
            dataSource={filteredPresentations}
            rowKey={({ codigoApresentacao }) => `${codigoApresentacao}`}
            columns={presentationTableColumns}
            footer={() => (
              <div>
                {`Apresentações: ${filteredPresentations.length} / ${presentations.length}`}
              </div>
            )}
            expandedRowRender={(presentation, _, __, expand) => {
              const ps: Projeto[] = presentation.projetos || []
              return expand ? (
                <Table
                  dataSource={ps}
                  rowKey={({ idProjeto }) => `${idProjeto}`}
                  columns={projectsColumns}
                  pagination={false}
                  size="middle"
                  locale={{
                    emptyText: (
                      <span>
                        <Result
                          title="Esta apresentação ainda não possui projetos."
                          status="info"
                        />
                      </span>
                    )
                  }}
                />
              ) : null
            }}
          />
        </RoundedDiv>
      </VerticalFade>
    </div>
  )
}
