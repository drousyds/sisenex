import { ReloadOutlined } from '@ant-design/icons'
import { Table, Button, PageHeader } from 'antd'
import React, { useMemo } from 'react'
import { useHistory } from 'react-router-dom'

import RoundedDiv from '../../components/common/RoundedDiv'
import { Projeto, useGetReviewerProjectsQuery } from '../../generated'
import { renderLongUnixDate } from '../../helpers'
import useAuth from '../../hooks/useAuth'
import { ApresentacaoAvaliadorPage, ProjetoAvaliadorPage } from './types'
import { ColumnProps } from 'antd/lib/table'

const AvaliadorProjectsPage: React.FC = () => {
  const { authState: { currentUser } } = useAuth()
  const history = useHistory()

  const { data, loading, refetch } = useGetReviewerProjectsQuery({
    variables: {
      idPessoa: currentUser!.idPessoa,
      idVinculoPessoa: currentUser!.idVinculoPessoa
    },
    fetchPolicy: 'no-cache',
    pollInterval: 10000
  })

  const apresentacoes: ApresentacaoAvaliadorPage[] = useMemo(() => {
    const { pessoa } = data || {}

    if (pessoa && (pessoa.__typename === 'Gerente' || pessoa.__typename === 'Avaliador')) {
      if (!pessoa.projetosParaAvaliar) return []

      const apresentacoesMap = {}

      pessoa.projetosParaAvaliar.forEach(projeto => {
        const { apresentacao } = projeto || {}
        if (apresentacao) {
          const { idApresentacao, id2Apresentacao } = apresentacao
          const presentationKey = `${idApresentacao}-${id2Apresentacao}`
          if (!apresentacoesMap[presentationKey]) {
            apresentacoesMap[presentationKey] = { ...apresentacao }
            apresentacoesMap[presentationKey].projetos = []
          }
          apresentacoesMap[presentationKey].projetos.push(projeto)
        }
      })
      return Object.values(apresentacoesMap)
    }

    return []
  }, [data])

  return (
    <RoundedDiv shadow style={{ height: '100%', padding: 0 }}>
      <PageHeader
        title={'Projetos para Avaliar'}
        extra={[
          <Button
            key="refresh-page"
            onClick={() => {
              refetch()
            }}
            icon={<ReloadOutlined />}
            loading={loading}
          >
            Atualizar
          </Button>
        ]}
      />

      <Table
        style={{ padding: '0 24px' }}
        loading={loading}
        dataSource={apresentacoes}
        rowKey={a => `${a.idApresentacao}-${a.id2Apresentacao}`}
        columns={[
          {
            title: 'Código',
            key: 'apresentacao-key',
            render (_, a) { return a.codigoApresentacao || `${a.idApresentacao}-${a.id2Apresentacao}` }
          },
          {
            title: 'Evento',
            key: 'aprentacao-evento',
            render (_, a) { return a.evento?.nomeEvento || '' }
          },
          {
            title: 'Local',
            key: 'aprentacao-local',
            dataIndex: 'salaApresentacao'
          },
          {
            title: 'Categoria',
            key: 'aprentacao-categoria',
            render (_, a) { return a.categoria.nomeCategoria }
          },
          {
            title: 'Área Temática',
            key: 'aprentacao-categoria',
            render (_, a) { return a.areaTematica?.nomeAreaTematica || '' }
          },
          {
            title: 'Horário',
            key: 'aprentacao-hora',
            render (_, a) { return a.horaApresentacao ? renderLongUnixDate(a.horaApresentacao) : 'Sem Data' }
          },
          {
            title: 'Nº de Projetos',
            key: 'aprentacao-projetos-contador',
            render (_, a) {
              const projetos = a.projetos ? a.projetos.length : 0
              return projetos
            }
          }
        ]}
        expandable={
          {
            indentSize: 0,
            expandedRowClassName: () => 'no-margin'
          }
        }
        expandedRowRender={(apresentacao, _, __, expand) => {
          if (!expand) return null

          const { categoria, projetos } = apresentacao

          const columns = [
            {
              title: 'Código',
              key: 'projeto-key',
              render (_, p: ProjetoAvaliadorPage) { return p.codigoProjeto }
            },
            {
              title: 'Título',
              key: 'projeto-titulo',
              dataIndex: 'tituloProjeto',
            },
            {
              title: 'Nota (Média)',
              key: 'projeto-media',
              render (_, p: ProjetoAvaliadorPage) { return p.mediaIndividual?.mediaProjeto?.toFixed(2) || '' }
            },
            {
              title: 'Avaliação',
              key: 'projeto-avalicao',
              render (_, p: ProjetoAvaliadorPage) {
                return (
                  <Button
                    block
                    type="primary"
                    onClick={() => history.push(`/avaliador/avaliacao/${p.idProjeto}`)}
                  >
                    {p.mediaIndividual?.mediaProjeto ? 'Reavaliar' : 'Avaliar'}
                  </Button>
                )
              }
            }
          ]

          // idCategoria === 2 -> Vídeo
          if (categoria.idCategoria === 2) {
            const item = {
              title: 'Vídeos',
              key: 'projeto-video',
              render (_, p: ProjetoAvaliadorPage) {
                return (
                  <span>
                    {p.linkArtefato && <a href={p.linkArtefato} target="_blank" rel="noreferrer"><Button>Assistir Vídeo</Button></a> }
                  </span>
                )
              }
            }
            columns.splice(2, 0, item)
          }

          return (
            <div>
              {/* idCategoria === 1 -> e-Tertulia */}
              {categoria.idCategoria === 1 && (
                <a href={apresentacao.linkApresentacao || ''} rel="noreferrer" target="_blank">
                  <Button
                    block
                    type="primary"
                  >
                  Clique aqui para participar da conferência.
                  </Button>
                </a>)}

              <Table
                dataSource={projetos || []}
                rowKey={p => `${p.idProjeto}`}
                pagination={false}
                columns={columns}
              />
            </div>
          )
        }}

      />
    </RoundedDiv>
  )
}

export default AvaliadorProjectsPage
