import { Apresentacao, Evento, Categoria, AreaTematica, Projeto, MediaPorAvaliador } from '../../generated'

export interface ApresentacaoAvaliadorPage {
  idApresentacao: Apresentacao['idApresentacao']
  id2Apresentacao: Apresentacao['id2Apresentacao']
  codigoApresentacao: Apresentacao['codigoApresentacao']
  salaApresentacao: Apresentacao['salaApresentacao']
  horaApresentacao: Apresentacao['horaApresentacao']
  linkApresentacao?: Apresentacao['linkApresentacao'] | null
  evento?: {
    idEvento: Evento['idEvento']
    nomeEvento: Evento['nomeEvento']
  } | null
  categoria: {
    idCategoria: Categoria['idCategoria']
    nomeCategoria: Categoria['nomeCategoria']
  }

  areaTematica?: {
    idAreaTematica: AreaTematica['idAreaTematica']
    nomeAreaTematica: AreaTematica['nomeAreaTematica']
  } | null

  projetos?: ProjetoAvaliadorPage[] | null
}

export interface ProjetoAvaliadorPage {
  idProjeto: Projeto['idProjeto']
  tituloProjeto: Projeto['tituloProjeto']
  codigoProjeto?: Projeto['codigoProjeto'] | null
  linkArtefato?: Projeto['linkArtefato'] | null
  categoria: {
    idCategoria: Categoria['idCategoria']
    nomeCategoria: Categoria['nomeCategoria']
  }
  mediaIndividual?: {
    mediaProjeto: MediaPorAvaliador['mediaProjeto']
  } | null

  apresentacao?: ApresentacaoAvaliadorPage | null
}
