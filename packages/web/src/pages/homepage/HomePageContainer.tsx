import { Typography } from 'antd'
import React from 'react'

import RoundedDiv from '../../components/common/RoundedDiv'
import { ScreenModeType } from '../../contexts/AuthContext'
import SisEnex from '../../resources/logos/SisEnex.png'

const { Title } = Typography

interface Props {
  screenMode?: ScreenModeType
}

const HomePageContainer: React.FC<Props> = (props) => {
  return (
    <RoundedDiv
      shadow
      style={{
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
      }}
    >
      <div style={{ flex: 0.5 }}>
        {props.screenMode && (
          <Title level={3}>
            {`Bem-vindo ao módulo de ${props.screenMode === 'AVALIADOR' ? 'Avaliação' : 'Gerência'} do SisEnex`}
          </Title>
        )}
      </div>
      <div>
        <img
          src={SisEnex}
          alt="SisEnex Logo"
          style={{ marginTop: 20, height: 250 }}
        />
      </div>
    </RoundedDiv>
  )
}

export default HomePageContainer
