import { EditOutlined, EyeOutlined } from '@ant-design/icons'
import { Table, Tag, Result, Tooltip, Button } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import React, { useMemo } from 'react'
import { useDispatch } from 'react-redux'

import { VerticalFade } from '../../components/animation'
import RoundedDiv from '../../components/common/RoundedDiv'
import { FETCH_MESSAGES } from '../../constants/messages'
import {
  Apresentacao,
  Projeto,
  useGetPresentationsWithProjectsQuery
} from '../../generated'
import {
  openNotificationWithIcon,
  renderLongUnixDate,
  getStatusTagColor,
  getPresentationStatus,
  getProjectStatus
} from '../../helpers'
import {
  useFilters,
  PresentationsFiltersType,
  useFilteredPresentations
} from '../../hooks'
import { openPresentationDetailsDialogAction } from '../../redux/ducks/dialogs/presentations/actions'
import {
  openProjectDetailsDialogAction,
  openUpdateProjectDialogAction
} from '../../redux/ducks/dialogs/projects/actions'
import { NotificationType } from '../../types'
import ReviewSearch from './ReviewSearch'

const OnlineReviewPageContainer: React.FC = () => {
  const dispatch = useDispatch()
  const [filters, setFilters] = useFilters<PresentationsFiltersType>({
    text: '',
    disponibilidadeApresentacao: undefined
  })

  // Presentations Query Hooks
  const { data, loading, refetch } = useGetPresentationsWithProjectsQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon(
        NotificationType.ERROR,
        FETCH_MESSAGES.ERROR.PRESENTATIONS,
        5.5
      )
    }
  })

  const presentations = useMemo(() => {
    const { apresentacoes } = data || {}
    return apresentacoes || []
  }, [data])

  const filteredPresentations = useFilteredPresentations({
    presentations,
    filters
  })

  const columns: ColumnProps<Apresentacao>[] = [
    {
      title: 'Código',
      key: 'presentationId',
      render: (_, { idApresentacao, id2Apresentacao, codigoApresentacao }) => (
        <span>
          {codigoApresentacao || `${idApresentacao}-${id2Apresentacao}`}
        </span>
      )
    },
    {
      title: 'Evento',
      key: 'event',
      render: (_, { evento }) => (
        <span>{evento ? evento.nomeEvento : null}</span>
      )
    },
    {
      title: 'Modalidade',
      dataIndex: 'modalidadeApresentacao',
      render: (_, presentation) => (
        <span>
          {presentation.modalidadeApresentacao === 'T'
            ? 'Tertúlia'
            : 'Performance'}
        </span>
      )
    },
    {
      title: 'Área Temática',
      key: 'area-tematica-presentations',
      render: (_, { areaTematica }) => (
        <span>{areaTematica ? areaTematica.nomeAreaTematica : null}</span>
      )
    },
    {
      title: 'Horário',
      key: 'presentationTime',
      render: (_, presentation) => (
        <span>
          {presentation.horaApresentacao
            ? renderLongUnixDate(presentation.horaApresentacao)
            : 'Sem Data'}
        </span>
      )
    },
    {
      title: 'Nº Projetos',
      key: 'projects-counter',
      sortDirections: ['ascend', 'descend'],
      sorter: ({ projetos: pA }, { projetos: pB }) =>
        pA && pB ? pA.length - pB.length : -1,
      render: (_, { projetos }) => <span>{projetos ? projetos.length : 0}</span>
    },
    {
      title: 'Visualizar',
      key: 'presentationDetailsButton',
      align: 'center',
      render: (_, presentation) => (
        <span style={{ margin: 10 }}>
          <Tooltip title="Visualizar Apresentação">
            <Button
              onClick={() =>
                dispatch(openPresentationDetailsDialogAction(presentation))
              }
              icon={<EyeOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    }
  ]

  const projectsColumns: ColumnProps<Projeto>[] = [
    {
      title: 'Código',
      dataIndex: 'codigoProjeto'
    },
    {
      title: 'Projeto',
      dataIndex: 'tituloProjeto'
    },
    {
      title: 'Estado',
      key: 'projectStatus',
      render: (_, project) => {
        return (
          <span>
            <Tag color={getStatusTagColor(project)} key="projetStatusTag">
              {getProjectStatus(project)}
            </Tag>
          </span>
        )
      }
    },
    {
      title: 'Editar',
      key: 'editProjectButton',
      align: 'center',
      render: (_, project) => (
        <span>
          <Tooltip title="Visualizar Projeto">
            <Button
              onClick={() => dispatch(openUpdateProjectDialogAction(project))}
              icon={<EditOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    },
    {
      title: 'Visualizar',
      key: 'viewProject',
      align: 'center',
      render: (_, project) => (
        <span>
          <Tooltip title="Visualizar Projeto">
            <Button
              onClick={() => dispatch(openProjectDetailsDialogAction(project))}
              icon={<EyeOutlined />}
              shape="circle"
              type="dashed"
            />
          </Tooltip>
        </span>
      )
    }
  ]
  return (
    <div>
      <ReviewSearch
        loading={loading}
        filters={filters}
        setFilters={setFilters}
        handleRefetch={() => refetch()}
      />
      <VerticalFade direction="up" amount={20}>
        <RoundedDiv shadow style={{ marginTop: 15 }}>
          <Table
            style={{ overflowX: 'auto' }}
            loading={loading}
            dataSource={filteredPresentations}
            rowKey={({ codigoApresentacao }) => `${codigoApresentacao}`}
            columns={columns}
            footer={() => (
              <div>
                {`Apresentações: ${filteredPresentations.length} / ${presentations.length}`}
              </div>
            )}
            expandedRowRender={(presentation, _, __, expand) => {
              const ps: Projeto[] = presentation.projetos || []
              return expand ? (
                <Table
                  dataSource={ps}
                  rowKey={({ idProjeto }) => `${idProjeto}`}
                  columns={projectsColumns}
                  pagination={false}
                  size="middle"
                  locale={{
                    emptyText: (
                      <span>
                        <Result
                          title="Esta apresentação ainda não possui projetos."
                          status="info"
                        />
                      </span>
                    )
                  }}
                />
              ) : null
            }}
          />
        </RoundedDiv>
      </VerticalFade>
    </div>
  )
}

export default OnlineReviewPageContainer
