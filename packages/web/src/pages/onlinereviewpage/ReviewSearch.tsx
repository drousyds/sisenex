import React from 'react'
import { ReloadOutlined } from '@ant-design/icons'
import { Input, Tooltip, Button } from 'antd'

import { VerticalFade } from '../../components/animation'
import RoundedDiv from '../../components/common/RoundedDiv'
import { PresentationsFiltersType } from '../../hooks'

export interface Props {
  loading: boolean
  handleRefetch: () => void
  filters: PresentationsFiltersType
  setFilters: (e: PresentationsFiltersType) => void
}

const AllPresentationsHeader: React.FC<Props> = ({
  loading,
  handleRefetch,
  filters,
  setFilters
}) => {
  return (
    <VerticalFade direction="down" amount={20}>
      <RoundedDiv>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <Input.Search
            style={{ marginRight: 10 }}
            placeholder="Pequisar..."
            onChange={e => {
              if (!e.target.value) {
                setFilters({ ...filters, text: '' })
              }
            }}
            onSearch={value => {
              setFilters({ ...filters, text: value })
            }}
          />
          <Tooltip title="Atualizar a página">
            <Button
              onClick={() => {
                handleRefetch()
              }}
              icon={<ReloadOutlined />}
              loading={loading}
              disabled={loading}
            >
              Atualizar
            </Button>
          </Tooltip>
        </div>
      </RoundedDiv>
    </VerticalFade>
  )
}

export default AllPresentationsHeader
