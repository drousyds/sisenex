import React, { useState } from 'react'
import { Evento, useExportPageContainerQuery, Projeto } from '../../generated'
import { openNotificationWithIcon } from '../../helpers'
import InfoPageHeader from '../infopage/InfoPageHeader'
import { Spin } from 'antd'
import { NoEventResult } from '../../components/results'
import ExportProjectsTable from './ExportProjectsTable'

const ExportPageContainer = () => {
  const [eventId, setEventId] = useState<number | undefined>(undefined)

  const { data, loading, refetch } = useExportPageContainerQuery({
    notifyOnNetworkStatusChange: true,
    skip: eventId ? false : true,
    fetchPolicy: 'network-only',
    variables: { idEvento: eventId! },
    onError: () => {
      openNotificationWithIcon(
        'error',
        'Ocorreu um erro ao buscar as informações.',
        5.5
      )
    }
  })

  let event: Evento | null = null
  let projects: Projeto[] = []
  if (data && data.evento && data.projetos) {
    event = data.evento
    projects = data.projetos
  }

  console.log(data)

  return (
    <div>
      <InfoPageHeader
        setEvent={(value: number | undefined) => setEventId(value)}
        eventId={eventId}
        loading={false}
        handleRefetch={() => refetch()}
      />
      <Spin spinning={loading} size="large">
        {!loading && eventId && data && event ? (
          <React.Fragment>
            <ExportProjectsTable projects={projects} event={event} />
          </React.Fragment>
        ) : (
          <NoEventResult />
        )}
      </Spin>
    </div>
  )
}

export default ExportPageContainer
