import { Table, Button, Typography } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import _ from 'lodash'
import React, { useMemo } from 'react'
import { CSVLink } from 'react-csv'

import { VerticalFade } from '../../components/animation'
import RoundedDiv from '../../components/common/RoundedDiv'
import { Projeto, Evento } from '../../generated'
import { getCampusNumber } from '../../helpers'

interface Props {
  projects: Projeto[]
  event: Evento
}

const ExportProjectsTable: React.FC<Props> = ({ projects, event }) => {
  const indexedProjects = useMemo(() => {
    return _.reverse(
      _.sortBy(projects, ({ media }) => {
        return media || -999
      })
    ).map((project, i) => ({
      ...project,
      mediaStr: project.media ? project.media!.toFixed(3) : 'Não Apresentado',
      index: i + 1
    }))
  }, [projects])

  const columns: ColumnProps<
    Projeto & { index: number; mediaStr: string }
  >[] = [
    {
      title: 'Nº',
      key: 'index',
      render: (_, { index }) => <span>{`${index}°`}</span>
    },
    {
      title: 'Código',
      key: 'idProjeto',
      render: (_, { codigoProjeto }) => (
        <span style={{ whiteSpace: 'nowrap' }}>{codigoProjeto || '-'}</span>
      )
    },
    {
      title: 'Nome',
      dataIndex: 'tituloProjeto'
    },
    {
      title: 'Área Temática',
      key: 'areaTematica',
      render: (_, { areaTematica }) => (
        <span>{areaTematica.nomeAreaTematica}</span>
      )
    },
    {
      title: 'Apresentação',
      key: 'idApresentacao',
      render: (_, { apresentacao }) =>
        apresentacao ? apresentacao.codigoApresentacao : 'Sem Apresentação'
    },
    {
      title: 'Nota',
      key: 'projectAverageScore',
      render: (_, { mediaStr }) => mediaStr || 'Sem Nota'
    }
  ]

  const csvHeaders = [
    { label: 'N° ', key: 'index' },
    { label: 'Código', key: 'codigoProjeto' },
    { label: 'Titulo', key: 'tituloProjeto' },
    { label: 'Nota', key: 'mediaStr' },
    { label: 'Apresentacao', key: 'apresentacao.codigoApresentacao' },
    { label: 'Área Temática', key: 'areaTematica.nomeAreaTematica' }
  ]

  const filename = `relatorio-geral-campus-${event.campi
    .map(({ idCampus }) => getCampusNumber(idCampus))
    .join('-')}.csv`

  return (
    <VerticalFade direction="up" amount={20}>
      <RoundedDiv shadow style={{ marginTop: 10 }}>
        <div
          style={{ display: 'flex', flexDirection: 'row', marginBottom: 10 }}
        >
          <div style={{ flex: 1 }}>
            <Typography.Title level={3} style={{ margin: 0 }}>
              Relatório Geral
            </Typography.Title>
          </div>
          <div>
            <CSVLink
              data={indexedProjects}
              headers={csvHeaders}
              filename={filename}
            >
              <Button>Exportar Dados</Button>
            </CSVLink>
          </div>
        </div>

        <Table
          style={{ overflowX: 'auto' }}
          dataSource={indexedProjects}
          rowKey={project => `${project.idProjeto}`}
          columns={columns}
        />
      </RoundedDiv>
    </VerticalFade>
  )
}

export default ExportProjectsTable
