import React, {useState} from 'react'
import { Typography } from 'antd'
import RoundedDiv from '../../components/common/RoundedDiv'
import { Form, Input, Button } from 'antd';

import './SolicitacaoLGPD.css'
import useAuth from '../../hooks/useAuth';
import { openNotificationWithIcon } from '../../helpers';
import { useCriarSolicitacaoLgpdMutation } from '../../generated';

interface FormData {
  tipoSolicitacao: number
  textoSolicitacao?: string
  nomeSolicitacao: string
  sobrenomeSolicitacao: string
  emailPessoa: string
}

const SolicitacaoLGPD: React.FC = (props) => {

  const [form] = Form.useForm();
  const { authState: { currentUser } } = useAuth()
  const [mutate, { loading }] = useCriarSolicitacaoLgpdMutation({
    onError () {
      openNotificationWithIcon('error', 'Ocorreu um erro ao enviar a solicitação.')
    },
    onCompleted () {
      openNotificationWithIcon('success', 'Solicitação enviado com sucesso.')
    }
  })

  const [selectedItems, setSelectedItems] = useState<boolean[]>([])
  let [items, setItems] = useState([false, false, false, false])

  const handleAdd = (add: boolean) => {
    setSelectedItems([...selectedItems, add])
  }

  const handleRemove = (add: boolean) => {
    setSelectedItems(
      selectedItems.filter((selected)=> {
        return selected!==add
      })
    )
  }

  const onFinish = async (data: FormData) => {
    for(let i = 0; i < 4; i++){
      if(items[i] == true){
        if(!currentUser) return
          try {
            await mutate({
              variables: {
                tipoSolicitacao: i+1,
                nomeSolicitacao: data.nomeSolicitacao,
                textoSolicitacao: i == 3 ? data.textoSolicitacao : null,
                sobrenomeSolicitacao: data.sobrenomeSolicitacao,
                emailPessoa: data.emailPessoa,
                idPessoa: currentUser.idPessoa,
                idVinculoPessoa: currentUser.idVinculoPessoa
              }
            })
            form.resetFields()
          } catch (error) {
            console.error(error)
          }
      }
    }
  };
    return (
      <RoundedDiv
        shadow
        style={{
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
      }}>

        <div style={{width: '60%', marginTop: '5%', marginBottom: '16px', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>

          <p style={{fontSize: '13px', color: 'gray', textAlign: 'center'}}>
          A Lei nº 13.709 - Lei Geral de Proteção de Dados (LGPD) permite que você exerça certos direitos, 
          como solicitar a confirmação da existência de processamento de suas informações pessoais, acesso, correção, 
          anonimização e exclusão de informações pessoais e categorias de informações pessoais sobre você, entre outras.
          Forneça as seguintes informações para que possamos processar sua solicitação.
          Observe que solicitar a exclusão das suas informações pessoais não garante a exclusão completa ou abrangente, 
          pois pode haver circunstâncias em que a lei não exija ou permita que excluamos as informações.
          </p>
        </div>
        
        <Form
          form={form}
          layout="vertical"
          autoComplete="off"
          style={{width: '60%'}}
          onFinish={onFinish}
        >
          <div
            style={{
              overflow: 'hidden',
            }}
          >
            <Form.Item
              name="type"
              label="Selecione o tipo de solicitação"
              style={{fontWeight: 'bold'}}
            >
              <div className="itens" style={{display: 'flex', flexDirection: 'row', alignItems:'center', justifyContent:'center', fontWeight: 600}}>
    
                <div className={`item ${items[0]? 'active' : ''}`} onClick={() => {
                  if (!items[0]) {
                    handleAdd(true)
                    setItems([true, items[1], items[2], items[3]])
                  }else{
                    handleRemove(false)
                    setItems([false, items[1], items[2], items[3]])
                  }
                }}>
                  <p>Consultar existência dos meus dados</p>
                </div>

                <div className={`item ${items[1]? 'active' : ''}`} onClick={() => {
                  if (!items[1]) {
                    handleAdd(true)
                    setItems([items[0], true, items[2], items[3]])
                  }else{
                    handleRemove(false)
                    setItems([items[0], false, items[2], items[3]])
                  }
                }}>
                  <p>Solicitar minhas informações pessoais</p>
                </div>

                <div className={`item ${items[2]? 'active' : ''}`} onClick={() => {
                  if (!items[2]) {
                    handleAdd(true)
                    setItems([items[0], items[1], true, items[3]])
                  }else{
                    handleRemove(false)
                    setItems([items[0], items[1], false, items[3]])
                  }
                }}>
                  <p>Excluir minhas informações pessoais</p>
                </div>

                <div className={`item ${items[3]? 'active' : ''}`} onClick={() => {
                  if (!items[3]) {
                    handleAdd(true)
                    setItems([items[0], items[1], items[2], true])
                  }else{
                    handleRemove(false)
                    setItems([items[0], items[1], items[2], false])
                  }
                }}>
                  <p>Outro tipo de solicitação</p>
                </div>
              </div>
              
            </Form.Item>

            <Form.Item
              name="textoSolicitacao"
              label="Se você selecionou “Outro tipo de solicitação”, por favor explique a sua solicitação."
              style={{fontWeight: 'bold'}}
            >
              <Input.TextArea />
            </Form.Item>
            <Form.Item
              name="nomeSolicitacao"
              label="Nome"
              style={{fontWeight: 'bold'}}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input  />
            </Form.Item>

            <Form.Item
              name="sobrenomeSolicitacao"
              label="Sobrenome"
              style={{fontWeight: 'bold'}}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="emailPessoa"
              label="E-mail"
              style={{fontWeight: 'bold'}}
              rules={[
                {
                  required: true,
                  type: 'email'
                },
              ]}
            >
              <Input />
            </Form.Item>
          </div>
          <div style={{display: 'flex', justifyContent:'center', alignItems:'center'}}>
            <Button type="primary" htmlType="submit">
              Enviar
            </Button>
          </div>
        </Form>
      </RoundedDiv>
    )
  }

export default SolicitacaoLGPD
