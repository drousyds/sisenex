import React from "react";
import { Button, message, Tabs} from 'antd';
import RoundedDiv from '../../components/common/RoundedDiv'
import { Typography } from "antd";

import { Link, Router, useRouteMatch, useParams } from "react-router-dom";
import useAuth from "../../hooks/useAuth";

interface Props{}

const {TabPane}= Tabs
const { Title, Text, Paragraph } = Typography 

const PoliticaPage: React.FC <Props> = (props) =>{
  const { authState: { currentUser } } = useAuth()

  const info = () => {
    message.error('Você precisa estar autenticado para fazer uma solicitação', 7);
  };

  return(
    <RoundedDiv
      shadow
      style={{
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
      }}
    >
  <div style={{margin: '25px'}}>      
  <Title style={{textAlign: 'center'}} level={3}>Política de Privacidade</Title>      
  <Tabs>
    <TabPane tab="Política de privacidade">
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <Title level={4}>Termo de Uso</Title>
      </div>
      <Paragraph></Paragraph>
      <Paragraph>
        O SisEnex é um sistema desenvolvido exclusivamente para apoiar o Encontro de Extensão (ENEX) da Universidade Federal da Paraíba (UFPB). Seu principal objetivo é automatizar o processo de avaliação e premiação das ações de extensão apresentadas durante o evento. O SisEnex foi projetado para executar apenas funcionalidades essenciais para a avaliação das apresentações, limitando-se a armazenar e processar os dados necessários para essa finalidade.
      </Paragraph>

      <Text strong>Compromisso com a Privacidade</Text>
      <Paragraph></Paragraph>
      <Paragraph>
        Nosso compromisso é garantir a segurança, privacidade e proteção dos dados pessoais de todos os docentes, discentes e usuários que utilizam a plataforma. Todos os dados coletados e processados pelo SisEnex são tratados de acordo com a Lei Geral de Proteção de Dados Pessoais (LGPD - Lei nº 13.709/2018). Esses dados serão utilizados exclusivamente para os fins descritos, e não serão compartilhados com terceiros sem o consentimento explícito dos usuários, exceto quando exigido por lei.
      </Paragraph>
      <Paragraph>
        Ao utilizar o SisEnex, o usuário concorda com a coleta e o uso de suas informações conforme descrito nesta política. Para dúvidas ou exercício dos direitos previstos na LGPD, como a solicitação de acesso, correção ou exclusão de dados, o usuário pode entrar em contato pelos canais disponíveis na plataforma.
      </Paragraph>
      
      <Text strong>Responsabilidades dos Usuários</Text>
      <Paragraph></Paragraph>
      <Paragraph>
        O uso do SisEnex é restrito a estudantes e servidores da UFPB com vínculo ativo, que tenham participado das ações de extensão apresentadas no ENEX. Ao utilizar a plataforma, os usuários se comprometem a:
      </Paragraph>
      <div style={{ paddingLeft: '20px' }}>
        <ul>
          <li>Usar suas contas pessoais e manter as senhas em sigilo;</li>
          <li>Não compartilhar senhas ou contas com terceiros;</li>
          <li>Respeitar os direitos de privacidade e segurança de outros usuários.</li>
        </ul>
      </div>

      <Text strong>Limitações de Responsabilidade</Text>
      <Paragraph></Paragraph>
      <Paragraph>
        O SisEnex não se responsabiliza por problemas decorrentes de fatores externos ao sistema, como:
      </Paragraph>
      <div style={{ paddingLeft: '20px' }}>
        <ul>
          <li>Equipamentos infectados ou comprometidos;</li>
          <li>Proteção inadequada dos dispositivos dos usuários;</li>
          <li>Monitoramento ilegal de dispositivos.</li>
        </ul>
      </div>


      <div style={{ paddingTop:'30px' , display: 'flex', justifyContent: 'center' }}>
        <Text strong>Aviso de Política de Privacidade</Text>
      </div>
      <Paragraph></Paragraph>

      <Paragraph></Paragraph>
      
      <Text strong>Finalidade dos Dados Coletados</Text>
      <Paragraph></Paragraph>
      <Paragraph>
        Os dados são coletados por meio da autenticação no Sistema de Controle Acadêmico (SIGAA/UFPB) para verificar o vínculo ativo dos participantes com a UFPB e garantir a integridade do processo avaliativo. Esses dados são usados exclusivamente para autenticação e alocação nas avaliações das ações de extensão, que determinam os projetos premiados no ENEX.
      </Paragraph>

      <Text strong>Dados Coletados</Text>
      <Paragraph></Paragraph>
      <Paragraph>
        Os dados coletados incluem: nome social, matrícula, lotação, endereço de e-mail e telefone. Esses dados são obtidos automaticamente ao autenticar-se pela primeira vez no aplicativo via SIGAA/UFPB. O uso do SisEnex implica a concordância com esta Política de Privacidade.
      </Paragraph>
      
      <Text strong>Uso dos Dados</Text>
      <Paragraph></Paragraph>
      <Paragraph>
        Os dados são utilizados para:
      </Paragraph>
      <div style={{ paddingLeft: '20px' }}>
        <ul>
          <li>Facilitar o processo de avaliação das ações apresentadas no ENEX;</li>
          <li>Enviar comunicações relevantes relacionadas ao SisEnex e ao evento ENEX;</li>
          <li>Gerar relatórios internos para a Pró-Reitoria de Extensão (PROEX).</li>
        </ul>
      </div>
      <Paragraph>
        Esses dados não serão compartilhados com terceiros fora da UFPB, exceto quando expressamente autorizado ou exigido por lei.
      </Paragraph>

      <Text strong>Armazenamento dos Dados</Text>
      <Paragraph></Paragraph>
      <Paragraph>
        Os dados dos usuários são mantidos durante o processo de avaliação e divulgação dos premiados no ENEX. Após a finalização do evento, os dados são consolidados em relatórios internos enviados à PROEX para auditoria, e as informações pessoais são excluídas do sistema. O usuário pode solicitar a exclusão de seus dados a qualquer momento, conforme previsto na LGPD.
      </Paragraph>

      <Text strong>Segurança dos Dados</Text>
      <Paragraph></Paragraph>
      <Paragraph>
        O SisEnex utiliza medidas de segurança reconhecidas para proteger os dados pessoais, embora nenhum sistema seja totalmente invulnerável. O processo de autenticação é de responsabilidade da Superintendência de Tecnologia da Informação (STI) da UFPB.
      </Paragraph>
      <Paragraph>
        O SisEnex se compromete em manter este Aviso de Privacidade atualizado, observando suas determinações e zelando por seu cumprimento. Além disso, assume o compromisso de buscar condições técnicas aptas a proteger todo o processo de tratamento de dados, mantendo os dados armazenados em nossa base de dados seguros. Em caso de ataque, vazamento ou algum corrompimento de dados, se compromete em realizar um comunicado de incidente de segurança.
      </Paragraph>
      <Paragraph>
        Qualquer comunicado de incidente de segurança será disponibilizado por contato direto com o e-mail de nossos usuários e com o auxílio dos meios de comunicação da PROEX. Os comunicados desenvolvidos terão como objetivo retratar de forma direta e objetiva a descrição do incidente, quais dados foram afetados, qual a gravidade do incidente e quais as salvaguardas e ações executadas para contenção de qualquer dano potencial.
      </Paragraph>

      <Text strong>Alterações na Política de Privacidade</Text>
      <Paragraph></Paragraph>
      <Paragraph>
        Esta política pode ser alterada periodicamente, e os usuários serão informados sobre mudanças significativas. A continuação do uso do SisEnex após modificações implica a aceitação das alterações.
      </Paragraph>

      <Text strong>Contato</Text>
      <Paragraph></Paragraph>
      <Paragraph>
        Para dúvidas, sugestões ou reclamações sobre a Política de Privacidade, entre em contato pelo e-mail: sisenex@lumo.ci.ufpb.br.
      </Paragraph>
    </TabPane>
  </Tabs>
</div>
    
      <Link to={`solicitacao`}>
        <Button type="primary" htmlType="submit" onClick={currentUser ? () => {} : info}>
          Fazer solicitação
        </Button>
      </Link>
    </RoundedDiv>
  );
}
export default PoliticaPage