
import { Button, Input, Select, Tooltip } from 'antd'
import React from 'react'
import { Grid, Cell } from 'styled-css-grid'
import { PlusOutlined, ReloadOutlined } from '@ant-design/icons'
import { AnimatedHeader } from '../../../components/animation'
import RoundedDiv from '../../../components/common/RoundedDiv'
import { AreaTematica } from '../../../generated'
import { ProjectsFiltersType } from '../../../hooks'

// css
import './ProjectsPage.css'

interface Props {
  createProjectDialog: () => void
  loading: boolean
  areasTematicas: AreaTematica[]
  handleRefetch: () => void
  filters: ProjectsFiltersType
  setFilters: (e: ProjectsFiltersType) => void
}

const ProjectsPageHeader: React.FC<Props> = ({
  createProjectDialog,
  handleRefetch,
  loading,
  areasTematicas,
  setFilters,
  filters
}) => {
  return (
    <AnimatedHeader>
      <RoundedDiv shadow>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <Input.Search
            style={{ marginRight: 10 }}
            placeholder="Pequisar..."
            onChange={e => {
              if (!e.target.value) {
                setFilters({ ...filters, text: '' })
              }
            }}
            onSearch={value => {
              setFilters({ ...filters, text: value })
            }}
          />
             <Button
              style={{ marginRight: 10 }}
              onClick={() => {
                createProjectDialog()
              }}
              icon={<PlusOutlined />}
              loading={loading}
              disabled={loading}
            >
                Criar Projeto
            </Button>

          <Tooltip title="Atualizar a página">
            <Button
              onClick={() => {
                handleRefetch()
              }}
              icon={<ReloadOutlined />}
              loading={loading}
            >
              Atualizar
            </Button>
          </Tooltip>
        </div>
        <Grid
          columns="repeat(auto-fit, minmax(250px, 1fr))"
          coljustifyContent="start"
          style={{ marginTop: 10 }}
        >
          <Cell>
            <div>
              <Select
                // className="projects-page-header-mts-select"
                mode="multiple"
                defaultValue={undefined}
                placeholder="Área Temática"
                onChange={values => {
                  setFilters({
                    ...filters,
                    areasTematicas: (values as number[]) || []
                  })
                }}
                allowClear
                disabled={loading}
                style={{ width: '100%' }}
              >
                {areasTematicas &&
                  areasTematicas.map(area => (
                    <Select.Option
                      key={area.idAreaTematica}
                      value={area.idAreaTematica}
                    >
                      {area.nomeAreaTematica}
                    </Select.Option>
                  ))}
              </Select>
            </div>
          </Cell>
          <Cell>
            <div>
              <Select
                placeholder="Categoria Projeto"
                onChange={value => {
                  setFilters({ ...filters, categoriaProjeto: value as number })
                }}
                allowClear
                style={{ width: '100%' }}
              >
                <Select.Option key="project-filter-category-e-tertulia" value={1}>
                  e-Tertúlia
                </Select.Option>
                <Select.Option key="project-filter-category-video" value={2}>
                  Vídeo
                </Select.Option>
                <Select.Option key="project-filter-category-tertulia" value={3}>
                  Tertúlia
                </Select.Option>
                <Select.Option key="project-filter-category-performance" value={4}>
                  Performance
                </Select.Option>
                <Select.Option key="project-filter-category-nao-apresentara" value={5}>
                  Não Apresentará
                </Select.Option>
                
                
              </Select>
            </div>
          </Cell>
          <Cell>
            <div>
              <Select
                placeholder="Alocação"
                onChange={value => {
                  setFilters({ ...filters, allocation: value as string })
                }}
                allowClear
                style={{ width: '100%' }}
              >
                <Select.Option key="project-filter-allocated" value="allocated">
                  Alocado
                </Select.Option>
                <Select.Option
                  key="project-filter-not-allocated"
                  value="not-allocated"
                >
                  Não Alocado
                </Select.Option>
              </Select>
            </div>
          </Cell>
          <Cell>
            <div>
              <Select
                placeholder="Status"
                onChange={value => {
                  setFilters({ ...filters, status: value as string })
                  console.log('here', value);
                }}
                allowClear
                style={{ width: '100%' }}
              >
                <Select.Option
                  key="project-filter-evaluated"
                  value="AVALIADO"
                >
                  AVALIADO
                </Select.Option>
                <Select.Option
                  key="project-filter-not-evaluated"
                  value="NAO AVALIADO"
                >
                  NÃO AVALIADO
                </Select.Option>
                <Select.Option
                  key="project-filter-absent"
                  value="AUSENTE"
                >
                  AUSENTE
                </Select.Option>
              </Select>
            </div>
          </Cell>
        </Grid>
      </RoundedDiv>
    </AnimatedHeader>
  )
}

export default ProjectsPageHeader
