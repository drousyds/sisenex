import { PlusCircleOutlined, ReloadOutlined } from '@ant-design/icons'
import { Button, Input, Select, Tooltip } from 'antd'
import React from 'react'
import { Grid, Cell } from 'styled-css-grid'
import { AnimatedHeader } from '../../../components/animation'
import { ProjectsFiltersType } from '../../../hooks'

// css
import './ProjectsPage.css'

interface Props {
  filters: ProjectsFiltersType
  setFilters: (e: ProjectsFiltersType) => void
}

const AddProjectsPageHeader: React.FC<Props> = ({
  setFilters,
  filters
}) => {
  return (
    <AnimatedHeader>
      <div style={{ display: 'flex', width: '100%', flexDirection: 'column' }}>
        <span style={{ marginBottom: '10px' }}> Projetos </span>
        <Input.Search
          style={{ width: '100%' }}
          placeholder="Pequise o projeto..."
          onChange={e => {
            if (!e.target.value) {
              setFilters({ ...filters, text: '' })
            }
          }}
          onSearch={value => {
            setFilters({ ...filters, text: value })
          }}
        />
      </div>
      <Grid
        columns="repeat(auto-fit, minmax(250px, 1fr))"
        coljustifyContent="start"
        style={{ marginTop: 10 }}
      >
      </Grid>
    </AnimatedHeader>
  )
}

export default AddProjectsPageHeader