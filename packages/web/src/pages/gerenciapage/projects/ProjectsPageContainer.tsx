import React, { useMemo } from 'react'

import ProjectsTable from '../../../components/projects/ProjectsTable'
import { CREATE_CREATEPROJECT_ERROR_MESSAGE, CREATE_CREATEPROJECT_SUCCESS_MESSAGE, FETCH_PROJECTS_ERROR_MESSAGE } from '../../../constants/messages'
import { useCriarProjetosMutation, useProjectsPageContainerQuery } from '../../../generated'
import { openNotificationWithIcon } from '../../../helpers'
import {
  ProjectsFiltersType,
  useFilteredProjects,
  useFilters
} from '../../../hooks'
import ProjectsPageHeader from './ProjectsPageHeader'
import CreateProjectDialog from '../../../components/dialogs/projects/CreateProjectDialog'
import { useGenericDialog } from '../../../hooks/useGenericDialog'


// Component
const ProjectsPageContainer: React.FC = () => {
  const createProjectDialog = useGenericDialog(
    'CREATE_PRESENTATION_DIALOG'
  )

  const [filters, setFilters] = useFilters<ProjectsFiltersType>({
    text: '',
    areasTematicas: [],
    categoriaProjeto: undefined,
    allocation: ''
    
  })

  const [
    criarProjetosMutation,
    { loading: mutationLoading2 }
  ] = useCriarProjetosMutation({
    onError() {
      openNotificationWithIcon('error', CREATE_CREATEPROJECT_ERROR_MESSAGE)
      createProjectDialog.close()
    },
    onCompleted() {
       openNotificationWithIcon('success',CREATE_CREATEPROJECT_SUCCESS_MESSAGE)
      createProjectDialog.close()
    }
  })

  // Query Hook
  const { data, refetch, loading } = useProjectsPageContainerQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon('error', FETCH_PROJECTS_ERROR_MESSAGE, 5.5)
    }
  })

  const { projects, areasTematicas } = useMemo(() => {
    const { projetos, areasTematicas } = data || {}
    return {
      projects: projetos || [],
      areasTematicas: areasTematicas || []
    }
  }, [data])

  const filteredProjects = useFilteredProjects({ filters, projects })

  return (
    <div>
       {createProjectDialog.visible && (
        <CreateProjectDialog   
          visible={createProjectDialog.visible}
          handleClose={createProjectDialog.close}
          handleSubmit={async data => {
            
            const dataInicio:any = data.dataInicioProjeto[0]
            const dataFim:any = data.dataInicioProjeto[1]
            const dataAnoProjeto:any = data.anoProjeto
            const parseIdUnidade = parseInt((JSON.parse(data.idUnidade)).id)
           

            await criarProjetosMutation({
              variables: {
                input: {
                  tituloProjeto: data.tituloProjeto,
                  idCategoria: data.idCategoria,
                  codigoProjeto: data.codigoProjeto,
                  idAreaTematica: data.idAreaTematica,
                  idUnidade: parseIdUnidade,
                  dataInicioProjeto: dataInicio.format('YYYY-MM-DD'),
                  dataFimProjeto: dataFim.format('YYYY-MM-DD'),
                  anoProjeto: dataAnoProjeto.format('YYYY-MM-DD'),
                  descricaoProjeto: data.descricaoProjeto,
                  
                }
              }
            })
            await refetch()
          }}
    
        />
      )}
      

      <ProjectsPageHeader
        loading={loading}
        handleRefetch={() => refetch()}
        createProjectDialog={() => createProjectDialog.open()}
        areasTematicas={areasTematicas}
        filters={filters}
        setFilters={setFilters}
      />
      <ProjectsTable
        loading={loading}
        projects={filteredProjects}
        tableFooter={
          <div>
            Projetos: {`${filteredProjects.length} / ${projects.length}`}
          </div>
        }
      />
    </div>
  )
}

export default ProjectsPageContainer
