import { ReloadOutlined } from '@ant-design/icons'
import { Input, Tooltip, Button, Select } from 'antd'
import React from 'react'

import { AnimatedHeader } from '../../../components/animation'
import RoundedDiv from '../../../components/common/RoundedDiv'
import { UsersFiltersType } from '../../../hooks'
import { UserTypes } from '../../../types'

// Component Interface
export interface Props {
  loading: boolean
  filters: UsersFiltersType
  handleRefetch: () => void
  setFilters: (e: UsersFiltersType) => void
}

const options = [
  {
    key: UserTypes.ADMINISTRATOR,
    text: 'Administrador'
  },
  {
    key: UserTypes.REVIEWER,
    text: 'Avaliador'
  },
  {
    key: UserTypes.MANAGER,
    text: 'Gerente'
  },
  {
    key: UserTypes.MONITOR,
    text: 'Monitor'
  },
  {
    key: UserTypes.NO_ROLE,
    text: 'Sem Atribuição'
  }
]

// Component
const UsersPageHeader = ({
  handleRefetch,
  loading,
  setFilters,
  filters
}: Props) => {
  return (
    <AnimatedHeader>
      <RoundedDiv shadow>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <Input.Search
            placeholder="Pequisar..."
            style={{ marginRight: 10 }}
            onChange={e => {
              if (!e.target.value) {
                setFilters({ ...filters, text: '' })
              }
            }}
            onSearch={value => {
              setFilters({ ...filters, text: value })
            }}
          />
          <Tooltip title="Atualizar a página">
            <Button
              onClick={() => {
                handleRefetch()
              }}
              icon={<ReloadOutlined />}
              loading={loading}
              style={{ maxWidth: 248, float: 'right' }}
            >
              Atualizar
            </Button>
          </Tooltip>
        </div>

        <div
          style={{
            display: 'grid',
            gridTemplateColumns: 'repeat(auto-fill, minmax(250px, 1fr))',
            justifyContent: 'start',
            marginTop: 10
          }}
        >
          <div>
            <Select
              mode="multiple"
              defaultValue={undefined}
              placeholder="Atribuição"
              onChange={values => {
                console.log('VALUES> ', values)
                setFilters({ ...filters, roles: (values as UserTypes[]) || [] })
              }}
              allowClear
              style={{ width: '100%' }}
            >
              {options.map(({ key, text }) => (
                <Select.Option key={key} value={key}>
                  {text}
                </Select.Option>
              ))}
            </Select>
          </div>
        </div>
      </RoundedDiv>
    </AnimatedHeader>
  )
}

export default UsersPageHeader
