import React from 'react'

import UsersTable from '../../../components/users/UsersTable'
import { FETCH_USERS_ERROR_MESSAGE } from '../../../constants/messages'
import { Pessoa, useUsersPageContainerQuery } from '../../../generated'
import { openNotificationWithIcon } from '../../../helpers'
import { UsersFiltersType, useFilters, useFilteredUsers } from '../../../hooks'
import UsersPageHeader from './UsersPageHeader'

const UsersPageContainer: React.FC = () => {
  const [filters, setFilters] = useFilters<UsersFiltersType>({
    text: '',
    roles: []
  })

  // Query Hooks
  const { data, loading, refetch } = useUsersPageContainerQuery({
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onError: () => {
      openNotificationWithIcon('error', FETCH_USERS_ERROR_MESSAGE, 5.5)
    }
  })

  // Data
  let users: Pessoa[] = []
  if (data) {
    users = data.pessoas || []
  }

  const filtredUsers = useFilteredUsers({ filters, users })

  return (
    <div>
      <UsersPageHeader
        loading={loading}
        handleRefetch={() => refetch()}
        setFilters={setFilters}
        filters={filters}
      />

      <UsersTable
        loading={loading}
        users={filtredUsers}
        tableFooter={
          <div>{`Pessoas: ${filtredUsers.length} / ${users.length}`}</div>
        }
      />
    </div>
  )
}

export default UsersPageContainer
