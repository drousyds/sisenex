import { Button, Form, Input, Typography } from 'antd'
import React from 'react'
import styled from 'styled-components'

import RoundedDiv from '../../components/common/RoundedDiv'
import { useCriarReportMutation } from '../../generated'
import { openNotificationWithIcon } from '../../helpers'
import useAuth from '../../hooks/useAuth'

const Container = styled.div`
  width: 100%;
  max-width: 650px;
  margin: 0 auto;
`

interface FormData {
  emailReport: string
  conteudoReport: string
}

export const AdicionarReportPage: React.FC = () => {
  const {
    authState: { currentUser }
  } = useAuth()
  const [form] = Form.useForm<FormData>()
  const [mutate, { loading }] = useCriarReportMutation({
    onError() {
      openNotificationWithIcon('error', 'Ocorreu um erro ao enviar o chamado.')
    },
    onCompleted() {
      openNotificationWithIcon('success', 'Chamado enviado com sucesso.')
    }
  })

  const onFinish = async (data: FormData) => {
    if (!currentUser) return
    try {
      await mutate({
        variables: {
          conteudoReport: data.conteudoReport,
          emailReport: data.emailReport,
          idPessoa: currentUser.idPessoa,
          idVinculoPessoa: currentUser.idVinculoPessoa
        }
      })
      form.resetFields()
    } catch (error) {
      console.error(error)
    }
  }

  if (!currentUser) return null

  return (
    <RoundedDiv shadow style={{ height: '100%' }}>
      <Container>
        <Typography.Title level={3} style={{ marginTop: '1rem' }}>
          Ajuda
        </Typography.Title>
        <Typography.Title
          level={5}
          style={{ fontWeight: 'normal', marginBottom: '1rem' }}
        >
          {`Olá ${currentUser.nomePessoa}, precisa de ajuda? Faça um chamado para nossa equipe!`}
        </Typography.Title>

        <Form form={form} layout="vertical" onFinish={onFinish}>
          <Form.Item
            name="emailReport"
            required
            label="Email"
            rules={[
              {
                required: true,
                type: 'email',
                message: 'É necessário fornecer um email válido.'
              }
            ]}
          >
            <Input disabled={loading} placeholder="email@email.com" />
          </Form.Item>

          <Form.Item
            name="conteudoReport"
            required
            label="Mensagem"
            rules={[
              {
                required: true,
                type: 'string',
                message: 'É necessário fornecer uma mensagem.'
              }
            ]}
          >
            <Input.TextArea disabled={loading} placeholder="Mensagem..." />
          </Form.Item>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              loading={loading}
              disabled={loading}
            >
              Enviar
            </Button>
          </Form.Item>
        </Form>
      </Container>
    </RoundedDiv>
  )
}
