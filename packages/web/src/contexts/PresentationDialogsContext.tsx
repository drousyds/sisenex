import React, { createContext } from 'react'

import { useGenericDialog } from '../hooks/useGenericDialog'

interface ContextType {
  createPresentationDialog: {
    open: () => void
    close: () => void
    visible: boolean
  }
}

const PresentationDialogsContext = createContext<ContextType>({} as ContextType)

export const PresentationDialogsProvider: React.FC = (props) => {
  const createPresentationDialog = useGenericDialog('Create Presentation Dialog')

  return (
    <PresentationDialogsContext.Provider
      value={{
        createPresentationDialog
      }}
    >
      {props.children}
    </PresentationDialogsContext.Provider>
  )
}

export default PresentationDialogsContext
