import { Modal } from 'antd'
import React, { createContext, useState, useEffect, useCallback } from 'react'
import { useHistory } from 'react-router-dom'

import { getVinculos } from '../api/sti'
import { createUser, getUser } from '../api/user'
import FullSpinner from '../components/FullSpinner/FullSpinner'
import UserModeSelector from '../components/UserModeSelector/UserModeSelector'
import SelectVinculo from '../components/vinculos/SelectVinculo'
import { Pessoa } from '../generated'
import { useLogger } from '../hooks/useLogger'
import { mockFakeVinculo, mockFakeVinculos } from '../pages/authpage/mockFakeVinculos'
import {
  saveScreenModeLocalStorage,
  saveUserLocalStorage,
  removeScreenModeLocalStorage,
  removeUserLocalStorage,
  loadScreenModeLocalStorage,
  loadUserLocalStorage
} from '../services/localStorage.services'
import { ModeType } from '../types'

export type ScreenModeType = 'AVALIADOR' | 'GERENTE'

interface AuthState {
  currentUser?: Pessoa
  mode?: ScreenModeType
}

interface AuthContextType {
  authLoading: boolean
  authState: AuthState
  setScreenMode: (mode: ScreenModeType) => void
  localLogout: () => void
  isAuthenticated: () => boolean
  isGerente: () => boolean
  isAvaliador: () => boolean
  authPageLogin: (code: string) => Promise<void>
}

const AuthContext = createContext<AuthContextType>({} as AuthContextType)

export const AuthProvider: React.FC = ({ children }) => {
  const [authLoading, setAuthLoading] = useState(true)
  const [multipleVinculos, setMultipleVinculos] = useState<Pessoa[]>([])
  const [toSelectModeUser, setToSelectModeUser] = useState<Pessoa | undefined>(undefined)

  const [authState, setAuthStateInfo] = useState<AuthState>({
    currentUser: undefined,
    mode: undefined
  })
  const history = useHistory()
  const logger = useLogger('AuthContext')

  const clearCache = () => {
    removeUserLocalStorage()
    removeScreenModeLocalStorage()
  }

  const localLogout = async () => {
    setAuthStateInfo({ currentUser: undefined, mode: undefined })
    clearCache()
    history.push('/')
  }

  const makeUserTypeLogin = useCallback((user: Pessoa) => {
    if (user.gerentePessoa || user.avaliadorPessoa) {
      logger.info('User has privileges')
      const payload: { user: Pessoa, mode: ScreenModeType} = { user, mode: 'GERENTE' }
      if (user.avaliadorPessoa && !user.gerentePessoa) {
        payload.mode = 'AVALIADOR'
      }
      return payload
    } else {
      logger.info('User does not have privileges')
    }
  }, [logger])

  const isAuthenticated = () => {
    return !!authState.currentUser
  }

  const hasRole = (role: 'gerentePessoa' | 'avaliadorPessoa' | 'monitorPessoa') => {
    if (authState.currentUser) {
      return Boolean(authState.currentUser[role])
    }
    return false
  }

  const isGerente = () => {
    return hasRole('gerentePessoa')
  }

  const isAvaliador = () => {
    return hasRole('avaliadorPessoa')
  }

  const setScreenMode = (mode: ScreenModeType) => {
    saveScreenModeLocalStorage(mode)
    setAuthStateInfo((newState) => ({ ...newState, mode }))
  }

  // using api
  const getRedirectURL = (user: Pessoa) => {
    if (user.avaliadorPessoa && !user.gerentePessoa) {
      return '/avaliador/home'
    }
    return '/gerente/home'
  }

  // using
  const handleCreateUser = async (pessoa: Pessoa, idCategoria: ModeType['id'] | undefined) => {
    const user = await createUser(pessoa, idCategoria)
    if (user) {
      history.push('/cadastro/sucesso', { user })
    } else {
      history.push('/login/erro', {
        error: true,
        message: {
          title: 'Ops! Algo de inesperado ocorreu',
          subtitle: 'Ocorreu um erro ao fazer o login, tente novamente.'
        }
      })
    }
  }

  // using
  const makeVinculoLogin = async (vinculo: Pessoa) => {
    // pegar user do banco de dados
    const pessoa = await getUser(`${vinculo.idPessoa}`, `${vinculo.idVinculoPessoa}`)
    // pessoa não esta no banco
    if (!pessoa) {
      await handleCreateUser(vinculo, 1)
      setAuthLoading(false)
    } else {
      // pessoa esta no banco
      const login = makeUserTypeLogin(pessoa)

      // pessoa tem acesso ao sistema
      if (login) {
        const redirectURL = getRedirectURL(login.user)
        logger.info(`Redirecting to: ${redirectURL}`)
        saveUserLocalStorage(login.user)
        saveScreenModeLocalStorage(login.mode)
        setAuthStateInfo({ currentUser: login.user, mode: login.mode })
        history.push(redirectURL)
        setAuthLoading(false)
      } else {
        // pessoa nao tem acesso ao sistema
        history.push('/login/erro', {
          error: true,
          message: {
            title: 'Você não tem acesso a esse módulo.',
            subtitle: 'Se você for um gerente, contate a coordenação'
          }
        })
        setAuthLoading(false)
      }
    }
  }

  // using
  const getVinculosFromCode = (code: string) => {
    return getVinculos(code)
  }

  // using
  const authPageLogin = async (code: string) => {
    setAuthLoading(true)
    const vinculos = await getVinculosFromCode(code)
    // const vinculos = mockFakeVinculos() // se quiser mockar multiplos vinculos
    // const vinculos = [] // se quiser mockar sem vinculos

    if (!vinculos || (vinculos && vinculos.length === 0)) {
      history.push('/login/erro', {
        error: true,
        message: {
          title: 'Ops! Algo de inesperado ocorreu',
          subtitle: 'Ocorreu um erro ao fazer o login, tente novamente.'
        }
      })
      setAuthLoading(false)
    } else if (vinculos.length === 1) {
      await makeVinculoLogin(vinculos[0])
    } else {
      setMultipleVinculos(vinculos)
    }
  }

  const handleToModeClick = async (toCreateUser: Pessoa, idCategoria: ModeType['id']) => {
    await handleCreateUser(toCreateUser, idCategoria)
    setAuthLoading(false)
  }

  // to use
  useEffect(() => {
    async function loadUser () {
      const shouldUseLocalTestUser = true
      if (shouldUseLocalTestUser && process.env.REACT_APP_NODE_ENV === 'local') {
        setAuthStateInfo({ currentUser: mockFakeVinculo(), mode: 'GERENTE' })
        setAuthLoading(false)
      } else {
        const user = loadUserLocalStorage()

        if (!user) {
          clearCache()
          setAuthLoading(false)
          return
        }

        const fromApiUser = await getUser(`${user.idPessoa}`, `${user.idVinculoPessoa}`)

        if (!fromApiUser) {
          clearCache()
          setAuthLoading(false)
          return
        }

        const login = makeUserTypeLogin(fromApiUser)

        if (!login) {
          clearCache()
          setAuthLoading(false)
          return
        }

        if (fromApiUser.gerentePessoa && fromApiUser.avaliadorPessoa) {
          const mode = loadScreenModeLocalStorage()
          if (mode && (mode === 'GERENTE' || mode === 'AVALIADOR')) {
            login.mode = mode
          }
        } else {
          removeScreenModeLocalStorage()
        }

        saveUserLocalStorage(login.user)
        saveScreenModeLocalStorage(login.mode)
        setAuthStateInfo({ currentUser: login.user, mode: login.mode })
        setAuthLoading(false)
      }
    }

    loadUser()
  }, [makeUserTypeLogin])

  if (toSelectModeUser) {
    return (
      <Modal visible={!!toSelectModeUser}
        footer={null}
        closable={false}
      >
        <UserModeSelector
          onModeClick={async (mode) => {
            await handleToModeClick(toSelectModeUser, mode.id)
            setToSelectModeUser(undefined)
          }}
        />
      </Modal>
    )
  }

  if (multipleVinculos && multipleVinculos.length > 1) {
    return (
      <Modal visible={ multipleVinculos.length > 1}
        footer={null}
        closable={false}
      >
        <SelectVinculo
          vinculos={multipleVinculos}
          onVinculoClick={async (v) => {
            setMultipleVinculos([])
            await makeVinculoLogin(v)
          }}
        />
      </Modal>
    )
  }

  if (authLoading) {
    return <FullSpinner />
  }

  return (
    <AuthContext.Provider value={{
      authState,
      authLoading,
      localLogout,
      isAuthenticated,
      isGerente,
      isAvaliador,
      setScreenMode,
      authPageLogin
    }}>
      {children}
    </AuthContext.Provider>
  )
}

export default AuthContext
