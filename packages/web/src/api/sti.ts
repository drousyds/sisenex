import axios from 'axios'

import {
  GetStiTokenMutation,
  GetStiTokenMutationVariables,
  GetStiTokenDocument
} from '../generated'
import { client } from '../graphql'
import mapVinculos from '../helpers/mapVinculos'

export async function getSTITokenFromBackend (code: string) {
  try {
    const { data } = await client.mutate<GetStiTokenMutation, GetStiTokenMutationVariables>({
      variables: { code },
      mutation: GetStiTokenDocument
    })
    const token = data?.autenticar.access_token

    return token || undefined
  } catch (e) {
    console.log('Erro ao buscar token', e)
    return undefined
  }
}

export async function getVinculosFromSTI (token: string) {
  try {
    const res = await axios.get(
      'https://api.ufpb.br/api/personal/vinculos/ativos',
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    )
    const { data } = res
    return data ? mapVinculos(data) : undefined
  } catch (e) {
    console.log('Erro ao buscar vinculo', e)
    return undefined
  }
}

export async function getVinculos (code: string) {
  try {
    const token = await getSTITokenFromBackend(code)

    if (!token) {
      return undefined
    }
    const vinculos = await getVinculosFromSTI(token)
    if (vinculos) return vinculos
  } catch (error) {
    console.log('UE - Erro ao buscar os vinculos', error)
  }
}
