import { Pessoa, GetPessoaQuery, GetPessoaQueryVariables, GetPessoaDocument, CreateUserDocument, CreateUserMutation, CreateUserMutationVariables } from '../generated'
import { client } from '../graphql'

export async function getUser (idPessoa: string, idVinculoPessoa: string) {
  try {
    const { data } = await client.query<GetPessoaQuery, GetPessoaQueryVariables>({
      variables: {
        idPessoa,
        idVinculoPessoa
      },
      query: GetPessoaDocument
    })
    const { pessoa } = data
    return pessoa || undefined
  } catch (error) {
    console.log('Error get user', error)
    return undefined
  }
}

export async function createUser (vinculo: Pessoa, idCategoria: 1 | 2 | undefined) {
  try {
    const { data } = await client.mutate<CreateUserMutation, CreateUserMutationVariables>({
      mutation: CreateUserDocument,
      variables: {
        input: {
          ...vinculo,
          idPessoa: `${vinculo.idPessoa}`,
          idVinculoPessoa: `${vinculo.idVinculoPessoa}`,
          matriculaPessoa: `${vinculo.matriculaPessoa}`,
          lotacaoPessoa: vinculo.lotacaoPessoa || undefined,
          gerentePessoa: 0,
          idCategoria
        }
      }
    })
    return data?.criarPessoa || undefined
  } catch (error) {
    console.log('Error creating user', error)
    return undefined
  }
}
