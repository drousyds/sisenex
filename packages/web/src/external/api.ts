import axios from 'axios'

import { GetStiTokenDocument, GetStiTokenMutation, GetStiTokenMutationVariables, Pessoa } from '../generated'
import { client } from '../graphql'
import mapVinculos from '../helpers/mapVinculos'

export async function oAuthGetToken (code: string): Promise<string|undefined> {
  try {
    const { data } = await client.mutate<GetStiTokenMutation, GetStiTokenMutationVariables>({
      variables: { code },
      mutation: GetStiTokenDocument
    })
    return data?.autenticar.access_token
  } catch (e) {
    console.log('ERROR - GET_TOKEN', e)
  }
}

async function oAuthGetVinculo (token: string): Promise<Pessoa[] | false> {
  try {
    const res = await axios.get(
      'https://api.ufpb.br/api/personal/vinculos/ativos',
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    )
    // console.log(res.data)
    const { data } = res
    return data ? mapVinculos(data) : false
  } catch (e) {
    console.log('ERROR - GET_VINCULO_API_CALL', e)
    return false
  }
}

export { oAuthGetVinculo }
