export class Logger {
  constructor (private readonly filename: string) {}

  private createMessage (message: string) {
    return `${this.filename} | ${message}`
  }

  info (message: string) {
    if (process.env.REACT_APP_NODE_ENV === 'local') {
      console.log(this.createMessage(message))
    }
  }

  warn (message: string) {
    if (process.env.REACT_APP_NODE_ENV === 'local') {
      console.warn(this.createMessage(message))
    }
  }

  error (message: string) {
    if (process.env.REACT_APP_NODE_ENV === 'local') {
      console.error(this.createMessage(message))
    }
  }
}
