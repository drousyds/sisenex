import { History } from 'history'

import {
  Pessoa,
  GetPessoaQuery,
  GetPessoaQueryVariables,
  GetPessoaDocument, CreateUserMutation, CreateUserDocument, CreateUserMutationVariables
} from '../generated'
import { client } from '../graphql'
import { openNotificationWithIcon } from '../helpers'
import { saveUserLocalStorage } from '../services/localStorage.services'

const loginAuthUser = async (
  vinculo: Pessoa,
  history: History<any>,
  loginUserAction: (user: any) => void
) => {
  const { data, errors } = await client.query<
    GetPessoaQuery,
    GetPessoaQueryVariables
  >({
    variables: {
      idPessoa: `${vinculo.idPessoa}`,
      idVinculoPessoa: `${vinculo.idVinculoPessoa}`
    },
    query: GetPessoaDocument
  })

  // an error occour in the query
  if (errors) {
    openNotificationWithIcon('error', 'Ocorreu um erro ao fazer o login', 5.5)
    history.push('/')
  }

  const user = data.pessoa

  if (user) {
    // user is in the database
    if (user.gerentePessoa) {
      // user has privileges to access the module
      loginUserAction(user)
      openNotificationWithIcon('success', 'Login realizado com successo.', 5.5)
      history.push('/')
      saveUserLocalStorage(user)
    } else {
      // user has no privileges to access module
      history.push('/login/erro', {
        error: true,
        message: {
          title: 'Você não tem acesso a esse módulo.',
          subtitle: 'Se você for um gerente, contate a coordenação'
        },
        type: 'UNAUTHORIZED_USER'
      })
    }
  } else {
    // user is not in the database
    const { data } = await client.mutate<CreateUserMutation, CreateUserMutationVariables>({
      mutation: CreateUserDocument,
      variables: {
        input: {
          ...vinculo,
          idPessoa: `${vinculo.idPessoa}`,
          idVinculoPessoa: `${vinculo.idVinculoPessoa}`,
          matriculaPessoa: `${vinculo.matriculaPessoa}`,
          lotacaoPessoa: vinculo.lotacaoPessoa || undefined,
          gerentePessoa: 0
        }
      }
    })
    if (data?.criarPessoa) {
      history.push('/cadastro/sucesso', { user: data?.criarPessoa })
    } else {
      openNotificationWithIcon('error', 'Ocorreu um erro ao fazer o login', 5.5)
      history.push('/')
    }
  }
}

export default loginAuthUser
