import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as React from 'react';
import * as ApolloReactComponents from '@apollo/react-components';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Upload: any;
};

export type Administrador = Pessoa & {
  __typename?: 'Administrador';
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  matriculaPessoa?: Maybe<Scalars['String']>;
  nomePessoa?: Maybe<Scalars['String']>;
  nomeSocialPessoa?: Maybe<Scalars['String']>;
  lotacaoPessoa?: Maybe<Scalars['String']>;
  emailPessoa?: Maybe<Scalars['String']>;
  telefonePessoa?: Maybe<Scalars['String']>;
  aptidaoPessoa: Scalars['Int'];
  avaliadorPessoa?: Maybe<Scalars['Int']>;
  monitorPessoa?: Maybe<Scalars['Int']>;
  gerentePessoa?: Maybe<Scalars['Int']>;
  administradorPessoa?: Maybe<Scalars['Int']>;
  avatarUrl?: Maybe<Scalars['String']>;
  projetosParaAvaliar?: Maybe<Array<Projeto>>;
  apresentacoesAlocacao?: Maybe<Array<Apresentacao>>;
  avaliacoes?: Maybe<Array<Avaliacao>>;
  categoria?: Maybe<Categoria>;
  created_at?: Maybe<Scalars['String']>;
};


export type AdministradorAvaliacoesArgs = {
  idProjeto?: Maybe<Scalars['Int']>;
};

export type AlocarProjetoInput = {
  eventoHorarioInicial: Scalars['String'];
  eventoHorarioFinal: Scalars['String'];
  numeroDeSalas: Scalars['Int'];
  numeroProjetosSala: Scalars['Int'];
  horarios?: Maybe<Array<Scalars['String']>>;
  idEvento: Scalars['Int'];
  idAreaTematica: Scalars['Int'];
};

export type AlocarProjetoVideoInput = {
  horarioInicial: Scalars['String'];
  numeroProjetosApresentacao: Scalars['Int'];
  idAreaTematica: Scalars['Int'];
};

export type Apresentacao = {
  __typename?: 'Apresentacao';
  idApresentacao: Scalars['Int'];
  id2Apresentacao: Scalars['String'];
  salaApresentacao?: Maybe<Scalars['String']>;
  horaApresentacao?: Maybe<Scalars['String']>;
  dataInicioApresentacao?: Maybe<Scalars['String']>;
  dataFimApresentacao?: Maybe<Scalars['String']>;
  disponibilidadeApresentacao?: Maybe<Scalars['Int']>;
  codigoApresentacao: Scalars['String'];
  modalidadeApresentacao?: Maybe<Scalars['String']>;
  projetos?: Maybe<Array<Projeto>>;
  avaliadores?: Maybe<Array<Avaliador>>;
  areaTematica?: Maybe<AreaTematica>;
  evento?: Maybe<Evento>;
  categoria: Categoria;
  linkApresentacao?: Maybe<Scalars['String']>;
};

export type ApresentacaoInput = {
  idApresentacao?: Maybe<Scalars['Int']>;
  id2Apresentacao?: Maybe<Scalars['String']>;
  codigoApresentacao?: Maybe<Scalars['String']>;
};

export type ApresentacoesInput = {
  idEvento?: Maybe<Scalars['Int']>;
  idAreaTematica?: Maybe<Scalars['Int']>;
  modalidadeApresentacao?: Maybe<Scalars['String']>;
};

export type AreaTematica = {
  __typename?: 'AreaTematica';
  idAreaTematica: Scalars['Int'];
  nomeAreaTematica: Scalars['String'];
  projetos?: Maybe<Array<Projeto>>;
};


export type AreaTematicaProjetosArgs = {
  idEvento?: Maybe<Scalars['Int']>;
};

export type AtualizarApresentacaoInput = {
  idApresentacao: Scalars['Int'];
  id2Apresentacao: Scalars['String'];
  salaApresentacao?: Maybe<Scalars['String']>;
  horaApresentacao?: Maybe<Scalars['String']>;
  dataInicioApresentacao?: Maybe<Scalars['String']>;
  dataFimApresentacao?: Maybe<Scalars['String']>;
  disponibilidadeApresentacao?: Maybe<Scalars['Int']>;
  modalidadeApresentacao?: Maybe<Scalars['String']>;
  idAreaTematica?: Maybe<Scalars['Int']>;
  idEvento?: Maybe<Scalars['Int']>;
  idCategoria?: Maybe<Scalars['Int']>;
  linkApresentacao?: Maybe<Scalars['String']>;
};
export type AdicionarProjetosInput = {
  idApresentacao: Scalars['Int'];
  id2Apresentacao?: Maybe<Scalars['String']>;
  idProjeto: Scalars['Int'];
  
};
export type AtualizarAreaTematica = {
  idAreaTematica: Scalars['Int'];
  nomeAreaTematica: Scalars['String'];
};

export type AtualizarCampusInput = {
  idCampus: Scalars['Int'];
  nomeCampus?: Maybe<Scalars['String']>;
};

export type AtualizarEventoInput = {
  idEvento: Scalars['Int'];
  nomeEvento?: Maybe<Scalars['String']>;
  areasTematicas?: Maybe<Array<Scalars['Int']>>;
  descricaoEvento?: Maybe<Scalars['String']>;
  dataInicioEvento?: Maybe<Scalars['String']>;
  dataFimEvento?: Maybe<Scalars['String']>;
  statusEvento?: Maybe<Scalars['String']>;
};

export type AtualizarPessoaInput = {
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  matriculaPessoa?: Maybe<Scalars['String']>;
  nomePessoa?: Maybe<Scalars['String']>;
  nomeSocialPessoa?: Maybe<Scalars['String']>;
  lotacaoPessoa?: Maybe<Scalars['String']>;
  emailPessoa?: Maybe<Scalars['String']>;
  telefonePessoa?: Maybe<Scalars['String']>;
  aptidaoPessoa?: Maybe<Scalars['Int']>;
  avaliadorPessoa?: Maybe<Scalars['Int']>;
  monitorPessoa?: Maybe<Scalars['Int']>;
  gerentePessoa?: Maybe<Scalars['Int']>;
  administradorPessoa?: Maybe<Scalars['Int']>;
  avatarUrl?: Maybe<Scalars['String']>;
  idCategoria?: Maybe<Scalars['Int']>;
};

export type AtualizarProjetoInput = {
  idProjeto: Scalars['Int'];
  tituloProjeto?: Maybe<Scalars['String']>;
  codigoProjeto?: Maybe<Scalars['String']>;
  descricaoProjeto?: Maybe<Scalars['String']>;
  dataInicioProjeto?: Maybe<Scalars['String']>;
  dataFimProjeto?: Maybe<Scalars['String']>;
  anoProjeto?: Maybe<Scalars['String']>;
  idAreaTematica?: Maybe<Scalars['Int']>;
  modalidadeProjeto?: Maybe<Scalars['String']>;
  idUnidade?: Maybe<Scalars['Int']>;
  disponibilidadeProjeto?: Maybe<Scalars['Int']>;
  liberacaoProjeto?: Maybe<Scalars['String']>;
  idApresentacao?: Maybe<Scalars['Int']>;
  id2Apresentacao?: Maybe<Scalars['String']>;
  idEvento?: Maybe<Scalars['Int']>;
  idCategoria?: Maybe<Scalars['Int']>;
  linkArtefato?: Maybe<Scalars['String']>;
  statusProjeto?: Maybe<Scalars['String']>;
};

export type AtualizarSolicitacaoLgpdInput = {
  idSolicitacao: Scalars['Int'];
  tipoSolicitacao?: Maybe<Scalars['Int']>;
  textoSolicitacao?: Maybe<Scalars['String']>;
  idPessoa?: Maybe<Scalars['String']>;
  idVinculoPessoa?: Maybe<Scalars['String']>;
  nomeSolicitacao?: Maybe<Scalars['String']>;
  sobrenomeSolicitacao?: Maybe<Scalars['String']>;
  emailPessoa?: Maybe<Scalars['String']>;
  statusSolicitacao?: Maybe<Scalars['String']>;
};

export type AtualizarStatusReportInput = {
  idReport: Scalars['Int'];
  statusReport: Scalars['String'];
};

export type AtualizarUnidadeInput = {
  idUnidade: Scalars['Int'];
  codigoUnidade?: Maybe<Scalars['String']>;
  nomeUnidade?: Maybe<Scalars['String']>;
  siglaUnidade?: Maybe<Scalars['String']>;
  hierarquiaUnidade?: Maybe<Scalars['String']>;
  unidadeGestora?: Maybe<Scalars['Int']>;
  tipoUnidade?: Maybe<Scalars['String']>;
  idCampus?: Maybe<Scalars['Int']>;
};

export type Avaliacao = {
  __typename?: 'Avaliacao';
  media: Scalars['Float'];
  notas: Array<Maybe<NotaPergunta>>;
  projeto: Projeto;
  avaliador: Pessoa;
  avaliacaoManual?: Maybe<Scalars['Int']>;
  nomeAvaliadorPapel?: Maybe<Scalars['String']>;
};

export type AvaliacaoInput = {
  notas: Array<Maybe<NotaPerguntaInput>>;
  idProjeto: Scalars['Int'];
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
};

export type Avaliador = Pessoa & {
  __typename?: 'Avaliador';
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  matriculaPessoa?: Maybe<Scalars['String']>;
  nomePessoa?: Maybe<Scalars['String']>;
  nomeSocialPessoa?: Maybe<Scalars['String']>;
  lotacaoPessoa?: Maybe<Scalars['String']>;
  emailPessoa?: Maybe<Scalars['String']>;
  telefonePessoa?: Maybe<Scalars['String']>;
  aptidaoPessoa: Scalars['Int'];
  avaliadorPessoa?: Maybe<Scalars['Int']>;
  monitorPessoa?: Maybe<Scalars['Int']>;
  gerentePessoa?: Maybe<Scalars['Int']>;
  administradorPessoa?: Maybe<Scalars['Int']>;
  avatarUrl?: Maybe<Scalars['String']>;
  projetosParaAvaliar?: Maybe<Array<Projeto>>;
  apresentacoesAlocacao?: Maybe<Array<Apresentacao>>;
  avaliacoes?: Maybe<Array<Avaliacao>>;
  categoria?: Maybe<Categoria>;
  created_at?: Maybe<Scalars['String']>;
};


export type AvaliadorAvaliacoesArgs = {
  idProjeto?: Maybe<Scalars['Int']>;
};

export type AvaliarProjetoInput = {
  avaliacao: AvaliacaoInput;
};

export type AvaliarProjetoPorUmAvaliadorInput = {
  idPessoaGerente: Scalars['String'];
  idVinculoPessoaGerente: Scalars['String'];
  avaliacao: AvaliacaoInput;
  nomeAvaliadorPapel: Scalars['String'];
};

export type BaterAvaliadorInput = {
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  idApresentacao: Scalars['Int'];
  id2Apresentacao: Scalars['String'];
};

export enum CacheControlScope {
  Public = 'PUBLIC',
  Private = 'PRIVATE'
}

export type Campus = {
  __typename?: 'Campus';
  idCampus: Scalars['Int'];
  nomeCampus?: Maybe<Scalars['String']>;
  unidades?: Maybe<Array<Unidade>>;
};

export type Categoria = {
  __typename?: 'Categoria';
  idCategoria: Scalars['Int'];
  nomeCategoria: Scalars['String'];
  perguntas?: Maybe<Array<Pergunta>>;
};

export type CriarApresentacaoInput = {
  id2Apresentacao: Scalars['String'];
  salaApresentacao: Scalars['String'];
  horaApresentacao: Scalars['String'];
  dataInicioApresentacao?: Maybe<Scalars['String']>;
  dataFimApresentacao?: Maybe<Scalars['String']>;
  disponibilidadeApresentacao?: Maybe<Scalars['Int']>;
  modalidadeApresentacao?: Maybe<Scalars['String']>;
  idAreaTematica?: Maybe<Scalars['Int']>;
  idEvento?: Maybe<Scalars['Int']>;
  idCategoria: Scalars['Int'];
  linkApresentacao?: Maybe<Scalars['String']>;
};

export type CriarAreaTematica = {
  idAreaTematica: Scalars['Int'];
  nomeAreaTematica: Scalars['String'];
};

export type CriarCampusInput = {
  idCampus: Scalars['Int'];
  nomeCampus: Scalars['String'];
};

export type CriarEventoInput = {
  nomeEvento: Scalars['String'];
  campi: Array<Scalars['Int']>;
  areasTematicas?: Maybe<Array<Scalars['Int']>>;
  descricaoEvento?: Maybe<Scalars['String']>;
  dataInicioEvento: Scalars['String'];
  dataFimEvento: Scalars['String'];
  statusEvento: Scalars['String'];
};

export type CriarPerguntaInput = {
  idPergunta: Scalars['Int'];
  idCategoria: Scalars['Int'];
  conteudoPergunta: Scalars['String'];
  dataPergunta?: Maybe<Scalars['String']>;
};

export type CriarPessoaInput = {
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  matriculaPessoa?: Maybe<Scalars['String']>;
  nomePessoa?: Maybe<Scalars['String']>;
  nomeSocialPessoa?: Maybe<Scalars['String']>;
  lotacaoPessoa?: Maybe<Scalars['String']>;
  emailPessoa?: Maybe<Scalars['String']>;
  telefonePessoa?: Maybe<Scalars['String']>;
  aptidaoPessoa?: Maybe<Scalars['Int']>;
  avaliadorPessoa?: Maybe<Scalars['Int']>;
  monitorPessoa?: Maybe<Scalars['Int']>;
  gerentePessoa?: Maybe<Scalars['Int']>;
  administradorPessoa?: Maybe<Scalars['Int']>;
  avatarUrl?: Maybe<Scalars['String']>;
  idCategoria?: Maybe<Scalars['Int']>;
};

// export type CriarProjetoInput2 = {
//   idProjeto: Scalars['Int'];
//   tituloProjeto: Scalars['String'];
//   descricaoProjeto: Scalars['String'];
//   dataInicioProjeto: Scalars['String'];
//   dataFimProjeto: Scalars['String'];
//   anoProjeto: Scalars['String'];
//   idAreaTematica: Scalars['Int'];
//   idUnidade: Scalars['Int'];
//   disponibilidadeProjeto?: Maybe<Scalars['Int']>;
//   codigoProjeto?: Maybe<Scalars['String']>;
//   modalidadeProjeto?: Maybe<Scalars['String']>;
//   liberacaoProjeto?: Maybe<Scalars['String']>;
//   idApresentacao?: Maybe<Scalars['Int']>;
//   id2Apresentacao?: Maybe<Scalars['String']>;
//   idCategoria: Scalars['Int'];
//   linkArtefato?: Maybe<Scalars['String']>;
//   statusProjeto?: Maybe<Scalars['String']>;
// };
export type CriarProjetoInput = {
 
  tituloProjeto:  Scalars['String'];  
  idCategoria: Scalars['Int'];
  codigoProjeto:  Scalars['String'];
  idAreaTematica?: Scalars['Int'];
  idUnidade: Scalars['Int'];
  descricaoProjeto: Scalars['String'];
  dataInicioProjeto: Scalars['String'];
  dataFimProjeto: Scalars['String'];
  anoProjeto: Scalars['String'];

};

export type CriarReportInput = {
  conteudoReport: Scalars['String'];
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  emailReport: Scalars['String'];
  idApresentacao?: Maybe<Scalars['Int']>;
  id2Apresentacao?: Maybe<Scalars['String']>;
};

export type CriarSolicitacaoLgpdInput = {
  tipoSolicitacao: Scalars['Int'];
  textoSolicitacao?: Maybe<Scalars['String']>;
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  nomeSolicitacao: Scalars['String'];
  sobrenomeSolicitacao: Scalars['String'];
  emailPessoa: Scalars['String'];
};

export type CriarUnidadeInput = {
  idUnidade: Scalars['Int'];
  codigoUnidade: Scalars['String'];
  nomeUnidade: Scalars['String'];
  siglaUnidade: Scalars['String'];
  hierarquiaUnidade: Scalars['String'];
  unidadeGestora: Scalars['Int'];
  tipoUnidade: Scalars['String'];
  idCampus: Scalars['Int'];
};

export type EnviarEmailParaAvaliadoresInput = {
  idApresentacao: Scalars['Int'];
  id2Apresentacao: Scalars['String'];
};

export type Evento = {
  __typename?: 'Evento';
  idEvento: Scalars['Int'];
  nomeEvento: Scalars['String'];
  campi: Array<Campus>;
  descricaoEvento?: Maybe<Scalars['String']>;
  dataInicioEvento?: Maybe<Scalars['String']>;
  dataFimEvento?: Maybe<Scalars['String']>;
  areasTematicas: Array<AreaTematica>;
  apresentacoes?: Maybe<Array<Apresentacao>>;
  projetos?: Maybe<Array<Projeto>>;
  statusEvento: Scalars['String'];
};


export type EventoApresentacoesArgs = {
  idAreaTematica?: Maybe<Scalars['Int']>;
  modalidadeApresentacao?: Maybe<Scalars['String']>;
};

export type ExcluirApresentacaoInput = {
  idApresentacao: Scalars['Int'];
  id2Apresentacao: Scalars['String'];
};

export type ExcluirCampusInput = {
  idCampus: Scalars['Int'];
};

export type ExcluirPessoaInput = {
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
};

export type ExcluirReportInput = {
  idReport: Scalars['Int'];
};

export type ExcluirUnidadeInput = {
  idUnidade: Scalars['Int'];
};

export type FecharApresentacaoInput = {
  codigoApresentacao: Scalars['String'];
};

export type FecharProjetoInput = {
  idProjeto: Scalars['Int'];
};

export type FecharProjetoParaUmAvaliadorInput = {
  idProjeto: Scalars['Int'];
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
};

export type FinalizarSolicitacoesLgpdInput = {
  idSolicitacao: Scalars['Int'];
};

export type Gerente = Pessoa & {
  __typename?: 'Gerente';
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  matriculaPessoa?: Maybe<Scalars['String']>;
  nomePessoa?: Maybe<Scalars['String']>;
  nomeSocialPessoa?: Maybe<Scalars['String']>;
  lotacaoPessoa?: Maybe<Scalars['String']>;
  emailPessoa?: Maybe<Scalars['String']>;
  telefonePessoa?: Maybe<Scalars['String']>;
  aptidaoPessoa: Scalars['Int'];
  avaliadorPessoa?: Maybe<Scalars['Int']>;
  monitorPessoa?: Maybe<Scalars['Int']>;
  gerentePessoa?: Maybe<Scalars['Int']>;
  administradorPessoa?: Maybe<Scalars['Int']>;
  avatarUrl?: Maybe<Scalars['String']>;
  projetosParaAvaliar?: Maybe<Array<Projeto>>;
  apresentacoesAlocacao?: Maybe<Array<Apresentacao>>;
  avaliacoes?: Maybe<Array<Avaliacao>>;
  categoria?: Maybe<Categoria>;
  created_at?: Maybe<Scalars['String']>;
};


export type GerenteAvaliacoesArgs = {
  idProjeto?: Maybe<Scalars['Int']>;
};

export type IniciarApresentacaoInput = {
  codigoApresentacao: Scalars['String'];
};

export type LiberarProjetoInput = {
  idProjeto: Scalars['Int'];
};

export type LiberarProjetoParaUmAvaliadorInput = {
  idProjeto: Scalars['Int'];
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
};

export type LoginInput = {
  digitoVerificador: Scalars['String'];
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  matriculaPessoa?: Maybe<Scalars['String']>;
  nomePessoa?: Maybe<Scalars['String']>;
  nomeSocialPessoa?: Maybe<Scalars['String']>;
  lotacaoPessoa?: Maybe<Scalars['String']>;
  emailPessoa?: Maybe<Scalars['String']>;
  telefonePessoa?: Maybe<Scalars['String']>;
  aptidaoPessoa?: Maybe<Scalars['Int']>;
  avaliadorPessoa?: Maybe<Scalars['Int']>;
  monitorPessoa?: Maybe<Scalars['Int']>;
  gerentePessoa?: Maybe<Scalars['Int']>;
  administradorPessoa?: Maybe<Scalars['Int']>;
  avatarUrl?: Maybe<Scalars['String']>;
};

export type LoginOutput = {
  __typename?: 'loginOutput';
  token: Scalars['String'];
  pessoa: Pessoa;
};

export type MediaPorAvaliador = {
  __typename?: 'mediaPorAvaliador';
  avaliador?: Maybe<Pessoa>;
  mediaProjeto?: Maybe<Scalars['Float']>;
};

export type Membro = {
  __typename?: 'Membro';
  idProjeto: Scalars['Int'];
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  funcao: Scalars['String'];
  nomePessoa?: Maybe<Scalars['String']>;
};

export type Monitor = Pessoa & {
  __typename?: 'Monitor';
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  matriculaPessoa?: Maybe<Scalars['String']>;
  nomePessoa?: Maybe<Scalars['String']>;
  nomeSocialPessoa?: Maybe<Scalars['String']>;
  lotacaoPessoa?: Maybe<Scalars['String']>;
  emailPessoa?: Maybe<Scalars['String']>;
  telefonePessoa?: Maybe<Scalars['String']>;
  aptidaoPessoa: Scalars['Int'];
  avaliadorPessoa?: Maybe<Scalars['Int']>;
  monitorPessoa?: Maybe<Scalars['Int']>;
  gerentePessoa?: Maybe<Scalars['Int']>;
  administradorPessoa?: Maybe<Scalars['Int']>;
  avatarUrl?: Maybe<Scalars['String']>;
  apresentacoesAlocacao?: Maybe<Array<Apresentacao>>;
  categoria?: Maybe<Categoria>;
  created_at?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  _dummy?: Maybe<Scalars['String']>;
  criarApresentacao?: Maybe<Apresentacao>;
  atualizarApresentacao?: Maybe<Apresentacao>;
  excluirApresentacao?: Maybe<Apresentacao>;
  enviarEmailParaAvaliadores?: Maybe<Scalars['Boolean']>;
  fecharApresentacoesPorArea?: Maybe<Scalars['Boolean']>;
  fecharApresentacoesPorEvento?: Maybe<Scalars['Boolean']>;
  liberarApresentacoesPorCategoria?: Maybe<Scalars['Boolean']>;
  iniciarApresentacao: Apresentacao;
  fecharApresentacao: Scalars['Boolean'];
  baterAvaliador: Avaliador;
  removerAvaliadorDaApresentacao: Scalars['Boolean'];
  login: LoginOutput;
  atualizarPessoa: Pessoa;
  excluirPessoa: Scalars['Boolean'];
  avaliarProjeto: Scalars['Boolean'];
  avaliarProjetoPorUmAvaliador: Scalars['Boolean'];
  liberarProjeto: Projeto;
  liberarProjetoParaUmAvaliador: Projeto;
  fecharProjeto: Projeto;
  fecharProjetoParaUmAvaliador: Projeto;
  removerProjetoDeApresentacao: Scalars['Boolean'];
  criarPessoa: Pessoa;
  alocarAvaliadoresVideo: Scalars['Boolean'];
  liberarAvaliadorProjetoPorCategoria: Scalars['Boolean'];
  criarProjeto?: Maybe<Projeto>;
  atualizarProjeto?: Maybe<Projeto>;
  atualizarListaDeProjetos?: Maybe<Scalars['Boolean']>;
  criarUnidade?: Maybe<Unidade>;
  atualizarUnidade?: Maybe<Unidade>;
  excluirUnidade?: Maybe<Scalars['Boolean']>;
  criarPergunta?: Maybe<Pergunta>;
  atualizarPergunta?: Maybe<Pergunta>;
  excluirPergunta?: Maybe<Scalars['Int']>;
  criarCategoria?: Maybe<Categoria>;
  atualizarCategoria?: Maybe<Categoria>;
  excluirCategoria?: Maybe<Scalars['Int']>;
  criarAreaTematica?: Maybe<AreaTematica>;
  atualizarAreaTematica?: Maybe<AreaTematica>;
  excluirAreaTematica?: Maybe<Scalars['Boolean']>;
  criarCampus?: Maybe<Campus>;
  atualizarCampus?: Maybe<Campus>;
  excluirCampus?: Maybe<Scalars['Boolean']>;
  criarEvento?: Maybe<Evento>;
  atualizarEvento?: Maybe<Evento>;
  excluirEvento?: Maybe<Evento>;
  alocarProjetos?: Maybe<Array<Apresentacao>>;
  alocarProjetosVideo?: Maybe<Array<Apresentacao>>;
  criarReport?: Maybe<Report>;
  excluirReport?: Maybe<Scalars['Boolean']>;
  atualizarStatusReport?: Maybe<Report>;
  autenticar: Token;
  criarSolicitacaoLGPD?: Maybe<Solicitacao>;
  finalizarSolicitacoesLGPD?: Maybe<Solicitacao>;
  atualizarSolicitacaoLGPD?: Maybe<Solicitacao>;
};


export type MutationCriarApresentacaoArgs = {
  input: CriarApresentacaoInput;
};


export type MutationAtualizarApresentacaoArgs = {
  input: AtualizarApresentacaoInput;
};


export type MutationExcluirApresentacaoArgs = {
  input?: Maybe<ExcluirApresentacaoInput>;
};


export type MutationEnviarEmailParaAvaliadoresArgs = {
  input?: Maybe<EnviarEmailParaAvaliadoresInput>;
};


export type MutationFecharApresentacoesPorAreaArgs = {
  idAreaTematica: Scalars['Int'];
};


export type MutationFecharApresentacoesPorEventoArgs = {
  idEvento: Scalars['Int'];
};


export type MutationLiberarApresentacoesPorCategoriaArgs = {
  idCategoria: Scalars['Int'];
};


export type MutationIniciarApresentacaoArgs = {
  input: IniciarApresentacaoInput;
};


export type MutationFecharApresentacaoArgs = {
  input: FecharApresentacaoInput;
};


export type MutationBaterAvaliadorArgs = {
  input: BaterAvaliadorInput;
};


export type MutationRemoverAvaliadorDaApresentacaoArgs = {
  input: RemoverAvaliadorDaApresentacaoInput;
};


export type MutationLoginArgs = {
  input: LoginInput;
};


export type MutationAtualizarPessoaArgs = {
  input: AtualizarPessoaInput;
};


export type MutationExcluirPessoaArgs = {
  input: ExcluirPessoaInput;
};


export type MutationAvaliarProjetoArgs = {
  input: AvaliarProjetoInput;
};


export type MutationAvaliarProjetoPorUmAvaliadorArgs = {
  input: AvaliarProjetoPorUmAvaliadorInput;
};


export type MutationLiberarProjetoArgs = {
  input: LiberarProjetoInput;
};


export type MutationLiberarProjetoParaUmAvaliadorArgs = {
  input: LiberarProjetoParaUmAvaliadorInput;
};


export type MutationFecharProjetoArgs = {
  input: FecharProjetoInput;
};


export type MutationFecharProjetoParaUmAvaliadorArgs = {
  input: FecharProjetoParaUmAvaliadorInput;
};


export type MutationRemoverProjetoDeApresentacaoArgs = {
  input: RemoverProjetoDeApresentacaoInput;
};


export type MutationCriarPessoaArgs = {
  input: CriarPessoaInput;
};


export type MutationLiberarAvaliadorProjetoPorCategoriaArgs = {
  idCategoria: Scalars['Int'];
};


export type MutationCriarProjetoArgs = {
  input: CriarProjetoInput;
};


export type MutationAtualizarProjetoArgs = {
  input: AtualizarProjetoInput;
};


export type MutationCriarUnidadeArgs = {
  input: CriarUnidadeInput;
};


export type MutationAtualizarUnidadeArgs = {
  input: AtualizarUnidadeInput;
};


export type MutationExcluirUnidadeArgs = {
  input: ExcluirUnidadeInput;
};


export type MutationCriarPerguntaArgs = {
  input: CriarPerguntaInput;
};


export type MutationAtualizarPerguntaArgs = {
  idPergunta: Scalars['Int'];
};


export type MutationCriarCategoriaArgs = {
  idCategoria: Scalars['Int'];
};


export type MutationAtualizarCategoriaArgs = {
  idCategoria: Scalars['Int'];
};


export type MutationCriarAreaTematicaArgs = {
  input: CriarAreaTematica;
};


export type MutationAtualizarAreaTematicaArgs = {
  input: AtualizarAreaTematica;
};


export type MutationExcluirAreaTematicaArgs = {
  idAreaTematica: Scalars['Int'];
};


export type MutationCriarCampusArgs = {
  input: CriarCampusInput;
};


export type MutationAtualizarCampusArgs = {
  input: AtualizarCampusInput;
};


export type MutationExcluirCampusArgs = {
  input: ExcluirCampusInput;
};


export type MutationCriarEventoArgs = {
  input: CriarEventoInput;
};


export type MutationAtualizarEventoArgs = {
  input: AtualizarEventoInput;
};


export type MutationExcluirEventoArgs = {
  idEvento: Scalars['Int'];
};


export type MutationAlocarProjetosArgs = {
  input?: Maybe<AlocarProjetoInput>;
};


export type MutationAlocarProjetosVideoArgs = {
  input?: Maybe<AlocarProjetoVideoInput>;
};


export type MutationCriarReportArgs = {
  input: CriarReportInput;
};


export type MutationExcluirReportArgs = {
  input: ExcluirReportInput;
};


export type MutationAtualizarStatusReportArgs = {
  input: AtualizarStatusReportInput;
};


export type MutationAutenticarArgs = {
  code: Scalars['String'];
};


export type MutationCriarSolicitacaoLgpdArgs = {
  input: CriarSolicitacaoLgpdInput;
};


export type MutationFinalizarSolicitacoesLgpdArgs = {
  input: FinalizarSolicitacoesLgpdInput;
};


export type MutationAtualizarSolicitacaoLgpdArgs = {
  input: AtualizarSolicitacaoLgpdInput;
};

export type NotaPergunta = {
  __typename?: 'NotaPergunta';
  pergunta: Pergunta;
  nota?: Maybe<Scalars['Float']>;
  respostaTextual?: Maybe<Scalars['String']>;
  data: Scalars['String'];
};

export type NotaPerguntaInput = {
  idPergunta: Scalars['Int'];
  nota?: Maybe<Scalars['Float']>;
  respostaTextual?: Maybe<Scalars['String']>;
};

export type Ouvinte = Pessoa & {
  __typename?: 'Ouvinte';
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  matriculaPessoa?: Maybe<Scalars['String']>;
  nomePessoa?: Maybe<Scalars['String']>;
  nomeSocialPessoa?: Maybe<Scalars['String']>;
  lotacaoPessoa?: Maybe<Scalars['String']>;
  emailPessoa?: Maybe<Scalars['String']>;
  telefonePessoa?: Maybe<Scalars['String']>;
  aptidaoPessoa: Scalars['Int'];
  avaliadorPessoa?: Maybe<Scalars['Int']>;
  monitorPessoa?: Maybe<Scalars['Int']>;
  gerentePessoa?: Maybe<Scalars['Int']>;
  administradorPessoa?: Maybe<Scalars['Int']>;
  avatarUrl?: Maybe<Scalars['String']>;
  categoria?: Maybe<Categoria>;
  apresentacoesAlocacao?: Maybe<Array<Apresentacao>>;
  created_at?: Maybe<Scalars['String']>;
};

export type Pergunta = {
  __typename?: 'Pergunta';
  idPergunta: Scalars['Int'];
  conteudoPergunta: Scalars['String'];
  dataPergunta?: Maybe<Scalars['String']>;
  categoria: Categoria;
};

export type Pessoa = {
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  matriculaPessoa?: Maybe<Scalars['String']>;
  nomePessoa?: Maybe<Scalars['String']>;
  nomeSocialPessoa?: Maybe<Scalars['String']>;
  lotacaoPessoa?: Maybe<Scalars['String']>;
  emailPessoa?: Maybe<Scalars['String']>;
  telefonePessoa?: Maybe<Scalars['String']>;
  aptidaoPessoa: Scalars['Int'];
  avaliadorPessoa?: Maybe<Scalars['Int']>;
  monitorPessoa?: Maybe<Scalars['Int']>;
  gerentePessoa?: Maybe<Scalars['Int']>;
  administradorPessoa?: Maybe<Scalars['Int']>;
  avatarUrl?: Maybe<Scalars['String']>;
  categoria?: Maybe<Categoria>;
  apresentacoesAlocacao?: Maybe<Array<Apresentacao>>;
  created_at?: Maybe<Scalars['String']>;
};

export type Projeto = {
  __typename?: 'Projeto';
  idProjeto: Scalars['Int'];
  tituloProjeto: Scalars['String'];
  descricaoProjeto: Scalars['String'];
  dataInicioProjeto: Scalars['String'];
  dataFimProjeto: Scalars['String'];
  anoProjeto: Scalars['String'];
  modalidadeProjeto: Scalars['String'];
  disponibilidadeProjeto: Scalars['Int'];
  liberacaoProjeto: Scalars['String'];
  areaTematica: AreaTematica;
  unidade?: Maybe<Unidade>;
  codigoProjeto?: Maybe<Scalars['String']>;
  apresentacao?: Maybe<Apresentacao>;
  avaliacoes?: Maybe<Array<Avaliacao>>;
  media?: Maybe<Scalars['Float']>;
  avaliadoresHabilitados?: Maybe<Array<Avaliador>>;
  mediaIndividual?: Maybe<MediaPorAvaliador>;
  mediasIndividuais?: Maybe<Array<MediaPorAvaliador>>;
  coordenador?: Maybe<Array<Membro>>;
  evento?: Maybe<Evento>;
  categoria: Categoria;
  linkArtefato?: Maybe<Scalars['String']>;
  statusProjeto?: Maybe<Scalars['String']>;
};


export type ProjetoMediaIndividualArgs = {
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
};

export type ProjetoAvaliadoInput = {
  idProjeto: Scalars['Int'];
};

export type ProjetoLiberadoInput = {
  idProjeto: Scalars['Int'];
};

export type ProjetosInput = {
  idCampus?: Maybe<Scalars['Int']>;
  idEvento?: Maybe<Scalars['Int']>;
  idAreaTematica?: Maybe<Scalars['Int']>;
};

export type Query = {
  __typename?: 'Query';
  _dummy?: Maybe<Scalars['String']>;
  apresentacao?: Maybe<Apresentacao>;
  apresentacoes?: Maybe<Array<Apresentacao>>;
  pessoa?: Maybe<Pessoa>;
  pessoas?: Maybe<Array<Pessoa>>;
  avaliadores?: Maybe<Array<Avaliador>>;
  monitores?: Maybe<Array<Monitor>>;
  gerentes?: Maybe<Array<Gerente>>;
  apresentacoesAvaliador?: Maybe<Array<Apresentacao>>;
  projeto?: Maybe<Projeto>;
  projetos?: Maybe<Array<Projeto>>;
  unidade?: Maybe<Unidade>;
  unidades?: Maybe<Array<Maybe<Unidade>>>;
  pergunta?: Maybe<Pergunta>;
  perguntas?: Maybe<Array<Pergunta>>;
  categoria?: Maybe<Categoria>;
  categorias?: Maybe<Array<Categoria>>;
  avaliacao?: Maybe<Avaliacao>;
  areaTematica?: Maybe<AreaTematica>;
  areasTematicas?: Maybe<Array<AreaTematica>>;
  campus?: Maybe<Campus>;
  campi?: Maybe<Array<Campus>>;
  evento?: Maybe<Evento>;
  eventos?: Maybe<Array<Evento>>;
  report?: Maybe<Report>;
  reports?: Maybe<Array<Report>>;
  solicitacaoLGPD?: Maybe<Solicitacao>;
  pessoaSolicitacoesLGPD?: Maybe<Array<Maybe<Solicitacao>>>;
  solicitacoesLGPD?: Maybe<Array<Solicitacao2>>;
  relatoriosAvaliadores?: Maybe<Array<RelatorioAvaliador>>;
  relatoriosMedias?: Maybe<Array<RelatorioMedia>>;
  relatoriosNotas?: Maybe<Array<RelatorioNotas>>;
};


export type QueryApresentacaoArgs = {
  input: ApresentacaoInput;
};


export type QueryApresentacoesArgs = {
  input?: Maybe<ApresentacoesInput>;
};


export type QueryPessoaArgs = {
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
};


export type QueryApresentacoesAvaliadorArgs = {
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
};


export type QueryProjetoArgs = {
  idProjeto: Scalars['Int'];
};


export type QueryProjetosArgs = {
  input?: Maybe<ProjetosInput>;
};


export type QueryUnidadeArgs = {
  idUnidade?: Maybe<Scalars['Int']>;
};


export type QueryPerguntaArgs = {
  idPergunta: Scalars['Int'];
};


export type QueryCategoriaArgs = {
  idCategoria: Scalars['Int'];
};


export type QueryAvaliacaoArgs = {
  idProjeto: Scalars['Int'];
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
};


export type QueryAreaTematicaArgs = {
  idAreaTematica: Scalars['Int'];
};


export type QueryCampusArgs = {
  idCampus: Scalars['Int'];
};


export type QueryEventoArgs = {
  idEvento: Scalars['Int'];
};


export type QueryReportArgs = {
  idReport: Scalars['Int'];
};


export type QuerySolicitacaoLgpdArgs = {
  idSolicitacao: Scalars['Int'];
};


export type QueryPessoaSolicitacoesLgpdArgs = {
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
};

export type RelatorioAvaliador = {
  __typename?: 'RelatorioAvaliador';
  avaliador: Pessoa;
  avaliou: Scalars['String'];
};

export type RelatorioMedia = {
  __typename?: 'RelatorioMedia';
  projeto: Projeto;
  media: Scalars['Float'];
  numAvaliadores: Scalars['Int'];
};

export type RelatorioNotas = {
  __typename?: 'RelatorioNotas';
  avaliador: Pessoa;
  projeto: Projeto;
  nota: Scalars['Float'];
};

export type RemoverAvaliadorDaApresentacaoInput = {
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  idApresentacao: Scalars['Int'];
  id2Apresentacao: Scalars['String'];
};

export type RemoverProjetoDeApresentacaoInput = {
  idProjeto: Scalars['Int'];
};

export type Report = {
  __typename?: 'Report';
  idReport: Scalars['Int'];
  conteudoReport: Scalars['String'];
  avaliador: Pessoa;
  apresentacao?: Maybe<Apresentacao>;
  dataCriacao?: Maybe<Scalars['String']>;
  statusReport: Scalars['String'];
  emailReport: Scalars['String'];
};

export type Solicitacao = {
  __typename?: 'Solicitacao';
  idSolicitacao: Scalars['Int'];
  tipoSolicitacao: Scalars['Int'];
  textoSolicitacao?: Maybe<Scalars['String']>;
  pessoa: Pessoa;
  nomeSolicitacao: Scalars['String'];
  sobrenomeSolicitacao: Scalars['String'];
  emailPessoa: Scalars['String'];
  statusSolicitacao?: Maybe<Scalars['String']>;
};

export type Solicitacao2 = {
  __typename?: 'Solicitacao2';
  idSolicitacao: Scalars['Int'];
  tipoSolicitacao: Scalars['Int'];
  textoSolicitacao?: Maybe<Scalars['String']>;
  pessoa?: Maybe<Pessoa>;
  nomeSolicitacao: Scalars['String'];
  sobrenomeSolicitacao: Scalars['String'];
  emailPessoa: Scalars['String'];
  statusSolicitacao?: Maybe<Scalars['String']>;
};

export type Subscription = {
  __typename?: 'Subscription';
  projetoLiberado: Projeto;
  projetoFechado: Projeto;
  projetoLiberadoParaUmAvaliador: Projeto;
  projetoFechadoParaUmAvaliador: Projeto;
  subBaterAvaliador: Avaliador;
  apresentacaoFechada: Apresentacao;
  projetoAvaliado: Avaliacao;
  avaliadorRemovidoDaApresentacao: Apresentacao;
};


export type SubscriptionProjetoLiberadoArgs = {
  input?: Maybe<LiberarProjetoInput>;
};


export type SubscriptionProjetoFechadoArgs = {
  input?: Maybe<FecharProjetoInput>;
};


export type SubscriptionProjetoLiberadoParaUmAvaliadorArgs = {
  input?: Maybe<LiberarProjetoParaUmAvaliadorInput>;
};


export type SubscriptionProjetoFechadoParaUmAvaliadorArgs = {
  input?: Maybe<FecharProjetoParaUmAvaliadorInput>;
};


export type SubscriptionSubBaterAvaliadorArgs = {
  input?: Maybe<BaterAvaliadorInput>;
};


export type SubscriptionApresentacaoFechadaArgs = {
  input?: Maybe<FecharApresentacaoInput>;
};


export type SubscriptionProjetoAvaliadoArgs = {
  input?: Maybe<ProjetoLiberadoInput>;
};


export type SubscriptionAvaliadorRemovidoDaApresentacaoArgs = {
  input?: Maybe<RemoverAvaliadorDaApresentacaoInput>;
};

export type Token = {
  __typename?: 'Token';
  access_token: Scalars['String'];
  token_type: Scalars['String'];
  refresh_token: Scalars['String'];
  expires_in: Scalars['Int'];
  scope: Scalars['String'];
  foto: Scalars['String'];
  id_usuario: Scalars['Int'];
  nome: Scalars['String'];
  jti: Scalars['String'];
};

export type Unidade = {
  __typename?: 'Unidade';
  idUnidade: Scalars['Int'];
  codigoUnidade: Scalars['String'];
  nomeUnidade: Scalars['String'];
  siglaUnidade: Scalars['String'];
  hierarquiaUnidade: Scalars['String'];
  unidadeGestora: Scalars['Int'];
  tipoUnidade: Scalars['String'];
  idCampus: Scalars['Int'];
};


export type GetStiTokenMutationVariables = Exact<{
  code: Scalars['String'];
}>;


export type GetStiTokenMutation = (
  { __typename?: 'Mutation' }
  & { autenticar: (
    { __typename?: 'Token' }
    & Pick<Token, 'access_token' | 'token_type' | 'refresh_token' | 'expires_in' | 'scope' | 'foto' | 'id_usuario' | 'nome' | 'jti'>
  ) }
);

export type ApresentacoesAvaliadorQueryVariables = Exact<{
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
}>;


export type ApresentacoesAvaliadorQuery = (
  { __typename?: 'Query' }
  & { apresentacoesAvaliador?: Maybe<Array<(
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao' | 'codigoApresentacao' | 'salaApresentacao' | 'horaApresentacao'>
    & { areaTematica?: Maybe<(
      { __typename?: 'AreaTematica' }
      & Pick<AreaTematica, 'nomeAreaTematica'>
    )>, categoria: (
      { __typename?: 'Categoria' }
      & Pick<Categoria, 'idCategoria' | 'nomeCategoria'>
    ), projetos?: Maybe<Array<(
      { __typename?: 'Projeto' }
      & Pick<Projeto, 'idProjeto' | 'tituloProjeto' | 'codigoProjeto'>
      & { mediaIndividual?: Maybe<(
        { __typename?: 'mediaPorAvaliador' }
        & Pick<MediaPorAvaliador, 'mediaProjeto'>
      )> }
    )>> }
  )>> }
);

export type CampusFragment = (
  { __typename?: 'Campus' }
  & Pick<Campus, 'idCampus' | 'nomeCampus'>
);

export type GetCampiQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCampiQuery = (
  { __typename?: 'Query' }
  & { campi?: Maybe<Array<(
    { __typename?: 'Campus' }
    & CampusFragment
  )>> }
);

export type GetCampusQueryVariables = Exact<{
  idCampus: Scalars['Int'];
}>;


export type GetCampusQuery = (
  { __typename?: 'Query' }
  & { campus?: Maybe<(
    { __typename?: 'Campus' }
    & CampusFragment
  )> }
);

export type CategoryFragment = (
  { __typename?: 'Categoria' }
  & Pick<Categoria, 'idCategoria' | 'nomeCategoria'>
);

export type CategoryRequiredFieldsFragment = (
  { __typename?: 'Categoria' }
  & Pick<Categoria, 'idCategoria' | 'nomeCategoria'>
);

export type GetCategoriesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCategoriesQuery = (
  { __typename?: 'Query' }
  & { categorias?: Maybe<Array<(
    { __typename?: 'Categoria' }
    & CategoryFragment
  )>> }
);

export type GetCategoriasComPerguntasQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCategoriasComPerguntasQuery = (
  { __typename?: 'Query' }
  & { categorias?: Maybe<Array<(
    { __typename?: 'Categoria' }
    & Pick<Categoria, 'idCategoria' | 'nomeCategoria'>
    & { perguntas?: Maybe<Array<(
      { __typename?: 'Pergunta' }
      & Pick<Pergunta, 'idPergunta' | 'conteudoPergunta'>
    )>> }
  )>> }
);

export type CreateEventMutationVariables = Exact<{
  input: CriarEventoInput;
}>;


export type CreateEventMutation = (
  { __typename?: 'Mutation' }
  & { criarEvento?: Maybe<(
    { __typename?: 'Evento' }
    & EventFragment
  )> }
);

export type EventRequiredFieldsFragment = (
  { __typename?: 'Evento' }
  & Pick<Evento, 'idEvento' | 'nomeEvento' | 'statusEvento'>
  & { campi: Array<(
    { __typename?: 'Campus' }
    & Pick<Campus, 'idCampus' | 'nomeCampus'>
  )>, areasTematicas: Array<(
    { __typename?: 'AreaTematica' }
    & AreaTematicaFragment
  )> }
);

export type EventFragment = (
  { __typename?: 'Evento' }
  & Pick<Evento, 'descricaoEvento' | 'dataInicioEvento' | 'dataFimEvento'>
  & EventRequiredFieldsFragment
);

export type GetEventQueryVariables = Exact<{
  idEvento: Scalars['Int'];
}>;


export type GetEventQuery = (
  { __typename?: 'Query' }
  & { evento?: Maybe<(
    { __typename?: 'Evento' }
    & EventFragment
  )> }
);

export type GetEventThematicsQueryVariables = Exact<{
  idEvento: Scalars['Int'];
}>;


export type GetEventThematicsQuery = (
  { __typename?: 'Query' }
  & { evento?: Maybe<(
    { __typename?: 'Evento' }
    & { areasTematicas: Array<(
      { __typename?: 'AreaTematica' }
      & Pick<AreaTematica, 'idAreaTematica' | 'nomeAreaTematica'>
    )> }
  )> }
);

export type GetEventsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetEventsQuery = (
  { __typename?: 'Query' }
  & { eventos?: Maybe<Array<(
    { __typename?: 'Evento' }
    & EventFragment
  )>> }
);

export type UpdateEventMutationVariables = Exact<{
  input: AtualizarEventoInput;
}>;


export type UpdateEventMutation = (
  { __typename?: 'Mutation' }
  & { atualizarEvento?: Maybe<(
    { __typename?: 'Evento' }
    & EventFragment
  )> }
);

export type ExportPageContainerQueryVariables = Exact<{
  idEvento: Scalars['Int'];
}>;


export type ExportPageContainerQuery = (
  { __typename?: 'Query' }
  & { evento?: Maybe<(
    { __typename?: 'Evento' }
    & EventFragment
  )>, projetos?: Maybe<Array<(
    { __typename: 'Projeto' }
    & Pick<Projeto, 'media'>
    & ProjectFragment
  )>> }
);

export type AtualizarSolicitacaoLgpdMutationVariables = Exact<{
  idSolicitacao: Scalars['Int'];
  statusSolicitacao?: Maybe<Scalars['String']>;
}>;


export type AtualizarSolicitacaoLgpdMutation = (
  { __typename?: 'Mutation' }
  & { atualizarSolicitacaoLGPD?: Maybe<(
    { __typename?: 'Solicitacao' }
    & Pick<Solicitacao, 'idSolicitacao' | 'statusSolicitacao'>
  )> }
);

export type CriarSolicitacaoLgpdMutationVariables = Exact<{
  tipoSolicitacao: Scalars['Int'];
  textoSolicitacao?: Maybe<Scalars['String']>;
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  nomeSolicitacao: Scalars['String'];
  sobrenomeSolicitacao: Scalars['String'];
  emailPessoa: Scalars['String'];
}>;


export type CriarSolicitacaoLgpdMutation = (
  { __typename?: 'Mutation' }
  & { criarSolicitacaoLGPD?: Maybe<(
    { __typename?: 'Solicitacao' }
    & Pick<Solicitacao, 'idSolicitacao'>
  )> }
);

export type GetSolicitacoesLgpdQueryVariables = Exact<{ [key: string]: never; }>;


export type GetSolicitacoesLgpdQuery = (
  { __typename?: 'Query' }
  & { solicitacoesLGPD?: Maybe<Array<(
    { __typename?: 'Solicitacao2' }
    & Pick<Solicitacao2, 'idSolicitacao' | 'tipoSolicitacao' | 'textoSolicitacao' | 'nomeSolicitacao' | 'sobrenomeSolicitacao' | 'emailPessoa' | 'statusSolicitacao'>
  )>> }
);

export type PresentationBasicFragment = (
  { __typename?: 'Apresentacao' }
  & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao' | 'salaApresentacao' | 'horaApresentacao' | 'dataInicioApresentacao' | 'dataFimApresentacao' | 'disponibilidadeApresentacao' | 'modalidadeApresentacao' | 'codigoApresentacao'>
);

export type PresentationRequiredFieldsFragment = (
  { __typename?: 'Apresentacao' }
  & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao' | 'codigoApresentacao'>
  & { categoria: (
    { __typename?: 'Categoria' }
    & CategoryRequiredFieldsFragment
  ) }
);

export type PresentationFragment = (
  { __typename?: 'Apresentacao' }
  & Pick<Apresentacao, 'salaApresentacao' | 'horaApresentacao' | 'dataInicioApresentacao' | 'dataFimApresentacao' | 'disponibilidadeApresentacao' | 'modalidadeApresentacao'>
  & { evento?: Maybe<(
    { __typename?: 'Evento' }
    & EventRequiredFieldsFragment
  )>, areaTematica?: Maybe<(
    { __typename?: 'AreaTematica' }
    & AreaTematicaFragment
  )> }
  & PresentationRequiredFieldsFragment
);

export type ProjectPresentationFragment = (
  { __typename?: 'Apresentacao' }
  & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao' | 'salaApresentacao' | 'codigoApresentacao'>
);

export type PresentationIdsFragment = (
  { __typename?: 'Apresentacao' }
  & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao' | 'codigoApresentacao'>
);

export type GetPresentationsWithProjectsQueryVariables = Exact<{
  input?: Maybe<ApresentacoesInput>;
}>;


export type GetPresentationsWithProjectsQuery = (
  { __typename?: 'Query' }
  & { apresentacoes?: Maybe<Array<(
    { __typename?: 'Apresentacao' }
    & { projetos?: Maybe<Array<(
      { __typename?: 'Projeto' }
      & Pick<Projeto, 'media'>
      & ProjectFragment
    )>> }
    & PresentationFragment
  )>> }
);

export type PresentationsPageContainerQueryVariables = Exact<{ [key: string]: never; }>;


export type PresentationsPageContainerQuery = (
  { __typename?: 'Query' }
  & { apresentacoes?: Maybe<Array<(
    { __typename?: 'Apresentacao' }
    & PresentationFragment
  )>> }
);

export type GetPresentationsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetPresentationsQuery = (
  { __typename?: 'Query' }
  & { apresentacoes?: Maybe<Array<(
    { __typename?: 'Apresentacao' }
    & PresentationFragment
  )>> }
);

export type GetEventPresentationsQueryVariables = Exact<{
  input: ApresentacoesInput;
}>;


export type GetEventPresentationsQuery = (
  { __typename?: 'Query' }
  & { apresentacoes?: Maybe<Array<(
    { __typename?: 'Apresentacao' }
    & PresentationFragment
  )>> }
);

export type GetEventPresentationsWithProjectsQueryVariables = Exact<{
  input?: Maybe<ApresentacoesInput>;
}>;


export type GetEventPresentationsWithProjectsQuery = (
  { __typename?: 'Query' }
  & { apresentacoes?: Maybe<Array<(
    { __typename?: 'Apresentacao' }
    & { projetos?: Maybe<Array<(
      { __typename?: 'Projeto' }
      & Pick<Projeto, 'media'>
      & ProjectFragment
    )>> }
    & PresentationFragment
  )>> }
);

export type GetPresentationReviewersQueryVariables = Exact<{
  input: ApresentacaoInput;
}>;


export type GetPresentationReviewersQuery = (
  { __typename?: 'Query' }
  & { apresentacao?: Maybe<(
    { __typename?: 'Apresentacao' }
    & { avaliadores?: Maybe<Array<(
      { __typename?: 'Avaliador' }
      & User_Avaliador_Fragment
    )>> }
    & PresentationBasicFragment
  )> }
);

export type GetPresentationsOptionsQueryVariables = Exact<{
  input?: Maybe<ApresentacoesInput>;
}>;


export type GetPresentationsOptionsQuery = (
  { __typename?: 'Query' }
  & { apresentacoes?: Maybe<Array<(
    { __typename?: 'Apresentacao' }
    & { evento?: Maybe<(
      { __typename?: 'Evento' }
      & Pick<Evento, 'idEvento' | 'nomeEvento'>
      & { campi: Array<(
        { __typename?: 'Campus' }
        & Pick<Campus, 'idCampus'>
      )>, areasTematicas: Array<(
        { __typename?: 'AreaTematica' }
        & Pick<AreaTematica, 'idAreaTematica' | 'nomeAreaTematica'>
      )> }
    )>, projetos?: Maybe<Array<(
      { __typename?: 'Projeto' }
      & Pick<Projeto, 'idProjeto' | 'tituloProjeto' | 'statusProjeto' | 'descricaoProjeto' | 'dataInicioProjeto' | 'dataFimProjeto'>
    )>> }
    & PresentationFragment
  )>> }
);

export type GetAllPresentationsOptionQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAllPresentationsOptionQuery = (
  { __typename?: 'Query' }
  & { apresentacoes?: Maybe<Array<(
    { __typename?: 'Apresentacao' }
    & { evento?: Maybe<(
      { __typename?: 'Evento' }
      & Pick<Evento, 'idEvento' | 'nomeEvento'>
      & { campi: Array<(
        { __typename?: 'Campus' }
        & Pick<Campus, 'idCampus'>
      )>, areasTematicas: Array<(
        { __typename?: 'AreaTematica' }
        & Pick<AreaTematica, 'idAreaTematica' | 'nomeAreaTematica'>
      )> }
    )>, projetos?: Maybe<Array<(
      { __typename?: 'Projeto' }
      & Pick<Projeto, 'idProjeto' | 'tituloProjeto' | 'descricaoProjeto' | 'dataInicioProjeto' | 'dataFimProjeto' | 'statusProjeto'>
    )>> }
    & PresentationFragment
  )>> }
);

export type GeneretaeQrPresentationsQueryVariables = Exact<{
  idEvento: Scalars['Int'];
  modalidadeApresentacao?: Maybe<Scalars['String']>;
  idAreaTematica?: Maybe<Scalars['Int']>;
}>;


export type GeneretaeQrPresentationsQuery = (
  { __typename?: 'Query' }
  & { apresentacoes?: Maybe<Array<(
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao' | 'salaApresentacao' | 'codigoApresentacao' | 'modalidadeApresentacao'>
    & { projetos?: Maybe<Array<(
      { __typename?: 'Projeto' }
      & Pick<Projeto, 'idProjeto' | 'tituloProjeto'>
    )>>, areaTematica?: Maybe<(
      { __typename?: 'AreaTematica' }
      & Pick<AreaTematica, 'idAreaTematica' | 'nomeAreaTematica'>
    )>, evento?: Maybe<(
      { __typename?: 'Evento' }
      & Pick<Evento, 'idEvento' | 'nomeEvento'>
    )> }
  )>> }
);

export type CriarApresentacaoMutationVariables = Exact<{
  input: CriarApresentacaoInput;
}>;


export type CriarApresentacaoMutation = (
  { __typename?: 'Mutation' }
  & { criarApresentacao?: Maybe<(
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'modalidadeApresentacao'>
    & PresentationFragment
  )> }
);

export type AdicionarProjetosMutationVariables = Exact<{
  input: AdicionarProjetosInput;
}>;

/*aqui*/
export type AdicionarProjetosMutation = ( 
  { __typename?: 'Mutation' }
  & { adicionarProjetos?: Maybe<(
    { __typename?: 'Projeto' }
    & ProjectFragment
  )> }
);

export type CriarProjetosMutationVariables = Exact<{
  input: CriarProjetoInput;
}>;

/*aqui*/
export type CriarProjetosMutation = ( 
  { __typename?: 'Mutation' }
  & { criarProjetos?: Maybe<(
    { __typename?: 'Projeto' }
    & ProjectFragment
  )> }
);




export type AtualizarApresentacaoMutationVariables = Exact<{
  input: AtualizarApresentacaoInput;
}>;


export type AtualizarApresentacaoMutation = (
  { __typename?: 'Mutation' }
  & { atualizarApresentacao?: Maybe<(
    { __typename?: 'Apresentacao' }
    & PresentationFragment
  )> }
);

export type ExcluirApresentacaoMutationVariables = Exact<{
  input: ExcluirApresentacaoInput;
}>;


export type ExcluirApresentacaoMutation = (
  { __typename?: 'Mutation' }
  & { excluirApresentacao?: Maybe<(
    { __typename?: 'Apresentacao' }
    & PresentationFragment
  )> }
);

export type AlocatProjetosMutationVariables = Exact<{
  input: AlocarProjetoInput;
}>;


export type AlocatProjetosMutation = (
  { __typename?: 'Mutation' }
  & { alocarProjetos?: Maybe<Array<(
    { __typename?: 'Apresentacao' }
    & PresentationFragment
  )>> }
);

export type ProjectRequiredFieldsFragment = (
  { __typename?: 'Projeto' }
  & Pick<Projeto, 'idProjeto' | 'tituloProjeto' | 'descricaoProjeto' | 'dataInicioProjeto' | 'dataFimProjeto' | 'anoProjeto' | 'modalidadeProjeto' | 'disponibilidadeProjeto' | 'liberacaoProjeto' | 'statusProjeto'>
  & { areaTematica: (
    { __typename?: 'AreaTematica' }
    & AreaTematicaFragment
  ), categoria: (
    { __typename?: 'Categoria' }
    & CategoryRequiredFieldsFragment
  ) }
);

export type ProjectFragment = (
  { __typename?: 'Projeto' }
  & Pick<Projeto, 'codigoProjeto'>
  & { apresentacao?: Maybe<(
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'salaApresentacao' | 'horaApresentacao' | 'dataInicioApresentacao' | 'dataFimApresentacao' | 'disponibilidadeApresentacao' | 'modalidadeApresentacao'>
    & PresentationRequiredFieldsFragment
  )> }
  & ProjectRequiredFieldsFragment
);

export type GetPresentationProjectsQueryVariables = Exact<{
  input: ApresentacaoInput;
}>;


export type GetPresentationProjectsQuery = (
  { __typename?: 'Query' }
  & { apresentacao?: Maybe<(
    { __typename?: 'Apresentacao' }
    & { projetos?: Maybe<Array<(
      { __typename?: 'Projeto' }
      & Pick<Projeto, 'media'>
      & ProjectFragment
    )>> }
    & PresentationFragment
  )> }
);

export type GetProjectQueryVariables = Exact<{
  idProjeto: Scalars['Int'];
}>;


export type GetProjectQuery = (
  { __typename?: 'Query' }
  & { projeto?: Maybe<(
    { __typename?: 'Projeto' }
    & ProjectFragment
  )> }
);

export type GetProjectDetailsQueryVariables = Exact<{
  idProjeto: Scalars['Int'];
}>;


export type GetProjectDetailsQuery = (
  { __typename?: 'Query' }
  & { projeto?: Maybe<(
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'media'>
    & { mediasIndividuais?: Maybe<Array<(
      { __typename?: 'mediaPorAvaliador' }
      & Pick<MediaPorAvaliador, 'mediaProjeto'>
    )>> }
    & ProjectFragment
  )> }
);

export type GetProjectReviewsQueryVariables = Exact<{
  idProjeto: Scalars['Int'];
}>;


export type GetProjectReviewsQuery = (
  { __typename?: 'Query' }
  & { projeto?: Maybe<(
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'idProjeto' | 'tituloProjeto' | 'descricaoProjeto' | 'dataInicioProjeto' | 'dataFimProjeto' | 'media' | 'statusProjeto'>
    & { mediasIndividuais?: Maybe<Array<(
      { __typename?: 'mediaPorAvaliador' }
      & Pick<MediaPorAvaliador, 'mediaProjeto'>
    )>> }
  )> }
);

export type GetProjectsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetProjectsQuery = (
  { __typename?: 'Query' }
  & { projetos?: Maybe<Array<(
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'media'>
    & ProjectFragment
  )>> }
);

export type ProjectsPageContainerQueryVariables = Exact<{ [key: string]: never; }>;


export type ProjectsPageContainerQuery = (
  { __typename?: 'Query' }
  & { projetos?: Maybe<Array<(
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'media'>
    & ProjectFragment
  )>>, areasTematicas?: Maybe<Array<(
    { __typename?: 'AreaTematica' }
    & AreaTematicaFragment
  )>> }
);

export type ProjetoPessoaFragment = (
  { __typename?: 'Projeto' }
  & Pick<Projeto, 'idProjeto' | 'codigoProjeto' | 'tituloProjeto' | 'linkArtefato'>
  & { categoria: (
    { __typename?: 'Categoria' }
    & Pick<Categoria, 'idCategoria' | 'nomeCategoria'>
  ), mediaIndividual?: Maybe<(
    { __typename?: 'mediaPorAvaliador' }
    & Pick<MediaPorAvaliador, 'mediaProjeto'>
  )>, apresentacao?: Maybe<(
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'idApresentacao' | 'id2Apresentacao' | 'codigoApresentacao' | 'salaApresentacao' | 'horaApresentacao' | 'linkApresentacao'>
    & { evento?: Maybe<(
      { __typename?: 'Evento' }
      & Pick<Evento, 'idEvento' | 'nomeEvento'>
    )>, categoria: (
      { __typename?: 'Categoria' }
      & Pick<Categoria, 'idCategoria' | 'nomeCategoria'>
    ), areaTematica?: Maybe<(
      { __typename?: 'AreaTematica' }
      & Pick<AreaTematica, 'idAreaTematica' | 'nomeAreaTematica'>
    )> }
  )> }
);

export type GetReviewerProjectsQueryVariables = Exact<{
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
}>;


export type GetReviewerProjectsQuery = (
  { __typename?: 'Query' }
  & { pessoa?: Maybe<(
    { __typename?: 'Avaliador' }
    & Pick<Avaliador, 'idPessoa' | 'idVinculoPessoa'>
    & { projetosParaAvaliar?: Maybe<Array<(
      { __typename?: 'Projeto' }
      & ProjetoPessoaFragment
    )>> }
  ) | (
    { __typename?: 'Monitor' }
    & Pick<Monitor, 'idPessoa' | 'idVinculoPessoa'>
  ) | (
    { __typename?: 'Gerente' }
    & Pick<Gerente, 'idPessoa' | 'idVinculoPessoa'>
    & { projetosParaAvaliar?: Maybe<Array<(
      { __typename?: 'Projeto' }
      & ProjetoPessoaFragment
    )>> }
  ) | (
    { __typename?: 'Ouvinte' }
    & Pick<Ouvinte, 'idPessoa' | 'idVinculoPessoa'>
  ) | (
    { __typename?: 'Administrador' }
    & Pick<Administrador, 'idPessoa' | 'idVinculoPessoa'>
  )> }
);

export type MakeProjectReviewMutationVariables = Exact<{
  input: AvaliarProjetoInput;
}>;


export type MakeProjectReviewMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'avaliarProjeto'>
);

export type UpdateProjectMutationVariables = Exact<{
  input: AtualizarProjetoInput;
}>;


export type UpdateProjectMutation = (
  { __typename?: 'Mutation' }
  & { atualizarProjeto?: Maybe<(
    { __typename?: 'Projeto' }
    & ProjectFragment
  )> }
);

export type QuestionFragment = (
  { __typename?: 'Pergunta' }
  & Pick<Pergunta, 'idPergunta' | 'conteudoPergunta' | 'dataPergunta'>
);

export type GetQuestionsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetQuestionsQuery = (
  { __typename?: 'Query' }
  & { perguntas?: Maybe<Array<(
    { __typename?: 'Pergunta' }
    & { categoria: (
      { __typename?: 'Categoria' }
      & CategoryFragment
    ) }
    & QuestionFragment
  )>> }
);

export type ReportPageContainerQueryVariables = Exact<{
  idEvento: Scalars['Int'];
}>;


export type ReportPageContainerQuery = (
  { __typename?: 'Query' }
  & { evento?: Maybe<(
    { __typename?: 'Evento' }
    & { areasTematicas: Array<(
      { __typename?: 'AreaTematica' }
      & AreaTematicaFragment
    )>, apresentacoes?: Maybe<Array<(
      { __typename?: 'Apresentacao' }
      & Pick<Apresentacao, 'modalidadeApresentacao' | 'disponibilidadeApresentacao'>
      & { areaTematica?: Maybe<(
        { __typename?: 'AreaTematica' }
        & AreaTematicaFragment
      )>, projetos?: Maybe<Array<(
        { __typename?: 'Projeto' }
        & Pick<Projeto, 'media'>
        & ProjectFragment
      )>> }
      & PresentationRequiredFieldsFragment
    )>> }
    & EventFragment
  )>, pessoas?: Maybe<Array<(
    { __typename?: 'Avaliador' }
    & User_Avaliador_Fragment
  ) | (
    { __typename?: 'Monitor' }
    & User_Monitor_Fragment
  ) | (
    { __typename?: 'Gerente' }
    & User_Gerente_Fragment
  ) | (
    { __typename?: 'Ouvinte' }
    & User_Ouvinte_Fragment
  ) | (
    { __typename?: 'Administrador' }
    & User_Administrador_Fragment
  )>> }
);

export type AtualizarStatusReportMutationVariables = Exact<{
  idReport: Scalars['Int'];
  statusReport: Scalars['String'];
}>;


export type AtualizarStatusReportMutation = (
  { __typename?: 'Mutation' }
  & { atualizarStatusReport?: Maybe<(
    { __typename?: 'Report' }
    & Pick<Report, 'idReport' | 'conteudoReport'>
  )> }
);

export type CriarReportMutationVariables = Exact<{
  conteudoReport: Scalars['String'];
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
  emailReport: Scalars['String'];
}>;


export type CriarReportMutation = (
  { __typename?: 'Mutation' }
  & { criarReport?: Maybe<(
    { __typename?: 'Report' }
    & Pick<Report, 'idReport'>
  )> }
);

export type GetReportsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetReportsQuery = (
  { __typename?: 'Query' }
  & { reports?: Maybe<Array<(
    { __typename?: 'Report' }
    & Pick<Report, 'idReport' | 'conteudoReport' | 'emailReport' | 'dataCriacao' | 'statusReport'>
    & { avaliador: (
      { __typename?: 'Avaliador' }
      & Pick<Avaliador, 'nomePessoa' | 'idPessoa' | 'idVinculoPessoa'>
    ) | (
      { __typename?: 'Monitor' }
      & Pick<Monitor, 'nomePessoa' | 'idPessoa' | 'idVinculoPessoa'>
    ) | (
      { __typename?: 'Gerente' }
      & Pick<Gerente, 'nomePessoa' | 'idPessoa' | 'idVinculoPessoa'>
    ) | (
      { __typename?: 'Ouvinte' }
      & Pick<Ouvinte, 'nomePessoa' | 'idPessoa' | 'idVinculoPessoa'>
    ) | (
      { __typename?: 'Administrador' }
      & Pick<Administrador, 'nomePessoa' | 'idPessoa' | 'idVinculoPessoa'>
    ), apresentacao?: Maybe<(
      { __typename?: 'Apresentacao' }
      & Pick<Apresentacao, 'salaApresentacao' | 'idApresentacao' | 'id2Apresentacao'>
    )> }
  )>> }
);

export type AvaliacaoPessoaFragment = (
  { __typename?: 'Avaliacao' }
  & Pick<Avaliacao, 'media'>
  & { projeto: (
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'idProjeto' | 'tituloProjeto'>
  ) }
);

export type AvaliacoesAvaliadorQueryVariables = Exact<{
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
}>;


export type AvaliacoesAvaliadorQuery = (
  { __typename?: 'Query' }
  & { pessoa?: Maybe<(
    { __typename?: 'Avaliador' }
    & Pick<Avaliador, 'idPessoa' | 'idVinculoPessoa'>
    & { avaliacoes?: Maybe<Array<(
      { __typename?: 'Avaliacao' }
      & AvaliacaoPessoaFragment
    )>> }
  ) | (
    { __typename?: 'Monitor' }
    & Pick<Monitor, 'idPessoa' | 'idVinculoPessoa'>
  ) | (
    { __typename?: 'Gerente' }
    & Pick<Gerente, 'idPessoa' | 'idVinculoPessoa'>
    & { avaliacoes?: Maybe<Array<(
      { __typename?: 'Avaliacao' }
      & AvaliacaoPessoaFragment
    )>> }
  ) | (
    { __typename?: 'Ouvinte' }
    & Pick<Ouvinte, 'idPessoa' | 'idVinculoPessoa'>
  ) | (
    { __typename?: 'Administrador' }
    & Pick<Administrador, 'idPessoa' | 'idVinculoPessoa'>
  )> }
);

export type AvaliarProjetoMutationVariables = Exact<{
  input: AvaliarProjetoInput;
}>;


export type AvaliarProjetoMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'avaliarProjeto'>
);

export type AvaliarProjetoPorUmAvaliadorMutationVariables = Exact<{
  input: AvaliarProjetoPorUmAvaliadorInput;
}>;


export type AvaliarProjetoPorUmAvaliadorMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'avaliarProjetoPorUmAvaliador'>
);

export type GetAddReviewPageDataQueryVariables = Exact<{
  idProjeto: Scalars['Int'];
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
}>;


export type GetAddReviewPageDataQuery = (
  { __typename?: 'Query' }
  & { projeto?: Maybe<(
    { __typename?: 'Projeto' }
    & Pick<Projeto, 'idProjeto' | 'tituloProjeto'>
    & { categoria: (
      { __typename?: 'Categoria' }
      & Pick<Categoria, 'idCategoria' | 'nomeCategoria'>
      & { perguntas?: Maybe<Array<(
        { __typename?: 'Pergunta' }
        & Pick<Pergunta, 'idPergunta' | 'conteudoPergunta'>
        & { categoria: (
          { __typename?: 'Categoria' }
          & Pick<Categoria, 'idCategoria'>
        ) }
      )>> }
    ) }
  )>, pessoa?: Maybe<(
    { __typename?: 'Avaliador' }
    & User_Avaliador_Fragment
  ) | (
    { __typename?: 'Monitor' }
    & User_Monitor_Fragment
  ) | (
    { __typename?: 'Gerente' }
    & User_Gerente_Fragment
  ) | (
    { __typename?: 'Ouvinte' }
    & User_Ouvinte_Fragment
  ) | (
    { __typename?: 'Administrador' }
    & User_Administrador_Fragment
  )> }
);

export type AreaTematicaFragment = (
  { __typename?: 'AreaTematica' }
  & Pick<AreaTematica, 'idAreaTematica' | 'nomeAreaTematica'>
);

export type AreasTematicasQueryVariables = Exact<{ [key: string]: never; }>;


export type AreasTematicasQuery = (
  { __typename?: 'Query' }
  & { areasTematicas?: Maybe<Array<(
    { __typename?: 'AreaTematica' }
    & AreaTematicaFragment
  )>> }
);

export type GetAreaTematicaQueryVariables = Exact<{
  idAreaTematica: Scalars['Int'];
}>;


export type GetAreaTematicaQuery = (
  { __typename?: 'Query' }
  & { areaTematica?: Maybe<(
    { __typename?: 'AreaTematica' }
    & AreaTematicaFragment
  )> }
);

export type CreateUserMutationVariables = Exact<{
  input: CriarPessoaInput;
}>;


export type CreateUserMutation = (
  { __typename?: 'Mutation' }
  & { criarPessoa: (
    { __typename?: 'Avaliador' }
    & User_Avaliador_Fragment
  ) | (
    { __typename?: 'Monitor' }
    & User_Monitor_Fragment
  ) | (
    { __typename?: 'Gerente' }
    & User_Gerente_Fragment
  ) | (
    { __typename?: 'Ouvinte' }
    & User_Ouvinte_Fragment
  ) | (
    { __typename?: 'Administrador' }
    & User_Administrador_Fragment
  ) }
);

type User_Avaliador_Fragment = (
  { __typename?: 'Avaliador' }
  & Pick<Avaliador, 'idPessoa' | 'idVinculoPessoa' | 'matriculaPessoa' | 'nomePessoa' | 'emailPessoa' | 'created_at' | 'aptidaoPessoa' | 'gerentePessoa' | 'avaliadorPessoa' | 'monitorPessoa' | 'administradorPessoa'>
);

type User_Monitor_Fragment = (
  { __typename?: 'Monitor' }
  & Pick<Monitor, 'idPessoa' | 'idVinculoPessoa' | 'matriculaPessoa' | 'nomePessoa' | 'emailPessoa' | 'created_at' | 'aptidaoPessoa' | 'gerentePessoa' | 'avaliadorPessoa' | 'monitorPessoa' | 'administradorPessoa'>
);

type User_Gerente_Fragment = (
  { __typename?: 'Gerente' }
  & Pick<Gerente, 'idPessoa' | 'idVinculoPessoa' | 'matriculaPessoa' | 'nomePessoa' | 'emailPessoa' | 'created_at' | 'aptidaoPessoa' | 'gerentePessoa' | 'avaliadorPessoa' | 'monitorPessoa' | 'administradorPessoa'>
);

type User_Ouvinte_Fragment = (
  { __typename?: 'Ouvinte' }
  & Pick<Ouvinte, 'idPessoa' | 'idVinculoPessoa' | 'matriculaPessoa' | 'nomePessoa' | 'emailPessoa' | 'created_at' | 'aptidaoPessoa' | 'gerentePessoa' | 'avaliadorPessoa' | 'monitorPessoa' | 'administradorPessoa'>
);

type User_Administrador_Fragment = (
  { __typename?: 'Administrador' }
  & Pick<Administrador, 'idPessoa' | 'idVinculoPessoa' | 'matriculaPessoa' | 'nomePessoa' | 'emailPessoa' | 'created_at' | 'aptidaoPessoa' | 'gerentePessoa' | 'avaliadorPessoa' | 'monitorPessoa' | 'administradorPessoa'>
);

export type UserFragment = User_Avaliador_Fragment | User_Monitor_Fragment | User_Gerente_Fragment | User_Ouvinte_Fragment | User_Administrador_Fragment;

export type UserProjectsFragment = (
  { __typename?: 'Projeto' }
  & Pick<Projeto, 'codigoProjeto' | 'tituloProjeto' | 'media' | 'idProjeto'>
  & { mediaIndividual?: Maybe<(
    { __typename?: 'mediaPorAvaliador' }
    & Pick<MediaPorAvaliador, 'mediaProjeto'>
  )> }
);

export type ProjetosAvaliadorQueryVariables = Exact<{
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
}>;


export type ProjetosAvaliadorQuery = (
  { __typename?: 'Query' }
  & { apresentacoesAvaliador?: Maybe<Array<(
    { __typename?: 'Apresentacao' }
    & Pick<Apresentacao, 'idApresentacao'>
    & { projetos?: Maybe<Array<(
      { __typename?: 'Projeto' }
      & UserProjectsFragment
    )>> }
  )>> }
);

export type UsersPageContainerQueryVariables = Exact<{ [key: string]: never; }>;


export type UsersPageContainerQuery = (
  { __typename?: 'Query' }
  & { pessoas?: Maybe<Array<(
    { __typename?: 'Avaliador' }
    & User_Avaliador_Fragment
  ) | (
    { __typename?: 'Monitor' }
    & User_Monitor_Fragment
  ) | (
    { __typename?: 'Gerente' }
    & User_Gerente_Fragment
  ) | (
    { __typename?: 'Ouvinte' }
    & User_Ouvinte_Fragment
  ) | (
    { __typename?: 'Administrador' }
    & User_Administrador_Fragment
  )>> }
);

export type GetReviewersQueryVariables = Exact<{ [key: string]: never; }>;


export type GetReviewersQuery = (
  { __typename?: 'Query' }
  & { avaliadores?: Maybe<Array<(
    { __typename?: 'Avaliador' }
    & User_Avaliador_Fragment
  )>> }
);

export type GetPessoaQueryVariables = Exact<{
  idPessoa: Scalars['String'];
  idVinculoPessoa: Scalars['String'];
}>;


export type GetPessoaQuery = (
  { __typename?: 'Query' }
  & { pessoa?: Maybe<(
    { __typename?: 'Avaliador' }
    & User_Avaliador_Fragment
  ) | (
    { __typename?: 'Monitor' }
    & User_Monitor_Fragment
  ) | (
    { __typename?: 'Gerente' }
    & User_Gerente_Fragment
  ) | (
    { __typename?: 'Ouvinte' }
    & User_Ouvinte_Fragment
  ) | (
    { __typename?: 'Administrador' }
    & User_Administrador_Fragment
  )> }
);

export type AtualizarPessoaMutationVariables = Exact<{
  input: AtualizarPessoaInput;
}>;


export type AtualizarPessoaMutation = (
  { __typename?: 'Mutation' }
  & { atualizarPessoa: (
    { __typename?: 'Avaliador' }
    & User_Avaliador_Fragment
  ) | (
    { __typename?: 'Monitor' }
    & User_Monitor_Fragment
  ) | (
    { __typename?: 'Gerente' }
    & User_Gerente_Fragment
  ) | (
    { __typename?: 'Ouvinte' }
    & User_Ouvinte_Fragment
  ) | (
    { __typename?: 'Administrador' }
    & User_Administrador_Fragment
  ) }
);

export const CampusFragmentDoc = gql`
    fragment Campus on Campus {
  idCampus
  nomeCampus
}
    `;
export const CategoryFragmentDoc = gql`
    fragment Category on Categoria {
  idCategoria
  nomeCategoria
}
    `;
export const AreaTematicaFragmentDoc = gql`
    fragment AreaTematica on AreaTematica {
  idAreaTematica
  nomeAreaTematica
}
    `;
export const EventRequiredFieldsFragmentDoc = gql`
    fragment EventRequiredFields on Evento {
  idEvento
  nomeEvento
  statusEvento
  campi {
    idCampus
    nomeCampus
  }
  areasTematicas {
    ...AreaTematica
  }
}
    ${AreaTematicaFragmentDoc}`;
export const EventFragmentDoc = gql`
    fragment Event on Evento {
  ...EventRequiredFields
  descricaoEvento
  dataInicioEvento
  dataFimEvento
}
    ${EventRequiredFieldsFragmentDoc}`;
export const PresentationBasicFragmentDoc = gql`
    fragment PresentationBasic on Apresentacao {
  idApresentacao
  id2Apresentacao
  salaApresentacao
  horaApresentacao
  dataInicioApresentacao
  dataFimApresentacao
  disponibilidadeApresentacao
  modalidadeApresentacao
  codigoApresentacao
}
    `;
export const CategoryRequiredFieldsFragmentDoc = gql`
    fragment CategoryRequiredFields on Categoria {
  idCategoria
  nomeCategoria
}
    `;
export const PresentationRequiredFieldsFragmentDoc = gql`
    fragment PresentationRequiredFields on Apresentacao {
  idApresentacao
  id2Apresentacao
  codigoApresentacao
  categoria {
    ...CategoryRequiredFields
  }
}
    ${CategoryRequiredFieldsFragmentDoc}`;
export const PresentationFragmentDoc = gql`
    fragment Presentation on Apresentacao {
  ...PresentationRequiredFields
  salaApresentacao
  horaApresentacao
  dataInicioApresentacao
  dataFimApresentacao
  disponibilidadeApresentacao
  modalidadeApresentacao
  evento {
    ...EventRequiredFields
  }
  areaTematica {
    ...AreaTematica
  }
}
    ${PresentationRequiredFieldsFragmentDoc}
${EventRequiredFieldsFragmentDoc}
${AreaTematicaFragmentDoc}`;
export const ProjectPresentationFragmentDoc = gql`
    fragment ProjectPresentation on Apresentacao {
  idApresentacao
  id2Apresentacao
  salaApresentacao
  codigoApresentacao
}
    `;
export const PresentationIdsFragmentDoc = gql`
    fragment PresentationIds on Apresentacao {
  idApresentacao
  id2Apresentacao
  codigoApresentacao
}
    `;
export const ProjectRequiredFieldsFragmentDoc = gql`
    fragment ProjectRequiredFields on Projeto {
  idProjeto
  tituloProjeto
  statusProjeto
  descricaoProjeto
  dataInicioProjeto
  dataFimProjeto
  anoProjeto
  modalidadeProjeto
  disponibilidadeProjeto
  liberacaoProjeto
  areaTematica {
    ...AreaTematica
  }
  categoria {
    ...CategoryRequiredFields
  }
}
    ${AreaTematicaFragmentDoc}
${CategoryRequiredFieldsFragmentDoc}`;
export const ProjectFragmentDoc = gql`
    fragment Project on Projeto {
  ...ProjectRequiredFields
  codigoProjeto
  apresentacao {
    ...PresentationRequiredFields
    salaApresentacao
    horaApresentacao
    dataInicioApresentacao
    dataFimApresentacao
    disponibilidadeApresentacao
    modalidadeApresentacao
  }
}
    ${ProjectRequiredFieldsFragmentDoc}
${PresentationRequiredFieldsFragmentDoc}`;
export const ProjetoPessoaFragmentDoc = gql`
    fragment ProjetoPessoa on Projeto {
  idProjeto
  codigoProjeto
  tituloProjeto
  linkArtefato
  categoria {
    idCategoria
    nomeCategoria
  }
  mediaIndividual(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
    mediaProjeto
  }
  apresentacao {
    idApresentacao
    id2Apresentacao
    codigoApresentacao
    salaApresentacao
    horaApresentacao
    linkApresentacao
    evento {
      idEvento
      nomeEvento
    }
    categoria {
      idCategoria
      nomeCategoria
    }
    areaTematica {
      idAreaTematica
      nomeAreaTematica
    }
  }
}
    `;
export const QuestionFragmentDoc = gql`
    fragment Question on Pergunta {
  idPergunta
  conteudoPergunta
  dataPergunta
}
    `;
export const AvaliacaoPessoaFragmentDoc = gql`
    fragment AvaliacaoPessoa on Avaliacao {
  media
  projeto {
    idProjeto
    tituloProjeto
  }
}
    `;
export const UserFragmentDoc = gql`
    fragment User on Pessoa {
  idPessoa
  idVinculoPessoa
  matriculaPessoa
  nomePessoa
  emailPessoa
  created_at
  aptidaoPessoa
  gerentePessoa
  avaliadorPessoa
  monitorPessoa
  gerentePessoa
  administradorPessoa
}
    `;
export const UserProjectsFragmentDoc = gql`
    fragment UserProjects on Projeto {
  codigoProjeto
  tituloProjeto
  media
  idProjeto
  mediaIndividual(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
    mediaProjeto
  }
}
    `;
export const GetStiTokenDocument = gql`
    mutation getSTIToken($code: String!) {
  autenticar(code: $code) {
    access_token
    token_type
    refresh_token
    expires_in
    scope
    foto
    id_usuario
    nome
    jti
  }
}
    `;
export type GetStiTokenMutationFn = ApolloReactCommon.MutationFunction<GetStiTokenMutation, GetStiTokenMutationVariables>;
export type GetStiTokenComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<GetStiTokenMutation, GetStiTokenMutationVariables>, 'mutation'>;

    export const GetStiTokenComponent = (props: GetStiTokenComponentProps) => (
      <ApolloReactComponents.Mutation<GetStiTokenMutation, GetStiTokenMutationVariables> mutation={GetStiTokenDocument} {...props} />
    );
    

/**
 * __useGetStiTokenMutation__
 *
 * To run a mutation, you first call `useGetStiTokenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useGetStiTokenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [getStiTokenMutation, { data, loading, error }] = useGetStiTokenMutation({
 *   variables: {
 *      code: // value for 'code'
 *   },
 * });
 */
export function useGetStiTokenMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<GetStiTokenMutation, GetStiTokenMutationVariables>) {
        return ApolloReactHooks.useMutation<GetStiTokenMutation, GetStiTokenMutationVariables>(GetStiTokenDocument, baseOptions);
      }
export type GetStiTokenMutationHookResult = ReturnType<typeof useGetStiTokenMutation>;
export type GetStiTokenMutationResult = ApolloReactCommon.MutationResult<GetStiTokenMutation>;
export type GetStiTokenMutationOptions = ApolloReactCommon.BaseMutationOptions<GetStiTokenMutation, GetStiTokenMutationVariables>;
export const ApresentacoesAvaliadorDocument = gql`
    query apresentacoesAvaliador($idPessoa: String!, $idVinculoPessoa: String!) {
  apresentacoesAvaliador(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
    idApresentacao
    id2Apresentacao
    codigoApresentacao
    salaApresentacao
    horaApresentacao
    areaTematica {
      nomeAreaTematica
    }
    categoria {
      idCategoria
      nomeCategoria
    }
    projetos {
      idProjeto
      tituloProjeto
      codigoProjeto
      mediaIndividual(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
        mediaProjeto
      }
    }
  }
}
    `;
export type ApresentacoesAvaliadorComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ApresentacoesAvaliadorQuery, ApresentacoesAvaliadorQueryVariables>, 'query'> & ({ variables: ApresentacoesAvaliadorQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const ApresentacoesAvaliadorComponent = (props: ApresentacoesAvaliadorComponentProps) => (
      <ApolloReactComponents.Query<ApresentacoesAvaliadorQuery, ApresentacoesAvaliadorQueryVariables> query={ApresentacoesAvaliadorDocument} {...props} />
    );
    

/**
 * __useApresentacoesAvaliadorQuery__
 *
 * To run a query within a React component, call `useApresentacoesAvaliadorQuery` and pass it any options that fit your needs.
 * When your component renders, `useApresentacoesAvaliadorQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useApresentacoesAvaliadorQuery({
 *   variables: {
 *      idPessoa: // value for 'idPessoa'
 *      idVinculoPessoa: // value for 'idVinculoPessoa'
 *   },
 * });
 */
export function useApresentacoesAvaliadorQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ApresentacoesAvaliadorQuery, ApresentacoesAvaliadorQueryVariables>) {
        return ApolloReactHooks.useQuery<ApresentacoesAvaliadorQuery, ApresentacoesAvaliadorQueryVariables>(ApresentacoesAvaliadorDocument, baseOptions);
      }
export function useApresentacoesAvaliadorLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ApresentacoesAvaliadorQuery, ApresentacoesAvaliadorQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<ApresentacoesAvaliadorQuery, ApresentacoesAvaliadorQueryVariables>(ApresentacoesAvaliadorDocument, baseOptions);
        }
export type ApresentacoesAvaliadorQueryHookResult = ReturnType<typeof useApresentacoesAvaliadorQuery>;
export type ApresentacoesAvaliadorLazyQueryHookResult = ReturnType<typeof useApresentacoesAvaliadorLazyQuery>;
export type ApresentacoesAvaliadorQueryResult = ApolloReactCommon.QueryResult<ApresentacoesAvaliadorQuery, ApresentacoesAvaliadorQueryVariables>;
export const GetCampiDocument = gql`
    query GetCampi {
  campi {
    ...Campus
  }
}
    ${CampusFragmentDoc}`;
export type GetCampiComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetCampiQuery, GetCampiQueryVariables>, 'query'>;

    export const GetCampiComponent = (props: GetCampiComponentProps) => (
      <ApolloReactComponents.Query<GetCampiQuery, GetCampiQueryVariables> query={GetCampiDocument} {...props} />
    );
    

/**
 * __useGetCampiQuery__
 *
 * To run a query within a React component, call `useGetCampiQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCampiQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCampiQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCampiQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetCampiQuery, GetCampiQueryVariables>) {
        return ApolloReactHooks.useQuery<GetCampiQuery, GetCampiQueryVariables>(GetCampiDocument, baseOptions);
      }
export function useGetCampiLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetCampiQuery, GetCampiQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetCampiQuery, GetCampiQueryVariables>(GetCampiDocument, baseOptions);
        }
export type GetCampiQueryHookResult = ReturnType<typeof useGetCampiQuery>;
export type GetCampiLazyQueryHookResult = ReturnType<typeof useGetCampiLazyQuery>;
export type GetCampiQueryResult = ApolloReactCommon.QueryResult<GetCampiQuery, GetCampiQueryVariables>;
export const GetCampusDocument = gql`
    query GetCampus($idCampus: Int!) {
  campus(idCampus: $idCampus) {
    ...Campus
  }
}
    ${CampusFragmentDoc}`;
export type GetCampusComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetCampusQuery, GetCampusQueryVariables>, 'query'> & ({ variables: GetCampusQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetCampusComponent = (props: GetCampusComponentProps) => (
      <ApolloReactComponents.Query<GetCampusQuery, GetCampusQueryVariables> query={GetCampusDocument} {...props} />
    );
    

/**
 * __useGetCampusQuery__
 *
 * To run a query within a React component, call `useGetCampusQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCampusQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCampusQuery({
 *   variables: {
 *      idCampus: // value for 'idCampus'
 *   },
 * });
 */
export function useGetCampusQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetCampusQuery, GetCampusQueryVariables>) {
        return ApolloReactHooks.useQuery<GetCampusQuery, GetCampusQueryVariables>(GetCampusDocument, baseOptions);
      }
export function useGetCampusLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetCampusQuery, GetCampusQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetCampusQuery, GetCampusQueryVariables>(GetCampusDocument, baseOptions);
        }
export type GetCampusQueryHookResult = ReturnType<typeof useGetCampusQuery>;
export type GetCampusLazyQueryHookResult = ReturnType<typeof useGetCampusLazyQuery>;
export type GetCampusQueryResult = ApolloReactCommon.QueryResult<GetCampusQuery, GetCampusQueryVariables>;
export const GetCategoriesDocument = gql`
    query getCategories {
  categorias {
    ...Category
  }
}
    ${CategoryFragmentDoc}`;
export type GetCategoriesComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetCategoriesQuery, GetCategoriesQueryVariables>, 'query'>;

    export const GetCategoriesComponent = (props: GetCategoriesComponentProps) => (
      <ApolloReactComponents.Query<GetCategoriesQuery, GetCategoriesQueryVariables> query={GetCategoriesDocument} {...props} />
    );
    

/**
 * __useGetCategoriesQuery__
 *
 * To run a query within a React component, call `useGetCategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCategoriesQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCategoriesQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetCategoriesQuery, GetCategoriesQueryVariables>) {
        return ApolloReactHooks.useQuery<GetCategoriesQuery, GetCategoriesQueryVariables>(GetCategoriesDocument, baseOptions);
      }
export function useGetCategoriesLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetCategoriesQuery, GetCategoriesQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetCategoriesQuery, GetCategoriesQueryVariables>(GetCategoriesDocument, baseOptions);
        }
export type GetCategoriesQueryHookResult = ReturnType<typeof useGetCategoriesQuery>;
export type GetCategoriesLazyQueryHookResult = ReturnType<typeof useGetCategoriesLazyQuery>;
export type GetCategoriesQueryResult = ApolloReactCommon.QueryResult<GetCategoriesQuery, GetCategoriesQueryVariables>;
export const GetCategoriasComPerguntasDocument = gql`
    query GetCategoriasComPerguntas {
  categorias {
    idCategoria
    nomeCategoria
    perguntas {
      idPergunta
      conteudoPergunta
    }
  }
}
    `;
export type GetCategoriasComPerguntasComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetCategoriasComPerguntasQuery, GetCategoriasComPerguntasQueryVariables>, 'query'>;

    export const GetCategoriasComPerguntasComponent = (props: GetCategoriasComPerguntasComponentProps) => (
      <ApolloReactComponents.Query<GetCategoriasComPerguntasQuery, GetCategoriasComPerguntasQueryVariables> query={GetCategoriasComPerguntasDocument} {...props} />
    );
    

/**
 * __useGetCategoriasComPerguntasQuery__
 *
 * To run a query within a React component, call `useGetCategoriasComPerguntasQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCategoriasComPerguntasQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCategoriasComPerguntasQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCategoriasComPerguntasQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetCategoriasComPerguntasQuery, GetCategoriasComPerguntasQueryVariables>) {
        return ApolloReactHooks.useQuery<GetCategoriasComPerguntasQuery, GetCategoriasComPerguntasQueryVariables>(GetCategoriasComPerguntasDocument, baseOptions);
      }
export function useGetCategoriasComPerguntasLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetCategoriasComPerguntasQuery, GetCategoriasComPerguntasQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetCategoriasComPerguntasQuery, GetCategoriasComPerguntasQueryVariables>(GetCategoriasComPerguntasDocument, baseOptions);
        }
export type GetCategoriasComPerguntasQueryHookResult = ReturnType<typeof useGetCategoriasComPerguntasQuery>;
export type GetCategoriasComPerguntasLazyQueryHookResult = ReturnType<typeof useGetCategoriasComPerguntasLazyQuery>;
export type GetCategoriasComPerguntasQueryResult = ApolloReactCommon.QueryResult<GetCategoriasComPerguntasQuery, GetCategoriasComPerguntasQueryVariables>;
export const CreateEventDocument = gql`
    mutation createEvent($input: criarEventoInput!) {
  criarEvento(input: $input) {
    ...Event
  }
}
    ${EventFragmentDoc}`;
export type CreateEventMutationFn = ApolloReactCommon.MutationFunction<CreateEventMutation, CreateEventMutationVariables>;
export type CreateEventComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CreateEventMutation, CreateEventMutationVariables>, 'mutation'>;

    export const CreateEventComponent = (props: CreateEventComponentProps) => (
      <ApolloReactComponents.Mutation<CreateEventMutation, CreateEventMutationVariables> mutation={CreateEventDocument} {...props} />
    );
    

/**
 * __useCreateEventMutation__
 *
 * To run a mutation, you first call `useCreateEventMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateEventMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createEventMutation, { data, loading, error }] = useCreateEventMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateEventMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateEventMutation, CreateEventMutationVariables>) {
        return ApolloReactHooks.useMutation<CreateEventMutation, CreateEventMutationVariables>(CreateEventDocument, baseOptions);
      }
export type CreateEventMutationHookResult = ReturnType<typeof useCreateEventMutation>;
export type CreateEventMutationResult = ApolloReactCommon.MutationResult<CreateEventMutation>;
export type CreateEventMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateEventMutation, CreateEventMutationVariables>;
export const GetEventDocument = gql`
    query GetEvent($idEvento: Int!) {
  evento(idEvento: $idEvento) {
    ...Event
  }
}
    ${EventFragmentDoc}`;
export type GetEventComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetEventQuery, GetEventQueryVariables>, 'query'> & ({ variables: GetEventQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetEventComponent = (props: GetEventComponentProps) => (
      <ApolloReactComponents.Query<GetEventQuery, GetEventQueryVariables> query={GetEventDocument} {...props} />
    );
    

/**
 * __useGetEventQuery__
 *
 * To run a query within a React component, call `useGetEventQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetEventQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetEventQuery({
 *   variables: {
 *      idEvento: // value for 'idEvento'
 *   },
 * });
 */
export function useGetEventQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetEventQuery, GetEventQueryVariables>) {
        return ApolloReactHooks.useQuery<GetEventQuery, GetEventQueryVariables>(GetEventDocument, baseOptions);
      }
export function useGetEventLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetEventQuery, GetEventQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetEventQuery, GetEventQueryVariables>(GetEventDocument, baseOptions);
        }
export type GetEventQueryHookResult = ReturnType<typeof useGetEventQuery>;
export type GetEventLazyQueryHookResult = ReturnType<typeof useGetEventLazyQuery>;
export type GetEventQueryResult = ApolloReactCommon.QueryResult<GetEventQuery, GetEventQueryVariables>;
export const GetEventThematicsDocument = gql`
    query GetEventThematics($idEvento: Int!) {
  evento(idEvento: $idEvento) {
    areasTematicas {
      idAreaTematica
      nomeAreaTematica
    }
  }
}
    `;
export type GetEventThematicsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetEventThematicsQuery, GetEventThematicsQueryVariables>, 'query'> & ({ variables: GetEventThematicsQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetEventThematicsComponent = (props: GetEventThematicsComponentProps) => (
      <ApolloReactComponents.Query<GetEventThematicsQuery, GetEventThematicsQueryVariables> query={GetEventThematicsDocument} {...props} />
    );
    

/**
 * __useGetEventThematicsQuery__
 *
 * To run a query within a React component, call `useGetEventThematicsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetEventThematicsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetEventThematicsQuery({
 *   variables: {
 *      idEvento: // value for 'idEvento'
 *   },
 * });
 */
export function useGetEventThematicsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetEventThematicsQuery, GetEventThematicsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetEventThematicsQuery, GetEventThematicsQueryVariables>(GetEventThematicsDocument, baseOptions);
      }
export function useGetEventThematicsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetEventThematicsQuery, GetEventThematicsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetEventThematicsQuery, GetEventThematicsQueryVariables>(GetEventThematicsDocument, baseOptions);
        }
export type GetEventThematicsQueryHookResult = ReturnType<typeof useGetEventThematicsQuery>;
export type GetEventThematicsLazyQueryHookResult = ReturnType<typeof useGetEventThematicsLazyQuery>;
export type GetEventThematicsQueryResult = ApolloReactCommon.QueryResult<GetEventThematicsQuery, GetEventThematicsQueryVariables>;
export const GetEventsDocument = gql`
    query GetEvents {
  eventos {
    ...Event
  }
}
    ${EventFragmentDoc}`;
export type GetEventsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetEventsQuery, GetEventsQueryVariables>, 'query'>;

    export const GetEventsComponent = (props: GetEventsComponentProps) => (
      <ApolloReactComponents.Query<GetEventsQuery, GetEventsQueryVariables> query={GetEventsDocument} {...props} />
    );
    

/**
 * __useGetEventsQuery__
 *
 * To run a query within a React component, call `useGetEventsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetEventsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetEventsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetEventsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetEventsQuery, GetEventsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetEventsQuery, GetEventsQueryVariables>(GetEventsDocument, baseOptions);
      }
export function useGetEventsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetEventsQuery, GetEventsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetEventsQuery, GetEventsQueryVariables>(GetEventsDocument, baseOptions);
        }
export type GetEventsQueryHookResult = ReturnType<typeof useGetEventsQuery>;
export type GetEventsLazyQueryHookResult = ReturnType<typeof useGetEventsLazyQuery>;
export type GetEventsQueryResult = ApolloReactCommon.QueryResult<GetEventsQuery, GetEventsQueryVariables>;
export const UpdateEventDocument = gql`
    mutation updateEvent($input: atualizarEventoInput!) {
  atualizarEvento(input: $input) {
    ...Event
  }
}
    ${EventFragmentDoc}`;
export type UpdateEventMutationFn = ApolloReactCommon.MutationFunction<UpdateEventMutation, UpdateEventMutationVariables>;
export type UpdateEventComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<UpdateEventMutation, UpdateEventMutationVariables>, 'mutation'>;

    export const UpdateEventComponent = (props: UpdateEventComponentProps) => (
      <ApolloReactComponents.Mutation<UpdateEventMutation, UpdateEventMutationVariables> mutation={UpdateEventDocument} {...props} />
    );
    

/**
 * __useUpdateEventMutation__
 *
 * To run a mutation, you first call `useUpdateEventMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateEventMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateEventMutation, { data, loading, error }] = useUpdateEventMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateEventMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateEventMutation, UpdateEventMutationVariables>) {
        return ApolloReactHooks.useMutation<UpdateEventMutation, UpdateEventMutationVariables>(UpdateEventDocument, baseOptions);
      }
export type UpdateEventMutationHookResult = ReturnType<typeof useUpdateEventMutation>;
export type UpdateEventMutationResult = ApolloReactCommon.MutationResult<UpdateEventMutation>;
export type UpdateEventMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdateEventMutation, UpdateEventMutationVariables>;
export const ExportPageContainerDocument = gql`
    query exportPageContainer($idEvento: Int!) {
  evento(idEvento: $idEvento) {
    ...Event
  }
  projetos(input: {idEvento: $idEvento}) {
    ...Project
    media
    __typename
  }
}
    ${EventFragmentDoc}
${ProjectFragmentDoc}`;
export type ExportPageContainerComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ExportPageContainerQuery, ExportPageContainerQueryVariables>, 'query'> & ({ variables: ExportPageContainerQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const ExportPageContainerComponent = (props: ExportPageContainerComponentProps) => (
      <ApolloReactComponents.Query<ExportPageContainerQuery, ExportPageContainerQueryVariables> query={ExportPageContainerDocument} {...props} />
    );
    

/**
 * __useExportPageContainerQuery__
 *
 * To run a query within a React component, call `useExportPageContainerQuery` and pass it any options that fit your needs.
 * When your component renders, `useExportPageContainerQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useExportPageContainerQuery({
 *   variables: {
 *      idEvento: // value for 'idEvento'
 *   },
 * });
 */
export function useExportPageContainerQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ExportPageContainerQuery, ExportPageContainerQueryVariables>) {
        return ApolloReactHooks.useQuery<ExportPageContainerQuery, ExportPageContainerQueryVariables>(ExportPageContainerDocument, baseOptions);
      }
export function useExportPageContainerLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ExportPageContainerQuery, ExportPageContainerQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<ExportPageContainerQuery, ExportPageContainerQueryVariables>(ExportPageContainerDocument, baseOptions);
        }
export type ExportPageContainerQueryHookResult = ReturnType<typeof useExportPageContainerQuery>;
export type ExportPageContainerLazyQueryHookResult = ReturnType<typeof useExportPageContainerLazyQuery>;
export type ExportPageContainerQueryResult = ApolloReactCommon.QueryResult<ExportPageContainerQuery, ExportPageContainerQueryVariables>;
export const AtualizarSolicitacaoLgpdDocument = gql`
    mutation atualizarSolicitacaoLGPD($idSolicitacao: Int!, $statusSolicitacao: String) {
  atualizarSolicitacaoLGPD(input: {idSolicitacao: $idSolicitacao, statusSolicitacao: $statusSolicitacao}) {
    idSolicitacao
    statusSolicitacao
  }
}
    `;
export type AtualizarSolicitacaoLgpdMutationFn = ApolloReactCommon.MutationFunction<AtualizarSolicitacaoLgpdMutation, AtualizarSolicitacaoLgpdMutationVariables>;
export type AtualizarSolicitacaoLgpdComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AtualizarSolicitacaoLgpdMutation, AtualizarSolicitacaoLgpdMutationVariables>, 'mutation'>;

    export const AtualizarSolicitacaoLgpdComponent = (props: AtualizarSolicitacaoLgpdComponentProps) => (
      <ApolloReactComponents.Mutation<AtualizarSolicitacaoLgpdMutation, AtualizarSolicitacaoLgpdMutationVariables> mutation={AtualizarSolicitacaoLgpdDocument} {...props} />
    );
    

/**
 * __useAtualizarSolicitacaoLgpdMutation__
 *
 * To run a mutation, you first call `useAtualizarSolicitacaoLgpdMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAtualizarSolicitacaoLgpdMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [atualizarSolicitacaoLgpdMutation, { data, loading, error }] = useAtualizarSolicitacaoLgpdMutation({
 *   variables: {
 *      idSolicitacao: // value for 'idSolicitacao'
 *      statusSolicitacao: // value for 'statusSolicitacao'
 *   },
 * });
 */
export function useAtualizarSolicitacaoLgpdMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AtualizarSolicitacaoLgpdMutation, AtualizarSolicitacaoLgpdMutationVariables>) {
        return ApolloReactHooks.useMutation<AtualizarSolicitacaoLgpdMutation, AtualizarSolicitacaoLgpdMutationVariables>(AtualizarSolicitacaoLgpdDocument, baseOptions);
      }
export type AtualizarSolicitacaoLgpdMutationHookResult = ReturnType<typeof useAtualizarSolicitacaoLgpdMutation>;
export type AtualizarSolicitacaoLgpdMutationResult = ApolloReactCommon.MutationResult<AtualizarSolicitacaoLgpdMutation>;
export type AtualizarSolicitacaoLgpdMutationOptions = ApolloReactCommon.BaseMutationOptions<AtualizarSolicitacaoLgpdMutation, AtualizarSolicitacaoLgpdMutationVariables>;
export const CriarSolicitacaoLgpdDocument = gql`
    mutation criarSolicitacaoLGPD($tipoSolicitacao: Int!, $textoSolicitacao: String, $idPessoa: String!, $idVinculoPessoa: String!, $nomeSolicitacao: String!, $sobrenomeSolicitacao: String!, $emailPessoa: String!) {
  criarSolicitacaoLGPD(input: {tipoSolicitacao: $tipoSolicitacao, textoSolicitacao: $textoSolicitacao, idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa, nomeSolicitacao: $nomeSolicitacao, sobrenomeSolicitacao: $sobrenomeSolicitacao, emailPessoa: $emailPessoa}) {
    idSolicitacao
  }
}
    `;
export type CriarSolicitacaoLgpdMutationFn = ApolloReactCommon.MutationFunction<CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables>;
export type CriarSolicitacaoLgpdComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables>, 'mutation'>;

    export const CriarSolicitacaoLgpdComponent = (props: CriarSolicitacaoLgpdComponentProps) => (
      <ApolloReactComponents.Mutation<CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables> mutation={CriarSolicitacaoLgpdDocument} {...props} />
    );
    

/**
 * __useCriarSolicitacaoLgpdMutation__
 *
 * To run a mutation, you first call `useCriarSolicitacaoLgpdMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCriarSolicitacaoLgpdMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [criarSolicitacaoLgpdMutation, { data, loading, error }] = useCriarSolicitacaoLgpdMutation({
 *   variables: {
 *      tipoSolicitacao: // value for 'tipoSolicitacao'
 *      textoSolicitacao: // value for 'textoSolicitacao'
 *      idPessoa: // value for 'idPessoa'
 *      idVinculoPessoa: // value for 'idVinculoPessoa'
 *      nomeSolicitacao: // value for 'nomeSolicitacao'
 *      sobrenomeSolicitacao: // value for 'sobrenomeSolicitacao'
 *      emailPessoa: // value for 'emailPessoa'
 *   },
 * });
 */
export function useCriarSolicitacaoLgpdMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables>) {
        return ApolloReactHooks.useMutation<CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables>(CriarSolicitacaoLgpdDocument, baseOptions);
      }
export type CriarSolicitacaoLgpdMutationHookResult = ReturnType<typeof useCriarSolicitacaoLgpdMutation>;
export type CriarSolicitacaoLgpdMutationResult = ApolloReactCommon.MutationResult<CriarSolicitacaoLgpdMutation>;
export type CriarSolicitacaoLgpdMutationOptions = ApolloReactCommon.BaseMutationOptions<CriarSolicitacaoLgpdMutation, CriarSolicitacaoLgpdMutationVariables>;
export const GetSolicitacoesLgpdDocument = gql`
    query GetSolicitacoesLGPD {
  solicitacoesLGPD {
    idSolicitacao
    tipoSolicitacao
    textoSolicitacao
    nomeSolicitacao
    sobrenomeSolicitacao
    emailPessoa
    statusSolicitacao
  }
}
    `;
export type GetSolicitacoesLgpdComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetSolicitacoesLgpdQuery, GetSolicitacoesLgpdQueryVariables>, 'query'>;

    export const GetSolicitacoesLgpdComponent = (props: GetSolicitacoesLgpdComponentProps) => (
      <ApolloReactComponents.Query<GetSolicitacoesLgpdQuery, GetSolicitacoesLgpdQueryVariables> query={GetSolicitacoesLgpdDocument} {...props} />
    );
    

/**
 * __useGetSolicitacoesLgpdQuery__
 *
 * To run a query within a React component, call `useGetSolicitacoesLgpdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSolicitacoesLgpdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSolicitacoesLgpdQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetSolicitacoesLgpdQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetSolicitacoesLgpdQuery, GetSolicitacoesLgpdQueryVariables>) {
        return ApolloReactHooks.useQuery<GetSolicitacoesLgpdQuery, GetSolicitacoesLgpdQueryVariables>(GetSolicitacoesLgpdDocument, baseOptions);
      }
export function useGetSolicitacoesLgpdLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetSolicitacoesLgpdQuery, GetSolicitacoesLgpdQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetSolicitacoesLgpdQuery, GetSolicitacoesLgpdQueryVariables>(GetSolicitacoesLgpdDocument, baseOptions);
        }
export type GetSolicitacoesLgpdQueryHookResult = ReturnType<typeof useGetSolicitacoesLgpdQuery>;
export type GetSolicitacoesLgpdLazyQueryHookResult = ReturnType<typeof useGetSolicitacoesLgpdLazyQuery>;
export type GetSolicitacoesLgpdQueryResult = ApolloReactCommon.QueryResult<GetSolicitacoesLgpdQuery, GetSolicitacoesLgpdQueryVariables>;
export const GetPresentationsWithProjectsDocument = gql`
    query GetPresentationsWithProjects($input: apresentacoesInput) {
  apresentacoes(input: $input) {
    ...Presentation
    projetos {
      ...Project
      media
    }
  }
}
    ${PresentationFragmentDoc}
${ProjectFragmentDoc}`;
export type GetPresentationsWithProjectsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetPresentationsWithProjectsQuery, GetPresentationsWithProjectsQueryVariables>, 'query'>;

    export const GetPresentationsWithProjectsComponent = (props: GetPresentationsWithProjectsComponentProps) => (
      <ApolloReactComponents.Query<GetPresentationsWithProjectsQuery, GetPresentationsWithProjectsQueryVariables> query={GetPresentationsWithProjectsDocument} {...props} />
    );
    

/**
 * __useGetPresentationsWithProjectsQuery__
 *
 * To run a query within a React component, call `useGetPresentationsWithProjectsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPresentationsWithProjectsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPresentationsWithProjectsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetPresentationsWithProjectsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetPresentationsWithProjectsQuery, GetPresentationsWithProjectsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetPresentationsWithProjectsQuery, GetPresentationsWithProjectsQueryVariables>(GetPresentationsWithProjectsDocument, baseOptions);
      }
export function useGetPresentationsWithProjectsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetPresentationsWithProjectsQuery, GetPresentationsWithProjectsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetPresentationsWithProjectsQuery, GetPresentationsWithProjectsQueryVariables>(GetPresentationsWithProjectsDocument, baseOptions);
        }
export type GetPresentationsWithProjectsQueryHookResult = ReturnType<typeof useGetPresentationsWithProjectsQuery>;
export type GetPresentationsWithProjectsLazyQueryHookResult = ReturnType<typeof useGetPresentationsWithProjectsLazyQuery>;
export type GetPresentationsWithProjectsQueryResult = ApolloReactCommon.QueryResult<GetPresentationsWithProjectsQuery, GetPresentationsWithProjectsQueryVariables>;
export const PresentationsPageContainerDocument = gql`
    query presentationsPageContainer {
  apresentacoes {
    ...Presentation
  }
}
    ${PresentationFragmentDoc}`;
export type PresentationsPageContainerComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<PresentationsPageContainerQuery, PresentationsPageContainerQueryVariables>, 'query'>;

    export const PresentationsPageContainerComponent = (props: PresentationsPageContainerComponentProps) => (
      <ApolloReactComponents.Query<PresentationsPageContainerQuery, PresentationsPageContainerQueryVariables> query={PresentationsPageContainerDocument} {...props} />
    );
    

/**
 * __usePresentationsPageContainerQuery__
 *
 * To run a query within a React component, call `usePresentationsPageContainerQuery` and pass it any options that fit your needs.
 * When your component renders, `usePresentationsPageContainerQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePresentationsPageContainerQuery({
 *   variables: {
 *   },
 * });
 */
export function usePresentationsPageContainerQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<PresentationsPageContainerQuery, PresentationsPageContainerQueryVariables>) {
        return ApolloReactHooks.useQuery<PresentationsPageContainerQuery, PresentationsPageContainerQueryVariables>(PresentationsPageContainerDocument, baseOptions);
      }
export function usePresentationsPageContainerLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<PresentationsPageContainerQuery, PresentationsPageContainerQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<PresentationsPageContainerQuery, PresentationsPageContainerQueryVariables>(PresentationsPageContainerDocument, baseOptions);
        }
export type PresentationsPageContainerQueryHookResult = ReturnType<typeof usePresentationsPageContainerQuery>;
export type PresentationsPageContainerLazyQueryHookResult = ReturnType<typeof usePresentationsPageContainerLazyQuery>;
export type PresentationsPageContainerQueryResult = ApolloReactCommon.QueryResult<PresentationsPageContainerQuery, PresentationsPageContainerQueryVariables>;
export const GetPresentationsDocument = gql`
    query GetPresentations {
  apresentacoes {
    ...Presentation
  }
}
    ${PresentationFragmentDoc}`;
export type GetPresentationsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetPresentationsQuery, GetPresentationsQueryVariables>, 'query'>;

    export const GetPresentationsComponent = (props: GetPresentationsComponentProps) => (
      <ApolloReactComponents.Query<GetPresentationsQuery, GetPresentationsQueryVariables> query={GetPresentationsDocument} {...props} />
    );
    

/**
 * __useGetPresentationsQuery__
 *
 * To run a query within a React component, call `useGetPresentationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPresentationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPresentationsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetPresentationsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetPresentationsQuery, GetPresentationsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetPresentationsQuery, GetPresentationsQueryVariables>(GetPresentationsDocument, baseOptions);
      }
export function useGetPresentationsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetPresentationsQuery, GetPresentationsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetPresentationsQuery, GetPresentationsQueryVariables>(GetPresentationsDocument, baseOptions);
        }
export type GetPresentationsQueryHookResult = ReturnType<typeof useGetPresentationsQuery>;
export type GetPresentationsLazyQueryHookResult = ReturnType<typeof useGetPresentationsLazyQuery>;
export type GetPresentationsQueryResult = ApolloReactCommon.QueryResult<GetPresentationsQuery, GetPresentationsQueryVariables>;
export const GetEventPresentationsDocument = gql`
    query GetEventPresentations($input: apresentacoesInput!) {
  apresentacoes(input: $input) {
    ...Presentation
  }
}
    ${PresentationFragmentDoc}`;
export type GetEventPresentationsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetEventPresentationsQuery, GetEventPresentationsQueryVariables>, 'query'> & ({ variables: GetEventPresentationsQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetEventPresentationsComponent = (props: GetEventPresentationsComponentProps) => (
      <ApolloReactComponents.Query<GetEventPresentationsQuery, GetEventPresentationsQueryVariables> query={GetEventPresentationsDocument} {...props} />
    );
    

/**
 * __useGetEventPresentationsQuery__
 *
 * To run a query within a React component, call `useGetEventPresentationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetEventPresentationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetEventPresentationsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetEventPresentationsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetEventPresentationsQuery, GetEventPresentationsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetEventPresentationsQuery, GetEventPresentationsQueryVariables>(GetEventPresentationsDocument, baseOptions);
      }
export function useGetEventPresentationsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetEventPresentationsQuery, GetEventPresentationsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetEventPresentationsQuery, GetEventPresentationsQueryVariables>(GetEventPresentationsDocument, baseOptions);
        }
export type GetEventPresentationsQueryHookResult = ReturnType<typeof useGetEventPresentationsQuery>;
export type GetEventPresentationsLazyQueryHookResult = ReturnType<typeof useGetEventPresentationsLazyQuery>;
export type GetEventPresentationsQueryResult = ApolloReactCommon.QueryResult<GetEventPresentationsQuery, GetEventPresentationsQueryVariables>;
export const GetEventPresentationsWithProjectsDocument = gql`
    query GetEventPresentationsWithProjects($input: apresentacoesInput) {
  apresentacoes(input: $input) {
    ...Presentation
    projetos {
      ...Project
      media
    }
  }
}
    ${PresentationFragmentDoc}
${ProjectFragmentDoc}`;
export type GetEventPresentationsWithProjectsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetEventPresentationsWithProjectsQuery, GetEventPresentationsWithProjectsQueryVariables>, 'query'>;

    export const GetEventPresentationsWithProjectsComponent = (props: GetEventPresentationsWithProjectsComponentProps) => (
      <ApolloReactComponents.Query<GetEventPresentationsWithProjectsQuery, GetEventPresentationsWithProjectsQueryVariables> query={GetEventPresentationsWithProjectsDocument} {...props} />
    );
    

/**
 * __useGetEventPresentationsWithProjectsQuery__
 *
 * To run a query within a React component, call `useGetEventPresentationsWithProjectsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetEventPresentationsWithProjectsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetEventPresentationsWithProjectsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetEventPresentationsWithProjectsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetEventPresentationsWithProjectsQuery, GetEventPresentationsWithProjectsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetEventPresentationsWithProjectsQuery, GetEventPresentationsWithProjectsQueryVariables>(GetEventPresentationsWithProjectsDocument, baseOptions);
      }
export function useGetEventPresentationsWithProjectsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetEventPresentationsWithProjectsQuery, GetEventPresentationsWithProjectsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetEventPresentationsWithProjectsQuery, GetEventPresentationsWithProjectsQueryVariables>(GetEventPresentationsWithProjectsDocument, baseOptions);
        }
export type GetEventPresentationsWithProjectsQueryHookResult = ReturnType<typeof useGetEventPresentationsWithProjectsQuery>;
export type GetEventPresentationsWithProjectsLazyQueryHookResult = ReturnType<typeof useGetEventPresentationsWithProjectsLazyQuery>;
export type GetEventPresentationsWithProjectsQueryResult = ApolloReactCommon.QueryResult<GetEventPresentationsWithProjectsQuery, GetEventPresentationsWithProjectsQueryVariables>;
export const GetPresentationReviewersDocument = gql`
    query GetPresentationReviewers($input: apresentacaoInput!) {
  apresentacao(input: $input) {
    ...PresentationBasic
    avaliadores {
      ... on Avaliador {
        ...User
      }
    }
  }
}
    ${PresentationBasicFragmentDoc}
${UserFragmentDoc}`;
export type GetPresentationReviewersComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetPresentationReviewersQuery, GetPresentationReviewersQueryVariables>, 'query'> & ({ variables: GetPresentationReviewersQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetPresentationReviewersComponent = (props: GetPresentationReviewersComponentProps) => (
      <ApolloReactComponents.Query<GetPresentationReviewersQuery, GetPresentationReviewersQueryVariables> query={GetPresentationReviewersDocument} {...props} />
    );
    

/**
 * __useGetPresentationReviewersQuery__
 *
 * To run a query within a React component, call `useGetPresentationReviewersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPresentationReviewersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPresentationReviewersQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetPresentationReviewersQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetPresentationReviewersQuery, GetPresentationReviewersQueryVariables>) {
        return ApolloReactHooks.useQuery<GetPresentationReviewersQuery, GetPresentationReviewersQueryVariables>(GetPresentationReviewersDocument, baseOptions);
      }
export function useGetPresentationReviewersLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetPresentationReviewersQuery, GetPresentationReviewersQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetPresentationReviewersQuery, GetPresentationReviewersQueryVariables>(GetPresentationReviewersDocument, baseOptions);
        }
export type GetPresentationReviewersQueryHookResult = ReturnType<typeof useGetPresentationReviewersQuery>;
export type GetPresentationReviewersLazyQueryHookResult = ReturnType<typeof useGetPresentationReviewersLazyQuery>;
export type GetPresentationReviewersQueryResult = ApolloReactCommon.QueryResult<GetPresentationReviewersQuery, GetPresentationReviewersQueryVariables>;
export const GetPresentationsOptionsDocument = gql`
    query GetPresentationsOptions($input: apresentacoesInput) {
  apresentacoes(input: $input) {
    ...Presentation
    evento {
      idEvento
      nomeEvento
      campi {
        idCampus
      }
      areasTematicas {
        idAreaTematica
        nomeAreaTematica
      }
    }
    projetos {
      idProjeto
      tituloProjeto
      statusProjeto
      descricaoProjeto
      dataInicioProjeto
      dataFimProjeto
    }
  }
}
    ${PresentationFragmentDoc}`;
export type GetPresentationsOptionsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetPresentationsOptionsQuery, GetPresentationsOptionsQueryVariables>, 'query'>;

    export const GetPresentationsOptionsComponent = (props: GetPresentationsOptionsComponentProps) => (
      <ApolloReactComponents.Query<GetPresentationsOptionsQuery, GetPresentationsOptionsQueryVariables> query={GetPresentationsOptionsDocument} {...props} />
    );
    

/**
 * __useGetPresentationsOptionsQuery__
 *
 * To run a query within a React component, call `useGetPresentationsOptionsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPresentationsOptionsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPresentationsOptionsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetPresentationsOptionsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetPresentationsOptionsQuery, GetPresentationsOptionsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetPresentationsOptionsQuery, GetPresentationsOptionsQueryVariables>(GetPresentationsOptionsDocument, baseOptions);
      }
export function useGetPresentationsOptionsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetPresentationsOptionsQuery, GetPresentationsOptionsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetPresentationsOptionsQuery, GetPresentationsOptionsQueryVariables>(GetPresentationsOptionsDocument, baseOptions);
        }
export type GetPresentationsOptionsQueryHookResult = ReturnType<typeof useGetPresentationsOptionsQuery>;
export type GetPresentationsOptionsLazyQueryHookResult = ReturnType<typeof useGetPresentationsOptionsLazyQuery>;
export type GetPresentationsOptionsQueryResult = ApolloReactCommon.QueryResult<GetPresentationsOptionsQuery, GetPresentationsOptionsQueryVariables>;
export const GetAllPresentationsOptionDocument = gql`
    query GetAllPresentationsOption {
  apresentacoes {
    ...Presentation
    evento {
      idEvento
      nomeEvento
      campi {
        idCampus
      }
      areasTematicas {
        idAreaTematica
        nomeAreaTematica
      }
    }
    projetos {
      idProjeto
      tituloProjeto
      statusProjeto
      descricaoProjeto
      dataInicioProjeto
      dataFimProjeto
    }
  }
}
    ${PresentationFragmentDoc}`;
export type GetAllPresentationsOptionComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetAllPresentationsOptionQuery, GetAllPresentationsOptionQueryVariables>, 'query'>;

    export const GetAllPresentationsOptionComponent = (props: GetAllPresentationsOptionComponentProps) => (
      <ApolloReactComponents.Query<GetAllPresentationsOptionQuery, GetAllPresentationsOptionQueryVariables> query={GetAllPresentationsOptionDocument} {...props} />
    );
    

/**
 * __useGetAllPresentationsOptionQuery__
 *
 * To run a query within a React component, call `useGetAllPresentationsOptionQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllPresentationsOptionQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllPresentationsOptionQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetAllPresentationsOptionQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetAllPresentationsOptionQuery, GetAllPresentationsOptionQueryVariables>) {
        return ApolloReactHooks.useQuery<GetAllPresentationsOptionQuery, GetAllPresentationsOptionQueryVariables>(GetAllPresentationsOptionDocument, baseOptions);
      }
export function useGetAllPresentationsOptionLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetAllPresentationsOptionQuery, GetAllPresentationsOptionQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetAllPresentationsOptionQuery, GetAllPresentationsOptionQueryVariables>(GetAllPresentationsOptionDocument, baseOptions);
        }
export type GetAllPresentationsOptionQueryHookResult = ReturnType<typeof useGetAllPresentationsOptionQuery>;
export type GetAllPresentationsOptionLazyQueryHookResult = ReturnType<typeof useGetAllPresentationsOptionLazyQuery>;
export type GetAllPresentationsOptionQueryResult = ApolloReactCommon.QueryResult<GetAllPresentationsOptionQuery, GetAllPresentationsOptionQueryVariables>;
export const GeneretaeQrPresentationsDocument = gql`
    query GeneretaeQRPresentations($idEvento: Int!, $modalidadeApresentacao: String, $idAreaTematica: Int) {
  apresentacoes(input: {idEvento: $idEvento, modalidadeApresentacao: $modalidadeApresentacao, idAreaTematica: $idAreaTematica}) {
    idApresentacao
    id2Apresentacao
    salaApresentacao
    codigoApresentacao
    modalidadeApresentacao
    projetos {
      idProjeto
      tituloProjeto
    }
    areaTematica {
      idAreaTematica
      nomeAreaTematica
    }
    evento {
      idEvento
      nomeEvento
    }
  }
}
    `;
export type GeneretaeQrPresentationsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GeneretaeQrPresentationsQuery, GeneretaeQrPresentationsQueryVariables>, 'query'> & ({ variables: GeneretaeQrPresentationsQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GeneretaeQrPresentationsComponent = (props: GeneretaeQrPresentationsComponentProps) => (
      <ApolloReactComponents.Query<GeneretaeQrPresentationsQuery, GeneretaeQrPresentationsQueryVariables> query={GeneretaeQrPresentationsDocument} {...props} />
    );
    

/**
 * __useGeneretaeQrPresentationsQuery__
 *
 * To run a query within a React component, call `useGeneretaeQrPresentationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGeneretaeQrPresentationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGeneretaeQrPresentationsQuery({
 *   variables: {
 *      idEvento: // value for 'idEvento'
 *      modalidadeApresentacao: // value for 'modalidadeApresentacao'
 *      idAreaTematica: // value for 'idAreaTematica'
 *   },
 * });
 */


/* adicionar projetos
*/
export const AdicionarProjetosDocument = gql`
    mutation adicionarProjetos($input: adicionarProjetosInput!) {
    adicionarProjetos(input: $input) {
    ...Presentation
  }
}
     ${PresentationFragmentDoc}
 
    `;
export type AdicionarProjetosMutationFn = ApolloReactCommon.MutationFunction<AdicionarProjetosMutation, AdicionarProjetosMutationVariables>;
export type AdicionarProjetosComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AdicionarProjetosMutation, AdicionarProjetosMutationVariables>, 'mutation'>;

    export const AdicionarProjetosComponent = (props: AdicionarProjetosComponentProps) => (
      <ApolloReactComponents.Mutation<AdicionarProjetosMutation, AdicionarProjetosMutationVariables> mutation={AdicionarProjetosDocument} {...props} />
    );

export function useAdicionarProjetosMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AdicionarProjetosMutation, AdicionarProjetosMutationVariables>) {
  return ApolloReactHooks.useMutation<AdicionarProjetosMutation, AdicionarProjetosMutationVariables>(AdicionarProjetosDocument, baseOptions);
}
export type AdicionarProjetosMutationHookResult = ReturnType<typeof useAdicionarProjetosMutation>;
export type AdicionarProjetosMutationResult = ApolloReactCommon.MutationResult<AdicionarProjetosMutation>;
export type AdicionarProjetosMutationOptions = ApolloReactCommon.BaseMutationOptions<AdicionarProjetosMutation, AdicionarProjetosMutationVariables>;

/* Criar projetos
*/
export const CriarProjetosDocument = gql`
    mutation criarProjeto($input: criarProjetoInput!) {
    criarProjeto(input: $input) {
    ...Project
      
  }
}
     ${ProjectFragmentDoc}
 
    `;
export type CriarProjetosMutationFn = ApolloReactCommon.MutationFunction<CriarProjetosMutation, CriarProjetosMutationVariables>;
export type CriarProjetosComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CriarProjetosMutation, CriarProjetosMutationVariables>, 'mutation'>;

    export const CriarProjetosComponent = (props: CriarProjetosComponentProps) => (
      <ApolloReactComponents.Mutation<CriarProjetosMutation, CriarProjetosMutationVariables> mutation={CriarProjetosDocument} {...props} />
    );

export function useCriarProjetosMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CriarProjetosMutation, CriarProjetosMutationVariables>) {
  return ApolloReactHooks.useMutation<CriarProjetosMutation, CriarProjetosMutationVariables>(CriarProjetosDocument, baseOptions);
}
export type CriarProjetosMutationHookResult = ReturnType<typeof useCriarProjetosMutation>;
export type CriarProjetosMutationResult = ApolloReactCommon.MutationResult<CriarProjetosMutation>;
export type CriarProjetosMutationOptions = ApolloReactCommon.BaseMutationOptions<CriarProjetosMutation, CriarProjetosMutationVariables>;


/* useGeneretaeQrPresentationsQuery
*/
export function useGeneretaeQrPresentationsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GeneretaeQrPresentationsQuery, GeneretaeQrPresentationsQueryVariables>) {
        return ApolloReactHooks.useQuery<GeneretaeQrPresentationsQuery, GeneretaeQrPresentationsQueryVariables>(GeneretaeQrPresentationsDocument, baseOptions);
      }
export function useGeneretaeQrPresentationsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GeneretaeQrPresentationsQuery, GeneretaeQrPresentationsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GeneretaeQrPresentationsQuery, GeneretaeQrPresentationsQueryVariables>(GeneretaeQrPresentationsDocument, baseOptions);
        }
export type GeneretaeQrPresentationsQueryHookResult = ReturnType<typeof useGeneretaeQrPresentationsQuery>;
export type GeneretaeQrPresentationsLazyQueryHookResult = ReturnType<typeof useGeneretaeQrPresentationsLazyQuery>;
export type GeneretaeQrPresentationsQueryResult = ApolloReactCommon.QueryResult<GeneretaeQrPresentationsQuery, GeneretaeQrPresentationsQueryVariables>;
export const CriarApresentacaoDocument = gql`
    mutation criarApresentacao($input: criarApresentacaoInput!) {
  criarApresentacao(input: $input) {
    ...Presentation
    modalidadeApresentacao
  }
}
    ${PresentationFragmentDoc}`;
export type CriarApresentacaoMutationFn = ApolloReactCommon.MutationFunction<CriarApresentacaoMutation, CriarApresentacaoMutationVariables>;
export type CriarApresentacaoComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CriarApresentacaoMutation, CriarApresentacaoMutationVariables>, 'mutation'>;

    export const CriarApresentacaoComponent = (props: CriarApresentacaoComponentProps) => (
      <ApolloReactComponents.Mutation<CriarApresentacaoMutation, CriarApresentacaoMutationVariables> mutation={CriarApresentacaoDocument} {...props} />
    );
    

/**
 * __useCriarApresentacaoMutation__
 *
 * To run a mutation, you first call `useCriarApresentacaoMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCriarApresentacaoMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [criarApresentacaoMutation, { data, loading, error }] = useCriarApresentacaoMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCriarApresentacaoMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CriarApresentacaoMutation, CriarApresentacaoMutationVariables>) {
        return ApolloReactHooks.useMutation<CriarApresentacaoMutation, CriarApresentacaoMutationVariables>(CriarApresentacaoDocument, baseOptions);
      }
export type CriarApresentacaoMutationHookResult = ReturnType<typeof useCriarApresentacaoMutation>;
export type CriarApresentacaoMutationResult = ApolloReactCommon.MutationResult<CriarApresentacaoMutation>;
export type CriarApresentacaoMutationOptions = ApolloReactCommon.BaseMutationOptions<CriarApresentacaoMutation, CriarApresentacaoMutationVariables>;
export const AtualizarApresentacaoDocument = gql`
    mutation atualizarApresentacao($input: atualizarApresentacaoInput!) {
  atualizarApresentacao(input: $input) {
    ...Presentation
  }
}
    ${PresentationFragmentDoc}`;
export type AtualizarApresentacaoMutationFn = ApolloReactCommon.MutationFunction<AtualizarApresentacaoMutation, AtualizarApresentacaoMutationVariables>;
export type AtualizarApresentacaoComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AtualizarApresentacaoMutation, AtualizarApresentacaoMutationVariables>, 'mutation'>;

    export const AtualizarApresentacaoComponent = (props: AtualizarApresentacaoComponentProps) => (
      <ApolloReactComponents.Mutation<AtualizarApresentacaoMutation, AtualizarApresentacaoMutationVariables> mutation={AtualizarApresentacaoDocument} {...props} />
    );   


/**
 * __useAtualizarApresentacaoMutation__
 *
 * To run a mutation, you first call `useAtualizarApresentacaoMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAtualizarApresentacaoMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [atualizarApresentacaoMutation, { data, loading, error }] = useAtualizarApresentacaoMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAtualizarApresentacaoMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AtualizarApresentacaoMutation, AtualizarApresentacaoMutationVariables>) {
        return ApolloReactHooks.useMutation<AtualizarApresentacaoMutation, AtualizarApresentacaoMutationVariables>(AtualizarApresentacaoDocument, baseOptions);
      }
export type AtualizarApresentacaoMutationHookResult = ReturnType<typeof useAtualizarApresentacaoMutation>;
export type AtualizarApresentacaoMutationResult = ApolloReactCommon.MutationResult<AtualizarApresentacaoMutation>;
export type AtualizarApresentacaoMutationOptions = ApolloReactCommon.BaseMutationOptions<AtualizarApresentacaoMutation, AtualizarApresentacaoMutationVariables>;
export const ExcluirApresentacaoDocument = gql`
    mutation excluirApresentacao($input: excluirApresentacaoInput!) {
  excluirApresentacao(input: $input) {
    ...Presentation
  }
}
    ${PresentationFragmentDoc}`;
export type ExcluirApresentacaoMutationFn = ApolloReactCommon.MutationFunction<ExcluirApresentacaoMutation, ExcluirApresentacaoMutationVariables>;
export type ExcluirApresentacaoComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<ExcluirApresentacaoMutation, ExcluirApresentacaoMutationVariables>, 'mutation'>;

    export const ExcluirApresentacaoComponent = (props: ExcluirApresentacaoComponentProps) => (
      <ApolloReactComponents.Mutation<ExcluirApresentacaoMutation, ExcluirApresentacaoMutationVariables> mutation={ExcluirApresentacaoDocument} {...props} />
    );
    

/**
 * __useExcluirApresentacaoMutation__
 *
 * To run a mutation, you first call `useExcluirApresentacaoMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useExcluirApresentacaoMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [excluirApresentacaoMutation, { data, loading, error }] = useExcluirApresentacaoMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useExcluirApresentacaoMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<ExcluirApresentacaoMutation, ExcluirApresentacaoMutationVariables>) {
        return ApolloReactHooks.useMutation<ExcluirApresentacaoMutation, ExcluirApresentacaoMutationVariables>(ExcluirApresentacaoDocument, baseOptions);
      }
export type ExcluirApresentacaoMutationHookResult = ReturnType<typeof useExcluirApresentacaoMutation>;
export type ExcluirApresentacaoMutationResult = ApolloReactCommon.MutationResult<ExcluirApresentacaoMutation>;
export type ExcluirApresentacaoMutationOptions = ApolloReactCommon.BaseMutationOptions<ExcluirApresentacaoMutation, ExcluirApresentacaoMutationVariables>;
export const AlocatProjetosDocument = gql`
    mutation AlocatProjetos($input: alocarProjetoInput!) {
  alocarProjetos(input: $input) {
    ...Presentation
  }
}
    ${PresentationFragmentDoc}`;
export type AlocatProjetosMutationFn = ApolloReactCommon.MutationFunction<AlocatProjetosMutation, AlocatProjetosMutationVariables>;
export type AlocatProjetosComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AlocatProjetosMutation, AlocatProjetosMutationVariables>, 'mutation'>;

    export const AlocatProjetosComponent = (props: AlocatProjetosComponentProps) => (
      <ApolloReactComponents.Mutation<AlocatProjetosMutation, AlocatProjetosMutationVariables> mutation={AlocatProjetosDocument} {...props} />
    );
    

/**
 * __useAlocatProjetosMutation__
 *
 * To run a mutation, you first call `useAlocatProjetosMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAlocatProjetosMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [alocatProjetosMutation, { data, loading, error }] = useAlocatProjetosMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAlocatProjetosMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AlocatProjetosMutation, AlocatProjetosMutationVariables>) {
        return ApolloReactHooks.useMutation<AlocatProjetosMutation, AlocatProjetosMutationVariables>(AlocatProjetosDocument, baseOptions);
      }
export type AlocatProjetosMutationHookResult = ReturnType<typeof useAlocatProjetosMutation>;
export type AlocatProjetosMutationResult = ApolloReactCommon.MutationResult<AlocatProjetosMutation>;
export type AlocatProjetosMutationOptions = ApolloReactCommon.BaseMutationOptions<AlocatProjetosMutation, AlocatProjetosMutationVariables>;
export const GetPresentationProjectsDocument = gql`
    query getPresentationProjects($input: apresentacaoInput!) {
  apresentacao(input: $input) {
    ...Presentation
    projetos {
      ...Project
      media
    }
  }
}
    ${PresentationFragmentDoc}
${ProjectFragmentDoc}`;
export type GetPresentationProjectsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetPresentationProjectsQuery, GetPresentationProjectsQueryVariables>, 'query'> & ({ variables: GetPresentationProjectsQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetPresentationProjectsComponent = (props: GetPresentationProjectsComponentProps) => (
      <ApolloReactComponents.Query<GetPresentationProjectsQuery, GetPresentationProjectsQueryVariables> query={GetPresentationProjectsDocument} {...props} />
    );
    

/**
 * __useGetPresentationProjectsQuery__
 *
 * To run a query within a React component, call `useGetPresentationProjectsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPresentationProjectsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPresentationProjectsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetPresentationProjectsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetPresentationProjectsQuery, GetPresentationProjectsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetPresentationProjectsQuery, GetPresentationProjectsQueryVariables>(GetPresentationProjectsDocument, baseOptions);
      }
export function useGetPresentationProjectsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetPresentationProjectsQuery, GetPresentationProjectsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetPresentationProjectsQuery, GetPresentationProjectsQueryVariables>(GetPresentationProjectsDocument, baseOptions);
        }
export type GetPresentationProjectsQueryHookResult = ReturnType<typeof useGetPresentationProjectsQuery>;
export type GetPresentationProjectsLazyQueryHookResult = ReturnType<typeof useGetPresentationProjectsLazyQuery>;
export type GetPresentationProjectsQueryResult = ApolloReactCommon.QueryResult<GetPresentationProjectsQuery, GetPresentationProjectsQueryVariables>;
export const GetProjectDocument = gql`
    query getProject($idProjeto: Int!) {
  projeto(idProjeto: $idProjeto) {
    ...Project
  }
}
    ${ProjectFragmentDoc}`;
export type GetProjectComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetProjectQuery, GetProjectQueryVariables>, 'query'> & ({ variables: GetProjectQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetProjectComponent = (props: GetProjectComponentProps) => (
      <ApolloReactComponents.Query<GetProjectQuery, GetProjectQueryVariables> query={GetProjectDocument} {...props} />
    );
    

/**
 * __useGetProjectQuery__
 *
 * To run a query within a React component, call `useGetProjectQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetProjectQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetProjectQuery({
 *   variables: {
 *      idProjeto: // value for 'idProjeto'
 *   },
 * });
 */
export function useGetProjectQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetProjectQuery, GetProjectQueryVariables>) {
        return ApolloReactHooks.useQuery<GetProjectQuery, GetProjectQueryVariables>(GetProjectDocument, baseOptions);
      }
export function useGetProjectLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetProjectQuery, GetProjectQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetProjectQuery, GetProjectQueryVariables>(GetProjectDocument, baseOptions);
        }
export type GetProjectQueryHookResult = ReturnType<typeof useGetProjectQuery>;
export type GetProjectLazyQueryHookResult = ReturnType<typeof useGetProjectLazyQuery>;
export type GetProjectQueryResult = ApolloReactCommon.QueryResult<GetProjectQuery, GetProjectQueryVariables>;
export const GetProjectDetailsDocument = gql`
    query getProjectDetails($idProjeto: Int!) {
  projeto(idProjeto: $idProjeto) {
    ...Project
    media
    mediasIndividuais {
      mediaProjeto
    }
  }
}
    ${ProjectFragmentDoc}`;
export type GetProjectDetailsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetProjectDetailsQuery, GetProjectDetailsQueryVariables>, 'query'> & ({ variables: GetProjectDetailsQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetProjectDetailsComponent = (props: GetProjectDetailsComponentProps) => (
      <ApolloReactComponents.Query<GetProjectDetailsQuery, GetProjectDetailsQueryVariables> query={GetProjectDetailsDocument} {...props} />
    );
    

/**
 * __useGetProjectDetailsQuery__
 *
 * To run a query within a React component, call `useGetProjectDetailsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetProjectDetailsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetProjectDetailsQuery({
 *   variables: {
 *      idProjeto: // value for 'idProjeto'
 *   },
 * });
 */
export function useGetProjectDetailsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetProjectDetailsQuery, GetProjectDetailsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetProjectDetailsQuery, GetProjectDetailsQueryVariables>(GetProjectDetailsDocument, baseOptions);
      }
export function useGetProjectDetailsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetProjectDetailsQuery, GetProjectDetailsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetProjectDetailsQuery, GetProjectDetailsQueryVariables>(GetProjectDetailsDocument, baseOptions);
        }
export type GetProjectDetailsQueryHookResult = ReturnType<typeof useGetProjectDetailsQuery>;
export type GetProjectDetailsLazyQueryHookResult = ReturnType<typeof useGetProjectDetailsLazyQuery>;
export type GetProjectDetailsQueryResult = ApolloReactCommon.QueryResult<GetProjectDetailsQuery, GetProjectDetailsQueryVariables>;
export const GetProjectReviewsDocument = gql`
    query getProjectReviews($idProjeto: Int!) {
  projeto(idProjeto: $idProjeto) {
    idProjeto
    tituloProjeto
    statusProjeto
    descricaoProjeto
    dataInicioProjeto
    dataFimProjeto
    media
    mediasIndividuais {
      mediaProjeto
    }
  }
}
    `;
export type GetProjectReviewsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetProjectReviewsQuery, GetProjectReviewsQueryVariables>, 'query'> & ({ variables: GetProjectReviewsQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetProjectReviewsComponent = (props: GetProjectReviewsComponentProps) => (
      <ApolloReactComponents.Query<GetProjectReviewsQuery, GetProjectReviewsQueryVariables> query={GetProjectReviewsDocument} {...props} />
    );
    

/**
 * __useGetProjectReviewsQuery__
 *
 * To run a query within a React component, call `useGetProjectReviewsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetProjectReviewsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetProjectReviewsQuery({
 *   variables: {
 *      idProjeto: // value for 'idProjeto'
 *   },
 * });
 */
export function useGetProjectReviewsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetProjectReviewsQuery, GetProjectReviewsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetProjectReviewsQuery, GetProjectReviewsQueryVariables>(GetProjectReviewsDocument, baseOptions);
      }
export function useGetProjectReviewsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetProjectReviewsQuery, GetProjectReviewsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetProjectReviewsQuery, GetProjectReviewsQueryVariables>(GetProjectReviewsDocument, baseOptions);
        }
export type GetProjectReviewsQueryHookResult = ReturnType<typeof useGetProjectReviewsQuery>;
export type GetProjectReviewsLazyQueryHookResult = ReturnType<typeof useGetProjectReviewsLazyQuery>;
export type GetProjectReviewsQueryResult = ApolloReactCommon.QueryResult<GetProjectReviewsQuery, GetProjectReviewsQueryVariables>;
export const GetProjectsDocument = gql`
    query GetProjects {
  projetos {
    ...Project
    media
  }
}
    ${ProjectFragmentDoc}`;
export type GetProjectsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetProjectsQuery, GetProjectsQueryVariables>, 'query'>;

    export const GetProjectsComponent = (props: GetProjectsComponentProps) => (
      <ApolloReactComponents.Query<GetProjectsQuery, GetProjectsQueryVariables> query={GetProjectsDocument} {...props} />
    );
    

/**
 * __useGetProjectsQuery__
 *
 * To run a query within a React component, call `useGetProjectsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetProjectsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetProjectsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetProjectsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetProjectsQuery, GetProjectsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetProjectsQuery, GetProjectsQueryVariables>(GetProjectsDocument, baseOptions);
      }
export function useGetProjectsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetProjectsQuery, GetProjectsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetProjectsQuery, GetProjectsQueryVariables>(GetProjectsDocument, baseOptions);
        }
export type GetProjectsQueryHookResult = ReturnType<typeof useGetProjectsQuery>;
export type GetProjectsLazyQueryHookResult = ReturnType<typeof useGetProjectsLazyQuery>;
export type GetProjectsQueryResult = ApolloReactCommon.QueryResult<GetProjectsQuery, GetProjectsQueryVariables>;
export const ProjectsPageContainerDocument = gql`
    query projectsPageContainer {
  projetos {
    ...Project
    media
  }
  areasTematicas {
    ...AreaTematica
  }
}
    ${ProjectFragmentDoc}
${AreaTematicaFragmentDoc}`;
export type ProjectsPageContainerComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ProjectsPageContainerQuery, ProjectsPageContainerQueryVariables>, 'query'>;

    export const ProjectsPageContainerComponent = (props: ProjectsPageContainerComponentProps) => (
      <ApolloReactComponents.Query<ProjectsPageContainerQuery, ProjectsPageContainerQueryVariables> query={ProjectsPageContainerDocument} {...props} />
    );
    

/**
 * __useProjectsPageContainerQuery__
 *
 * To run a query within a React component, call `useProjectsPageContainerQuery` and pass it any options that fit your needs.
 * When your component renders, `useProjectsPageContainerQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useProjectsPageContainerQuery({
 *   variables: {
 *   },
 * });
 */
export function useProjectsPageContainerQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ProjectsPageContainerQuery, ProjectsPageContainerQueryVariables>) {
        return ApolloReactHooks.useQuery<ProjectsPageContainerQuery, ProjectsPageContainerQueryVariables>(ProjectsPageContainerDocument, baseOptions);
      }
export function useProjectsPageContainerLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ProjectsPageContainerQuery, ProjectsPageContainerQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<ProjectsPageContainerQuery, ProjectsPageContainerQueryVariables>(ProjectsPageContainerDocument, baseOptions);
        }
export type ProjectsPageContainerQueryHookResult = ReturnType<typeof useProjectsPageContainerQuery>;
export type ProjectsPageContainerLazyQueryHookResult = ReturnType<typeof useProjectsPageContainerLazyQuery>;
export type ProjectsPageContainerQueryResult = ApolloReactCommon.QueryResult<ProjectsPageContainerQuery, ProjectsPageContainerQueryVariables>;
export const GetReviewerProjectsDocument = gql`
    query getReviewerProjects($idPessoa: String!, $idVinculoPessoa: String!) {
  pessoa(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
    idPessoa
    idVinculoPessoa
    ... on Gerente {
      projetosParaAvaliar {
        ...ProjetoPessoa
      }
    }
    ... on Avaliador {
      projetosParaAvaliar {
        ...ProjetoPessoa
      }
    }
  }
}
    ${ProjetoPessoaFragmentDoc}`;
export type GetReviewerProjectsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetReviewerProjectsQuery, GetReviewerProjectsQueryVariables>, 'query'> & ({ variables: GetReviewerProjectsQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetReviewerProjectsComponent = (props: GetReviewerProjectsComponentProps) => (
      <ApolloReactComponents.Query<GetReviewerProjectsQuery, GetReviewerProjectsQueryVariables> query={GetReviewerProjectsDocument} {...props} />
    );
    

/**
 * __useGetReviewerProjectsQuery__
 *
 * To run a query within a React component, call `useGetReviewerProjectsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetReviewerProjectsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetReviewerProjectsQuery({
 *   variables: {
 *      idPessoa: // value for 'idPessoa'
 *      idVinculoPessoa: // value for 'idVinculoPessoa'
 *   },
 * });
 */
export function useGetReviewerProjectsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetReviewerProjectsQuery, GetReviewerProjectsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetReviewerProjectsQuery, GetReviewerProjectsQueryVariables>(GetReviewerProjectsDocument, baseOptions);
      }
export function useGetReviewerProjectsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetReviewerProjectsQuery, GetReviewerProjectsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetReviewerProjectsQuery, GetReviewerProjectsQueryVariables>(GetReviewerProjectsDocument, baseOptions);
        }
export type GetReviewerProjectsQueryHookResult = ReturnType<typeof useGetReviewerProjectsQuery>;
export type GetReviewerProjectsLazyQueryHookResult = ReturnType<typeof useGetReviewerProjectsLazyQuery>;
export type GetReviewerProjectsQueryResult = ApolloReactCommon.QueryResult<GetReviewerProjectsQuery, GetReviewerProjectsQueryVariables>;
export const MakeProjectReviewDocument = gql`
    mutation makeProjectReview($input: avaliarProjetoInput!) {
  avaliarProjeto(input: $input)
}
    `;
export type MakeProjectReviewMutationFn = ApolloReactCommon.MutationFunction<MakeProjectReviewMutation, MakeProjectReviewMutationVariables>;
export type MakeProjectReviewComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<MakeProjectReviewMutation, MakeProjectReviewMutationVariables>, 'mutation'>;

    export const MakeProjectReviewComponent = (props: MakeProjectReviewComponentProps) => (
      <ApolloReactComponents.Mutation<MakeProjectReviewMutation, MakeProjectReviewMutationVariables> mutation={MakeProjectReviewDocument} {...props} />
    );
    

/**
 * __useMakeProjectReviewMutation__
 *
 * To run a mutation, you first call `useMakeProjectReviewMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useMakeProjectReviewMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [makeProjectReviewMutation, { data, loading, error }] = useMakeProjectReviewMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useMakeProjectReviewMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<MakeProjectReviewMutation, MakeProjectReviewMutationVariables>) {
        return ApolloReactHooks.useMutation<MakeProjectReviewMutation, MakeProjectReviewMutationVariables>(MakeProjectReviewDocument, baseOptions);
      }
export type MakeProjectReviewMutationHookResult = ReturnType<typeof useMakeProjectReviewMutation>;
export type MakeProjectReviewMutationResult = ApolloReactCommon.MutationResult<MakeProjectReviewMutation>;
export type MakeProjectReviewMutationOptions = ApolloReactCommon.BaseMutationOptions<MakeProjectReviewMutation, MakeProjectReviewMutationVariables>;
export const UpdateProjectDocument = gql`
    mutation updateProject($input: atualizarProjetoInput!) {
  atualizarProjeto(input: $input) {
    ...Project
  }
}
    ${ProjectFragmentDoc}`;
export type UpdateProjectMutationFn = ApolloReactCommon.MutationFunction<UpdateProjectMutation, UpdateProjectMutationVariables>;
export type UpdateProjectComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<UpdateProjectMutation, UpdateProjectMutationVariables>, 'mutation'>;

    export const UpdateProjectComponent = (props: UpdateProjectComponentProps) => (
      <ApolloReactComponents.Mutation<UpdateProjectMutation, UpdateProjectMutationVariables> mutation={UpdateProjectDocument} {...props} />
    );
    

/**
 * __useUpdateProjectMutation__
 *
 * To run a mutation, you first call `useUpdateProjectMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateProjectMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateProjectMutation, { data, loading, error }] = useUpdateProjectMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateProjectMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateProjectMutation, UpdateProjectMutationVariables>) {
        return ApolloReactHooks.useMutation<UpdateProjectMutation, UpdateProjectMutationVariables>(UpdateProjectDocument, baseOptions);
      }
export type UpdateProjectMutationHookResult = ReturnType<typeof useUpdateProjectMutation>;
export type UpdateProjectMutationResult = ApolloReactCommon.MutationResult<UpdateProjectMutation>;
export type UpdateProjectMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdateProjectMutation, UpdateProjectMutationVariables>;
export const GetQuestionsDocument = gql`
    query getQuestions {
  perguntas {
    ...Question
    categoria {
      ...Category
    }
  }
}
    ${QuestionFragmentDoc}
${CategoryFragmentDoc}`;
export type GetQuestionsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetQuestionsQuery, GetQuestionsQueryVariables>, 'query'>;

    export const GetQuestionsComponent = (props: GetQuestionsComponentProps) => (
      <ApolloReactComponents.Query<GetQuestionsQuery, GetQuestionsQueryVariables> query={GetQuestionsDocument} {...props} />
    );
    

/**
 * __useGetQuestionsQuery__
 *
 * To run a query within a React component, call `useGetQuestionsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetQuestionsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetQuestionsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetQuestionsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetQuestionsQuery, GetQuestionsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetQuestionsQuery, GetQuestionsQueryVariables>(GetQuestionsDocument, baseOptions);
      }
export function useGetQuestionsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetQuestionsQuery, GetQuestionsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetQuestionsQuery, GetQuestionsQueryVariables>(GetQuestionsDocument, baseOptions);
        }
export type GetQuestionsQueryHookResult = ReturnType<typeof useGetQuestionsQuery>;
export type GetQuestionsLazyQueryHookResult = ReturnType<typeof useGetQuestionsLazyQuery>;
export type GetQuestionsQueryResult = ApolloReactCommon.QueryResult<GetQuestionsQuery, GetQuestionsQueryVariables>;
export const ReportPageContainerDocument = gql`
    query reportPageContainer($idEvento: Int!) {
  evento(idEvento: $idEvento) {
    ...Event
    areasTematicas {
      ...AreaTematica
    }
    apresentacoes {
      ...PresentationRequiredFields
      modalidadeApresentacao
      disponibilidadeApresentacao
      areaTematica {
        ...AreaTematica
      }
      projetos {
        ...Project
        media
      }
    }
  }
  pessoas {
    ...User
  }
}
    ${EventFragmentDoc}
${AreaTematicaFragmentDoc}
${PresentationRequiredFieldsFragmentDoc}
${ProjectFragmentDoc}
${UserFragmentDoc}`;
export type ReportPageContainerComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ReportPageContainerQuery, ReportPageContainerQueryVariables>, 'query'> & ({ variables: ReportPageContainerQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const ReportPageContainerComponent = (props: ReportPageContainerComponentProps) => (
      <ApolloReactComponents.Query<ReportPageContainerQuery, ReportPageContainerQueryVariables> query={ReportPageContainerDocument} {...props} />
    );
    

/**
 * __useReportPageContainerQuery__
 *
 * To run a query within a React component, call `useReportPageContainerQuery` and pass it any options that fit your needs.
 * When your component renders, `useReportPageContainerQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useReportPageContainerQuery({
 *   variables: {
 *      idEvento: // value for 'idEvento'
 *   },
 * });
 */
export function useReportPageContainerQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ReportPageContainerQuery, ReportPageContainerQueryVariables>) {
        return ApolloReactHooks.useQuery<ReportPageContainerQuery, ReportPageContainerQueryVariables>(ReportPageContainerDocument, baseOptions);
      }
export function useReportPageContainerLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ReportPageContainerQuery, ReportPageContainerQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<ReportPageContainerQuery, ReportPageContainerQueryVariables>(ReportPageContainerDocument, baseOptions);
        }
export type ReportPageContainerQueryHookResult = ReturnType<typeof useReportPageContainerQuery>;
export type ReportPageContainerLazyQueryHookResult = ReturnType<typeof useReportPageContainerLazyQuery>;
export type ReportPageContainerQueryResult = ApolloReactCommon.QueryResult<ReportPageContainerQuery, ReportPageContainerQueryVariables>;
export const AtualizarStatusReportDocument = gql`
    mutation atualizarStatusReport($idReport: Int!, $statusReport: String!) {
  atualizarStatusReport(input: {idReport: $idReport, statusReport: $statusReport}) {
    idReport
    conteudoReport
  }
}
    `;
export type AtualizarStatusReportMutationFn = ApolloReactCommon.MutationFunction<AtualizarStatusReportMutation, AtualizarStatusReportMutationVariables>;
export type AtualizarStatusReportComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AtualizarStatusReportMutation, AtualizarStatusReportMutationVariables>, 'mutation'>;

    export const AtualizarStatusReportComponent = (props: AtualizarStatusReportComponentProps) => (
      <ApolloReactComponents.Mutation<AtualizarStatusReportMutation, AtualizarStatusReportMutationVariables> mutation={AtualizarStatusReportDocument} {...props} />
    );
    

/**
 * __useAtualizarStatusReportMutation__
 *
 * To run a mutation, you first call `useAtualizarStatusReportMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAtualizarStatusReportMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [atualizarStatusReportMutation, { data, loading, error }] = useAtualizarStatusReportMutation({
 *   variables: {
 *      idReport: // value for 'idReport'
 *      statusReport: // value for 'statusReport'
 *   },
 * });
 */
export function useAtualizarStatusReportMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AtualizarStatusReportMutation, AtualizarStatusReportMutationVariables>) {
        return ApolloReactHooks.useMutation<AtualizarStatusReportMutation, AtualizarStatusReportMutationVariables>(AtualizarStatusReportDocument, baseOptions);
      }
export type AtualizarStatusReportMutationHookResult = ReturnType<typeof useAtualizarStatusReportMutation>;
export type AtualizarStatusReportMutationResult = ApolloReactCommon.MutationResult<AtualizarStatusReportMutation>;
export type AtualizarStatusReportMutationOptions = ApolloReactCommon.BaseMutationOptions<AtualizarStatusReportMutation, AtualizarStatusReportMutationVariables>;
export const CriarReportDocument = gql`
    mutation criarReport($conteudoReport: String!, $idPessoa: String!, $idVinculoPessoa: String!, $emailReport: String!) {
  criarReport(input: {conteudoReport: $conteudoReport, idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa, emailReport: $emailReport}) {
    idReport
  }
}
    `;
export type CriarReportMutationFn = ApolloReactCommon.MutationFunction<CriarReportMutation, CriarReportMutationVariables>;
export type CriarReportComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CriarReportMutation, CriarReportMutationVariables>, 'mutation'>;

    export const CriarReportComponent = (props: CriarReportComponentProps) => (
      <ApolloReactComponents.Mutation<CriarReportMutation, CriarReportMutationVariables> mutation={CriarReportDocument} {...props} />
    );
    

/**
 * __useCriarReportMutation__
 *
 * To run a mutation, you first call `useCriarReportMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCriarReportMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [criarReportMutation, { data, loading, error }] = useCriarReportMutation({
 *   variables: {
 *      conteudoReport: // value for 'conteudoReport'
 *      idPessoa: // value for 'idPessoa'
 *      idVinculoPessoa: // value for 'idVinculoPessoa'
 *      emailReport: // value for 'emailReport'
 *   },
 * });
 */
export function useCriarReportMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CriarReportMutation, CriarReportMutationVariables>) {
        return ApolloReactHooks.useMutation<CriarReportMutation, CriarReportMutationVariables>(CriarReportDocument, baseOptions);
      }
export type CriarReportMutationHookResult = ReturnType<typeof useCriarReportMutation>;
export type CriarReportMutationResult = ApolloReactCommon.MutationResult<CriarReportMutation>;
export type CriarReportMutationOptions = ApolloReactCommon.BaseMutationOptions<CriarReportMutation, CriarReportMutationVariables>;
export const GetReportsDocument = gql`
    query GetReports {
  reports {
    idReport
    conteudoReport
    emailReport
    dataCriacao
    statusReport
    avaliador {
      nomePessoa
      idPessoa
      idVinculoPessoa
    }
    apresentacao {
      salaApresentacao
      idApresentacao
      id2Apresentacao
    }
  }
}
    `;
export type GetReportsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetReportsQuery, GetReportsQueryVariables>, 'query'>;

    export const GetReportsComponent = (props: GetReportsComponentProps) => (
      <ApolloReactComponents.Query<GetReportsQuery, GetReportsQueryVariables> query={GetReportsDocument} {...props} />
    );
    

/**
 * __useGetReportsQuery__
 *
 * To run a query within a React component, call `useGetReportsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetReportsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetReportsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetReportsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetReportsQuery, GetReportsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetReportsQuery, GetReportsQueryVariables>(GetReportsDocument, baseOptions);
      }
export function useGetReportsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetReportsQuery, GetReportsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetReportsQuery, GetReportsQueryVariables>(GetReportsDocument, baseOptions);
        }
export type GetReportsQueryHookResult = ReturnType<typeof useGetReportsQuery>;
export type GetReportsLazyQueryHookResult = ReturnType<typeof useGetReportsLazyQuery>;
export type GetReportsQueryResult = ApolloReactCommon.QueryResult<GetReportsQuery, GetReportsQueryVariables>;
export const AvaliacoesAvaliadorDocument = gql`
    query avaliacoesAvaliador($idPessoa: String!, $idVinculoPessoa: String!) {
  pessoa(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
    idPessoa
    idVinculoPessoa
    ... on Gerente {
      avaliacoes {
        ...AvaliacaoPessoa
      }
    }
    ... on Avaliador {
      avaliacoes {
        ...AvaliacaoPessoa
      }
    }
  }
}
    ${AvaliacaoPessoaFragmentDoc}`;
export type AvaliacoesAvaliadorComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<AvaliacoesAvaliadorQuery, AvaliacoesAvaliadorQueryVariables>, 'query'> & ({ variables: AvaliacoesAvaliadorQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const AvaliacoesAvaliadorComponent = (props: AvaliacoesAvaliadorComponentProps) => (
      <ApolloReactComponents.Query<AvaliacoesAvaliadorQuery, AvaliacoesAvaliadorQueryVariables> query={AvaliacoesAvaliadorDocument} {...props} />
    );
    

/**
 * __useAvaliacoesAvaliadorQuery__
 *
 * To run a query within a React component, call `useAvaliacoesAvaliadorQuery` and pass it any options that fit your needs.
 * When your component renders, `useAvaliacoesAvaliadorQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAvaliacoesAvaliadorQuery({
 *   variables: {
 *      idPessoa: // value for 'idPessoa'
 *      idVinculoPessoa: // value for 'idVinculoPessoa'
 *   },
 * });
 */
export function useAvaliacoesAvaliadorQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<AvaliacoesAvaliadorQuery, AvaliacoesAvaliadorQueryVariables>) {
        return ApolloReactHooks.useQuery<AvaliacoesAvaliadorQuery, AvaliacoesAvaliadorQueryVariables>(AvaliacoesAvaliadorDocument, baseOptions);
      }
export function useAvaliacoesAvaliadorLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AvaliacoesAvaliadorQuery, AvaliacoesAvaliadorQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<AvaliacoesAvaliadorQuery, AvaliacoesAvaliadorQueryVariables>(AvaliacoesAvaliadorDocument, baseOptions);
        }
export type AvaliacoesAvaliadorQueryHookResult = ReturnType<typeof useAvaliacoesAvaliadorQuery>;
export type AvaliacoesAvaliadorLazyQueryHookResult = ReturnType<typeof useAvaliacoesAvaliadorLazyQuery>;
export type AvaliacoesAvaliadorQueryResult = ApolloReactCommon.QueryResult<AvaliacoesAvaliadorQuery, AvaliacoesAvaliadorQueryVariables>;
export const AvaliarProjetoDocument = gql`
    mutation avaliarProjeto($input: avaliarProjetoInput!) {
  avaliarProjeto(input: $input)
}
    `;
export type AvaliarProjetoMutationFn = ApolloReactCommon.MutationFunction<AvaliarProjetoMutation, AvaliarProjetoMutationVariables>;
export type AvaliarProjetoComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AvaliarProjetoMutation, AvaliarProjetoMutationVariables>, 'mutation'>;

    export const AvaliarProjetoComponent = (props: AvaliarProjetoComponentProps) => (
      <ApolloReactComponents.Mutation<AvaliarProjetoMutation, AvaliarProjetoMutationVariables> mutation={AvaliarProjetoDocument} {...props} />
    );
    

/**
 * __useAvaliarProjetoMutation__
 *
 * To run a mutation, you first call `useAvaliarProjetoMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAvaliarProjetoMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [avaliarProjetoMutation, { data, loading, error }] = useAvaliarProjetoMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAvaliarProjetoMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AvaliarProjetoMutation, AvaliarProjetoMutationVariables>) {
        return ApolloReactHooks.useMutation<AvaliarProjetoMutation, AvaliarProjetoMutationVariables>(AvaliarProjetoDocument, baseOptions);
      }
export type AvaliarProjetoMutationHookResult = ReturnType<typeof useAvaliarProjetoMutation>;
export type AvaliarProjetoMutationResult = ApolloReactCommon.MutationResult<AvaliarProjetoMutation>;
export type AvaliarProjetoMutationOptions = ApolloReactCommon.BaseMutationOptions<AvaliarProjetoMutation, AvaliarProjetoMutationVariables>;
export const AvaliarProjetoPorUmAvaliadorDocument = gql`
    mutation avaliarProjetoPorUmAvaliador($input: avaliarProjetoPorUmAvaliadorInput!) {
  avaliarProjetoPorUmAvaliador(input: $input)
}
    `;
export type AvaliarProjetoPorUmAvaliadorMutationFn = ApolloReactCommon.MutationFunction<AvaliarProjetoPorUmAvaliadorMutation, AvaliarProjetoPorUmAvaliadorMutationVariables>;
export type AvaliarProjetoPorUmAvaliadorComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AvaliarProjetoPorUmAvaliadorMutation, AvaliarProjetoPorUmAvaliadorMutationVariables>, 'mutation'>;

    export const AvaliarProjetoPorUmAvaliadorComponent = (props: AvaliarProjetoPorUmAvaliadorComponentProps) => (
      <ApolloReactComponents.Mutation<AvaliarProjetoPorUmAvaliadorMutation, AvaliarProjetoPorUmAvaliadorMutationVariables> mutation={AvaliarProjetoPorUmAvaliadorDocument} {...props} />
    );
    

/**
 * __useAvaliarProjetoPorUmAvaliadorMutation__
 *
 * To run a mutation, you first call `useAvaliarProjetoPorUmAvaliadorMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAvaliarProjetoPorUmAvaliadorMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [avaliarProjetoPorUmAvaliadorMutation, { data, loading, error }] = useAvaliarProjetoPorUmAvaliadorMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAvaliarProjetoPorUmAvaliadorMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AvaliarProjetoPorUmAvaliadorMutation, AvaliarProjetoPorUmAvaliadorMutationVariables>) {
        return ApolloReactHooks.useMutation<AvaliarProjetoPorUmAvaliadorMutation, AvaliarProjetoPorUmAvaliadorMutationVariables>(AvaliarProjetoPorUmAvaliadorDocument, baseOptions);
      }
export type AvaliarProjetoPorUmAvaliadorMutationHookResult = ReturnType<typeof useAvaliarProjetoPorUmAvaliadorMutation>;
export type AvaliarProjetoPorUmAvaliadorMutationResult = ApolloReactCommon.MutationResult<AvaliarProjetoPorUmAvaliadorMutation>;
export type AvaliarProjetoPorUmAvaliadorMutationOptions = ApolloReactCommon.BaseMutationOptions<AvaliarProjetoPorUmAvaliadorMutation, AvaliarProjetoPorUmAvaliadorMutationVariables>;
export const GetAddReviewPageDataDocument = gql`
    query getAddReviewPageData($idProjeto: Int!, $idPessoa: String!, $idVinculoPessoa: String!) {
  projeto(idProjeto: $idProjeto) {
    idProjeto
    tituloProjeto
    categoria {
      idCategoria
      nomeCategoria
      perguntas {
        idPergunta
        conteudoPergunta
        categoria {
          idCategoria
        }
      }
    }
  }
  pessoa(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
    ...User
  }
}
    ${UserFragmentDoc}`;
export type GetAddReviewPageDataComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetAddReviewPageDataQuery, GetAddReviewPageDataQueryVariables>, 'query'> & ({ variables: GetAddReviewPageDataQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetAddReviewPageDataComponent = (props: GetAddReviewPageDataComponentProps) => (
      <ApolloReactComponents.Query<GetAddReviewPageDataQuery, GetAddReviewPageDataQueryVariables> query={GetAddReviewPageDataDocument} {...props} />
    );
    

/**
 * __useGetAddReviewPageDataQuery__
 *
 * To run a query within a React component, call `useGetAddReviewPageDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAddReviewPageDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAddReviewPageDataQuery({
 *   variables: {
 *      idProjeto: // value for 'idProjeto'
 *      idPessoa: // value for 'idPessoa'
 *      idVinculoPessoa: // value for 'idVinculoPessoa'
 *   },
 * });
 */
export function useGetAddReviewPageDataQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetAddReviewPageDataQuery, GetAddReviewPageDataQueryVariables>) {
        return ApolloReactHooks.useQuery<GetAddReviewPageDataQuery, GetAddReviewPageDataQueryVariables>(GetAddReviewPageDataDocument, baseOptions);
      }
export function useGetAddReviewPageDataLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetAddReviewPageDataQuery, GetAddReviewPageDataQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetAddReviewPageDataQuery, GetAddReviewPageDataQueryVariables>(GetAddReviewPageDataDocument, baseOptions);
        }
export type GetAddReviewPageDataQueryHookResult = ReturnType<typeof useGetAddReviewPageDataQuery>;
export type GetAddReviewPageDataLazyQueryHookResult = ReturnType<typeof useGetAddReviewPageDataLazyQuery>;
export type GetAddReviewPageDataQueryResult = ApolloReactCommon.QueryResult<GetAddReviewPageDataQuery, GetAddReviewPageDataQueryVariables>;
export const AreasTematicasDocument = gql`
    query areasTematicas {
  areasTematicas {
    ...AreaTematica
  }
}
    ${AreaTematicaFragmentDoc}`;
export type AreasTematicasComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<AreasTematicasQuery, AreasTematicasQueryVariables>, 'query'>;

    export const AreasTematicasComponent = (props: AreasTematicasComponentProps) => (
      <ApolloReactComponents.Query<AreasTematicasQuery, AreasTematicasQueryVariables> query={AreasTematicasDocument} {...props} />
    );
    

/**
 * __useAreasTematicasQuery__
 *
 * To run a query within a React component, call `useAreasTematicasQuery` and pass it any options that fit your needs.
 * When your component renders, `useAreasTematicasQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAreasTematicasQuery({
 *   variables: {
 *   },
 * });
 */
export function useAreasTematicasQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<AreasTematicasQuery, AreasTematicasQueryVariables>) {
        return ApolloReactHooks.useQuery<AreasTematicasQuery, AreasTematicasQueryVariables>(AreasTematicasDocument, baseOptions);
      }
export function useAreasTematicasLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<AreasTematicasQuery, AreasTematicasQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<AreasTematicasQuery, AreasTematicasQueryVariables>(AreasTematicasDocument, baseOptions);
        }
export type AreasTematicasQueryHookResult = ReturnType<typeof useAreasTematicasQuery>;
export type AreasTematicasLazyQueryHookResult = ReturnType<typeof useAreasTematicasLazyQuery>;
export type AreasTematicasQueryResult = ApolloReactCommon.QueryResult<AreasTematicasQuery, AreasTematicasQueryVariables>;
export const GetAreaTematicaDocument = gql`
    query GetAreaTematica($idAreaTematica: Int!) {
  areaTematica(idAreaTematica: $idAreaTematica) {
    ...AreaTematica
  }
}
    ${AreaTematicaFragmentDoc}`;
export type GetAreaTematicaComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetAreaTematicaQuery, GetAreaTematicaQueryVariables>, 'query'> & ({ variables: GetAreaTematicaQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetAreaTematicaComponent = (props: GetAreaTematicaComponentProps) => (
      <ApolloReactComponents.Query<GetAreaTematicaQuery, GetAreaTematicaQueryVariables> query={GetAreaTematicaDocument} {...props} />
    );
    

/**
 * __useGetAreaTematicaQuery__
 *
 * To run a query within a React component, call `useGetAreaTematicaQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAreaTematicaQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAreaTematicaQuery({
 *   variables: {
 *      idAreaTematica: // value for 'idAreaTematica'
 *   },
 * });
 */
export function useGetAreaTematicaQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetAreaTematicaQuery, GetAreaTematicaQueryVariables>) {
        return ApolloReactHooks.useQuery<GetAreaTematicaQuery, GetAreaTematicaQueryVariables>(GetAreaTematicaDocument, baseOptions);
      }
export function useGetAreaTematicaLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetAreaTematicaQuery, GetAreaTematicaQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetAreaTematicaQuery, GetAreaTematicaQueryVariables>(GetAreaTematicaDocument, baseOptions);
        }
export type GetAreaTematicaQueryHookResult = ReturnType<typeof useGetAreaTematicaQuery>;
export type GetAreaTematicaLazyQueryHookResult = ReturnType<typeof useGetAreaTematicaLazyQuery>;
export type GetAreaTematicaQueryResult = ApolloReactCommon.QueryResult<GetAreaTematicaQuery, GetAreaTematicaQueryVariables>;
export const CreateUserDocument = gql`
    mutation createUser($input: criarPessoaInput!) {
  criarPessoa(input: $input) {
    ...User
  }
}
    ${UserFragmentDoc}`;
export type CreateUserMutationFn = ApolloReactCommon.MutationFunction<CreateUserMutation, CreateUserMutationVariables>;
export type CreateUserComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CreateUserMutation, CreateUserMutationVariables>, 'mutation'>;

    export const CreateUserComponent = (props: CreateUserComponentProps) => (
      <ApolloReactComponents.Mutation<CreateUserMutation, CreateUserMutationVariables> mutation={CreateUserDocument} {...props} />
    );
    

/**
 * __useCreateUserMutation__
 *
 * To run a mutation, you first call `useCreateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createUserMutation, { data, loading, error }] = useCreateUserMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateUserMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateUserMutation, CreateUserMutationVariables>) {
        return ApolloReactHooks.useMutation<CreateUserMutation, CreateUserMutationVariables>(CreateUserDocument, baseOptions);
      }
export type CreateUserMutationHookResult = ReturnType<typeof useCreateUserMutation>;
export type CreateUserMutationResult = ApolloReactCommon.MutationResult<CreateUserMutation>;
export type CreateUserMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateUserMutation, CreateUserMutationVariables>;
export const ProjetosAvaliadorDocument = gql`
    query projetosAvaliador($idPessoa: String!, $idVinculoPessoa: String!) {
  apresentacoesAvaliador(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
    idApresentacao
    projetos {
      ...UserProjects
    }
  }
}
    ${UserProjectsFragmentDoc}`;
export type ProjetosAvaliadorComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ProjetosAvaliadorQuery, ProjetosAvaliadorQueryVariables>, 'query'> & ({ variables: ProjetosAvaliadorQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const ProjetosAvaliadorComponent = (props: ProjetosAvaliadorComponentProps) => (
      <ApolloReactComponents.Query<ProjetosAvaliadorQuery, ProjetosAvaliadorQueryVariables> query={ProjetosAvaliadorDocument} {...props} />
    );
    

/**
 * __useProjetosAvaliadorQuery__
 *
 * To run a query within a React component, call `useProjetosAvaliadorQuery` and pass it any options that fit your needs.
 * When your component renders, `useProjetosAvaliadorQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useProjetosAvaliadorQuery({
 *   variables: {
 *      idPessoa: // value for 'idPessoa'
 *      idVinculoPessoa: // value for 'idVinculoPessoa'
 *   },
 * });
 */
export function useProjetosAvaliadorQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ProjetosAvaliadorQuery, ProjetosAvaliadorQueryVariables>) {
        return ApolloReactHooks.useQuery<ProjetosAvaliadorQuery, ProjetosAvaliadorQueryVariables>(ProjetosAvaliadorDocument, baseOptions);
      }
export function useProjetosAvaliadorLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ProjetosAvaliadorQuery, ProjetosAvaliadorQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<ProjetosAvaliadorQuery, ProjetosAvaliadorQueryVariables>(ProjetosAvaliadorDocument, baseOptions);
        }
export type ProjetosAvaliadorQueryHookResult = ReturnType<typeof useProjetosAvaliadorQuery>;
export type ProjetosAvaliadorLazyQueryHookResult = ReturnType<typeof useProjetosAvaliadorLazyQuery>;
export type ProjetosAvaliadorQueryResult = ApolloReactCommon.QueryResult<ProjetosAvaliadorQuery, ProjetosAvaliadorQueryVariables>;
export const UsersPageContainerDocument = gql`
    query usersPageContainer {
  pessoas {
    ...User
  }
}
    ${UserFragmentDoc}`;
export type UsersPageContainerComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<UsersPageContainerQuery, UsersPageContainerQueryVariables>, 'query'>;

    export const UsersPageContainerComponent = (props: UsersPageContainerComponentProps) => (
      <ApolloReactComponents.Query<UsersPageContainerQuery, UsersPageContainerQueryVariables> query={UsersPageContainerDocument} {...props} />
    );
    

/**
 * __useUsersPageContainerQuery__
 *
 * To run a query within a React component, call `useUsersPageContainerQuery` and pass it any options that fit your needs.
 * When your component renders, `useUsersPageContainerQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUsersPageContainerQuery({
 *   variables: {
 *   },
 * });
 */
export function useUsersPageContainerQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<UsersPageContainerQuery, UsersPageContainerQueryVariables>) {
        return ApolloReactHooks.useQuery<UsersPageContainerQuery, UsersPageContainerQueryVariables>(UsersPageContainerDocument, baseOptions);
      }
export function useUsersPageContainerLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<UsersPageContainerQuery, UsersPageContainerQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<UsersPageContainerQuery, UsersPageContainerQueryVariables>(UsersPageContainerDocument, baseOptions);
        }
export type UsersPageContainerQueryHookResult = ReturnType<typeof useUsersPageContainerQuery>;
export type UsersPageContainerLazyQueryHookResult = ReturnType<typeof useUsersPageContainerLazyQuery>;
export type UsersPageContainerQueryResult = ApolloReactCommon.QueryResult<UsersPageContainerQuery, UsersPageContainerQueryVariables>;
export const GetReviewersDocument = gql`
    query GetReviewers {
  avaliadores {
    ... on Avaliador {
      ...User
    }
  }
}
    ${UserFragmentDoc}`;
export type GetReviewersComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetReviewersQuery, GetReviewersQueryVariables>, 'query'>;

    export const GetReviewersComponent = (props: GetReviewersComponentProps) => (
      <ApolloReactComponents.Query<GetReviewersQuery, GetReviewersQueryVariables> query={GetReviewersDocument} {...props} />
    );
    

/**
 * __useGetReviewersQuery__
 *
 * To run a query within a React component, call `useGetReviewersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetReviewersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetReviewersQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetReviewersQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetReviewersQuery, GetReviewersQueryVariables>) {
        return ApolloReactHooks.useQuery<GetReviewersQuery, GetReviewersQueryVariables>(GetReviewersDocument, baseOptions);
      }
export function useGetReviewersLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetReviewersQuery, GetReviewersQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetReviewersQuery, GetReviewersQueryVariables>(GetReviewersDocument, baseOptions);
        }
export type GetReviewersQueryHookResult = ReturnType<typeof useGetReviewersQuery>;
export type GetReviewersLazyQueryHookResult = ReturnType<typeof useGetReviewersLazyQuery>;
export type GetReviewersQueryResult = ApolloReactCommon.QueryResult<GetReviewersQuery, GetReviewersQueryVariables>;
export const GetPessoaDocument = gql`
    query getPessoa($idPessoa: String!, $idVinculoPessoa: String!) {
  pessoa(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
    ...User
  }
}
    ${UserFragmentDoc}`;
export type GetPessoaComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<GetPessoaQuery, GetPessoaQueryVariables>, 'query'> & ({ variables: GetPessoaQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const GetPessoaComponent = (props: GetPessoaComponentProps) => (
      <ApolloReactComponents.Query<GetPessoaQuery, GetPessoaQueryVariables> query={GetPessoaDocument} {...props} />
    );
    

/**
 * __useGetPessoaQuery__
 *
 * To run a query within a React component, call `useGetPessoaQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPessoaQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPessoaQuery({
 *   variables: {
 *      idPessoa: // value for 'idPessoa'
 *      idVinculoPessoa: // value for 'idVinculoPessoa'
 *   },
 * });
 */
export function useGetPessoaQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetPessoaQuery, GetPessoaQueryVariables>) {
        return ApolloReactHooks.useQuery<GetPessoaQuery, GetPessoaQueryVariables>(GetPessoaDocument, baseOptions);
      }
export function useGetPessoaLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetPessoaQuery, GetPessoaQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetPessoaQuery, GetPessoaQueryVariables>(GetPessoaDocument, baseOptions);
        }
export type GetPessoaQueryHookResult = ReturnType<typeof useGetPessoaQuery>;
export type GetPessoaLazyQueryHookResult = ReturnType<typeof useGetPessoaLazyQuery>;
export type GetPessoaQueryResult = ApolloReactCommon.QueryResult<GetPessoaQuery, GetPessoaQueryVariables>;
export const AtualizarPessoaDocument = gql`
    mutation atualizarPessoa($input: atualizarPessoaInput!) {
  atualizarPessoa(input: $input) {
    ...User
  }
}
    ${UserFragmentDoc}`;
export type AtualizarPessoaMutationFn = ApolloReactCommon.MutationFunction<AtualizarPessoaMutation, AtualizarPessoaMutationVariables>;
export type AtualizarPessoaComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AtualizarPessoaMutation, AtualizarPessoaMutationVariables>, 'mutation'>;

    export const AtualizarPessoaComponent = (props: AtualizarPessoaComponentProps) => (
      <ApolloReactComponents.Mutation<AtualizarPessoaMutation, AtualizarPessoaMutationVariables> mutation={AtualizarPessoaDocument} {...props} />
    );
    

/**
 * __useAtualizarPessoaMutation__
 *
 * To run a mutation, you first call `useAtualizarPessoaMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAtualizarPessoaMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [atualizarPessoaMutation, { data, loading, error }] = useAtualizarPessoaMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAtualizarPessoaMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AtualizarPessoaMutation, AtualizarPessoaMutationVariables>) {
        return ApolloReactHooks.useMutation<AtualizarPessoaMutation, AtualizarPessoaMutationVariables>(AtualizarPessoaDocument, baseOptions);
      }
export type AtualizarPessoaMutationHookResult = ReturnType<typeof useAtualizarPessoaMutation>;
export type AtualizarPessoaMutationResult = ApolloReactCommon.MutationResult<AtualizarPessoaMutation>;
export type AtualizarPessoaMutationOptions = ApolloReactCommon.BaseMutationOptions<AtualizarPessoaMutation, AtualizarPessoaMutationVariables>;