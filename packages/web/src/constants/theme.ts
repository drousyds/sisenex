const TOP_BOTTOM = 10
const SIDES = 10
export default {
  layout: {
    content: {
      marginTop: TOP_BOTTOM,
      marginBottom: TOP_BOTTOM,
      marginLeft: SIDES,
      marginRight: SIDES,
      padding: 15
    },
    border: '1px solid #e8e8e8'
  }
}
