/**
 * FIELD VALIDATION MESSAGES
 */
export const EMPTY_FIELD_ERROR_MESSAGE = 'O campo precisa ser preenchido.'
export const FIELDS_MESSAGES = {
  IS_NUMERIC: 'Digite um valor numérico.',
  CREATE: {
    EVENT: {
      EMPTY: {
        NAME: 'É necessário inserir o nome do evento.',
        DESCRIPTION: 'É necessário inserir a descrição do evento.',
        LOCATION: 'É necessário inserir o local do evento.',
        THEMATICS: 'É necessário inserir uma área temática.',
        DATE: 'É necessário inserir uma data para o evento.'
      }
    }
  },
  ALLOCATION: {
    ROOMS: {
      EMPTY: 'É necessário inserir a quantidade de salas.'
    }
  }
}

export const FETCH_MESSAGES = {
  ERROR: {
    EVENTS: 'Ocorreu um erro ao acessar os Eventos.',
    EVENT: 'Ocorreu um erro ao acessar o Evento.',
    THEMATICS: 'Ocorreu um erro ao buscar as áreas temáticas.',
    PRESENTATIONS: 'Ocorreu um erro ao buscar as apresentações.',
    REPORTS: 'Ocorreu um erro ao buscar os reports.',
    REQUESTS: 'Ocorreu um erro ao buscar as solicitações LGPD',
    PROJECTS: 'Ocorreu um erro ao buscar os Projetos.',
    USERS: 'Ocorreu um erro ao buscar as Pessoas.',
    QUESTIONS: 'Ocorreu um erro ao buscar os critérios de avaliação.',
    PROJECTEQUAL: 'Não é possível adicionar o projeto. Este projeto já foi adicionado anteriormente.',
  }
}

export const MUTATIONS_MESSAGES = {
  EVENT: {
    CREATE: {
      SUCCESS: 'O Evento foi criado com sucesso.',
      ERROR: 'Ocorreu um erro ao criar o evento.'
    }
  },
  PROJECT: {
    UPDATE: {
      SUCCESS: 'O Projeto foi atualizado com sucesso.',
      ERROR: 'Ocorreu um erro ao atualizar o projeto.'
    },
    REMOVE: {
      SUCCESS: 'O Projeto foi removido da apresentação com sucesso.',
      ERROR: 'Ocorreu um erro ao remover o projeto da apresentação.'
    }
  },
  PRESENTATION: {
    REMOVE: {
      SUCCESS: 'A Apresentação foi excluída com sucesso.',
      ERROR: 'Ocorreu um erro ao exlcuir a apresentação.'
    }
  },
  REPORT: {
    REMOVE: {
      SUCCESS: 'Report foi excluído com sucesso.',
      ERROR: 'Ocorreu um erro ao exlcuir o report.'
    },
    SOLVE: {
      SUCCESS: 'O estado do report foi atualizado com sucesso.',
      ERROR: 'Ocorreu um erro ao atualizar o estado do report'
    }
  },
  LGPD: {
    SOLVE: {
      SUCCESS: 'O estado da solicitação foi atualizado com sucesso.',
      ERROR: 'Ocorreu um erro ao atualizar o estado da soliticação.'
    }
  }
}
/**
 * CREATE  PRESENTATION
 */
export const CREATE_PRESENTATION_SUCCESS_MESSAGE =
  'A Apresentação foi criada com sucesso.'
export const CREATE_PRESENTATION_ERROR_MESSAGE =
  'Ocorreu um erro ao criar a Apresentação.'
/**
 * CREATE  ADD PROJECT
 */
export const CREATE_ADDPROJECT_SUCCESS_MESSAGE =
  'Projeto foi adicionado com sucesso.'
export const CREATE_ADDPROJECT_ERROR_MESSAGE =
  'Projeto foi nao foi adicionado.'

  /**
 * CREATE  CREATE PROJECT
 */
export const CREATE_CREATEPROJECT_SUCCESS_MESSAGE =
'Projeto foi criado com sucesso.'
export const CREATE_CREATEPROJECT_ERROR_MESSAGE =
'Projeto foi nao foi criado.'
/**
 * UPDATE PRESENTATION MESSAGES
 */
export const UPDATE_PRESENTATION_SUCESS_MESSAGE =
  'A Apresentação foi atualizada com sucesso.'
export const UPDATE_PRESENTATION_ERROR_MESSAGE =
  'Ocorreu um erro ao atulizar a Apresentação.'

/**
 * FETCH PRESENTATIONS
 */
export const FETCH_PRESENTATIONS_ERROR_MESSAGE =
  'Ocorreu um erro ao buscar as apresentações. '
/**
 * FETCH PROJECTS
 */
export const FETCH_PROJECTS_ERROR_MESSAGE =
  'Ocorreu um erro ao buscar os projetos. '

/**
 * UPDATE PROJECT
 */
export const UPDATE_PROJECT_SUCCESS_MESSAGE =
  'O projeto foi atualizado com sucesso.'
export const UPDATE_PROJECT_ERROR_MESSAGE =
  'Ocorreu um erro ao atulizar o Projeto.'

/**
 * FETCH USERS
 */
export const FETCH_USERS_ERROR_MESSAGE =
  'Ocorreu um erro ao buscar os usuários.'
export const UPDATE_USERS_ERROR_MESSAGE =
  'Ocorreu um erro ao atualizar o usuário.'

/**
 * NO PROJECTS ON TERTULIA
 */
export const NO_PROJECTS_ON_TERTULIA_MESSAGE =
  'Não existe nenhum projeto cadastrado na tertulia.'

/**
 * NO TERTULIA FOUND
 */
export const NO_TERTULIA_FOUND_MESSAGE = 'Não há nenhuma tertulia cadastrada.'

/**
 * NO PROJECTS FOUND
 */
export const NO_PROJECTS_FOUND_MESSAGE = 'Não há nenhum projeto cadastrado.'

/**
 * NO USERS FOUND
 */
export const NO_USERS_FOUND_MESSAGE = 'Não há nenhuma Pessoa cadastrada.'

/**
 * FETCH REVIEWS
 */

export const FETCH_REVIEWS_ERROR_MESSAGE =
  'Ocorreu um erro ao buscar as avaliações'

/**
 * UPDATE USER
 */
export const UPDATE_USER_SUCCESS_MESSAGE =
  'O usuário foi atualizado com sucesso.'

/**
 * FETCH UNITS
 */

export const FETCH_UNITS_ERROR_MESSAGE =
  'Ocorreu um erro ao buscar as unidades.'

/**
 * FETCH FIELDS
 */
export const FETCH_FIELDS_ERROR_MESSAGE =
  'Ocorreu um erro ao buscar as áreas temáticas.'

/**
 * FETCH QUESTIONS
 */
export const FETCH_QUESTIONS_ERROR_MESSAGE =
  'Ocorreu um erro aos buscar as perguntas.'

  
/**
 *PROJECT ADDED
 */
  export const ADD_PROJECT_SUCCESS_MESSAGE =
  'Projeto adicionado com sucesso.'