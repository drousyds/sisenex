function getOAuthRedirectURI() {
  return process.env.REACT_APP_NODE_ENV === 'local'
    ? 'https%3A%2F%2Fauth.expo.io%2F%40v-in%2Fsisenex'
    : encodeURIComponent(process.env.REACT_APP_BASE_URL + '/login')
}

const OAUTH_URL = `https://sistemas.ufpb.br/auth-server/oauth/authorize?response_type=code&client_id=sisenex&scope=projeto-extensao-read%20personal-read&redirect_uri=${getOAuthRedirectURI()}`

export { OAUTH_URL }
