import {
  InMemoryCache,
  defaultDataIdFromObject,
  IntrospectionFragmentMatcher
} from 'apollo-cache-inmemory'
import { ApolloClient } from 'apollo-client'
import { ApolloLink } from 'apollo-link'
import { onError } from 'apollo-link-error'
import { HttpLink } from 'apollo-link-http'

// import { message } from 'antd'
import {
  Apresentacao,
  Projeto,
  Pessoa,
  AreaTematica,
  Categoria,
  Pergunta,
  Evento,
  Campus
} from '../generated'
import introspectionQueryResultData from '../generated/fragmentTypes.json'
import {
  presentationCacheId,
  projectCacheId,
  userCacheId,
  AreaTematicaCacheId,
  categoryCacheId,
  questionCacheId,
  eventCacheId,
  campusCacheId
} from './cache'

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData
})

const cache = new InMemoryCache({
  dataIdFromObject: object => {
    switch (object.__typename) {
      case 'Apresentacao': {
        return presentationCacheId(object as Apresentacao)
      }
      case 'Projeto': {
        return projectCacheId(object as Projeto)
      }
      case 'Pessoa':
      case 'Ouvinte':
      case 'Administrador':
      case 'Avaliador':
      case 'Monitor':
      case 'Gerente': {
        return userCacheId(object as Pessoa)
      }
      case 'AreaTematica': {
        return AreaTematicaCacheId(object as AreaTematica)
      }
      case 'Categoria': {
        return categoryCacheId(object as Categoria)
      }
      case 'Pergunta': {
        return questionCacheId(object as Pergunta)
      }
      case 'Evento': {
        return eventCacheId(object as Evento)
      }
      case 'Campus': {
        return campusCacheId(object as Campus)
      }
      default:
        return defaultDataIdFromObject(object) // fall back to default handling
    }
  },
  fragmentMatcher
})

export const client = new ApolloClient({
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors) {
        graphQLErrors.map(({ message: gqlMessage, locations, path }) =>
          console.log(
            `[GraphQL error]: Message: ${gqlMessage}, Location: ${JSON.stringify(
              locations,
              null,
              2
            )}, Path: ${path}`
          )
        )
      }
      if (networkError) {
        // message.error('Ocorreu um erro de conexão.')
        console.log(`[Network error]: ${networkError}`)
      }
    }),
    new HttpLink({
      uri: process.env.REACT_APP_BASE_URL + '/graphql'
      // uri: 'http://sisenex.ufpb.br' + '/graphql'
    })
  ]),
  cache,
  connectToDevTools: true
})
