import gql from 'graphql-tag'

export const AreaTematica = gql`
  fragment AreaTematica on AreaTematica {
    idAreaTematica
    nomeAreaTematica
  }
`
