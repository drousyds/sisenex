import gql from 'graphql-tag'
// Fragments
import { AreaTematica } from './fragments'

// Areas Tematicas Query
export const AreasTematicas = gql`
  query areasTematicas {
    areasTematicas {
      ...AreaTematica
    }
  }
  ${AreaTematica}
`
export const GetAreaTematica = gql`
  query GetAreaTematica($idAreaTematica: Int!) {
    areaTematica(idAreaTematica: $idAreaTematica) {
      ...AreaTematica
    }
  }
  ${AreaTematica}
`
