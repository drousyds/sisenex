import gql from 'graphql-tag'

export const getReports = gql`
  query GetReports {
    reports {
      idReport
      conteudoReport
      emailReport
      dataCriacao
      statusReport
      avaliador {
        nomePessoa
        idPessoa
        idVinculoPessoa
      }
      apresentacao {
        salaApresentacao
        idApresentacao
        id2Apresentacao
      }
    }
  }
`
