import gql from 'graphql-tag'

export const atualizaReport = gql`
  mutation atualizarStatusReport($idReport: Int!, $statusReport: String!) {
    atualizarStatusReport(
      input: { idReport: $idReport, statusReport: $statusReport }
    ) {
      idReport
      conteudoReport
    }
  }
`
