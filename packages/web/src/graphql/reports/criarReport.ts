import gql from 'graphql-tag'

export const criarReport = gql`
  mutation criarReport(
    $conteudoReport: String!
    $idPessoa: String!
    $idVinculoPessoa: String!
    $emailReport: String!
  ) {
    criarReport(
      input: {
        conteudoReport: $conteudoReport
        idPessoa: $idPessoa
        idVinculoPessoa: $idVinculoPessoa
        emailReport: $emailReport
      }
    ) {
      idReport
    }
  }
`
