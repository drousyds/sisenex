import gql from 'graphql-tag'

import { User } from '../users/fragments'

export const getAddReviewPageData = gql`
  query getAddReviewPageData(
    $idProjeto: Int!
    $idPessoa: String!
    $idVinculoPessoa: String!
  ) {
    projeto(idProjeto: $idProjeto) {
      idProjeto
      tituloProjeto

      categoria {
        idCategoria
        nomeCategoria
        perguntas {
          idPergunta
          conteudoPergunta

          categoria {
            idCategoria
          }
        }
      }
    }
    pessoa(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
      ...User
    }
  }
  ${User}
`
