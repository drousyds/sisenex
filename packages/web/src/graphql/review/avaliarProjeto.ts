import gql from 'graphql-tag'

export const avaliarProjeto = gql`
  mutation avaliarProjeto($input: avaliarProjetoInput!) {
    avaliarProjeto(input: $input)
  }
`
