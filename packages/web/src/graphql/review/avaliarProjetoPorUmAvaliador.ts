import gql from 'graphql-tag'

export const avaliarProjetoPorUmAvaliador = gql`
  mutation avaliarProjetoPorUmAvaliador(
    $input: avaliarProjetoPorUmAvaliadorInput!
  ) {
    avaliarProjetoPorUmAvaliador(input: $input)
  }
`
