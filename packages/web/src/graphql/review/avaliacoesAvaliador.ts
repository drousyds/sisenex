import gql from 'graphql-tag'

export const avaliacoesAvaliador = gql`
  fragment AvaliacaoPessoa on Avaliacao {
    media
    projeto {
      idProjeto
      tituloProjeto
    }
  }

  query avaliacoesAvaliador($idPessoa: String!, $idVinculoPessoa: String!) {
    pessoa(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
      idPessoa
      idVinculoPessoa
      ... on Gerente {
        avaliacoes {
          ...AvaliacaoPessoa
        }
      }
      ... on Avaliador {
        avaliacoes {
          ...AvaliacaoPessoa
        }
      }
    }
  }
`
