import {
  Apresentacao,
  Projeto,
  Pessoa,
  Avaliador,
  Monitor,
  Gerente,
  AreaTematica,
  Categoria,
  Pergunta,
  Evento,
  Campus
} from '../../generated'

export const presentationCacheId = ({
  idApresentacao,
  id2Apresentacao,
  __typename
}: Apresentacao) => `${__typename}-${idApresentacao}-${id2Apresentacao}`

export const projectCacheId = ({ __typename, idProjeto }: Projeto) =>
  `${__typename}-${idProjeto}`

export const userCacheId = ({
  idPessoa,
  idVinculoPessoa
}: Pessoa | Avaliador | Monitor | Gerente) =>
  `Pessoa-${idPessoa}-${idVinculoPessoa}`

export const AreaTematicaCacheId = ({
  __typename,
  idAreaTematica
}: AreaTematica) => `${__typename}-${idAreaTematica}`

export const categoryCacheId = ({ __typename, idCategoria }: Categoria) =>
  `${__typename}-${idCategoria}`

export const questionCacheId = ({ __typename, idPergunta }: Pergunta) =>
  `${__typename}-${idPergunta}`

export const eventCacheId = ({ __typename, idEvento }: Evento) =>
  `${__typename}-${idEvento}`

export const campusCacheId = ({ __typename, idCampus }: Campus) =>
  `${__typename}-${idCampus}`
