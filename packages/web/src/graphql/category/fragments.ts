import gql from 'graphql-tag'

export const Category = gql`
  fragment Category on Categoria {
    idCategoria
    nomeCategoria
  }
`
export const CategoryRequiredFields = gql`
  fragment CategoryRequiredFields on Categoria {
    idCategoria
    nomeCategoria
  }
`
