import gql from 'graphql-tag'

import { Category } from './fragments'

export const getCategories = gql`
  query getCategories {
    categorias {
      ...Category
    }
  }
  ${Category}
`

export const getCategoriasComPerguntas = gql`
  query GetCategoriasComPerguntas {
    categorias {
      idCategoria
      nomeCategoria
      perguntas {
        idPergunta
        conteudoPergunta
      }
    }
  }
`
