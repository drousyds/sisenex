import gql from "graphql-tag"

export const GetSolicitacoesLGPD = gql`
    query GetSolicitacoesLGPD {
        solicitacoesLGPD {
        idSolicitacao
        tipoSolicitacao
        textoSolicitacao
        nomeSolicitacao
        sobrenomeSolicitacao
        emailPessoa
        statusSolicitacao
        }
    }
  
`