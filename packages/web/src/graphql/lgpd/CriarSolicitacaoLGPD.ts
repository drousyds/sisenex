import gql from "graphql-tag"

export const criarSolicitacaoLGPD = gql`
  mutation criarSolicitacaoLGPD(
    $tipoSolicitacao: Int!
    $textoSolicitacao: String
    $idPessoa: String!
    $idVinculoPessoa: String!
    $nomeSolicitacao: String!
    $sobrenomeSolicitacao: String!
    $emailPessoa: String!
  ) {
    criarSolicitacaoLGPD(
      input: {
        tipoSolicitacao: $tipoSolicitacao
        textoSolicitacao: $textoSolicitacao
        idPessoa: $idPessoa
        idVinculoPessoa: $idVinculoPessoa
        nomeSolicitacao: $nomeSolicitacao
        sobrenomeSolicitacao: $sobrenomeSolicitacao
        emailPessoa: $emailPessoa
      }
    ) {
      idSolicitacao
    }
  }
`