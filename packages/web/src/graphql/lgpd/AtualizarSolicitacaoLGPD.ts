import gql from "graphql-tag"

export const atualizarSolicitacaoLGPD = gql`
  mutation atualizarSolicitacaoLGPD(
    $idSolicitacao: Int!
    $statusSolicitacao: String
  ) {
    atualizarSolicitacaoLGPD(
      input: {
        idSolicitacao: $idSolicitacao
        statusSolicitacao: $statusSolicitacao
      }
    ) {
      idSolicitacao
      statusSolicitacao
    }
  }
`