import gql from 'graphql-tag'

export const Campus = gql`
  fragment Campus on Campus {
    idCampus
    nomeCampus
  }
`
