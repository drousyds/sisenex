import gql from 'graphql-tag'
import { Campus } from './fragments'

// Get Campi
export const GetCampi = gql`
  query GetCampi {
    campi {
      ...Campus
    }
  }
  ${Campus}
`

export const GetCampus = gql`
  query GetCampus($idCampus: Int!) {
    campus(idCampus: $idCampus) {
      ...Campus
    }
  }
  ${Campus}
`
