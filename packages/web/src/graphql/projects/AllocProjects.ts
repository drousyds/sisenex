import gql from 'graphql-tag'

// Fragments
import { Presentation } from '../presentations/fragments'

export const AlocarProjetos = gql`
  mutation AlocatProjetos($input: alocarProjetoInput!) {
    alocarProjetos(input: $input) {
      ...Presentation
    }
  }
  ${Presentation}
`
