import gql from 'graphql-tag'
import { Project } from './fragments'
import { AreaTematica } from '../thematics/fragments'

export const ProjectsPageContainer = gql`
  query projectsPageContainer {
    projetos {
      ...Project
      media
    }
    areasTematicas {
      ...AreaTematica
    }
  }
  ${Project}
  ${AreaTematica}
`
