import gql from 'graphql-tag'

export const MakeProjectReview = gql`
  mutation makeProjectReview($input: avaliarProjetoInput!) {
    avaliarProjeto(input: $input)
  }
`
