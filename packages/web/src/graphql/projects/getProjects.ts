import gql from 'graphql-tag'
import { Project } from './fragments'

export const GetProjects = gql`
  query GetProjects {
    projetos {
      ...Project
      media
    }
  }
  ${Project}
`
