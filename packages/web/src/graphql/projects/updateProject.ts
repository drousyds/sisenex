import gql from 'graphql-tag'

// Fragments
import { Project } from './fragments'

// Update Project Mutation
export const UpdateProject = gql`
  mutation updateProject($input: atualizarProjetoInput!) {
    atualizarProjeto(input: $input) {
      ...Project
    }
  }
  ${Project}
`
