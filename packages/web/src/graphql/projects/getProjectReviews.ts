import gql from 'graphql-tag'

export const GetProjectReviews = gql`
  query getProjectReviews($idProjeto: Int!) {
    projeto(idProjeto: $idProjeto) {
      idProjeto
      tituloProjeto
      descricaoProjeto
      dataInicioProjeto
      dataFimProjeto
      media
      mediasIndividuais {
        mediaProjeto
      }
    }
  }
`
