import gql from 'graphql-tag'

export const getProjectDetails = gql`
  query getProjectDetails($idProjeto: Int!) {
    projeto(idProjeto: $idProjeto) {
      ...Project
      media
      mediasIndividuais {
        mediaProjeto
      }
    }
  }
`
