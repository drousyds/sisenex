import gql from 'graphql-tag'

// Fragments
import { Project } from './fragments'

export const GetProject = gql`
  query getProject($idProjeto: Int!) {
    projeto(idProjeto: $idProjeto) {
      ...Project
    }
  }
  ${Project}
`
