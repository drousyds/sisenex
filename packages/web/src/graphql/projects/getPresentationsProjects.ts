import gql from 'graphql-tag'

// Fragments
import { Project } from './fragments'
import { Presentation } from '../presentations/fragments'

export const GetPresentationProjects = gql`
  query getPresentationProjects($input: apresentacaoInput!) {
    apresentacao(input: $input) {
      ...Presentation
      projetos {
        ...Project
        media
      }
    }
  }
  ${Presentation}
  ${Project}
`
