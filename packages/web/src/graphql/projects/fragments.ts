import gql from 'graphql-tag'

import { CategoryRequiredFields } from '../category/fragments'
import { PresentationRequiredFields } from '../presentations/fragments'
import { AreaTematica } from '../thematics/fragments'

export const ProjectRequiredFields = gql`
  fragment ProjectRequiredFields on Projeto {
    idProjeto
    tituloProjeto
    descricaoProjeto
    dataInicioProjeto
    dataFimProjeto
    anoProjeto
    modalidadeProjeto
    disponibilidadeProjeto
    liberacaoProjeto
    areaTematica {
    ...AreaTematica
    }
    categoria {
      ...CategoryRequiredFields
    }
  }
  ${AreaTematica}
  ${CategoryRequiredFields}
`

export const Project = gql`
  fragment Project on Projeto {
    ...ProjectRequiredFields
    codigoProjeto
    apresentacao {
      ...PresentationRequiredFields
      salaApresentacao
      horaApresentacao
      dataInicioApresentacao
      dataFimApresentacao
      disponibilidadeApresentacao
      modalidadeApresentacao
    }
  }
  ${ProjectRequiredFields}
  ${PresentationRequiredFields}
`
