import gql from 'graphql-tag'

export const getReviewerProjects = gql`
  fragment ProjetoPessoa on Projeto {
    idProjeto
    codigoProjeto
    tituloProjeto
    linkArtefato
    categoria {
      idCategoria
      nomeCategoria
    }

    mediaIndividual(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
      mediaProjeto
    }

    apresentacao {
      idApresentacao
      id2Apresentacao
      codigoApresentacao
      salaApresentacao
      horaApresentacao
      linkApresentacao

      evento {
        idEvento
        nomeEvento
      }
      
      categoria {
        idCategoria
        nomeCategoria
      }

      areaTematica {
        idAreaTematica
        nomeAreaTematica
      }
    }
  }

  query getReviewerProjects($idPessoa: String!, $idVinculoPessoa: String!) {
    pessoa(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
      idPessoa
      idVinculoPessoa
      ... on Gerente {
        projetosParaAvaliar {
          ...ProjetoPessoa
        }
      }
      ... on Avaliador {
        projetosParaAvaliar {
          ...ProjetoPessoa
        }
      }
    }
  }
`
