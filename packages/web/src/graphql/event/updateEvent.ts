import gql from 'graphql-tag'
import { Event } from './fragments'

// Update Event Mutation
export const UpdateEvent = gql`
  mutation updateEvent($input: atualizarEventoInput!) {
    atualizarEvento(input: $input) {
      ...Event
    }
  }
  ${Event}
`
