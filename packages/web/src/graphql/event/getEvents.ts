import gql from 'graphql-tag'

// Get Events
export const GetEvents = gql`
  query GetEvents {
    eventos {
      ...Event
    }
  }
  ${Event}
`
