import gql from 'graphql-tag'
import { Event } from './fragments'

export const GetEvent = gql`
  query GetEvent($idEvento: Int!) {
    evento(idEvento: $idEvento) {
      ...Event
    }
  }
  ${Event}
`
