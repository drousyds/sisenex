import gql from 'graphql-tag'

import { AreaTematica } from '../thematics/fragments'

export const EventRequiredFields = gql`
  fragment EventRequiredFields on Evento {
    idEvento
    nomeEvento
    statusEvento
    campi {
      idCampus
      nomeCampus
    }
    areasTematicas {
      ...AreaTematica
    }
  }
  ${AreaTematica}
`

export const Event = gql`
  fragment Event on Evento {
    ...EventRequiredFields
    descricaoEvento
    dataInicioEvento
    dataFimEvento
  }
  ${EventRequiredFields}
`
