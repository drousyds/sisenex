import gql from 'graphql-tag'
import { Event } from './fragments'

// Create Event Mutation
export const CreateEvent = gql`
  mutation createEvent($input: criarEventoInput!) {
    criarEvento(input: $input) {
      ...Event
    }
  }
  ${Event}
`
