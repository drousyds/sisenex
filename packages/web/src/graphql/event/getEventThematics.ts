import gql from 'graphql-tag'

export const GetEventThematics = gql`
  query GetEventThematics($idEvento: Int!) {
    evento(idEvento: $idEvento) {
      areasTematicas {
        idAreaTematica
        nomeAreaTematica
      }
    }
  }
`
