import gql from 'graphql-tag'

import { Event } from '../event/fragments'
import { PresentationRequiredFields } from '../presentations/fragments'
import { Project } from '../projects/fragments'
import { AreaTematica } from '../thematics/fragments'
import { User } from '../users/fragments'

export const ReportPageContainer = gql`
  query reportPageContainer($idEvento: Int!) {
    evento(idEvento: $idEvento) {
      ...Event
      areasTematicas {
        ...AreaTematica
      }
      apresentacoes {
        ...PresentationRequiredFields
        modalidadeApresentacao
        disponibilidadeApresentacao
        areaTematica {
          ...AreaTematica
        }
        projetos {
          ...Project
          media
        }
      }
    }
    pessoas {
      ...User
    }
  }
  ${Event}
  ${AreaTematica}
  ${PresentationRequiredFields}
  ${Project}
  ${User}
`
