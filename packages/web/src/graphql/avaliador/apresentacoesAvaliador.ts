import gql from 'graphql-tag'

export const apresentacoesAvaliador = gql`
  query apresentacoesAvaliador($idPessoa: String!, $idVinculoPessoa: String!) {
  apresentacoesAvaliador(
    idPessoa: $idPessoa
    idVinculoPessoa: $idVinculoPessoa
  ) {
    idApresentacao
    id2Apresentacao
    codigoApresentacao
    salaApresentacao

    horaApresentacao

    areaTematica {
      nomeAreaTematica
    }

    categoria {
      idCategoria
      nomeCategoria
    }

    projetos {
      idProjeto
      tituloProjeto
      codigoProjeto
      mediaIndividual(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
        mediaProjeto
      }
    }
  }
}
`
