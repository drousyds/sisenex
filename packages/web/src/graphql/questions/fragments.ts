import gql from 'graphql-tag'

export const Question = gql`
  fragment Question on Pergunta {
    idPergunta
    conteudoPergunta
    dataPergunta
  }
`
