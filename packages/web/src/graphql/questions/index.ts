import gql from 'graphql-tag'
import { Question } from './fragments'
import { Category } from '../category/fragments'

export const GetQuestions = gql`
  query getQuestions {
    perguntas {
      ...Question
      categoria {
        ...Category
      }
    }
  }
  ${Question}
  ${Category}
`
