import gql from 'graphql-tag'

export const getSTIToken = gql`
  mutation getSTIToken($code: String!) {
    autenticar(code: $code) {
      access_token
      token_type
      refresh_token
      expires_in
      scope
      foto
      id_usuario
      nome
      jti
    }
  }
`
