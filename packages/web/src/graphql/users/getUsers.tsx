import gql from 'graphql-tag'
import { User } from './fragments'

// Users Page Container Query
export const GetUsers = gql`
  query GetUsers {
    pessoas {
      ...User
    }
  }
  ${User}
`
