import gql from 'graphql-tag'
import { User, UserProjects } from './fragments'

// Users Page Container Query
export const UsersPageContainer = gql`
  query usersPageContainer {
    pessoas {
      ...User
    }
  }
  ${User}
`
export const GetReviewers = gql`
  query GetReviewers {
    avaliadores {
      ... on Avaliador {
        ...User
      }
    }
  }
  ${User}
`

export const GetPessoa = gql`
  query getPessoa($idPessoa: String!, $idVinculoPessoa: String!) {
    pessoa(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
      ...User
    }
  }
`

// Update User Mutation
export const UpdateUser = gql`
  mutation atualizarPessoa($input: atualizarPessoaInput!) {
    atualizarPessoa(input: $input) {
      ...User
    }
  }
  ${User}
`
