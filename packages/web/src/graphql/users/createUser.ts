import gql from 'graphql-tag'

import { User } from './fragments'

export const createUserMutation = gql`
  mutation createUser($input: criarPessoaInput!) {
    criarPessoa(input: $input) {
      ...User
    }
  }
  ${User}
`
