import gql from 'graphql-tag'
import { UserProjects } from './fragments'

export const GetProjectsAvaliador = gql`
  query projetosAvaliador($idPessoa: String!, $idVinculoPessoa: String!) {
    apresentacoesAvaliador(
      idPessoa: $idPessoa
      idVinculoPessoa: $idVinculoPessoa
    ) {
      idApresentacao
      projetos {
        ...UserProjects
      }
    }
  }
  ${UserProjects}
`
