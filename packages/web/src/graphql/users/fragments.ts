import gql from 'graphql-tag'

export const User = gql`
  fragment User on Pessoa {
    idPessoa
    idVinculoPessoa
    matriculaPessoa
    nomePessoa
    emailPessoa
    created_at
    aptidaoPessoa
    gerentePessoa
    avaliadorPessoa
    monitorPessoa
    gerentePessoa
    administradorPessoa
  }
`
export const UserProjects = gql`
  fragment UserProjects on Projeto {
    codigoProjeto
    tituloProjeto
    media
    idProjeto
    mediaIndividual(idPessoa: $idPessoa, idVinculoPessoa: $idVinculoPessoa) {
      mediaProjeto
    }
  }
`
