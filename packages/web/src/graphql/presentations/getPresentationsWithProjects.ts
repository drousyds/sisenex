import gql from 'graphql-tag'

import { Project } from '../projects/fragments'
import { Presentation } from './fragments'

export const GetPresentationsWithProjects = gql`
  query GetPresentationsWithProjects($input: apresentacoesInput) {
    apresentacoes(input: $input) {
      ...Presentation
      projetos {
        ...Project
        media
      }
    }
  }
  ${Presentation}
  ${Project}
`
