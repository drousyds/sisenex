import gql from 'graphql-tag'

// Fragments
import { Presentation, PresentationBasic } from './fragments'
import { User } from '../users/fragments'
import { Project } from '../projects/fragments'

// Presentations Page Container Query
export const PresentationsPageContainer = gql`
  query presentationsPageContainer {
    apresentacoes {
      ...Presentation
    }
  }
  ${Presentation}
`
export const GetPresentations = gql`
  query GetPresentations {
    apresentacoes {
      ...Presentation
    }
  }
  ${Presentation}
`

export const GetEventPresentations = gql`
  query GetEventPresentations($input: apresentacoesInput!) {
    apresentacoes(input: $input) {
      ...Presentation
    }
  }
  ${Presentation}
  ${Project}
`

export const GetEventPresentationsWithProjects = gql`
  query GetEventPresentationsWithProjects($input: apresentacoesInput) {
    apresentacoes(input: $input) {
      ...Presentation
      projetos {
        ...Project
        media
      }
    }
  }
  ${Presentation}
  ${Project}
`

export const GetPresentationReviewers = gql`
  query GetPresentationReviewers($input: apresentacaoInput!) {
    apresentacao(input: $input) {
      ...PresentationBasic
      avaliadores {
        ... on Avaliador {
          ...User
        }
      }
    }
  }
  ${PresentationBasic}
  ${User}
`

export const GetPresentationsOptions = gql`
  query GetPresentationsOptions($input: apresentacoesInput) {
    apresentacoes(input: $input) {
      ...Presentation
      evento {
        idEvento
        nomeEvento
        campi {
          idCampus
        }
        areasTematicas {
          idAreaTematica
          nomeAreaTematica
        }
      }
      projetos {
        idProjeto
        tituloProjeto
        descricaoProjeto
        dataInicioProjeto
        dataFimProjeto
      }
    }
  }
  ${Presentation}
`

export const GetAllPresentationsOptions = gql`
  query GetAllPresentationsOption {
    apresentacoes {
      ...Presentation
      evento {
        idEvento
        nomeEvento
        campi {
          idCampus
        }
        areasTematicas {
          idAreaTematica
          nomeAreaTematica
        }
      }
      projetos {
        idProjeto
        tituloProjeto
        descricaoProjeto
        dataInicioProjeto
        dataFimProjeto
      }
    }
  }
  ${Presentation}
`

export const GeneretaeQRPresentations = gql`
  query GeneretaeQRPresentations(
    $idEvento: Int!
    $modalidadeApresentacao: String
    $idAreaTematica: Int
  ) {
    apresentacoes(
      input: {
        idEvento: $idEvento
        modalidadeApresentacao: $modalidadeApresentacao
        idAreaTematica: $idAreaTematica
      }
    ) {
      idApresentacao
      id2Apresentacao
      salaApresentacao
      codigoApresentacao
      modalidadeApresentacao
      projetos {
        idProjeto
        tituloProjeto
      }
      areaTematica {
        idAreaTematica
        nomeAreaTematica
      }
      evento {
        idEvento
        nomeEvento
      }
    }
  }
`

// Create Presentation Mutation
export const CreatePresentation = gql`
  mutation criarApresentacao($input: criarApresentacaoInput!) {
    criarApresentacao(input: $input) {
      ...Presentation
      modalidadeApresentacao
    }
  }
  ${Presentation}
`
// Update Presentation Mutation
export const UpdatePresentation = gql`
  mutation atualizarApresentacao($input: atualizarApresentacaoInput!) {
    atualizarApresentacao(input: $input) {
      ...Presentation
    }
  }
  ${Presentation}
`

export const ExcluirApresentacao = gql`
  mutation excluirApresentacao($input: excluirApresentacaoInput!) {
    excluirApresentacao(input: $input) {
      ...Presentation
    }
  }
  ${Presentation}
`
