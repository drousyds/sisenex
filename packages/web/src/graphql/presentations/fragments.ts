import gql from 'graphql-tag'

import { CategoryRequiredFields } from '../category/fragments'
import { EventRequiredFields } from '../event/fragments'
import { AreaTematica } from '../thematics/fragments'

// Presentation Fragment
export const PresentationBasic = gql`
  fragment PresentationBasic on Apresentacao {
    idApresentacao
    id2Apresentacao
    salaApresentacao
    horaApresentacao
    dataInicioApresentacao
    dataFimApresentacao
    disponibilidadeApresentacao
    modalidadeApresentacao
    codigoApresentacao
  }
`
// Presentation Fragment
export const PresentationRequiredFields = gql`
  fragment PresentationRequiredFields on Apresentacao {
    idApresentacao
    id2Apresentacao
    codigoApresentacao
    categoria {
      ...CategoryRequiredFields
    }
  }
  ${CategoryRequiredFields}
`

// Presentation Fragment
export const Presentation = gql`
  fragment Presentation on Apresentacao {
    ...PresentationRequiredFields
    salaApresentacao
    horaApresentacao
    dataInicioApresentacao
    dataFimApresentacao
    disponibilidadeApresentacao
    modalidadeApresentacao
    evento {
      ...EventRequiredFields
    }
    areaTematica {
      ...AreaTematica
    }
  }
  ${EventRequiredFields}
  ${PresentationRequiredFields}
  ${AreaTematica}
`

// Project Presentation Fragment
export const ProjectPresentation = gql`
  fragment ProjectPresentation on Apresentacao {
    idApresentacao
    id2Apresentacao
    salaApresentacao
    codigoApresentacao
  }
`

// Presentation Ids Fragment
export const PresentationIds = gql`
  fragment PresentationIds on Apresentacao {
    idApresentacao
    id2Apresentacao
    codigoApresentacao
  }
`
