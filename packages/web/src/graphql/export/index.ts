import gql from 'graphql-tag'

import { Project } from '../projects/fragments'
import { Event } from '../event/fragments'
// Export Page Container Query
export const ExportPageContainer = gql`
  query exportPageContainer($idEvento: Int!) {
    evento(idEvento: $idEvento) {
      ...Event
    }
    projetos(input: { idEvento: $idEvento }) {
      ...Project
      media
      __typename
    }
  }
  ${Event}
  ${Project}
`
