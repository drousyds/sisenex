import * as Sentry from '@sentry/browser'
import { ConfigProvider, notification } from 'antd'
import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
/* eslint-disable-next-line */
import pt_BR from 'antd/es/locale/pt_BR'

import DialogsWrapper from './components/dialogs/wrappers/DialogsWrapper'
import { AuthProvider } from './contexts/AuthContext'
import { PresentationDialogsProvider } from './contexts/PresentationDialogsContext'
import configureStore from './redux/store/configureStore'
import Routes from './Routes'

if (process.env.REACT_APP_NODE_ENV !== 'local') {
  Sentry.init({
    dsn: 'https://6e2a6a70ab354c59b2a0facc1edbf1f6@sentry.io/1423387'
  })
}

const store = configureStore()

notification.config({
  placement: 'bottomLeft',
  bottom: 20,
  duration: 4.5
})

const App = () => {
  return (
    <Provider store={store}>
      {/* eslint-disable-next-line */}
      <ConfigProvider locale={pt_BR}>
        <PresentationDialogsProvider>
          <DialogsWrapper />
          <BrowserRouter>
            <AuthProvider>
              <Routes />
            </AuthProvider>
          </BrowserRouter>
        </PresentationDialogsProvider>
      </ConfigProvider>
    </Provider>
  )
}

export default App
