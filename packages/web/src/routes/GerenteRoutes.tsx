import React from 'react'
import { Switch, Route, useRouteMatch } from 'react-router-dom'

import ProjectsPageContainer from '../pages/gerenciapage/projects/ProjectsPageContainer'
import UsersPageContainer from '../pages/gerenciapage/users/UsersPageContainer'
import SolicitacoesLGPD from '../pages/gerente/lgpd/SolicitacoesLGPD'
import ReportPageContainer from '../pages/gerente/reports'
import { GerenteApresentacoesPage } from '../pages/GerenteApresentacoes'
import HomePageContainer from '../pages/homepage/HomePageContainer'
import InfoPageContainer from '../pages/infopage/InfoPageContainer'
import PoliticaPage from '../pages/lgpd/PoliticaPage'
import SolicitacaoLGPD from '../pages/lgpd/SolicitacaoLGPD'
import AddAvaliacaoGerente from '../pages/review/gerente/AddAvaliacaoGerente'
import SelectAvaliacaoInformationPage from '../pages/review/gerente/SelectAvaliacaoInformationPage'

const GerenteRoutes: React.FC = () => {
  // eslint-disable-next-line
  // @ts-ignore
  const { path } = useRouteMatch()

  return (
    <Switch>
      <Route exact path={`${path}/home`}>
        <HomePageContainer screenMode={'GERENTE'}/>
      </Route>
        
      <Route exact path={`${path}/politica`}>
        <PoliticaPage />
      </Route>
      <Route exact path={`${path}/apresentacoes`}>
        <GerenteApresentacoesPage />
      </Route>
      <Route exact path={`${path}/projetos`}>
        <ProjectsPageContainer />
      </Route>
      <Route exact path={`${path}/pessoas`}>
        <UsersPageContainer />
      </Route>
      <Route exact path={`${path}/reports`}>
        <ReportPageContainer />
      </Route>
      <Route exact path={`${path}/solicitacoeslgpd`}>
        <SolicitacoesLGPD/>
      </Route>

      <Route exact path={`${path}/relatorio`}>
        <InfoPageContainer />
      </Route>
        <Route exact path={`${path}/avaliacao`}>
      <Route/>
     
     
        <SelectAvaliacaoInformationPage />
      </Route>
      <Route exact path={`${path}/avaliacao/:idProjeto/:idPessoa/:idVinculoPessoa`}>
        <AddAvaliacaoGerente />
      </Route>
      <Route exact path={`${path}/solicitacao`}>
        <SolicitacaoLGPD/>
      </Route>
    </Switch>
  )
}

export default GerenteRoutes
