import React from 'react'
import { Redirect, Route, RouteProps } from 'react-router-dom'

import useAuth from '../hooks/useAuth'

const GerenteOnlyRoute: React.FC<RouteProps> = ({ children, ...rest }) => {
  const { authState, isAuthenticated, isGerente } = useAuth()

  return (
    <Route {...rest} render={() => {
      return isAuthenticated() && isGerente() && authState.mode === 'GERENTE' ? (
        <>
          { children }
        </>
      ) : <Redirect to="/" />
    }}
    />
  )
}

export default GerenteOnlyRoute
