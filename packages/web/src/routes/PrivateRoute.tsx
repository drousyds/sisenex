import { Spin } from 'antd'
import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'

import { openNotificationWithIcon } from '../helpers'
import { useLoggedUser } from '../hooks'
import { AppStateType, NotificationType } from '../types'

export interface Props {
  component: React.ReactNode
  path: string
  exact?: boolean
}

const PrivateRoute: React.FC<Props> = ({ component: Component, ...rest }) => {
  const dispatch = useDispatch()
  const currentUser = useSelector(
    (state: AppStateType) => state.auth.currentUser
  )
  const { loading } = useLoggedUser(dispatch)

  return (
    <Route
      {...rest}
      render={props => {
        if (loading) return <Spin spinning={loading} />
        if (
          currentUser &&
          (currentUser.administradorPessoa || currentUser.gerentePessoa)
        ) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
          // @ts-ignore
          return <Component {...props} />
        } else {
          openNotificationWithIcon(
            NotificationType.ERROR,
            'Você não tem acesso.',
            5.5
          )
          return <Redirect to="/" />
        }
      }}
    />
  )
}

export default PrivateRoute
