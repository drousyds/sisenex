import React from 'react'
import { Redirect, Route, RouteProps } from 'react-router-dom'

import useAuth from '../hooks/useAuth'

const AvaliadorOnlyRoute: React.FC<RouteProps> = ({ children, ...rest }) => {
  const { isAuthenticated, isAvaliador } = useAuth()
  return (
    <Route {...rest} render={() => {
      return isAuthenticated() && isAvaliador() ? (
        <>
          { children }
        </>
      ) : <Redirect to="/" />
    }}
    />
  )
}

export default AvaliadorOnlyRoute
