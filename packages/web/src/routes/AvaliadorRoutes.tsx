import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'

import { AdicionarReportPage } from '../pages/AdicionarReport'
import AvaliadorAvaliacoesPage from '../pages/avaliador/avaliacoes'
import AvaliadorProjectsPage from '../pages/avaliadorProjects/AvaliadorProjectsPage'
import HomePageContainer from '../pages/homepage/HomePageContainer'
import AddReviewAvaliador from '../pages/review/Avaliador/AddReviewAvaliador'
import SolicitacaoLGPD from '../pages/lgpd/SolicitacaoLGPD'
import PoliticaPage from '../pages/lgpd/PoliticaPage'
import AvaliadorIdentidadePage from '../pages/avaliador/identidade'

const AvaliadorRoutes: React.FC = () => {
  // eslint-disable-next-line
  // @ts-ignore
  const { path } = useRouteMatch()

  return (
    <Switch>
     
      <Route exact path={`${path}/home`}>
        <HomePageContainer screenMode={'AVALIADOR'}/>
      </Route>
      <Route exact path={`${path}/projetos`}>
        <AvaliadorProjectsPage />
      </Route>
      <Route exact path={`${path}/identidade`}>
        <AvaliadorIdentidadePage />
      </Route>
      <Route exact path={`${path}/reports`}>
        <AdicionarReportPage />
      </Route>
      <Route exact path={`${path}/avaliacoes`}>
        <AvaliadorAvaliacoesPage />
      </Route>
      <Route exact path={`${path}/politica`}>
        <PoliticaPage />
      </Route>
     
      
      <Route exact path={`${path}/avaliacao/:idProjeto`}>
        <AddReviewAvaliador />
      </Route>
      <Route exact path={`${path}/solicitacao`}>
        <SolicitacaoLGPD/>
      </Route>
    </Switch>
  )
}

export default AvaliadorRoutes
