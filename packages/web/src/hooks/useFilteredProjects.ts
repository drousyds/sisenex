import { useMemo } from 'react'
import { Projeto } from '../generated'
import { ProjectsFiltersType } from '.'
import { getProjectStatus } from '../helpers'

export interface UseFilteredProjectsType {
  projects: Projeto[]
  filters: ProjectsFiltersType
}

export default function useFilteredProjects({
  filters: { areasTematicas, text, categoriaProjeto, allocation, status },
  projects
}: UseFilteredProjectsType) {
  const filteredByKeyword = useMemo(
    () =>
      text
        ? projects.filter(({ tituloProjeto, codigoProjeto, apresentacao }) =>
            `${tituloProjeto}-${codigoProjeto ? `-${codigoProjeto}` : ''}-${
              apresentacao ? `${apresentacao.codigoApresentacao}` : ''
            }-${
              apresentacao && apresentacao.salaApresentacao
                ? apresentacao.salaApresentacao
                : ''
            }`
              .toLowerCase()
              .trim()
              .includes(text.toLowerCase().trim())
          )
        : projects,
    [projects, text]
  )

  const filtersByArea = useMemo(
    () =>
      areasTematicas.length > 0
        ? filteredByKeyword.filter(({ areaTematica }) =>
            areasTematicas.includes(areaTematica.idAreaTematica)
          )
        : filteredByKeyword,
    [filteredByKeyword, areasTematicas]
  )

  const filtersByCategoria = useMemo(
    () =>
      categoriaProjeto
        ? filtersByArea.filter(p =>
            p.categoria
              ? categoriaProjeto === p.categoria.idCategoria
              : false
          )
        : filtersByArea,
    [filtersByArea, categoriaProjeto]
  )

  const filteredByAllocation = useMemo(
    () =>
      allocation
        ? filtersByCategoria.filter(p => {
            if (allocation === 'allocated') return p.apresentacao ? true : false
            else if (allocation === 'not-allocated')
              return p.apresentacao ? false : true
            return false
          })
        : filtersByCategoria,
    [filtersByCategoria, allocation]
  )

  const filteredByStatus = useMemo(
    () =>
      status
        ? filteredByAllocation.filter(p => getProjectStatus(p) === status)
        : filteredByAllocation,
    [filteredByAllocation, status]
  )

  return filteredByStatus
}
