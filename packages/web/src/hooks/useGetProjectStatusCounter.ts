import { useMemo } from 'react'
import { Projeto } from '../generated'
import { getProjectStatus } from '../helpers'
import { ProjectStatus } from '../types'

const useGetProjectStatusCounter = (projects: Projeto[]) => {
  return useMemo(() => {
    const counter = {
      evaluated: 0,
      notEvaluated: 0,
      notAttended: 0,
      total: 0
    }

    projects.forEach(p => {
      const status = getProjectStatus(p)
      if (status === ProjectStatus.EVALUATED) counter.evaluated++
      if (status === ProjectStatus.NOT_ATTENDED) counter.notAttended++
      if (status === ProjectStatus.NOT_EVALUATED) counter.notEvaluated++
      counter.total++
    })

    return counter
  }, [projects])
}
export default useGetProjectStatusCounter
