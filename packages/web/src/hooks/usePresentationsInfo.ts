import { useMemo } from 'react'
import { Apresentacao } from '../generated'
import { getPresentationsInfo } from '../helpers'

const usePresentationsInfo = (presentations: Apresentacao[]) => {
  const info = useMemo(() => {
    return getPresentationsInfo(presentations)
  }, [presentations])

  return info
}

export default usePresentationsInfo
