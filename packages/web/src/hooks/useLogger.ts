import { useState } from 'react'

import { Logger } from '../utils/Logger'

export function useLogger (filename: string) {
  const [logger] = useState(new Logger(filename))
  return logger
}
