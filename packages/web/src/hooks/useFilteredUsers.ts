import { useMemo } from 'react'
import { Pessoa } from '../generated'
import { UsersFiltersType } from '.'
import { UserTypes } from '../types'

export interface UseFilteredUsersInput {
  users: Pessoa[]
  filters: UsersFiltersType
}

const useFilteredUsers = ({
  filters: { roles, text },
  users
}: UseFilteredUsersInput) => {
  // console.log(users)
  const filteredByKeyword = useMemo(
    () =>
      text
        ? users.filter(
            ({ nomePessoa, emailPessoa, created_at, matriculaPessoa }) => {
              let s = `${nomePessoa}`
              if (emailPessoa) s += `-${emailPessoa}`
              if (created_at) s += `-${created_at}`
              if (matriculaPessoa) s += `-${matriculaPessoa}`

              return s
                .toLowerCase()
                .trim()
                .includes(text.toLowerCase().trim())
            }
          )
        : users,
    [text, users]
  )

  const filteredByRoles = useMemo(
    () =>
      roles.length > 0
        ? filteredByKeyword.filter(
            ({
              administradorPessoa,
              gerentePessoa,
              monitorPessoa,
              avaliadorPessoa
            }) => {
              if (
                administradorPessoa &&
                roles.includes(UserTypes.ADMINISTRATOR)
              )
                return true
              if (gerentePessoa && roles.includes(UserTypes.MANAGER))
                return true
              if (avaliadorPessoa && roles.includes(UserTypes.REVIEWER))
                return true
              if (monitorPessoa && roles.includes(UserTypes.MONITOR))
                return true
              if (
                !administradorPessoa &&
                !gerentePessoa &&
                !avaliadorPessoa &&
                !monitorPessoa &&
                roles.includes(UserTypes.NO_ROLE)
              )
                return true
              return false
            }
          )
        : filteredByKeyword,
    [filteredByKeyword, roles]
  )

  return filteredByRoles
}

export default useFilteredUsers
