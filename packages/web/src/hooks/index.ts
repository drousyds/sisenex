import { UserTypes } from '../types'
import useFilteredEvents from './useFilteredEvents'
import useFilteredPresentations from './useFilteredPresentations'
import useFilteredProjects from './useFilteredProjects'
import useFilteredReports from './useFilteredReports'
import useFilteredSolicitacoes from './useFilteredSolicitacoes'
import useFilteredUsers from './useFilteredUsers'
import useFilters from './useFilters'
import useGetProjectStatusCounter from './useGetProjectStatusCounter'
import useLoggedUser from './useLoggedUser'
import usePresentationsInfo from './usePresentationsInfo'
import useUsersCounter from './useUsersCounter'

export interface BaseFilter {
  text?: string
}

export interface EventsFilterType extends BaseFilter {
  campiIds: number[]
}

export interface ProjectsFiltersType extends BaseFilter {
  areasTematicas: number[]
  categoriaProjeto?: number
  allocation?: string
  status?: string
}

export interface UsersFiltersType extends BaseFilter {
  roles: UserTypes[]
}

export interface PresentationsFiltersType extends BaseFilter {
  disponibilidadeApresentacao?: number
  idEvento?: number
  modalidadeApresentacao?: string
  idAreaTematica?: number
  idCategoria?: number
  hora_Apresentacao?: number | string  

}

export interface ReportsFilterType extends BaseFilter {
  statusReport?: string
}

export interface SolicitacoesFilterType extends BaseFilter {
  statusSolicitacao?: string
}

export {
  useFilteredEvents,
  useFilteredProjects,
  useUsersCounter,
  useFilters,
  useFilteredUsers,
  useFilteredPresentations,
  useGetProjectStatusCounter,
  usePresentationsInfo,
  useLoggedUser,
  useFilteredReports,
  useFilteredSolicitacoes
}
