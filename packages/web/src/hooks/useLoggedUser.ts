import { useState, useEffect } from 'react'
import { Pessoa } from '../generated'
import { loadUserLocalStorage } from '../services/localStorage.services'
import { loginUserAction } from '../redux/ducks/auth/actions'
import { Dispatch } from 'redux'

interface UseLoggedUser {
  user: Pessoa | undefined
  loading: boolean
}

const useLoggedUser = (dispatch: Dispatch<any>): UseLoggedUser => {
  const [loading, setLoading] = useState(true)
  const [user, setUser] = useState<Pessoa | undefined>(undefined)
  useEffect(() => {
    // console.log("LOADING",loading)
    const user = loadUserLocalStorage()
    if (user) {
      dispatch(loginUserAction(user))
      setUser(user)
      setLoading(false)
    }
    setLoading(false)
  }, [dispatch])
  return { user, loading }
}

export default useLoggedUser
