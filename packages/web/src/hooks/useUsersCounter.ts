import { useMemo } from 'react'
import { Pessoa } from '../generated'
import { getUsersCount } from '../helpers'

function useUsersCounter(users: Pessoa[]) {
  const counter = useMemo(() => {
    return getUsersCount(users)
  }, [users])
  return counter
}

export default useUsersCounter
