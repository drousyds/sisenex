import { useMemo } from 'react'

import { ReportsFilterType } from '.'
import { getReportStatusText } from '../helpers'
import { ReportTableItem } from '../pages/gerente/reports'

export interface UseFilteredReports {
  reports: ReportTableItem[]
  filters: ReportsFilterType
}

const useFilteredReports = ({
  reports,
  filters: { text, statusReport }
}: UseFilteredReports) => {
  const filteredByKeyword = useMemo(
    () =>
      text
        ? reports.filter(({ conteudoReport, emailReport }) => {
            const reportString = `${conteudoReport}${emailReport}`
            return reportString
              .toLowerCase()
              .trim()
              .includes(text.toLowerCase().trim())
          })
        : reports,
    [reports, text]
  )
  const filteredByStatus = useMemo(
    () =>
      statusReport
        ? filteredByKeyword.filter(
            s => statusReport === getReportStatusText(s.statusReport)
          )
        : filteredByKeyword,
    [filteredByKeyword, statusReport]
  )

  return filteredByStatus
}

export default useFilteredReports
