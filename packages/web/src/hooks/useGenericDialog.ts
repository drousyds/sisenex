import { useState, useCallback } from 'react'

export function useGenericDialog (name: string) {
  const [visible, setVisible] = useState(false)

  const open = useCallback(() => {
    console.log(`OPEN ${name}`)
    setVisible(true)
  }, [name])

  const close = useCallback(() => {
    console.log(`CLOSE ${name}`)
    setVisible(false)
  }, [name])

  return {
    visible,
    open,
    close
  }
}
