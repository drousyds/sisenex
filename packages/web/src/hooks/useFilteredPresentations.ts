import { useMemo } from 'react'

import { PresentationsFiltersType } from '.'
import { Apresentacao } from '../generated'

export interface UseFilteredPresentationsInput {
  presentations: Apresentacao[]
  filters: PresentationsFiltersType
}

const useFilteredPresentations = ({
  presentations,
  filters: {
    text,
    disponibilidadeApresentacao,
    idEvento,
    idAreaTematica,
    modalidadeApresentacao: modalidadeFilter,
    idCategoria,
    hora_Apresentacao,
   

  }
}: UseFilteredPresentationsInput) => {
  const filteredByKeyword = useMemo(
    () =>
      text
        ? presentations.filter(
          ({ codigoApresentacao, salaApresentacao, evento, projetos }) => {
            let s = `${codigoApresentacao}`
            if (salaApresentacao) s += `-${salaApresentacao}`
            if (evento) s += `-${evento.nomeEvento}`
            if (projetos) {
              const ps = projetos.reduce((acc, p) => {
                return acc + `-${p.codigoProjeto}-${p.tituloProjeto}`
              }, '')
              s += ps
            }

            return s
              .toLowerCase()
              .trim()
              .includes(text.toLowerCase().trim())
          }
        )
        : presentations,
    [presentations, text]
  )

  const filteredByStatus = useMemo(
    () =>
      disponibilidadeApresentacao !== undefined
        ? filteredByKeyword.filter(
          p => p.disponibilidadeApresentacao === disponibilidadeApresentacao
        )
        : filteredByKeyword,
    [filteredByKeyword, disponibilidadeApresentacao]
  )

  const filteredByEvent = useMemo(
    () =>
      idEvento
        ? filteredByStatus.filter(({ evento }) =>
          evento ? evento.idEvento === idEvento : false
        )
        : filteredByStatus,
    [filteredByStatus, idEvento]
  )

  const filteredByArea = useMemo(
    () =>
      idAreaTematica
        ? filteredByEvent.filter(({ areaTematica }) =>
          areaTematica
            ? areaTematica.idAreaTematica === idAreaTematica
            : false
        )
        : filteredByEvent,
    [filteredByEvent, idAreaTematica]
  )

  const filteredByCategoria = useMemo(() => {
    return idCategoria !== undefined ? filteredByArea.filter((apresentacao) => {
      return apresentacao.categoria.idCategoria === idCategoria
    }) : filteredByArea
  }, [filteredByArea, idCategoria])

  const filteredByMode = useMemo(
    () =>
      modalidadeFilter
        ? filteredByCategoria.filter(({ modalidadeApresentacao }) =>
          modalidadeApresentacao
            ? modalidadeApresentacao === modalidadeFilter
            : false
        )
        : filteredByCategoria,
    [filteredByCategoria, modalidadeFilter]
  )

  const filteredByDate = useMemo(
    () =>
      hora_Apresentacao
        ? filteredByStatus.filter(({ horaApresentacao  }) => {

            const hora_Apresentacao1 = new Date(parseInt(horaApresentacao as string))
            const horaApresentacaoFilter = new Date(hora_Apresentacao)

                 
            if (horaApresentacaoFilter.getDate()+1 === hora_Apresentacao1.getDate() 
              && horaApresentacaoFilter.getMonth() === hora_Apresentacao1.getMonth()  
              && horaApresentacaoFilter.getFullYear() === hora_Apresentacao1.getFullYear() )
                return true
            else 
                return false
          })
        : filteredByStatus,
    [filteredByMode, hora_Apresentacao]
  );

  return filteredByDate;
}

export default useFilteredPresentations
