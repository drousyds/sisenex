import { useMemo } from 'react'

import { SolicitacoesFilterType } from '.'
import {SolicitacaoTableItem} from '../pages/gerente/lgpd/SolicitacoesLGPD'
import { getReportStatusText } from '../helpers'
import { ReportTableItem } from '../pages/gerente/reports'
import getLGPDStatusText from '../helpers/getLGPDStatusText'

export interface UseFilteredSolicitacoes {
  solicitacoesLGPD: SolicitacaoTableItem[]
  filters: SolicitacoesFilterType
}

const useFilteredSolicitacoes = ({
  solicitacoesLGPD,
  filters: { text, statusSolicitacao }
}: UseFilteredSolicitacoes) => {
  const textoSolicitacao = {
    1: "Consulta de dados",
    2: "Solicitação de dados",
    3: "Exclusão de dados",
    4: "Outros"
  }
  const filteredByKeyword = useMemo(
    () =>
      text
        ? solicitacoesLGPD.filter(({ tipoSolicitacao, emailPessoa }) => {
          const solicitacaoString = `${textoSolicitacao[tipoSolicitacao]}${emailPessoa}`
          return solicitacaoString
            .toLowerCase()
            .trim()
            .includes(text.toLowerCase().trim())
        })
        : solicitacoesLGPD,
    [solicitacoesLGPD, text]
  )
  const filteredByStatus = useMemo(
    () =>
      statusSolicitacao
        ? filteredByKeyword.filter(
          r => statusSolicitacao === getLGPDStatusText(r.statusSolicitacao)
        )
        : filteredByKeyword,
    [filteredByKeyword, statusSolicitacao]
  )

  return filteredByStatus
}

export default useFilteredSolicitacoes;
