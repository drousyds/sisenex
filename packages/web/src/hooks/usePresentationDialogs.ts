import { useContext } from 'react'

import PresentationDialogsContext from '../contexts/PresentationDialogsContext'

/**
 * Hooks that wraps AuthContext
 * @returns {{}}
 */
export function usePresentationDialogs () {
  const context = useContext(PresentationDialogsContext)

  if (context === undefined) {
    throw new Error('usePresentationDialogs must be used within a PresentationDialogsContext Provider')
  }

  return context
}
