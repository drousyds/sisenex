import { useMemo } from 'react'
import { Evento } from '../generated'
import { EventsFilterType } from '.'

export interface UseFilteredEventsType {
  events: Evento[]
  filters: EventsFilterType
}

export default function useFilteredEvents({
  events,
  filters: { text, campiIds }
}: UseFilteredEventsType) {
  const filteredByCampus = useMemo(
    () =>
      campiIds.length > 0
        ? events.filter(({ campi }) =>
            campi.map(c => c.idCampus).some(c => campiIds.includes(c))
          )
        : events,
    [events, campiIds]
  )

  const filteredByKeyword = useMemo(
    () =>
      text
        ? filteredByCampus.filter(({ nomeEvento, descricaoEvento }) =>
            `${nomeEvento}-${descricaoEvento || ''}`
              .toLowerCase()
              .trim()
              .includes(text.toLowerCase().trim())
          )
        : filteredByCampus,
    [filteredByCampus, text]
  )

  return filteredByKeyword
}
