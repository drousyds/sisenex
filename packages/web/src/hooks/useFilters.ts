import { useState } from 'react'

function useFilters<T>(initial: T): [T, (e: T) => void] {
  const [state, setState] = useState<T>({ ...initial })

  const setStateHandler = (e: T) => {
    setState({ ...state, ...e })
  }

  return [state, setStateHandler]
}

export default useFilters
