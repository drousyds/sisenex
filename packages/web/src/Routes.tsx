
import { message } from 'antd'
import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import { SelectVinculoPage } from './components/auth'
import NotFoundPage from './components/common/NotFoundPage'
import AppLayoutContainer from './components/layout/AppLayoutContainer'
import useAuth from './hooks/useAuth'
import AuthPageContainer from './pages/authpage/AuthPageContainer'
import LoginErrorPage from './pages/authpage/LoginErrorPage'
import SuccessSignupPage from './pages/authpage/SuccessSignupPage'
import HomePageContainer from './pages/homepage/HomePageContainer'
import LgpdPageContainer from './pages/lgpd/PoliticaPage'
import SolicitacaoLGPD from './pages/lgpd/SolicitacaoLGPD'
import AvaliadorOnlyRoute from './routes/AvaliadorOnlyRoute'
import AvaliadorRoutes from './routes/AvaliadorRoutes'
import GerenteOnlyRoute from './routes/GerenteOnlyRoute'
import GerenteRoutes from './routes/GerenteRoutes'

const AuthenticatedRoutes: React.FC = () => {
  return (
    <AppLayoutContainer>

      <Switch>
        <Route path="/" exact>
          <HomePageContainer />
        </Route>

        <GerenteOnlyRoute path="/gerente">
          <GerenteRoutes />
        </GerenteOnlyRoute>

        <AvaliadorOnlyRoute path="/avaliador">
          <AvaliadorRoutes />
        </AvaliadorOnlyRoute>

        <Route>
          <Redirect to="/" />
        </Route>
      </Switch>
    </AppLayoutContainer>
  )
}

const UnauthenticatedRoutes: React.FC = () => {
  return (
    <AppLayoutContainer>
      <Switch>
        <Route path="/" exact>
          <HomePageContainer />
        </Route>
       
        <Route exact path="/politica">
          <LgpdPageContainer />
        </Route>

        <Route exact path="/login" component={AuthPageContainer} />
        <Route exact path="/login/vinculos" component={SelectVinculoPage} />
        <Route exact path="/login/erro" component={LoginErrorPage} />
        <Route exact path="/cadastro/sucesso" component={SuccessSignupPage} />
        <Route>
          <Redirect to="/" />
        </Route>
      </Switch>
    </AppLayoutContainer>

  )
}

const Routes: React.FC = () => {
  const { authState: { currentUser } } = useAuth()
  return currentUser ? <AuthenticatedRoutes /> : <UnauthenticatedRoutes />
}

export default Routes
