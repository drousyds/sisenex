import { Pessoa } from '../generated'

const cacheKey = '@sisenex/2020/user'
const screenModeKey = '@sisenex/2020/screenMode'

export const saveUserLocalStorage = (user: any) => {
  try {
    localStorage.setItem(cacheKey, JSON.stringify(user))
    return true
  } catch (error) {
    console.log('ERROR HASH: ', error)
    return false
  }
}

export const loadUserLocalStorage = () => {
  try {
    const item = localStorage.getItem(cacheKey)
    if (item) {
      const parsed: Pessoa = JSON.parse(item)
      return parsed
    }
    return false
  } catch (error) {
    return false
  }
}

export const removeUserLocalStorage = () => {
  try {
    localStorage.removeItem(cacheKey)
    return true
  } catch (error) {
    console.log('ERROR HASH: ', error)
    return false
  }
}

export const saveScreenModeLocalStorage = (mode: 'GERENTE' | 'AVALIADOR') => {
  try {
    localStorage.setItem(screenModeKey, mode)
    return true
  } catch (error) {
    console.log('ERROR HASH: ', error)
    return false
  }
}

export const loadScreenModeLocalStorage = () => {
  try {
    const mode = localStorage.getItem(screenModeKey)
    return mode || false
  } catch (error) {
    return false
  }
}

export const removeScreenModeLocalStorage = () => {
  try {
    localStorage.removeItem(screenModeKey)
    return true
  } catch (error) {
    console.log('ERROR HASH: ', error)
    return false
  }
}
