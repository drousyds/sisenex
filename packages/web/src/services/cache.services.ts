import { DataProxy } from 'apollo-cache'
import {
  PresentationFragment as PresentationFragmentType,
  EventFragment as EventoFragmentType,
  GetEventsDocument,
  GetEventsQuery,
  GetEventsQueryVariables,
  ApresentacoesInput,
  GetEventPresentationsQuery,
  GetEventPresentationsQueryVariables,
  GetEventPresentationsDocument
} from '../generated'

type Client = DataProxy

export const writePresentation = (
  client: Client,
  presentation: PresentationFragmentType,
  queryVariables: ApresentacoesInput
) => {
  console.log('VAR: ', queryVariables)
  try {
    const queryData = client.readQuery<
      GetEventPresentationsQuery,
      GetEventPresentationsQueryVariables
    >({
      query: GetEventPresentationsDocument,
      variables: {
        input:
          presentation.modalidadeApresentacao === 'T' &&
          presentation.areaTematica
            ? {
                ...queryVariables,
                idAreaTematica: presentation.areaTematica.idAreaTematica
              }
            : queryVariables
      }
    })
    if (!queryData) {
      return null
    }
    const { apresentacoes } = queryData

    if (!apresentacoes) {
      return null
    }

    // Write presentation only happens on successful mutation, this in this
    // context presentation is garanteed to not be null
    return client.writeQuery<
      GetEventPresentationsQuery,
      GetEventPresentationsQueryVariables
    >({
      query: GetEventPresentationsDocument,
      variables: { input: queryVariables },
      data: { apresentacoes: [presentation, ...apresentacoes] }
    })
  } catch (error) {
    // console.log('Not found in cache')
  }
}

export const removePresentation = (
  client: Client,
  presentation: PresentationFragmentType,
  queryVariables: ApresentacoesInput
) => {
  console.log('VAR: ', queryVariables)
  const queryData = client.readQuery<
    GetEventPresentationsQuery,
    GetEventPresentationsQueryVariables
  >({
    query: GetEventPresentationsDocument,
    variables: { input: queryVariables }
  })
  if (!queryData) {
    return null
  }
  const { apresentacoes } = queryData

  if (!apresentacoes) {
    return null
  }

  // Write presentation only happens on successful mutation, this in this
  // context presentation is garanteed to not be null
  return client.writeQuery<
    GetEventPresentationsQuery,
    GetEventPresentationsQueryVariables
  >({
    query: GetEventPresentationsDocument,
    variables: { input: queryVariables },
    data: {
      apresentacoes: apresentacoes.filter(
        p =>
          !(
            p.idApresentacao === presentation.idApresentacao &&
            p.id2Apresentacao === presentation.id2Apresentacao
          )
      )
    }
  })
}

export const writeEvent = (client: Client, event: EventoFragmentType) => {
  const queryData = client.readQuery<GetEventsQuery, GetEventsQueryVariables>({
    query: GetEventsDocument
  })
  if (!queryData) return null

  const { eventos } = queryData

  if (!eventos) return null

  return client.writeQuery<GetEventsQuery, GetEventsQueryVariables>({
    query: GetEventsDocument,
    data: { eventos: [event, ...eventos] }
  })
}
