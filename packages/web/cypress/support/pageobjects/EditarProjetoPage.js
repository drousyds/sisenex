/// <reference types="Cypress" />

class TesteEditarProjetoPage{

    editar(){
        cy.wait(1000)
        cy.get('.ant-input').type('PJ441-2019')
        cy.get('.ant-input-suffix > .anticon').click();
        cy.wait(1000)
        cy.get(':nth-child(7) > :nth-child(1) > .ant-btn').click()
        cy.wait(2000)
    }
 
    clicaCampo(select){
        cy
        .wait(1000)
        .get(':nth-child('+ select +') > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-select > .ant-select-selector')
        .click()
    }

    altera(select2){
        cy
        .wait(1000)
        .get(':nth-child('+ select2 +') > .ant-select-item-option-content')
        .click()
            
    }
    
    salvar(){
        
        cy.get('.ant-btn-primary').click()
        cy.get('.ant-notification-notice-message').contains('O Projeto foi atualizado com sucesso')
    }
    
    alteraModalidade(){
        cy
        .get(':nth-child(1) > .ant-select-item-option-content')
        .click();
        cy.get('.ant-btn-primary').click();
        cy.get('.ant-notification-notice-message').contains('O Projeto foi atualizado com sucesso')
    }  
       
}
export default TesteEditarProjetoPage;