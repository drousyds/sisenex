/// <reference types='Cypress'/>

const url = Cypress.config('baseUrl');

class AcessarPage{
    acessar(pagina){
        cy.visit(pagina);
    }
}

export default AcessarPage;