/// <reference types='Cypress'/>

const url = Cypress.config('baseUrl');
class AtribuicaoPage {
  
    clicarEditar(){
        cy.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            //contornando Erro da aplicação
            return false
        })
        cy.get('button[type="button"]:eq(1)').click();
         
    }
    mudarAtribuicao(){
        cy.get('input[value = "ADMINISTRATOR"]').check();
        cy.contains('Salvar').click();
    }
    mudarAtribuicaoMonitor(){
        cy.get('input[value = "MONITOR"]').check();
    }
    editadoInsucesso(){
        cy.contains("Um Usuário não pode ser monitor e gerente simultaneamente.");
    }
    
    editadoSucesso(){
        cy.get('.ant-notification-notice').should('contain','O usuário foi atualizado com sucesso.');  
    }


}

export default AtribuicaoPage;