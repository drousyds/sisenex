/// <reference types="Cypress" />

class ReportPage{

    clicaReport(){
        cy.get(':nth-child(4) > .nav-text').should('contain','Reports').click();
    }

    verificacaoReport(){
        cy.get('h3.ant-typography').should('contain','Ajuda');
    }

    email(email){
        cy.get('#emailReport').type(email);
    }

    mensagem(mensagem){
        cy.get('#conteudoReport').type(mensagem);
    }

    enviar(){
        cy.get('.ant-btn').click();
    }

    confirmacaoReport(){
        cy.wait(2000)
        .get('.ant-notification-notice-message')
    }

    emailVazio(){
        cy.get('.ant-form-item-explain > div')
        .wait(2000)
        .should('contain', 'É necessário fornecer um email válido.');
        cy.reload();
    }

    mensagemVazia(){
        cy.get('.ant-form-item-explain > div')
        .wait(2000)
        .should('contain', 'É necessário fornecer uma mensagem.');
    }
    }

export default ReportPage;