/// <reference types="Cypress" />

class VisualizarArtefatoPage{

    clicamodoavaliador(){
         cy.wait(4000)
         .get('strong').click()
         .get('.ant-dropdown-menu > :nth-child(1)')
         .click();
     }
     
     clicatelaprojetos(){
        cy
         .get(':nth-child(2) > .nav-text').click();
     }
     
    clicaexpandeapresentacao(key){
        cy
         .get('[data-row-key='+ key +'] > .ant-table-row-expand-icon-cell > .ant-table-row-expand-icon').click();
    }
     
   
    assistirvideo(){
        cy
         .get('[data-row-key="3088"] > :nth-child(3)')
         .should('contain', 'Assistir Vídeo')
         .click();
         
     }
    
     clicaexpandeconferencia(key2){

        cy.get('[data-row-key='+key2+'] > .ant-table-row-expand-icon-cell > .ant-table-row-expand-icon').click()
     }

     entrarnaconferencia(){
        cy
        .get('a > .ant-btn')
        .should('contain', 'Clique aqui para participar da conferência')
        .click();
     }

}    

export default VisualizarArtefatoPage;