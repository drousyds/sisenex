/// <reference types="Cypress" />

const url = Cypress.config("baseUrl")

class Apresentacao {
    
    verificarTexto(key,texto){
      cy.get('[data-row-key='+ key +']').should('contain',texto)
    }

    selectDropDown(campo,select){
        cy
      .get(':nth-child('+ campo + ') > .ant-select > .ant-select-selector') 
        .wait(2000)
        .click()
      .get(':nth-child('+ select +') > .ant-select-item-option-content')
        .click();
    }

    showDescription(id){
      cy.get('[data-row-key=' + id + '] > :nth-child(2)').should('contain', id)
    } 
    
    emptyDescription(){
      cy.get('.ant-empty-description').should('contain', 'Não há dados')
    } 
    
    visualizacao(texto,posicao){
      cy.get('.ant-table-row > :nth-child('+ posicao +')').should('contain',texto)
    }
  

}

export default Apresentacao;