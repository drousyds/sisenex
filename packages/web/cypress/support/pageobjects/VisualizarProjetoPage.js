/// <reference types="Cypress" />

class TesteVisualizarProjetoPage{

 clicaProjeto(key){
      cy
       .on('uncaught:exception', (err, runnable) => {
       return false
  })

      cy
       .get('[data-row-key='+key+'] > :nth-child(8) > :nth-child(1) > .ant-btn')
       .click()
        
 }
 clicaprojetopesquisado(){
      cy
      .wait(1000)
      .get(':nth-child(8) > :nth-child(1) > .ant-btn').click()
 }

 exibeProjeto(textovisualização){
      cy
      .wait(1000)
      cy.get('.ant-modal-body > :nth-child(1) > :nth-child(1) > .ant-typography').contains(textovisualização)
  }

 }
export default TesteVisualizarProjetoPage;