/// <reference types="Cypress" />

const url = Cypress.config("baseUrl")

class AvaliacaoPage{
    atribuirTodasNotas(nota){
        cy.get('#basic_numerico_1').type(nota);
        cy.get('#basic_numerico_2').type(nota);
        cy.get('#basic_numerico_3').type(nota);
        cy.get('#basic_numerico_4').type(nota);
        cy.get('#basic_numerico_5').type(nota);
        cy.get('#basic_numerico_6').type(nota);
        cy.get('#basic_numerico_7').type(nota);
        cy.get('#basic_numerico_8').type(nota);
    }
    atribuirSeteNotas(nota){
        cy.get('#basic_numerico_1').type(nota);
        cy.get('#basic_numerico_2').type(nota);
        cy.get('#basic_numerico_3').type(nota);
        cy.get('#basic_numerico_4').type(nota);
        cy.get('#basic_numerico_5').type(nota);
        cy.get('#basic_numerico_6').type(nota);
        cy.get('#basic_numerico_7').type(nota);
    }
    enviarNotas(){
        cy.get('.ant-btn-primary').click();
        cy.get('.ant-modal-confirm-btns > .ant-btn-primary').click();
    }
    verificaAvaliacaoFeita(){
        //cy.url().should('contain','avaliador/projetos');
        cy.get('.ant-notification-notice-message').contains('Avaliação feita com sucesso.')
    }
    moduloAvaliador(){
        cy.wait(4000)
        .get('strong').click()
        .get('.ant-dropdown-menu > :nth-child(1)')
        .click();

    }
    telaprojetos(){
        cy.get(':nth-child(2) > .nav-text').click();

    }

    telaAvaliacao(){
        cy.get(':nth-child(3) > .nav-text').click();
    }

    confirmacaoTelaAvaliacao(){
        cy.get('.ant-page-header-heading-title').should('contain','Avaliações');
    }

    mediaProjeto(projeto, media){
        cy.get('[data-row-key='+projeto+'] > :nth-child(2)').should('contain',media);
    }
    
    clicaexpandeapresentacao(key){
        cy.get('[data-row-key='+ key +'] > .ant-table-row-expand-icon-cell > .ant-table-row-expand-icon').click();
    }
    selecionaProjeto(key){
        cy.get('[data-row-key='+ key +'] > :nth-child(4) > .ant-btn').click();
    }
    verificaMediaProjeto(projeto,media){
        cy.get('[data-row-key='+ projeto +'] > :nth-child(3)').should('contain.text',media);
    }
    preencherComentario(comentario){
        cy.get('#basic_textual_9')
        .type(comentario);
    }
    verificaNotaAtribuida(criterio,nota){
        cy.get('#basic_numerico_'+criterio+'').click();
        cy.get('#basic_numerico_'+criterio+'').should('have.value',nota);
    }
    AtribuirUmaNota(criterio,nota){
        cy.get('#basic_numerico_'+ criterio +'').type(nota);
    }
    VerificaTruncamentodeNotas(nota){
        cy.get('#basic_numerico_1').should('have.value',nota);
        cy.get('#basic_numerico_2').should('have.value',nota); 
        cy.get('#basic_numerico_3').should('have.value',nota);
        cy.get('#basic_numerico_4').should('have.value',nota);
        cy.get('#basic_numerico_5').should('have.value',nota);
        cy.get('#basic_numerico_6').should('have.value',nota);
        cy.get('#basic_numerico_7').should('have.value',nota);
        cy.get('#basic_numerico_8').should('have.value',nota);
    }



}
export default AvaliacaoPage;