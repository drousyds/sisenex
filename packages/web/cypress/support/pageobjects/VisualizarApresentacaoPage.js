/// <reference types="Cypress" />

class VisualizarApresentacaoPage{

    clicaApresentacao(key){
    cy.on('uncaught:exception', (err, runnable) => {
     return false
     })

     cy
     .get('[data-row-key='+key+'] > [style="text-align: center;"] > [style="margin: 10px;"] > .ant-btn')
     .click();


    }

    showApresentation(apresentacao){
        cy.get('.ant-modal-body > :nth-child(1) > :nth-child(1)').should('contain', apresentacao)
    }


    

    }
export default VisualizarApresentacaoPage;