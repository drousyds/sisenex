/// <reference types="Cypress" />

class TesteFiltrarProjetoPage {

    clicaCampo(campo){
        cy
        .wait(5000)
        .get(':nth-child('+ campo + ') > :nth-child(1) > .ant-select > .ant-select-selector') 
        .wait(1000)
        .click()    
    }

    seleciona(select){
        cy
        .get(':nth-child('+ select +') > .ant-select-item-option-content')
        .click();         
    }    
    
}
export default TesteFiltrarProjetoPage;