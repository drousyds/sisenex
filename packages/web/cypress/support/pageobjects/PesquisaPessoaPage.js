/// <reference types="Cypress" />

const url = Cypress.config("baseUrl")

class PesquisaPage {
    pesquisa (teste) {
        cy.get('.ant-input').type(teste + '{enter}') 
    }

    verificarCampo(posicao,texto) {
        cy.wait(2000)
        cy.get('.ant-table-row > :nth-child('+posicao+')').should('contain', texto)
    }

}

export default PesquisaPage;