import ApresentacaoPage from '../pageobjects/ApresentacaoPage'
import AcessoPage from '../pageobjects/AcessoPage';

const Apresentacao = new ApresentacaoPage;
const Acesso = new AcessoPage;

/*
    Parâmetro selectDropDown(campo,select):
    
    campo:
    '1' => Categoria
    '2' => Área temática
    '3' => Modalidade
    '4' => Estado
*/

Given ("Sou um gerente e estou na página de apresentacoes", ()=>{
    Acesso.acessar('apresentacoes')
})

When ("Eu clico em categoria e seleciono e-Tertulia", ()=>{
    //select:
    //'1' => e-Tertúlia
    Apresentacao.selectDropDown('1','1')
})

Then ("Checa o primeiro projeto na categoria e-Tertulia", ()=>{
    Apresentacao.showDescription('45332444')
})

When ("Eu clico em categoria e seleciono video", ()=>{
    //select:
    //'2' => video
    Apresentacao.selectDropDown('1','2')
})

Then ("Checa o primeiro projeto na categoria video", ()=>{
    Apresentacao.showDescription('84717244')
})

When ("Eu clico em Área Temática e seleciono COMUNICAÇÃO", ()=>{
    //     select:
    //     '1' => COMUNICAÇÃO
    Apresentacao.selectDropDown('2','1')
})

Then ("Checa o primeiro projeto na Área Temática COMUNICAÇÃO", ()=>{
    Apresentacao.emptyDescription()
})

When ("Eu clico em Área Temática e seleciono CULTURA", ()=>{
    //     select:
    //     '2' => CULTURA
    Apresentacao.selectDropDown('2','2')
})

Then ("Checa o primeiro projeto na Área Temática CULTURA", ()=>{
    Apresentacao.emptyDescription()
})

When ("Eu clico em Área Temática e seleciono DIREITOS HUMANOS E JUSTIÇA", ()=>{
    //     select:
    //     '3' => DIREITOS HUMANOS E JUSTIÇA
    Apresentacao.selectDropDown('2','3')
})

Then ("Checa o primeiro projeto na Área Temática DIREITOS HUMANOS E JUSTIÇA", ()=>{
    Apresentacao.emptyDescription()
})

When ("Eu clico em Área Temática e seleciono EDUCAÇÃO", ()=>{
    //     select:
    //     '4' => EDUCAÇÃO
    Apresentacao.selectDropDown('2','4')
})

Then ("Checa o primeiro projeto na Área Temática EDUCAÇÃO", ()=>{
    Apresentacao.emptyDescription()
})

When ("Eu clico em Área Temática e seleciono MEIO AMBIENTE", ()=>{
    //     select:
    //     '5' => MEIO AMBIENTE
    Apresentacao.selectDropDown('2','5')
})

Then ("Checa o primeiro projeto na Área Temática MEIO AMBIENTE", ()=>{
    Apresentacao.emptyDescription()
})

When ("Eu clico em Área Temática e seleciono SAÚDE", ()=>{
    //     select:
    //     '6' => SAÚDE
    Apresentacao.selectDropDown('2','6')
})

Then ("Checa o primeiro projeto na Área Temática SAÚDE", ()=>{
    Apresentacao.emptyDescription()
})

When ("Eu clico em Área Temática e seleciono TECNOLOGIA E PRODUÇÃO", ()=>{
    //     select:
    //     '7' => TECNOLOGIA E PRODUÇÃO
    Apresentacao.selectDropDown('2','7')
})

Then ("Checa o primeiro projeto na Área Temática TECNOLOGIA E PRODUÇÃO", ()=>{
    Apresentacao.emptyDescription()
})

When ("Eu clico em Área Temática e seleciono TRABALHO", ()=>{
    //     select:
    //     '8' => TRABALHO
    Apresentacao.selectDropDown('2','8')
})

Then ("Checa o primeiro projeto na Área Temática TRABALHO", ()=>{
    Apresentacao.emptyDescription()
})

When ("Eu clico em modalidade e seleciono Tertulia", ()=>{
    //     select:
    //     '1' => Tertúlia
    Apresentacao.selectDropDown('3','1')
})

Then ("Checa o primeiro projeto na modalidade Tertulia", ()=>{
    Apresentacao.verificarTexto('84717244','Vídeo')
})

When ("Eu clico em modalidade e seleciono Performance", ()=>{
    //     select:
    //     '2' => Performance
    Apresentacao.selectDropDown('3','2')
})

Then ("Checa o primeiro projeto na modalidade Performance", ()=>{
    Apresentacao.verificarTexto('74995724','Vídeo')
})

When ("Eu clico em Estado e seleciono EM EXECUÇÃO", ()=>{
    //     select:
    //     '1' => EM EXECUÇÃO
    Apresentacao.selectDropDown('4','1')
})

Then ("Checa o primeiro projeto no Estado EM EXECUÇÃO", ()=>{
    Apresentacao.verificarTexto('84717244','EM EXECUÇÃO')
})

When ("Eu clico em Estado e seleciono PENDENTE", ()=>{
    //     select:
    //     '2' => PENDENTE
    Apresentacao.selectDropDown('4','2')
})

Then ("Checa o primeiro projeto no Estado PENDENTE", ()=>{
    Apresentacao.verificarTexto('86522464','PENDENTE')
})

When ("Eu clico em Estado e seleciono FINALIZADA", ()=>{
    //     select:
    //     '3' => FINALIZADA
    Apresentacao.selectDropDown('4','3')
})

Then ("Checa o primeiro projeto no Estado FINALIZADA", ()=>{
    Apresentacao.emptyDescription()
})









