import AtribuicaoPage from '../pageobjects/AtribuicaoPage';
import PesquisaPage from '../pageobjects/PesquisaPessoaPage';
import AcessoPage from '../pageobjects/AcessoPage';

const Atribuicao = new AtribuicaoPage
const Pesquisa = new PesquisaPage
const Acesso = new AcessoPage;

Given ("Sou um gerente e estou na página de pessoas",()=>{
	Acesso.acessar('pessoas');
})

When("Eu insiro o número de matrícula no campo pesquisar e solicito", () => {
	Pesquisa.pesquisa('30-339-0564');
});

And("o usuário pesquisado aparecer na tabela",()=>{
	Pesquisa.verificarCampo('2','Diena Carbine');
});

And("clico em editar",()=>{
	Atribuicao.clicarEditar();
});

And ("marco a opção de Administrador e clico em Salvar",()=>{
	Atribuicao.mudarAtribuicao();

})
And ("marco a opção de Monitor e clico em Salvar",()=>{
	Atribuicao.mudarAtribuicaoMonitor();

})
Then ("sistema deve informar que um usuário não pode ser monitor e gerente simultaneamente",()=>{
	Atribuicao.editadoInsucesso();
})
Then("o usuário deve ser atualizado com sucesso", () => {
	Atribuicao.editadoSucesso();
});


