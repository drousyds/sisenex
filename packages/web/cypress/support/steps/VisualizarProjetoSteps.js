import TesteVisualizarProjetoPage from '../pageobjects/VisualizarProjetoPage'
import AcessoPage from '../pageobjects/AcessoPage'
import PesquisaPage from '../pageobjects/PesquisaPessoaPage'

const pesquisa = new PesquisaPage;
const teste = new TesteVisualizarProjetoPage;
const acesso = new AcessoPage;

// CEN-01: Visualizar projeto na página inicial
Given("sou um gerente e estou na página de projetos", ()=> {
    acesso.acessar('projetos');
})
When("clico em visualizar um projeto", ()=> {
    teste.clicaProjeto('6403');

})

Then("o projeto deve ser exibido", ()=> {
    teste.exibeProjeto("Dançando no castelo: Forró e samba.")
})
// CEN:02

Given("sou um gerente e estou na página de projetos", ()=> {
    acesso.acessar('projetos');
})
When("insiro o nome do projeto que desejo visualizar e pesquiso", ()=> {
     pesquisa.pesquisa('PJ775-2019')

})

And("clico em visualizar o projeto pesquisado", ()=> {
     teste.clicaprojetopesquisado();
})

Then("o projeto deve ser exibido", ()=> {
    teste.exibeProjeto("Dançando no castelo: Forró e samba.")
})

