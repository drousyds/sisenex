import ApresentacaoPage from '../pageobjects/ApresentacaoPage'
import PesquisaPage from '../pageobjects/PesquisaPessoaPage';
import AcessoPage from '../pageobjects/AcessoPage';

const Apresentacao = new ApresentacaoPage;
const Pesquisa = new PesquisaPage
const Acesso = new AcessoPage;

//Scenario: Gerente pesquisa apresentacao por codigo 
Given ("Sou um gerente e estou na página de apresentacoes", ()=>{
    Acesso.acessar('apresentacoes')
})

When ("Eu insiro o codigo no campo pesquisar e solicito", ()=>{
    Pesquisa.pesquisa('84717244')
})

Then ("O codigo deve ser checado", ()=>{
    Apresentacao.verificarTexto('84717244','84717244')
})
