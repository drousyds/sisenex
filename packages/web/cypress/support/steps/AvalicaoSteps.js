import AvaliacaoPage from '../pageobjects/AvaliacaoPage'
import AcessoPage from '../pageobjects/AcessoPage'

const acesso = new AcessoPage;
const testAvaliar = new AvaliacaoPage;

//CEN-01: Avaliar projeto sem inserir comentário
Given ("que eu estou na tela de avaliação de um projeto",() => {
    cy.visit('http://localhost');
    testAvaliar.moduloAvaliador();
    testAvaliar.telaprojetos();
    testAvaliar.clicaexpandeapresentacao("1-2019");
    testAvaliar.selecionaProjeto(1452);
})
And("insiro nota 8,9 a todos os critérios e deixo o campo de comentário vazio",()=>{
    testAvaliar.atribuirTodasNotas('8,9');

})
And("eu clico em Enviar",()=>{
    testAvaliar.enviarNotas();
});
Then("devo ser direcionado para a tela de projetos e a média do projeto deve ser 8.90",()=>{
    testAvaliar.verificaAvaliacaoFeita();
    testAvaliar.clicaexpandeapresentacao("1-2019");
    testAvaliar.verificaMediaProjeto(1452,'8.90')
})
//CEN-02: Avaliar projeto inseririndo comentário
Given ("que eu estou na tela de avaliação do projeto 2662",() => {
    cy.visit('http://localhost');
    testAvaliar.moduloAvaliador();
    testAvaliar.telaprojetos();
    testAvaliar.clicaexpandeapresentacao("1-2019");
    testAvaliar.selecionaProjeto(2662);
})
And("insiro nota 8,9 a todos os critérios",()=>{
    testAvaliar.atribuirTodasNotas('8,9');

})
And ("preencho o campo comentário com a palavra testando",()=>{
    testAvaliar.preencherComentario('testando');
})
Then("devo ser direcionado para a tela de projetos e a média do projeto deve ser 8.90",()=>{
    testAvaliar.verificaAvaliacaoFeita();
    testAvaliar.clicaexpandeapresentacao("1-2019");
    testAvaliar.verificaMediaProjeto(2662,'8.90')
})

//CEN-03: Inserir mais de duas casas decimais em um critério

when ("eu insiro a nota 9,888888",()=>{
    testAvaliar.AtribuirUmaNota('9,888888');
})
Then ("indo para o próximo critério a nota deve ficar 9,8",()=>{
    testAvaliar.verificaNotaAtribuida(1,'9,8');
})

//CEN-04: Inserir nota 100 a todos os critérios

when ("eu insiro a nota 100 a todos os critérios",() => {
    testAvaliar.atribuirTodasNotas(100);
})
Then("o sistema deve truncar em 10,0 todas as notas",() => {
    testAvaliar.VerificaTruncamentodeNotas('10,0');
})

//CEN-05: Inserir nota 8,785 a todos os critérios

When ("eu insiro a nota 8,785 a todos os critérios",() => {
    testAvaliar.atribuirTodasNotas('8,785');
})
Then("devo ser direcionado para a tela de projetos e a média do projeto deve ser 8.80",()=>{
    testAvaliar.verificaAvaliacaoFeita();
    testAvaliar.clicaexpandeapresentacao("1-2019");
    testAvaliar.verificaMediaProjeto(1452,'8.78')
})
// CEN-06: Realizar avaliação inserindo nota 100 no último critério

When ("eu insiro a nota 8,8 aos sete primeiros critérios",() => {
    testAvaliar.atribuirSeteNotas('8,8');
})
And ("insiro a nota 100 no último critério",() => {
    testAvaliar.AtribuirUmaNota(8,100);
})
Then("devo ser direcionado para a tela de projetos e a média do projeto deve ser 8.90",()=>{
    testAvaliar.verificaAvaliacaoFeita();
    testAvaliar.clicaexpandeapresentacao("1-2019");
    testAvaliar.verificaMediaProjeto(1452,'8.95')
})