import ApresentacaoPage from '../pageobjects/ApresentacaoPage'
import AcessoPage from '../pageobjects/AcessoPage'

const acesso = new AcessoPage;
const Filtro = new ApresentacaoPage;

Given("eu sou um gerente e estou na página de pessoas", ()=>{
    acesso.acessar('pessoas');
})

When("eu clico no campo de atribuição e seleciono a atribuição Administrador", ()=>{
    Filtro.selectDropDown('1','1')
   
})

Then("O usuário que possui a atribuição de Administrador deve aparecer na tela", ()=>{
    Filtro.visualizacao('30-339-0564','1')
})

