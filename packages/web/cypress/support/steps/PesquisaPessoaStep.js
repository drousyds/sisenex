import PesquisaPage from '../pageobjects/PesquisaPessoaPage';
import AcessarPage from '../pageobjects/AcessoPage';

const PesquisaPessoa = new PesquisaPage;
const Acesso = new AcessarPage;

//Scenario: Gerente filtra usuário por número de matrícula
Given ("Sou um gerente e estou na página de pessoas", ()=>{
   Acesso.acessar('pessoas');
})

When ("Eu insiro o número de matrícula no campo pesquisar e solicito", ()=>{
    PesquisaPessoa.pesquisa('30-339-0564');
})

Then ("A matricula do usuario pesquisado deve aparecer na tabela", ()=>{
   PesquisaPessoa.verificarCampo('1','30-339-0564');

})

//Scenario: Gerente filtra usuário por número de telefone

When ("Eu insiro o número de telefone no campo pesquisar e solicito", ()=>{
    PesquisaPessoa.pesquisa('6525001276');
})

Then ("O telefone do usuario pesquisado deve aparecer na tabela", ()=>{
   PesquisaPessoa.verificarCampo('4','6525001276');

})

//Scenario: Gerente filtra usuário por email

When ("Eu insiro o email no campo pesquisar e solicito", ()=>{
    PesquisaPessoa.pesquisa('mbricket2@hc360.com');
})

Then ("O email do usuario pesquisado deve aparecer na tabela", ()=>{
   PesquisaPessoa.verificarCampo('3','mbricket2@hc360.com');

})

//Scenario: Gerente filtra usuário por nome

When ("Eu insiro o nome no campo pesquisar e solicito", ()=>{
    PesquisaPessoa.pesquisa('Vite Northing');
})

Then ("O nome do usuario pesquisado deve aparecer na tabela", ()=>{
   PesquisaPessoa.verificarCampo('2','Vite Northing');

})








