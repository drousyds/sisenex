import TesteEditarProjetoPage from '../pageobjects/EditarProjetoPage'
import AcessoPage from '../pageobjects/AcessoPage'

const acesso = new AcessoPage;
const teste = new TesteEditarProjetoPage;

// CEN-01: Editar modalidade do projeto
Given("eu sou um gerente e estou na página de projetos", ()=>{
    acesso.acessar('projetos');
})

When("eu clico no campo de editar projeto", ()=>{
    
    teste.editar();
})

And("altero a modalidade do projeto para Tértulia", ()=> {
    teste.clicaCampo('2');
     
})

Then("a modalidade do projeto deve ser alterada com sucesso", ()=>{
    teste.alteraModalidade();
    
})

// CEN-02: Adicionar projeto a uma apresentação  

And("clico no campo de Nova Apresentação", ()=> {
    teste.clicaCampo('1');
    
})

And("aloco o projeto a uma apresentação", ()=> {
    teste.altera('6');
})

Then("o projeto deve ser alocado a apresentação selecionada com sucesso", ()=>{
      teste.salvar();
    
})

