import TesteFiltrarProjetoPage from '../pageobjects/FiltrarProjetoPage'
import AcessoPage from '../pageobjects/AcessoPage'

const acesso = new AcessoPage;
const teste = new TesteFiltrarProjetoPage;

// CEN-01: Filtrar projeto área temática
Given("eu sou um gerente e estou na página de projetos", ()=>{
    acesso.acessar('projetos');
})

When("eu clico no campo de área temática", ()=>{
    teste.clicaCampo('1');
   
})
And("escolho a opção Comunicação", ()=> {
    teste.seleciona('1');    
})

Then("devo visualizar todos os projetos da área temática selecionada", ()=>{
    
})

// CEN-02: Filtrar projeto por modalidade
When("eu clico no campo de modalidade", ()=>{
    teste.clicaCampo('2');
})

And("escolho a opção Tértulia", ()=> {
    teste.seleciona('1');
})

Then("devo visualizar todos os projetos com a modalidade selecionada", ()=>{
      
})

// CEN-03: Filtrar projeto por alocação
When("eu clico no campo de alocação", ()=>{
    teste.clicaCampo('3');
})

And("escolho a opção alocado", ()=> {
    teste.seleciona('1');
})

Then("devo visualizar todos os projetos alocados", ()=>{
     
})

// CEN-04: Filtrar projeto por status
When("eu clico no campo de status", ()=>{
    teste.clicaCampo('4');
})

And("escolho a opção apresentado", ()=> {
    teste.seleciona('1');
})

Then("devo visualizar todos os projetos apresentados", ()=>{
      
})


