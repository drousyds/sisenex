import VisualizarApresentacaoPage from '../pageobjects/VisualizarApresentacaoPage'
import AcessoPage from '../pageobjects/AcessoPage'
import TesteVisualizarProjetoPage from '../pageobjects/VisualizarProjetoPage'


const acesso = new AcessoPage;
const visualiza = new VisualizarApresentacaoPage;

Given("sou um gerente e estou na página de apresentação", ()=> {
    acesso.acessar('apresentacoes');
})
When("clico em visualizar uma apresentação", ()=> {
    visualiza.clicaApresentacao('18372984');

})

Then("a apresentação deve ser exibida", ()=> {
    visualiza.showApresentation('18372984')
})