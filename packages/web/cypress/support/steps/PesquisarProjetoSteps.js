import TestePesquisarProjetoPage from '../pageobjects/PesquisarProjetoPage'
import AcessoPage from '../pageobjects/AcessoPage'
import PesquisaPage from '../pageobjects/PesquisaPessoaPage'

const pesquisa = new PesquisaPage;
const acesso = new AcessoPage;
const teste = new TestePesquisarProjetoPage;

// CEN-01: Pesquisar projeto por código
Given("eu sou um gerente e estou na página de projetos", ()=>{
    acesso.acessar('projetos');
})

When("insiro o código do projeto desejado no campo de pesquisa", ()=>{
   pesquisa.pesquisa('PJ727-2019');
})


Then("o projeto pesquisado deve ser exibido", ()=>{
    
    pesquisa.verificarCampo('1', 'PJ727-2019')
})

// CEN-02: Pesquisar projeto por título

When("insiro o título do projeto desejado no campo de pesquisa", ()=>{
    pesquisa.pesquisa('Dançando no castelo: Forró e samba.');
})

Then("o projeto pesquisado pelo título deve ser exibido", ()=>{
   
   pesquisa.verificarCampo('2', 'Dançando no castelo: Forró e samba.')
      
})

//CEN-03: Pesquisar projeto inexistente por código

When("insiro o código do projeto inexistente", ()=>{
     pesquisa.pesquisa('PXXX-976');
})

Then("devo receber a mensagem de: Não há dados", ()=>{
    teste.visualizaProjetoInexistente();
})