import AvaliacaoPage from '../pageobjects/AvaliacaoPage'
import AcessoPage from '../pageobjects/AcessoPage'
import ReportPage from '../pageobjects/ReportPage'

const acesso = new AcessoPage;
const avaliador = new AvaliacaoPage;
const report = new ReportPage;

Given ("que eu estou logado no sistema e sou avaliador",() => {
    acesso.acessar('home')
    avaliador.moduloAvaliador()
})


when("eu clico em “Reports” na barra de menu lateral",() =>{
    report.clicaReport()
})

then ("a tela de Report deve ser exibida",()=>{
    report.verificacaoReport()
})

When ("insiro meu email", ()=>{
    report.email('email@email.com')
})

And("insiro a mensagem de report",()=>{
    report.mensagem('teste')
})

And("clico em 'Enviar'",()=>{
    report.enviar()
})

then("o report deve ser enviado com sucesso",()=>{
    report.confirmacaoReport()
})

when("não informo meu email",()=>{

})

And("insiro a mensagem de report", ()=>{
    report.mensagem('testando')
})

And("clico em 'Enviar'", ()=>{
    report.enviar()
})
then("o sistema deve exibir: 'É necessário fornecer um email válido.'",() =>{
    report.emailVazio()
})
when("informo meu email", ()=>{
    report.email('email@email.com')
})

And("deixo o campo de mensagem vazio", ()=>{

})

And("clico em 'Enviar'",()=>{
    report.enviar()
})

then("o sistema deve exibir: 'É necessário fornecer uma mensagem.'",()=>{
    report.mensagemVazia()
})





