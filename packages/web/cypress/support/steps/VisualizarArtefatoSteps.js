import  VisualizarArtefatoPage from '../pageobjects/VisualizarArtefatoPage'
import AcessoPage from '../pageobjects/AcessoPage'
const teste = new VisualizarArtefatoPage;
const acesso = new AcessoPage;

// CEN-01: Visualizar o artefato vídeo de um projeto
Given("que eu estou na tela de projetos", ()=> {
    acesso.acessar('home');
    teste.clicamodoavaliador();
    teste.clicatelaprojetos();
})
And("estou alocado a uma apresentação do tipo vídeo", ()=> {
        
})
When("eu clico em + de uma apresentação", ()=> {
    teste.clicaexpandeapresentacao("2-2019");

})

And("clico no botão Assistir ao Vídeo", ()=>{
    teste.assistirvideo();
    
})
Then("devo ser direcionado para a página de visualizar o vídeo", ()=> {
    
})

// CEN-02: Participar da conferência de uma e-tertúlia
When("eu clico em + de uma apresentação tipo tertúlia", ()=> {
    teste.clicaexpandeconferencia("1-2019");

})

And("clico no botão Clique aqui para participar da conferência", ()=>{
    teste.entrarnaconferencia();
})
Then("devo ser direcionado para a página do google meets", ()=> {
    
})

 