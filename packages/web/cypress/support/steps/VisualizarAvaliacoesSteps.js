import AvaliacaoPage from '../pageobjects/AvaliacaoPage'
import AcessoPage from '../pageobjects/AcessoPage'

const acesso = new AcessoPage;
const avaliar = new AvaliacaoPage;

Given ("que eu já tenha avaliado ao menos um projeto",() => {
    acesso.acessar('home');
    avaliar.moduloAvaliador();
    avaliar.telaprojetos();
    avaliar.clicaexpandeapresentacao("1-2019");
    avaliar.selecionaProjeto(1452);
    avaliar.atribuirTodasNotas(10);
    avaliar.enviarNotas();
})

when("eu clico em “Avaliações” na barra de menu lateral",() =>{
    avaliar.telaAvaliacao();
})

then ("devo ser direcionado para a página de Avaliações",()=>{
    avaliar.confirmacaoTelaAvaliacao();
})

And ("na página deve conter os projetos já avaliados e suas médias", ()=>{
    avaliar.mediaProjeto("1452",'10');
})