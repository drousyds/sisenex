Feature: FT[03] Avaliar Projetos

    Um avalidor pode avaliar seus projetos pelo módulo web


    Scenario: CEN-01: Avaliar Projeto sem inserir comentário
        Given que eu estou na tela de avaliação de um projeto
        And insiro nota 8,9 a todos os critérios e deixo o campo de comentário vazio 
        When eu clico em Enviar 
        Then devo ser direcionado para a tela de projetos e a média do projeto deve ser 8.90

    Scenario: CEN-02: Avaliar Projeto inserindo comentário
        Given que eu estou na tela de avaliação do projeto 2662
        And insiro nota 8,9 a todos os critérios
        And preencho o campo comentário com a palavra testando 
        When eu clico em Enviar 
        Then devo ser direcionado para a tela de projetos e a média do projeto deve ser 8.90

    Scenario: CEN-03: Inserir mais de duas casas decimais em um critério
        Given que eu estou na tela de avaliação de um projeto
        When eu insiro a nota 9,888888 
        Then indo para o próximo critério a nota deve ficar 9,8

    Scenario: CEN-04: Inserir nota 100 a todos os critérios
        Given que eu estou na tela de avaliação de um projeto
        When eu insiro a nota 100 a todos os critérios
        Then o sistema deve truncar em 10,0 todas as notas 

    Scenario: CEN-05: Inserir nota 8,785 a todos os critérios
        Given que eu estou na tela de avaliação de um projeto
        When eu insiro a nota 8,785 a todos os critérios
        And preencho o campo comentário com a palavra testando
        And eu clico em Enviar 
        Then devo ser direcionado para a tela de projetos e a média do projeto deve ser 8.80

    Scenario: CEN-06: Realizar avaliação inserindo nota 100 no último critério
        Given que eu estou na tela de avaliação de um projeto
        When eu insiro a nota 8,8 aos sete primeiros critérios
        And insiro a nota 100 no último critério
        And eu clico em Enviar 
        Then devo ser direcionado para a tela de projetos e a média do projeto deve ser 8.90
