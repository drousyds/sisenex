   Feature: [FT- 010] Filtrar Apresentações

   Scenario: CEN-01: Gerente filtra apresentacao por categoria e-Tertúlia
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em categoria e seleciono e-Tertulia
   Then Checa o primeiro projeto na categoria e-Tertulia

   Scenario: CEN-02: Gerente filtra apresentacao por categoria video 
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em categoria e seleciono video
   Then Checa o primeiro projeto na categoria video

   Scenario: CEN-03: Gerente filtra apresentacao por Área Temática COMUNICAÇÃO
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em Área Temática e seleciono COMUNICAÇÃO
   Then Checa o primeiro projeto na Área Temática COMUNICAÇÃO 

   Scenario: CEN-04: Gerente filtra apresentacao por Área Temática CULTURA
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em Área Temática e seleciono CULTURA
   Then Checa o primeiro projeto na Área Temática CULTURA 

   Scenario: CEN-05: Gerente filtra apresentacao por Área Temática DIREITOS HUMANOS E JUSTIÇA
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em Área Temática e seleciono DIREITOS HUMANOS E JUSTIÇA
   Then Checa o primeiro projeto na Área Temática DIREITOS HUMANOS E JUSTIÇA

   Scenario: CEN-06: Gerente filtra apresentacao por Área Temática EDUCAÇÃO
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em Área Temática e seleciono EDUCAÇÃO
   Then Checa o primeiro projeto na Área Temática EDUCAÇÃO

   Scenario: CEN-07: Gerente filtra apresentacao por Área Temática MEIO AMBIENTE
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em Área Temática e seleciono MEIO AMBIENTE
   Then Checa o primeiro projeto na Área Temática MEIO AMBIENTE

   Scenario: CEN-08: Gerente filtra apresentacao por Área Temática SAÚDE
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em Área Temática e seleciono SAÚDE
   Then Checa o primeiro projeto na Área Temática SAÚDE

   Scenario: CEN-09: Gerente filtra apresentacao por Área Temática TECNOLOGIA E PRODUÇÃO
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em Área Temática e seleciono TECNOLOGIA E PRODUÇÃO
   Then Checa o primeiro projeto na Área Temática TECNOLOGIA E PRODUÇÃO

   Scenario: CEN-10: Gerente filtra apresentacao por Área Temática TRABALHO
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em Área Temática e seleciono TRABALHO
   Then Checa o primeiro projeto na Área Temática TRABALHO

   Scenario: CEN-11: Gerente filtra apresentacao por Modalidade Tertúlia
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em modalidade e seleciono Tertulia
   Then Checa o primeiro projeto na modalidade Tertulia 

   Scenario: CEN-12: Gerente filtra apresentacao por Modalidade Performance
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em modalidade e seleciono Performance
   Then Checa o primeiro projeto na modalidade Performance

   Scenario: CEN-13: Gerente filtra apresentacao por Estado EM EXECUÇÃO
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em Estado e seleciono EM EXECUÇÃO
   Then Checa o primeiro projeto no Estado EM EXECUÇÃO

   Scenario: CEN-14: Gerente filtra apresentacao por Estado PENDENTE
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em Estado e seleciono PENDENTE
   Then Checa o primeiro projeto no Estado PENDENTE

   Scenario: CEN-15: Gerente filtra apresentacao por Estado FINALIZADA
   Given Sou um gerente e estou na página de apresentacoes
   When Eu clico em Estado e seleciono FINALIZADA
   Then Checa o primeiro projeto no Estado FINALIZADA

   
