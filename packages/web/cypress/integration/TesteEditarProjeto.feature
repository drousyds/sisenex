Feature: [FT-008] Editar Projeto

     
    Scenario: CEN-01: Editar modalidade do projeto
        Given eu sou um gerente e estou na página de projetos
        When eu clico no campo de editar projeto
        And  altero a modalidade do projeto para Tértulia
        Then a modalidade do projeto deve ser alterada com sucesso

    Scenario: CEN-02: Adicionar projeto a uma apresentação  
        Given eu sou um gerente e estou na página de projetos
        When eu clico no campo de editar projeto
        And  clico no campo de Nova Apresentação
        And  aloco o projeto a uma apresentação
        Then o projeto deve ser alocado a apresentação selecionada com sucesso

    
    