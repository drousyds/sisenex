Feature: [FT- 004] Visualizar Avaliações 
         
    Scenario: CEN-01: Visualizar Avaliações realizadas
        Given que eu já tenha avaliado ao menos um projeto 
        When  eu clico em “Avaliações” na barra de menu lateral
        Then  devo ser direcionado para a página de Avaliações
        And   na página deve conter os projetos já avaliados e suas médias