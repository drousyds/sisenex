Feature: [FT-006] Filtrar Projeto
   
    Scenario: CEN-01: Filtrar projeto área temática
        Given eu sou um gerente e estou na página de projetos
        When eu clico no campo de área temática
        And  escolho a opção Comunicação
        Then devo visualizar todos os projetos da área temática selecionada

    Scenario: CEN-02: Filtrar projeto por modalidade
        Given eu sou um gerente e estou na página de projetos
        When eu clico no campo de modalidade
        And  escolho a opção Tértulia
        Then devo visualizar todos os projetos com a modalidade selecionada
    
    Scenario: CEN-03: Filtrar projeto por alocação
        Given eu sou um gerente e estou na página de projetos
        When eu clico no campo de alocação
        And escolho a opção alocado
        Then devo visualizar todos os projetos alocados

     
    Scenario: CEN-04: Filtrar projeto por status
        Given  eu sou um gerente e estou na página de projetos
        When eu clico no campo de status
        And  escolho a opção apresentado
        Then devo visualizar todos os projetos apresentados

    
    
     

    

     