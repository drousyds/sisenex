Feature: FT[014] Mudar atribuiçao da pessoa

    Um gerente pode mudar a atribuição de uma pessoa no sistema


    Scenario: CEN-01: Gerente muda Atribuição com Sucesso
        Given Sou um gerente e estou na página de pessoas
        When Eu insiro o número de matrícula no campo pesquisar e solicito
        And o usuário pesquisado aparecer na tabela
        And clico em editar 
        And marco a opção de Administrador e clico em Salvar
        Then o usuário deve ser atualizado com sucesso

    Scenario: CEN-02: Gerente realiza mudança de atribuição não permitida
        Given Sou um gerente e estou na página de pessoas
        When Eu insiro o número de matrícula no campo pesquisar e solicito
        And o usuário pesquisado aparecer na tabela
        And clico em editar
        And marco a opção de Monitor e clico em Salvar
        Then sistema deve informar que um usuário não pode ser monitor e gerente simultaneamente
        
    
