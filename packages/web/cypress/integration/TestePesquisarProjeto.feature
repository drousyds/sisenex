Feature: [FT-005] Pesquisar Projeto

   Scenario: CEN-01: Pesquisar projeto por código
        Given eu sou um gerente e estou na página de projetos
        When insiro o código do projeto desejado no campo de pesquisa
        Then o projeto pesquisado deve ser exibido
         
    Scenario: CEN-02: Pesquisar projeto por título
        Given eu sou um gerente e estou na página de projetos
        When insiro o título do projeto desejado no campo de pesquisa
        Then o projeto pesquisado pelo título deve ser exibido

    Scenario: CEN-03: Pesquisar projeto inexistente por código
        Given eu sou um gerente e estou na página de projetos
        When insiro o código do projeto inexistente 
        Then devo receber a mensagem de: Não há dados
         

       