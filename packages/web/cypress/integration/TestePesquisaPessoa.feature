Feature: FT[012] Pesquisar Pessoa

    Scenario: CEN-01: Gerente pesquisa usuário por número de matrícula
    Given Sou um gerente e estou na página de pessoas
    When Eu insiro o número de matrícula no campo pesquisar e solicito
    Then A matricula do usuario pesquisado deve aparecer na tabela

    Scenario: CEN-02: Gerente pesquisa usuário por número de telefone
    Given Sou um gerente e estou na página de pessoas
    When Eu insiro o número de telefone no campo pesquisar e solicito
    Then O telefone do usuario pesquisado deve aparecer na tabela

    Scenario: CEN-03: Gerente pesquisa usuário por email
    Given Sou um gerente e estou na página de pessoas
    When Eu insiro o email no campo pesquisar e solicito
    Then O email do usuario pesquisado deve aparecer na tabela

    Scenario: CEN-04: Gerente pesquisa usuário por nome
    Given Sou um gerente e estou na página de pessoas
    When Eu insiro o nome no campo pesquisar e solicito
    Then O nome do usuario pesquisado deve aparecer na tabela


