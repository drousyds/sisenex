Feature: [FT-007] Visualizar Projeto
         
    Scenario: CEN-01: Visualizar projeto na página inicial
        Given sou um gerente e estou na página de projetos
        When clico em visualizar um projeto
        Then o projeto deve ser exibido

    Scenario: CEN-02: Pesquisar e visualizar projeto  
        Given sou um gerente e estou na página de projetos
        When insiro o nome do projeto que desejo visualizar e pesquiso
        And clico em visualizar o projeto pesquisado
        Then o projeto deve ser exibido
