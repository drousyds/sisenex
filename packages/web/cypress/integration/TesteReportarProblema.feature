   Feature: [FT-005]: Reportar problema
   

      Scenario: CEN-01: Criar report

        Given que eu estou logado no sistema e sou avaliador
        When   eu clico em “Reports” na barra de menu lateral
        Then   a tela de Report deve ser exibida
        When   insiro meu email
        And    insiro a mensagem de report
        And    clico em 'Enviar'
        Then   o report deve ser enviado com sucesso

      Scenario: CEN-02: Criar report com o campo de email vazio

        Given que eu estou logado no sistema e sou avaliador
        When  eu clico em “Reports” na barra de menu lateral
        Then  a tela de Report deve ser exibida
        When  não informo meu email
        And   insiro a mensagem de report
        And   clico em 'Enviar'
        Then  o sistema deve exibir: 'É necessário fornecer um email válido.'

      Scenario: CEN-03: Criar report com o campo de mensagem vazio

        Given que eu estou logado no sistema e sou avaliador
        When  eu clico em “Reports” na barra de menu lateral
        Then  a tela de Report deve ser exibida
        When  informo meu email
        And   deixo o campo de mensagem vazio 
        And   clico em 'Enviar'
        Then  o sistema deve exibir: 'É necessário fornecer uma mensagem.'