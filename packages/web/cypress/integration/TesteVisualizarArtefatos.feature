Feature: [FT- 002] Visualizar artefatos dos projetos
         
    Scenario: CEN-01: Visualizar o artefato vídeo de um projeto
        Given que eu estou na tela de projetos
        And estou alocado a uma apresentação do tipo vídeo
        When eu clico em + de uma apresentação
        And clico no botão Assistir ao Vídeo
        Then devo ser direcionado para a página de visualizar o vídeo


    Scenario: CEN-02: Participar da conferência de uma e-tertúlia
        Given que eu estou na tela de projetos
        And estou alocado a uma apresentação do tipo vídeo
        When eu clico em + de uma apresentação tipo tertúlia
        And clico no botão Clique aqui para participar da conferência
        Then devo ser direcionado para a página do google meets
   

