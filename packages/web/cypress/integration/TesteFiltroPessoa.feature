
Feature: [FT- 013] Filtrar Pessoas por atribuição 

    Scenario: CEN-01: Filtrar pessoa com uma Atribuição
    Given Sou um gerente e estou na página de pessoas
    When eu clico no campo de atribuição e seleciono a atribuição Administrador
    Then O usuário que possui a atribuição de Administrador deve aparecer na tela