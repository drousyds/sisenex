const reporter = require('cucumber-html-reporter')

const options = {
  theme: 'bootstrap',
  jsonDir: 'cypress/reports/cucumber-json',
  output: 'cypress/reports/cucumber_report.html',
  reportSuiteAsScenarios: true,
  scenarioTimestamp: true,
  launchReport: true,
  metadata: {
      "App Name":"Sisenex",
      "Test Environment": "DEVEL",
      "Browser": "Electron",
      "Platform": "Ubuntu 18.4",
      "Executed": "Local"
  }
};

reporter.generate(options)