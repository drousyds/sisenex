-- MySQL Script generated by MySQL Workbench
<<<<<<< Updated upstream
-- Dom 09 Set 2018 19:26:02 -03
=======
-- Tue Sep  4 13:10:51 2018
>>>>>>> Stashed changes
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema Sisenex
-- -----------------------------------------------------
-- Banco de Dados da Edição 2018 do Sisenex

-- -----------------------------------------------------
-- Schema Sisenex
--
-- Banco de Dados da Edição 2018 do Sisenex
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Sisenex` DEFAULT CHARACTER SET utf8 ;
USE `Sisenex` ;

-- -----------------------------------------------------
-- Table `Sisenex`.`AreasTematicas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sisenex`.`AreasTematicas` (
  `idAreaTematica` INT NOT NULL,
  `nomeAreaTematica` VARCHAR(56) NOT NULL,
  PRIMARY KEY (`idAreaTematica`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sisenex`.`Unidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sisenex`.`Unidades` (
  `idUnidade` INT NOT NULL,
  `codigoUnidade` VARCHAR(45) NOT NULL,
  `nomeUnidade` VARCHAR(256) NOT NULL,
  `siglaUnidade` VARCHAR(45) NOT NULL,
  `hierarquiaUnidade` VARCHAR(45) NOT NULL,
  `unidadeGestora` INT NOT NULL,
  `tipoUnidade` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idUnidade`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sisenex`.`Tertulias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sisenex`.`Tertulias` (
  `idTertulia` INT NOT NULL AUTO_INCREMENT,
  `id2Tertulia` CHAR(5) NOT NULL,
  `salaTertulia` VARCHAR(32) NOT NULL,
  `horaTertulia` DATETIME NULL,
  `disponibilidadeTertulia` TINYINT NULL,
  `datainicioTertulia` DATETIME NULL,
  `datafimTertulia` DATETIME NULL,
  PRIMARY KEY (`idTertulia`, `id2Tertulia`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sisenex`.`Projetos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sisenex`.`Projetos` (
  `idProjeto` INT NOT NULL,
  `tituloProjeto` VARCHAR(256) NOT NULL,
  `descricaoProjeto` VARCHAR(32) NOT NULL,
  `dataInicioProjeto` DATE NOT NULL,
  `dataFimProjeto` DATE NOT NULL,
  `anoProjeto` DATE NOT NULL,
  `idAreaTematica` INT NOT NULL,
  `idUnidade` INT NOT NULL,
  `idTertulia` INT NULL,
  `id2Tertulia` CHAR(5) NULL,
  PRIMARY KEY (`idProjeto`, `idAreaTematica`, `idUnidade`),
  UNIQUE INDEX `idProjetos_UNIQUE` (`idProjeto` ASC),
  INDEX `fk_Projetos_AreaTematica_idx` (`idAreaTematica` ASC),
<<<<<<< Updated upstream
  INDEX `fk_Projetos_Unidade1_idx` (`idUnidade` ASC),
=======
  INDEX `fk_Projetos_Unidade1_idx` (`idUnidade` ASC) VISIBLE,
>>>>>>> Stashed changes
  INDEX `fk_Projetos_Tertulias1_idx` (`idTertulia` ASC, `id2Tertulia` ASC),
  CONSTRAINT `fk_Projetos_AreaTematica`
    FOREIGN KEY (`idAreaTematica`)
    REFERENCES `Sisenex`.`AreasTematicas` (`idAreaTematica`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Projetos_Unidade1`
    FOREIGN KEY (`idUnidade`)
    REFERENCES `Sisenex`.`Unidades` (`idUnidade`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Projetos_Tertulias1`
    FOREIGN KEY (`idTertulia` , `id2Tertulia`)
    REFERENCES `Sisenex`.`Tertulias` (`idTertulia` , `id2Tertulia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sisenex`.`Pessoas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sisenex`.`Pessoas` (
  `idPessoa` VARCHAR(16) NOT NULL,
  `idVinculoPessoa` VARCHAR(16) NOT NULL,
  `matriculaPessoa` VARCHAR(16) NULL,
  `nomePessoa` VARCHAR(256) NULL,
  `nomeSocialPessoa` VARCHAR(256) NOT NULL,
  `lotacaoPessoa` VARCHAR(256) NULL,
  `emailPessoa` VARCHAR(45) NULL,
  `telefonePessoa` VARCHAR(32) NULL,
  `aptidaoPessoa` TINYINT NOT NULL,
  `avaliadorPessoa` TINYINT NOT NULL,
  `monitorPessoa` TINYINT NOT NULL,
  `gerentePessoa` TINYINT NOT NULL,
  PRIMARY KEY (`idPessoa`, `idVinculoPessoa`),
<<<<<<< Updated upstream
=======
  UNIQUE INDEX `cpfUsuario_UNIQUE` (`cpfPessoa` ASC),
>>>>>>> Stashed changes
  UNIQUE INDEX `matriculaPessoa_UNIQUE` (`matriculaPessoa` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sisenex`.`Membros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sisenex`.`Membros` (
  `idMembro` INT NOT NULL AUTO_INCREMENT,
  `idPessoa` VARCHAR(16) NOT NULL,
  `idVinculoPessoa` VARCHAR(16) NOT NULL,
  `IdProjeto` INT NOT NULL,
  `categoriaMembro` VARCHAR(45) NULL,
  `funcaoMembro` VARCHAR(45) NULL,
  PRIMARY KEY (`idMembro`),
  INDEX `fk_Membros_Projetos1_idx` (`IdProjeto` ASC),
  INDEX `fk_Membros_Pessoas1_idx` (`idPessoa` ASC, `idVinculoPessoa` ASC),
  CONSTRAINT `fk_Membros_Projetos1`
    FOREIGN KEY (`IdProjeto`)
    REFERENCES `Sisenex`.`Projetos` (`idProjeto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Membros_Pessoas1`
    FOREIGN KEY (`idPessoa` , `idVinculoPessoa`)
    REFERENCES `Sisenex`.`Pessoas` (`idPessoa` , `idVinculoPessoa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sisenex`.`MembrosExtensao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sisenex`.`MembrosExtensao` (
  `idMembroExtensao` INT NOT NULL AUTO_INCREMENT,
  `idPessoa` VARCHAR(16) NULL,
  `idVinculoPessoa` VARCHAR(16) NULL,
  `idProjeto` INT NULL,
  `situacaoMembroExtensao` VARCHAR(12) NOT NULL,
  `vinculoMembroExtensao` VARCHAR(45) NULL,
  PRIMARY KEY (`idMembroExtensao`),
  INDEX `fk_MembrosExtensao_Projetos1_idx` (`idProjeto` ASC),
  INDEX `fk_MembrosExtensao_Pessoas1_idx` (`idPessoa` ASC, `idVinculoPessoa` ASC),
  CONSTRAINT `fk_MembrosExtensao_Projetos1`
    FOREIGN KEY (`idProjeto`)
    REFERENCES `Sisenex`.`Projetos` (`idProjeto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_MembrosExtensao_Pessoas1`
    FOREIGN KEY (`idPessoa` , `idVinculoPessoa`)
    REFERENCES `Sisenex`.`Pessoas` (`idPessoa` , `idVinculoPessoa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sisenex`.`Categorias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sisenex`.`Categorias` (
  `idCategoria` INT NOT NULL,
  `nomeCategoria` VARCHAR(45) NULL,
  PRIMARY KEY (`idCategoria`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sisenex`.`Perguntas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sisenex`.`Perguntas` (
  `idPergunta` INT NOT NULL AUTO_INCREMENT,
  `perguntaPergunta` VARCHAR(45) NOT NULL,
  `idCategoria` INT NOT NULL,
  `dataPergunta` DATE NULL,
  PRIMARY KEY (`idPergunta`, `idCategoria`),
  INDEX `fk_Perguntas_Categorias1_idx` (`idCategoria` ASC),
  CONSTRAINT `fk_Perguntas_Categorias1`
    FOREIGN KEY (`idCategoria`)
    REFERENCES `Sisenex`.`Categorias` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sisenex`.`Avaliacoes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sisenex`.`Avaliacoes` (
  `idAvaliacao` INT NOT NULL AUTO_INCREMENT,
  `idPergunta` INT NOT NULL,
  `Projetos_idProjeto` INT NOT NULL,
  `idUsuario` VARCHAR(32) NOT NULL,
  `notaAvaliacao` VARCHAR(45) NULL,
  PRIMARY KEY (`idAvaliacao`, `idPergunta`, `Projetos_idProjeto`, `idUsuario`),
  INDEX `fk_Avaliacoes_Perguntas1_idx` (`idPergunta` ASC),
  INDEX `fk_Avaliacoes_Projetos1_idx` (`Projetos_idProjeto` ASC),
  INDEX `fk_Avaliacoes_Usuarios1_idx` (`idUsuario` ASC),
  CONSTRAINT `fk_Avaliacoes_Perguntas1`
    FOREIGN KEY (`idPergunta`)
    REFERENCES `Sisenex`.`Perguntas` (`idPergunta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Avaliacoes_Projetos1`
    FOREIGN KEY (`Projetos_idProjeto`)
    REFERENCES `Sisenex`.`Projetos` (`idProjeto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Avaliacoes_Usuarios1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `Sisenex`.`Pessoas` (`idPessoa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
