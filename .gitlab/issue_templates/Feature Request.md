<h1>Título da funcionalidade principal</h1>

<h2>Principais objetivos</h2>

- [ ] Implementar XPTO
- [ ] Otimizar XYZ


<h2>Principais atividades do Pull Request</h2>

- [ ] Testes unitários implementados
- [ ] Referência da Issue a ser resolvida após aceite: #123
- [ ] Houve incremento da documentação do projeto
- [ ] Gerenciador de dependências sofreu alterações

<h2>Critério de aceite</h2>
Descrever aqui o critério de aceite do pull request. Caso exista, é importante descrever o impacto na regra de negócio do projeto. 

<h2>Como revisar</h2>
Explicar como validar corretamente o funcionamento da nova funcionalidade. Descrever o passo a passo para testar sua funcionalidade.

<h2>Avalição de nível de criticidade da funcionalidade</h2>
Há impacto direto no funcionamento da aplicação SisEnex? Se a sua funcionalidade não funcionar como esperado, o projeto ficará indisponível ou inviável de ser utilizado?

- [ ] Baixo
- [ ] Médio
- [ ] Alto
