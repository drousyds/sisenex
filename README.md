[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

- [Introdução](#introdução)
  - [Docker](#docker)
    - [Instalação](#instalação)
      - [Com https://get.docker.com](#com-httpsgetdockercom)
      - [Manualmente](#manualmente)
      - [Pós-instalação](#pós-instalação)
  - [Docker-compose](#docker-compose)
  - [Docker no windows](#docker-no-windows)
- [Ferramentas de desenvolvimento](#ferramentas-de-desenvolvimento)
  - [Extensões](#extensões)
  - [React - Qualidade de vida](#react---qualidade-de-vida)
  - [Markdown - Escrita de documentação](#markdown---escrita-de-documentação)
- [Iniciando pacotes](#iniciando-pacotes)
  - [Sem docker](#sem-docker)
  - [Com Docker](#com-docker)
- [Contribuindo](#contribuindo)
  - [Branchs e Ambientes](#branchs-e-ambientes)
    - [Devel](#devel)
    - [Staging](#staging)
    - [Master](#master)
    - [WIP - Work in progress](#wip---work-in-progress)
    - [Issue branchs](#issue-branchs)
    - [Executando localmente](#executando-localmente)
  - [Commits](#commits)
  - [Fluxos de trabalho](#fluxos-de-trabalho)
    - [Novo feature](#novo-feature)
      - [Branch local](#branch-local)
      - [Branch remota](#branch-remota)
    - [Corrigindo bugs / Resolvendo issues](#corrigindo-bugs--resolvendo-issues)
    - [Mudancas longas](#mudancas-longas)
    - [Deployment](#deployment)
- [DB: Manutenção](#db-manutenção)
  - [Backup e Restore](#backup-e-restore)
- [FAQ](#faq)
  - [Que operações posso realizar com docker?](#que-operações-posso-realizar-com-docker)
  - [Como semear o banco?](#como-semear-o-banco)
  - [Como iniciar um serviço?](#como-iniciar-um-serviço)
  - [Como iniciar todos os serviços?](#como-iniciar-todos-os-serviços)
  - [Como matar um serviço?](#como-matar-um-serviço)
  - [Como reiniciar um serviço?](#como-reiniciar-um-serviço)
  - [Quando é preciso recompilar imagens?](#quando-é-preciso-recompilar-imagens)
  - [Como executar um comando dentro de um container?](#como-executar-um-comando-dentro-de-um-container)
  - [Como listar containers?](#como-listar-containers)
  - [Como adicionar/configurar variaveis no **ambiente**?](#como-adicionarconfigurar-variaveis-no-ambiente)
  - [Como um container se comunica com outro?](#como-um-container-se-comunica-com-outro)
  - [Como acessar o banco que vive na rede virtual?](#como-acessar-o-banco-que-vive-na-rede-virtual)
    - [Com Docker](#com-docker-1)
    - [Expondo portas do container ao pc](#expondo-portas-do-container-ao-pc)
  - [Como remover completamente dados do banco?](#como-remover-completamente-dados-do-banco)
- [Troubleshooting](#troubleshooting)
  - [Seeds não funcionam](#seeds-não-funcionam)
  - [Bind error com o proxy ao iniciar o projeto](#bind-error-com-o-proxy-ao-iniciar-o-projeto)

## Introdução

Esse repositório é composto de pacotes - projetos discretos e de baixa interdependencia nos quais a funcionalidade do sistema como um todo depende.

Todos pacotes devem estar localizados na pasta `packages/<package-name>`

Cada pacote tem sua documentação individual presente em sua raiz - `packages/<package-name>/README.md`.

##### Pré Instalação

Não esqueça de configurar o Git. Só assim você conseguirá rodar o programa e fazer seus commits.

```bash
$ git config --global user.name "Usuario"
$ git config --global user.email usuario@exemplo.br
```

###### Node

Instale o Node Version Manager em sua [última versão](https://nodejs.org/en/download/package-manager). Você precisará dele pois as versões Web e Mobile utilizam versões diferentes do Node.

```bash
$ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.0/install.sh | bash
```

Instale a útima versão 

```bash
$ nvm install 22
```

Verifique se estão instalados e as versões do Node e NPM no seu ambiente.

```bash
$ node -v # a última versão foi v22.9.9
$ npm -v # a última versão foi 10.8.3
$ nvm -v # a última versão foi 0.39.1
```

Com isso, você já pode logar em sua conta de Developer no Gitlab e clonar seu repositório no diretório que desejar.

```bash
$ git clone https://gitlab.com/drousyds/sisenex.git
```

###### Yarn

Após ter o npm instalado. Instale ou atualize o gerenciador de pacotes Yarn.

```bash
$ npm install --global yarn # ou
$ sudo npm install -g yarn
```

#### Docker

O projeto e seus pacotes utilizam do [Docker](https://www.docker.com/) como base para execução de todos os processos, logo sua instalação é essencial. Existem duas principais formas de instalar o Docker:

##### Instalação

###### Com https://get.docker.com

O script [get.docker](https://get.docker.com) instala docker e configura seu estado no apt-get automaticamente. Para usar esse script faça:

```bash
$ curl https://get.docker.com | bash
```

É importante notar que caso a instalação não tenha sido feita com o get.docker, upgrade de versões não devem ser feitas por ele.

###### Manualmente

Para instalar o docker manualmente siga os passos da [pagina oficial de instalação](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

###### Pós-instalação

Apos instalar docker inclua seu usuário ao grupo `docker` para que os comandos do docker não precisem usar sudo

```bash
$ sudo groupadd docker
```

```bash
$ sudo usermod -aG docker $USER
```

Para que essa mudança de permissão entre em efeito, saia de sua conta e entre novamente com o seguinte comando

```bash
$ newgrp docker
```

Teste sua instalação:

```bash
# Se o comando a seguir não funcionar, reinicie o seu PC e tente novamente. Se mesmo assim não funcionar, entre em contato com um manteiner do projeto
$ docker run hello-world
```

Se os comandos de docker rodam no seu terminal mas não no terminal do Vscode, pode ser um bug pela instalação do Vscode com flatpak. Tente instalar o Vscode por deb no lugar.

##### Início rápido

Para rodar o Sisenex localmente, vá até o diretório clonado e execute


```bash
$ yarn install
```

```bash
$ yarn local up
```

Esse processo irá subir todos os containers localmente. Para acessar cada ambiente individualmente pode checar o IP e portas utilizando o comando que mostra os containers rodando ou achá-los nos arquivos da pasta config.

```bash
$ docker ps
```

#### Docker-compose

O [docker-compose](https://docs.docker.com/compose/) permite orquestrar o processo de iniciar e parar varios containers, volumes e redes virtuais

A configuração do `compose` é feita com um arquivo `docker-compose.yml`. Nesse arquivo são especificados `servicos`, que nada mais são que containers que devem ser iniciados e terminados de forma coordenada.

Exemplo de arquivo do compose:

```yml
# Versao do compose
version: "3.3"

#Servicos a serem orquestrados
services:
  hello-world:
    # Nome legivel para o container
    container_name: pikachu
    # Qual imagem deve ser utilizada para esse servico
    image: hello-world
    # Redes as quais esse servico pertence
    networks:
      - hello-net
    # Mapping de volumes persistentes e links simbolicos
    volumes:
      # Volume
      - "hello-volume:/app"
      # Link simbolico
      - "./config.js:/app/config.js"

  nginx:
    # Mapping de portas - 80:80 faz o link da porta 80 no seu PC para porta 80 no container
    ports:
      - "80:80"
    volume:
      # Bind mount pratico
      - "./nginx.conf:/etc/nginx/nginx.conf"

# Definicoes de redes
networks:
  hello-net:

# Definicoes de volumes
volumes:
  hello-volume:
```

Esse arquivo pode ser utilizado pela cli do `compose`:

```bash
# O parametro -f indica que o arquivo a seguir deve ser utilizado pelo compose
# Por default o valor desse arquivo ja é docker-compose.yml
$ docker-compose -f docker-compose.yml up
```

Multiplos arquivos do compose podem ser utilizados sequencialmente, de forma que as configurações especificadas em cada arquivo sobrescrevem as dos anteriores. Isso permite definir um `docker-compose.yml` basico seguido de um por ambiente.

Nesse projeto foram definidos multiplos arquivos `docker-compose.yml`, sendo um por ambiente.

Para executar os containers referentes a um ambiente especifico basta fazer:

```bash
$ docker-compose -f docker-compose.yml -f docker-compose.AMBIENTE.yml COMANDO
```

Para evitar repetição foram criados scripts na raiz do repositório que já prefixam esse comando ao longo da sua operação, de forma que:

```bash
$ yarn local up
```

tem o mesmo efeito que:

```bash
$ docker-compose -f docker-compose.yml -f docker-compose.local.yml up
```

#### Docker no windows

É recomendado utilizar linux para participar do desenvolvimento desse projeto, mas se por algum motivo for **necessário** realizar o desenvolvimento no Windows, siga os [passos oficiais](https://docs.docker.com/docker-for-windows/) de instalação no windows.

## Ferramentas de desenvolvimento

O projeto possui alguns arquivos de configuração que visam garantir uma boa experiência de desenvolvimento e consistência no estilo de código. A utilização dos mesmos, no entanto, depende da instalação de alguns plugins.

E recomendada a utilização do editor [VSCode](https://code.visualstudio.com/download) com os seguintes plugins:

#### Extensões

- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) para Javascript e [TSlint](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin) para Typescript
  > Highlight de erros comuns e correcão automática
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
  > Formatação automática com guia de estilos pré-configurado
- [Commitzen](https://marketplace.visualstudio.com/items?itemName=KnisterPeter.vscode-commitizen)
  > Commits que seguem o padrão de [commits convencionais](https://www.conventionalcommits.org/en/v1.0.0-beta.3/)

#### React - Qualidade de vida

- [Auto Close Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag)
  > Fechamento automático de tags
- [Auto Rename Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)
  > Rename automático de ambas extremidades de uma tag

### Markdown - Escrita de documentação

- [Mardown - All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
  > Diversos utilitários para trabalho em markdown, eg: inserção automática de TOC

## Iniciando pacotes

### Sem docker

Cada projeto pode ter scripts de iniciação diferentes, mas todos utilizam o [yarn](https://yarnpkg.com/lang/en/docs/install/#debian-stable) como gerenciador de pacotes.

Normalmente para iniciar um pacote basta fazer:

1. Instale dependências

```bash
$ cd packages/<package-name>
$ yarn install
```

2. Inicie a aplicação

```bash
$ yarn start
```

### Com Docker

Basta utilizar do `docker-compose` e espeficar um serviço:

```bash
$ yarn local up backend
# Equivalente:
# $ docker-compose -f docker-compose.yml -f docker-compose.local.yml up backend
```

Questões relevantes:

[Como executar um comando dentro de um container?](#como-executar-um-comando-dentro-de-um-container)
[Como semear o banco?](#como-semear-o-banco)

## Contribuindo

### Branchs e Ambientes

#### Devel

Na branch `devel` são publicadas mudanças experimentais ou instáveis. Commits nessa branch geram releases no seguinte ambiente:

| API - URL                      | Web - URL               | Expo-url                                                   |
| ------------------------------ | ----------------------- | ---------------------------------------------------------- |
| http://150.165.202.197/graphql | http://150.165.202.197/ | https://expo.io/@sisenexlumo/sisenex?release-channel=devel |

Arquivos de configuração do ambiente `devel` estão na pasta `config/devel`

Cada novo commit em `origin/devel` inicia o seguinte pipeline:

![docs/assets/dev-pipeline.png](docs/assets/dev-pipeline.png)

#### Staging

Na branch `staging` são publicadas mudanças estáveis suficientes para testes. Commits nessa branch geram releases no seguinte ambiente:

| API - URL                      | Web - URL               | Expo-url                                                     |
| ------------------------------ | ----------------------- | ------------------------------------------------------------ |
| http://150.165.202.197/graphql | http://150.165.202.197/ | https://expo.io/@sisenexlumo/sisenex?release-channel=staging |

Arquivos de configuraçãoo do ambiente `devel` estão na pasta `config/devel`

Cada novo commit em `origin/staging` inicia o seguinte pipeline:

#### Master

Na branch `master` são publicadas mudanças estáveis que podem ser colocadas em produção.

Publicação nessa branch deve ser feita através de merge requests que serão avaliados pelo time, pois cada novo commit altera o estado da aplicação real disponível na PlayStore.

Commits nessa branch geram releases no seguinte ambiente:

| API - URL                  | Web - URL               |
| -------------------------- | ----------------------- |
| http://sisenex.ufpb.br/api | http://sisenex.ufpb.br/ |

Cada novo commit em `origin/master` inicia o seguinte pipeline:

![docs/assets/prod-pipeline.png](docs/assets/prod-pipeline.png)

#### WIP - Work in progress

- No caso de trabalhos de longo prazo - vários dias/semanas - cada desenvolvedor pode criar branchs individuais, nesse caso o nome da branch deve seguir o padrão `@<sigla com ate tres caracteres>/<nome da branch>` eg: `@vmm/docs`.

#### Issue branchs

- Branchs de issues tambem são permitidas, nesse caso o nome da branch deve seguir o padrão `issue-<issue-id>` eg: `issue-13`

#### Executando localmente

Na maioria dos casos enquanto desenvolve sera necessário iniciar apenas um pacote, pois versões de desenvolvimento de outros já estarão disponiveis em seus urls remotos.

Caso venha a ser util executar todo o projeto localmente, siga os passos a seguir:

2 - Execute o comando:

```bash
$ yarn local build
$ yarn local up
```

Havendo executado o comando acima ficaram disponíveis:

| API - URL            | Web - URL         |
| -------------------- | ----------------- |
| http://localhost/api | http://localhost/ |

QRCode para execução do expo no celular ficara disponível no terminal onde o comando foi executado.

### Commits

- Commits devem ser realizados seguindo o padrão de [ commits convencionais ](https://www.conventionalcommits.org/en/v1.0.0-beta.3/), o que pode e automatizado pela extensão [commitzen](https://marketplace.visualstudio.com/items?itemName=KnisterPeter.vscode-commitizen)
- Exemplo de escopos que podem ser utilizados nos commits:
  - mobile
  - backend
  - web
  - project

### Fluxos de trabalho

#### Novo feature

Eg. @v-in implementando feature-1.

##### Branch local

A branch de feature pode existir inteiramente local, sem uma correspondente remota. Nesse caso, segue o seguinte workflow:

```bash
#Criacao da branch local
git checkout -b  @vmm/feature-1
#Commits de trabalho realizado...
#Atualize estado da branch atual com mudancas realizadas na devel. Como a branch e inteiramente local, e 100% seguro realizar rebase
git checkout devel
git pull
git checkout @vmm/feature-1
git rebase devel
#Repita o processo acima ao longo de seu tempo de trabalho. Quando finalizar a feature sera hora de realizar o merge feature -> devel:
git checkout devel
git merge @vmm/feature-1
#Feature agora faz parte da devel
git push
```

A vantagem de usar rebase é que a atualização devel -> feature não polui o histórico da branch feature com mensagens de merge / outros commits, gerando uma história mais limpa após o merge feature -> devel ( simples fast-forward )

Em casos em que sua branch local tem correspondente remota, rebase feito dessa forma pode quebrar a branch para outras pessoas que estejam a usando, logo só deve ser realizado em branchs privadas.

##### Branch remota

Caso comum para WIP branchs

Caso sua branch de feature tenha correspondente remota na qual outras pessoas podem commitar / dar merge, use merges e não rebases para atualizar a branch de feature:

```bash
#Criacao da branch local
git checkout -b  @vmm/feature-1
#Commits de trabalho realizado...
#Atualize estado da branch atual com mudancas realizadas na devel.
git checkout devel
git pull
git checkout @vmm/feature-1
git merge devel
#Repita o processo acima ao longo de seu tempo de trabalho. Quando finalizar a feature sera hora de realizar o merge feature -> devel:
git checkout devel
git merge @vmm/feature-1
git push
```

Como dito antes a desvantagem de usar merges como forma de atualizar a branch de feature com mudancas da devel é a poluicao do histórico de commits da branch de feature. De toda forma a realização de multiplos merges pequenos causa menos dor de cabeca que um merge de branchs que divergiram por semanas.

#### Corrigindo bugs / Resolvendo issues

Mesmo workflow que o de novas features, com exceção de o nome da branch ser a issue correspondente ao bug.

#### Mudancas longas

Mesmo workflow de implementação de features com branch remota, pois uma branch apenas local com commits de vários dias pode resultar em muito trabalho perdido caso tenha problema com dispositivo.

#### Deployment

Deployment é feito de forma automática sempre que commits são realizados nas branchs devel e master.

Atualizações da branch master devem ser feitas sempre com merge requests da branch devel para branch master. Esses requests podem ser feitos pela interface do gitlab e serão avaliados pelo time

## DB: Manutenção

### Backup e Restore

Backups realizados pela interface do pgadmin devem ser configurados da seguinte forma:

- Formato: TAR
- Nome do arquivo: `<ANO>-<MES>-<DIA>T<HORA>:<MINUTO>:<SEGUNDO>Z_<NOME DO BACKUP>.backup` ( ex: 2019-11-14T23:49:00Z_pos-treinamentos.backup )
- Dump Options:
  - Pre-data
  - Post-data
  - Data
  - Blobs
  - Verbose messages

Para carregar arquivos de backup devem ser usadas as opções:

- Pre-data
- Post-data
- Data
- Single Transactions
- Verbose messages

Os arquivos de backup gerados ficam na pasta `/var/lib/docker/volumes/sisenex_pgadmin/_data/storage/sisenex_ci.com/backups` da maquina que está hospedando a instância do PGAdmin em questão.

A copia de tais backups para outras maquinas pode ser feita por [scp](https://linux.die.net/man/1/scp):

```bash
# Exemplo de processo para transferir arquivo de backup da maquina de producao para 150.165.202.196, na pasta backups do usuario vinicius
$ ssh sisenex@sisenex.ufpb.br
$ sudo su
cd /var/lib/docker/volumes/sisenex_pgadmin/_data/storage/sisenex_ci.com/backups
scp ./2019-11-14T23:49:00Z_pos-treinamentos.backup  vinicius@150.165.202.196:/home/vinicius/backups
```

## FAQ

### Que operações posso realizar com docker?

Todos comandos do docker-compose estão disponíveis, para lista-los faça:

```bash
yarn local help
```

### Como semear o banco?

```bash
yarn local:seed
```

Caso o commit atual tenha introduzido uma mudança não retrocompatível ao banco, pode ser necessário remover o volume do `mysql`, garantindo que novas migrações sejam feitas com um banco virgem.

```bash
$ yarn local down
$ docker volume rm sisenex_db-local
$ yarn local up
```

### Como iniciar um serviço?

```bash
#Mude local para seu ambiente
$ yarn local up SERVICO
```

### Como iniciar todos os serviços?

```bash
$ yarn local up
```

### Como matar um serviço?

```bash
$ yarn local down SERVICO
```

### Como reiniciar um serviço?

```bash
$ yarn local restart SERVICO
```

### Quando é preciso recompilar imagens?

Quando mudancas forem feitas em algum Dockerfile do projeto será necessário recompilar as imagens:

```bash
$ yarn local build
```

### Como executar um comando dentro de um container?

```bash
#Para rodar um script que existe no container faca:
$ yarn local run SERVICO COMANDO
#Para abrir um terminal dentro do container faca:
$ yarn local run SERVICO sh
```

### Como listar containers?

```bash
#Para listar todos containers vivos no seu computador faca
$ docker ps
#Para listar apenas containers desse projeto vivos em seu computador faca
$ yarn local ps
```

### Como adicionar/configurar variaveis no **ambiente**?

Cada ambiente possui seu respectivo arquivo de configuração em `/config/local/.env`, basta alterar esses arquivos.

Configurações em comum entre todos containers eventualmente serão adicionadas em `/config/common.env`

### Como um container se comunica com outro?

O docker compose desse projeto define uma rede virtual chamada sisenex_base, todos containers dentro dessa rede podem se comunicar com outros dela através do nome de seus serviços, por exemplo:

O container `backend` cria uma conexão com o banco em `db` através de `db:3306`, o dns `db` e resolvido em um IP internamente pelo docker.

### Como acessar o banco que vive na rede virtual?

#### Com Docker

Depois de iniciado o docker, para abrir um terminal iterativo no container do banco faça:

```bash
yarn <AMBIENTE> exec db sh
```

Inicie o mysql

```bash
mysql -u USUARIO -p
```

Digite a senha do banco e vc terá o terminal do mysql disponível

#### Expondo portas do container ao pc

Para temporariamente expor um container da rede virtual a seu pc e possivel modificar o docker-compose corresponde adicionando/modificando a definição do serviço:

```yml
# Exemplo: Expondo porta do mysql para acesso direto na rede do seu PC
service:
  db:
    ports:
      - "PORTA_NO_SEU_PC:3306"
```

Após isso você podera, por exemplo, se conectar ao banco:

```bash
$ mysql -u USUARIO_BANCO -p<SENHA_BANCO>
```

### Como remover completamente dados do banco?

Foram definidos no compose volumes chamados `sisenex_db-AMBIENTE`, onde ficam dados do banco.
Para remover esses volumes basta fazer

```bash
#eg: sisenex_db-local
$ docker volume rm sisenex_db-AMBIENTE
```

Se o container que usa esse volume estiver vivo é necessário paralo antes da remoção do volume

```bash
$ yarn AMBIENTE down SERVICO
$ yarn AMBIENTE rm SERVICO
$ docker volume rm sisenex_db-AMBIENTE
```

## Troubleshooting

### Seeds não funcionam

Vide [Como semear o banco?](#como-semear-o-banco)

### Bind error com o proxy ao iniciar o projeto

Os scripts de iniciação dos containers especifica um container `proxy` que expõe a porta `80/tcp` no seu PC, caso essa porta já esteja ocupada tais scripts irão falhar.

Caso você tenha `apache` ou `nginx` instalados e deseja para-los temporariamente para inicar o projeto faça:

```bash
# Para parar o servico
sudo service nginx stop

# Para reiniciar o servico
sudo service nginx start

# Para impedir que o servico inicie automaticamente ao reinicar PC
sudo systemctl disable nginx
```
